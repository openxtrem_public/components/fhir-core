<h1>OX-FHIR-CORE</h1>

<h2>About</h2>

This library includes a generator for creating PHP classes from the HL7 FHIR specification, together with serializers and parsers for JSON and XML.

__The project is under development__

<h2>Generated classes usage example</h2>

```php
$humanName = (new FHIRHumanName())
    ->setFamily('MyFamilyName')
    ->addGiven('MyGiven')
    ->addGiven('MySecondGiven');

$patient = (new FHIRPatient())
    ->setActive(true)
    ->setName($humanName);
```

<h2>Serializer</h2>

* Using Generated classes and structure definition (R4 FHIR version)

**<h3>Example</h3>**

PHP Usage
```php
$XMLSerializer = new XMLSerializer();
$JSONSerializer = new JSONSerializer();

$XMLPatientString = $XMLSerializer->serializeResource($patient)
```

or 

```php
$XMLPatientString = (new XMLSerializer())->serializeResource($patient)
```

<h2>Parser</h2>

* Using Generated classes and structure definition (R4 FHIR version)

**<h3>Example</h3>**

XML Patient
```xml
<?xml version="1.0" encoding="UTF-8"?>
<Patient xmlns="http://hl7.org/fhir">
  <active value="true"/>
  <name>
    <family value="MyFamilyName"/>
    <given value="MyGiven"/>
    <given value="MySecondGiven"/>
  </name>
</Patient>
```

PHP Usage
```php
$XMLParser = new XMLParser(FHIRVersion::R4);
$JSONParser = new JSONParser(FHIRVersion::R4);

$patient = $XMLParser->parse($XMLPatientString)
```

or 

```php
$patient = Parser::parse($XMLPatientString, FHIRVersion::R4)
```

<h2>Writing values</h2>

**Initialize fhir_path**
```php
$fhir_path = new \Ox\Components\FHIRCore\FHIRPath\FHIRPath();
```

**Place complex values**
```php
$patient = new \Ox\Components\FHIRCore\Model\R4\Resources\FHIRPatient();
$new_id = new \Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier();

// Overwrite
$fhir_path->placeValue($patient, 'Patient.identifier', $new_id);

// Without root name
$fhir_path->placeValue($patient, 'identifier', $new_id);

// append at index 1
$fhir_path->placeValue($patient, 'Patient.identifier[1]', $new_id);

// overwrite at index 0
$fhir_path->placeValue($patient, 'Patient.identifier[0]', $new_id);

// Append
$fhir_path->placeValue($patient, 'Patient.identifier[]', $new_id);

// Creates needed datatypes
$fhir_path->placeValue($fhir_patient, 'Patient.name.given', 'Erwan');
```