<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Tests\Model;

use Ox\Components\FHIRCore\Model\R4\StructureDefinition;

class R4GeneratedTest extends AbstractGeneratedTest
{
    final public const STRUCTURE_DEFINITION = StructureDefinition::class;
}
