<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Tests\Model;

use Exception;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRBaseInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRElementInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRBase64BinaryInterface;
use Ox\Components\FHIRCore\Interfaces\StructureDefinitionInterface;
use Ox\Components\FHIRCore\Tests\Trait\MultiVersionsHelperTrait;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use ReflectionException;

abstract class AbstractGeneratedTest extends TestCase
{
    use MultiVersionsHelperTrait;

    public const STRUCTURE_DEFINITION = StructureDefinitionInterface::class;

    /**
     * @throws ReflectionException
     */
    public static function providerDefinitions(): array
    {
        $not_abstract = array_filter(
            static::STRUCTURE_DEFINITION::DEFINITIONS,
            fn($def) => !(new ReflectionClass($def['object']))->isAbstract()
        );

        return [[$not_abstract]];
    }

    /**
     * @dataProvider providerDefinitions
     *
     * @param array $definitions
     *
     * @return void
     */
    public function testEveryFilesInstanced(array $definitions): void
    {
        $classes = [];
        foreach ($definitions as $resource) {
            $classes[] = new $resource['object']();
        }

        $this->assertSameSize($definitions, $classes);
    }

    /**
     * @dataProvider providerDefinitions
     *
     * @param array $definitions
     *
     * @return void
     */
    public function testEveryValueReturnNullByDefault(array $definitions): void
    {
        $returned = [];

        foreach ($definitions as $resource) {
            $object = new $resource['object']();
            foreach ($resource['fields'] as $field) {
                $getValue = $object->{'get' . ucfirst($field['label'])}();
                if ($field['unique']) {
                    $returned[] = is_null($getValue);
                } else {
                    $returned[] = empty($getValue);
                }
            }
        }

        $this->assertNotContains('false', $returned);
    }

    /**
     * @dataProvider providerDefinitions
     *
     * @param array $definitions
     *
     * @return void
     */
    public function testEveryValueCanBeSetNullOrEmpty(array $definitions): void
    {
        $returned = [];

        foreach ($definitions as $resource) {
            $object = new $resource['object']();
            foreach ($resource['fields'] as $field) {
                if ($field['unique']) {
                    $object->{'set' . ucfirst($field['label'])}(null);
                    $getValue   = $object->{'get' . ucfirst($field['label'])}();
                    $returned[] = is_null($getValue);
                } else {
                    $object->{'set' . ucfirst($field['label'])}();
                    $getValue   = $object->{'get' . ucfirst($field['label'])}();
                    $returned[] = empty($getValue);
                }
            }
        }

        $this->assertNotContains('false', $returned);
    }

    /**
     * @dataProvider providerDefinitions
     * @depends      testEveryValueCanBeSetNullOrEmpty
     *
     * @param array $definitions
     *
     * @return void
     */
    public function testSetterReturnTheClassObject(array $definitions): void
    {
        $returned = [];
        foreach ($definitions as $resource) {
            $object = new $resource['object']();
            foreach ($resource['fields'] as $field) {
                if ($field['unique']) {
                    $returned[] = $object->{'set' . ucfirst($field['label'])}(null) === $object;
                } else {
                    $returned[] = $object->{'set' . ucfirst($field['label'])}() === $object;
                }
            }
        }

        $this->assertNotContains(false, $returned);
    }

    /**
     * @dataProvider providerDefinitions
     *
     * @param array $definitions
     *
     * @return void
     */
    public function testUniqueNonPrimitiveElementsReturnsSameValue(array $definitions): void
    {
        $returned = [];

        foreach ($definitions as $resource) {
            $object = new $resource['object']();
            foreach ($resource['fields'] as $field) {
                if (!$field['unique'] || $field['types'][0]['final']) {
                    continue;
                }
                $mock = $this->getMock($field['types'][0]['name'], $definitions);
                $object->{'set' . ucfirst($field['label'])}($mock);
                $returned[] = $object->{'get' . ucfirst($field['label'])}();
            }
        }
        $this->assertNotContains(false, $returned);
    }

    /**
     * @dataProvider providerDefinitions
     *
     * @param array $definitions
     *
     * @return void
     */
    public function testPrimitiveElementsReturnsSameValue(array $definitions): void
    {
        $returned = [];

        foreach ($definitions as $resource) {
            $object = new $resource['object']();
            foreach ($resource['fields'] as $field) {
                if (!$field['unique'] || !$field['types'][0]['final']) {
                    continue;
                }
                $str = ['http://hl7.org/fhirpath/System.String', 'http://hl7.org/fhirpath/System.Boolean'];
                if (in_array($field['types'][0]['name'], $str)) {
                    $value = 'myValue';
                } else {
                    $value = 1;
                }
                $object->{'set' . ucfirst($field['label'])}($value);
                $returned[] = $object->{'get' . ucfirst($field['label'])}();
            }
        }
        $this->assertNotContains(false, $returned);
    }

    private function isPrimitive(string $name, array $definitions): ?string
    {
        if (!key_exists($name, $definitions)) {
            return null;
        }
        if ($inherit = $this->isPrimitive($definitions[$name]['inherit'], $definitions)) {
            return $inherit;
        }
        $fields = $definitions[$name]['fields'];
        if (sizeof($fields) != 1) {
            return null;
        }
        if (!reset($fields)['unique']) {
            return null;
        }
        if (!reset($fields)['types'][0]['final']) {
            return null;
        }

        return $name;
    }

    /**
     * @dataProvider providerDefinitions
     *
     * @param array $definitions
     *
     * @return void
     */
    public function testUniquePrimitiveValueFieldsHandlePHPPrimitives(array $definitions): void
    {
        $returned = [];

        foreach ($definitions as $resource) {
            $object = new $resource['object']();
            foreach ($resource['fields'] as $field) {
                if ($field['types'][0]['final'] || (sizeof($field['types']) != 1)) {
                    continue;
                }
                $typeName = $field['types'][0]['name'];
                if (!($typeName = $this->isPrimitive($typeName, $definitions))) {
                    continue;
                }
                $str = ['http://hl7.org/fhirpath/System.String', 'http://hl7.org/fhirpath/System.Boolean'];
                if (in_array($definitions[$typeName]['fields'][0]['types'][0]['name'], $str)) {
                    $value = 'value';
                } else {
                    $value = 3;
                }
                $object->{'set' . ucfirst($field['label'])}($value);
                $getValue = $object->{'get' . ucfirst($field['label'])}();
                if (!$field['unique']) {
                    $getValue = reset($getValue);
                }
                $returned[] = is_a($getValue, FHIRElementInterface::class);
            }
        }
        $this->assertNotContains(false, $returned);
    }

    private function getMock(string $name, array $definitions): object
    {
        if (key_exists($name, $definitions)) {
            return $this->createMock($definitions[$name]['object']);
        }
        $types = array_filter(
            $definitions,
            fn($x) => str_ends_with($x['object'], 'FHIR' . $name)
        );
        if (count($types) === 0) {
            $types = [static::STRUCTURE_DEFINITION::DEFINITIONS['Patient']];
        }

        return $this->createMock(reset($types)['object']);
    }

    /**
     * @dataProvider providerDefinitions
     *
     * @param array $definitions
     *
     * @return void
     */
    public function testAdderIncrementHisValue(array $definitions): void
    {
        $returned = [];

        foreach ($definitions as $resource) {
            $object = new $resource['object']();
            foreach ($resource['fields'] as $field) {
                if ($field['unique']) {
                    continue;
                }
                $typeName = $field['types'][0]['name'];

                $mock = $this->getMock($typeName, $definitions);
                $object->{'set' . ucfirst($field['label'])}($mock, $mock);
                $object->{'add' . ucfirst($field['label'])}($mock);
                $returned[] = sizeof($object->{'get' . ucfirst($field['label'])}()) === 3;
            }
        }
        $this->assertNotContains(false, $returned);
    }

    /**
     * @dataProvider providerDefinitions
     *
     * @param array $definitions
     *
     * @return void
     */
    public function testEveryEmptyElementIsNullReturnTrue(array $definitions): void
    {
        $returned = [];

        foreach ($definitions as $resource) {
            $object     = new $resource['object']();
            $returned[] = $object->isNull();
        }

        $this->assertNotContains(false, $returned);
    }

    /**
     * @throws Exception
     */
    public function testIsNullMethodOnDatatype(): void
    {
        $string = self::getResource('string');

        $this->assertTrue($string->isNull());
        $this->assertNotTrue($string->setValue('value')->isNull());
        $this->assertNotTrue((self::getResource('string'))->setId('id')->isNull());
    }

    /**
     * @dataProvider providerNullResources
     *
     * @param mixed $resource
     * @param bool  $expected
     *
     * @return void
     */
    public function testIsNullMethodOnResource(FHIRBaseInterface $resource, bool $expected): void
    {
        $this->assertSame($expected, $resource->isNull());
    }

    /**
     * @throws Exception
     */
    public function providerNullResources(): array
    {
        $patient_object    = static::STRUCTURE_DEFINITION::DEFINITIONS['Patient']['object'];
        $human_name_object = static::STRUCTURE_DEFINITION::DEFINITIONS['HumanName']['object'];

        return [
            'Empty Patient'               => [self::getResource('Patient'), true],
            'Patient W/ id'               => [self::getResource('Patient')->setId('id'), false],
            'Active Patient'              => [self::getResource('Patient')->setActive(true), false],
            'Inactive Patient'            => [self::getResource('Patient')->setActive(false), false],
            'Name'                        => [self::getResource('HumanName'), true],
            'Empty Patient W/ Empty Name' => [self::getResource('Patient')->setName(new $human_name_object()), true],
            'Empty Patient W/ Name'       => [
                (new $patient_object())->setName(
                    (new $human_name_object())->addGiven('aGiven')
                ),
                false,
            ],
        ];
    }

    /**
     * @throws Exception
     */
    public function testIsNullWorkingOnElementWithoutProperty(): void
    {
        $age = self::getResource('Age');

        $this->assertTrue($age->isNull());
        $this->assertNotTrue($age->setId('id')->isNull());
    }

    /**
     * @dataProvider providerValidSystem
     *
     * @param string $base
     * @param string $test
     * @param bool   $expected
     *
     * @return void
     * @throws Exception
     */
    public function testIsSystemMatch(?string $base, string $test, bool $expected): void
    {
        $uri = self::getResource('uri')
                   ->setValue($base);

        $this->assertSame($expected, $uri->isMatch($test));
    }

    public function providerValidSystem(): array
    {
        return [
            ['urn:uuid:53fefa32-fcbb-4ff8-8a92-55ee120877b7', 'urn:uuid:53fefa32-fcbb-4ff8-8a92-55ee120877b7', true],
            'Uppercase' => [
                'urn:uuid:53fefa32-fcbb-4ff8-8a92-55ee120877b7',
                'urn:uuid:53fEfa32-fcbb-4ff8-8a92-55EE120877b7',
                false,
            ],
            ['urn:uuid:53fefa32-fcbb-4ff8-8a92-55ee120877b7', '53fefa32-fcbb-4ff8-8a92-55ee120877b7', true],
            ['53fefa32-fcbb-4ff8-8a92-55ee120877b7', 'urn:uuid:53fefa32-fcbb-4ff8-8a92-55ee120877b7', false],
            [null, 'urn:uuid:53fefa32-fcbb-4ff8-8a92-55ee120877b7', false],
        ];
    }

    /**
     * @dataProvider providerSystemCode
     *
     * @param string|null $baseS
     * @param string|null $baseC
     * @param string|null $testS
     * @param string|null $testC
     * @param bool        $expd
     *
     * @return void
     * @throws Exception
     */
    public function testCodingIsMatch(?string $baseS, ?string $baseC, ?string $testS, ?string $testC, bool $expd): void
    {
        $coding = self::getResource('Coding')
                      ->setCode($baseC)
                      ->setSystem(
                          self::getResource('uri')->setValue($baseS)
                      );

        $this->assertSame($expd, $coding->isMatch($testS, $testC));
    }

    public function providerSystemCode(): array
    {
        return [
            ['urn:uuid:uri', 'code', 'urn:uuid:uri', 'code', true],
            ['urn:uuid:uri', 'code', 'uri', 'code', true],
            ['urn:uuid:uri', 'code', 'urn:uuid:uri', 'Code', false],
            ['urn:uuid:uri', 'code', 'urn:uuid:Uri', 'code', false],
            ['urn:uuid:uri', 'code', 'urn:uuid:uri', 'code1', false],
            ['urn:uuid:uri', null, 'urn:uuid:uri', 'code', false],
            [null, 'code', 'urn:uuid:uri', 'code', false],
        ];
    }

    public function testSearchCoding(): void
    {
        $coding_object           = static::STRUCTURE_DEFINITION::DEFINITIONS['Coding']['object'];
        $codeable_concept_object = static::STRUCTURE_DEFINITION::DEFINITIONS['CodeableConcept']['object'];

        $coding1 = (new $coding_object())
            ->setSystem('urn:uuid:1')
            ->setCode('code');

        $coding2 = (new $coding_object())
            ->setSystem('urn:uuid:2')
            ->setCode('code2');

        $cc = (new $codeable_concept_object())
            ->addCoding($coding1, $coding2);

        $this->assertNull((new $codeable_concept_object())->searchCoding('test', 'test'));
        $this->assertSame($coding1, $cc->searchCoding('1', 'code'));
        $this->assertSame($coding2, $cc->searchCoding('urn:uuid:2'));
    }

    /**
     * @dataProvider providerEncodedBase64
     *
     * @param FHIRBase64BinaryInterface $datatype
     * @param string                    $expected
     *
     * @return void
     */
    public function testGetDecoded(FHIRBase64BinaryInterface $datatype, ?string $expected): void
    {
        $this->assertSame($expected, $datatype->getDecodedData());
    }

    public function providerEncodedBase64(): array
    {
        $decodedValue  = 'MyEncodedTest';
        $base64_object = static::STRUCTURE_DEFINITION::DEFINITIONS['base64Binary']['object'];

        return [
            [(new $base64_object())->setValue($decodedValue, false), $decodedValue],
            [(new $base64_object())->setValue(base64_encode($decodedValue), true), $decodedValue],
            [(new $base64_object())->setValue(base64_encode($decodedValue)), $decodedValue],
            [new $base64_object(), null],
        ];
    }

    public function testSearchingExtensionByUrl(): void
    {
        $string_object    = static::STRUCTURE_DEFINITION::DEFINITIONS['string']['object'];
        $extension_object = static::STRUCTURE_DEFINITION::DEFINITIONS['Extension']['object'];

        $searchingExtension = (new $extension_object())
            ->setUrl('s2')
            ->setValue((new $string_object())->setValue('aValue'));

        $element = (new $string_object())
            ->setValue('myValue')
            ->addExtension(
                (new $extension_object())->setUrl('s1'),
                (new $extension_object())
                    ->setUrl('s1')
                    ->setExtension($searchingExtension),
                (new $extension_object())->setUrl('1'),
                (new $extension_object())->setUrl('s2')
            );

        $found = $element->getExtensionFromUrls('s1', 's2');

        $this->assertSame($searchingExtension, $found);
    }
}
