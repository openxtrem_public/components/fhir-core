<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Tests\Validator;

use Exception;
use Ox\Components\FHIRCore\Enum\FHIRVersion;
use Ox\Components\FHIRCore\Interfaces\Resources\FHIROperationOutcomeInterface;
use Ox\Components\FHIRCore\Parser\Exception\NoElementException;
use Ox\Components\FHIRCore\Parser\Exception\ParserException;
use Ox\Components\FHIRCore\Parser\JSONParser;
use Ox\Components\FHIRCore\Parser\XMLParser;
use Ox\Components\FHIRCore\Serializer\Exception\SerializerException;
use Ox\Components\FHIRCore\Serializer\JSONSerializer;
use Ox\Components\FHIRCore\Serializer\XMLSerializer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * Description
 */
class ValidatorTest extends TestCase
{
    final public const TIMEOUT = 300;

    /**
     * @throws TransportExceptionInterface
     */
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::assertTrue(self::hasValidator(), 'FHIR Validator Connection');
    }

    /**
     * @throws TransportExceptionInterface
     */
    private static function hasValidator(): bool
    {
        $response = HttpClient::create()->request(
            Request::METHOD_GET,
            'http://fhir_validator:8099/',
            ['timeout' => self::TIMEOUT]
        );

        return $response->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE;
    }

    /**
     * @param string $package_version
     * @param string $resource_content
     * @param string $resource_version
     * @param string $resource_profile
     * @param string $resource_format
     *
     * @return ResponseInterface
     * @throws TransportExceptionInterface
     */
    private function validateWithDocker(
        string $package_version,
        string $resource_content,
        string $resource_version,
        string $resource_profile,
        string $resource_format
    ): ResponseInterface {
        $data_part = new DataPart(
            $resource_content,
            "resource.$resource_format",
            "application/$resource_format",
            'base64'
        );

        $formData = new FormDataPart(
            [
                'package_version'  => $package_version,
                'resource_file'    => $data_part,
                'resource_version' => $resource_version,
                'resource_profile' => $resource_profile,
            ]
        );

        return HttpClient::create()->request(
            Request::METHOD_POST,
            'http://fhir_validator:8099',
            [
                'headers' => $formData->getPreparedHeaders()->toArray(),
                'body'    => $formData->bodyToIterable(),
                'timeout' => self::TIMEOUT,
            ],
        );
    }

    /**
     * @throws SerializerException
     * @throws NoElementException
     * @throws ParserException
     */
    public static function getResourceString(string $original, string $format, FHIRVersion $version): string
    {
        $serializer = ($format === 'xml') ? new XMLSerializer() : new JSONSerializer();

        $parsedXML  = (new XMLParser($version))->parseResource(file_get_contents("$original.xml"));
        $parsedJSON = (new JSONParser($version))->parseResource(file_get_contents("$original.json"));

        self::assertSame(
            $serializer->serializeResource($parsedJSON, ["summarize" => true, 'formatOutput' => false]),
            $serializer->serializeResource($parsedXML, ["summarize" => true, 'formatOutput' => false]),
        );

        return $serializer->serializeResource(($format === 'xml') ? $parsedJSON : $parsedXML);
    }

    public function assertNoError(FHIROperationOutcomeInterface $outcome, string $message): void
    {
        $result = true;
        foreach ($outcome->getIssue() as $issue) {
            if ($issue->getSeverity()->getValue() === "error") {
                $result = false;
                break;
            }
        }

        $this->assertTrue($result, $message);
    }

    public function providerResources(): array
    {
        return [
            'Patient'                  => [
                FHIRVersion::R4,
                __DIR__ . '/resources/patient',
                'hl7.fhir.us.core',
                '4.0',
                'http://hl7.org/fhir/StructureDefinition/Patient',
            ],
            'Patient deceased boolean' => [
                FHIRVersion::R4,
                __DIR__ . '/resources/patient-deceased-boolean',
                'hl7.fhir.us.core',
                '4.0',
                'http://hl7.org/fhir/StructureDefinition/Patient',
            ],
            'Patient deceased date'    => [
                FHIRVersion::R4,
                __DIR__ . '/resources/patient-deceased-date',
                'hl7.fhir.us.core',
                '4.0',
                'http://hl7.org/fhir/StructureDefinition/Patient',
            ],
            'Patient Chinese'          => [
                FHIRVersion::R4,
                __DIR__ . '/resources/patient-chinese',
                'hl7.fhir.us.core',
                '4.0',
                'http://hl7.org/fhir/StructureDefinition/Patient',
            ],
            'Observation profiled'     => [
                FHIRVersion::R4,
                __DIR__ . '/resources/observation-fhir-profile',
                'hl7.fhir.us.core',
                '4.0',
                'http://hl7.org/fhir/StructureDefinition/vitalsigns',
            ],
            'Practitioner'             => [
                FHIRVersion::R4,
                __DIR__ . '/resources/practitioner',
                'hl7.fhir.us.core',
                '4.0.1',
                'http://hl7.org/fhir/StructureDefinition/Practitioner',
            ],
            'Bundle Response'          => [
                FHIRVersion::R4,
                __DIR__ . '/resources/bundle-response',
                'hl7.fhir.us.core',
                '4.0.1',
                'http://hl7.org/fhir/StructureDefinition/Bundle',
            ],
            'Bundle Transaction'       => [
                FHIRVersion::R4,
                __DIR__ . '/resources/bundle-transaction',
                'hl7.fhir.us.core',
                '4.0.1',
                'http://hl7.org/fhir/StructureDefinition/Bundle',
            ],
            'Bundle Batch'             => [
                FHIRVersion::R4,
                __DIR__ . '/resources/bundle-batch',
                'hl7.fhir.us.core',
                '4.0.1',
                'http://hl7.org/fhir/StructureDefinition/Bundle',
            ],
            'Contained'                => [
                FHIRVersion::R4,
                __DIR__ . '/resources/contained',
                'hl7.fhir.us.core',
                '4.0.1',
                'http://hl7.org/fhir/StructureDefinition/Condition',
            ],
            'Organization'             => [
                FHIRVersion::R4,
                __DIR__ . '/resources/organization',
                'hl7.fhir.us.core',
                '4.0.1',
                'http://hl7.org/fhir/StructureDefinition/Organization',
            ],
            /*'Patient FR'               => [
            FHIRVersion::R4,
                __DIR__ . '/resources/patient-fr.xml',
                'hl7.fhir.fr.core',
                '4.0',
                'https://interop-sante.github.io/hl7.fhir.fr.core/ig/main/StructureDefinition-fr-core-patient.xml',
            ],*/
            /*'Patient FR as US'         => [
            FHIRVersion::R4,
                __DIR__ . '/resources/patient-fr.xml',
                'hl7.fhir.us.core',
                '4.0',
                'http://hl7.org/fhir/StructureDefinition/Patient',
            ],*/
        ];
    }

    /**
     * @dataProvider providerResources
     * @throws Exception|TransportExceptionInterface
     */
    public function testResourcesAreValidXML(
        FHIRVersion $FHIR_version,
        string      $path,
        string      $package_version,
        string      $resource_version,
        string      $resource_profile
    ): void {
        $resource_format = 'xml';
        $resource = self::getResourceString($path, $resource_format, $FHIR_version);

        $response = self::validateWithDocker(
            $package_version,
            $resource,
            $resource_version,
            $resource_profile,
            $resource_format,
        );

        $this->assertSame(200, $response->getStatusCode());

        $content = $response->getContent();
        /** @var FHIROperationOutcomeInterface $outcome */
        $outcome = (new JSONParser($FHIR_version))->parseResource($content);

        $this->assertNoError($outcome, $content);
    }

    /**
     * @dataProvider providerResources
     * @throws Exception|TransportExceptionInterface
     */
    public function testResourcesAreValidJSON(
        FHIRVersion $FHIR_version,
        string      $path,
        string      $package_version,
        string      $resource_version,
        string      $resource_profile
    ): void {
        $resource_format = 'json';
        $resource = self::getResourceString($path, $resource_format, $FHIR_version);

        $response = self::validateWithDocker(
            $package_version,
            $resource,
            $resource_version,
            $resource_profile,
            $resource_format,
        );

        $this->assertSame(200, $response->getStatusCode());

        $content = $response->getContent();
        /** @var FHIROperationOutcomeInterface $outcome */
        $outcome = (new JSONParser($FHIR_version))->parseResource($content);

        $this->assertNoError($outcome, $content);
    }
}
