<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Tests\Utils;

use Exception;
use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRPathException;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRBaseInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRElementInterface;
use Ox\Components\FHIRCore\Interfaces\StructureDefinitionInterface;
use Ox\Components\FHIRCore\Parser\Exception\NoElementException;
use Ox\Components\FHIRCore\Parser\Exception\ParserException;
use Ox\Components\FHIRCore\Parser\XMLParser;
use Ox\Components\FHIRCore\Utils\Comparisons;
use PHPUnit\Framework\TestCase;

abstract class AbstractComparisonTest extends TestCase
{
    public const STRUCTURE_DEFINITION = StructureDefinitionInterface::class;

    /**
     * @throws NoElementException
     * @throws ParserException
     */
    public function providerEqualityResults(): array
    {
        $boolean_object    = static::STRUCTURE_DEFINITION::DEFINITIONS['boolean']['object'];
        $patient_object    = static::STRUCTURE_DEFINITION::DEFINITIONS['Patient']['object'];
        $human_name_object = static::STRUCTURE_DEFINITION::DEFINITIONS['HumanName']['object'];

        $bt1 = (new $boolean_object())->setValue(true);
        $bt2 = (new $boolean_object())->setValue(true);
        $bf  = (new $boolean_object())->setValue(false);

        $p1 = (new $patient_object())->setName(
            (new $human_name_object())
                ->setGiven('a', 'b')
                ->setFamily('c'),
            (new $human_name_object())
                ->setFamily('d')
        );

        $p2 = (new $patient_object())->setName(
            (new $human_name_object())
                ->setGiven('a', 'b')
                ->setFamily('c'),
            (new $human_name_object())
                ->setFamily('d')
        );

        $p3 = (new $patient_object())->setName(
            (new $human_name_object())
                ->setGiven('a', 'e')
                ->setFamily('c'),
            (new $human_name_object())
                ->setFamily('d')
        );

        $p4 = (new XMLParser(static::FHIR_VERSION))
            ->parseResource(file_get_contents(__DIR__ . '/../resources/patient-example.xml'));

        return [
            [$bt1, $bt2, true],
            [$bt1, $bf, false],
            [$bt1, $p1, false],
            [$p1, $p2, true],
            [$p1, $p3, false],
            [$p4, $p3, false],
            [$p3, $p4, false],
            [$p4, $p4, true],
        ];
    }

    /**
     * @dataProvider providerEqualityResults
     * @throws Exception
     */
    public function testEqual(FHIRBaseInterface $left, FHIRBaseInterface $right, bool $expected): void
    {
        $helper = new Comparisons();

        $result = $helper->equal($left, $right);

        $this->assertSame($expected, $result);
    }

    public function providerIncludes(): array
    {
        $int_object = static::STRUCTURE_DEFINITION::DEFINITIONS['integer']['object'];

        $i1 = (new $int_object())->setValue(1);
        $i2 = (new $int_object())->setValue(2);
        $i3 = (new $int_object())->setValue(3);

        return [
            [$i1, [$i2, $i3, $i2], false],
            [$i1, [$i2, $i3, $i1], true],
            [$i1, [], false],
            [$i1, [$i1], true],
        ];
    }

    /**
     * @dataProvider providerIncludes
     * @throws FHIRPathException
     */
    public function testInclude(FHIRElementInterface $needle, array $haystack, bool $expected): void
    {
        $helper = $this->getMockBuilder(Comparisons::class)
                       ->onlyMethods(['equal'])
                       ->getMock();
        $helper->expects($this->any())
               ->method('equal')
               ->willReturnCallback(
                   fn($l, $r) => $l->getValue() === $r->getValue()
               );

        $result = $helper->includes($needle, $haystack);

        $this->assertSame($expected, $result);
    }

    public function providerSubsets(): array
    {
        $int_object = static::STRUCTURE_DEFINITION::DEFINITIONS['integer']['object'];

        $i1 = (new $int_object())->setValue(1);
        $i2 = (new $int_object())->setValue(2);
        $i3 = (new $int_object())->setValue(3);

        return [
            '1 not in'     => [[$i1], [$i2, $i3, $i2], false],
            '1 in 1 out'   => [[$i1, $i3], [$i2, $i2, $i1], false],
            '1 in'         => [[$i1], [$i2, $i3, $i1], true],
            'empty super'  => [[$i1], [], false],
            'empty subset' => [[], [$i1], true],
            'same'         => [[$i1], [$i1], true],
        ];
    }

    /**
     * @depends      testInclude
     * @dataProvider providerSubsets
     *
     * @param array $subset
     * @param array $superset
     * @param bool  $expected
     *
     * @return void
     * @throws FHIRPathException
     */
    public function testSubsets(array $subset, array $superset, bool $expected): void
    {
        $helper = $this->getMockBuilder(Comparisons::class)
                       ->onlyMethods(['equal'])
                       ->getMock();
        $helper->expects($this->any())
               ->method('equal')
               ->willReturnCallback(
                   fn($l, $r) => $l->getValue() === $r->getValue()
               );

        $result = $helper->subsetOf($subset, $superset);

        $this->assertSame($expected, $result);
    }
}
