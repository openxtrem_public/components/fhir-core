<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Tests\Utils;

use Ox\Components\FHIRCore\Enum\FHIRVersion;
use Ox\Components\FHIRCore\Model\R4B\StructureDefinition;

class ComparisonR4BTest extends AbstractComparisonTest
{
    final public const    STRUCTURE_DEFINITION = StructureDefinition::class;
    final protected const FHIR_VERSION         = FHIRVersion::R4B;
}
