<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Tests\FHIRPath;

use Ox\Components\FHIRCore\Model\R4\StructureDefinition;

final class FHIRPathR4Test extends AbstractFHIRPathTest
{
    final protected const STRUCTURE_DEFINITION = StructureDefinition::class;
}
