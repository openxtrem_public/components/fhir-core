<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Tests\FHIRPath;

use Exception;
use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRPathException;
use Ox\Components\FHIRCore\FHIRPath\FHIRPath;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRBaseInterface;
use Ox\Components\FHIRCore\Interfaces\Resources\FHIRResourceInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRDataRequirement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRDosage;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRHumanName;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRRatio;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Resources\FHIRObservation;
use Ox\Components\FHIRCore\Model\R4\Resources\FHIRPatient;
use PHPUnit\Framework\TestCase;

final class FHIRPathWriteTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public static function assertSame($expected, $actual, string $message = ''): void
    {
        if (!($expected instanceof FHIRBaseInterface)) {
            $actual = array_map(fn($x) => $x->getValue(), $actual);
        }

        if (!is_array($expected)) {
            $expected = [$expected];
        }

        parent::assertSame($expected, $actual, $message);
    }

    public function providerFHIRPath(): array
    {
        $period = (new FHIRPeriod())
            ->setStart('start date')
            ->setEnd('end date');

        return [
            'Root element'      => [new FHIRPatient(), 'Patient.birthDate', 'date', 'date'],
            'Root datatype'     => [new FHIRPatient(), 'Patient.birthDate', (new FHIRDate())->setValue('date'), 'date'],
            'Backbone'          => [new FHIRPatient(), 'Patient.name.family', 'test_name', 'test_name'],
            'Backbone multiple' => [new FHIRPatient(), 'Patient.name.given', 'test_given', 'test_given'],
            'Backbone []'       => [new FHIRPatient(), 'Patient.name.given[0]', 'test_given', 'test_given'],
            'Choice datatype1'  => [
                new FHIRPatient(),
                'Patient.deceased',
                (new FHIRDateTime())->setValue('date'),
                'date',
            ],
            'Choice datatype2'  => [new FHIRPatient(), 'Patient.deceased', (new FHIRBoolean())->setValue(true), true],
            'Choice datatype3'  => [new FHIRObservation(), 'Observation.effective', $period, $period],
        ];
    }

    /**
     * @dataProvider providerFHIRPath
     * @throws Exception
     */
    public function testWriteOnEmpty(
        FHIRResourceInterface $resource,
        string                $expression,
        mixed                 $value,
        mixed                 $expected
    ): void {
        $fhir_path = new FHIRPath();

        $fhir_path->placeValue($resource, $expression, $value);

        $this->assertSame($expected, $fhir_path->apply($resource, $expression));
    }

    public function providerValueAfterChoice(): array
    {
        return [
            'Among tree'              => [
                new FHIRDosage(),
                'Dosage.doseAndRate.rate.denominator',
                3,
                'Dosage.doseAndRate.rate',
                FHIRRatio::class,
            ],
            'First'                   => [
                new FHIRDataRequirement(),
                'DataRequirement.subject.text',
                'text',
                'DataRequirement.subject',
                FHIRCodeableConcept::class,
            ],
            'Second'                  => [
                new FHIRDataRequirement(),
                'DataRequirement.subject.reference',
                'text',
                'DataRequirement.subject',
                FHIRReference::class,
            ],
            'complex Among primitive' => [
                new FHIRDosage(),
                'Dosage.asNeeded.text',
                'text',
                'Dosage.asNeeded',
                FHIRCodeableConcept::class,
            ],
        ];
    }

    /**
     * @dataProvider providerValueAfterChoice
     * @throws FHIRPathException
     */
    public function testDatatypeChoseAfterChoice(
        FHIRBaseInterface $resource,
        string            $expression,
        mixed             $value,
        string            $check_expression,
        string            $type_expected
    ): void {
        $fhir_path = new FHIRPath();

        $fhir_path->placeValue($resource, $expression, $value);

        $this->assertInstanceOf($type_expected, $fhir_path->apply($resource, $check_expression)[0]);
    }

    public function providerExceptionFHIRPathWrite(): array
    {
        return [
            'Choice'    => [
                new FHIRPatient(),
                'Patient.deceased',
                true,
                'You must set has value a FHIR datatype for choice elements.',
            ],
            'Operation' => [
                new FHIRPatient(),
                'Patient.active + 1',
                true,
                'Only path are allowed in a write expression.',
            ],
            'Function'  => [
                new FHIRPatient(),
                'Patient.active.allTrue()',
                true,
                'Only path are allowed in a write expression.',
            ],
            'Function2' => [
                new FHIRPatient(),
                "Patient.where(use = 'official').active",
                true,
                'Only path are allowed in a write expression.',
            ],
        ];
    }

    /**
     * @dataProvider providerExceptionFHIRPathWrite
     * @throws FHIRPathException
     */
    public function testWriteFails(FHIRBaseInterface $resource, string $expression, mixed $value, string $message): void
    {
        $fhir_path = new FHIRPath();

        $this->expectException(FHIRPathException::class);
        $this->expectExceptionMessage($message);

        $fhir_path->placeValue($resource, $expression, $value);
    }

    public function providerSubsetPrimitive(): array
    {
        return [
            ['Patient.name[0].given', 1],
            ['Patient.name[0].given[]', 3],
            ['Patient.name[0].given[0]', 2],
            ['Patient.name[0].given[2]', 3],
        ];
    }

    /**
     * @dataProvider providerSubsetPrimitive
     * @throws FHIRPathException
     */
    public function testSubsetsPrimitive(string $expression, int $count): void
    {
        $fhir_path = new FHIRPath();
        $patient   = (new FHIRPatient())
            ->setName((new FHIRHumanName())->setGiven('name1', 'name2'));

        $fhir_path->placeValue($patient, $expression, 'name3');

        $this->assertCount($count, $patient->getName()[0]->getGiven());
    }

    public function providerSubsetComplex(): array
    {
        return [
            ['Patient.identifier', 1],
            ['Patient.identifier[]', 3],
            ['Patient.identifier[0]', 2],
            ['Patient.identifier[2]', 3],
        ];
    }

    /**
     * @dataProvider providerSubsetComplex
     * @throws FHIRPathException
     */
    public function testSubsetsComplex(string $expression, int $count): void
    {
        $fhir_path  = new FHIRPath();
        $identifier = (new FHIRIdentifier())
            ->setValue(1);
        $patient    = (new FHIRPatient())
            ->setIdentifier($identifier, (new FHIRIdentifier())->setValue(2));

        $fhir_path->placeValue($patient, $expression, $identifier);

        $this->assertCount($count, $patient->getIdentifier());
    }

    /**
     * @throws FHIRPathException
     */
    public function testWriteOnRoot(): void
    {
        $fhir_path  = new FHIRPath();
        $patient    = (new FHIRPatient());
        $identifier = (new FHIRIdentifier())
            ->setValue(1);

        $fhir_path->placeValue($patient, 'identifier', $identifier);

        $this->assertCount(1, $patient->getIdentifier());
    }
}
