<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Tests\FHIRPath;

use Exception;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\BooleanLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Operator;
use Ox\Components\FHIRCore\FHIRPath\Expressions\VoidExpression;
use Ox\Components\FHIRCore\Utils\Comparisons;
use PHPUnit\Framework\TestCase;

final class FHIRPathOperatorsTest extends TestCase
{
    /**
     * @dataProvider providerEqualities
     * @throws Exception
     */
    public function testEqualities(mixed $left, mixed $right, mixed $expected): void
    {
        $helper = $this->createMock(Comparisons::class);
        $helper->expects($this->any())
               ->method('equal')
               ->willReturnCallback(fn($l, $r) => $l->getValue() === $r->getValue());

        $result = (new Operator('=', new VoidExpression(), new VoidExpression(), $helper))->equal($left, $right);
        $result = (count($result) == 0) ? [] : reset($result);
        $result = is_object($result) ? $result->getValue() : $result;

        $this->assertSame($expected, $result);
    }

    /**
     * @dataProvider providerEqualities
     * @throws Exception
     */
    public function testInequalities(mixed $left, mixed $right, mixed $expected): void
    {
        $helper = $this->createMock(Comparisons::class);
        $helper->expects($this->any())
               ->method('equal')
               ->willReturnCallback(fn($l, $r) => $l->getValue() === $r->getValue());

        $result = (new Operator('!=', new VoidExpression(), new VoidExpression(), $helper))->notEqual($left, $right);
        $result = is_object($result) ? $result->getValue() : $result;

        $this->assertSame(is_bool($expected) ? !$expected : $expected, $result);
    }

    public function providerEqualities(): array
    {
        $true  = (new BooleanLiteral())->setValue(true);
        $false = (new BooleanLiteral())->setValue(false);

        return [
            'both empty'        => [[], [], []],
            'right empty'       => [[$true], [], []],
            'left empty'        => [[], [$true], []],
            'same truthy'       => [[$true], [$true], true],
            'not same'          => [[$true], [$false], false],
            'same falsy'        => [[$false], [$false], true],
            'different sizes'   => [[$false, $true], [$false], false],
            'multiple same'     => [[$false, $true], [$false, $true], true],
            'order differences' => [[$false, $true], [$true, $false], false],
        ];
    }
}
