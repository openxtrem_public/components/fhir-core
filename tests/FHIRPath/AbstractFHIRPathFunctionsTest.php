<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Tests\FHIRPath;

use Exception;
use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRPathException;
use Ox\Components\FHIRCore\FHIRPath\Expressions\FunctionInvocation;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\BooleanLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Unprocessed;
use Ox\Components\FHIRCore\FHIRPath\Expressions\VoidExpression;
use Ox\Components\FHIRCore\FHIRPath\FHIRPath;
use Ox\Components\FHIRCore\Tests\Trait\MultiVersionsHelperTrait;
use Ox\Components\FHIRCore\Utils\Comparisons;
use PHPUnit\Framework\TestCase;

abstract class AbstractFHIRPathFunctionsTest extends TestCase
{
    use MultiVersionsHelperTrait;

    /**
     * @dataProvider providerFhirPathPatientsEmptyFields
     * @throws Exception
     */
    public function testEmptyFunction(string $fhirPath, bool $expected): void
    {
        $patient = $this->getResource('Patient')
                        ->setActive(false)
                        ->setName(
                            $this->getResource('HumanName')
                                 ->addGiven('Bernard', 'OK')
                                 ->setFamily('familyName'),
                            $this->getResource('HumanName')
                                 ->setSuffix('suffix')
                        );

        $values = (new FHIRPath())->apply($patient, $fhirPath);

        $this->assertSame($expected, reset($values)->getValue());
    }


    public function providerFhirPathPatientsEmptyFields(): array
    {
        return [
            ['Patient.name.empty()', false],
            ['Patient.name.given.empty()', false],
            ['Patient.birthdate.empty()', true],
            ['Patient.name.suffix.empty()', false],
        ];
    }

    /**
     * @throws FHIRPathException
     * @throws Exception
     */
    public function testExistsCallWhereWhenArguments(): void
    {
        $exists = $this->getMockBuilder(FunctionInvocation::class)
                       ->setConstructorArgs([new VoidExpression(), '', new Unprocessed('')])
                       ->onlyMethods(['not', 'empty', 'where'])
                       ->getMock();
        $exists->expects($this->once())
               ->method('where')
               ->willReturn([$this->getResource('HumanName')]);
        $exists->expects($this->once())
               ->method('not')
               ->willReturn((new BooleanLiteral())->setValue(true));

        $exists->exists([$this->getResource('HumanName')]);
    }

    /**
     * @throws FHIRPathException
     * @throws Exception
     */
    public function testExistsDoNotCallWhereWhenNoArguments(): void
    {
        $exists = $this->getMockBuilder(FunctionInvocation::class)
                       ->setConstructorArgs([new VoidExpression(), '', new VoidExpression()])
                       ->onlyMethods(['not', 'empty', 'where'])
                       ->getMock();
        $exists->expects($this->never())
               ->method('where')
               ->willReturn([$this->getResource('HumanName')]);
        $exists->expects($this->once())
               ->method('not')
               ->willReturn((new BooleanLiteral())->setValue(true));

        $exists->exists([$this->getResource('HumanName')]);
    }

    /**
     * @depends testExistsDoNotCallWhereWhenNoArguments
     * @return void
     * @throws FHIRPathException
     * @throws Exception
     */
    public function testExistsIsAEmptyNot(): void
    {
        $exists = $this->getMockBuilder(FunctionInvocation::class)
                       ->setConstructorArgs([new VoidExpression(), '', new VoidExpression()])
                       ->onlyMethods(['not', 'empty'])
                       ->getMock();
        $exists->expects($this->once())
               ->method('not')
               ->willReturn((new BooleanLiteral())->setValue(true));
        $exists->expects(($this->once()))
               ->method('empty');

        $exists->exists([$this->getResource('HumanName')]);
    }

    /**
     * @throws Exception
     */
    public function providerAll(): array
    {
        $true  = $this->getResource('boolean')->setValue(true);
        $false = $this->getResource('boolean')->setValue(false);

        return [
            [[$true, $true, $true], true],
            [[$true, $false, $true], false],
            [[$false, $false, $false], false],
            [[], true],
        ];
    }

    /**
     * @throws FHIRPathException
     * @throws Exception
     */
    public function testAllTrue(): void
    {
        $fnc = new FunctionInvocation(new VoidExpression(), 'all', new VoidExpression());

        $true  = $this->getResource('boolean')->setValue(true);
        $false = $this->getResource('boolean')->setValue(false);

        $this->assertTrue($fnc->allTrue([])->getValue());
        $this->assertTrue($fnc->allTrue([$true, $true])->getValue());
        $this->assertFalse($fnc->allTrue([$true, $false, $true])->getValue());
    }

    /**
     * @throws Exception
     */
    public function testAnyTrue(): void
    {
        $fnc = new FunctionInvocation(new VoidExpression(), 'all', new VoidExpression());

        $true  = $this->getResource('boolean')->setValue(true);
        $false = $this->getResource('boolean')->setValue(false);

        $this->assertFalse($fnc->anyTrue([])->getValue());
        $this->assertTrue($fnc->anyTrue([$true, $true])->getValue());
        $this->assertTrue($fnc->anyTrue([$true, $false, $true])->getValue());
        $this->assertFalse($fnc->anyTrue([$false, $false, $false])->getValue());
    }

    /**
     * @throws Exception
     */
    public function testAllFalse(): void
    {
        $fnc = new FunctionInvocation(new VoidExpression(), 'all', new VoidExpression());

        $true  = $this->getResource('boolean')->setValue(true);
        $false = $this->getResource('boolean')->setValue(false);

        $this->assertTrue($fnc->allFalse([])->getValue());
        $this->assertTrue($fnc->allFalse([$false, $false])->getValue());
        $this->assertFalse($fnc->allFalse([$true, $false, $true])->getValue());
    }

    /**
     * @throws Exception
     */
    public function testAnyFalse(): void
    {
        $fnc = new FunctionInvocation(new VoidExpression(), 'all', new VoidExpression());

        $true  = $this->getResource('boolean')->setValue(true);
        $false = $this->getResource('boolean')->setValue(false);

        $this->assertFalse($fnc->anyFalse([])->getValue());
        $this->assertFalse($fnc->anyFalse([$true, $true])->getValue());
        $this->assertTrue($fnc->anyFalse([$true, $false, $true])->getValue());
        $this->assertTrue($fnc->anyFalse([$false, $false, $false])->getValue());
    }

    /**
     * @throws Exception
     */
    public function providerCount(): array
    {
        $true  = $this->getResource('boolean')->setValue(true);
        $false = $this->getResource('boolean')->setValue(false);

        $hn = $this->getResource('HumanName')
                   ->setGiven('1', '2', '3');

        return [
            [[$true, $false, $true], 3],
            [[], 0],
            [[$false, $false, $hn], 3],
        ];
    }

    /**
     * @dataProvider providerCount
     *
     * @param array $input
     * @param int   $expected
     *
     * @return void
     */
    public function testCount(array $input, int $expected): void
    {
        $fnc    = new FunctionInvocation(new VoidExpression(), 'count', new VoidExpression());
        $result = $fnc->count($input);

        $this->assertSame($expected, $result->getValue());
    }

    public function testDistinctUseHelper(): void
    {
        $input = [1, 2, 2, 4];

        $helper = $this->createMock(Comparisons::class);
        $helper->expects($this->exactly(count($input)))
               ->method('includes')
               ->withAnyParameters()
               ->willReturnCallback(fn($n, $h) => in_array($n, $h));

        $fnc = new FunctionInvocation(new VoidExpression(), 'count', new VoidExpression(), $helper);

        $this->assertSameSize([1, 2, 4], $fnc->distinct($input));
        $this->assertSame([], $fnc->distinct([]));
    }

    /**
     * @depends testCount
     * @depends testDistinctUseHelper
     * @return void
     * @throws FHIRPathException
     */
    public function testIsDistinct(): void
    {
        $helper = $this->createMock(Comparisons::class);
        $helper->expects($this->any())
               ->method('includes')
               ->withAnyParameters()
               ->willReturnCallback(fn($n, $h) => in_array($n, $h));

        $fnc = new FunctionInvocation(new VoidExpression(), 'count', new VoidExpression(), $helper);

        $this->assertTrue($fnc->isDistinct([1, 2, 3])->getValue());
        $this->assertFalse($fnc->isDistinct([1, 2, 2, 3])->getValue());
        $this->assertFalse($fnc->isDistinct([0, 0, 2, 3])->getValue());
        $this->assertFalse($fnc->isDistinct([0, 0, 2, 2])->getValue());
        $this->assertTrue($fnc->isDistinct([])->getValue());
    }

    /**
     * @throws Exception
     */
    public function providerNotSingleBooleanCriteria(): array
    {
        return [
            [[true]],
            [[false]],
            [[self::getResource('integer')->setValue(0)]],
            [[self::getResource('integer')->setValue(true), self::getResource('boolean')->setValue(true)]],
        ];
    }

    /**
     * @throws Exception
     */
    public function providerToFilter(): array
    {
        $t = self::getResource('boolean')->setValue(true);
        $f = self::getResource('boolean')->setValue(false);

        return [
            [[], []],
            [[$t], [$t]],
            [[$t, $f], [$t]],
            [[$f], []],
        ];
    }

    /**
     * @throws Exception
     */
    public function testWhere(): void
    {
        $patient = self::getResource('Patient')
                       ->setActive(false)
                       ->setName(
                           self::getResource('HumanName')
                               ->addGiven('Bernard', 'OK')
                               ->setFamily('familyName'),
                           self::getResource('HumanName')
                               ->setSuffix('suffix')
                       );

        $values = (new FHIRPath())->apply($patient, "Patient.name.where(family = 'familyName')");

        $this->assertSame(1, count($values));
    }

    /**
     * @throws Exception
     */
    public function testOfTypeDatatype(): void
    {
        $patient = self::getResource('Patient')
                       ->setActive(false)
                       ->setDeceased(self::getResource('boolean')->setValue(false));

        $values = (new FHIRPath())->apply($patient, 'Patient.deceased.ofType(boolean)');

        $this->assertFalse(reset($values)->getValue());

        $values = (new FHIRPath())->apply($patient, 'Patient.deceased.ofType(dateTime)');

        $this->assertEmpty($values);
    }

    /**
     * @throws FHIRPathException
     */
    public function testSingleThrowErrorOnMultiple(): void
    {
        $fnc = new FunctionInvocation(new VoidExpression(), 'single', new VoidExpression());

        $this->expectExceptionMessage("Function 'single' can accept in input only a collection with a single element.");
        $fnc->single([1, 2]);
    }

    /**
     * @throws FHIRPathException
     */
    public function testSingleEmptyOnEmpty(): void
    {
        $fnc = new FunctionInvocation(new VoidExpression(), 'single', new VoidExpression());

        $this->assertEmpty($fnc->single([]));
    }

    /**
     * @throws FHIRPathException
     * @throws Exception
     */
    public function testSingle(): void
    {
        $fnc = new FunctionInvocation(new VoidExpression(), 'single', new VoidExpression());

        $values = $fnc->single([self::getResource('integer')->setValue(1)]);

        $this->assertSame(1, $values->getValue());
    }
}
