<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Tests\FHIRPath;

use Exception;
use Ox\Components\FHIRCore\FHIRPath\Expressions\FunctionInvocation;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literal;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Modifier;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Operator;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Path;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Subset;
use Ox\Components\FHIRCore\FHIRPath\PathParser;
use PHPUnit\Framework\TestCase;

final class FHIRPathParserTest extends TestCase
{
    public function providerFhirPathGroups(): array
    {
        return [
            ['Patient.given()', 2],
            ['(Patient.given())', 1],
            ['(Patient.given.count()) > (Patient.name.count())', 3],
            ['Patient.`something(`.given', 3],
        ];
    }

    /**
     * @dataProvider providerFhirPathGroups
     * @throws Exception
     */
    public function testExpressionsGrouped(string $fhirPath, int $count): void
    {
        $parser = new PathParser($fhirPath);
        $groups = $parser->getGroups();

        $this->assertCount($count, $groups);
    }

    public function providerFhirPathExpressionType(): array
    {
        return [
            'Simple Path'              => ['Patient.name.family', Path::class],
            'Function empty arguments' => ['Patient.name.given.count()', FunctionInvocation::class],
            'Function with argument'   => ['Patient.name.given.count(45)', FunctionInvocation::class],
            'Function with $this'      => [
                'name.given.where($this > \'ba\' and $this < \'bc\')',
                FunctionInvocation::class,
            ],
            'Operator >'               => ['Patient.name.given.count() > 3', Operator::class],
            'Operator > grouped'       => ['(Patient.name.given.count() > 3)', Operator::class],
            'Index'                    => ['Patient.name.given[2]', Subset::class],
            'Negative'                 => ['-7', Modifier::class],
            'Choice'                   => ['Patient.deceased as Boolean', Operator::class],
            'Literal string'           => ["'string'", Literal::class],
            'Literal number'           => ["7", Literal::class],
            'Literal Date'             => ["@2023-06-29", Literal::class],
            'Literal DateTime'         => ["@2023-06-29T09:32:23.15615+01:00", Literal::class],
            'Literal Time'             => ["@T09:32:23.156", Literal::class],
            'Literal true'             => ["true", Literal::class],
            'Literal false'            => ["false", Literal::class],
            'Literal decimal'          => ["3.14", Literal::class],
            'Literal empty'            => ["{ }", Literal::class],
            'Literal quantity'         => ["4.2 '[degF]'", Literal::class],
            'Identifier'               => ["`myIdentifier`", Path::class],
            'Path with Identifier'     => ["Patient.`myIdentifier`", Path::class],
            'Criteria'                 => ['generalPractitioner.all($this is Practitioner)', FunctionInvocation::class],
        ];
    }

    /**
     * @dataProvider providerFhirPathExpressionType
     * @throws Exception
     */
    public function testSameExpressions(string $fhirPath, string $expected): void
    {
        $path_parser       = new PathParser($fhirPath);
        $unprocessedGroups = $path_parser->getGroups();
        $expression        = $path_parser->parseUnprocessed($unprocessedGroups);

        $this->assertInstanceOf($expected, $expression);
    }

    public function providerLiteralTypes(): array
    {
        return [
            'string'        => ["'string'", Literal::STRING],
            'number'        => ["7", Literal::INTEGER],
            'Date'          => ["@2023-06-29", Literal::DATE],
            'DateTime'      => ["@2023-06-29T09:32:23.15615+01:00", Literal::DATETIME],
            'Time'          => ["@T09:32:23.156", Literal::TIME],
            'true'          => ["true", Literal::BOOLEAN],
            'false'         => ["false", Literal::BOOLEAN],
            'decimal'       => ["3.14", Literal::DECIMAL],
            'empty'         => ["{ }", Literal::EMPTY],
            'quantity int'  => ["4 '[degF]'", Literal::QUANTITY],
            'quantity dec'  => ["4.2 '[degF]'", Literal::QUANTITY],
            'quantity time' => ["1 year", Literal::QUANTITY],
        ];
    }

    /**
     * @dataProvider providerLiteralTypes
     * @throws Exception
     */
    public function testLiteralReturned(string $fhirPath, string $expected): void
    {
        $path_parser       = new PathParser($fhirPath);
        $unprocessedGroups = $path_parser->getGroups();
        $expression        = $path_parser->parseUnprocessed($unprocessedGroups);

        $this->assertInstanceOf(Literal::class, $expression);
        $this->assertSame($expected, $expression->type);
    }
}
