<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Tests\FHIRPath;

use Ox\Components\FHIRCore\Enum\FHIRVersion;

class UltimateR5Test extends AbstractUltimateTest
{
    final protected const FHIR_VERSION = FHIRVersion::R5;
}
