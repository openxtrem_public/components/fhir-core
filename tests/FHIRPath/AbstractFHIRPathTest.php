<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Tests\FHIRPath;

use Exception;
use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRPathException;
use Ox\Components\FHIRCore\FHIRPath\FHIRPath;
use Ox\Components\FHIRCore\Tests\Trait\MultiVersionsHelperTrait;
use PHPUnit\Framework\TestCase;

abstract class AbstractFHIRPathTest extends TestCase
{
    use MultiVersionsHelperTrait;

    /**
     * @throws FHIRPathException
     * @throws Exception
     */
    public function testPredicateThrowErrorOnMultiple(): void
    {
        $patient = self::getResource('Patient')->setName(
            self::getResource('HumanName')->setGiven('A', 'B', 'C', 'D', 'E', 'F', 'G')
        );
        $fp      = new FHIRPath();

        $this->expectExceptionMessage("Expected a single output from the expression, got a collection of 7.");

        $fp->predicate($patient, 'Patient.name.given');
    }

    /**
     * @throws FHIRPathException
     */
    public function testPredicateThrowErrorOnEmpty(): void
    {
        $patient = self::getResource('Patient');
        $fp      = new FHIRPath();

        $this->expectExceptionMessage("Expected a single output from the expression, got a collection of 0.");

        $fp->predicate($patient, 'Patient.name.given');
    }

    /**
     * @throws FHIRPathException
     * @throws Exception
     */
    public function testPredicateThrowErrorWrongType(): void
    {
        $patient = self::getResource('Patient')->setGender('gender');
        $fp      = new FHIRPath();

        $this->assertCount(1, $fp->apply($patient, 'Patient.gender'));

        $this->expectException(FHIRPathException::class);

        $fp->predicate($patient, 'Patient.gender');
    }

    public function providerPredicates(): array
    {
        return [
            "Existing field"    => ['Patient.name.given.exists()', true],
            "Un-Existing field" => ['Patient.gender.exists()', false],
            "Empty field"       => ['Patient.gender.empty()', true],
            "Count field"       => ['Patient.name.given.count() = 3', true],
            "Wrong count"       => ['Patient.name.given.count() = 2', false],
            "From Value"        => ['Patient.active', true],
        ];
    }

    /**
     * @dataProvider providerPredicates
     * @throws FHIRPathException
     * @throws Exception
     */
    public function testPredicate(string $fhirPath, bool $expected): void
    {
        $patient = self::getResource('Patient')
                       ->setName(
                           self::getResource('HumanName')->setGiven('A', 'B', 'C')
                       )
                       ->setActive(true);

        $fp = new FHIRPath();

        $result = $fp->predicate($patient, $fhirPath);

        $this->assertSame($expected, $result);
    }

    public function providerFhirPathPatientsPrimitives(): array
    {
        return [
            ['Patient.name.given', ['Bernard', 'OK', 'oui', 'non']],
            ['Patient.`name`.given', ['Bernard', 'OK', 'oui', 'non']],
            ['Patient.name.family', ['familyName']],
            ['Patient.active', [false]],
            ['Patient.id', []],
        ];
    }

    /**
     * @dataProvider providerFhirPathPatientsPrimitives
     * @throws Exception
     */
    public function testSamePathPrimitives(string $fhirPath, array $expected): void
    {
        $patient = self::getResource('Patient')
                       ->setActive(false)
                       ->setName(
                           self::getResource('HumanName')
                               ->addGiven('Bernard', 'OK')
                               ->setFamily('familyName'),
                           self::getResource('HumanName')
                               ->addGiven('oui', 'non')
                       );

        $values = (new FHIRPath())->apply($patient, $fhirPath);

        $this->assertSameSize($expected, $values);
    }


    /**
     * @throws Exception
     */
    public function testErrorOnInvalidPath(): void
    {
        // Should it ? Or empty is enough
        $this->markTestSkipped();
        $patient = self::getResource('Patient')
                       ->setActive(false)
                       ->setName(
                           self::getResource('HumanName')
                               ->addGiven('Bernard', 'OK')
                               ->setFamily('familyName')
                       );

        $this->expectException(FHIRPathException::class);

        (new FHIRPath())->apply($patient, 'Patient.invalidPath');
    }

    /**
     * @throws Exception
     */
    public function testErrorOnInvalidResourceType(): void
    {
        $this->markTestSkipped('Need to resolve everything to check if exists');
        $patient = self::getResource('Patient')
                       ->setActive(false)
                       ->setName(
                           self::getResource('HumanName')
                               ->addGiven('Bernard', 'OK')
                               ->setFamily('familyName')
                       );

        $this->expectException(FHIRPathException::class);

        (new FHIRPath())->apply($patient, 'NotAResource.name');
    }

    /**
     * @depends testErrorOnInvalidPath
     * @throws Exception
     */
    public function testErrorOnWrongFunctionInvocation(): void
    {
        $patient = self::getResource('Patient')
                       ->setName(
                           self::getResource('HumanName')
                               ->addGiven('Bernard', 'OK')
                       );

        $this->expectException(FHIRPathException::class);

        (new FHIRPath())->apply($patient, 'Patient.name.given.notAFunction()');
    }

    /**
     * @throws Exception
     */
    public function testParenthesisAfterPathActLikeWhereFunction(): void
    {
        $patient = self::getResource('Patient')
                       ->setName(
                           self::getResource('HumanName')
                               ->addGiven('Bernard', 'OK')
                               ->setFamily('family'),
                           self::getResource('HumanName')
                               ->addGiven('name', 'name2')
                               ->setFamily('notFamily'),
                       );

        $value = (new FHIRPath())->apply($patient, "Patient.name(family = 'family')");

        $this->assertCount(1, $value);
    }

    public function providerSubSettings(): array
    {
        return [
            ['Patient.name[0].family', 'family'],
            ['Patient.name[0].given[1]', 'OK'],
            ['Patient.name[1].family', 'notFamily'],
            ['Patient.name[1].given[0]', 'name'],
            ['Patient.name[0].given[0]', 'Bernard'],
        ];
    }

    /**
     * @dataProvider providerSubSettings
     * @throws Exception
     */
    public function testSubSetting(string $fhirPath, string $expected): void
    {
        $patient = self::getResource('Patient')
                       ->setName(
                           self::getResource('HumanName')
                               ->addGiven('Bernard', 'OK')
                               ->setFamily('family'),
                           self::getResource('HumanName')
                               ->addGiven('name', 'name2')
                               ->setFamily('notFamily'),
                       );

        $values = (new FHIRPath())->apply($patient, $fhirPath);

        $this->assertSame($expected, reset($values)->getValue());
    }

    /**
     * @throws Exception
     */
    public function testUnknownVariableThrowsError(): void
    {
        $patient = self::getResource('Patient');

        $this->expectExceptionMessage("The variable 'unknown' is not set.");

        (new FHIRPath())->apply($patient, 'Patient.%unknown');
    }

    public function providerDefaultVariables(): array
    {
        return [
            ['sct', 'http://snomed.info/sct'],
            ['loinc', 'http://loinc.org'],
            ['ucum', 'http://unitsofmeasure.org'],
        ];
    }

    /**
     * @dataProvider providerDefaultVariables
     * @throws FHIRPathException
     */
    public function testDefaultEnvironmentVariables(string $key, string $value): void
    {
        $fp     = new FHIRPath();
        $result = $fp->apply(self::getResource('Patient'), "%$key");

        $this->assertSame(reset($result)->getValue(), $value);
    }

    public function providerVariables(): array
    {
        return [
            ['test1', 'value1'],
            ['test-1', 'value1'],
        ];
    }

    /**
     * @dataProvider providerVariables
     * @throws FHIRPathException
     * @throws Exception
     */
    public function testEnvironmentVariables(string $key, string $value): void
    {
        $fp = new FHIRPath();
        $fp->addEnvironmentVariable($key, $value);
        $result = $fp->apply(self::getResource('Patient'), "%`$key`");

        $this->assertSame(reset($result)->getValue(), $value);
    }
}
