<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Tests\FHIRPath;

use Exception;
use Ox\Components\FHIRCore\FHIRPath\FHIRPath;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRQuantityInterface;
use Ox\Components\FHIRCore\Model\R4\Resources\FHIRPatient;
use Ox\Components\FHIRCore\Parser\Exception\NoElementException;
use Ox\Components\FHIRCore\Parser\Exception\ParserException;
use Ox\Components\FHIRCore\Parser\JSONParser;
use Ox\Components\FHIRCore\Parser\XMLParser;
use PHPUnit\Framework\TestCase;

abstract class AbstractUltimateTest extends TestCase
{
    public static array $resources = [];

    /**
     * @throws NoElementException
     * @throws ParserException
     */
    public static function setUpBeforeClass(): void
    {
        self::$resources = [
            'observation-example.xml'
            => (new XMLParser(static::FHIR_VERSION))
                ->parseResource(file_get_contents(__DIR__ . '/../resources/observation-example.xml')),
            'patient-example.xml'
            => (new XMLParser(static::FHIR_VERSION))
                ->parseResource(file_get_contents(__DIR__ . '/../resources/patient-example.xml')),
            'patient-example-period.xml'
            => (new XMLParser(static::FHIR_VERSION))
                ->parseResource(file_get_contents(__DIR__ . '/../resources/patient-example-period.xml')),
            'patient-container-example.json'
            => (new JSONParser(static::FHIR_VERSION))
                ->parseResource(file_get_contents(__DIR__ . '/../resources/patient-container-example.json')),
            'questionnaire-example.xml'
            => (new XMLParser(static::FHIR_VERSION))
                ->parseResource(file_get_contents(__DIR__ . '/../resources/questionnaire-example.xml')),
            'valueset-example-expansion.xml'
            => (new XMLParser(static::FHIR_VERSION))
                ->parseResource(file_get_contents(__DIR__ . '/../resources/valueset-example-expansion.xml')),
            'codesystem-example.xml'
            => (new XMLParser(static::FHIR_VERSION))
                ->parseResource(file_get_contents(__DIR__ . '/../resources/codesystem-example.xml')),
            'parameters-example-types.xml'
            => (new XMLParser(static::FHIR_VERSION))
                ->parseResource(file_get_contents(__DIR__ . '/../resources/parameters-example-types.xml')),
            'explanationofbenefit-example.json'
            => (new JSONParser(static::FHIR_VERSION))
                ->parseResource(file_get_contents(__DIR__ . '/../resources/explanationofbenefit-example.json')),
            'void' => new FHIRPatient()
        ];
    }

    public function providerExpressions(): array
    {
        return [
            'test2resources' => ['Patient.name.given | Practitioner.name.given', 'patient-example.xml', 'Peter'],
            'testExtractBirthDate' => ['birthDate', 'patient-example.xml', '1974-12-25'],
            'testPatientTelecomTypes' => ['telecom.use', 'patient-example.xml', 'home'],
            'testSimple' => ['name.given', 'patient-example.xml', 'Peter'],
            'testSimpleNone' => ['name.suffix', 'patient-example.xml', []],
            'testEscapedIdentifier' => ['name.`given`', 'patient-example.xml', 'Peter'],
            'testSimpleBackTick1' => ['`Patient`.name.`given`', 'patient-example.xml', 'Peter'],
            'testSimpleWithContext' => ['Patient.name.given', 'patient-example.xml', 'Peter'],
            'testPolymorphismA' => ['Observation.value.unit', 'observation-example.xml', 'lbs'],
            'testPolymorphismIsA1' => ['Observation.value.is(Quantity)', 'observation-example.xml', true],
            'testPolymorphismIsA2' => ['Observation.value is Quantity', 'observation-example.xml', true],
            'testPolymorphismIsA3' => ['Observation.issued is instant', 'observation-example.xml', []],
            'testPolymorphismIsB' => ['Observation.value.is(Period).not()', 'observation-example.xml', true],
            'testPolymorphismAsA' => ['Observation.value.as(Quantity).unit', 'observation-example.xml', 'lbs'],
            'testPolymorphismAsAFunction' => ['(Observation.value as Quantity).unit', 'observation-example.xml', 'lbs'],
            'testPolymorphismAsBFunction' => ['Observation.value.as(Period).start', 'observation-example.xml', []],
            'testDollarThis1' => [
                'Patient.name.given.where(substring($this.length()-3) = \'out\')',
                'patient-example.xml',
                []
            ],
            'testDollarThis2' => [
                'Patient.name.given.where(substring($this.length()-3) = \'ter\')',
                'patient-example.xml',
                'Peter',
            ],
            'testDollarOrderAllowed' => ['Patient.name.skip(1).given', 'patient-example.xml', 'Jim'],
            'testDollarOrderAllowedA' => ['Patient.name.skip(3).given', 'patient-example.xml', []],
            'testLiteralTrue' => ['Patient.name.exists() = true', 'patient-example.xml', true],
            'testLiteralFalse' => ['Patient.name.empty() = false', 'patient-example.xml', true],
            'testLiteralString1' => ['Patient.name.given.first() = \'Peter\'', 'patient-example.xml', true],
            'testLiteralInteger1' => ['1.convertsToInteger()', 'patient-example.xml', true],
            'testLiteralInteger0' => ['0.convertsToInteger()', 'patient-example.xml', true],
            'testLiteralIntegerNegative1' => ['(-1).convertsToInteger()', 'patient-example.xml', true],
            'testLiteralIntegerMax' => ['2147483647.convertsToInteger()', 'patient-example.xml', true],
            'testLiteralString2' => ['\'test\'.convertsToString()', 'patient-example.xml', true],
            'testLiteralStringEscapes' => ['\'\\\\\/\f\r\n\t\"\`\\\'\u002a\'.convertsToString()', 'patient-example.xml', true],
            'testLiteralBooleanTrue' => ['true.convertsToBoolean()', 'patient-example.xml', true],
            'testLiteralBooleanFalse' => ['false.convertsToBoolean()', 'patient-example.xml', true],
            'testLiteralDecimal10' => ['1.0.convertsToDecimal()', 'patient-example.xml', true],
            'testLiteralDecimal01' => ['0.1.convertsToDecimal()', 'patient-example.xml', true],
            'testLiteralDecimal00' => ['0.0.convertsToDecimal()', 'patient-example.xml', true],
            'testLiteralDecimalNegative01' => ['(-0.1).convertsToDecimal()', 'patient-example.xml', true],
            'testLiteralDecimalMax' => ['1234567890987654321.0.convertsToDecimal()', 'patient-example.xml', true],
            'testLiteralDecimalStep' => ['0.00000001.convertsToDecimal()', 'patient-example.xml', true],
            'testLiteralDateYear' => ['@2015.is(Date)', 'patient-example.xml', true],
            'testLiteralDateMonth' => ['@2015-02.is(Date)', 'patient-example.xml', true],
            'testLiteralDateDay' => ['@2015-02-04.is(Date)', 'patient-example.xml', true],
            'testLiteralDateTimeYear' => ['@2015T.is(DateTime)', 'patient-example.xml', true],
            'testLiteralDateTimeMonth' => ['@2015-02T.is(DateTime)', 'patient-example.xml', true],
            'testLiteralDateTimeDay' => ['@2015-02-04T.is(DateTime)', 'patient-example.xml', true],
            'testLiteralDateTimeHour' => ['@2015-02-04T14.is(DateTime)', 'patient-example.xml', true],
            'testLiteralDateTimeMinute' => ['@2015-02-04T14:34.is(DateTime)', 'patient-example.xml', true],
            'testLiteralDateTimeSecond' => ['@2015-02-04T14:34:28.is(DateTime)', 'patient-example.xml', true],
            'testLiteralDateTimeMillisecond' => ['@2015-02-04T14:34:28.123.is(DateTime)', 'patient-example.xml', true],
            'testLiteralDateTimeUTC' => ['@2015-02-04T14:34:28Z.is(DateTime)', 'patient-example.xml', true],
            'testLiteralDateTimeTimezoneOffset' => ['@2015-02-04T14:34:28+10:00.is(DateTime)', 'patient-example.xml', true],
            'testLiteralTimeHour' => ['@T14.is(Time)', 'patient-example.xml', true],
            'testLiteralTimeMinute' => ['@T14:34.is(Time)', 'patient-example.xml', true],
            'testLiteralTimeSecond' => ['@T14:34:28.is(Time)', 'patient-example.xml', true],
            'testLiteralTimeMillisecond' => ['@T14:34:28.123.is(Time)', 'patient-example.xml', true],
            'testLiteralQuantityDecimal' => ['10.1 \'mg\'.convertsToQuantity()', 'patient-example.xml', true],
            'testLiteralQuantityInteger' => ['10 \'mg\'.convertsToQuantity()', 'patient-example.xml', true],
            'testLiteralQuantityDay' => ['4 days.convertsToQuantity()', 'patient-example.xml', true],
            'testLiteralIntegerNotEqual' => ['-3 != 3', 'patient-example.xml', true],
            'testLiteralIntegerEqual' => ['Patient.name.given.count() = 5', 'patient-example.xml', true],
            'testPolarityPrecedence' => ['-Patient.name.given.count() = -5', 'patient-example.xml', true],
            'testLiteralIntegerGreaterThan' => ['Patient.name.given.count() > -3', 'patient-example.xml', true],
            'testLiteralIntegerCountNotEqual' => ['Patient.name.given.count() != 0', 'patient-example.xml', true],
            'testLiteralIntegerLessThanTrue' => ['1 < 2', 'patient-example.xml', true],
            'testLiteralIntegerLessThanFalse' => ['1 < -2', 'patient-example.xml', false],
            'testLiteralIntegerLessThanPolarityTrue' => ['+1 < +2', 'patient-example.xml', true],
            'testLiteralIntegerLessThanPolarityFalse' => ['-1 < 2', 'patient-example.xml', true],
            'testLiteralDecimalGreaterThanNonZeroTrue' => ['Observation.value.value > 180.0', 'observation-example.xml', true],
            'testLiteralDecimalGreaterThanZeroTrue' => ['Observation.value.value > 0.0', 'observation-example.xml', true],
            'testLiteralDecimalGreaterThanIntegerTrue' => ['Observation.value.value > 0', 'observation-example.xml', true],
            'testLiteralDecimalLessThanInteger' => ['Observation.value.value < 190', 'observation-example.xml', true],
            'testDateEqual' => ['Patient.birthDate = @1974-12-25', 'patient-example.xml', true],
            'testDateTimeGreaterThanDate2' => ['now() > today()', 'patient-example.xml', []],
            'testLiteralDateTimeTZGreater' => [
                '@2017-11-05T01:30:00.0-04:00 > @2017-11-05T01:15:00.0-05:00',
                'patient-example.xml',
                false,
            ],
            'testLiteralDateTimeTZLess' => [
                '@2017-11-05T01:30:00.0-04:00 < @2017-11-05T01:15:00.0-05:00',
                'patient-example.xml',
                true,
            ],
            'testLiteralDateTimeTZEqualFalse' => [
                '@2017-11-05T01:30:00.0-04:00 = @2017-11-05T01:15:00.0-05:00',
                'patient-example.xml',
                false,
            ],
            'testLiteralDateTimeTZEqualTrue' => [
                '@2017-11-05T01:30:00.0-04:00 = @2017-11-05T00:30:00.0-05:00',
                'patient-example.xml',
                true,
            ],
            'testLiteralUnicode' => ['Patient.name.given.first() = \'P\u0065ter\'', 'patient-example.xml', true],
            'testCollectionNotEmpty' => ['Patient.name.given.empty().not()', 'patient-example.xml', true],
            'testCollectionNotEqualEmpty' => ['Patient.name.given != {}', 'patient-example.xml', []],
            'testExpressions' => ['Patient.name.select(given | family).distinct()', 'patient-example.xml', 'Peter'],
            'testExpressionsEqual' => ['Patient.name.given.count() = 1 + 4', 'patient-example.xml', true],
            'testNotEmpty' => ['Patient.name.empty().not()', 'patient-example.xml', true],
            'testEmpty' => ['Patient.link.empty()', 'patient-example.xml', true],
            'testLiteralNotOnEmpty' => ['{}.not().empty()', 'patient-example.xml', true],
            'testLiteralNotTrue' => ['true.not() = false', 'patient-example.xml', true],
            'testLiteralNotFalse' => ['false.not() = true', 'patient-example.xml', true],
            'testStringYearConvertsToDate' => ['\'2015\'.convertsToDate()', 'patient-example.xml', true],
            'testStringMonthConvertsToDate' => ['\'2015-02\'.convertsToDate()', 'patient-example.xml', true],
            'testStringDayConvertsToDate' => ['\'2015-02-04\'.convertsToDate()', 'patient-example.xml', true],
            'testStringYearConvertsToDateTime' => ['\'2015\'.convertsToDateTime()', 'patient-example.xml', true],
            'testStringMonthConvertsToDateTime' => ['\'2015-02\'.convertsToDateTime()', 'patient-example.xml', true],
            'testStringDayConvertsToDateTime' => ['\'2015-02-04\'.convertsToDateTime()', 'patient-example.xml', true],
            'testStringHourConvertsToDateTime' => ['\'2015-02-04T14\'.convertsToDateTime()', 'patient-example.xml', true],
            'testStringMinuteConvertsToDateTime' => ['\'2015-02-04T14:34\'.convertsToDateTime()', 'patient-example.xml', true],
            'testStringSecondConvertsToDateTime' => [
                '\'2015-02-04T14:34:28\'.convertsToDateTime()',
                'patient-example.xml',
                true,
            ],
            'testStringMillisecondConvertsToDateTime' => [
                '\'2015-02-04T14:34:28.123\'.convertsToDateTime()',
                'patient-example.xml',
                true,
            ],
            'testStringUTCConvertsToDateTime' => ['\'2015-02-04T14:34:28Z\'.convertsToDateTime()', 'patient-example.xml', true],
            'testStringTZConvertsToDateTime' => [
                '\'2015-02-04T14:34:28+10:00\'.convertsToDateTime()',
                'patient-example.xml',
                true,
            ],
            'testStringHourConvertsToTime' => ['\'14\'.convertsToTime()', 'patient-example.xml', true],
            'testStringMinuteConvertsToTime' => ['\'14:34\'.convertsToTime()', 'patient-example.xml', true],
            'testStringSecondConvertsToTime' => ['\'14:34:28\'.convertsToTime()', 'patient-example.xml', true],
            'testStringMillisecondConvertsToTime' => ['\'14:34:28.123\'.convertsToTime()', 'patient-example.xml', true],
            'testIntegerLiteralConvertsToInteger' => ['1.convertsToInteger()', 'patient-example.xml', true],
            'testIntegerLiteralIsInteger' => ['1.is(Integer)', 'patient-example.xml', true],
            'testIntegerLiteralIsSystemInteger' => ['1.is(System.Integer)', 'patient-example.xml', true],
            'testStringLiteralConvertsToInteger' => ['\'1\'.convertsToInteger()', 'patient-example.xml', true],
            'testStringLiteralConvertsToIntegerFalse' => ['\'a\'.convertsToInteger().not()', 'patient-example.xml', true],
            'testStringDecimalConvertsToIntegerFalse' => ['\'1.0\'.convertsToInteger().not()', 'patient-example.xml', true],
            'testStringLiteralIsNotInteger' => ['\'1\'.is(Integer).not()', 'patient-example.xml', true],
            'testBooleanLiteralConvertsToInteger' => ['true.convertsToInteger()', 'patient-example.xml', true],
            'testBooleanLiteralIsNotInteger' => ['true.is(Integer).not()', 'patient-example.xml', true],
            'testDateIsNotInteger' => ['@2013-04-05.is(Integer).not()', 'patient-example.xml', true],
            'testIntegerLiteralToInteger' => ['1.toInteger() = 1', 'patient-example.xml', true],
            'testStringIntegerLiteralToInteger' => ['\'1\'.toInteger() = 1', 'patient-example.xml', true],
            'testDecimalLiteralToInteger' => ['\'1.1\'.toInteger() = {}', 'patient-example.xml', []],
            'testDecimalLiteralToIntegerIsEmpty' => ['\'1.1\'.toInteger().empty()', 'patient-example.xml', true],
            'testBooleanLiteralToInteger' => ['true.toInteger() = 1', 'patient-example.xml', true],
            'testIntegerLiteralConvertsToDecimal' => ['1.convertsToDecimal()', 'patient-example.xml', true],
            'testIntegerLiteralIsNotDecimal' => ['1.is(Decimal).not()', 'patient-example.xml', true],
            'testDecimalLiteralConvertsToDecimal' => ['1.0.convertsToDecimal()', 'patient-example.xml', true],
            'testDecimalLiteralIsDecimal' => ['1.0.is(Decimal)', 'patient-example.xml', true],
            'testStringIntegerLiteralConvertsToDecimal' => ['\'1\'.convertsToDecimal()', 'patient-example.xml', true],
            'testStringIntegerLiteralIsNotDecimal' => ['\'1\'.is(Decimal).not()', 'patient-example.xml', true],
            'testStringLiteralConvertsToDecimalFalse' => ['\'1.a\'.convertsToDecimal().not()', 'patient-example.xml', true],
            'testStringDecimalLiteralConvertsToDecimal' => ['\'1.0\'.convertsToDecimal()', 'patient-example.xml', true],
            'testStringDecimalLiteralIsNotDecimal' => ['\'1.0\'.is(Decimal).not()', 'patient-example.xml', true],
            'testBooleanLiteralConvertsToDecimal' => ['true.convertsToDecimal()', 'patient-example.xml', true],
            'testBooleanLiteralIsNotDecimal' => ['true.is(Decimal).not()', 'patient-example.xml', true],
            'testIntegerLiteralToDecimal' => ['1.toDecimal() = 1.0', 'patient-example.xml', true],
            'testIntegerLiteralToDeciamlEquivalent' => ['1.toDecimal() ~ 1.0', 'patient-example.xml', true],
            'testDecimalLiteralToDecimal' => ['1.0.toDecimal() = 1.0', 'patient-example.xml', true],
            'testDecimalLiteralToDecimalEqual' => ['\'1.1\'.toDecimal() = 1.1', 'patient-example.xml', true],
            'testBooleanLiteralToDecimal' => ['true.toDecimal() = 1', 'patient-example.xml', true],
            'testIntegerLiteralConvertsToQuantity' => ['1.convertsToQuantity()', 'patient-example.xml', true],
            'testIntegerLiteralIsNotQuantity' => ['1.is(Quantity).not()', 'patient-example.xml', true],
            'testDecimalLiteralConvertsToQuantity' => ['1.0.convertsToQuantity()', 'patient-example.xml', true],
            'testDecimalLiteralIsNotQuantity' => ['1.0.is(System.Quantity).not()', 'patient-example.xml', true],
            'testStringIntegerLiteralConvertsToQuantity' => ['\'1\'.convertsToQuantity()', 'patient-example.xml', true],
            'testStringIntegerLiteralIsNotQuantity' => ['\'1\'.is(System.Quantity).not()', 'patient-example.xml', true],
            'testStringQuantityLiteralConvertsToQuantity' => ['\'1 day\'.convertsToQuantity()', 'patient-example.xml', true],
            'testStringQuantityWeekConvertsToQuantity' => [
                '\'1 \\\'wk\\\'\'.convertsToQuantity()',
                'patient-example.xml',
                true,
            ],
            'testStringQuantityWeekConvertsToQuantityFalse' => [
                '\'1 wk\'.convertsToQuantity().not()',
                'patient-example.xml',
                true,
            ],
            'testStringDecimalLiteralConvertsToQuantityFalse' => [
                '\'1.a\'.convertsToQuantity().not()',
                'patient-example.xml',
                true,
            ],
            'testStringDecimalLiteralConvertsToQuantity' => ['\'1.0\'.convertsToQuantity()', 'patient-example.xml', true],
            'testStringDecimalLiteralIsNotSystemQuantity' => ['\'1.0\'.is(System.Quantity).not()', 'patient-example.xml', true],
            'testBooleanLiteralConvertsToQuantity' => ['true.convertsToQuantity()', 'patient-example.xml', true],
            'testBooleanLiteralIsNotSystemQuantity' => ['true.is(System.Quantity).not()', 'patient-example.xml', true],
            'testIntegerLiteralToQuantity' => ['1.toQuantity() = 1 \'1\'', 'patient-example.xml', true],
            'testDecimalLiteralToQuantity' => ['1.0.toQuantity() = 1.0 \'1\'', 'patient-example.xml', true],
            'testStringIntegerLiteralToQuantity' => ['\'1\'.toQuantity()', 'patient-example.xml', '#Q1 \'1\''],
            'testStringQuantityLiteralToQuantity' => ['\'1 day\'.toQuantity() = 1 day', 'patient-example.xml', true],
            'testIntegerLiteralConvertsToBooleanFalse' => ['2.convertsToBoolean()', 'patient-example.xml', false],
            'testNegativeIntegerLiteralConvertsToBooleanFalse' => ['(-1).convertsToBoolean()', 'patient-example.xml', false],
            'testIntegerLiteralFalseConvertsToBoolean' => ['0.convertsToBoolean()', 'patient-example.xml', true],
            'testDecimalLiteralConvertsToBoolean' => ['1.0.convertsToBoolean()', 'patient-example.xml', true],
            'testStringTrueLiteralConvertsToBoolean' => ['\'true\'.convertsToBoolean()', 'patient-example.xml', true],
            'testStringFalseLiteralConvertsToBoolean' => ['\'false\'.convertsToBoolean()', 'patient-example.xml', true],
            'testStringFalseLiteralAlsoConvertsToBoolean' => ['\'False\'.convertsToBoolean()', 'patient-example.xml', true],
            'testTrueLiteralConvertsToBoolean' => ['true.convertsToBoolean()', 'patient-example.xml', true],
            'testFalseLiteralConvertsToBoolean' => ['false.convertsToBoolean()', 'patient-example.xml', true],
            'testIntegerLiteralToBoolean' => ['1.toBoolean()', 'patient-example.xml', true],
            'testIntegerLiteralToBooleanEmpty' => ['2.toBoolean()', 'patient-example.xml', []],
            'testIntegerLiteralToBooleanFalse' => ['0.toBoolean()', 'patient-example.xml', false],
            'testStringTrueToBoolean' => ['\'true\'.toBoolean()', 'patient-example.xml', true],
            'testStringFalseToBoolean' => ['\'false\'.toBoolean()', 'patient-example.xml', false],
            'testIntegerLiteralConvertsToString' => ['1.convertsToString()', 'patient-example.xml', true],
            'testIntegerLiteralIsNotString' => ['1.is(String).not()', 'patient-example.xml', true],
            'testNegativeIntegerLiteralConvertsToString' => ['(-1).convertsToString()', 'patient-example.xml', true],
            'testDecimalLiteralConvertsToString' => ['1.0.convertsToString()', 'patient-example.xml', true],
            'testStringLiteralConvertsToString' => ['\'true\'.convertsToString()', 'patient-example.xml', true],
            'testBooleanLiteralConvertsToString' => ['true.convertsToString()', 'patient-example.xml', true],
            'testQuantityLiteralConvertsToString' => ['1 \'wk\'.convertsToString()', 'patient-example.xml', true],
            'testIntegerLiteralToString' => ['1.toString()', 'patient-example.xml', '1'],
            'testNegativeIntegerLiteralToString' => ['(-1).toString()', 'patient-example.xml', '-1'],
            'testDecimalLiteralToString' => ['1.0.toString()', 'patient-example.xml', '1.0'],
            'testStringLiteralToString' => ['\'true\'.toString()', 'patient-example.xml', 'true'],
            'testBooleanLiteralToString' => ['true.toString()', 'patient-example.xml', 'true'],
            'testQuantityLiteralWkToString' => ['1 \'wk\'.toString()', 'patient-example.xml', '1 \'wk\''],
            'testExists1' => ['Patient.name.exists()', 'patient-example.xml', true],
            'testExists2' => ['Patient.name.exists(use = \'nickname\')', 'patient-example.xml', false],
            'testExists3' => ['Patient.name.exists(use = \'official\')', 'patient-example.xml', true],
            'testExists4' => [
                "Patient.maritalStatus.coding.exists(code = 'P' and system = 'http://terminology.hl7.org/CodeSystem/v3-MaritalStatus')\n      or Patient.maritalStatus.coding.exists(code = 'A' and system = 'http://terminology.hl7.org/CodeSystem/v3-MaritalStatus')",
                'patient-example.xml',
                false,
            ],
            'testAllTrue1' => ['Patient.name.select(given.exists()).allTrue()', 'patient-example.xml', true],
            'testAllTrue2' => ['Patient.name.select(period.exists()).allTrue()', 'patient-example.xml', false],
            'testAllTrue3' => ['Patient.name.all(given.exists())', 'patient-example.xml', true],
            'testAllTrue4' => ['Patient.name.all(period.exists())', 'patient-example.xml', false],
            'testCollectionBoolean2' => ['iif({}, true, false)', 'patient-example.xml', false],
            'testCollectionBoolean3' => ['iif(true, true, false)', 'patient-example.xml', true],
            'testCollectionBoolean4' => ['iif({} | true, true, false)', 'patient-example.xml', true],
            'testCollectionBoolean5' => ['iif(true, true, 1/0)', 'patient-example.xml', true],
            'testCollectionBoolean6' => ['iif(false, 1/0, true)', 'patient-example.xml', true],
            'testDistinct1' => ['(1 | 2 | 3).isDistinct()', 'patient-example.xml', true],
            'testDistinct2' => ['Questionnaire.descendants().linkId.isDistinct()', 'questionnaire-example.xml', true],
            'testDistinct3' => [
                'Questionnaire.descendants().linkId.select(substring(0,1)).isDistinct().not()',
                'questionnaire-example.xml',
                true,
            ],
            'testDistinct4' => ['(1 | 2 | 3).distinct()', 'patient-example.xml', 1],
            'testDistinct5' => ['Questionnaire.descendants().linkId.distinct().count()', 'questionnaire-example.xml', 10],
            'testDistinct6' => [
                'Questionnaire.descendants().linkId.select(substring(0,1)).distinct().count()',
                'questionnaire-example.xml',
                2,
            ],
            'testCount1' => ['Patient.name.count()', 'patient-example.xml', 3],
            'testCount2' => ['Patient.name.count() = 3', 'patient-example.xml', true],
            'testCount3' => ['Patient.name.first().count()', 'patient-example.xml', 1],
            'testCount4' => ['Patient.name.first().count() = 1', 'patient-example.xml', true],
            'testWhere1' => ['Patient.name.count() = 3', 'patient-example.xml', true],
            'testWhere2' => ['Patient.name.where(given = \'Jim\').count() = 1', 'patient-example.xml', true],
            'testWhere3' => ['Patient.name.where(given = \'X\').count() = 0', 'patient-example.xml', true],
            'testWhere4' => ['Patient.name.where($this.given = \'Jim\').count() = 1', 'patient-example.xml', true],
            'testSelect1' => ['Patient.name.select(given).count() = 5', 'patient-example.xml', true],
            'testSelect2' => ['Patient.name.select(given | family).count() = 7 ', 'patient-example.xml', true],
            'testSelect3' => ['name.select(use.contains(\'i\')).count()', 'patient-example.xml', 3],
            'testRepeat1' => ['ValueSet.expansion.repeat(contains).count() = 10', 'valueset-example-expansion.xml', true],
            'testRepeat2' => ['Questionnaire.repeat(item).code.count() = 11', 'questionnaire-example.xml', true],
            'testRepeat3' => ['Questionnaire.descendants().code.count() = 23', 'questionnaire-example.xml', true],
            'testRepeat4' => ['Questionnaire.children().code.count() = 2', 'questionnaire-example.xml', true],
            'testRepeat5' => ['Patient.name.repeat(\'test\')', 'patient-example.xml', 'test'],
            'testAggregate1' => ['(1|2|3|4|5|6|7|8|9).aggregate($this+$total, 0) = 45', 'patient-example.xml', true],
            'testAggregate2' => ['(1|2|3|4|5|6|7|8|9).aggregate($this+$total, 2) = 47', 'patient-example.xml', true],
            'testAggregate3' => [
                '(1|2|3|4|5|6|7|8|9).aggregate(iif($total.empty(), $this, iif($this < $total, $this, $total))) = 1',
                'patient-example.xml',
                true,
            ],
            'testAggregate4' => [
                '(1|2|3|4|5|6|7|8|9).aggregate(iif($total.empty(), $this, iif($this > $total, $this, $total))) = 9',
                'patient-example.xml',
                true,
            ],
            'testIndexer1' => ['Patient.name[0].given = \'Peter\' | \'James\'', 'patient-example.xml', true],
            'testIndexer2' => ['Patient.name[1].given = \'Jim\'', 'patient-example.xml', true],
            'testSingle1' => ['Patient.name.first().single().exists()', 'patient-example.xml', true],
            'testFirstLast1' => ['Patient.name.first().given = \'Peter\' | \'James\'', 'patient-example.xml', true],
            'testFirstLast2' => ['Patient.name.last().given = \'Peter\' | \'James\'', 'patient-example.xml', true],
            'testTail1' => ['(0 | 1 | 2).tail() = 1 | 2', 'patient-example.xml', true],
            'testTail2' => ['Patient.name.tail().given = \'Jim\' | \'Peter\' | \'James\'', 'patient-example.xml', true],
            'testSkip1' => ['(0 | 1 | 2).skip(1) = 1 | 2', 'patient-example.xml', true],
            'testSkip2' => ['(0 | 1 | 2).skip(2) = 2', 'patient-example.xml', true],
            'testSkip3' => [
                'Patient.name.skip(1).given.trace(\'test\') = \'Jim\' | \'Peter\' | \'James\'',
                'patient-example.xml',
                true,
            ],
            'testSkip4' => ['Patient.name.skip(3).given.exists() = false', 'patient-example.xml', true],
            'testTake1' => ['(0 | 1 | 2).take(1) = 0', 'patient-example.xml', true],
            'testTake2' => ['(0 | 1 | 2).take(2) = 0 | 1', 'patient-example.xml', true],
            'testTake3' => ['Patient.name.take(1).given = \'Peter\' | \'James\'', 'patient-example.xml', true],
            'testTake4' => ['Patient.name.take(2).given = \'Peter\' | \'James\' | \'Jim\'', 'patient-example.xml', true],
            'testTake5' => ['Patient.name.take(3).given.count() = 5', 'patient-example.xml', true],
            'testTake6' => ['Patient.name.take(4).given.count() = 5', 'patient-example.xml', true],
            'testTake7' => ['Patient.name.take(0).given.exists() = false', 'patient-example.xml', true],
            'testIif1' => ['iif(Patient.name.exists(), \'named\', \'unnamed\') = \'named\'', 'patient-example.xml', true],
            'testIif2' => ['iif(Patient.name.empty(), \'unnamed\', \'named\') = \'named\'', 'patient-example.xml', true],
            'testIif3' => ['iif(true, true, (1 | 2).toString())', 'patient-example.xml', true],
            'testIif4' => ['iif(false, (1 | 2).toString(), true)', 'patient-example.xml', true],
            'testToInteger1' => ['\'1\'.toInteger() = 1', 'patient-example.xml', true],
            'testToInteger2' => ['\'-1\'.toInteger() = -1', 'patient-example.xml', true],
            'testToInteger3' => ['\'0\'.toInteger() = 0', 'patient-example.xml', true],
            'testToInteger4' => ['\'0.0\'.toInteger().empty()', 'patient-example.xml', true],
            'testToInteger5' => ['\'st\'.toInteger().empty()', 'patient-example.xml', true],
            'testToDecimal1' => ['\'1\'.toDecimal() = 1', 'patient-example.xml', true],
            'testToDecimal2' => ['\'-1\'.toInteger() = -1', 'patient-example.xml', true],
            'testToDecimal3' => ['\'0\'.toDecimal() = 0', 'patient-example.xml', true],
            'testToDecimal4' => ['\'0.0\'.toDecimal() = 0.0', 'patient-example.xml', true],
            'testToDecimal5' => ['\'st\'.toDecimal().empty()', 'patient-example.xml', true],
            'testToString1' => ['1.toString() = \'1\'', 'patient-example.xml', true],
            'testToString2' => ['\'-1\'.toInteger() = -1', 'patient-example.xml', true],
            'testToString3' => ['0.toString() = \'0\'', 'patient-example.xml', true],
            'testToString4' => ['0.0.toString() = \'0.0\'', 'patient-example.xml', true],
            'testToString5' => ['@2014-12-14.toString() = \'2014-12-14\'', 'patient-example.xml', true],
            'testCase1' => ['\'t\'.upper() = \'T\'', 'patient-example.xml', true],
            'testCase2' => ['\'t\'.lower() = \'t\'', 'patient-example.xml', true],
            'testCase3' => ['\'T\'.upper() = \'T\'', 'patient-example.xml', true],
            'testCase4' => ['\'T\'.lower() = \'t\'', 'patient-example.xml', true],
            'testToChars1' => ['\'t2\'.toChars() = \'t\' | \'2\'', 'patient-example.xml', true],
            'testIndexOf1' => ['\'LogicalModel-Person\'.indexOf(\'-\')', 'patient-example.xml', 12],
            'testIndexOf2' => ['\'LogicalModel-Person\'.indexOf(\'z\')', 'patient-example.xml', -1],
            'testIndexOf3' => ['\'LogicalModel-Person\'.indexOf(\'\')', 'patient-example.xml', 0],
            'testIndexOf5' => ['\'LogicalModel-Person\'.indexOf({}).empty() = true', 'patient-example.xml', true],
            'testIndexOf4' => ['{}.indexOf(\'-\').empty() = true', 'patient-example.xml', true],
            'testIndexOf6' => ['{}.indexOf({}).empty() = true', 'patient-example.xml', true],
            'testSubstring1' => ['\'12345\'.substring(2) = \'345\'', 'patient-example.xml', true],
            'testSubstring2' => ['\'12345\'.substring(2,1) = \'3\'', 'patient-example.xml', true],
            'testSubstring3' => ['\'12345\'.substring(2,5) = \'345\'', 'patient-example.xml', true],
            'testSubstring4' => ['\'12345\'.substring(25).empty()', 'patient-example.xml', true],
            'testSubstring5' => ['\'12345\'.substring(-1).empty()', 'patient-example.xml', true],
            'testSubstring7' => ['\'LogicalModel-Person\'.substring(0, 12)', 'patient-example.xml', 'LogicalModel'],
            'testSubstring8' => [
                '\'LogicalModel-Person\'.substring(0, \'LogicalModel-Person\'.indexOf(\'-\'))',
                'patient-example.xml',
                'LogicalModel',
            ],
            'testSubstring9' => ['{}.substring(25).empty() = true', 'patient-example.xml', true],
            'testStartsWith1' => ['\'12345\'.startsWith(\'2\') = false', 'patient-example.xml', true],
            'testStartsWith2' => ['\'12345\'.startsWith(\'1\') = true', 'patient-example.xml', true],
            'testStartsWith3' => ['\'12345\'.startsWith(\'12\') = true', 'patient-example.xml', true],
            'testStartsWith4' => ['\'12345\'.startsWith(\'13\') = false', 'patient-example.xml', true],
            'testStartsWith5' => ['\'12345\'.startsWith(\'12345\') = true', 'patient-example.xml', true],
            'testStartsWith6' => ['\'12345\'.startsWith(\'123456\') = false', 'patient-example.xml', true],
            'testStartsWith7' => ['\'12345\'.startsWith(\'\') = true', 'patient-example.xml', true],
            'testStartsWith8' => ['{}.startsWith(\'1\').empty() = true', 'patient-example.xml', true],
            'testStartsWith9' => ['{}.startsWith(\'\').empty() = true', 'patient-example.xml', true],
            'testStartsWith10' => ['\'\'.startsWith(\'\') = true', 'patient-example.xml', true],
            'testStartsWith11' => ['{}.startsWith(\'\').exists() = false', 'patient-example.xml', true],
            'testEndsWith1' => ['\'12345\'.endsWith(\'2\') = false', 'patient-example.xml', true],
            'testEndsWith2' => ['\'12345\'.endsWith(\'5\') = true', 'patient-example.xml', true],
            'testEndsWith3' => ['\'12345\'.endsWith(\'45\') = true', 'patient-example.xml', true],
            'testEndsWith4' => ['\'12345\'.endsWith(\'35\') = false', 'patient-example.xml', true],
            'testEndsWith5' => ['\'12345\'.endsWith(\'12345\') = true', 'patient-example.xml', true],
            'testEndsWith6' => ['\'12345\'.endsWith(\'012345\') = false', 'patient-example.xml', true],
            'testEndsWith7' => ['\'12345\'.endsWith(\'\') = true', 'patient-example.xml', true],
            'testEndsWith8' => ['{}.endsWith(\'1\').empty() = true', 'patient-example.xml', true],
            'testEndsWith9' => ['{}.endsWith(\'\').empty() = true', 'patient-example.xml', true],
            'testContainsString1' => ['\'12345\'.contains(\'6\') = false', 'patient-example.xml', true],
            'testContainsString2' => ['\'12345\'.contains(\'5\') = true', 'patient-example.xml', true],
            'testContainsString3' => ['\'12345\'.contains(\'45\') = true', 'patient-example.xml', true],
            'testContainsString4' => ['\'12345\'.contains(\'35\') = false', 'patient-example.xml', true],
            'testContainsString5' => ['\'12345\'.contains(\'12345\') = true', 'patient-example.xml', true],
            'testContainsString6' => ['\'12345\'.contains(\'012345\') = false', 'patient-example.xml', true],
            'testContainsString7' => ['\'12345\'.contains(\'\') = true', 'patient-example.xml', true],
            'testContainsString8' => ['{}.contains(\'a\').empty() = true', 'patient-example.xml', true],
            'testContainsString9' => ['{}.contains(\'\').empty() = true', 'patient-example.xml', true],
            'testMatchesEmpty' => ['\'FHIR\'.matches({}).empty() = true', 'void', true],
            'testMatchesEmpty2' => ['{}.matches(\'FHIR\').empty() = true', 'void', true],
            'testMatchesEmpty3' => ['{}.matches({}).empty() = true', 'void', true],
            'testMatchesWithinUrl1' => [
                '\'http://fhir.org/guides/cqf/common/Library/FHIR-ModelInfo|4.0.1\'.matches(\'library\')',
                'void',
                false,
            ],
            'testMatchesWithinUrl2' => [
                '\'http://fhir.org/guides/cqf/common/Library/FHIR-ModelInfo|4.0.1\'.matches(\'Library\')',
                'void',
                true,
            ],
            'testMatchesWithinUrl3' => [
                '\'http://fhir.org/guides/cqf/common/Library/FHIR-ModelInfo|4.0.1\'.matches(\'^Library$\')',
                'void',
                false,
            ],
            'testMatchesWithinUrl1a' => [
                '\'http://fhir.org/guides/cqf/common/Library/FHIR-ModelInfo|4.0.1\'.matches(\'.*Library.*\')',
                'void',
                true,
            ],
            'testMatchesWithinUrl4' => [
                '\'http://fhir.org/guides/cqf/common/Library/FHIR-ModelInfo|4.0.1\'.matches(\'Measure\')',
                'void',
                false,
            ],
            'testReplace1' => ['\'123456\'.replace(\'234\', \'X\')', 'patient-example.xml', '1X56'],
            'testReplace2' => ['\'abc\'.replace(\'\', \'x\')', 'patient-example.xml', 'xaxbxcx'],
            'testReplace3' => ['\'123456\'.replace(\'234\', \'\')', 'patient-example.xml', '156'],
            'testReplace4' => ['{}.replace(\'234\', \'X\').empty() = true', 'patient-example.xml', true],
            'testReplace5' => ['\'123\'.replace({}, \'X\').empty() = true', 'patient-example.xml', true],
            'testLength1' => ['\'123456\'.length() = 6', 'patient-example.xml', true],
            'testLength2' => ['\'12345\'.length() = 5', 'patient-example.xml', true],
            'testLength3' => ['\'123\'.length() = 3', 'patient-example.xml', true],
            'testLength4' => ['\'1\'.length() = 1', 'patient-example.xml', true],
            'testLength5' => ['\'\'.length() = 0', 'patient-example.xml', true],
            'testLength6' => ['{}.length().empty() = true', 'patient-example.xml', true],
            'testTrace1' => ['name.given.trace(\'test\').count() = 5', 'patient-example.xml', true],
            'testTrace2' => ['name.trace(\'test\', given).count() = 3', 'patient-example.xml', true],
            'testToday1' => ['Patient.birthDate < today()', 'patient-example.xml', true],
            'testToday2' => ['today().toString().length() = 10', 'patient-example.xml', true],
            'testNow2' => ['now().toString().length() > 10', 'patient-example.xml', true],
            'testEquality1' => ['1 = 1', 'patient-example.xml', true],
            'testEquality2' => ['{} = {}', 'patient-example.xml', []],
            'testEquality3' => ['true = {}', 'patient-example.xml', []],
            'testEquality4' => ['(1) = (1)', 'patient-example.xml', true],
            'testEquality5' => ['(1 | 2) = (1 | 2)', 'patient-example.xml', true],
            'testEquality6' => ['(1 | 2 | 3) = (1 | 2 | 3)', 'patient-example.xml', true],
            'testEquality7' => ['(1 | 1) = (1 | 2 | {})', 'patient-example.xml', false],
            'testEquality8' => ['1 = 2', 'patient-example.xml', false],
            'testEquality9' => ['\'a\' = \'a\'', 'patient-example.xml', true],
            'testEquality10' => ['\'a\' = \'A\'', 'patient-example.xml', false],
            'testEquality11' => ['\'a\' = \'b\'', 'patient-example.xml', false],
            'testEquality12' => ['1.1 = 1.1', 'patient-example.xml', true],
            'testEquality13' => ['1.1 = 1.2', 'patient-example.xml', false],
            'testEquality14' => ['1.10 = 1.1', 'patient-example.xml', true],
            'testEquality15' => ['0 = 0', 'patient-example.xml', true],
            'testEquality16' => ['0.0 = 0', 'patient-example.xml', true],
            'testEquality17' => ['@2012-04-15 = @2012-04-15', 'patient-example.xml', true],
            'testEquality18' => ['@2012-04-15 = @2012-04-16', 'patient-example.xml', false],
            'testEquality19' => ['@2012-04-15 = @2012-04-15T10:00:00', 'patient-example.xml', []],
            'testEquality20' => ['@2012-04-15T15:00:00 = @2012-04-15T10:00:00', 'patient-example.xml', false],
            'testEquality21' => ['@2012-04-15T15:30:31 = @2012-04-15T15:30:31.0', 'patient-example.xml', true],
            'testEquality22' => ['@2012-04-15T15:30:31 = @2012-04-15T15:30:31.1', 'patient-example.xml', false],
            'testEquality23' => ['@2012-04-15T15:00:00Z = @2012-04-15T10:00:00', 'patient-example.xml', []],
            'testEquality24' => ['@2012-04-15T15:00:00+02:00 = @2012-04-15T16:00:00+03:00', 'patient-example.xml', true],
            'testEquality25' => ['name = name', 'patient-example.xml', true],
            'testEquality26' => ['name.take(2) = name.take(2).first() | name.take(2).last()', 'patient-example.xml', true],
            'testEquality27' => ['name.take(2) = name.take(2).last() | name.take(2).first()', 'patient-example.xml', false],
            'testNEquality1' => ['1 != 1', 'patient-example.xml', false],
            'testNEquality2' => ['{} != {}', 'patient-example.xml', []],
            'testNEquality3' => ['1 != 2', 'patient-example.xml', true],
            'testNEquality4' => ['\'a\' != \'a\'', 'patient-example.xml', false],
            'testNEquality5' => ['\'a\' != \'b\'', 'patient-example.xml', true],
            'testNEquality6' => ['1.1 != 1.1', 'patient-example.xml', false],
            'testNEquality7' => ['1.1 != 1.2', 'patient-example.xml', true],
            'testNEquality8' => ['1.10 != 1.1', 'patient-example.xml', false],
            'testNEquality9' => ['0 != 0', 'patient-example.xml', false],
            'testNEquality10' => ['0.0 != 0', 'patient-example.xml', false],
            'testNEquality11' => ['@2012-04-15 != @2012-04-15', 'patient-example.xml', false],
            'testNEquality12' => ['@2012-04-15 != @2012-04-16', 'patient-example.xml', true],
            'testNEquality13' => ['@2012-04-15 != @2012-04-15T10:00:00', 'patient-example.xml', []],
            'testNEquality14' => ['@2012-04-15T15:00:00 != @2012-04-15T10:00:00', 'patient-example.xml', true],
            'testNEquality15' => ['@2012-04-15T15:30:31 != @2012-04-15T15:30:31.0', 'patient-example.xml', false],
            'testNEquality16' => ['@2012-04-15T15:30:31 != @2012-04-15T15:30:31.1', 'patient-example.xml', true],
            'testNEquality17' => ['@2012-04-15T15:00:00Z != @2012-04-15T10:00:00', 'patient-example.xml', []],
            'testNEquality18' => ['@2012-04-15T15:00:00+02:00 != @2012-04-15T16:00:00+03:00', 'patient-example.xml', false],
            'testNEquality19' => ['name != name', 'patient-example.xml', false],
            'testNEquality20' => ['name.take(2) != name.take(2).first() | name.take(2).last()', 'patient-example.xml', false],
            'testNEquality21' => ['name.take(2) != name.take(2).last() | name.take(2).first()', 'patient-example.xml', true],
            'testNEquality22' => ['(1.2 / 1.8).round(2) != 0.6666667', 'patient-example.xml', true],
            'testNEquality23' => ['(1.2 / 1.8).round(2) != 0.67', 'patient-example.xml', false],
            'testNEquality24' => ['Observation.value != 185 \'kg\'', 'observation-example.xml', true],
            'testEquivalent1' => ['1 ~ 1', 'patient-example.xml', true],
            'testEquivalent2' => ['{} ~ {}', 'patient-example.xml', true],
            'testEquivalent3' => ['1 ~ {}', 'patient-example.xml', false],
            'testEquivalent4' => ['1 ~ 2', 'patient-example.xml', false],
            'testEquivalent5' => ['\'a\' ~ \'a\'', 'patient-example.xml', true],
            'testEquivalent6' => ['\'a\' ~ \'A\'', 'patient-example.xml', true],
            'testEquivalent7' => ['\'a\' ~ \'b\'', 'patient-example.xml', false],
            'testEquivalent8' => ['1.1 ~ 1.1', 'patient-example.xml', true],
            'testEquivalent9' => ['1.1 ~ 1.2', 'patient-example.xml', false],
            'testEquivalent10' => ['1.10 ~ 1.1', 'patient-example.xml', true],
            'testEquivalent11' => ['1.2 / 1.8 ~ 0.67', 'patient-example.xml', true],
            'testEquivalent12' => ['0 ~ 0', 'patient-example.xml', true],
            'testEquivalent13' => ['0.0 ~ 0', 'patient-example.xml', true],
            'testEquivalent14' => ['@2012-04-15 ~ @2012-04-15', 'patient-example.xml', true],
            'testEquivalent15' => ['@2012-04-15 ~ @2012-04-16', 'patient-example.xml', false],
            'testEquivalent16' => ['@2012-04-15 ~ @2012-04-15T10:00:00', 'patient-example.xml', false],
            'testEquivalent17' => ['@2012-04-15T15:30:31 ~ @2012-04-15T15:30:31.0', 'patient-example.xml', true],
            'testEquivalent18' => ['@2012-04-15T15:30:31 ~ @2012-04-15T15:30:31.1', 'patient-example.xml', false],
            'testEquivalent19' => ['name ~ name', 'patient-example.xml', true],
            'testEquivalent20' => [
                'name.take(2).given ~ name.take(2).first().given | name.take(2).last().given',
                'patient-example.xml',
                true,
            ],
            'testEquivalent21' => [
                'name.take(2).given ~ name.take(2).last().given | name.take(2).first().given',
                'patient-example.xml',
                true,
            ],
            'testNotEquivalent1' => ['1 !~ 1', 'patient-example.xml', false],
            'testNotEquivalent2' => ['{} !~ {}', 'patient-example.xml', false],
            'testNotEquivalent3' => ['{} !~ 1', 'patient-example.xml', true],
            'testNotEquivalent4' => ['1 !~ 2', 'patient-example.xml', true],
            'testNotEquivalent5' => ['\'a\' !~ \'a\'', 'patient-example.xml', false],
            'testNotEquivalent6' => ['\'a\' !~ \'A\'', 'patient-example.xml', false],
            'testNotEquivalent7' => ['\'a\' !~ \'b\'', 'patient-example.xml', true],
            'testNotEquivalent8' => ['1.1 !~ 1.1', 'patient-example.xml', false],
            'testNotEquivalent9' => ['1.1 !~ 1.2', 'patient-example.xml', true],
            'testNotEquivalent10' => ['1.10 !~ 1.1', 'patient-example.xml', false],
            'testNotEquivalent11' => ['0 !~ 0', 'patient-example.xml', false],
            'testNotEquivalent12' => ['0.0 !~ 0', 'patient-example.xml', false],
            'testNotEquivalent13' => ['1.2 / 1.8 !~ 0.6', 'patient-example.xml', true],
            'testNotEquivalent14' => ['@2012-04-15 !~ @2012-04-15', 'patient-example.xml', false],
            'testNotEquivalent15' => ['@2012-04-15 !~ @2012-04-16', 'patient-example.xml', true],
            'testNotEquivalent16' => ['@2012-04-15 !~ @2012-04-15T10:00:00', 'patient-example.xml', true],
            'testNotEquivalent17' => ['@2012-04-15T15:30:31 !~ @2012-04-15T15:30:31.0', 'patient-example.xml', false],
            'testNotEquivalent18' => ['@2012-04-15T15:30:31 !~ @2012-04-15T15:30:31.1', 'patient-example.xml', true],
            'testNotEquivalent19' => ['name !~ name', 'patient-example.xml', false],
            'testNotEquivalent20' => [
                'name.take(2).given !~ name.take(2).first().given | name.take(2).last().given',
                'patient-example.xml',
                false,
            ],
            'testNotEquivalent21' => [
                'name.take(2).given !~ name.take(2).last().given | name.take(2).first().given',
                'patient-example.xml',
                false,
            ],
            'testNotEquivalent22' => ['Observation.value !~ 185 \'kg\'', 'observation-example.xml', true],
            'testLessThan1' => ['1 < 2', 'patient-example.xml', true],
            'testLessThan2' => ['1.0 < 1.2', 'patient-example.xml', true],
            'testLessThan3' => ['\'a\' < \'b\'', 'patient-example.xml', true],
            'testLessThan4' => ['\'A\' < \'a\'', 'patient-example.xml', true],
            'testLessThan5' => ['@2014-12-12 < @2014-12-13', 'patient-example.xml', true],
            'testLessThan6' => ['@2014-12-13T12:00:00 < @2014-12-13T12:00:01', 'patient-example.xml', true],
            'testLessThan7' => ['@T12:00:00 < @T14:00:00', 'patient-example.xml', true],
            'testLessThan8' => ['1 < 1', 'patient-example.xml', false],
            'testLessThan9' => ['1.0 < 1.0', 'patient-example.xml', false],
            'testLessThan10' => ['\'a\' < \'a\'', 'patient-example.xml', false],
            'testLessThan11' => ['\'A\' < \'A\'', 'patient-example.xml', false],
            'testLessThan12' => ['@2014-12-12 < @2014-12-12', 'patient-example.xml', false],
            'testLessThan13' => ['@2014-12-13T12:00:00 < @2014-12-13T12:00:00', 'patient-example.xml', false],
            'testLessThan14' => ['@T12:00:00 < @T12:00:00', 'patient-example.xml', false],
            'testLessThan15' => ['2 < 1', 'patient-example.xml', false],
            'testLessThan16' => ['1.1 < 1.0', 'patient-example.xml', false],
            'testLessThan17' => ['\'b\' < \'a\'', 'patient-example.xml', false],
            'testLessThan18' => ['\'B\' < \'A\'', 'patient-example.xml', false],
            'testLessThan19' => ['@2014-12-13 < @2014-12-12', 'patient-example.xml', false],
            'testLessThan20' => ['@2014-12-13T12:00:01 < @2014-12-13T12:00:00', 'patient-example.xml', false],
            'testLessThan21' => ['@T12:00:01 < @T12:00:00', 'patient-example.xml', false],
            'testLessThan22' => ['Observation.value < 200 \'[lb_av]\'', 'observation-example.xml', true],
            'testLessThan23' => ['@2018-03 < @2018-03-01', 'patient-example.xml', []],
            'testLessThan24' => ['@2018-03-01T10:30 < @2018-03-01T10:30:00', 'patient-example.xml', []],
            'testLessThan25' => ['@T10:30 < @T10:30:00', 'patient-example.xml', []],
            'testLessThan26' => ['@2018-03-01T10:30:00 < @2018-03-01T10:30:00.0', 'patient-example.xml', false],
            'testLessThan27' => ['@T10:30:00 < @T10:30:00.0', 'patient-example.xml', false],
            'testLessOrEqual1' => ['1 <= 2', 'patient-example.xml', true],
            'testLessOrEqual2' => ['1.0 <= 1.2', 'patient-example.xml', true],
            'testLessOrEqual3' => ['\'a\' <= \'b\'', 'patient-example.xml', true],
            'testLessOrEqual4' => ['\'A\' <= \'a\'', 'patient-example.xml', true],
            'testLessOrEqual5' => ['@2014-12-12 <= @2014-12-13', 'patient-example.xml', true],
            'testLessOrEqual6' => ['@2014-12-13T12:00:00 <= @2014-12-13T12:00:01', 'patient-example.xml', true],
            'testLessOrEqual7' => ['@T12:00:00 <= @T14:00:00', 'patient-example.xml', true],
            'testLessOrEqual8' => ['1 <= 1', 'patient-example.xml', true],
            'testLessOrEqual9' => ['1.0 <= 1.0', 'patient-example.xml', true],
            'testLessOrEqual10' => ['\'a\' <= \'a\'', 'patient-example.xml', true],
            'testLessOrEqual11' => ['\'A\' <= \'A\'', 'patient-example.xml', true],
            'testLessOrEqual12' => ['@2014-12-12 <= @2014-12-12', 'patient-example.xml', true],
            'testLessOrEqual13' => ['@2014-12-13T12:00:00 <= @2014-12-13T12:00:00', 'patient-example.xml', true],
            'testLessOrEqual14' => ['@T12:00:00 <= @T12:00:00', 'patient-example.xml', true],
            'testLessOrEqual15' => ['2 <= 1', 'patient-example.xml', false],
            'testLessOrEqual16' => ['1.1 <= 1.0', 'patient-example.xml', false],
            'testLessOrEqual17' => ['\'b\' <= \'a\'', 'patient-example.xml', false],
            'testLessOrEqual18' => ['\'B\' <= \'A\'', 'patient-example.xml', false],
            'testLessOrEqual19' => ['@2014-12-13 <= @2014-12-12', 'patient-example.xml', false],
            'testLessOrEqual20' => ['@2014-12-13T12:00:01 <= @2014-12-13T12:00:00', 'patient-example.xml', false],
            'testLessOrEqual21' => ['@T12:00:01 <= @T12:00:00', 'patient-example.xml', false],
            'testLessOrEqual22' => ['Observation.value <= 200 \'[lb_av]\'', 'observation-example.xml', true],
            'testLessOrEqual23' => ['@2018-03 <= @2018-03-01', 'patient-example.xml', []],
            'testLessOrEqual24' => ['@2018-03-01T10:30 <= @2018-03-01T10:30:00', 'patient-example.xml', []],
            'testLessOrEqual25' => ['@T10:30 <= @T10:30:00', 'patient-example.xml', []],
            'testLessOrEqual26' => ['@2018-03-01T10:30:00  <= @2018-03-01T10:30:00.0', 'patient-example.xml', true],
            'testLessOrEqual27' => ['@T10:30:00 <= @T10:30:00.0', 'patient-example.xml', true],
            'testGreatorOrEqual1' => ['1 >= 2', 'patient-example.xml', false],
            'testGreatorOrEqual2' => ['1.0 >= 1.2', 'patient-example.xml', false],
            'testGreatorOrEqual3' => ['\'a\' >= \'b\'', 'patient-example.xml', false],
            'testGreatorOrEqual4' => ['\'A\' >= \'a\'', 'patient-example.xml', false],
            'testGreatorOrEqual5' => ['@2014-12-12 >= @2014-12-13', 'patient-example.xml', false],
            'testGreatorOrEqual6' => ['@2014-12-13T12:00:00 >= @2014-12-13T12:00:01', 'patient-example.xml', false],
            'testGreatorOrEqual7' => ['@T12:00:00 >= @T14:00:00', 'patient-example.xml', false],
            'testGreatorOrEqual8' => ['1 >= 1', 'patient-example.xml', true],
            'testGreatorOrEqual9' => ['1.0 >= 1.0', 'patient-example.xml', true],
            'testGreatorOrEqual10' => ['\'a\' >= \'a\'', 'patient-example.xml', true],
            'testGreatorOrEqual11' => ['\'A\' >= \'A\'', 'patient-example.xml', true],
            'testGreatorOrEqual12' => ['@2014-12-12 >= @2014-12-12', 'patient-example.xml', true],
            'testGreatorOrEqual13' => ['@2014-12-13T12:00:00 >= @2014-12-13T12:00:00', 'patient-example.xml', true],
            'testGreatorOrEqual14' => ['@T12:00:00 >= @T12:00:00', 'patient-example.xml', true],
            'testGreatorOrEqual15' => ['2 >= 1', 'patient-example.xml', true],
            'testGreatorOrEqual16' => ['1.1 >= 1.0', 'patient-example.xml', true],
            'testGreatorOrEqual17' => ['\'b\' >= \'a\'', 'patient-example.xml', true],
            'testGreatorOrEqual18' => ['\'B\' >= \'A\'', 'patient-example.xml', true],
            'testGreatorOrEqual19' => ['@2014-12-13 >= @2014-12-12', 'patient-example.xml', true],
            'testGreatorOrEqual20' => ['@2014-12-13T12:00:01 >= @2014-12-13T12:00:00', 'patient-example.xml', true],
            'testGreatorOrEqual21' => ['@T12:00:01 >= @T12:00:00', 'patient-example.xml', true],
            'testGreatorOrEqual22' => ['Observation.value >= 100 \'[lb_av]\'', 'observation-example.xml', true],
            'testGreatorOrEqual23' => ['@2018-03 >= @2018-03-01', 'patient-example.xml', []],
            'testGreatorOrEqual24' => ['@2018-03-01T10:30 >= @2018-03-01T10:30:00', 'patient-example.xml', []],
            'testGreatorOrEqual25' => ['@T10:30 >= @T10:30:00', 'patient-example.xml', []],
            'testGreatorOrEqual26' => ['@2018-03-01T10:30:00 >= @2018-03-01T10:30:00.0', 'patient-example.xml', true],
            'testGreatorOrEqual27' => ['@T10:30:00 >= @T10:30:00.0', 'patient-example.xml', true],
            'testGreaterThan1' => ['1 > 2', 'patient-example.xml', false],
            'testGreaterThan2' => ['1.0 > 1.2', 'patient-example.xml', false],
            'testGreaterThan3' => ['\'a\' > \'b\'', 'patient-example.xml', false],
            'testGreaterThan4' => ['\'A\' > \'a\'', 'patient-example.xml', false],
            'testGreaterThan5' => ['@2014-12-12 > @2014-12-13', 'patient-example.xml', false],
            'testGreaterThan6' => ['@2014-12-13T12:00:00 > @2014-12-13T12:00:01', 'patient-example.xml', false],
            'testGreaterThan7' => ['@T12:00:00 > @T14:00:00', 'patient-example.xml', false],
            'testGreaterThan8' => ['1 > 1', 'patient-example.xml', false],
            'testGreaterThan9' => ['1.0 > 1.0', 'patient-example.xml', false],
            'testGreaterThan10' => ['\'a\' > \'a\'', 'patient-example.xml', false],
            'testGreaterThan11' => ['\'A\' > \'A\'', 'patient-example.xml', false],
            'testGreaterThan12' => ['@2014-12-12 > @2014-12-12', 'patient-example.xml', false],
            'testGreaterThan13' => ['@2014-12-13T12:00:00 > @2014-12-13T12:00:00', 'patient-example.xml', false],
            'testGreaterThan14' => ['@T12:00:00 > @T12:00:00', 'patient-example.xml', false],
            'testGreaterThan15' => ['2 > 1', 'patient-example.xml', true],
            'testGreaterThan16' => ['1.1 > 1.0', 'patient-example.xml', true],
            'testGreaterThan17' => ['\'b\' > \'a\'', 'patient-example.xml', true],
            'testGreaterThan18' => ['\'B\' > \'A\'', 'patient-example.xml', true],
            'testGreaterThan19' => ['@2014-12-13 > @2014-12-12', 'patient-example.xml', true],
            'testGreaterThan20' => ['@2014-12-13T12:00:01 > @2014-12-13T12:00:00', 'patient-example.xml', true],
            'testGreaterThan21' => ['@T12:00:01 > @T12:00:00', 'patient-example.xml', true],
            'testGreaterThan22' => ['Observation.value > 100 \'[lb_av]\'', 'observation-example.xml', true],
            'testGreaterThan23' => ['@2018-03 > @2018-03-01', 'patient-example.xml', []],
            'testGreaterThan24' => ['@2018-03-01T10:30 > @2018-03-01T10:30:00', 'patient-example.xml', []],
            'testGreaterThan25' => ['@T10:30 > @T10:30:00', 'patient-example.xml', []],
            'testGreaterThan26' => ['@2018-03-01T10:30:00 > @2018-03-01T10:30:00.0', 'patient-example.xml', false],
            'testGreaterThan27' => ['@T10:30:00 > @T10:30:00.0', 'patient-example.xml', false],
            'testUnion1' => ['(1 | 2 | 3).count() = 3', 'patient-example.xml', true],
            'testUnion2' => ['(1 | 2 | 2).count() = 2', 'patient-example.xml', true],
            'testUnion3' => ['(1|1).count() = 1', 'patient-example.xml', true],
            'testUnion4' => ['1.union(2).union(3).count() = 3', 'patient-example.xml', true],
            'testUnion5' => ['1.union(2.union(3)).count() = 3', 'patient-example.xml', true],
            'testUnion6' => ['(1 | 2).combine(2).count() = 3', 'patient-example.xml', true],
            'testUnion7' => ['1.combine(1).count() = 2', 'patient-example.xml', true],
            'testUnion8' => ['1.combine(1).union(2).count() = 2', 'patient-example.xml', true],
            'testUnion9' => ['name.select(use | given).count()', 'patient-example.xml', 8],
            'testUnion10' => ['name.select(use.union($this.given)).count()', 'patient-example.xml', 8],
            'testUnion11' => ['name.select(use.union(given)).count()', 'patient-example.xml', 8],
            'testIntersect1' => ['(1 | 2 | 3).intersect(2 | 4) = 2', 'patient-example.xml', true],
            'testIntersect2' => ['(1 | 2).intersect(4).empty()', 'patient-example.xml', true],
            'testIntersect3' => ['(1 | 2).intersect({}).empty()', 'patient-example.xml', true],
            'testIntersect4' => ['1.combine(1).intersect(1).count() = 1', 'patient-example.xml', true],
            'testExclude1' => ['(1 | 2 | 3).exclude(2 | 4) = 1 | 3', 'patient-example.xml', true],
            'testExclude2' => ['(1 | 2).exclude(4) = 1 | 2', 'patient-example.xml', true],
            'testExclude3' => ['(1 | 2).exclude({}) = 1 | 2', 'patient-example.xml', true],
            'testExclude4' => ['1.combine(1).exclude(2).count() = 2', 'patient-example.xml', true],
            'testIn1' => ['1 in (1 | 2 | 3)', 'patient-example.xml', true],
            'testIn2' => ['1 in (2 | 3)', 'patient-example.xml', false],
            'testIn3' => ['\'a\' in (\'a\' | \'c\' | \'d\')', 'patient-example.xml', true],
            'testIn4' => ['\'b\' in (\'a\' | \'c\' | \'d\')', 'patient-example.xml', false],
            'testContainsCollection1' => ['(1 | 2 | 3) contains 1', 'patient-example.xml', true],
            'testContainsCollection2' => ['(2 | 3) contains 1 ', 'patient-example.xml', false],
            'testContainsCollection3' => ['(\'a\' | \'c\' | \'d\') contains \'a\'', 'patient-example.xml', true],
            'testContainsCollection4' => ['(\'a\' | \'c\' | \'d\') contains \'b\'', 'patient-example.xml', false],
            'testBooleanLogicAnd1' => ['(true and true) = true', 'patient-example.xml', true],
            'testBooleanLogicAnd2' => ['(true and false) = false', 'patient-example.xml', true],
            'testBooleanLogicAnd3' => ['(true and {}).empty()', 'patient-example.xml', true],
            'testBooleanLogicAnd4' => ['(false and true) = false', 'patient-example.xml', true],
            'testBooleanLogicAnd5' => ['(false and false) = false', 'patient-example.xml', true],
            'testBooleanLogicAnd6' => ['(false and {}) = false', 'patient-example.xml', true],
            'testBooleanLogicAnd7' => ['({} and true).empty()', 'patient-example.xml', true],
            'testBooleanLogicAnd8' => ['({} and false) = false', 'patient-example.xml', true],
            'testBooleanLogicAnd9' => ['({} and {}).empty()', 'patient-example.xml', true],
            'testBooleanLogicOr1' => ['(true or true) = true', 'patient-example.xml', true],
            'testBooleanLogicOr2' => ['(true or false) = true', 'patient-example.xml', true],
            'testBooleanLogicOr3' => ['(true or {}) = true', 'patient-example.xml', true],
            'testBooleanLogicOr4' => ['(false or true) = true', 'patient-example.xml', true],
            'testBooleanLogicOr5' => ['(false or false) = false', 'patient-example.xml', true],
            'testBooleanLogicOr6' => ['(false or {}).empty()', 'patient-example.xml', true],
            'testBooleanLogicOr7' => ['({} or true) = true', 'patient-example.xml', true],
            'testBooleanLogicOr8' => ['({} or false).empty()', 'patient-example.xml', true],
            'testBooleanLogicOr9' => ['({} or {}).empty()', 'patient-example.xml', true],
            'testBooleanLogicXOr1' => ['(true xor true) = false', 'patient-example.xml', true],
            'testBooleanLogicXOr2' => ['(true xor false) = true', 'patient-example.xml', true],
            'testBooleanLogicXOr3' => ['(true xor {}).empty()', 'patient-example.xml', true],
            'testBooleanLogicXOr4' => ['(false xor true) = true', 'patient-example.xml', true],
            'testBooleanLogicXOr5' => ['(false xor false) = false', 'patient-example.xml', true],
            'testBooleanLogicXOr6' => ['(false xor {}).empty()', 'patient-example.xml', true],
            'testBooleanLogicXOr7' => ['({} xor true).empty()', 'patient-example.xml', true],
            'testBooleanLogicXOr8' => ['({} xor false).empty()', 'patient-example.xml', true],
            'testBooleanLogicXOr9' => ['({} xor {}).empty()', 'patient-example.xml', true],
            'testBooleanImplies1' => ['(true implies true) = true', 'patient-example.xml', true],
            'testBooleanImplies2' => ['(true implies false) = false', 'patient-example.xml', true],
            'testBooleanImplies3' => ['(true implies {}).empty()', 'patient-example.xml', true],
            'testBooleanImplies4' => ['(false implies true) = true', 'patient-example.xml', true],
            'testBooleanImplies5' => ['(false implies false) = true', 'patient-example.xml', true],
            'testBooleanImplies6' => ['(false implies {}) = true', 'patient-example.xml', true],
            'testBooleanImplies7' => ['({} implies true) = true', 'patient-example.xml', true],
            'testBooleanImplies8' => ['({} implies false).empty()', 'patient-example.xml', true],
            'testBooleanImplies9' => ['({} implies {}).empty()', 'patient-example.xml', true],
            'testPlus1' => ['1 + 1 = 2', 'patient-example.xml', true],
            'testPlus2' => ['1 + 0 = 1', 'patient-example.xml', true],
            'testPlus3' => ['1.2 + 1.8 = 3.0', 'patient-example.xml', true],
            'testPlus4' => ['\'a\'+\'b\' = \'ab\'', 'patient-example.xml', true],
            'testConcatenate1' => ['\'a\' & \'b\' = \'ab\'', 'patient-example.xml', true],
            'testConcatenate2' => ['\'1\' & {} = \'1\'', 'patient-example.xml', true],
            'testConcatenate3' => ['{} & \'b\' = \'b\'', 'patient-example.xml', true],
            'testMinus1' => ['1 - 1 = 0', 'patient-example.xml', true],
            'testMinus2' => ['1 - 0 = 1', 'patient-example.xml', true],
            'testMinus3' => ['1.8 - 1.2 = 0.6', 'patient-example.xml', true],
            'testMultiply1' => ['1 * 1 = 1', 'patient-example.xml', true],
            'testMultiply2' => ['1 * 0 = 0', 'patient-example.xml', true],
            'testMultiply3' => ['1.2 * 1.8 = 2.16', 'patient-example.xml', true],
            'testDivide1' => ['1 / 1 = 1', 'patient-example.xml', true],
            'testDivide2' => ['4 / 2 = 2', 'patient-example.xml', true],
            'testDivide3' => ['4.0 / 2.0 = 2.0', 'patient-example.xml', true],
            'testDivide4' => ['1 / 2 = 0.5', 'patient-example.xml', true],
            'testDivide5' => ['(1.2 / 1.8).round(2) = 0.67', 'patient-example.xml', true],
            'testDivide6' => ['1 / 0', 'patient-example.xml', []],
            'testDiv1' => ['1 div 1 = 1', 'patient-example.xml', true],
            'testDiv2' => ['4 div 2 = 2', 'patient-example.xml', true],
            'testDiv3' => ['5 div 2 = 2', 'patient-example.xml', true],
            'testDiv4' => ['2.2 div 1.8 = 1', 'patient-example.xml', true],
            'testDiv5' => ['5 div 0', 'patient-example.xml', []],
            'testMod1' => ['1 mod 1 = 0', 'patient-example.xml', true],
            'testMod2' => ['4 mod 2 = 0', 'patient-example.xml', true],
            'testMod3' => ['5 mod 2 = 1', 'patient-example.xml', true],
            'testMod4' => ['2.2 mod 1.8 = 0.4', 'patient-example.xml', true],
            'testMod5' => ['5 mod 0', 'patient-example.xml', []],
            'testRound1' => ['1.round() = 1', 'patient-example.xml', true],
            'testRound2' => ['3.14159.round(3) = 3.142', 'patient-example.xml', true],
            'testSqrt1' => ['81.sqrt() = 9.0', 'patient-example.xml', true],
            'testSqrt2' => ['(-1).sqrt()', 'patient-example.xml', []],
            'testAbs1' => ['(-5).abs() = 5', 'patient-example.xml', true],
            'testAbs2' => ['(-5.5).abs() = 5.5', 'patient-example.xml', true],
            'testAbs3' => ['(-5.5 \'mg\').abs() = 5.5 \'mg\'', 'patient-example.xml', true],
            'testCeiling1' => ['1.ceiling() = 1', 'patient-example.xml', true],
            'testCeiling2' => ['(-1.1).ceiling() = -1', 'patient-example.xml', true],
            'testCeiling3' => ['1.1.ceiling() = 2', 'patient-example.xml', true],
            'testExp1' => ['0.exp() = 1', 'patient-example.xml', true],
            'testExp2' => ['(-0.0).exp() = 1', 'patient-example.xml', true],
            'testExp3' => ['{}.exp().empty() = true', 'patient-example.xml', true],
            'testFloor1' => ['1.floor() = 1', 'patient-example.xml', true],
            'testFloor2' => ['2.1.floor() = 2', 'patient-example.xml', true],
            'testFloor3' => ['(-2.1).floor() = -3', 'patient-example.xml', true],
            'testLn1' => ['1.ln() = 0.0', 'patient-example.xml', true],
            'testLn2' => ['1.0.ln() = 0.0', 'patient-example.xml', true],
            'testLog1' => ['16.log(2) = 4.0', 'patient-example.xml', true],
            'testLog2' => ['100.0.log(10.0) = 2.0', 'patient-example.xml', true],
            'testPower1' => ['2.power(3) = 8', 'patient-example.xml', true],
            'testPower2' => ['2.5.power(2) = 6.25', 'patient-example.xml', true],
            'testPower3' => ['(-1).power(0.5)', 'patient-example.xml', []],
            'testTruncate1' => ['101.truncate() = 101', 'patient-example.xml', true],
            'testTruncate2' => ['1.00000001.truncate() = 1', 'patient-example.xml', true],
            'testTruncate3' => ['(-1.56).truncate() = -1', 'patient-example.xml', true],
            'testPrecedence2' => ['1+2*3+4 = 11', 'patient-example.xml', true],
            'testVariables1' => ['%sct = \'http://snomed.info/sct\'', 'patient-example.xml', true],
            'testVariables2' => ['%loinc = \'http://loinc.org\'', 'patient-example.xml', true],
            'testVariables3' => ['%ucum = \'http://unitsofmeasure.org\'', 'patient-example.xml', true],
            'testExtension1' => [
                'Patient.birthDate.extension(\'http://hl7.org/fhir/StructureDefinition/patient-birthTime\').exists()',
                'patient-example.xml',
                true,
            ],
            'testExtension3' => [
                'Patient.birthDate.extension(\'http://hl7.org/fhir/StructureDefinition/patient-birthTime1\').empty()',
                'patient-example.xml',
                true,
            ],
            'testType1' => ['1.type().namespace = \'System\'', 'patient-example.xml', true],
            'testType1a' => ['1.type().name = \'Integer\'', 'patient-example.xml', true],
            'testType2' => ['\'1\'.type().namespace = \'System\'', 'patient-example.xml', true],
            'testType2a' => ['\'1\'.type().name = \'String\'', 'patient-example.xml', true],
            'testType3' => ['true.type().namespace = \'System\'', 'patient-example.xml', true],
            'testType4' => ['true.type().name = \'Boolean\'', 'patient-example.xml', true],
            'testType5' => ['true.is(Boolean)', 'patient-example.xml', true],
            'testType6' => ['true.is(System.Boolean)', 'patient-example.xml', true],
            'testType7' => ['true is Boolean', 'patient-example.xml', true],
            'testType8' => ['true is System.Boolean', 'patient-example.xml', true],
            'testType9' => ['Patient.active.type().namespace = \'FHIR\'', 'patient-example.xml', true],
            'testType10' => ['Patient.active.type().name = \'boolean\'', 'patient-example.xml', true],
            'testType11' => ['Patient.active.is(boolean)', 'patient-example.xml', true],
            'testType12' => ['Patient.active.is(Boolean).not()', 'patient-example.xml', true],
            'testType13' => ['Patient.active.is(FHIR.boolean)', 'patient-example.xml', true],
            'testType14' => ['Patient.active.is(System.Boolean).not()', 'patient-example.xml', true],
            'testType15' => ['Patient.type().namespace = \'FHIR\'', 'patient-example.xml', true],
            'testType16' => ['Patient.type().name = \'Patient\'', 'patient-example.xml', true],
            'testType17' => ['Patient.is(Patient)', 'patient-example.xml', true],
            'testType18' => ['Patient.is(FHIR.Patient)', 'patient-example.xml', true],
            'testType19' => ['Patient.is(FHIR.`Patient`)', 'patient-example.xml', true],
            'testType20' => ['Patient.ofType(Patient).type().name', 'patient-example.xml', 'Patient'],
            'testType21' => ['Patient.ofType(FHIR.Patient).type().name', 'patient-example.xml', 'Patient'],
            'testType22' => ['Patient.is(System.Patient).not()', 'patient-example.xml', true],
            'testType23' => ['Patient.ofType(FHIR.`Patient`).type().name', 'patient-example.xml', 'Patient'],
            'testTypeA1' => ['Parameters.parameter[0].value.is(FHIR.string)', 'parameters-example-types.xml', true],
            'testTypeA2' => ['Parameters.parameter[1].value.is(FHIR.integer)', 'parameters-example-types.xml', true],
            'testTypeA3' => ['Parameters.parameter[2].value.is(FHIR.uuid)', 'parameters-example-types.xml', true],
            'testTypeA4' => ['Parameters.parameter[2].value.is(FHIR.uri)', 'parameters-example-types.xml', true],
            'testTypeA' => ['Parameters.parameter[3].value.is(FHIR.decimal)', 'parameters-example-types.xml', true],

            'testPolymorphicsA' => ['Observation.value.exists()', 'observation-example.xml', true],
            'testIndex' => ['Patient.telecom.select(iif(value=\'(03) 3410 5613\', $index, {} ))', 'patient-example.xml', 2],
            'testPeriodInvariantOld' => [
                'Patient.identifier.period.all(start.hasValue().not() or end.hasValue().not() or (start <= end))',
                'patient-example-period.xml',
                false,
            ],
            'testFHIRPathIsFunction1' => ['Patient.gender.is(code)', 'patient-example.xml', true],
            'testFHIRPathIsFunction2' => ['Patient.gender.is(string)', 'patient-example.xml', true],
            'testFHIRPathIsFunction3' => ['Patient.gender.is(id)', 'patient-example.xml', false],
            'testFHIRPathIsFunction4' => ['Questionnaire.url.is(uri)', 'questionnaire-example.xml', true],
            'testFHIRPathIsFunction5' => ['Questionnaire.url.is(url)', 'questionnaire-example.xml', false],
            'testFHIRPathIsFunction6' => ['ValueSet.version.is(string)', 'valueset-example-expansion.xml', true],
            'testFHIRPathIsFunction7' => ['ValueSet.version.is(code)', 'valueset-example-expansion.xml', false],
            'testFHIRPathIsFunction8' => [
                'Observation.extension(\'http://example.com/fhir/StructureDefinition/patient-age\').value is Age',
                'observation-example.xml',
                true,
            ],
            'testFHIRPathIsFunction9' => [
                'Observation.extension(\'http://example.com/fhir/StructureDefinition/patient-age\').value is Quantity',
                'observation-example.xml',
                true,
            ],
            'testFHIRPathIsFunction10' => [
                'Observation.extension(\'http://example.com/fhir/StructureDefinition/patient-age\').value is Duration',
                'observation-example.xml',
                false,
            ],
            'testFHIRPathAsFunction12' => ['Patient.gender.as(code)', 'patient-example.xml', 'male'],
            'testFHIRPathAsFunction13' => ['Patient.gender.as(id)', 'patient-example.xml', []],
            'testFHIRPathAsFunction14' => ['ValueSet.version.as(string)', 'valueset-example-expansion.xml', '20150622'],
            'testFHIRPathAsFunction15' => ['ValueSet.version.as(code)', 'valueset-example-expansion.xml', []],
            'testFHIRPathAsFunction16' => ['Patient.gender.ofType(string)', 'patient-example.xml', []],
            'testFHIRPathAsFunction17' => ['Patient.gender.ofType(code)', 'patient-example.xml', 'male'],
            'testFHIRPathAsFunction18' => ['Patient.gender.ofType(id)', 'patient-example.xml', []],
            'testFHIRPathAsFunction19' => ['ValueSet.version.ofType(string)', 'valueset-example-expansion.xml', '20150622'],
            'testFHIRPathAsFunction20' => ['ValueSet.version.ofType(code)', 'valueset-example-expansion.xml', []],
            'testFHIRPathAsFunction22' => ['Patient.name.ofType(HumanName).use', 'patient-example.xml', 'official'],
            'testContainedId' => ['contained.id', 'patient-container-example.json', '1'],
            'testFHIRAndSystemConversion1' => ["Patient.identifier.first().system = 'urn:oid:1.2.36.146.595.217.0.1'", 'patient-example.xml', true],
            'testFHIRAndSystemConversion2' => ["Observation.value.system = 'http://unitsofmeasure.org'", 'observation-example.xml', true],
            'testFHIRAndSystemConversion3' => ["Observation.value.value = 185", 'observation-example.xml', true],
            'testFHIRAndSystemConversion4' => ["Observation.value = 185 'lbs'", 'observation-example.xml', true],
            'testFHIRAndSystemConversion5' => ["Patient.identifier.type.coding.system = 'http://terminology.hl7.org/CodeSystem/v2-0203'", 'patient-example.xml', true],
            'testFHIRAndSystemConversion6' => ["Patient.identifier.where(type.coding.system = 'http://terminology.hl7.org/CodeSystem/v2-0203').value", 'patient-example.xml', "12345"],
            'testCollection' => ["(Patient.name.given)", 'patient-example.xml', 'Peter'],
            'testCollection2' => ["Patient.gender = 'male' and (Patient.gender.first() = 'male')", 'patient-example.xml', true],
            'testCollection3' => ["Patient.name.given.subsetOf('Peter' | 'James' | 'Jim')", 'patient-example.xml', true],
            'testCollection3.5' => ["Patient.name.given.subsetOf(('Peter' | 'James' | 'Jim'))", 'patient-example.xml', true],
            'testCollection4' => ["Patient.name.given.supersetOf('Peter' | 'James')", 'patient-example.xml', true],
            'testCollection4.5' => ["Patient.name.given.supersetOf({})", 'patient-example.xml', true],
            'testCollection5' => ["Patient.name.family.empty() or Patient.name.first().family is string", 'patient-example.xml', true],
            'testCollection6' => ["Patient.birthDate.extension('http://hl7.org/fhir/StructureDefinition/patient-birthTime').empty() or Patient.birthDate.extension('http://hl7.org/fhir/StructureDefinition/patient-birthTime').value is dateTime", 'patient-example.xml', true],
            ];
    }

    public function providerExpressionsExecutionFail(): array
    {
        return [
            'testLiteralIntegerNegative1Invalid' => ['-1.convertsToInteger()', 'patient-example.xml'],
            'testLiteralDecimalNegative01Invalid' => ['-0.1.convertsToDecimal()', 'patient-example.xml'],
            'testLiteralDecimalLessThanInvalid' => ['Observation.value.value < \'test\'', 'observation-example.xml'],
            'testNotInvalid' => ['(1|2).not() = false', 'patient-example.xml'],
            'testCollectionBoolean1' => ['iif(1 | 2 | 3, true, false)', 'patient-example.xml'],
            'testSingle2' => ['Patient.name.single().exists()', 'patient-example.xml'],
            'testPlusDate14' => ['@1973-12-25 + 1 \'mo\'', 'patient-example.xml'],
            'testPlusDate16' => ['@1973-12-25 + 1 \'a\'', 'patient-example.xml'],
            'testPlusDate17' => ['@1975-12-25 + 1 \'a\'', 'patient-example.xml'],
            'testConcatenate4' => ['(1 | 2 | 3) & \'b\' = \'1,2,3b\'', 'patient-example.xml'],
            'testMinus4' => ['\'a\'-\'b\' = \'ab\'', 'patient-example.xml'],
            'testMinus6' => ['@1974-12-25 - 1 \'cm\'', 'patient-example.xml'],
            'testPrecedence1' => ['-1.convertsToInteger()', 'patient-example.xml'],
            'from-zulip-2' => ['(true | \'foo\').allTrue()', 'patient-example.xml'],
            'testFHIRPathAsFunction21' => ['Patient.name.as(HumanName).use', 'patient-example.xml'],
            'testFHIRPathAsFunction23' => ['Patient.gender.as(string1)', 'patient-example.xml'],
            'testFHIRPathAsFunction24' => ['Patient.gender.ofType(string1)', 'patient-example.xml'],
        ];
    }

    public function providerExpressionsSemanticFail(): array
    {
        // TODO : skipped because of descendant and children
        return [
            'testSimpleFail' => ['name.given1', 'patient-example.xml'],
            'testSimpleWithWrongContext' => ['Encounter.name.given', 'patient-example.xml'],
            'testPolymorphismB' => ['Observation.valueQuantity.unit', 'observation-example.xml'],
            'testPolymorphismAsB' => ['(Observation.value as Period).unit', 'observation-example.xml'],
            'testDollarOrderNotAllowed' => ['Patient.children().skip(1)', 'patient-example.xml'],
            'testStartsWithNonString1' => ['Appointment.identifier.startsWith(\'rand\')', 'appointment-examplereq.json'],
            'testEndsWithNonString1' => ['Appointment.identifier.endsWith(\'rand\')', 'appointment-examplereq.json'],
            'testContainsNonString1' => ['Appointment.identifier.contains(\'rand\')', 'appointment-examplereq.json'],
            'testPlus6' => ['@1974-12-25 + 7', 'patient-example.xml'],
            'testPolymorphicsB' => ['Observation.valueQuantity.exists()', 'observation-example.xml'],
        ];
    }

    public function providerExpressionsSyntaxFail(): array
    {
        return [
            'testComment7' => ['2 + 2 /', 'patient-example.xml'],
            'testComment8' => ['2 + 2 /* not finished', 'patient-example.xml'],
        ];
    }

    public function providerExpressionsToImplement(): array
    {
        return [
            // TODO : handle extensions values ??
            'testNow1' => ['Patient.birthDate < now()', 'patient-example.xml', true],
            'testDateNotEqual' => ['Patient.birthDate != @1974-12-25T12:34:00', 'patient-example.xml', []],
            'testDateNotEqualTimezoneOffsetBefore' => [
                'Patient.birthDate != @1974-12-25T12:34:00-10:00',
                'patient-example.xml',
                [],
            ],
            'testDateNotEqualTimezoneOffsetAfter' => [
                'Patient.birthDate != @1974-12-25T12:34:00+10:00',
                'patient-example.xml',
                [],
            ],
            'testDateNotEqualUTC' => ['Patient.birthDate != @1974-12-25T12:34:00Z', 'patient-example.xml', []],
            'testDateNotEqualTimeSecond' => ['Patient.birthDate != @T12:14:15', 'patient-example.xml', true],
            'testDateNotEqualTimeMinute' => ['Patient.birthDate != @T12:14', 'patient-example.xml', true],
            'testDateNotEqualToday' => ['Patient.birthDate < today()', 'patient-example.xml', true],
            'testDateTimeGreaterThanDate1' => ['now() > Patient.birthDate', 'patient-example.xml', true],
            'testDateGreaterThanDate' => ['today() > Patient.birthDate', 'patient-example.xml', true],
            'testEquality28' => ['Observation.value = 185 \'[lb_av]\'', 'observation-example.xml', true], // TODO : UCUM - lb_av ~ lbs ?
            'testQuantity1' => ['4.0000 \'g\' = 4000.0 \'mg\'', 'patient-example.xml', true],
            'testQuantity2' => ['4 \'g\' ~ 4000 \'mg\'', 'patient-example.xml', true],
            'testQuantity3' => ['4 \'g\' != 4040 \'mg\'', 'patient-example.xml', true],
            'testQuantity4' => ['4 \'g\' ~ 4040 \'mg\'', 'patient-example.xml', true],
            'testQuantity5' => ['7 days = 1 week', 'patient-example.xml', true],
            'testQuantity6' => ['7 days = 1 \'wk\'', 'patient-example.xml', true],
            'testQuantity7' => ['6 days < 1 week', 'patient-example.xml', true],
            'testQuantity8' => ['8 days > 1 week', 'patient-example.xml', true],
            'testQuantity9' => ['2.0 \'cm\' * 2.0 \'m\' = 0.040 \'m2\'', 'patient-example.xml', true],
            'testQuantity10' => ['4.0 \'g\' / 2.0 \'m\' = 2 \'g/m\'', 'patient-example.xml', true],
            'testQuantity11' => ['1.0 \'m\' / 1.0 \'m\' = 1 \'1\'', 'patient-example.xml', true],
            'testEquivalent22' => ['Observation.value ~ 185 \'[lb_av]\'', 'observation-example.xml', true],
            'testComment1' => ['2 + 2 // This is a single-line comment + 4', 'patient-example.xml', 4],
            'testComment2' => [
                "// This is a multi line comment using // that\n      // should not fail during parsing\n      2+2",
                'patient-example.xml',
                4,
            ],
            'testComment3' => [
                "2 + 2\n      /*\n      This is a multi-line comment\n      Any text enclosed within is ignored\n      +2\n      */",
                'patient-example.xml',
                4,
            ],
            'testComment4' => [
                "2 + 2\n      /*\n      This is a multi-line comment\n      Any text enclosed within is ignored\n      */\n      +2",
                'patient-example.xml',
                6,
            ],
            'testComment5' => [
                "/*\n      This is a multi-line comment\n      Any text enclosed within is ignored\n      */\n      2+2",
                'patient-example.xml',
                4,
            ],
            'testComment6' => ["2 // comment\n      / 2", 'patient-example.xml', 1.0],
            'testComment9' => ['2 + /* inline $@%^+ * */ 2 = 4', 'patient-example.xml', true],
            // https://chat.fhir.org/#narrow/stream/179266-fhirpath/topic/Legal.20FhirPath
            'testIntegerBooleanNotTrue' => ['(0).not() = false', 'patient-example.xml', true],
            'testIntegerBooleanNotFalse' => ['(1).not() = false', 'patient-example.xml', true],
            // not 'week'
            'testQuantityLiteralWeekToString' => ['1 week.toString()', 'patient-example.xml', '1 week'],
            'testStringQuantityDayLiteralToQuantity' => ['\'1 day\'.toQuantity() = 1 \'d\'', 'patient-example.xml', true],
            'testStringQuantityWeekLiteralToQuantity' => [
                '\'1 \\\'wk\\\'\'.toQuantity() = 1 week',
                'patient-example.xml',
                true,
            ],
            'testStringQuantityMonthLiteralToQuantity' => ['\'1 \\\'mo\\\'\'.toQuantity() = 1 month', 'void', []],
            'testStringQuantityYearLiteralToQuantity' => ['\'1 \\\'a\\\'\'.toQuantity() = 1 year', 'void', []],
            'testStringDecimalLiteralToQuantity' => ['\'1.0\'.toQuantity() ~ 1 \'1\'', 'patient-example.xml', true],
            'testIntegerLiteralConvertsToBoolean' => ['1.convertsToBoolean()', 'patient-example.xml', true],
            'testMatchesCaseSensitive1' => ['\'FHIR\'.matches(\'FHIR\')', 'void', true],
            'testMatchesCaseSensitive2' => ['\'FHIR\'.matches(\'fhir\')', 'void', false],
            'testReplaceMatches1' => ['\'123456\'.replaceMatches(\'234\', \'X\')', 'patient-example.xml', '1X56'],
            'testReplaceMatches2' => ['\'abc\'.replaceMatches(\'\', \'x\')', 'patient-example.xml', 'abc'],
            'testReplaceMatches3' => ['\'123456\'.replaceMatches(\'234\', \'\')', 'patient-example.xml', '156'],
            'testReplaceMatches4' => ['{}.replaceMatches(\'234\', \'X\').empty() = true', 'patient-example.xml', true],
            'testReplaceMatches5' => ['\'123\'.replaceMatches({}, \'X\').empty() = true', 'patient-example.xml', true],
            'testReplaceMatches6' => ['\'123\'.replaceMatches(\'2\', {}).empty() = true', 'patient-example.xml', true],
            'testReplaceMatches7' => ['\'abc123\'.replaceMatches(\'[0-9]\', \'-\')', 'patient-example.xml', 'abc---'],
            'testMatchesSingleLineMode1' => ["'A\n      B'.matches('A.*B')", 'void', true], // TODO : match newline
            'testReplace6' => ['\'123\'.replace(\'2\', {}).empty() = true', 'patient-example.xml', true], // TODO : fix arguments
            'testEncodeBase64A' => ['\'test\'.encode(\'base64\')', 'patient-example.xml', 'dGVzdA=='],
            'testEncodeHex' => ['\'test\'.encode(\'hex\')', 'patient-example.xml', '74657374'],
            'testEncodeBase64B' => ['\'subjects?_d\'.encode(\'base64\')', 'patient-example.xml', 'c3ViamVjdHM/X2Q='],
            'testEncodeUrlBase64' => ['\'subjects?_d\'.encode(\'urlbase64\')', 'patient-example.xml', 'c3ViamVjdHM_X2Q='],
            'testDecodeBase64A' => ['\'dGVzdA==\'.decode(\'base64\')', 'patient-example.xml', 'test'],
            'testDecodeHex' => ['\'74657374\'.decode(\'hex\')', 'patient-example.xml', 'test'],
            'testDecodeBase64B' => ['\'c3ViamVjdHM/X2Q=\'.decode(\'base64\')', 'patient-example.xml', 'subjects?_d'],
            'testDecodeUrlBase64' => ['\'c3ViamVjdHM_X2Q=\'.decode(\'urlbase64\')', 'patient-example.xml', 'subjects?_d'],
            'testEscapeHtml' => ['\'"1<2"\'.escape(\'html\')', 'patient-example.xml', '&quot;1&lt;2&quot;'],
            'testEscapeJson' => ['\'"1<2"\'.escape(\'json\')', 'patient-example.xml', '\"1<2\"'],
            'testUnescapeHtml' => ['\'&quot;1&lt;2&quot;\'.unescape(\'html\')', 'patient-example.xml', '"1<2"'],
            'testUnescapeJson' => ['\'\"1<2\"\'.unescape(\'json\')', 'patient-example.xml', '"1<2"'],
            'testTrim1' => ['\'123456\'.trim().length() = 6', 'patient-example.xml', true],
            'testTrim2' => ['\'123 456\'.trim().length() = 7', 'patient-example.xml', true],
            'testTrim3' => ['\' 123456 \'.trim().length() = 6', 'patient-example.xml', true],
            'testTrim4' => ['\'  \'.trim().length() = 0', 'patient-example.xml', true],
            'testTrim5' => ['{}.trim().empty() = true', 'patient-example.xml', true],
            'testTrim6' => ['\'      \'.trim() = \'\'', 'patient-example.xml', true],
            'testSplit1' => ['\'Peter,James,Jim,Peter,James\'.split(\',\').count() = 5', 'patient-example.xml', true],
            'testSplit2' => ['\'A,,C\'.split(\',\').join(\',\') = \'A,,C\'', 'patient-example.xml', true],
            'testSplit3' => [
                '\'[stop]ONE[stop][stop]TWO[stop][stop][stop]THREE[stop][stop]\'.split(\'[stop]\').trace(\'n\').count() = 7',
                'patient-example.xml',
                true,
            ],
            'testJoin' => ['name.given.join(\',\')', 'patient-example.xml', 'Peter,James,Jim,Peter,James'],
            'testPlusDate1' => ['@1973-12-25 + 7 days', 'patient-example.xml', '1974-01-01'],
            'testPlusDate2' => ['@1973-12-25 + 7.7 days', 'patient-example.xml', '1974-01-01'],
            'testPlusDate3' => [
                '@1973-12-25T00:00:00.000+10:00 + 7 days',
                'patient-example.xml',
                '@1974-01-01T00:00:00.000+10:00',
            ],
            'testPlusDate4' => [
                '@1973-12-25T00:00:00.000+10:00 + 7.7 days',
                'patient-example.xml',
                '@1974-01-01T00:00:00.000+10:00',
            ],
            'testPlusDate5' => [
                '@1973-12-25T00:00:00.000+10:00 + 1 second',
                'patient-example.xml',
                '@1973-12-25T00:00:01.000+10:00',
            ],
            'testPlusDate6' => [
                '@1973-12-25T00:00:00.000+10:00 + 10 millisecond',
                'patient-example.xml',
                '@1973-12-25T00:00:00.010+10:00',
            ],
            'testPlusDate7' => [
                '@1973-12-25T00:00:00.000+10:00 + 1 minute',
                'patient-example.xml',
                '@1973-12-25T00:01:00.000+10:00',
            ],
            'testPlusDate8' => [
                '@1973-12-25T00:00:00.000+10:00 + 1 hour',
                'patient-example.xml',
                '@1973-12-25T01:00:00.000+10:00',
            ],
            'testPlusDate9' => ['@1973-12-25 + 1 day', 'patient-example.xml', '1973-12-26'],
            'testPlusDate10' => ['@1973-12-25 + 1 month', 'patient-example.xml', '1974-01-25'],
            'testPlusDate11' => ['@1973-12-25 + 1 week', 'patient-example.xml', '1974-01-01'],
            'testPlusDate12' => ['@1973-12-25 + 1 year', 'patient-example.xml', '1974-12-25'],
            'testPlusDate13' => ['@1973-12-25 + 1 \'d\'', 'patient-example.xml', '1973-12-26'],
            'testPlusDate15' => ['@1973-12-25 + 1 \'wk\'', 'patient-example.xml', '1974-01-01'],
            'testPlusDate18' => [
                '@1973-12-25T00:00:00.000+10:00 + 1 \'s\'',
                'patient-example.xml',
                '@1973-12-25T00:00:01.000+10:00',
            ],
            'testPlusDate19' => [
                '@1973-12-25T00:00:00.000+10:00 + 0.1 \'s\'',
                'patient-example.xml',
                '@1973-12-25T00:00:00.000+10:00',
            ],
            'testPlusDate20' => [
                '@1973-12-25T00:00:00.000+10:00 + 10 \'ms\'',
                'patient-example.xml',
                '@1973-12-25T00:00:00.010+10:00',
            ],
            'testPlusDate21' => [
                '@1973-12-25T00:00:00.000+10:00 + 1 \'min\'',
                'patient-example.xml',
                '@1973-12-25T00:01:00.000+10:00',
            ],
            'testPlusDate22' => [
                '@1973-12-25T00:00:00.000+10:00 + 1 \'h\'',
                'patient-example.xml',
                '@1973-12-25T01:00:00.000+10:00',
            ],
            'testMinus5' => ['@1974-12-25 - 1 \'month\'', 'patient-example.xml', '1974-11-25'],
            'testPrecedence3' => ['1 > 2 is Boolean', 'patient-example.xml', true],
            'testPrecedence4' => ['1 | 1 is Integer', 'patient-example.xml', true],
            'testVariables4' => [
                '%`vs-administrative-gender` = \'http://hl7.org/fhir/ValueSet/administrative-gender\'',
                'patient-example.xml',
                true,
            ],
            'testExtension2' => ['Patient.birthDate.extension(%`ext-patient-birthTime`).exists()', 'patient-example.xml', true],
            'testConformsTo1' => [
                'conformsTo(\'http://hl7.org/fhir/StructureDefinition/Patient\')',
                'patient-example.xml',
                true,
            ],
            'testConformsTo2' => [
                'conformsTo(\'http://hl7.org/fhir/StructureDefinition/Person\')',
                'patient-example.xml',
                false,
            ],
            'LowBoundaryDecimal' => ['1.587.lowBoundary(8)', 'void', 1.5865],
            'LowBoundaryQuantity' => ['1.587 \'cm\'.lowBoundary(8)', 'void', '1.58650000 \'cm\''],
            'LowBoundaryDateMonth' => ['@2014.lowBoundary(6)', 'void', '2014-01'],
            'LowBoundaryDateTimeMillisecond1' => [
                '@2014-01-01T08:05+08:00.lowBoundary(17)',
                'void',
                '@2014-01-01T08:05:00.000+08:00',
            ],
            'LowBoundaryDateTimeMillisecond2' => ['@2014-01-01T08.lowBoundary(8)', 'void', '2014-01-01'],
            'LowBoundaryTimeMillisecond' => ['@T10:30.lowBoundary(9)', 'void', 'T10:30:00.000'],
            'HighBoundaryDecimal' => ['1.587.highBoundary(8)', 'void', 1.5875],
            'HighBoundaryQuantity' => ['1.587 \'m\'.highBoundary(8)', 'void', '1.58750000 \'m\''],
            'HighBoundaryDateMonth' => ['@2014.highBoundary(6)', 'void', '2014-12'],
            'HighBoundaryDateTimeMillisecond' => ['@2014-01-01T08.highBoundary(17)', 'void', '2014-01-01T08:00:59.999-12:00'],
            'HighBoundaryTimeMillisecond' => ['@T10:30.highBoundary(9)', 'void', 'T10:30:59.999'],
            'Comparable1' => ['1 \'cm\'.comparable(1 \'[in_i]\')', 'void', true],
            'Comparable2' => ['1 \'cm\'.comparable(1 \'[s]\')', 'void', false],
            // https://build.fhir.org/ig/HL7/FHIRPath/#singleton-evaluation-of-collections
            'from-zulip-1' => ['(true and \'foo\').empty()', 'patient-example.xml', false],
            // high and low boundary functions
            'testPeriodInvariantNew' => [
                'Patient.identifier.period.all(start.empty() or end.empty() or (start.lowBoundary() < end.highBoundary()))',
                'patient-example-period.xml',
                true,
            ],
            'testConformsTo3' => ['conformsTo(\'http://trash\')', 'patient-example.xml', false], // execution fail
            'testLiteralTimeUTC' => ['@T14:34:28Z.is(Time)', 'patient-example.xml', false], // execution fail
            'testLiteralTimeTimezoneOffset' => ['@T14:34:28+10:00.is(Time)', 'patient-example.xml', false], // execution fail
        ];
    }

    public function providerExpressionsIncorrect(): array
    {
        return [
            'testLiteralTimeUTC' => ['@T14:34:28Z.is(Time)', 'patient-example', []], // TODO : not a time -> error ? : path error -> empty ?
            'testLiteralTimeTimezoneOffset' => ['@T14:34:28+10:00.is(Time)', 'patient-example', []], // same
            // $this refers to HumanName ? to check
            'testSubSetOf1' => ['Patient.name.first().subsetOf($this.name)', 'patient-example.xml', true],
            'testSubSetOf2' => ['Patient.name.subsetOf($this.name.first()).not()', 'patient-example.xml', true],
            'testSubSetOf3' => [
                'supportingInfo.where(category.coding.code = \'additionalbodysite\').sequence.subsetOf($this.item.informationSequence)',
                'explanationofbenefit-example.json',
                true,
            ],
            'testSuperSetOf1' => ['Patient.name.first().supersetOf($this.name).not()', 'patient-example.xml', true],
            'testSuperSetOf2' => ['Patient.name.supersetOf($this.name.first())', 'patient-example.xml', true],
            'testCombine' => [
                'concept.code.combine($this.descendants().concept.code).isDistinct()',
                'codesystem-example.xml',
                false,
            ],
            'testMatchesFullWithinUrl1' => [
                '\'http://fhir.org/guides/cqf/common/Library/FHIR-ModelInfo|4.0.1\'.matchesFull(\'library\')',
                'void',
                false,
            ],
            'testMatchesFullWithinUrl3' => [
                '\'http://fhir.org/guides/cqf/common/Library/FHIR-ModelInfo|4.0.1\'.matchesFull(\'Library\')',
                'void',
                false,
            ],
            'testMatchesFullWithinUrl4' => [
                '\'http://fhir.org/guides/cqf/common/Library/FHIR-ModelInfo|4.0.1\'.matchesFull(\'^Library$\')',
                'void',
                false,
            ],
            'testMatchesFullWithinUrl1a' => [
                '\'http://fhir.org/guides/cqf/common/Library/FHIR-ModelInfo|4.0.1\'.matchesFull(\'.*Library.*\')',
                'void',
                true,
            ],
            'testMatchesFullWithinUrl2' => [
                '\'http://fhir.org/guides/cqf/common/Library/FHIR-ModelInfo|4.0.1\'.matchesFull(\'Measure\')',
                'void',
                false,
            ],
            'PrecisionDecimal' => ['1.58700.precision()', 'void', 5],
            'PrecisionYear' => ['@2014.precision()', 'void', 4],
            'PrecisionDateTimeMilliseconds' => ['@2014-01-05T10:30:00.000.precision()', 'void', 17],
            'PrecisionTimeMinutes' => ['@T10:30.precision()', 'void', 4],
            'PrecisionTimeMilliseconds' => ['@T10:30:00.000.precision()', 'void', 9],
            'testFHIRPathAsFunction11' => ['Patient.gender.as(string)', 'patient-example.xml', []], // should work
        ];
    }

    /**
     * @dataProvider providerExpressions
     * @param string $expression
     * @param string $resource
     * @param array $output
     * @return void
     * @throws Exception
     */
    public function testExpressions(string $expression, string $resourceName, mixed $expected): void
    {
        $resource = self::$resources[$resourceName];

        $fp = new FHIRPath();

        if (is_array($expected)) {
            $value = $fp->apply($resource, $expression);
        } elseif (is_bool($expected)) {
            $value = $fp->predicate($resource, $expression);
        } else {
            $result = $fp->apply($resource, $expression);
            if (!($first = reset($result))) {
                $this->fail("Expected output, got empty.");
            }
            if (str_starts_with($expected, '#Q')) {
                /** @var FHIRQuantityInterface $first */
                $value = $first->getValue()->getValue() . " '" . $first->getUnit()->getValue() . "'";
                $expected = substr($expected, 2);
            } else {
                $value = $first->getvalue();
            }
        }

        $this->assertSame($expected, $value);
    }

    /**
     * @dataProvider providerExpressionsExecutionFail
     * @param string $expression
     * @param string $resource
     * @param array $output
     * @return void
     * @throws Exception
     */
    public function testErrorAtExecution(string $expression, string $resourceName): void
    {
        $resource = self::$resources[$resourceName];
        $fp = new FHIRPath();

        $this->expectException(Exception::class);
        $fp->apply($resource, $expression);
    }

    /**
     * @dataProvider providerExpressionsToImplement
     * @param string $expression
     * @param string $resource
     * @param array $output
     * @return void
     * @throws Exception
     */
    public function testToImplement(string $expression, string $resourceName, mixed $output): void
    {
        $this->markTestSkipped();

        $resource = self::$resources[$resourceName];

        $fp = new FHIRPath();

        if (is_array($expected)) {
            $value = $fp->apply($resource, $expression);
        } elseif (is_bool($expected)) {
            $value = $fp->predicate($resource, $expression);
        } else {
            $result = $fp->apply($resource, $expression);
            if (!($first = reset($result))) {
                $this->fail("Expected output, got empty.");
            }
            if (str_starts_with($expected, '#Q')) {
                /** @var FHIRQuantity $first */
                $value = $first->getValue()->getValue() . " '" . $first->getUnit()->getValue() . "'";
                $expected = substr($expected, 2);
            } else {
                $value = $first->getvalue();
            }
        }

        $this->assertSame($expected, $value);
    }

    /**
     * @dataProvider providerExpressionsIncorrect
     * @param string $expression
     * @param string $resource
     * @param array $output
     * @return void
     * @throws Exception
     */
    public function testSeemsIncorrect(string $expression, string $resourceName, mixed $output): void
    {
        // TODO : theses test were given but don't seems to respect the specification
        $this->markTestSkipped();

        $resource = self::$resources[$resourceName];

        $fp = new FHIRPath();

        if (is_array($expected)) {
            $value = $fp->apply($resource, $expression);
        } elseif (is_bool($expected)) {
            $value = $fp->predicate($resource, $expression);
        } else {
            $result = $fp->apply($resource, $expression);
            if (!($first = reset($result))) {
                $this->fail("Expected output, got empty.");
            }
            if (str_starts_with($expected, '#Q')) {
                /** @var FHIRQuantity $first */
                $value = $first->getValue()->getValue() . " '" . $first->getUnit()->getValue() . "'";
                $expected = substr($expected, 2);
            } else {
                $value = $first->getvalue();
            }
        }

        $this->assertSame($expected, $value);
    }
}
