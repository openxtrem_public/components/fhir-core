<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Tests\Parser;

use Ox\Components\FHIRCore\Enum\FHIRVersion;

class R4XMLParserTest extends AbstractXMLParserTest
{
    final protected const FHIR_VERSION = FHIRVersion::R4;
}
