<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Tests\Parser;

use Ox\Components\FHIRCore\Enum\FHIRVersion;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRExtensionInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRHumanNameInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRBooleanInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRDateInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRIntegerInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRStringInterface;
use Ox\Components\FHIRCore\Interfaces\Resources\FHIRPatientInterface;
use Ox\Components\FHIRCore\Parser\Exception\NoElementException;
use Ox\Components\FHIRCore\Parser\JSONParser;
use Ox\Components\FHIRCore\Serializer\Exception\SerializerException;
use PHPUnit\Framework\TestCase;

/**
 * Description
 */
abstract class AbstractJSONParserTest extends TestCase
{
    protected const FHIR_VERSION = FHIRVersion::R4;

    /**
     * @throws NoElementException
     */
    public function testGoodClassCreated(): void
    {
        $input = '{
        "resourceType": "Patient"
        }';

        $parser = new JSONParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertInstanceOf(FHIRPatientInterface::class, $out);
    }

    /**
     * @depends testGoodClassCreated
     * @throws NoElementException
     */
    public function testIDIsDatatype(): void
    {
        $input = '{
        "resourceType": "Patient",
        "id": "myID"
        }';

        $parser = new JSONParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertInstanceOf(FHIRStringInterface::class, $out->getId());
    }

    /**
     * @depends testGoodClassCreated
     * @depends testIDIsDatatype
     * @return void
     * @throws NoElementException
     */
    public function testIDOfResource(): void
    {
        $input = '{
        "resourceType": "Patient",
        "id": "myID"
        }';

        $parser = new JSONParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertSame("myID", $out->getId()->getValue());
    }

    /**
     * @depends testGoodClassCreated
     * @throws NoElementException
     */
    public function testResourceFieldSet(): void
    {
        $input = '{
        "resourceType": "Patient",
        "active": "true"
        }';

        $parser = new JSONParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertNotNull($out->getActive());
    }

    /**
     * @depends testGoodClassCreated
     * @throws NoElementException
     */
    public function testResourceFieldAdd(): void
    {
        $input = '{
        "resourceType": "Patient",
        "name": [{
            "given": [
                "name1",
                "name2",
                "name3"
                ]
            }]
        }';

        $parser = new JSONParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertCount(3, $out->getName()[0]->getGiven());
    }

    /**
     * @depends testIDIsDatatype
     * @depends testGoodClassCreated
     * @return void
     * @throws NoElementException
     */
    public function testIDOfDatatype(): void
    {
        $input = '{
        "resourceType": "Patient",
        "name": [{
            "id": "myID",
            "given": ["name"]
            }
        ]
        }';

        $parser = new JSONParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertSame("myID", $out->getName()[0]->getId()->getValue());
    }

    /**
     * @depends testIDIsDatatype
     * @depends testGoodClassCreated
     * @return void
     * @throws NoElementException
     */
    public function testPrepend(): void
    {
        $input = '{
        "resourceType": "Patient",
        "active": "true",
        "_active": {
            "id": "myID",
            "extension": [{"url":"myUrl"}]
            }
        }';

        $parser = new JSONParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertSame("myID", $out->getActive()->getId()->getValue());
        $this->assertSame("myUrl", $out->getActive()->getExtension()[0]->getUrl()->getValue());
    }

    /**
     * @depends testIDIsDatatype
     * @depends testGoodClassCreated
     * @return void
     * @throws NoElementException
     */
    public function testIDInPrependWithNoBaseValue(): void
    {
        $input = '{
        "resourceType": "Patient",
        "_active": {
            "id": "myID"
            }
        }';

        $parser = new JSONParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertSame("myID", $out->getActive()->getId()->getValue());
    }

    /**
     * @depends testIDIsDatatype
     * @depends testResourceFieldAdd
     * @return void
     * @throws NoElementException
     */
    public function testPrependOfMultiple(): void
    {
        $input = '{
        "resourceType": "Patient",
        "name": [
            {
                "given": [
                    "name1",
                    null,
                    "name3"
                ],
                "_given": [
                    null,
                    {"id": "myID2"},
                    {
                        "id": "myID3",
                        "extension": [{"url":"myUrl"}]
                    }
                ]
            }
        ]
        }';

        $parser = new JSONParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertSame("myID2", $out->getName()[0]->getGiven()[1]->getId()->getValue());
        $this->assertSame("name1", $out->getName()[0]->getGiven()[0]->getValue());
        $this->assertSame("myUrl", $out->getName()[0]->getGiven()[2]->getExtension()[0]->getUrl()->getValue());
        $this->assertNull($out->getName()[0]->getGiven()[0]->getId());
        $this->assertEmpty($out->getName()[0]->getGiven()[0]->getExtension());
        $this->assertNull($out->getName()[0]->getGiven()[1]->getValue());
    }

    /**
     * @depends testIDIsDatatype
     * @depends testResourceFieldAdd
     * @return void
     * @throws NoElementException
     */
    public function testPrependOfMultipleWithNoBaseValue(): void
    {
        $input = '{
        "resourceType": "Patient",
        "name": [
            {
                "_given": [
                    {"id": "myID1"},
                    {
                        "id": "myID2",
                        "extension": [{"url":"myUrl"}]
                    }
                ]
            }
        ]
        }';

        $parser = new JSONParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertSame("myID1", $out->getName()[0]->getGiven()[0]->getId()->getValue());
        $this->assertSame("myUrl", $out->getName()[0]->getGiven()[1]->getExtension()[0]->getUrl()->getValue());
        $this->assertNull($out->getName()[0]->getGiven()[0]->getValue());
        $this->assertNull($out->getName()[0]->getGiven()[1]->getValue());
    }

    /**
     * @depends testResourceFieldSet
     * @depends testResourceFieldAdd
     * @return void
     * @throws NoElementException
     */
    public function testBackboneElementCreated(): void
    {
        $input = '{
        "resourceType": "Patient",
        "contact": [{
            "name": {
                "given": [
                    "Bénédicte"
                    ]
                }
            }]
        }';

        $parser = new JSONParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertSame("Bénédicte", $out->getContact()[0]->getName()->getGiven()[0]->getValue());
    }

    /**
     * @depends testResourceFieldSet
     * @depends testResourceFieldAdd
     * @return void
     * @throws NoElementException
     */
    public function testExtensionAdded(): void
    {
        $input = '{
        "resourceType": "Patient",
        "active": "true",
        "_active": {
            "extension": [
                    {
                        "url": "myUrl",
                        "valueString": "myValue"
                    }
                ]
            }
        }';

        $parser = new JSONParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertInstanceOf(FHIRExtensionInterface::class, $out->getActive()->getExtension()[0]);
    }

    /**
     * @depends testExtensionAdded
     * @return void
     * @throws NoElementException
     */
    public function testUrlOfExtensionAreDatatype(): void
    {
        $input = '{
        "resourceType": "Patient",
        "active": "true",
        "_active": {
            "extension": [{
                "url": "myUrl",
                "valueString": "myValue"
                }]
            }
        }';

        $parser = new JSONParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertInstanceOf(FHIRStringInterface::class, $out->getActive()->getExtension()[0]->getUrl());
    }

    /**
     * @depends      testExtensionAdded
     * @dataProvider providerMultipleTypeType
     * @throws NoElementException
     */
    public function testMultipleTypeElementsAreAdded(string $value, string $expected): void
    {
        $input = '{
        "resourceType": "Patient",
        "extension": [{
            "url": "myUrl",
            ' . $value . '
            }]
        }';

        $parser = new JSONParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertInstanceOf($expected, $out->getExtension()[0]->getValue());
    }

    public function providerMultipleTypeType(): array
    {
        return [
            ['"valueInteger": "1"', FHIRIntegerInterface::class],
            ['"valueBoolean": "true"', FHIRBooleanInterface::class],
            ['"valueString": "myValue"', FHIRStringInterface::class],
            ['"valueDate": "2022-12-30"', FHIRDateInterface::class],
            ['"valueHumanName": {"given":["name1"]}', FHIRHumanNameInterface::class],
        ];
    }

    /**
     * @depends testResourceFieldSet
     *
     * @return void
     * @throws NoElementException
     */
    public function testBooleanValuesTrue(): void
    {
        $input = '{
        "resourceType": "Patient",
        "active": true
        }';

        $parser = new JSONParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertTrue($out->getActive()->getValue());
    }

    /**
     * @depends testResourceFieldSet
     * @return void
     * @throws NoElementException
     */
    public function testBooleanValuesFalse(): void
    {
        $input = '{
        "resourceType": "Patient",
        "active": false
        }';

        $parser = new JSONParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertNotTrue($out->getActive()->getValue());
    }

    /**
     * @depends testResourceFieldAdd
     * @return void
     * @throws NoElementException
     */
    public function testStringValue(): void
    {
        $input = '{
        "resourceType": "Patient",
        "name": [{
            "given": [
                "myNameéàç02"
                ]
            }]
        }';

        $parser = new JSONParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertSame('myNameéàç02', $out->getName()[0]->getGiven()[0]->getValue());
    }

    /**
     * @depends      testResourceFieldSet
     * @depends      testMultipleTypeElementsAreAdded
     * @dataProvider providerIntegers
     *
     * @param int $integer
     *
     * @return void
     * @throws NoElementException
     */
    public function testIntegerValues(int $integer): void
    {
        $input = '{
        "resourceType": "Patient",
        "multipleBirthInteger": ' . $integer . '
        }';

        $parser = new JSONParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertSame($integer, $out->getMultipleBirth()->getValue());
    }

    public function providerIntegers(): array
    {
        return [
            [1],
            [0],
            [-3],
            [7],
        ];
    }

    /**
     * @depends      testMultipleTypeElementsAreAdded
     * @dataProvider providerFloat
     *
     * @param float $value
     *
     * @return void
     * @throws NoElementException
     */
    public function testFloatValues(float $value): void
    {
        $input = '{
        "resourceType": "Patient",
        "extension": [{
            "valueDecimal": ' . $value . '
            }]
        }';

        $parser = new JSONParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertSame($value, $out->getExtension()[0]->getValue()->getValue());
    }

    public function providerFloat(): array
    {
        return [
            [1.2],
            [0.0],
            [-3.456],
            [7],
        ];
    }

    /**
     * @depends testResourceFieldAdd
     * @return void
     * @throws NoElementException
     */
    public function testInvalidElementNameThrowException(): void
    {
        $input = '{
        "resourceType": "Patient",
        "preferred": "true"
        }';

        $parser = new JSONParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertEmpty($out->getCommunication());

        $this->expectError();
        $out->getPreferred();
    }

    /**
     * @depends testResourceFieldAdd
     * @return void
     * @throws NoElementException
     */
    public function testContainedResource(): void
    {
        $input = '{
        "resourceType": "Patient",
        "contained": [
            {
                "resourceType": "Patient",
                "name": [
                    {
                    "given": [
                        "name1",
                        "name2",
                        "name3"
                     ]
                    }
                ]
            }
        ]
        }';

        $parser = new JSONParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertInstanceOf(FHIRPatientInterface::class, $out->getContained()[0]);
    }

    /**
     * @throws SerializerException|NoElementException
     */
    public function testBundleCreation(): void
    {
        $input = '{
        "resourceType": "Bundle",
        "entry": [
            {
                "resource": {
                    "resourceType": "Patient",
                    "id": "id",
                    "active": true
                }
            }
        ]
        }';

        $parser = new JSONParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertInstanceOf(FHIRPatientInterface::class, $out->getEntry()[0]->getResource());
        $this->assertSame('id', $out->getEntry()[0]->getResource()->getId()->getValue());
    }

    /**
     * @throws SerializerException|NoElementException
     */
    public function testXHtmlElementsHasAllDiv(): void
    {
        $input = '{
            "resourceType": "Patient",
            "text": {
                "status": "generated",
                "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\"><p>ACTIVE</p></div>"
            },
            "active": "true"
        }';

        $parser = new JSONParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertSame(
            '<div xmlns="http://www.w3.org/1999/xhtml"><p>ACTIVE</p></div>',
            $out->getText()->getDiv()->getValue()
        );
    }
}
