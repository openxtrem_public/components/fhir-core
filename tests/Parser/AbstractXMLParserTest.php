<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Tests\Parser;

use Ox\Components\FHIRCore\Enum\FHIRVersion;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRExtensionInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRHumanNameInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRBooleanInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRDateInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRIntegerInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRStringInterface;
use Ox\Components\FHIRCore\Interfaces\Resources\FHIRPatientInterface;
use Ox\Components\FHIRCore\Parser\Exception\NoElementException;
use Ox\Components\FHIRCore\Parser\Exception\ParserException;
use Ox\Components\FHIRCore\Parser\XMLParser;
use Ox\Components\FHIRCore\Serializer\Exception\SerializerException;
use PHPUnit\Framework\TestCase;

/**
 * Description
 */
abstract class AbstractXMLParserTest extends TestCase
{
    protected const FHIR_VERSION = FHIRVersion::R4;

    /**
     * @throws NoElementException
     * @throws ParserException
     */
    public function testGoodClassCreated(): void
    {
        $input = '<?xml version="1.0" encoding="UTF-8"?>
<Patient>
</Patient>';

        $parser = new XMLParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertInstanceOf(FHIRPatientInterface::class, $out);
    }

    /**
     * @depends testGoodClassCreated
     * @throws NoElementException
     * @throws ParserException
     */
    public function testIDIsDatatype(): void
    {
        $input = '<?xml version="1.0" encoding="UTF-8"?>
<Patient>
  <id value="myID"/>
</Patient>';

        $parser = new XMLParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertInstanceOf(FHIRStringInterface::class, $out->getId());
    }

    /**
     * @depends testGoodClassCreated
     * @depends testIDIsDatatype
     * @return void
     * @throws NoElementException
     * @throws ParserException
     */
    public function testIDOfResource(): void
    {
        $input = '<?xml version="1.0" encoding="UTF-8"?>
<Patient>
  <id value="myID"/>
</Patient>';

        $parser = new XMLParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertSame("myID", $out->getId()->getValue());
    }

    /**
     * @depends testGoodClassCreated
     * @throws NoElementException
     * @throws ParserException
     */
    public function testResourceFieldSet(): void
    {
        $input = '<?xml version="1.0" encoding="UTF-8"?>
<Patient>
  <active value="true"/>
</Patient>';

        $parser = new XMLParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertNotNull($out->getActive());
    }

    /**
     * @depends testGoodClassCreated
     * @throws NoElementException
     * @throws ParserException
     */
    public function testResourceFieldAdd(): void
    {
        $input = '<?xml version="1.0" encoding="UTF-8"?>
<Patient>
  <name>
    <given value="nom1"/>
    <given value="nom2"/>
    <given value="nom3"/>
  </name>
</Patient>';

        $parser = new XMLParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertCount(3, $out->getName()[0]->getGiven());
    }

    /**
     * @depends testIDIsDatatype
     * @depends testGoodClassCreated
     * @return void
     * @throws NoElementException
     * @throws ParserException
     */
    public function testIDOfDatatype(): void
    {
        $input = '<?xml version="1.0" encoding="UTF-8"?>
<Patient>
  <active id="myID" value="true"/>
</Patient>';

        $parser = new XMLParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertSame("myID", $out->getActive()->getId()->getValue());
    }

    /**
     * @depends testResourceFieldSet
     * @depends testResourceFieldAdd
     * @return void
     * @throws NoElementException
     * @throws ParserException
     */
    public function testBackboneElementCreated(): void
    {
        $input = '<?xml version="1.0" encoding="UTF-8"?>
<Patient>
  <contact>
    <name>
      <given value="Bénédicte"/>
    </name>
  </contact>
</Patient>';

        $parser = new XMLParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertSame("Bénédicte", $out->getContact()[0]->getName()->getGiven()[0]->getValue());
    }

    /**
     * @depends testResourceFieldSet
     * @depends testResourceFieldAdd
     * @return void
     * @throws NoElementException
     * @throws ParserException
     */
    public function testExtensionAdded(): void
    {
        $input = '<?xml version="1.0" encoding="UTF-8"?>
<Patient>
  <active>
      <extension url="https://url">
          <valueCode value="archived"/>
      </extension>
  </active>
</Patient>';

        $parser = new XMLParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertInstanceOf(FHIRExtensionInterface::class, $out->getActive()->getExtension()[0]);
    }

    /**
     * @depends testExtensionAdded
     * @return void
     * @throws NoElementException
     * @throws ParserException
     */
    public function testUrlOfExtensionAreDatatype(): void
    {
        $input = '<?xml version="1.0" encoding="UTF-8"?>
<Patient>
  <active>
      <extension url="https://url">
          <valueCode value="archived"/>
      </extension>
  </active>
</Patient>';

        $parser = new XMLParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertInstanceOf(FHIRStringInterface::class, $out->getActive()->getExtension()[0]->getUrl());
    }

    /**
     * @depends      testExtensionAdded
     * @dataProvider providerMultipleTypeType
     *
     * @param $value
     * @param $expected
     *
     * @return void
     * @throws NoElementException
     * @throws ParserException
     */
    public function testMultipleTypeElementsAreAdded($value, $expected): void
    {
        $input = '<?xml version="1.0" encoding="UTF-8"?>
<Patient>
  <extension url="myUrl">
    ' . $value . '
  </extension>
</Patient>';

        $parser = new XMLParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertInstanceOf($expected, $out->getExtension()[0]->getValue());
    }

    public function providerMultipleTypeType(): array
    {
        return [
            ['<valueInteger value="1"/>', FHIRIntegerInterface::class],
            ['<valueBoolean value="true"/>', FHIRBooleanInterface::class],
            ['<valueString value="myStr"/>', FHIRStringInterface::class],
            ['<valueDate value="2022-12-30"/>', FHIRDateInterface::class],
            ['<valueHumanName><given value="myName"/></valueHumanName>', FHIRHumanNameInterface::class],
        ];
    }

    /**
     * @depends testResourceFieldSet
     * @return void
     * @throws NoElementException
     * @throws ParserException
     */
    public function testBooleanValuesTrue(): void
    {
        $input = '<?xml version="1.0" encoding="UTF-8"?>
<Patient>
  <active value="true"/>
</Patient>';

        $parser = new XMLParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertTrue($out->getActive()->getValue());
    }

    /**
     * @depends testResourceFieldSet
     * @return void
     * @throws NoElementException
     * @throws ParserException
     */
    public function testBooleanValuesFalse(): void
    {
        $input = '<?xml version="1.0" encoding="UTF-8"?>
<Patient>
  <active value="False"/>
</Patient>';

        $parser = new XMLParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertNotTrue($out->getActive()->getValue());
    }

    /**
     * @depends testResourceFieldAdd
     * @return void
     * @throws NoElementException
     * @throws ParserException
     */
    public function testStringValue(): void
    {
        $input = '<?xml version="1.0" encoding="UTF-8"?>
<Patient>
  <name>
    <given value="myName"/>
  </name>
</Patient>';

        $parser = new XMLParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertSame('myName', $out->getName()[0]->getGiven()[0]->getValue());
    }

    /**
     * @depends      testResourceFieldSet
     * @depends      testMultipleTypeElementsAreAdded
     * @dataProvider providerIntegers
     *
     * @param int $integer
     *
     * @return void
     * @throws NoElementException
     * @throws ParserException
     */
    public function testIntegerValues(int $integer): void
    {
        $input = '<?xml version="1.0" encoding="UTF-8"?>
<Patient>
  <multipleBirthInteger value="' . $integer . '"/>
</Patient>';

        $parser = new XMLParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertSame($integer, $out->getMultipleBirth()->getValue());
    }

    public function providerIntegers(): array
    {
        return [
            [1],
            [0],
            [-3],
            [7],
        ];
    }

    /**
     * @depends      testMultipleTypeElementsAreAdded
     * @dataProvider providerFloat
     *
     * @param int $integer
     *
     * @return void
     * @throws NoElementException
     * @throws ParserException
     */
    public function testFloatValues(float $value): void
    {
        $input = '<?xml version="1.0" encoding="UTF-8"?>
<Patient>
  <extension>
    <valueDecimal value="' . $value . '"/>
  </extension>
</Patient>';

        $parser = new XMLParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertSame($value, $out->getExtension()[0]->getValue()->getValue());
    }

    public function providerFloat(): array
    {
        return [
            [1.2],
            [0.0],
            [-3.456],
            [7],
        ];
    }

    /**
     * @depends testResourceFieldAdd
     * @return void
     * @throws NoElementException
     * @throws ParserException
     */
    public function testInvalidElementNameThrowException(): void
    {
        $input = '<?xml version="1.0" encoding="UTF-8"?>
<Patient>
  <active custom="true"/>
  <preferred value="true"/>
</Patient>';

        $parser = new XMLParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertEmpty($out->getCommunication());

        $this->expectError();
        $out->getPreferred();

        $this->assertNull($out->getActive());
    }

    /**
     * @depends testResourceFieldAdd
     * @return void
     * @throws NoElementException
     * @throws ParserException
     */
    public function testContainedResource(): void
    {
        $input = '<?xml version="1.0" encoding="UTF-8"?>
<Patient>
  <contained>
    <Patient>
      <name>
        <given value="myName"/>
      </name>
    </Patient>
  </contained>
</Patient>';

        $parser = new XMLParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertInstanceOf(FHIRPatientInterface::class, $out->getContained()[0]);
    }

    /**
     * @throws SerializerException
     * @throws NoElementException
     * @throws ParserException
     */
    public function testBundleCreation(): void
    {
        $input = '<?xml version="1.0" encoding="UTF-8"?>
<Bundle xmlns="http://hl7.org/fhir">
  <entry>
    <resource>
      <Patient>
        <id value="id"/>
        <active value="true"/>
      </Patient>
    </resource>
  </entry>
</Bundle>';

        $parser = new XMLParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertInstanceOf(FHIRPatientInterface::class, $out->getEntry()[0]->getResource());
        $this->assertSame('id', $out->getEntry()[0]->getResource()->getId()->getValue());
    }

    /**
     * @throws SerializerException
     * @throws NoElementException
     * @throws ParserException
     */
    public function testXHtmlElementsHasAllDiv(): void
    {
        $input = '<?xml version="1.0" encoding="UTF-8"?>
<Patient xmlns="http://hl7.org/fhir">
  <text>
    <status value="generated"/>
    <div xmlns="http://www.w3.org/1999/xhtml"><p>ACTIVE</p></div>
  </text>
  <active value="true"/>
</Patient>';

        $parser = new XMLParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertSame(
            '<div xmlns="http://www.w3.org/1999/xhtml"><p>ACTIVE</p></div>',
            $out->getText()->getDiv()->getValue()
        );
    }

    /**
     * @throws SerializerException
     * @throws NoElementException
     * @throws ParserException
     */
    public function testXHtmlContainedElementsHasAllDiv(): void
    {
        $input = '<?xml version="1.0" encoding="UTF-8"?>
<!-- First comment -->
<Bundle xmlns="http://hl7.org/fhir">
  <entry>
    <fullUrl value="urn:uuid:61ebe359-bfdc-4613-8bf2-c5e300945f0a"/>
    <resource>
      <Patient>
        <text>
          <status value="generated"/>
          <div xmlns="http://www.w3.org/1999/xhtml">Some narrative</div>
        </text>
        <active value="true"/>
      </Patient>
    </resource>
  </entry>
</Bundle>';

        $parser = new XMLParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertSame(
            '<div xmlns="http://www.w3.org/1999/xhtml">Some narrative</div>',
            $out->getEntry()[0]->getResource()->getText()->getDiv()->getValue()
        );
    }

    /**
     * @throws SerializerException
     * @throws NoElementException
     * @throws ParserException
     */
    public function testIgnoreComments(): void
    {
        $input = '<?xml version="1.0" encoding="UTF-8"?>
<!-- First comment -->
<Bundle xmlns="http://hl7.org/fhir">
  <!--   middle comment   -->
  <entry>
    <fullUrl value="urn:uuid:61ebe359-bfdc-4613-8bf2-c5e300945f0a"/>
    <resource>
      <!--   a simple create operation   -->
      <Patient>
        <!--   middle comment 2   -->
        <text>
          <status value="generated"/>
          <div xmlns="http://www.w3.org/1999/xhtml">Some narrative</div>
        </text>
        <active value="true"/>
      </Patient>
    </resource>
  </entry>
</Bundle>
<!-- Last comment -->';

        $parser = new XMLParser(static::FHIR_VERSION);
        $out    = $parser->parseResource($input);

        $this->assertNotNull($out);
        $this->assertTrue($out->getEntry()[0]->getResource()->getActive()->getValue());
    }
}
