<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Tests\Trait;

use Exception;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRBaseInterface;
use Throwable;

/**
 * Description
 */
trait MultiVersionsHelperTrait
{
    /**
     * @throws Exception
     */
    public static function getResource(string $resource_name): FHIRBaseInterface
    {
        try {
            return new (static::STRUCTURE_DEFINITION::DEFINITIONS[$resource_name]['object'])();
        } catch (Throwable) {
            throw new Exception('Invalid Structure definition for tests.');
        }
    }
}
