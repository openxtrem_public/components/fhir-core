<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Tests\Serializer\JSON;

use Exception;
use Ox\Components\FHIRCore\Serializer\Exception\SerializerException;
use Ox\Components\FHIRCore\Serializer\JSONSerializer;
use Ox\Components\FHIRCore\Tests\Trait\MultiVersionsHelperTrait;
use PHPUnit\Framework\TestCase;

/**
 * Description
 */
abstract class AbstractJSONSerializerTest extends TestCase
{
    use MultiVersionsHelperTrait;

    public static JSONSerializer $serializer;
    public static array          $objects;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$serializer = new JSONSerializer();
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testFormatOutput(): void
    {
        $formattedSerializer = new JSONSerializer();

        $p = $this->getResource('Patient')
                  ->setActive(true);

        $formatted  = $formattedSerializer->serializeResource($p);
        $nFormatted = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertNotSame($formatted, $nFormatted);
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testCaseSensitive(): void
    {
        $p = $this->getResource('Patient')
                  ->setActive(true);

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertStringContainsString('Patient', $out);
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testMultipleSameElementAsArray(): void
    {
        $p  = $this->getResource('Patient');
        $hn = $this->getResource('HumanName')
                   ->addGiven('b')
                   ->addGiven('z')
                   ->addGiven('a')
                   ->addGiven('c');
        $p->setName($hn);

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertMatchesRegularExpression(
            '/\[[abcz, "]*]/',
            $out
        );
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testMultipleSameElementButUniqueAsArray(): void
    {
        $p  = $this->getResource('Patient');
        $hn = $this->getResource('HumanName')
                   ->addGiven('b');
        $p->setName($hn);

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertMatchesRegularExpression(
            '/"name":\[/',
            $out
        );
    }

    /**
     * @dataProvider providerSetterAndValues
     *
     * @param string $setter
     * @param mixed  $value
     * @param string $expected
     *
     * @return void
     * @throws SerializerException
     * @throws Exception
     */
    public function testValueOfPrimitiveFormatted(string $setter, mixed $value, string $expected): void
    {
        $set = 'set' . ucfirst($setter);
        $p   = $this->getResource('Patient')
                    ->$set(
                        $value
                    );

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertMatchesRegularExpression("/\"$setter\w*\":$expected/", $out);
    }

    /**
     * @throws Exception
     */
    public function providerSetterAndValues(): array
    {
        $int  = $this->getResource('integer')
                     ->setValue(7);
        $zero = $this->getResource('integer')
                     ->setValue(0);

        return [
            ['active', true, 'true'],
            ['active', false, 'false'],
            ['gender', 'male', '"male"'],
            ['multipleBirth', $int, '7'],
            ['multipleBirth', $zero, '0'],
        ];
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testExtensionOfPrimitiveElementsAreInUnderscore(): void
    {
        $str = $this->getResource('string')
                    ->setValue('extValue');
        $ext = $this->getResource('Extension')
                    ->setValue($str)
                    ->setUrl('myUrl');

        $b = $this->getResource('boolean')
                  ->setValue(true)
                  ->setExtension($ext);

        $p = $this->getResource('Patient')
                  ->setActive($b);

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertStringContainsString('_active":{"extension":', $out);
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testExtensionOfPrimitiveMultipleElementsAreInUnderscore(): void
    {
        $str = $this->getResource('string')
                    ->setValue('extValue');
        $ext = $this->getResource('Extension')
                    ->setValue($str)
                    ->setUrl('myUrl');

        $strName = $this->getResource('string')
                        ->setValue('name2')->setExtension($ext)->setId('nameID');

        $hn = $this->getResource('HumanName')
                   ->addGiven('name1', $strName, 'name3');

        $p = $this->getResource('Patient')
                  ->setName($hn);

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertMatchesRegularExpression('/"_given":\[[^[]*"extension":/', $out);
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testPrimitiveElementWithOnlyExtensionHasOnlyUnderscore(): void
    {
        $str = $this->getResource('string')
                    ->setValue('extValue');

        $ext = $this->getResource('Extension')
                    ->setUrl('myUrl')
                    ->setValue($str);

        $dt = $this->getResource('dateTime')
                   ->setExtension($ext);

        $p = $this->getResource('Patient')
                  ->setDeceased($dt);

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertStringNotContainsString('"deceased"', $out);
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testExtensionOfNonPrimitiveElementsHasNotUnderscore(): void
    {
        $str = $this->getResource('string')
                    ->setValue('extValue');

        $ext = $this->getResource('Extension')
                    ->setUrl('myUrl')
                    ->setValue($str);

        $hn = $this->getResource('HumanName')
                   ->setGiven('myName')
                   ->setExtension($ext);

        $p = $this->getResource('Patient')
                  ->setName($hn);


        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertStringNotContainsString('_name', $out);
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testIdOfPrimitiveElementsAreInUnderscore(): void
    {
        $b = $this->getResource('boolean')
                  ->setValue(true)
                  ->setId('booleanID');

        $p = $this->getResource('Patient')
                  ->setActive($b);

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertStringContainsString('"_active":{"id":"booleanID"', $out);
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testElementWithUnderscoreAndNoValueAreNotCreated(): void
    {
        $b = $this->getResource('boolean')
                  ->setId('booleanID');

        $p = $this->getResource('Patient')
                  ->setActive($b);

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);
        $this->assertStringContainsString('"_active":{"id":"booleanID"}', $out);
        $this->assertStringNotContainsString('"active"', $out);
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testIdOfNonPrimitiveElementsAreNotInUnderscore(): void
    {
        $hn = $this->getResource('HumanName')
                   ->setGiven('myName')
                   ->setId('myID');

        $p = $this->getResource('Patient')
                  ->setName($hn);


        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertStringNotContainsString('_name', $out);
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testUnderscoreIsSameSizeAsOriginal(): void
    {
        $s1 = $this->getResource('string')
                   ->setValue('name1')
                   ->setId('id1');

        $s2 = $this->getResource('string')
                   ->setValue('name2');

        $s3 = $this->getResource('string')
                   ->setValue('name3');

        $s4 = $this->getResource('string')
                   ->setId('id4');

        $hn = $this->getResource('HumanName')
                   ->addGiven($s1, $s2, $s3, $s4);

        $p = $this->getResource('Patient')
                  ->setName($hn);

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertMatchesRegularExpression('/"given":\[(("name[123]"|null),?){4}]/', $out);
        $this->assertMatchesRegularExpression('/"_given":\[((\{"id":"id[14]"}|null),?){4}]/', $out);
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testExtensionOfPrimitiveElementsAreInUnderscoreWithID(): void
    {
        $str = $this->getResource('string')
                    ->setValue('extValue');

        $ext = $this->getResource('Extension')
                    ->setValue($str)
                    ->setUrl('myUrl');

        $b = $this->getResource('boolean')
                  ->setValue(true)
                  ->setExtension($ext)
                  ->setId('BooleanID');


        $p = $this->getResource('Patient')
                  ->setActive($b);

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertMatchesRegularExpression('/_active":\{(.|myUrl|BooleanID)*}/', $out);
    }

    /**
     * @throws SerializerException
     */
    public function testResourceTypeProperty(): void
    {
        $p = $this->getResource('Patient')
                  ->setActive(true);

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertMatchesRegularExpression('/"resourceType"\s*:\s*"Patient"/', $out);
    }

    /**
     * @throws Exception
     */
    public function providerEmptyElements(): array
    {
        $emptyExt = $this->getResource('Extension')
                         ->setUrl('myUrl');

        $p1 = $this->getResource('Patient')
                   ->setExtension($emptyExt);

        $emptyStr = $this->getResource('string');
        $ext      = $this->getResource('Extension')
                         ->setUrl('myUrl')
                         ->setValue($emptyStr);

        $p2 = $this->getResource('Patient')
                   ->addExtension($ext);

        return [
            'Empty Resource' => [$this->getResource('Patient')],
            //'Empty Extension' => [$p1], // TODO: Vérifier si passe en JSON (interdit en XML)
            'Empty Value'    => [$p2],
        ];
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testPrintOnlySummarizeElementsWhenActivated(): void
    {
        $pc = $this->getResource('Patient.contact')
                   ->setGender('male');

        $p = $this->getResource('Patient')
                  ->setActive(true)
                  ->setContact($pc);

        $out = self::$serializer->serializeResource($p, ['summarize' => true, 'formatOutput' => false]);

        $this->assertStringNotContainsString('contact', $out);
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testContainedResourcesAreCreated(): void
    {
        $subPatient = $this->getResource('Patient')
                           ->setActive(true);
        $subPract   = $this->getResource('Practitioner')
                           ->setActive(true);

        $hn = $this->getResource('HumanName')
                   ->addGiven('myName');

        $p = $this->getResource('Patient')
                  ->setName($hn)
                  ->setContained($subPatient, $subPract);

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false, 'summarize' => false]);

        $this->assertMatchesRegularExpression(
            '/"contained":\[.*\{("active":true|"resourceType":"Patient"|,){3}}.*]/',
            $out
        );
        $this->assertMatchesRegularExpression(
            '/"contained":\[.*\{("active":true|"resourceType":"Practitioner"|,){3}}.*]/',
            $out
        );
        $this->assertStringContainsString('myName', $out);
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testBundleResource(): void
    {
        $bundle = $this->getResource('Bundle')
                       ->setEntry(
                           $this->getResource('Bundle.entry')
                                ->setResource(
                                    $this->getResource('Patient')
                                         ->setActive(true)
                                )
                       );

        $out = self::$serializer->serializeResource($bundle, ['formatOutput' => false, 'summarize' => false]);

        $this->assertMatchesRegularExpression(
            '/"entry":\[\{"resource":\{("active":true|"resourceType":"Patient"|,){3}}}]/',
            $out
        );
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testEmptyFieldsAreNotCreated(): void
    {
        $p = $this->getResource('Patient')
                  ->setActive(true)
                  ->setGender(null)
                  ->setDeceased($this->getResource('boolean'));

        $serialized = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertSame('{"resourceType":"Patient","active":true}', $serialized);
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testXHtmlFieldsAreTheValue(): void
    {
        $p = $this->getResource('Patient')
                  ->setActive(true)
                  ->setText(
                      $this->getResource('Narrative')
                           ->setStatus("generated")
                           ->setDiv('<div xmlns="http://www.w3.org/1999/xhtml"><p>ACTIVE</p></div>')
                  );

        $serialized = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertStringContainsString(
            '"div":"<div xmlns=\"http://www.w3.org/1999/xhtml\"><p>ACTIVE</p></div>"',
            $serialized
        );
    }
}
