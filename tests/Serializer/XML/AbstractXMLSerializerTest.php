<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Tests\Serializer\XML;

use DOMException;
use Exception;
use Ox\Components\FHIRCore\Serializer\Exception\SerializerException;
use Ox\Components\FHIRCore\Serializer\XMLSerializer;
use Ox\Components\FHIRCore\Tests\Trait\MultiVersionsHelperTrait;
use PHPUnit\Framework\TestCase;

/**
 * Description
 */
abstract class AbstractXMLSerializerTest extends TestCase
{
    use MultiVersionsHelperTrait;

    public static XMLSerializer $serializer;
    public static array         $objects;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$serializer = new XMLSerializer();
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testFormatOutput(): void
    {
        $p = $this->getResource('Patient')
                  ->setActive(true);

        $formatted  = self::$serializer->serializeResource($p, ['formatOutput' => true]);
        $nFormatted = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertNotSame($formatted, $nFormatted);
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testCaseSensitive(): void
    {
        $p = $this->getResource('Patient')
                  ->setActive(true);

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertStringContainsString('Patient', $out);
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testElementsAppearInTheOrderDocumented(): void
    {
        $p = $this->getResource('Patient')
                  ->setBirthDate('07-07-2002')
                  ->setActive(true)
                  ->setGender('male');

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertMatchesRegularExpression('/active.*gender.*birthDate/', $out);
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testElementsWithNoValueButIDAreNotDisplayed(): void
    {
        $p = $this->getResource('Patient')
                  ->setBirthDate('07-07-2002')
                  ->setActive($this->getResource('boolean')->setId('myId'))
                  ->setGender('male');

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertStringNotContainsString('active', $out);
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testElementsWithNoValueButExtensionAreDisplayed(): void
    {
        $p = $this->getResource('Patient')
                  ->setBirthDate('07-07-2002')
                  ->setGender('male')
                  ->setActive(
                      $this->getResource('boolean')
                           ->setExtension(
                               $this->getResource('Extension')
                                    ->setValue($this->getResource('boolean')->setValue(true))
                           )
                  );

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertStringContainsString('active', $out);
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testElementsAppearOrderedWhenSameMultipleElementString(): void
    {
        $p  = $this->getResource('Patient');
        $hn = $this->getResource('HumanName')
                   ->addGiven('b')
                   ->addGiven('z')
                   ->addGiven('a')
                   ->addGiven('c');
        $p->setName($hn);

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertMatchesRegularExpression(
            '/a.*b.*c.*z.*/',
            $out
        );
    }

    /**
     * @throws SerializerException
     * @throws Exception
     * @depends testElementsWithNoValueButExtensionAreDisplayed
     * @depends testElementsAppearOrderedWhenSameMultipleElementString
     */
    public function testElementsAppearOrderedWithANullValue(): void
    {
        $p  = $this->getResource('Patient');
        $hn = $this->getResource('HumanName')
                   ->addGiven('z')
                   ->addGiven('a')
                   ->addGiven('c')
                   ->addGiven(
                       $this->getResource('string')
                            ->setExtension(
                                $this->getResource('Extension')
                                     ->setValue($this->getResource('string')->setValue('b'))
                            )
                   );

        $p->setName($hn);

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertMatchesRegularExpression(
            '/"b".*"a".*"c".*"z".*/',
            $out
        );
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testElementsAppearOrderedWhenSameMultipleElementInteger(): void
    {
        try {
            $r = $this->getResource('CoverageEligibilityRequest');
        } catch (Exception) {
            $this->markTestSkipped('Resource doest not exists in this version');
        }

        $i = $this->getResource('CoverageEligibilityRequest.item');
        $i->addSupportingInfoSequence(7, 8, 2, 1, 9, 4, 15, 3, 5, 6);
        //required
        $ref = $this->getResource('Reference');
        $ref->setReference('1');
        $r->setStatus('active')
          ->setPurpose('validation')
          ->setPatient($ref)
          ->setInsurer($ref)
          ->setCreated('2')
          ->addItem($i);

        $out = self::$serializer->serializeResource($r, ['formatOutput' => false]);

        $this->assertMatchesRegularExpression(
            '/supportingInfoSequence.*1.*2.*3.*4.*5.*6.*7.*8.*9.*15/',
            $out
        );
    }

    /**
     * @dataProvider providerSetterAndValues
     *
     * @param string $setter
     * @param mixed  $value
     * @param string $expected
     *
     * @return void
     * @throws DOMException
     * @throws SerializerException
     * @throws Exception
     */
    public function testValueOfPrimitiveInAttributeFormatted(string $setter, mixed $value, string $expected): void
    {
        $p = $this->getResource('Patient')
                  ->$setter(
                      $value
                  );

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertStringContainsString("value=$expected", $out);
    }

    /**
     * @throws Exception
     */
    public function providerSetterAndValues(): array
    {
        return [
            ['setActive', true, '"true"'],
            ['setActive', false, '"false"'],
            ['setGender', 'male', '"male"'],
            ['setMultipleBirth', $this->getResource('integer')->setValue(7), '"7"'],
            ['setMultipleBirth', $this->getResource('integer')->setValue(0), '"0"'],
        ];
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testUrlExtensionValueInAttribute(): void
    {
        $ext = $this->getResource('Extension')
                    ->setUrl('myUrl')
                    ->setValue($this->getResource('string')->setValue('myStr'));

        $p = $this->getResource('Patient')
                  ->addExtension($ext);

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertStringContainsString('<extension url="myUrl">', $out);
        $this->assertStringNotContainsString('<url value="myUrl"/>', $out);
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testIdOfElementsInAttribute(): void
    {
        $p  = $this->getResource('Patient');
        $hn = $this->getResource('HumanName')
                   ->addGiven('aName')
                   ->setId('myID');
        $p->setName($hn);

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertStringContainsString('<name id="myID">', $out);
        $this->assertStringNotContainsString('<id value="myID"/>', $out);
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testIdOfResourcesAreElement(): void
    {
        $p = $this->getResource('Patient')
                  ->setActive(true)
                  ->setId('myID');

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertStringContainsString('<id value="myID"/>', $out);
        $this->assertStringNotContainsString('id="myID"', $out);
    }

    /**
     * @throws SerializerException
     */
    public function testXMLNamespaceInTheRootElement(): void
    {
        $p = $this->getResource('Patient')
                  ->setActive(true);

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertStringContainsString('<Patient xmlns="http://hl7.org/fhir">', $out);
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testPrintOnlySummarizeElementsWhenActivated(): void
    {
        $pc = $this->getResource('Patient.contact')
                   ->setGender('male');

        $p = $this->getResource('Patient')
                  ->setActive(true)
                  ->setContact($pc);

        $out = self::$serializer->serializeResource($p, ['summarize' => true, 'formatOutput' => false]);

        $this->assertStringNotContainsString('contact', $out);
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testContainedResourcesAreCreated(): void
    {
        $subPatient = $this->getResource('Patient')
                           ->setActive(true);
        $subPract   = $this->getResource('Practitioner')
                           ->setActive(true);

        $p = $this->getResource('Patient')
                  ->setName($this->getResource('HumanName')->addGiven('myName'))
                  ->setContained($subPatient, $subPract);

        $out = self::$serializer->serializeResource($p, ['formatOutput' => false, 'summarize' => false]);

        $this->assertStringContainsString('<contained><Patient><active value="true"/></Patient>', $out);
        $this->assertMatchesRegularExpression(
            '~<contained>(<Patient><active value="true"/></Patient>|<Practitioner><active value="true"/></Practitioner>){2}~',
            $out
        );
        $this->assertStringContainsString('myName', $out);
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testBundleResource(): void
    {
        $bundle = $this->getResource('Bundle')
                       ->setEntry(
                           $this->getResource('Bundle.entry')
                                ->setResource(
                                    $this->getResource('Patient')
                                         ->setActive(true)
                                )
                       );

        $out = self::$serializer->serializeResource($bundle, ['formatOutput' => false, 'summarize' => false]);

        $this->assertStringContainsString(
            '<Bundle xmlns="http://hl7.org/fhir"><entry><resource><Patient><active value="true"/></Patient></resource></entry></Bundle>',
            $out
        );
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testEmptyFieldsAreNotCreated(): void
    {
        $p = $this->getResource('Patient')
                  ->setActive(true)
                  ->setGender(null)
                  ->setDeceased($this->getResource('boolean'));

        $serialized = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertStringContainsString(
            '<Patient xmlns="http://hl7.org/fhir"><active value="true"/></Patient>',
            $serialized
        );
    }

    /**
     * @throws SerializerException
     * @throws Exception
     */
    public function testXHtmlFieldsAreTheValue(): void
    {
        $p = $this->getResource('Patient')
                  ->setActive(true)
                  ->setText(
                      $this->getResource('Narrative')
                           ->setStatus("generated")
                           ->setDiv('<div xmlns="http://www.w3.org/1999/xhtml"><p>ACTIVE</p></div>')
                  );

        $serialized = self::$serializer->serializeResource($p, ['formatOutput' => false]);

        $this->assertStringContainsString('<div xmlns="http://www.w3.org/1999/xhtml"><p>ACTIVE</p></div>', $serialized);
        $this->assertStringNotContainsString('<div value="', $serialized);
    }
}
