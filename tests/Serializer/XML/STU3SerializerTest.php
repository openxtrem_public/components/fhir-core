<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Tests\Serializer\XML;

use Ox\Components\FHIRCore\Model\STU3\StructureDefinition;

class STU3SerializerTest extends AbstractXMLSerializerTest
{
    final public const STRUCTURE_DEFINITION = StructureDefinition::class;
}
