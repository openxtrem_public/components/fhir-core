<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImagingSelection Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRImagingSelectionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUnsignedInt;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRImagingSelectionInstance;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRImagingSelectionPerformer;

class FHIRImagingSelection extends FHIRDomainResource implements FHIRImagingSelectionInterface
{
    public const RESOURCE_NAME = 'ImagingSelection';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRInstant $issued = null;

    /** @var FHIRImagingSelectionPerformer[] */
    protected array $performer = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRId $studyUid = null;

    /** @var FHIRReference[] */
    protected array $derivedFrom = [];

    /** @var FHIRReference[] */
    protected array $endpoint = [];
    protected ?FHIRId $seriesUid = null;
    protected ?FHIRUnsignedInt $seriesNumber = null;
    protected ?FHIRId $frameOfReferenceUid = null;
    protected ?FHIRCodeableReference $bodySite = null;

    /** @var FHIRReference[] */
    protected array $focus = [];

    /** @var FHIRImagingSelectionInstance[] */
    protected array $instance = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getIssued(): ?FHIRInstant
    {
        return $this->issued;
    }

    public function setIssued(string|FHIRInstant|null $value): self
    {
        $this->issued = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRImagingSelectionPerformer[]
     */
    public function getPerformer(): array
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRImagingSelectionPerformer ...$value): self
    {
        $this->performer = array_filter($value);

        return $this;
    }

    public function addPerformer(?FHIRImagingSelectionPerformer ...$value): self
    {
        $this->performer = array_filter(array_merge($this->performer, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getStudyUid(): ?FHIRId
    {
        return $this->studyUid;
    }

    public function setStudyUid(string|FHIRId|null $value): self
    {
        $this->studyUid = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getDerivedFrom(): array
    {
        return $this->derivedFrom;
    }

    public function setDerivedFrom(?FHIRReference ...$value): self
    {
        $this->derivedFrom = array_filter($value);

        return $this;
    }

    public function addDerivedFrom(?FHIRReference ...$value): self
    {
        $this->derivedFrom = array_filter(array_merge($this->derivedFrom, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEndpoint(): array
    {
        return $this->endpoint;
    }

    public function setEndpoint(?FHIRReference ...$value): self
    {
        $this->endpoint = array_filter($value);

        return $this;
    }

    public function addEndpoint(?FHIRReference ...$value): self
    {
        $this->endpoint = array_filter(array_merge($this->endpoint, $value));

        return $this;
    }

    public function getSeriesUid(): ?FHIRId
    {
        return $this->seriesUid;
    }

    public function setSeriesUid(string|FHIRId|null $value): self
    {
        $this->seriesUid = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getSeriesNumber(): ?FHIRUnsignedInt
    {
        return $this->seriesNumber;
    }

    public function setSeriesNumber(int|FHIRUnsignedInt|null $value): self
    {
        $this->seriesNumber = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getFrameOfReferenceUid(): ?FHIRId
    {
        return $this->frameOfReferenceUid;
    }

    public function setFrameOfReferenceUid(string|FHIRId|null $value): self
    {
        $this->frameOfReferenceUid = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getBodySite(): ?FHIRCodeableReference
    {
        return $this->bodySite;
    }

    public function setBodySite(?FHIRCodeableReference $value): self
    {
        $this->bodySite = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getFocus(): array
    {
        return $this->focus;
    }

    public function setFocus(?FHIRReference ...$value): self
    {
        $this->focus = array_filter($value);

        return $this;
    }

    public function addFocus(?FHIRReference ...$value): self
    {
        $this->focus = array_filter(array_merge($this->focus, $value));

        return $this;
    }

    /**
     * @return FHIRImagingSelectionInstance[]
     */
    public function getInstance(): array
    {
        return $this->instance;
    }

    public function setInstance(?FHIRImagingSelectionInstance ...$value): self
    {
        $this->instance = array_filter($value);

        return $this;
    }

    public function addInstance(?FHIRImagingSelectionInstance ...$value): self
    {
        $this->instance = array_filter(array_merge($this->instance, $value));

        return $this;
    }
}
