<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR NutritionProduct Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRNutritionProductInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRNutritionProductCharacteristic;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRNutritionProductIngredient;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRNutritionProductInstance;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRNutritionProductNutrient;

class FHIRNutritionProduct extends FHIRDomainResource implements FHIRNutritionProductInterface
{
    public const RESOURCE_NAME = 'NutritionProduct';

    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRCode $status = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];

    /** @var FHIRReference[] */
    protected array $manufacturer = [];

    /** @var FHIRNutritionProductNutrient[] */
    protected array $nutrient = [];

    /** @var FHIRNutritionProductIngredient[] */
    protected array $ingredient = [];

    /** @var FHIRCodeableReference[] */
    protected array $knownAllergen = [];

    /** @var FHIRNutritionProductCharacteristic[] */
    protected array $characteristic = [];

    /** @var FHIRNutritionProductInstance[] */
    protected array $instance = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getManufacturer(): array
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?FHIRReference ...$value): self
    {
        $this->manufacturer = array_filter($value);

        return $this;
    }

    public function addManufacturer(?FHIRReference ...$value): self
    {
        $this->manufacturer = array_filter(array_merge($this->manufacturer, $value));

        return $this;
    }

    /**
     * @return FHIRNutritionProductNutrient[]
     */
    public function getNutrient(): array
    {
        return $this->nutrient;
    }

    public function setNutrient(?FHIRNutritionProductNutrient ...$value): self
    {
        $this->nutrient = array_filter($value);

        return $this;
    }

    public function addNutrient(?FHIRNutritionProductNutrient ...$value): self
    {
        $this->nutrient = array_filter(array_merge($this->nutrient, $value));

        return $this;
    }

    /**
     * @return FHIRNutritionProductIngredient[]
     */
    public function getIngredient(): array
    {
        return $this->ingredient;
    }

    public function setIngredient(?FHIRNutritionProductIngredient ...$value): self
    {
        $this->ingredient = array_filter($value);

        return $this;
    }

    public function addIngredient(?FHIRNutritionProductIngredient ...$value): self
    {
        $this->ingredient = array_filter(array_merge($this->ingredient, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getKnownAllergen(): array
    {
        return $this->knownAllergen;
    }

    public function setKnownAllergen(?FHIRCodeableReference ...$value): self
    {
        $this->knownAllergen = array_filter($value);

        return $this;
    }

    public function addKnownAllergen(?FHIRCodeableReference ...$value): self
    {
        $this->knownAllergen = array_filter(array_merge($this->knownAllergen, $value));

        return $this;
    }

    /**
     * @return FHIRNutritionProductCharacteristic[]
     */
    public function getCharacteristic(): array
    {
        return $this->characteristic;
    }

    public function setCharacteristic(?FHIRNutritionProductCharacteristic ...$value): self
    {
        $this->characteristic = array_filter($value);

        return $this;
    }

    public function addCharacteristic(?FHIRNutritionProductCharacteristic ...$value): self
    {
        $this->characteristic = array_filter(array_merge($this->characteristic, $value));

        return $this;
    }

    /**
     * @return FHIRNutritionProductInstance[]
     */
    public function getInstance(): array
    {
        return $this->instance;
    }

    public function setInstance(?FHIRNutritionProductInstance ...$value): self
    {
        $this->instance = array_filter($value);

        return $this;
    }

    public function addInstance(?FHIRNutritionProductInstance ...$value): self
    {
        $this->instance = array_filter(array_merge($this->instance, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }
}
