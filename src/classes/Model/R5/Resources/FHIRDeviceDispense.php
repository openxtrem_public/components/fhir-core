<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceDispense Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRDeviceDispenseInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDeviceDispensePerformer;

class FHIRDeviceDispense extends FHIRDomainResource implements FHIRDeviceDispenseInterface
{
    public const RESOURCE_NAME = 'DeviceDispense';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];

    /** @var FHIRReference[] */
    protected array $partOf = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableReference $statusReason = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRCodeableReference $device = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $receiver = null;
    protected ?FHIRReference $encounter = null;

    /** @var FHIRReference[] */
    protected array $supportingInformation = [];

    /** @var FHIRDeviceDispensePerformer[] */
    protected array $performer = [];
    protected ?FHIRReference $location = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRDateTime $preparedDate = null;
    protected ?FHIRDateTime $whenHandedOver = null;
    protected ?FHIRReference $destination = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];
    protected ?FHIRMarkdown $usageInstruction = null;

    /** @var FHIRReference[] */
    protected array $eventHistory = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPartOf(): array
    {
        return $this->partOf;
    }

    public function setPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter($value);

        return $this;
    }

    public function addPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter(array_merge($this->partOf, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getStatusReason(): ?FHIRCodeableReference
    {
        return $this->statusReason;
    }

    public function setStatusReason(?FHIRCodeableReference $value): self
    {
        $this->statusReason = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getDevice(): ?FHIRCodeableReference
    {
        return $this->device;
    }

    public function setDevice(?FHIRCodeableReference $value): self
    {
        $this->device = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getReceiver(): ?FHIRReference
    {
        return $this->receiver;
    }

    public function setReceiver(?FHIRReference $value): self
    {
        $this->receiver = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupportingInformation(): array
    {
        return $this->supportingInformation;
    }

    public function setSupportingInformation(?FHIRReference ...$value): self
    {
        $this->supportingInformation = array_filter($value);

        return $this;
    }

    public function addSupportingInformation(?FHIRReference ...$value): self
    {
        $this->supportingInformation = array_filter(array_merge($this->supportingInformation, $value));

        return $this;
    }

    /**
     * @return FHIRDeviceDispensePerformer[]
     */
    public function getPerformer(): array
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRDeviceDispensePerformer ...$value): self
    {
        $this->performer = array_filter($value);

        return $this;
    }

    public function addPerformer(?FHIRDeviceDispensePerformer ...$value): self
    {
        $this->performer = array_filter(array_merge($this->performer, $value));

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getPreparedDate(): ?FHIRDateTime
    {
        return $this->preparedDate;
    }

    public function setPreparedDate(string|FHIRDateTime|null $value): self
    {
        $this->preparedDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getWhenHandedOver(): ?FHIRDateTime
    {
        return $this->whenHandedOver;
    }

    public function setWhenHandedOver(string|FHIRDateTime|null $value): self
    {
        $this->whenHandedOver = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getDestination(): ?FHIRReference
    {
        return $this->destination;
    }

    public function setDestination(?FHIRReference $value): self
    {
        $this->destination = $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    public function getUsageInstruction(): ?FHIRMarkdown
    {
        return $this->usageInstruction;
    }

    public function setUsageInstruction(string|FHIRMarkdown|null $value): self
    {
        $this->usageInstruction = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEventHistory(): array
    {
        return $this->eventHistory;
    }

    public function setEventHistory(?FHIRReference ...$value): self
    {
        $this->eventHistory = array_filter($value);

        return $this;
    }

    public function addEventHistory(?FHIRReference ...$value): self
    {
        $this->eventHistory = array_filter(array_merge($this->eventHistory, $value));

        return $this;
    }
}
