<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Resource Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRResourceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBase;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRMeta;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;

abstract class FHIRResource extends FHIRBase implements FHIRResourceInterface
{
    public const RESOURCE_NAME = 'Resource';

    protected ?FHIRString $id = null;
    protected ?FHIRMeta $meta = null;
    protected ?FHIRUri $implicitRules = null;
    protected ?FHIRCode $language = null;

    public function getId(): ?FHIRString
    {
        return $this->id;
    }

    public function setId(string|FHIRString|null $value): self
    {
        $this->id = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getMeta(): ?FHIRMeta
    {
        return $this->meta;
    }

    public function setMeta(?FHIRMeta $value): self
    {
        $this->meta = $value;

        return $this;
    }

    public function getImplicitRules(): ?FHIRUri
    {
        return $this->implicitRules;
    }

    public function setImplicitRules(string|FHIRUri|null $value): self
    {
        $this->implicitRules = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getLanguage(): ?FHIRCode
    {
        return $this->language;
    }

    public function setLanguage(string|FHIRCode|null $value): self
    {
        $this->language = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }
}
