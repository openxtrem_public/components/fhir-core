<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceAssociation Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRDeviceAssociationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDeviceAssociationOperation;

class FHIRDeviceAssociation extends FHIRDomainResource implements FHIRDeviceAssociationInterface
{
    public const RESOURCE_NAME = 'DeviceAssociation';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRReference $device = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRCodeableConcept $status = null;

    /** @var FHIRCodeableConcept[] */
    protected array $statusReason = [];
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $bodyStructure = null;
    protected ?FHIRPeriod $period = null;

    /** @var FHIRDeviceAssociationOperation[] */
    protected array $operation = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getDevice(): ?FHIRReference
    {
        return $this->device;
    }

    public function setDevice(?FHIRReference $value): self
    {
        $this->device = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCodeableConcept
    {
        return $this->status;
    }

    public function setStatus(?FHIRCodeableConcept $value): self
    {
        $this->status = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getStatusReason(): array
    {
        return $this->statusReason;
    }

    public function setStatusReason(?FHIRCodeableConcept ...$value): self
    {
        $this->statusReason = array_filter($value);

        return $this;
    }

    public function addStatusReason(?FHIRCodeableConcept ...$value): self
    {
        $this->statusReason = array_filter(array_merge($this->statusReason, $value));

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getBodyStructure(): ?FHIRReference
    {
        return $this->bodyStructure;
    }

    public function setBodyStructure(?FHIRReference $value): self
    {
        $this->bodyStructure = $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    /**
     * @return FHIRDeviceAssociationOperation[]
     */
    public function getOperation(): array
    {
        return $this->operation;
    }

    public function setOperation(?FHIRDeviceAssociationOperation ...$value): self
    {
        $this->operation = array_filter($value);

        return $this;
    }

    public function addOperation(?FHIRDeviceAssociationOperation ...$value): self
    {
        $this->operation = array_filter(array_merge($this->operation, $value));

        return $this;
    }
}
