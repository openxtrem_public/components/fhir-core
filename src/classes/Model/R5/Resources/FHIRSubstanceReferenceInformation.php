<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceReferenceInformation Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRSubstanceReferenceInformationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRSubstanceReferenceInformationGene;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRSubstanceReferenceInformationGeneElement;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRSubstanceReferenceInformationTarget;

class FHIRSubstanceReferenceInformation extends FHIRDomainResource implements FHIRSubstanceReferenceInformationInterface
{
    public const RESOURCE_NAME = 'SubstanceReferenceInformation';

    protected ?FHIRString $comment = null;

    /** @var FHIRSubstanceReferenceInformationGene[] */
    protected array $gene = [];

    /** @var FHIRSubstanceReferenceInformationGeneElement[] */
    protected array $geneElement = [];

    /** @var FHIRSubstanceReferenceInformationTarget[] */
    protected array $target = [];

    public function getComment(): ?FHIRString
    {
        return $this->comment;
    }

    public function setComment(string|FHIRString|null $value): self
    {
        $this->comment = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRSubstanceReferenceInformationGene[]
     */
    public function getGene(): array
    {
        return $this->gene;
    }

    public function setGene(?FHIRSubstanceReferenceInformationGene ...$value): self
    {
        $this->gene = array_filter($value);

        return $this;
    }

    public function addGene(?FHIRSubstanceReferenceInformationGene ...$value): self
    {
        $this->gene = array_filter(array_merge($this->gene, $value));

        return $this;
    }

    /**
     * @return FHIRSubstanceReferenceInformationGeneElement[]
     */
    public function getGeneElement(): array
    {
        return $this->geneElement;
    }

    public function setGeneElement(?FHIRSubstanceReferenceInformationGeneElement ...$value): self
    {
        $this->geneElement = array_filter($value);

        return $this;
    }

    public function addGeneElement(?FHIRSubstanceReferenceInformationGeneElement ...$value): self
    {
        $this->geneElement = array_filter(array_merge($this->geneElement, $value));

        return $this;
    }

    /**
     * @return FHIRSubstanceReferenceInformationTarget[]
     */
    public function getTarget(): array
    {
        return $this->target;
    }

    public function setTarget(?FHIRSubstanceReferenceInformationTarget ...$value): self
    {
        $this->target = array_filter($value);

        return $this;
    }

    public function addTarget(?FHIRSubstanceReferenceInformationTarget ...$value): self
    {
        $this->target = array_filter(array_merge($this->target, $value));

        return $this;
    }
}
