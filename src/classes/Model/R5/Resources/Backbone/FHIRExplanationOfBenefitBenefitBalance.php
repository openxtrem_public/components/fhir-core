<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExplanationOfBenefitBenefitBalance Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExplanationOfBenefitBenefitBalanceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRExplanationOfBenefitBenefitBalance extends FHIRBackboneElement implements FHIRExplanationOfBenefitBenefitBalanceInterface
{
    public const RESOURCE_NAME = 'ExplanationOfBenefit.benefitBalance';

    protected ?FHIRCodeableConcept $category = null;
    protected ?FHIRBoolean $excluded = null;
    protected ?FHIRString $name = null;
    protected ?FHIRString $description = null;
    protected ?FHIRCodeableConcept $network = null;
    protected ?FHIRCodeableConcept $unit = null;
    protected ?FHIRCodeableConcept $term = null;

    /** @var FHIRExplanationOfBenefitBenefitBalanceFinancial[] */
    protected array $financial = [];

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getExcluded(): ?FHIRBoolean
    {
        return $this->excluded;
    }

    public function setExcluded(bool|FHIRBoolean|null $value): self
    {
        $this->excluded = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getNetwork(): ?FHIRCodeableConcept
    {
        return $this->network;
    }

    public function setNetwork(?FHIRCodeableConcept $value): self
    {
        $this->network = $value;

        return $this;
    }

    public function getUnit(): ?FHIRCodeableConcept
    {
        return $this->unit;
    }

    public function setUnit(?FHIRCodeableConcept $value): self
    {
        $this->unit = $value;

        return $this;
    }

    public function getTerm(): ?FHIRCodeableConcept
    {
        return $this->term;
    }

    public function setTerm(?FHIRCodeableConcept $value): self
    {
        $this->term = $value;

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitBenefitBalanceFinancial[]
     */
    public function getFinancial(): array
    {
        return $this->financial;
    }

    public function setFinancial(?FHIRExplanationOfBenefitBenefitBalanceFinancial ...$value): self
    {
        $this->financial = array_filter($value);

        return $this;
    }

    public function addFinancial(?FHIRExplanationOfBenefitBenefitBalanceFinancial ...$value): self
    {
        $this->financial = array_filter(array_merge($this->financial, $value));

        return $this;
    }
}
