<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR InventoryItemAssociation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRInventoryItemAssociationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRatio;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRInventoryItemAssociation extends FHIRBackboneElement implements FHIRInventoryItemAssociationInterface
{
    public const RESOURCE_NAME = 'InventoryItem.association';

    protected ?FHIRCodeableConcept $associationType = null;
    protected ?FHIRReference $relatedItem = null;
    protected ?FHIRRatio $quantity = null;

    public function getAssociationType(): ?FHIRCodeableConcept
    {
        return $this->associationType;
    }

    public function setAssociationType(?FHIRCodeableConcept $value): self
    {
        $this->associationType = $value;

        return $this;
    }

    public function getRelatedItem(): ?FHIRReference
    {
        return $this->relatedItem;
    }

    public function setRelatedItem(?FHIRReference $value): self
    {
        $this->relatedItem = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRRatio
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRRatio $value): self
    {
        $this->quantity = $value;

        return $this;
    }
}
