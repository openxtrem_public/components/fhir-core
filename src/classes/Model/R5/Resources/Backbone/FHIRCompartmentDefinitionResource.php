<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CompartmentDefinitionResource Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCompartmentDefinitionResourceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;

class FHIRCompartmentDefinitionResource extends FHIRBackboneElement implements FHIRCompartmentDefinitionResourceInterface
{
    public const RESOURCE_NAME = 'CompartmentDefinition.resource';

    protected ?FHIRCode $code = null;

    /** @var FHIRString[] */
    protected array $param = [];
    protected ?FHIRString $documentation = null;
    protected ?FHIRUri $startParam = null;
    protected ?FHIRUri $endParam = null;

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getParam(): array
    {
        return $this->param;
    }

    public function setParam(string|FHIRString|null ...$value): self
    {
        $this->param = [];
        $this->addParam(...$value);

        return $this;
    }

    public function addParam(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->param = array_filter(array_merge($this->param, $values));

        return $this;
    }

    public function getDocumentation(): ?FHIRString
    {
        return $this->documentation;
    }

    public function setDocumentation(string|FHIRString|null $value): self
    {
        $this->documentation = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStartParam(): ?FHIRUri
    {
        return $this->startParam;
    }

    public function setStartParam(string|FHIRUri|null $value): self
    {
        $this->startParam = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getEndParam(): ?FHIRUri
    {
        return $this->endParam;
    }

    public function setEndParam(string|FHIRUri|null $value): self
    {
        $this->endParam = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }
}
