<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubscriptionTopicEventTrigger Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubscriptionTopicEventTriggerInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;

class FHIRSubscriptionTopicEventTrigger extends FHIRBackboneElement implements FHIRSubscriptionTopicEventTriggerInterface
{
    public const RESOURCE_NAME = 'SubscriptionTopic.eventTrigger';

    protected ?FHIRMarkdown $description = null;
    protected ?FHIRCodeableConcept $event = null;
    protected ?FHIRUri $resource = null;

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getEvent(): ?FHIRCodeableConcept
    {
        return $this->event;
    }

    public function setEvent(?FHIRCodeableConcept $value): self
    {
        $this->event = $value;

        return $this;
    }

    public function getResource(): ?FHIRUri
    {
        return $this->resource;
    }

    public function setResource(string|FHIRUri|null $value): self
    {
        $this->resource = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }
}
