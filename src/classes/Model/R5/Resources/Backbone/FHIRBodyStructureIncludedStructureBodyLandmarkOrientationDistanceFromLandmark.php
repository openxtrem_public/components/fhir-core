<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR BodyStructureIncludedStructureBodyLandmarkOrientationDistanceFromLandmark Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRBodyStructureIncludedStructureBodyLandmarkOrientationDistanceFromLandmarkInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;

class FHIRBodyStructureIncludedStructureBodyLandmarkOrientationDistanceFromLandmark extends FHIRBackboneElement implements FHIRBodyStructureIncludedStructureBodyLandmarkOrientationDistanceFromLandmarkInterface
{
    public const RESOURCE_NAME = 'BodyStructure.includedStructure.bodyLandmarkOrientation.distanceFromLandmark';

    /** @var FHIRCodeableReference[] */
    protected array $device = [];

    /** @var FHIRQuantity[] */
    protected array $value = [];

    /**
     * @return FHIRCodeableReference[]
     */
    public function getDevice(): array
    {
        return $this->device;
    }

    public function setDevice(?FHIRCodeableReference ...$value): self
    {
        $this->device = array_filter($value);

        return $this;
    }

    public function addDevice(?FHIRCodeableReference ...$value): self
    {
        $this->device = array_filter(array_merge($this->device, $value));

        return $this;
    }

    /**
     * @return FHIRQuantity[]
     */
    public function getValue(): array
    {
        return $this->value;
    }

    public function setValue(?FHIRQuantity ...$value): self
    {
        $this->value = array_filter($value);

        return $this;
    }

    public function addValue(?FHIRQuantity ...$value): self
    {
        $this->value = array_filter(array_merge($this->value, $value));

        return $this;
    }
}
