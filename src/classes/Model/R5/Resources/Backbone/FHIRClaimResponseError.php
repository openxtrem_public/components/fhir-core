<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClaimResponseError Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRClaimResponseErrorInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRClaimResponseError extends FHIRBackboneElement implements FHIRClaimResponseErrorInterface
{
    public const RESOURCE_NAME = 'ClaimResponse.error';

    protected ?FHIRPositiveInt $itemSequence = null;
    protected ?FHIRPositiveInt $detailSequence = null;
    protected ?FHIRPositiveInt $subDetailSequence = null;
    protected ?FHIRCodeableConcept $code = null;

    /** @var FHIRString[] */
    protected array $expression = [];

    public function getItemSequence(): ?FHIRPositiveInt
    {
        return $this->itemSequence;
    }

    public function setItemSequence(int|FHIRPositiveInt|null $value): self
    {
        $this->itemSequence = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getDetailSequence(): ?FHIRPositiveInt
    {
        return $this->detailSequence;
    }

    public function setDetailSequence(int|FHIRPositiveInt|null $value): self
    {
        $this->detailSequence = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getSubDetailSequence(): ?FHIRPositiveInt
    {
        return $this->subDetailSequence;
    }

    public function setSubDetailSequence(int|FHIRPositiveInt|null $value): self
    {
        $this->subDetailSequence = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getExpression(): array
    {
        return $this->expression;
    }

    public function setExpression(string|FHIRString|null ...$value): self
    {
        $this->expression = [];
        $this->addExpression(...$value);

        return $this;
    }

    public function addExpression(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->expression = array_filter(array_merge($this->expression, $values));

        return $this;
    }
}
