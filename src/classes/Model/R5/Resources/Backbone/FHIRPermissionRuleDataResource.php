<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PermissionRuleDataResource Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRPermissionRuleDataResourceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;

class FHIRPermissionRuleDataResource extends FHIRBackboneElement implements FHIRPermissionRuleDataResourceInterface
{
    public const RESOURCE_NAME = 'Permission.rule.data.resource';

    protected ?FHIRCode $meaning = null;
    protected ?FHIRReference $reference = null;

    public function getMeaning(): ?FHIRCode
    {
        return $this->meaning;
    }

    public function setMeaning(string|FHIRCode|null $value): self
    {
        $this->meaning = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getReference(): ?FHIRReference
    {
        return $this->reference;
    }

    public function setReference(?FHIRReference $value): self
    {
        $this->reference = $value;

        return $this;
    }
}
