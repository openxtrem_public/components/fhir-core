<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AppointmentRecurrenceTemplate Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAppointmentRecurrenceTemplateInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;

class FHIRAppointmentRecurrenceTemplate extends FHIRBackboneElement implements FHIRAppointmentRecurrenceTemplateInterface
{
    public const RESOURCE_NAME = 'Appointment.recurrenceTemplate';

    protected ?FHIRCodeableConcept $timezone = null;
    protected ?FHIRCodeableConcept $recurrenceType = null;
    protected ?FHIRDate $lastOccurrenceDate = null;
    protected ?FHIRPositiveInt $occurrenceCount = null;

    /** @var FHIRDate[] */
    protected array $occurrenceDate = [];
    protected ?FHIRAppointmentRecurrenceTemplateWeeklyTemplate $weeklyTemplate = null;
    protected ?FHIRAppointmentRecurrenceTemplateMonthlyTemplate $monthlyTemplate = null;
    protected ?FHIRAppointmentRecurrenceTemplateYearlyTemplate $yearlyTemplate = null;

    /** @var FHIRDate[] */
    protected array $excludingDate = [];

    /** @var FHIRPositiveInt[] */
    protected array $excludingRecurrenceId = [];

    public function getTimezone(): ?FHIRCodeableConcept
    {
        return $this->timezone;
    }

    public function setTimezone(?FHIRCodeableConcept $value): self
    {
        $this->timezone = $value;

        return $this;
    }

    public function getRecurrenceType(): ?FHIRCodeableConcept
    {
        return $this->recurrenceType;
    }

    public function setRecurrenceType(?FHIRCodeableConcept $value): self
    {
        $this->recurrenceType = $value;

        return $this;
    }

    public function getLastOccurrenceDate(): ?FHIRDate
    {
        return $this->lastOccurrenceDate;
    }

    public function setLastOccurrenceDate(string|FHIRDate|null $value): self
    {
        $this->lastOccurrenceDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getOccurrenceCount(): ?FHIRPositiveInt
    {
        return $this->occurrenceCount;
    }

    public function setOccurrenceCount(int|FHIRPositiveInt|null $value): self
    {
        $this->occurrenceCount = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRDate[]
     */
    public function getOccurrenceDate(): array
    {
        return $this->occurrenceDate;
    }

    public function setOccurrenceDate(string|FHIRDate|null ...$value): self
    {
        $this->occurrenceDate = [];
        $this->addOccurrenceDate(...$value);

        return $this;
    }

    public function addOccurrenceDate(string|FHIRDate|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRDate())->setValue($v) : $v, $value);

        $this->occurrenceDate = array_filter(array_merge($this->occurrenceDate, $values));

        return $this;
    }

    public function getWeeklyTemplate(): ?FHIRAppointmentRecurrenceTemplateWeeklyTemplate
    {
        return $this->weeklyTemplate;
    }

    public function setWeeklyTemplate(?FHIRAppointmentRecurrenceTemplateWeeklyTemplate $value): self
    {
        $this->weeklyTemplate = $value;

        return $this;
    }

    public function getMonthlyTemplate(): ?FHIRAppointmentRecurrenceTemplateMonthlyTemplate
    {
        return $this->monthlyTemplate;
    }

    public function setMonthlyTemplate(?FHIRAppointmentRecurrenceTemplateMonthlyTemplate $value): self
    {
        $this->monthlyTemplate = $value;

        return $this;
    }

    public function getYearlyTemplate(): ?FHIRAppointmentRecurrenceTemplateYearlyTemplate
    {
        return $this->yearlyTemplate;
    }

    public function setYearlyTemplate(?FHIRAppointmentRecurrenceTemplateYearlyTemplate $value): self
    {
        $this->yearlyTemplate = $value;

        return $this;
    }

    /**
     * @return FHIRDate[]
     */
    public function getExcludingDate(): array
    {
        return $this->excludingDate;
    }

    public function setExcludingDate(string|FHIRDate|null ...$value): self
    {
        $this->excludingDate = [];
        $this->addExcludingDate(...$value);

        return $this;
    }

    public function addExcludingDate(string|FHIRDate|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRDate())->setValue($v) : $v, $value);

        $this->excludingDate = array_filter(array_merge($this->excludingDate, $values));

        return $this;
    }

    /**
     * @return FHIRPositiveInt[]
     */
    public function getExcludingRecurrenceId(): array
    {
        return $this->excludingRecurrenceId;
    }

    public function setExcludingRecurrenceId(int|FHIRPositiveInt|null ...$value): self
    {
        $this->excludingRecurrenceId = [];
        $this->addExcludingRecurrenceId(...$value);

        return $this;
    }

    public function addExcludingRecurrenceId(int|FHIRPositiveInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRPositiveInt())->setValue($v) : $v, $value);

        $this->excludingRecurrenceId = array_filter(array_merge($this->excludingRecurrenceId, $values));

        return $this;
    }
}
