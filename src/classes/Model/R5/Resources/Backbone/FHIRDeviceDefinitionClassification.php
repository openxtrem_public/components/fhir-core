<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceDefinitionClassification Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceDefinitionClassificationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRelatedArtifact;

class FHIRDeviceDefinitionClassification extends FHIRBackboneElement implements FHIRDeviceDefinitionClassificationInterface
{
    public const RESOURCE_NAME = 'DeviceDefinition.classification';

    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRRelatedArtifact[] */
    protected array $justification = [];

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRRelatedArtifact[]
     */
    public function getJustification(): array
    {
        return $this->justification;
    }

    public function setJustification(?FHIRRelatedArtifact ...$value): self
    {
        $this->justification = array_filter($value);

        return $this;
    }

    public function addJustification(?FHIRRelatedArtifact ...$value): self
    {
        $this->justification = array_filter(array_merge($this->justification, $value));

        return $this;
    }
}
