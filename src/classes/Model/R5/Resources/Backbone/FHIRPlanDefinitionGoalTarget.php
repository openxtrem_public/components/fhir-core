<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PlanDefinitionGoalTarget Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRPlanDefinitionGoalTargetInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRatio;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRPlanDefinitionGoalTarget extends FHIRBackboneElement implements FHIRPlanDefinitionGoalTargetInterface
{
    public const RESOURCE_NAME = 'PlanDefinition.goal.target';

    protected ?FHIRCodeableConcept $measure = null;
    protected FHIRQuantity|FHIRRange|FHIRCodeableConcept|FHIRString|FHIRBoolean|FHIRInteger|FHIRRatio|null $detail = null;
    protected ?FHIRDuration $due = null;

    public function getMeasure(): ?FHIRCodeableConcept
    {
        return $this->measure;
    }

    public function setMeasure(?FHIRCodeableConcept $value): self
    {
        $this->measure = $value;

        return $this;
    }

    public function getDetail(
    ): FHIRQuantity|FHIRRange|FHIRCodeableConcept|FHIRString|FHIRBoolean|FHIRInteger|FHIRRatio|null
    {
        return $this->detail;
    }

    public function setDetail(
        FHIRQuantity|FHIRRange|FHIRCodeableConcept|FHIRString|FHIRBoolean|FHIRInteger|FHIRRatio|null $value,
    ): self
    {
        $this->detail = $value;

        return $this;
    }

    public function getDue(): ?FHIRDuration
    {
        return $this->due;
    }

    public function setDue(?FHIRDuration $value): self
    {
        $this->due = $value;

        return $this;
    }
}
