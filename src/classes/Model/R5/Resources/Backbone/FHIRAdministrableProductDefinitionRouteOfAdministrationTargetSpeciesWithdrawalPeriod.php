<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AdministrableProductDefinitionRouteOfAdministrationTargetSpeciesWithdrawalPeriod Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAdministrableProductDefinitionRouteOfAdministrationTargetSpeciesWithdrawalPeriodInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRAdministrableProductDefinitionRouteOfAdministrationTargetSpeciesWithdrawalPeriod extends FHIRBackboneElement implements FHIRAdministrableProductDefinitionRouteOfAdministrationTargetSpeciesWithdrawalPeriodInterface
{
    public const RESOURCE_NAME = 'AdministrableProductDefinition.routeOfAdministration.targetSpecies.withdrawalPeriod';

    protected ?FHIRCodeableConcept $tissue = null;
    protected ?FHIRQuantity $value = null;
    protected ?FHIRString $supportingInformation = null;

    public function getTissue(): ?FHIRCodeableConcept
    {
        return $this->tissue;
    }

    public function setTissue(?FHIRCodeableConcept $value): self
    {
        $this->tissue = $value;

        return $this;
    }

    public function getValue(): ?FHIRQuantity
    {
        return $this->value;
    }

    public function setValue(?FHIRQuantity $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getSupportingInformation(): ?FHIRString
    {
        return $this->supportingInformation;
    }

    public function setSupportingInformation(string|FHIRString|null $value): self
    {
        $this->supportingInformation = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
