<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExplanationOfBenefitInsurance Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExplanationOfBenefitInsuranceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRExplanationOfBenefitInsurance extends FHIRBackboneElement implements FHIRExplanationOfBenefitInsuranceInterface
{
    public const RESOURCE_NAME = 'ExplanationOfBenefit.insurance';

    protected ?FHIRBoolean $focal = null;
    protected ?FHIRReference $coverage = null;

    /** @var FHIRString[] */
    protected array $preAuthRef = [];

    public function getFocal(): ?FHIRBoolean
    {
        return $this->focal;
    }

    public function setFocal(bool|FHIRBoolean|null $value): self
    {
        $this->focal = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getCoverage(): ?FHIRReference
    {
        return $this->coverage;
    }

    public function setCoverage(?FHIRReference $value): self
    {
        $this->coverage = $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getPreAuthRef(): array
    {
        return $this->preAuthRef;
    }

    public function setPreAuthRef(string|FHIRString|null ...$value): self
    {
        $this->preAuthRef = [];
        $this->addPreAuthRef(...$value);

        return $this;
    }

    public function addPreAuthRef(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->preAuthRef = array_filter(array_merge($this->preAuthRef, $values));

        return $this;
    }
}
