<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceDefinitionProperty Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceDefinitionPropertyInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRDeviceDefinitionProperty extends FHIRBackboneElement implements FHIRDeviceDefinitionPropertyInterface
{
    public const RESOURCE_NAME = 'DeviceDefinition.property';

    protected ?FHIRCodeableConcept $type = null;
    protected FHIRQuantity|FHIRCodeableConcept|FHIRString|FHIRBoolean|FHIRInteger|FHIRRange|FHIRAttachment|null $value = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getValue(
    ): FHIRQuantity|FHIRCodeableConcept|FHIRString|FHIRBoolean|FHIRInteger|FHIRRange|FHIRAttachment|null
    {
        return $this->value;
    }

    public function setValue(
        FHIRQuantity|FHIRCodeableConcept|FHIRString|FHIRBoolean|FHIRInteger|FHIRRange|FHIRAttachment|null $value,
    ): self
    {
        $this->value = $value;

        return $this;
    }
}
