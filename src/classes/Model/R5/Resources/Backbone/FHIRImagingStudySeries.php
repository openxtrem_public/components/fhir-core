<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImagingStudySeries Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImagingStudySeriesInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUnsignedInt;

class FHIRImagingStudySeries extends FHIRBackboneElement implements FHIRImagingStudySeriesInterface
{
    public const RESOURCE_NAME = 'ImagingStudy.series';

    protected ?FHIRId $uid = null;
    protected ?FHIRUnsignedInt $number = null;
    protected ?FHIRCodeableConcept $modality = null;
    protected ?FHIRString $description = null;
    protected ?FHIRUnsignedInt $numberOfInstances = null;

    /** @var FHIRReference[] */
    protected array $endpoint = [];
    protected ?FHIRCodeableReference $bodySite = null;
    protected ?FHIRCodeableConcept $laterality = null;

    /** @var FHIRReference[] */
    protected array $specimen = [];
    protected ?FHIRDateTime $started = null;

    /** @var FHIRImagingStudySeriesPerformer[] */
    protected array $performer = [];

    /** @var FHIRImagingStudySeriesInstance[] */
    protected array $instance = [];

    public function getUid(): ?FHIRId
    {
        return $this->uid;
    }

    public function setUid(string|FHIRId|null $value): self
    {
        $this->uid = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getNumber(): ?FHIRUnsignedInt
    {
        return $this->number;
    }

    public function setNumber(int|FHIRUnsignedInt|null $value): self
    {
        $this->number = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getModality(): ?FHIRCodeableConcept
    {
        return $this->modality;
    }

    public function setModality(?FHIRCodeableConcept $value): self
    {
        $this->modality = $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getNumberOfInstances(): ?FHIRUnsignedInt
    {
        return $this->numberOfInstances;
    }

    public function setNumberOfInstances(int|FHIRUnsignedInt|null $value): self
    {
        $this->numberOfInstances = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEndpoint(): array
    {
        return $this->endpoint;
    }

    public function setEndpoint(?FHIRReference ...$value): self
    {
        $this->endpoint = array_filter($value);

        return $this;
    }

    public function addEndpoint(?FHIRReference ...$value): self
    {
        $this->endpoint = array_filter(array_merge($this->endpoint, $value));

        return $this;
    }

    public function getBodySite(): ?FHIRCodeableReference
    {
        return $this->bodySite;
    }

    public function setBodySite(?FHIRCodeableReference $value): self
    {
        $this->bodySite = $value;

        return $this;
    }

    public function getLaterality(): ?FHIRCodeableConcept
    {
        return $this->laterality;
    }

    public function setLaterality(?FHIRCodeableConcept $value): self
    {
        $this->laterality = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSpecimen(): array
    {
        return $this->specimen;
    }

    public function setSpecimen(?FHIRReference ...$value): self
    {
        $this->specimen = array_filter($value);

        return $this;
    }

    public function addSpecimen(?FHIRReference ...$value): self
    {
        $this->specimen = array_filter(array_merge($this->specimen, $value));

        return $this;
    }

    public function getStarted(): ?FHIRDateTime
    {
        return $this->started;
    }

    public function setStarted(string|FHIRDateTime|null $value): self
    {
        $this->started = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRImagingStudySeriesPerformer[]
     */
    public function getPerformer(): array
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRImagingStudySeriesPerformer ...$value): self
    {
        $this->performer = array_filter($value);

        return $this;
    }

    public function addPerformer(?FHIRImagingStudySeriesPerformer ...$value): self
    {
        $this->performer = array_filter(array_merge($this->performer, $value));

        return $this;
    }

    /**
     * @return FHIRImagingStudySeriesInstance[]
     */
    public function getInstance(): array
    {
        return $this->instance;
    }

    public function setInstance(?FHIRImagingStudySeriesInstance ...$value): self
    {
        $this->instance = array_filter($value);

        return $this;
    }

    public function addInstance(?FHIRImagingStudySeriesInstance ...$value): self
    {
        $this->instance = array_filter(array_merge($this->instance, $value));

        return $this;
    }
}
