<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExplanationOfBenefitItemReviewOutcome Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExplanationOfBenefitItemReviewOutcomeInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRExplanationOfBenefitItemReviewOutcome extends FHIRBackboneElement implements FHIRExplanationOfBenefitItemReviewOutcomeInterface
{
    public const RESOURCE_NAME = 'ExplanationOfBenefit.item.reviewOutcome';

    protected ?FHIRCodeableConcept $decision = null;

    /** @var FHIRCodeableConcept[] */
    protected array $reason = [];
    protected ?FHIRString $preAuthRef = null;
    protected ?FHIRPeriod $preAuthPeriod = null;

    public function getDecision(): ?FHIRCodeableConcept
    {
        return $this->decision;
    }

    public function setDecision(?FHIRCodeableConcept $value): self
    {
        $this->decision = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReason(): array
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableConcept ...$value): self
    {
        $this->reason = array_filter($value);

        return $this;
    }

    public function addReason(?FHIRCodeableConcept ...$value): self
    {
        $this->reason = array_filter(array_merge($this->reason, $value));

        return $this;
    }

    public function getPreAuthRef(): ?FHIRString
    {
        return $this->preAuthRef;
    }

    public function setPreAuthRef(string|FHIRString|null $value): self
    {
        $this->preAuthRef = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPreAuthPeriod(): ?FHIRPeriod
    {
        return $this->preAuthPeriod;
    }

    public function setPreAuthPeriod(?FHIRPeriod $value): self
    {
        $this->preAuthPeriod = $value;

        return $this;
    }
}
