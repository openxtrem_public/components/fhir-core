<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExplanationOfBenefitAddItemBodySite Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExplanationOfBenefitAddItemBodySiteInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;

class FHIRExplanationOfBenefitAddItemBodySite extends FHIRBackboneElement implements FHIRExplanationOfBenefitAddItemBodySiteInterface
{
    public const RESOURCE_NAME = 'ExplanationOfBenefit.addItem.bodySite';

    /** @var FHIRCodeableReference[] */
    protected array $site = [];

    /** @var FHIRCodeableConcept[] */
    protected array $subSite = [];

    /**
     * @return FHIRCodeableReference[]
     */
    public function getSite(): array
    {
        return $this->site;
    }

    public function setSite(?FHIRCodeableReference ...$value): self
    {
        $this->site = array_filter($value);

        return $this;
    }

    public function addSite(?FHIRCodeableReference ...$value): self
    {
        $this->site = array_filter(array_merge($this->site, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSubSite(): array
    {
        return $this->subSite;
    }

    public function setSubSite(?FHIRCodeableConcept ...$value): self
    {
        $this->subSite = array_filter($value);

        return $this;
    }

    public function addSubSite(?FHIRCodeableConcept ...$value): self
    {
        $this->subSite = array_filter(array_merge($this->subSite, $value));

        return $this;
    }
}
