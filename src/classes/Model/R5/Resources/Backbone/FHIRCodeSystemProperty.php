<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CodeSystemProperty Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCodeSystemPropertyInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;

class FHIRCodeSystemProperty extends FHIRBackboneElement implements FHIRCodeSystemPropertyInterface
{
    public const RESOURCE_NAME = 'CodeSystem.property';

    protected ?FHIRCode $code = null;
    protected ?FHIRUri $uri = null;
    protected ?FHIRString $description = null;
    protected ?FHIRCode $type = null;

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getUri(): ?FHIRUri
    {
        return $this->uri;
    }

    public function setUri(string|FHIRUri|null $value): self
    {
        $this->uri = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }
}
