<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestPlanTestCaseTestRunScript Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestPlanTestCaseTestRunScriptInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRTestPlanTestCaseTestRunScript extends FHIRBackboneElement implements FHIRTestPlanTestCaseTestRunScriptInterface
{
    public const RESOURCE_NAME = 'TestPlan.testCase.testRun.script';

    protected ?FHIRCodeableConcept $language = null;
    protected FHIRString|FHIRReference|null $source = null;

    public function getLanguage(): ?FHIRCodeableConcept
    {
        return $this->language;
    }

    public function setLanguage(?FHIRCodeableConcept $value): self
    {
        $this->language = $value;

        return $this;
    }

    public function getSource(): FHIRString|FHIRReference|null
    {
        return $this->source;
    }

    public function setSource(FHIRString|FHIRReference|null $value): self
    {
        $this->source = $value;

        return $this;
    }
}
