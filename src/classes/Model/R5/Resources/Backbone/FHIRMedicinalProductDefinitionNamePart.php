<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductDefinitionNamePart Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductDefinitionNamePartInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRMedicinalProductDefinitionNamePart extends FHIRBackboneElement implements FHIRMedicinalProductDefinitionNamePartInterface
{
    public const RESOURCE_NAME = 'MedicinalProductDefinition.name.part';

    protected ?FHIRString $part = null;
    protected ?FHIRCodeableConcept $type = null;

    public function getPart(): ?FHIRString
    {
        return $this->part;
    }

    public function setPart(string|FHIRString|null $value): self
    {
        $this->part = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }
}
