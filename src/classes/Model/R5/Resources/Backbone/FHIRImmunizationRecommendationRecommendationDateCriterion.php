<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImmunizationRecommendationRecommendationDateCriterion Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImmunizationRecommendationRecommendationDateCriterionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;

class FHIRImmunizationRecommendationRecommendationDateCriterion extends FHIRBackboneElement implements FHIRImmunizationRecommendationRecommendationDateCriterionInterface
{
    public const RESOURCE_NAME = 'ImmunizationRecommendation.recommendation.dateCriterion';

    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRDateTime $value = null;

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getValue(): ?FHIRDateTime
    {
        return $this->value;
    }

    public function setValue(string|FHIRDateTime|null $value): self
    {
        $this->value = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }
}
