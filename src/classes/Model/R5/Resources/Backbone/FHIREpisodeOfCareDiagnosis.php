<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EpisodeOfCareDiagnosis Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREpisodeOfCareDiagnosisInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;

class FHIREpisodeOfCareDiagnosis extends FHIRBackboneElement implements FHIREpisodeOfCareDiagnosisInterface
{
    public const RESOURCE_NAME = 'EpisodeOfCare.diagnosis';

    /** @var FHIRCodeableReference[] */
    protected array $condition = [];
    protected ?FHIRCodeableConcept $use = null;

    /**
     * @return FHIRCodeableReference[]
     */
    public function getCondition(): array
    {
        return $this->condition;
    }

    public function setCondition(?FHIRCodeableReference ...$value): self
    {
        $this->condition = array_filter($value);

        return $this;
    }

    public function addCondition(?FHIRCodeableReference ...$value): self
    {
        $this->condition = array_filter(array_merge($this->condition, $value));

        return $this;
    }

    public function getUse(): ?FHIRCodeableConcept
    {
        return $this->use;
    }

    public function setUse(?FHIRCodeableConcept $value): self
    {
        $this->use = $value;

        return $this;
    }
}
