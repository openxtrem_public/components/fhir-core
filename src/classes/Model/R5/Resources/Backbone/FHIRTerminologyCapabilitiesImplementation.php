<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TerminologyCapabilitiesImplementation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTerminologyCapabilitiesImplementationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUrl;

class FHIRTerminologyCapabilitiesImplementation extends FHIRBackboneElement implements FHIRTerminologyCapabilitiesImplementationInterface
{
    public const RESOURCE_NAME = 'TerminologyCapabilities.implementation';

    protected ?FHIRString $description = null;
    protected ?FHIRUrl $url = null;

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getUrl(): ?FHIRUrl
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUrl|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUrl())->setValue($value) : $value;

        return $this;
    }
}
