<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClaimItemDetailSubDetail Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRClaimItemDetailSubDetailInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;

class FHIRClaimItemDetailSubDetail extends FHIRBackboneElement implements FHIRClaimItemDetailSubDetailInterface
{
    public const RESOURCE_NAME = 'Claim.item.detail.subDetail';

    protected ?FHIRPositiveInt $sequence = null;

    /** @var FHIRIdentifier[] */
    protected array $traceNumber = [];
    protected ?FHIRCodeableConcept $revenue = null;
    protected ?FHIRCodeableConcept $category = null;
    protected ?FHIRCodeableConcept $productOrService = null;
    protected ?FHIRCodeableConcept $productOrServiceEnd = null;

    /** @var FHIRCodeableConcept[] */
    protected array $modifier = [];

    /** @var FHIRCodeableConcept[] */
    protected array $programCode = [];
    protected ?FHIRMoney $patientPaid = null;
    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRMoney $unitPrice = null;
    protected ?FHIRDecimal $factor = null;
    protected ?FHIRMoney $tax = null;
    protected ?FHIRMoney $net = null;

    /** @var FHIRReference[] */
    protected array $udi = [];

    public function getSequence(): ?FHIRPositiveInt
    {
        return $this->sequence;
    }

    public function setSequence(int|FHIRPositiveInt|null $value): self
    {
        $this->sequence = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getTraceNumber(): array
    {
        return $this->traceNumber;
    }

    public function setTraceNumber(?FHIRIdentifier ...$value): self
    {
        $this->traceNumber = array_filter($value);

        return $this;
    }

    public function addTraceNumber(?FHIRIdentifier ...$value): self
    {
        $this->traceNumber = array_filter(array_merge($this->traceNumber, $value));

        return $this;
    }

    public function getRevenue(): ?FHIRCodeableConcept
    {
        return $this->revenue;
    }

    public function setRevenue(?FHIRCodeableConcept $value): self
    {
        $this->revenue = $value;

        return $this;
    }

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getProductOrService(): ?FHIRCodeableConcept
    {
        return $this->productOrService;
    }

    public function setProductOrService(?FHIRCodeableConcept $value): self
    {
        $this->productOrService = $value;

        return $this;
    }

    public function getProductOrServiceEnd(): ?FHIRCodeableConcept
    {
        return $this->productOrServiceEnd;
    }

    public function setProductOrServiceEnd(?FHIRCodeableConcept $value): self
    {
        $this->productOrServiceEnd = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getModifier(): array
    {
        return $this->modifier;
    }

    public function setModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->modifier = array_filter($value);

        return $this;
    }

    public function addModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->modifier = array_filter(array_merge($this->modifier, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getProgramCode(): array
    {
        return $this->programCode;
    }

    public function setProgramCode(?FHIRCodeableConcept ...$value): self
    {
        $this->programCode = array_filter($value);

        return $this;
    }

    public function addProgramCode(?FHIRCodeableConcept ...$value): self
    {
        $this->programCode = array_filter(array_merge($this->programCode, $value));

        return $this;
    }

    public function getPatientPaid(): ?FHIRMoney
    {
        return $this->patientPaid;
    }

    public function setPatientPaid(?FHIRMoney $value): self
    {
        $this->patientPaid = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getUnitPrice(): ?FHIRMoney
    {
        return $this->unitPrice;
    }

    public function setUnitPrice(?FHIRMoney $value): self
    {
        $this->unitPrice = $value;

        return $this;
    }

    public function getFactor(): ?FHIRDecimal
    {
        return $this->factor;
    }

    public function setFactor(float|FHIRDecimal|null $value): self
    {
        $this->factor = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getTax(): ?FHIRMoney
    {
        return $this->tax;
    }

    public function setTax(?FHIRMoney $value): self
    {
        $this->tax = $value;

        return $this;
    }

    public function getNet(): ?FHIRMoney
    {
        return $this->net;
    }

    public function setNet(?FHIRMoney $value): self
    {
        $this->net = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getUdi(): array
    {
        return $this->udi;
    }

    public function setUdi(?FHIRReference ...$value): self
    {
        $this->udi = array_filter($value);

        return $this;
    }

    public function addUdi(?FHIRReference ...$value): self
    {
        $this->udi = array_filter(array_merge($this->udi, $value));

        return $this;
    }
}
