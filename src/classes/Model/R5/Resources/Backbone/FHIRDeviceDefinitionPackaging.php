<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceDefinitionPackaging Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceDefinitionPackagingInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInteger;

class FHIRDeviceDefinitionPackaging extends FHIRBackboneElement implements FHIRDeviceDefinitionPackagingInterface
{
    public const RESOURCE_NAME = 'DeviceDefinition.packaging';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRInteger $count = null;

    /** @var FHIRDeviceDefinitionPackagingDistributor[] */
    protected array $distributor = [];

    /** @var FHIRDeviceDefinitionUdiDeviceIdentifier[] */
    protected array $udiDeviceIdentifier = [];

    /** @var FHIRDeviceDefinitionPackaging[] */
    protected array $packaging = [];

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getCount(): ?FHIRInteger
    {
        return $this->count;
    }

    public function setCount(int|FHIRInteger|null $value): self
    {
        $this->count = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRDeviceDefinitionPackagingDistributor[]
     */
    public function getDistributor(): array
    {
        return $this->distributor;
    }

    public function setDistributor(?FHIRDeviceDefinitionPackagingDistributor ...$value): self
    {
        $this->distributor = array_filter($value);

        return $this;
    }

    public function addDistributor(?FHIRDeviceDefinitionPackagingDistributor ...$value): self
    {
        $this->distributor = array_filter(array_merge($this->distributor, $value));

        return $this;
    }

    /**
     * @return FHIRDeviceDefinitionUdiDeviceIdentifier[]
     */
    public function getUdiDeviceIdentifier(): array
    {
        return $this->udiDeviceIdentifier;
    }

    public function setUdiDeviceIdentifier(?FHIRDeviceDefinitionUdiDeviceIdentifier ...$value): self
    {
        $this->udiDeviceIdentifier = array_filter($value);

        return $this;
    }

    public function addUdiDeviceIdentifier(?FHIRDeviceDefinitionUdiDeviceIdentifier ...$value): self
    {
        $this->udiDeviceIdentifier = array_filter(array_merge($this->udiDeviceIdentifier, $value));

        return $this;
    }

    /**
     * @return FHIRDeviceDefinitionPackaging[]
     */
    public function getPackaging(): array
    {
        return $this->packaging;
    }

    public function setPackaging(?FHIRDeviceDefinitionPackaging ...$value): self
    {
        $this->packaging = array_filter($value);

        return $this;
    }

    public function addPackaging(?FHIRDeviceDefinitionPackaging ...$value): self
    {
        $this->packaging = array_filter(array_merge($this->packaging, $value));

        return $this;
    }
}
