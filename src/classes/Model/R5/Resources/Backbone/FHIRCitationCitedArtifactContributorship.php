<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CitationCitedArtifactContributorship Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCitationCitedArtifactContributorshipInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;

class FHIRCitationCitedArtifactContributorship extends FHIRBackboneElement implements FHIRCitationCitedArtifactContributorshipInterface
{
    public const RESOURCE_NAME = 'Citation.citedArtifact.contributorship';

    protected ?FHIRBoolean $complete = null;

    /** @var FHIRCitationCitedArtifactContributorshipEntry[] */
    protected array $entry = [];

    /** @var FHIRCitationCitedArtifactContributorshipSummary[] */
    protected array $summary = [];

    public function getComplete(): ?FHIRBoolean
    {
        return $this->complete;
    }

    public function setComplete(bool|FHIRBoolean|null $value): self
    {
        $this->complete = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCitationCitedArtifactContributorshipEntry[]
     */
    public function getEntry(): array
    {
        return $this->entry;
    }

    public function setEntry(?FHIRCitationCitedArtifactContributorshipEntry ...$value): self
    {
        $this->entry = array_filter($value);

        return $this;
    }

    public function addEntry(?FHIRCitationCitedArtifactContributorshipEntry ...$value): self
    {
        $this->entry = array_filter(array_merge($this->entry, $value));

        return $this;
    }

    /**
     * @return FHIRCitationCitedArtifactContributorshipSummary[]
     */
    public function getSummary(): array
    {
        return $this->summary;
    }

    public function setSummary(?FHIRCitationCitedArtifactContributorshipSummary ...$value): self
    {
        $this->summary = array_filter($value);

        return $this;
    }

    public function addSummary(?FHIRCitationCitedArtifactContributorshipSummary ...$value): self
    {
        $this->summary = array_filter(array_merge($this->summary, $value));

        return $this;
    }
}
