<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AccountDiagnosis Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAccountDiagnosisInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;

class FHIRAccountDiagnosis extends FHIRBackboneElement implements FHIRAccountDiagnosisInterface
{
    public const RESOURCE_NAME = 'Account.diagnosis';

    protected ?FHIRPositiveInt $sequence = null;
    protected ?FHIRCodeableReference $condition = null;
    protected ?FHIRDateTime $dateOfDiagnosis = null;

    /** @var FHIRCodeableConcept[] */
    protected array $type = [];
    protected ?FHIRBoolean $onAdmission = null;

    /** @var FHIRCodeableConcept[] */
    protected array $packageCode = [];

    public function getSequence(): ?FHIRPositiveInt
    {
        return $this->sequence;
    }

    public function setSequence(int|FHIRPositiveInt|null $value): self
    {
        $this->sequence = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getCondition(): ?FHIRCodeableReference
    {
        return $this->condition;
    }

    public function setCondition(?FHIRCodeableReference $value): self
    {
        $this->condition = $value;

        return $this;
    }

    public function getDateOfDiagnosis(): ?FHIRDateTime
    {
        return $this->dateOfDiagnosis;
    }

    public function setDateOfDiagnosis(string|FHIRDateTime|null $value): self
    {
        $this->dateOfDiagnosis = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }

    public function getOnAdmission(): ?FHIRBoolean
    {
        return $this->onAdmission;
    }

    public function setOnAdmission(bool|FHIRBoolean|null $value): self
    {
        $this->onAdmission = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getPackageCode(): array
    {
        return $this->packageCode;
    }

    public function setPackageCode(?FHIRCodeableConcept ...$value): self
    {
        $this->packageCode = array_filter($value);

        return $this;
    }

    public function addPackageCode(?FHIRCodeableConcept ...$value): self
    {
        $this->packageCode = array_filter(array_merge($this->packageCode, $value));

        return $this;
    }
}
