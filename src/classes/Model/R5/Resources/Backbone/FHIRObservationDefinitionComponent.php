<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ObservationDefinitionComponent Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRObservationDefinitionComponentInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;

class FHIRObservationDefinitionComponent extends FHIRBackboneElement implements FHIRObservationDefinitionComponentInterface
{
    public const RESOURCE_NAME = 'ObservationDefinition.component';

    protected ?FHIRCodeableConcept $code = null;

    /** @var FHIRCode[] */
    protected array $permittedDataType = [];

    /** @var FHIRCoding[] */
    protected array $permittedUnit = [];

    /** @var FHIRObservationDefinitionQualifiedValue[] */
    protected array $qualifiedValue = [];

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getPermittedDataType(): array
    {
        return $this->permittedDataType;
    }

    public function setPermittedDataType(string|FHIRCode|null ...$value): self
    {
        $this->permittedDataType = [];
        $this->addPermittedDataType(...$value);

        return $this;
    }

    public function addPermittedDataType(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->permittedDataType = array_filter(array_merge($this->permittedDataType, $values));

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getPermittedUnit(): array
    {
        return $this->permittedUnit;
    }

    public function setPermittedUnit(?FHIRCoding ...$value): self
    {
        $this->permittedUnit = array_filter($value);

        return $this;
    }

    public function addPermittedUnit(?FHIRCoding ...$value): self
    {
        $this->permittedUnit = array_filter(array_merge($this->permittedUnit, $value));

        return $this;
    }

    /**
     * @return FHIRObservationDefinitionQualifiedValue[]
     */
    public function getQualifiedValue(): array
    {
        return $this->qualifiedValue;
    }

    public function setQualifiedValue(?FHIRObservationDefinitionQualifiedValue ...$value): self
    {
        $this->qualifiedValue = array_filter($value);

        return $this;
    }

    public function addQualifiedValue(?FHIRObservationDefinitionQualifiedValue ...$value): self
    {
        $this->qualifiedValue = array_filter(array_merge($this->qualifiedValue, $value));

        return $this;
    }
}
