<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MolecularSequenceRelativeStartingSequence Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMolecularSequenceRelativeStartingSequenceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRMolecularSequenceRelativeStartingSequence extends FHIRBackboneElement implements FHIRMolecularSequenceRelativeStartingSequenceInterface
{
    public const RESOURCE_NAME = 'MolecularSequence.relative.startingSequence';

    protected ?FHIRCodeableConcept $genomeAssembly = null;
    protected ?FHIRCodeableConcept $chromosome = null;
    protected FHIRCodeableConcept|FHIRString|FHIRReference|null $sequence = null;
    protected ?FHIRInteger $windowStart = null;
    protected ?FHIRInteger $windowEnd = null;
    protected ?FHIRCode $orientation = null;
    protected ?FHIRCode $strand = null;

    public function getGenomeAssembly(): ?FHIRCodeableConcept
    {
        return $this->genomeAssembly;
    }

    public function setGenomeAssembly(?FHIRCodeableConcept $value): self
    {
        $this->genomeAssembly = $value;

        return $this;
    }

    public function getChromosome(): ?FHIRCodeableConcept
    {
        return $this->chromosome;
    }

    public function setChromosome(?FHIRCodeableConcept $value): self
    {
        $this->chromosome = $value;

        return $this;
    }

    public function getSequence(): FHIRCodeableConcept|FHIRString|FHIRReference|null
    {
        return $this->sequence;
    }

    public function setSequence(FHIRCodeableConcept|FHIRString|FHIRReference|null $value): self
    {
        $this->sequence = $value;

        return $this;
    }

    public function getWindowStart(): ?FHIRInteger
    {
        return $this->windowStart;
    }

    public function setWindowStart(int|FHIRInteger|null $value): self
    {
        $this->windowStart = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getWindowEnd(): ?FHIRInteger
    {
        return $this->windowEnd;
    }

    public function setWindowEnd(int|FHIRInteger|null $value): self
    {
        $this->windowEnd = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getOrientation(): ?FHIRCode
    {
        return $this->orientation;
    }

    public function setOrientation(string|FHIRCode|null $value): self
    {
        $this->orientation = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getStrand(): ?FHIRCode
    {
        return $this->strand;
    }

    public function setStrand(string|FHIRCode|null $value): self
    {
        $this->strand = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }
}
