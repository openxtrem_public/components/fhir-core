<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImmunizationProgramEligibility Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImmunizationProgramEligibilityInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;

class FHIRImmunizationProgramEligibility extends FHIRBackboneElement implements FHIRImmunizationProgramEligibilityInterface
{
    public const RESOURCE_NAME = 'Immunization.programEligibility';

    protected ?FHIRCodeableConcept $program = null;
    protected ?FHIRCodeableConcept $programStatus = null;

    public function getProgram(): ?FHIRCodeableConcept
    {
        return $this->program;
    }

    public function setProgram(?FHIRCodeableConcept $value): self
    {
        $this->program = $value;

        return $this;
    }

    public function getProgramStatus(): ?FHIRCodeableConcept
    {
        return $this->programStatus;
    }

    public function setProgramStatus(?FHIRCodeableConcept $value): self
    {
        $this->programStatus = $value;

        return $this;
    }
}
