<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ContractTermSecurityLabel Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRContractTermSecurityLabelInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUnsignedInt;

class FHIRContractTermSecurityLabel extends FHIRBackboneElement implements FHIRContractTermSecurityLabelInterface
{
    public const RESOURCE_NAME = 'Contract.term.securityLabel';

    /** @var FHIRUnsignedInt[] */
    protected array $number = [];
    protected ?FHIRCoding $classification = null;

    /** @var FHIRCoding[] */
    protected array $category = [];

    /** @var FHIRCoding[] */
    protected array $control = [];

    /**
     * @return FHIRUnsignedInt[]
     */
    public function getNumber(): array
    {
        return $this->number;
    }

    public function setNumber(int|FHIRUnsignedInt|null ...$value): self
    {
        $this->number = [];
        $this->addNumber(...$value);

        return $this;
    }

    public function addNumber(int|FHIRUnsignedInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRUnsignedInt())->setValue($v) : $v, $value);

        $this->number = array_filter(array_merge($this->number, $values));

        return $this;
    }

    public function getClassification(): ?FHIRCoding
    {
        return $this->classification;
    }

    public function setClassification(?FHIRCoding $value): self
    {
        $this->classification = $value;

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCoding ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCoding ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getControl(): array
    {
        return $this->control;
    }

    public function setControl(?FHIRCoding ...$value): self
    {
        $this->control = array_filter($value);

        return $this;
    }

    public function addControl(?FHIRCoding ...$value): self
    {
        $this->control = array_filter(array_merge($this->control, $value));

        return $this;
    }
}
