<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledgeCost Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationKnowledgeCostInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRMedicationKnowledgeCost extends FHIRBackboneElement implements FHIRMedicationKnowledgeCostInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge.cost';

    /** @var FHIRPeriod[] */
    protected array $effectiveDate = [];
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRString $source = null;
    protected FHIRMoney|FHIRCodeableConcept|null $cost = null;

    /**
     * @return FHIRPeriod[]
     */
    public function getEffectiveDate(): array
    {
        return $this->effectiveDate;
    }

    public function setEffectiveDate(?FHIRPeriod ...$value): self
    {
        $this->effectiveDate = array_filter($value);

        return $this;
    }

    public function addEffectiveDate(?FHIRPeriod ...$value): self
    {
        $this->effectiveDate = array_filter(array_merge($this->effectiveDate, $value));

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getSource(): ?FHIRString
    {
        return $this->source;
    }

    public function setSource(string|FHIRString|null $value): self
    {
        $this->source = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCost(): FHIRMoney|FHIRCodeableConcept|null
    {
        return $this->cost;
    }

    public function setCost(FHIRMoney|FHIRCodeableConcept|null $value): self
    {
        $this->cost = $value;

        return $this;
    }
}
