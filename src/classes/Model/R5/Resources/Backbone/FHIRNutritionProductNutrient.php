<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR NutritionProductNutrient Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRNutritionProductNutrientInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRatio;

class FHIRNutritionProductNutrient extends FHIRBackboneElement implements FHIRNutritionProductNutrientInterface
{
    public const RESOURCE_NAME = 'NutritionProduct.nutrient';

    protected ?FHIRCodeableReference $item = null;

    /** @var FHIRRatio[] */
    protected array $amount = [];

    public function getItem(): ?FHIRCodeableReference
    {
        return $this->item;
    }

    public function setItem(?FHIRCodeableReference $value): self
    {
        $this->item = $value;

        return $this;
    }

    /**
     * @return FHIRRatio[]
     */
    public function getAmount(): array
    {
        return $this->amount;
    }

    public function setAmount(?FHIRRatio ...$value): self
    {
        $this->amount = array_filter($value);

        return $this;
    }

    public function addAmount(?FHIRRatio ...$value): self
    {
        $this->amount = array_filter(array_merge($this->amount, $value));

        return $this;
    }
}
