<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceNucleicAcidSubunitLinkage Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceNucleicAcidSubunitLinkageInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRSubstanceNucleicAcidSubunitLinkage extends FHIRBackboneElement implements FHIRSubstanceNucleicAcidSubunitLinkageInterface
{
    public const RESOURCE_NAME = 'SubstanceNucleicAcid.subunit.linkage';

    protected ?FHIRString $connectivity = null;
    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRString $name = null;
    protected ?FHIRString $residueSite = null;

    public function getConnectivity(): ?FHIRString
    {
        return $this->connectivity;
    }

    public function setConnectivity(string|FHIRString|null $value): self
    {
        $this->connectivity = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getResidueSite(): ?FHIRString
    {
        return $this->residueSite;
    }

    public function setResidueSite(string|FHIRString|null $value): self
    {
        $this->residueSite = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
