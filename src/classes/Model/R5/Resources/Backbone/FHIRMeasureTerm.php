<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MeasureTerm Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMeasureTermInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;

class FHIRMeasureTerm extends FHIRBackboneElement implements FHIRMeasureTermInterface
{
    public const RESOURCE_NAME = 'Measure.term';

    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRMarkdown $definition = null;

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getDefinition(): ?FHIRMarkdown
    {
        return $this->definition;
    }

    public function setDefinition(string|FHIRMarkdown|null $value): self
    {
        $this->definition = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }
}
