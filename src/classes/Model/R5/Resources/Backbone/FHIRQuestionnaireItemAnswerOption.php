<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR QuestionnaireItemAnswerOption Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRQuestionnaireItemAnswerOptionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRTime;

class FHIRQuestionnaireItemAnswerOption extends FHIRBackboneElement implements FHIRQuestionnaireItemAnswerOptionInterface
{
    public const RESOURCE_NAME = 'Questionnaire.item.answerOption';

    protected FHIRInteger|FHIRDate|FHIRTime|FHIRString|FHIRCoding|FHIRReference|null $value = null;
    protected ?FHIRBoolean $initialSelected = null;

    public function getValue(): FHIRInteger|FHIRDate|FHIRTime|FHIRString|FHIRCoding|FHIRReference|null
    {
        return $this->value;
    }

    public function setValue(FHIRInteger|FHIRDate|FHIRTime|FHIRString|FHIRCoding|FHIRReference|null $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getInitialSelected(): ?FHIRBoolean
    {
        return $this->initialSelected;
    }

    public function setInitialSelected(bool|FHIRBoolean|null $value): self
    {
        $this->initialSelected = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }
}
