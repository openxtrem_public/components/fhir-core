<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClinicalUseDefinitionInteraction Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRClinicalUseDefinitionInteractionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;

class FHIRClinicalUseDefinitionInteraction extends FHIRBackboneElement implements FHIRClinicalUseDefinitionInteractionInterface
{
    public const RESOURCE_NAME = 'ClinicalUseDefinition.interaction';

    /** @var FHIRClinicalUseDefinitionInteractionInteractant[] */
    protected array $interactant = [];
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableReference $effect = null;
    protected ?FHIRCodeableConcept $incidence = null;

    /** @var FHIRCodeableConcept[] */
    protected array $management = [];

    /**
     * @return FHIRClinicalUseDefinitionInteractionInteractant[]
     */
    public function getInteractant(): array
    {
        return $this->interactant;
    }

    public function setInteractant(?FHIRClinicalUseDefinitionInteractionInteractant ...$value): self
    {
        $this->interactant = array_filter($value);

        return $this;
    }

    public function addInteractant(?FHIRClinicalUseDefinitionInteractionInteractant ...$value): self
    {
        $this->interactant = array_filter(array_merge($this->interactant, $value));

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getEffect(): ?FHIRCodeableReference
    {
        return $this->effect;
    }

    public function setEffect(?FHIRCodeableReference $value): self
    {
        $this->effect = $value;

        return $this;
    }

    public function getIncidence(): ?FHIRCodeableConcept
    {
        return $this->incidence;
    }

    public function setIncidence(?FHIRCodeableConcept $value): self
    {
        $this->incidence = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getManagement(): array
    {
        return $this->management;
    }

    public function setManagement(?FHIRCodeableConcept ...$value): self
    {
        $this->management = array_filter($value);

        return $this;
    }

    public function addManagement(?FHIRCodeableConcept ...$value): self
    {
        $this->management = array_filter(array_merge($this->management, $value));

        return $this;
    }
}
