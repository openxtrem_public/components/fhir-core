<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SpecimenDefinitionTypeTestedContainerAdditive Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSpecimenDefinitionTypeTestedContainerAdditiveInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRSpecimenDefinitionTypeTestedContainerAdditive extends FHIRBackboneElement implements FHIRSpecimenDefinitionTypeTestedContainerAdditiveInterface
{
    public const RESOURCE_NAME = 'SpecimenDefinition.typeTested.container.additive';

    protected FHIRCodeableConcept|FHIRReference|null $additive = null;

    public function getAdditive(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->additive;
    }

    public function setAdditive(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->additive = $value;

        return $this;
    }
}
