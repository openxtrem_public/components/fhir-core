<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TerminologyCapabilitiesCodeSystem Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTerminologyCapabilitiesCodeSystemInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;

class FHIRTerminologyCapabilitiesCodeSystem extends FHIRBackboneElement implements FHIRTerminologyCapabilitiesCodeSystemInterface
{
    public const RESOURCE_NAME = 'TerminologyCapabilities.codeSystem';

    protected ?FHIRCanonical $uri = null;

    /** @var FHIRTerminologyCapabilitiesCodeSystemVersion[] */
    protected array $version = [];
    protected ?FHIRCode $content = null;
    protected ?FHIRBoolean $subsumption = null;

    public function getUri(): ?FHIRCanonical
    {
        return $this->uri;
    }

    public function setUri(string|FHIRCanonical|null $value): self
    {
        $this->uri = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRTerminologyCapabilitiesCodeSystemVersion[]
     */
    public function getVersion(): array
    {
        return $this->version;
    }

    public function setVersion(?FHIRTerminologyCapabilitiesCodeSystemVersion ...$value): self
    {
        $this->version = array_filter($value);

        return $this;
    }

    public function addVersion(?FHIRTerminologyCapabilitiesCodeSystemVersion ...$value): self
    {
        $this->version = array_filter(array_merge($this->version, $value));

        return $this;
    }

    public function getContent(): ?FHIRCode
    {
        return $this->content;
    }

    public function setContent(string|FHIRCode|null $value): self
    {
        $this->content = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getSubsumption(): ?FHIRBoolean
    {
        return $this->subsumption;
    }

    public function setSubsumption(bool|FHIRBoolean|null $value): self
    {
        $this->subsumption = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }
}
