<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExplanationOfBenefitAddItem Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExplanationOfBenefitAddItemInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAddress;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;

class FHIRExplanationOfBenefitAddItem extends FHIRBackboneElement implements FHIRExplanationOfBenefitAddItemInterface
{
    public const RESOURCE_NAME = 'ExplanationOfBenefit.addItem';

    /** @var FHIRPositiveInt[] */
    protected array $itemSequence = [];

    /** @var FHIRPositiveInt[] */
    protected array $detailSequence = [];

    /** @var FHIRPositiveInt[] */
    protected array $subDetailSequence = [];

    /** @var FHIRIdentifier[] */
    protected array $traceNumber = [];

    /** @var FHIRReference[] */
    protected array $provider = [];
    protected ?FHIRCodeableConcept $revenue = null;
    protected ?FHIRCodeableConcept $productOrService = null;
    protected ?FHIRCodeableConcept $productOrServiceEnd = null;

    /** @var FHIRReference[] */
    protected array $request = [];

    /** @var FHIRCodeableConcept[] */
    protected array $modifier = [];

    /** @var FHIRCodeableConcept[] */
    protected array $programCode = [];
    protected FHIRDate|FHIRPeriod|null $serviced = null;
    protected FHIRCodeableConcept|FHIRAddress|FHIRReference|null $location = null;
    protected ?FHIRMoney $patientPaid = null;
    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRMoney $unitPrice = null;
    protected ?FHIRDecimal $factor = null;
    protected ?FHIRMoney $tax = null;
    protected ?FHIRMoney $net = null;

    /** @var FHIRExplanationOfBenefitAddItemBodySite[] */
    protected array $bodySite = [];

    /** @var FHIRPositiveInt[] */
    protected array $noteNumber = [];
    protected ?FHIRExplanationOfBenefitItemReviewOutcome $reviewOutcome = null;

    /** @var FHIRExplanationOfBenefitItemAdjudication[] */
    protected array $adjudication = [];

    /** @var FHIRExplanationOfBenefitAddItemDetail[] */
    protected array $detail = [];

    /**
     * @return FHIRPositiveInt[]
     */
    public function getItemSequence(): array
    {
        return $this->itemSequence;
    }

    public function setItemSequence(int|FHIRPositiveInt|null ...$value): self
    {
        $this->itemSequence = [];
        $this->addItemSequence(...$value);

        return $this;
    }

    public function addItemSequence(int|FHIRPositiveInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRPositiveInt())->setValue($v) : $v, $value);

        $this->itemSequence = array_filter(array_merge($this->itemSequence, $values));

        return $this;
    }

    /**
     * @return FHIRPositiveInt[]
     */
    public function getDetailSequence(): array
    {
        return $this->detailSequence;
    }

    public function setDetailSequence(int|FHIRPositiveInt|null ...$value): self
    {
        $this->detailSequence = [];
        $this->addDetailSequence(...$value);

        return $this;
    }

    public function addDetailSequence(int|FHIRPositiveInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRPositiveInt())->setValue($v) : $v, $value);

        $this->detailSequence = array_filter(array_merge($this->detailSequence, $values));

        return $this;
    }

    /**
     * @return FHIRPositiveInt[]
     */
    public function getSubDetailSequence(): array
    {
        return $this->subDetailSequence;
    }

    public function setSubDetailSequence(int|FHIRPositiveInt|null ...$value): self
    {
        $this->subDetailSequence = [];
        $this->addSubDetailSequence(...$value);

        return $this;
    }

    public function addSubDetailSequence(int|FHIRPositiveInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRPositiveInt())->setValue($v) : $v, $value);

        $this->subDetailSequence = array_filter(array_merge($this->subDetailSequence, $values));

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getTraceNumber(): array
    {
        return $this->traceNumber;
    }

    public function setTraceNumber(?FHIRIdentifier ...$value): self
    {
        $this->traceNumber = array_filter($value);

        return $this;
    }

    public function addTraceNumber(?FHIRIdentifier ...$value): self
    {
        $this->traceNumber = array_filter(array_merge($this->traceNumber, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getProvider(): array
    {
        return $this->provider;
    }

    public function setProvider(?FHIRReference ...$value): self
    {
        $this->provider = array_filter($value);

        return $this;
    }

    public function addProvider(?FHIRReference ...$value): self
    {
        $this->provider = array_filter(array_merge($this->provider, $value));

        return $this;
    }

    public function getRevenue(): ?FHIRCodeableConcept
    {
        return $this->revenue;
    }

    public function setRevenue(?FHIRCodeableConcept $value): self
    {
        $this->revenue = $value;

        return $this;
    }

    public function getProductOrService(): ?FHIRCodeableConcept
    {
        return $this->productOrService;
    }

    public function setProductOrService(?FHIRCodeableConcept $value): self
    {
        $this->productOrService = $value;

        return $this;
    }

    public function getProductOrServiceEnd(): ?FHIRCodeableConcept
    {
        return $this->productOrServiceEnd;
    }

    public function setProductOrServiceEnd(?FHIRCodeableConcept $value): self
    {
        $this->productOrServiceEnd = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getRequest(): array
    {
        return $this->request;
    }

    public function setRequest(?FHIRReference ...$value): self
    {
        $this->request = array_filter($value);

        return $this;
    }

    public function addRequest(?FHIRReference ...$value): self
    {
        $this->request = array_filter(array_merge($this->request, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getModifier(): array
    {
        return $this->modifier;
    }

    public function setModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->modifier = array_filter($value);

        return $this;
    }

    public function addModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->modifier = array_filter(array_merge($this->modifier, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getProgramCode(): array
    {
        return $this->programCode;
    }

    public function setProgramCode(?FHIRCodeableConcept ...$value): self
    {
        $this->programCode = array_filter($value);

        return $this;
    }

    public function addProgramCode(?FHIRCodeableConcept ...$value): self
    {
        $this->programCode = array_filter(array_merge($this->programCode, $value));

        return $this;
    }

    public function getServiced(): FHIRDate|FHIRPeriod|null
    {
        return $this->serviced;
    }

    public function setServiced(FHIRDate|FHIRPeriod|null $value): self
    {
        $this->serviced = $value;

        return $this;
    }

    public function getLocation(): FHIRCodeableConcept|FHIRAddress|FHIRReference|null
    {
        return $this->location;
    }

    public function setLocation(FHIRCodeableConcept|FHIRAddress|FHIRReference|null $value): self
    {
        $this->location = $value;

        return $this;
    }

    public function getPatientPaid(): ?FHIRMoney
    {
        return $this->patientPaid;
    }

    public function setPatientPaid(?FHIRMoney $value): self
    {
        $this->patientPaid = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getUnitPrice(): ?FHIRMoney
    {
        return $this->unitPrice;
    }

    public function setUnitPrice(?FHIRMoney $value): self
    {
        $this->unitPrice = $value;

        return $this;
    }

    public function getFactor(): ?FHIRDecimal
    {
        return $this->factor;
    }

    public function setFactor(float|FHIRDecimal|null $value): self
    {
        $this->factor = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getTax(): ?FHIRMoney
    {
        return $this->tax;
    }

    public function setTax(?FHIRMoney $value): self
    {
        $this->tax = $value;

        return $this;
    }

    public function getNet(): ?FHIRMoney
    {
        return $this->net;
    }

    public function setNet(?FHIRMoney $value): self
    {
        $this->net = $value;

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitAddItemBodySite[]
     */
    public function getBodySite(): array
    {
        return $this->bodySite;
    }

    public function setBodySite(?FHIRExplanationOfBenefitAddItemBodySite ...$value): self
    {
        $this->bodySite = array_filter($value);

        return $this;
    }

    public function addBodySite(?FHIRExplanationOfBenefitAddItemBodySite ...$value): self
    {
        $this->bodySite = array_filter(array_merge($this->bodySite, $value));

        return $this;
    }

    /**
     * @return FHIRPositiveInt[]
     */
    public function getNoteNumber(): array
    {
        return $this->noteNumber;
    }

    public function setNoteNumber(int|FHIRPositiveInt|null ...$value): self
    {
        $this->noteNumber = [];
        $this->addNoteNumber(...$value);

        return $this;
    }

    public function addNoteNumber(int|FHIRPositiveInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRPositiveInt())->setValue($v) : $v, $value);

        $this->noteNumber = array_filter(array_merge($this->noteNumber, $values));

        return $this;
    }

    public function getReviewOutcome(): ?FHIRExplanationOfBenefitItemReviewOutcome
    {
        return $this->reviewOutcome;
    }

    public function setReviewOutcome(?FHIRExplanationOfBenefitItemReviewOutcome $value): self
    {
        $this->reviewOutcome = $value;

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitItemAdjudication[]
     */
    public function getAdjudication(): array
    {
        return $this->adjudication;
    }

    public function setAdjudication(?FHIRExplanationOfBenefitItemAdjudication ...$value): self
    {
        $this->adjudication = array_filter($value);

        return $this;
    }

    public function addAdjudication(?FHIRExplanationOfBenefitItemAdjudication ...$value): self
    {
        $this->adjudication = array_filter(array_merge($this->adjudication, $value));

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitAddItemDetail[]
     */
    public function getDetail(): array
    {
        return $this->detail;
    }

    public function setDetail(?FHIRExplanationOfBenefitAddItemDetail ...$value): self
    {
        $this->detail = array_filter($value);

        return $this;
    }

    public function addDetail(?FHIRExplanationOfBenefitAddItemDetail ...$value): self
    {
        $this->detail = array_filter(array_merge($this->detail, $value));

        return $this;
    }
}
