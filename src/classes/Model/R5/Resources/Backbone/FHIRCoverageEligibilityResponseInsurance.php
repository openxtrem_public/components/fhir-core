<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CoverageEligibilityResponseInsurance Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCoverageEligibilityResponseInsuranceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;

class FHIRCoverageEligibilityResponseInsurance extends FHIRBackboneElement implements FHIRCoverageEligibilityResponseInsuranceInterface
{
    public const RESOURCE_NAME = 'CoverageEligibilityResponse.insurance';

    protected ?FHIRReference $coverage = null;
    protected ?FHIRBoolean $inforce = null;
    protected ?FHIRPeriod $benefitPeriod = null;

    /** @var FHIRCoverageEligibilityResponseInsuranceItem[] */
    protected array $item = [];

    public function getCoverage(): ?FHIRReference
    {
        return $this->coverage;
    }

    public function setCoverage(?FHIRReference $value): self
    {
        $this->coverage = $value;

        return $this;
    }

    public function getInforce(): ?FHIRBoolean
    {
        return $this->inforce;
    }

    public function setInforce(bool|FHIRBoolean|null $value): self
    {
        $this->inforce = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getBenefitPeriod(): ?FHIRPeriod
    {
        return $this->benefitPeriod;
    }

    public function setBenefitPeriod(?FHIRPeriod $value): self
    {
        $this->benefitPeriod = $value;

        return $this;
    }

    /**
     * @return FHIRCoverageEligibilityResponseInsuranceItem[]
     */
    public function getItem(): array
    {
        return $this->item;
    }

    public function setItem(?FHIRCoverageEligibilityResponseInsuranceItem ...$value): self
    {
        $this->item = array_filter($value);

        return $this;
    }

    public function addItem(?FHIRCoverageEligibilityResponseInsuranceItem ...$value): self
    {
        $this->item = array_filter(array_merge($this->item, $value));

        return $this;
    }
}
