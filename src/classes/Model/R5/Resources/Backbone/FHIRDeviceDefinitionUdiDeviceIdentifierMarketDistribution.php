<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceDefinitionUdiDeviceIdentifierMarketDistribution Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceDefinitionUdiDeviceIdentifierMarketDistributionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;

class FHIRDeviceDefinitionUdiDeviceIdentifierMarketDistribution extends FHIRBackboneElement implements FHIRDeviceDefinitionUdiDeviceIdentifierMarketDistributionInterface
{
    public const RESOURCE_NAME = 'DeviceDefinition.udiDeviceIdentifier.marketDistribution';

    protected ?FHIRPeriod $marketPeriod = null;
    protected ?FHIRUri $subJurisdiction = null;

    public function getMarketPeriod(): ?FHIRPeriod
    {
        return $this->marketPeriod;
    }

    public function setMarketPeriod(?FHIRPeriod $value): self
    {
        $this->marketPeriod = $value;

        return $this;
    }

    public function getSubJurisdiction(): ?FHIRUri
    {
        return $this->subJurisdiction;
    }

    public function setSubJurisdiction(string|FHIRUri|null $value): self
    {
        $this->subJurisdiction = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }
}
