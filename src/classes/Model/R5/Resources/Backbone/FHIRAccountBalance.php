<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AccountBalance Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAccountBalanceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;

class FHIRAccountBalance extends FHIRBackboneElement implements FHIRAccountBalanceInterface
{
    public const RESOURCE_NAME = 'Account.balance';

    protected ?FHIRCodeableConcept $aggregate = null;
    protected ?FHIRCodeableConcept $term = null;
    protected ?FHIRBoolean $estimate = null;
    protected ?FHIRMoney $amount = null;

    public function getAggregate(): ?FHIRCodeableConcept
    {
        return $this->aggregate;
    }

    public function setAggregate(?FHIRCodeableConcept $value): self
    {
        $this->aggregate = $value;

        return $this;
    }

    public function getTerm(): ?FHIRCodeableConcept
    {
        return $this->term;
    }

    public function setTerm(?FHIRCodeableConcept $value): self
    {
        $this->term = $value;

        return $this;
    }

    public function getEstimate(): ?FHIRBoolean
    {
        return $this->estimate;
    }

    public function setEstimate(bool|FHIRBoolean|null $value): self
    {
        $this->estimate = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getAmount(): ?FHIRMoney
    {
        return $this->amount;
    }

    public function setAmount(?FHIRMoney $value): self
    {
        $this->amount = $value;

        return $this;
    }
}
