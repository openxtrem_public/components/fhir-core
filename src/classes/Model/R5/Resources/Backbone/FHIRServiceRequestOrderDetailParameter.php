<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ServiceRequestOrderDetailParameter Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRServiceRequestOrderDetailParameterInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRatio;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRServiceRequestOrderDetailParameter extends FHIRBackboneElement implements FHIRServiceRequestOrderDetailParameterInterface
{
    public const RESOURCE_NAME = 'ServiceRequest.orderDetail.parameter';

    protected ?FHIRCodeableConcept $code = null;
    protected FHIRQuantity|FHIRRatio|FHIRRange|FHIRBoolean|FHIRCodeableConcept|FHIRString|FHIRPeriod|null $value = null;

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getValue(
    ): FHIRQuantity|FHIRRatio|FHIRRange|FHIRBoolean|FHIRCodeableConcept|FHIRString|FHIRPeriod|null
    {
        return $this->value;
    }

    public function setValue(
        FHIRQuantity|FHIRRatio|FHIRRange|FHIRBoolean|FHIRCodeableConcept|FHIRString|FHIRPeriod|null $value,
    ): self
    {
        $this->value = $value;

        return $this;
    }
}
