<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExampleScenarioProcessStepAlternative Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExampleScenarioProcessStepAlternativeInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRExampleScenarioProcessStepAlternative extends FHIRBackboneElement implements FHIRExampleScenarioProcessStepAlternativeInterface
{
    public const RESOURCE_NAME = 'ExampleScenario.process.step.alternative';

    protected ?FHIRString $title = null;
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRExampleScenarioProcessStep[] */
    protected array $step = [];

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRExampleScenarioProcessStep[]
     */
    public function getStep(): array
    {
        return $this->step;
    }

    public function setStep(?FHIRExampleScenarioProcessStep ...$value): self
    {
        $this->step = array_filter($value);

        return $this;
    }

    public function addStep(?FHIRExampleScenarioProcessStep ...$value): self
    {
        $this->step = array_filter(array_merge($this->step, $value));

        return $this;
    }
}
