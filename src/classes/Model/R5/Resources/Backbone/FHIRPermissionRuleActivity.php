<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PermissionRuleActivity Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRPermissionRuleActivityInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRPermissionRuleActivity extends FHIRBackboneElement implements FHIRPermissionRuleActivityInterface
{
    public const RESOURCE_NAME = 'Permission.rule.activity';

    /** @var FHIRReference[] */
    protected array $actor = [];

    /** @var FHIRCodeableConcept[] */
    protected array $action = [];

    /** @var FHIRCodeableConcept[] */
    protected array $purpose = [];

    /**
     * @return FHIRReference[]
     */
    public function getActor(): array
    {
        return $this->actor;
    }

    public function setActor(?FHIRReference ...$value): self
    {
        $this->actor = array_filter($value);

        return $this;
    }

    public function addActor(?FHIRReference ...$value): self
    {
        $this->actor = array_filter(array_merge($this->actor, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getAction(): array
    {
        return $this->action;
    }

    public function setAction(?FHIRCodeableConcept ...$value): self
    {
        $this->action = array_filter($value);

        return $this;
    }

    public function addAction(?FHIRCodeableConcept ...$value): self
    {
        $this->action = array_filter(array_merge($this->action, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getPurpose(): array
    {
        return $this->purpose;
    }

    public function setPurpose(?FHIRCodeableConcept ...$value): self
    {
        $this->purpose = array_filter($value);

        return $this;
    }

    public function addPurpose(?FHIRCodeableConcept ...$value): self
    {
        $this->purpose = array_filter(array_merge($this->purpose, $value));

        return $this;
    }
}
