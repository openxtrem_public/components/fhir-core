<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExampleScenarioProcessStep Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExampleScenarioProcessStepInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRExampleScenarioProcessStep extends FHIRBackboneElement implements FHIRExampleScenarioProcessStepInterface
{
    public const RESOURCE_NAME = 'ExampleScenario.process.step';

    protected ?FHIRString $number = null;
    protected ?FHIRExampleScenarioProcess $process = null;
    protected ?FHIRCanonical $workflow = null;
    protected ?FHIRExampleScenarioProcessStepOperation $operation = null;

    /** @var FHIRExampleScenarioProcessStepAlternative[] */
    protected array $alternative = [];
    protected ?FHIRBoolean $pause = null;

    public function getNumber(): ?FHIRString
    {
        return $this->number;
    }

    public function setNumber(string|FHIRString|null $value): self
    {
        $this->number = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getProcess(): ?FHIRExampleScenarioProcess
    {
        return $this->process;
    }

    public function setProcess(?FHIRExampleScenarioProcess $value): self
    {
        $this->process = $value;

        return $this;
    }

    public function getWorkflow(): ?FHIRCanonical
    {
        return $this->workflow;
    }

    public function setWorkflow(string|FHIRCanonical|null $value): self
    {
        $this->workflow = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    public function getOperation(): ?FHIRExampleScenarioProcessStepOperation
    {
        return $this->operation;
    }

    public function setOperation(?FHIRExampleScenarioProcessStepOperation $value): self
    {
        $this->operation = $value;

        return $this;
    }

    /**
     * @return FHIRExampleScenarioProcessStepAlternative[]
     */
    public function getAlternative(): array
    {
        return $this->alternative;
    }

    public function setAlternative(?FHIRExampleScenarioProcessStepAlternative ...$value): self
    {
        $this->alternative = array_filter($value);

        return $this;
    }

    public function addAlternative(?FHIRExampleScenarioProcessStepAlternative ...$value): self
    {
        $this->alternative = array_filter(array_merge($this->alternative, $value));

        return $this;
    }

    public function getPause(): ?FHIRBoolean
    {
        return $this->pause;
    }

    public function setPause(bool|FHIRBoolean|null $value): self
    {
        $this->pause = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }
}
