<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR FamilyMemberHistoryProcedure Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRFamilyMemberHistoryProcedureInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAge;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRFamilyMemberHistoryProcedure extends FHIRBackboneElement implements FHIRFamilyMemberHistoryProcedureInterface
{
    public const RESOURCE_NAME = 'FamilyMemberHistory.procedure';

    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRCodeableConcept $outcome = null;
    protected ?FHIRBoolean $contributedToDeath = null;
    protected FHIRAge|FHIRRange|FHIRPeriod|FHIRString|FHIRDateTime|null $performed = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getOutcome(): ?FHIRCodeableConcept
    {
        return $this->outcome;
    }

    public function setOutcome(?FHIRCodeableConcept $value): self
    {
        $this->outcome = $value;

        return $this;
    }

    public function getContributedToDeath(): ?FHIRBoolean
    {
        return $this->contributedToDeath;
    }

    public function setContributedToDeath(bool|FHIRBoolean|null $value): self
    {
        $this->contributedToDeath = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getPerformed(): FHIRAge|FHIRRange|FHIRPeriod|FHIRString|FHIRDateTime|null
    {
        return $this->performed;
    }

    public function setPerformed(FHIRAge|FHIRRange|FHIRPeriod|FHIRString|FHIRDateTime|null $value): self
    {
        $this->performed = $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }
}
