<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExampleScenarioInstanceContainedInstance Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExampleScenarioInstanceContainedInstanceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRExampleScenarioInstanceContainedInstance extends FHIRBackboneElement implements FHIRExampleScenarioInstanceContainedInstanceInterface
{
    public const RESOURCE_NAME = 'ExampleScenario.instance.containedInstance';

    protected ?FHIRString $instanceReference = null;
    protected ?FHIRString $versionReference = null;

    public function getInstanceReference(): ?FHIRString
    {
        return $this->instanceReference;
    }

    public function setInstanceReference(string|FHIRString|null $value): self
    {
        $this->instanceReference = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getVersionReference(): ?FHIRString
    {
        return $this->versionReference;
    }

    public function setVersionReference(string|FHIRString|null $value): self
    {
        $this->versionReference = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
