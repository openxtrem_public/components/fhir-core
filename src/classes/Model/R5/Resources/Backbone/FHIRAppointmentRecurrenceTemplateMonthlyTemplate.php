<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AppointmentRecurrenceTemplateMonthlyTemplate Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAppointmentRecurrenceTemplateMonthlyTemplateInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;

class FHIRAppointmentRecurrenceTemplateMonthlyTemplate extends FHIRBackboneElement implements FHIRAppointmentRecurrenceTemplateMonthlyTemplateInterface
{
    public const RESOURCE_NAME = 'Appointment.recurrenceTemplate.monthlyTemplate';

    protected ?FHIRPositiveInt $dayOfMonth = null;
    protected ?FHIRCoding $nthWeekOfMonth = null;
    protected ?FHIRCoding $dayOfWeek = null;
    protected ?FHIRPositiveInt $monthInterval = null;

    public function getDayOfMonth(): ?FHIRPositiveInt
    {
        return $this->dayOfMonth;
    }

    public function setDayOfMonth(int|FHIRPositiveInt|null $value): self
    {
        $this->dayOfMonth = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getNthWeekOfMonth(): ?FHIRCoding
    {
        return $this->nthWeekOfMonth;
    }

    public function setNthWeekOfMonth(?FHIRCoding $value): self
    {
        $this->nthWeekOfMonth = $value;

        return $this;
    }

    public function getDayOfWeek(): ?FHIRCoding
    {
        return $this->dayOfWeek;
    }

    public function setDayOfWeek(?FHIRCoding $value): self
    {
        $this->dayOfWeek = $value;

        return $this;
    }

    public function getMonthInterval(): ?FHIRPositiveInt
    {
        return $this->monthInterval;
    }

    public function setMonthInterval(int|FHIRPositiveInt|null $value): self
    {
        $this->monthInterval = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }
}
