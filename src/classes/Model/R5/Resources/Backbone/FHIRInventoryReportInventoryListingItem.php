<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR InventoryReportInventoryListingItem Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRInventoryReportInventoryListingItemInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;

class FHIRInventoryReportInventoryListingItem extends FHIRBackboneElement implements FHIRInventoryReportInventoryListingItemInterface
{
    public const RESOURCE_NAME = 'InventoryReport.inventoryListing.item';

    protected ?FHIRCodeableConcept $category = null;
    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRCodeableReference $item = null;

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getItem(): ?FHIRCodeableReference
    {
        return $this->item;
    }

    public function setItem(?FHIRCodeableReference $value): self
    {
        $this->item = $value;

        return $this;
    }
}
