<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ProcedureFocalDevice Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRProcedureFocalDeviceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRProcedureFocalDevice extends FHIRBackboneElement implements FHIRProcedureFocalDeviceInterface
{
    public const RESOURCE_NAME = 'Procedure.focalDevice';

    protected ?FHIRCodeableConcept $action = null;
    protected ?FHIRReference $manipulated = null;

    public function getAction(): ?FHIRCodeableConcept
    {
        return $this->action;
    }

    public function setAction(?FHIRCodeableConcept $value): self
    {
        $this->action = $value;

        return $this;
    }

    public function getManipulated(): ?FHIRReference
    {
        return $this->manipulated;
    }

    public function setManipulated(?FHIRReference $value): self
    {
        $this->manipulated = $value;

        return $this;
    }
}
