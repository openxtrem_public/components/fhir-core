<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR RequirementsStatement Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRRequirementsStatementInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUrl;

class FHIRRequirementsStatement extends FHIRBackboneElement implements FHIRRequirementsStatementInterface
{
    public const RESOURCE_NAME = 'Requirements.statement';

    protected ?FHIRId $key = null;
    protected ?FHIRString $label = null;

    /** @var FHIRCode[] */
    protected array $conformance = [];
    protected ?FHIRBoolean $conditionality = null;
    protected ?FHIRMarkdown $requirement = null;
    protected ?FHIRString $derivedFrom = null;
    protected ?FHIRString $parent = null;

    /** @var FHIRUrl[] */
    protected array $satisfiedBy = [];

    /** @var FHIRUrl[] */
    protected array $reference = [];

    /** @var FHIRReference[] */
    protected array $source = [];

    public function getKey(): ?FHIRId
    {
        return $this->key;
    }

    public function setKey(string|FHIRId|null $value): self
    {
        $this->key = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getLabel(): ?FHIRString
    {
        return $this->label;
    }

    public function setLabel(string|FHIRString|null $value): self
    {
        $this->label = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getConformance(): array
    {
        return $this->conformance;
    }

    public function setConformance(string|FHIRCode|null ...$value): self
    {
        $this->conformance = [];
        $this->addConformance(...$value);

        return $this;
    }

    public function addConformance(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->conformance = array_filter(array_merge($this->conformance, $values));

        return $this;
    }

    public function getConditionality(): ?FHIRBoolean
    {
        return $this->conditionality;
    }

    public function setConditionality(bool|FHIRBoolean|null $value): self
    {
        $this->conditionality = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getRequirement(): ?FHIRMarkdown
    {
        return $this->requirement;
    }

    public function setRequirement(string|FHIRMarkdown|null $value): self
    {
        $this->requirement = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getDerivedFrom(): ?FHIRString
    {
        return $this->derivedFrom;
    }

    public function setDerivedFrom(string|FHIRString|null $value): self
    {
        $this->derivedFrom = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getParent(): ?FHIRString
    {
        return $this->parent;
    }

    public function setParent(string|FHIRString|null $value): self
    {
        $this->parent = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRUrl[]
     */
    public function getSatisfiedBy(): array
    {
        return $this->satisfiedBy;
    }

    public function setSatisfiedBy(string|FHIRUrl|null ...$value): self
    {
        $this->satisfiedBy = [];
        $this->addSatisfiedBy(...$value);

        return $this;
    }

    public function addSatisfiedBy(string|FHIRUrl|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUrl())->setValue($v) : $v, $value);

        $this->satisfiedBy = array_filter(array_merge($this->satisfiedBy, $values));

        return $this;
    }

    /**
     * @return FHIRUrl[]
     */
    public function getReference(): array
    {
        return $this->reference;
    }

    public function setReference(string|FHIRUrl|null ...$value): self
    {
        $this->reference = [];
        $this->addReference(...$value);

        return $this;
    }

    public function addReference(string|FHIRUrl|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUrl())->setValue($v) : $v, $value);

        $this->reference = array_filter(array_merge($this->reference, $values));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSource(): array
    {
        return $this->source;
    }

    public function setSource(?FHIRReference ...$value): self
    {
        $this->source = array_filter($value);

        return $this;
    }

    public function addSource(?FHIRReference ...$value): self
    {
        $this->source = array_filter(array_merge($this->source, $value));

        return $this;
    }
}
