<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ContractLegal Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRContractLegalInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRContractLegal extends FHIRBackboneElement implements FHIRContractLegalInterface
{
    public const RESOURCE_NAME = 'Contract.legal';

    protected FHIRAttachment|FHIRReference|null $content = null;

    public function getContent(): FHIRAttachment|FHIRReference|null
    {
        return $this->content;
    }

    public function setContent(FHIRAttachment|FHIRReference|null $value): self
    {
        $this->content = $value;

        return $this;
    }
}
