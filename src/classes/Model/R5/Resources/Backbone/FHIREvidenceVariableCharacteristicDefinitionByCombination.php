<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EvidenceVariableCharacteristicDefinitionByCombination Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREvidenceVariableCharacteristicDefinitionByCombinationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;

class FHIREvidenceVariableCharacteristicDefinitionByCombination extends FHIRBackboneElement implements FHIREvidenceVariableCharacteristicDefinitionByCombinationInterface
{
    public const RESOURCE_NAME = 'EvidenceVariable.characteristic.definitionByCombination';

    protected ?FHIRCode $code = null;
    protected ?FHIRPositiveInt $threshold = null;

    /** @var FHIREvidenceVariableCharacteristic[] */
    protected array $characteristic = [];

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getThreshold(): ?FHIRPositiveInt
    {
        return $this->threshold;
    }

    public function setThreshold(int|FHIRPositiveInt|null $value): self
    {
        $this->threshold = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIREvidenceVariableCharacteristic[]
     */
    public function getCharacteristic(): array
    {
        return $this->characteristic;
    }

    public function setCharacteristic(?FHIREvidenceVariableCharacteristic ...$value): self
    {
        $this->characteristic = array_filter($value);

        return $this;
    }

    public function addCharacteristic(?FHIREvidenceVariableCharacteristic ...$value): self
    {
        $this->characteristic = array_filter(array_merge($this->characteristic, $value));

        return $this;
    }
}
