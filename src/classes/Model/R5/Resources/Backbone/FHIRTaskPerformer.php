<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TaskPerformer Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTaskPerformerInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRTaskPerformer extends FHIRBackboneElement implements FHIRTaskPerformerInterface
{
    public const RESOURCE_NAME = 'Task.performer';

    protected ?FHIRCodeableConcept $function = null;
    protected ?FHIRReference $actor = null;

    public function getFunction(): ?FHIRCodeableConcept
    {
        return $this->function;
    }

    public function setFunction(?FHIRCodeableConcept $value): self
    {
        $this->function = $value;

        return $this;
    }

    public function getActor(): ?FHIRReference
    {
        return $this->actor;
    }

    public function setActor(?FHIRReference $value): self
    {
        $this->actor = $value;

        return $this;
    }
}
