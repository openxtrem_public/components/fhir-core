<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DiagnosticReportMedia Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDiagnosticReportMediaInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRDiagnosticReportMedia extends FHIRBackboneElement implements FHIRDiagnosticReportMediaInterface
{
    public const RESOURCE_NAME = 'DiagnosticReport.media';

    protected ?FHIRString $comment = null;
    protected ?FHIRReference $link = null;

    public function getComment(): ?FHIRString
    {
        return $this->comment;
    }

    public function setComment(string|FHIRString|null $value): self
    {
        $this->comment = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getLink(): ?FHIRReference
    {
        return $this->link;
    }

    public function setLink(?FHIRReference $value): self
    {
        $this->link = $value;

        return $this;
    }
}
