<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationAdministrationPerformer Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationAdministrationPerformerInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;

class FHIRMedicationAdministrationPerformer extends FHIRBackboneElement implements FHIRMedicationAdministrationPerformerInterface
{
    public const RESOURCE_NAME = 'MedicationAdministration.performer';

    protected ?FHIRCodeableConcept $function = null;
    protected ?FHIRCodeableReference $actor = null;

    public function getFunction(): ?FHIRCodeableConcept
    {
        return $this->function;
    }

    public function setFunction(?FHIRCodeableConcept $value): self
    {
        $this->function = $value;

        return $this;
    }

    public function getActor(): ?FHIRCodeableReference
    {
        return $this->actor;
    }

    public function setActor(?FHIRCodeableReference $value): self
    {
        $this->actor = $value;

        return $this;
    }
}
