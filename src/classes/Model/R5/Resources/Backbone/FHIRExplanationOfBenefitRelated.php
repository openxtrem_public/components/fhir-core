<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExplanationOfBenefitRelated Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExplanationOfBenefitRelatedInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRExplanationOfBenefitRelated extends FHIRBackboneElement implements FHIRExplanationOfBenefitRelatedInterface
{
    public const RESOURCE_NAME = 'ExplanationOfBenefit.related';

    protected ?FHIRReference $claim = null;
    protected ?FHIRCodeableConcept $relationship = null;
    protected ?FHIRIdentifier $reference = null;

    public function getClaim(): ?FHIRReference
    {
        return $this->claim;
    }

    public function setClaim(?FHIRReference $value): self
    {
        $this->claim = $value;

        return $this;
    }

    public function getRelationship(): ?FHIRCodeableConcept
    {
        return $this->relationship;
    }

    public function setRelationship(?FHIRCodeableConcept $value): self
    {
        $this->relationship = $value;

        return $this;
    }

    public function getReference(): ?FHIRIdentifier
    {
        return $this->reference;
    }

    public function setReference(?FHIRIdentifier $value): self
    {
        $this->reference = $value;

        return $this;
    }
}
