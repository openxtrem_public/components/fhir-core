<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClinicalUseDefinitionContraindicationOtherTherapy Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRClinicalUseDefinitionContraindicationOtherTherapyInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;

class FHIRClinicalUseDefinitionContraindicationOtherTherapy extends FHIRBackboneElement implements FHIRClinicalUseDefinitionContraindicationOtherTherapyInterface
{
    public const RESOURCE_NAME = 'ClinicalUseDefinition.contraindication.otherTherapy';

    protected ?FHIRCodeableConcept $relationshipType = null;
    protected ?FHIRCodeableReference $treatment = null;

    public function getRelationshipType(): ?FHIRCodeableConcept
    {
        return $this->relationshipType;
    }

    public function setRelationshipType(?FHIRCodeableConcept $value): self
    {
        $this->relationshipType = $value;

        return $this;
    }

    public function getTreatment(): ?FHIRCodeableReference
    {
        return $this->treatment;
    }

    public function setTreatment(?FHIRCodeableReference $value): self
    {
        $this->treatment = $value;

        return $this;
    }
}
