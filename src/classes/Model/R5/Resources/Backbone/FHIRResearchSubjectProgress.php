<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ResearchSubjectProgress Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRResearchSubjectProgressInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;

class FHIRResearchSubjectProgress extends FHIRBackboneElement implements FHIRResearchSubjectProgressInterface
{
    public const RESOURCE_NAME = 'ResearchSubject.progress';

    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableConcept $subjectState = null;
    protected ?FHIRCodeableConcept $milestone = null;
    protected ?FHIRCodeableConcept $reason = null;
    protected ?FHIRDateTime $startDate = null;
    protected ?FHIRDateTime $endDate = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getSubjectState(): ?FHIRCodeableConcept
    {
        return $this->subjectState;
    }

    public function setSubjectState(?FHIRCodeableConcept $value): self
    {
        $this->subjectState = $value;

        return $this;
    }

    public function getMilestone(): ?FHIRCodeableConcept
    {
        return $this->milestone;
    }

    public function setMilestone(?FHIRCodeableConcept $value): self
    {
        $this->milestone = $value;

        return $this;
    }

    public function getReason(): ?FHIRCodeableConcept
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableConcept $value): self
    {
        $this->reason = $value;

        return $this;
    }

    public function getStartDate(): ?FHIRDateTime
    {
        return $this->startDate;
    }

    public function setStartDate(string|FHIRDateTime|null $value): self
    {
        $this->startDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getEndDate(): ?FHIRDateTime
    {
        return $this->endDate;
    }

    public function setEndDate(string|FHIRDateTime|null $value): self
    {
        $this->endDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }
}
