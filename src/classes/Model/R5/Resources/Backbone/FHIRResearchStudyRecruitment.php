<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ResearchStudyRecruitment Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRResearchStudyRecruitmentInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUnsignedInt;

class FHIRResearchStudyRecruitment extends FHIRBackboneElement implements FHIRResearchStudyRecruitmentInterface
{
    public const RESOURCE_NAME = 'ResearchStudy.recruitment';

    protected ?FHIRUnsignedInt $targetNumber = null;
    protected ?FHIRUnsignedInt $actualNumber = null;
    protected ?FHIRReference $eligibility = null;
    protected ?FHIRReference $actualGroup = null;

    public function getTargetNumber(): ?FHIRUnsignedInt
    {
        return $this->targetNumber;
    }

    public function setTargetNumber(int|FHIRUnsignedInt|null $value): self
    {
        $this->targetNumber = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getActualNumber(): ?FHIRUnsignedInt
    {
        return $this->actualNumber;
    }

    public function setActualNumber(int|FHIRUnsignedInt|null $value): self
    {
        $this->actualNumber = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getEligibility(): ?FHIRReference
    {
        return $this->eligibility;
    }

    public function setEligibility(?FHIRReference $value): self
    {
        $this->eligibility = $value;

        return $this;
    }

    public function getActualGroup(): ?FHIRReference
    {
        return $this->actualGroup;
    }

    public function setActualGroup(?FHIRReference $value): self
    {
        $this->actualGroup = $value;

        return $this;
    }
}
