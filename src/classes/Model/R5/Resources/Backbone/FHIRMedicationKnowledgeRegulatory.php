<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledgeRegulatory Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationKnowledgeRegulatoryInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRMedicationKnowledgeRegulatory extends FHIRBackboneElement implements FHIRMedicationKnowledgeRegulatoryInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge.regulatory';

    protected ?FHIRReference $regulatoryAuthority = null;

    /** @var FHIRMedicationKnowledgeRegulatorySubstitution[] */
    protected array $substitution = [];

    /** @var FHIRCodeableConcept[] */
    protected array $schedule = [];
    protected ?FHIRMedicationKnowledgeRegulatoryMaxDispense $maxDispense = null;

    public function getRegulatoryAuthority(): ?FHIRReference
    {
        return $this->regulatoryAuthority;
    }

    public function setRegulatoryAuthority(?FHIRReference $value): self
    {
        $this->regulatoryAuthority = $value;

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeRegulatorySubstitution[]
     */
    public function getSubstitution(): array
    {
        return $this->substitution;
    }

    public function setSubstitution(?FHIRMedicationKnowledgeRegulatorySubstitution ...$value): self
    {
        $this->substitution = array_filter($value);

        return $this;
    }

    public function addSubstitution(?FHIRMedicationKnowledgeRegulatorySubstitution ...$value): self
    {
        $this->substitution = array_filter(array_merge($this->substitution, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSchedule(): array
    {
        return $this->schedule;
    }

    public function setSchedule(?FHIRCodeableConcept ...$value): self
    {
        $this->schedule = array_filter($value);

        return $this;
    }

    public function addSchedule(?FHIRCodeableConcept ...$value): self
    {
        $this->schedule = array_filter(array_merge($this->schedule, $value));

        return $this;
    }

    public function getMaxDispense(): ?FHIRMedicationKnowledgeRegulatoryMaxDispense
    {
        return $this->maxDispense;
    }

    public function setMaxDispense(?FHIRMedicationKnowledgeRegulatoryMaxDispense $value): self
    {
        $this->maxDispense = $value;

        return $this;
    }
}
