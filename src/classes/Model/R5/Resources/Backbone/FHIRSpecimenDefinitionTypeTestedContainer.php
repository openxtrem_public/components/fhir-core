<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SpecimenDefinitionTypeTestedContainer Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSpecimenDefinitionTypeTestedContainerInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRSpecimenDefinitionTypeTestedContainer extends FHIRBackboneElement implements FHIRSpecimenDefinitionTypeTestedContainerInterface
{
    public const RESOURCE_NAME = 'SpecimenDefinition.typeTested.container';

    protected ?FHIRCodeableConcept $material = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableConcept $cap = null;
    protected ?FHIRMarkdown $description = null;
    protected ?FHIRQuantity $capacity = null;
    protected FHIRQuantity|FHIRString|null $minimumVolume = null;

    /** @var FHIRSpecimenDefinitionTypeTestedContainerAdditive[] */
    protected array $additive = [];
    protected ?FHIRMarkdown $preparation = null;

    public function getMaterial(): ?FHIRCodeableConcept
    {
        return $this->material;
    }

    public function setMaterial(?FHIRCodeableConcept $value): self
    {
        $this->material = $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getCap(): ?FHIRCodeableConcept
    {
        return $this->cap;
    }

    public function setCap(?FHIRCodeableConcept $value): self
    {
        $this->cap = $value;

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getCapacity(): ?FHIRQuantity
    {
        return $this->capacity;
    }

    public function setCapacity(?FHIRQuantity $value): self
    {
        $this->capacity = $value;

        return $this;
    }

    public function getMinimumVolume(): FHIRQuantity|FHIRString|null
    {
        return $this->minimumVolume;
    }

    public function setMinimumVolume(FHIRQuantity|FHIRString|null $value): self
    {
        $this->minimumVolume = $value;

        return $this;
    }

    /**
     * @return FHIRSpecimenDefinitionTypeTestedContainerAdditive[]
     */
    public function getAdditive(): array
    {
        return $this->additive;
    }

    public function setAdditive(?FHIRSpecimenDefinitionTypeTestedContainerAdditive ...$value): self
    {
        $this->additive = array_filter($value);

        return $this;
    }

    public function addAdditive(?FHIRSpecimenDefinitionTypeTestedContainerAdditive ...$value): self
    {
        $this->additive = array_filter(array_merge($this->additive, $value));

        return $this;
    }

    public function getPreparation(): ?FHIRMarkdown
    {
        return $this->preparation;
    }

    public function setPreparation(string|FHIRMarkdown|null $value): self
    {
        $this->preparation = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }
}
