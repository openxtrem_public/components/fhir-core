<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceDefinitionRegulatoryIdentifier Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceDefinitionRegulatoryIdentifierInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;

class FHIRDeviceDefinitionRegulatoryIdentifier extends FHIRBackboneElement implements FHIRDeviceDefinitionRegulatoryIdentifierInterface
{
    public const RESOURCE_NAME = 'DeviceDefinition.regulatoryIdentifier';

    protected ?FHIRCode $type = null;
    protected ?FHIRString $deviceIdentifier = null;
    protected ?FHIRUri $issuer = null;
    protected ?FHIRUri $jurisdiction = null;

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDeviceIdentifier(): ?FHIRString
    {
        return $this->deviceIdentifier;
    }

    public function setDeviceIdentifier(string|FHIRString|null $value): self
    {
        $this->deviceIdentifier = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getIssuer(): ?FHIRUri
    {
        return $this->issuer;
    }

    public function setIssuer(string|FHIRUri|null $value): self
    {
        $this->issuer = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getJurisdiction(): ?FHIRUri
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(string|FHIRUri|null $value): self
    {
        $this->jurisdiction = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }
}
