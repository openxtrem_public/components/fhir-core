<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExplanationOfBenefitPayee Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExplanationOfBenefitPayeeInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRExplanationOfBenefitPayee extends FHIRBackboneElement implements FHIRExplanationOfBenefitPayeeInterface
{
    public const RESOURCE_NAME = 'ExplanationOfBenefit.payee';

    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRReference $party = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getParty(): ?FHIRReference
    {
        return $this->party;
    }

    public function setParty(?FHIRReference $value): self
    {
        $this->party = $value;

        return $this;
    }
}
