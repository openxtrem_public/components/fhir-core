<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceDefinitionPackagingDistributor Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceDefinitionPackagingDistributorInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRDeviceDefinitionPackagingDistributor extends FHIRBackboneElement implements FHIRDeviceDefinitionPackagingDistributorInterface
{
    public const RESOURCE_NAME = 'DeviceDefinition.packaging.distributor';

    protected ?FHIRString $name = null;

    /** @var FHIRReference[] */
    protected array $organizationReference = [];

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getOrganizationReference(): array
    {
        return $this->organizationReference;
    }

    public function setOrganizationReference(?FHIRReference ...$value): self
    {
        $this->organizationReference = array_filter($value);

        return $this;
    }

    public function addOrganizationReference(?FHIRReference ...$value): self
    {
        $this->organizationReference = array_filter(array_merge($this->organizationReference, $value));

        return $this;
    }
}
