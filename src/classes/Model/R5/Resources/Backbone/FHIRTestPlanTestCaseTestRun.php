<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestPlanTestCaseTestRun Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestPlanTestCaseTestRunInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;

class FHIRTestPlanTestCaseTestRun extends FHIRBackboneElement implements FHIRTestPlanTestCaseTestRunInterface
{
    public const RESOURCE_NAME = 'TestPlan.testCase.testRun';

    protected ?FHIRMarkdown $narrative = null;
    protected ?FHIRTestPlanTestCaseTestRunScript $script = null;

    public function getNarrative(): ?FHIRMarkdown
    {
        return $this->narrative;
    }

    public function setNarrative(string|FHIRMarkdown|null $value): self
    {
        $this->narrative = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getScript(): ?FHIRTestPlanTestCaseTestRunScript
    {
        return $this->script;
    }

    public function setScript(?FHIRTestPlanTestCaseTestRunScript $value): self
    {
        $this->script = $value;

        return $this;
    }
}
