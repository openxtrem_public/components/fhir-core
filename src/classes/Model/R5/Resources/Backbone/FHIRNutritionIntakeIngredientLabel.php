<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR NutritionIntakeIngredientLabel Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRNutritionIntakeIngredientLabelInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;

class FHIRNutritionIntakeIngredientLabel extends FHIRBackboneElement implements FHIRNutritionIntakeIngredientLabelInterface
{
    public const RESOURCE_NAME = 'NutritionIntake.ingredientLabel';

    protected ?FHIRCodeableReference $nutrient = null;
    protected ?FHIRQuantity $amount = null;

    public function getNutrient(): ?FHIRCodeableReference
    {
        return $this->nutrient;
    }

    public function setNutrient(?FHIRCodeableReference $value): self
    {
        $this->nutrient = $value;

        return $this;
    }

    public function getAmount(): ?FHIRQuantity
    {
        return $this->amount;
    }

    public function setAmount(?FHIRQuantity $value): self
    {
        $this->amount = $value;

        return $this;
    }
}
