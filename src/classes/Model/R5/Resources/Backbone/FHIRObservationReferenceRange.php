<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ObservationReferenceRange Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRObservationReferenceRangeInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;

class FHIRObservationReferenceRange extends FHIRBackboneElement implements FHIRObservationReferenceRangeInterface
{
    public const RESOURCE_NAME = 'Observation.referenceRange';

    protected ?FHIRQuantity $low = null;
    protected ?FHIRQuantity $high = null;
    protected ?FHIRCodeableConcept $normalValue = null;
    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRCodeableConcept[] */
    protected array $appliesTo = [];
    protected ?FHIRRange $age = null;
    protected ?FHIRMarkdown $text = null;

    public function getLow(): ?FHIRQuantity
    {
        return $this->low;
    }

    public function setLow(?FHIRQuantity $value): self
    {
        $this->low = $value;

        return $this;
    }

    public function getHigh(): ?FHIRQuantity
    {
        return $this->high;
    }

    public function setHigh(?FHIRQuantity $value): self
    {
        $this->high = $value;

        return $this;
    }

    public function getNormalValue(): ?FHIRCodeableConcept
    {
        return $this->normalValue;
    }

    public function setNormalValue(?FHIRCodeableConcept $value): self
    {
        $this->normalValue = $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getAppliesTo(): array
    {
        return $this->appliesTo;
    }

    public function setAppliesTo(?FHIRCodeableConcept ...$value): self
    {
        $this->appliesTo = array_filter($value);

        return $this;
    }

    public function addAppliesTo(?FHIRCodeableConcept ...$value): self
    {
        $this->appliesTo = array_filter(array_merge($this->appliesTo, $value));

        return $this;
    }

    public function getAge(): ?FHIRRange
    {
        return $this->age;
    }

    public function setAge(?FHIRRange $value): self
    {
        $this->age = $value;

        return $this;
    }

    public function getText(): ?FHIRMarkdown
    {
        return $this->text;
    }

    public function setText(string|FHIRMarkdown|null $value): self
    {
        $this->text = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }
}
