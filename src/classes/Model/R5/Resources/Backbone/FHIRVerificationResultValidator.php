<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR VerificationResultValidator Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRVerificationResultValidatorInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRSignature;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRVerificationResultValidator extends FHIRBackboneElement implements FHIRVerificationResultValidatorInterface
{
    public const RESOURCE_NAME = 'VerificationResult.validator';

    protected ?FHIRReference $organization = null;
    protected ?FHIRString $identityCertificate = null;
    protected ?FHIRSignature $attestationSignature = null;

    public function getOrganization(): ?FHIRReference
    {
        return $this->organization;
    }

    public function setOrganization(?FHIRReference $value): self
    {
        $this->organization = $value;

        return $this;
    }

    public function getIdentityCertificate(): ?FHIRString
    {
        return $this->identityCertificate;
    }

    public function setIdentityCertificate(string|FHIRString|null $value): self
    {
        $this->identityCertificate = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getAttestationSignature(): ?FHIRSignature
    {
        return $this->attestationSignature;
    }

    public function setAttestationSignature(?FHIRSignature $value): self
    {
        $this->attestationSignature = $value;

        return $this;
    }
}
