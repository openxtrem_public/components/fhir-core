<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AuditEventEntity Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAuditEventEntityInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBase64Binary;

class FHIRAuditEventEntity extends FHIRBackboneElement implements FHIRAuditEventEntityInterface
{
    public const RESOURCE_NAME = 'AuditEvent.entity';

    protected ?FHIRReference $what = null;
    protected ?FHIRCodeableConcept $role = null;

    /** @var FHIRCodeableConcept[] */
    protected array $securityLabel = [];
    protected ?FHIRBase64Binary $query = null;

    /** @var FHIRAuditEventEntityDetail[] */
    protected array $detail = [];

    /** @var FHIRAuditEventAgent[] */
    protected array $agent = [];

    public function getWhat(): ?FHIRReference
    {
        return $this->what;
    }

    public function setWhat(?FHIRReference $value): self
    {
        $this->what = $value;

        return $this;
    }

    public function getRole(): ?FHIRCodeableConcept
    {
        return $this->role;
    }

    public function setRole(?FHIRCodeableConcept $value): self
    {
        $this->role = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSecurityLabel(): array
    {
        return $this->securityLabel;
    }

    public function setSecurityLabel(?FHIRCodeableConcept ...$value): self
    {
        $this->securityLabel = array_filter($value);

        return $this;
    }

    public function addSecurityLabel(?FHIRCodeableConcept ...$value): self
    {
        $this->securityLabel = array_filter(array_merge($this->securityLabel, $value));

        return $this;
    }

    public function getQuery(): ?FHIRBase64Binary
    {
        return $this->query;
    }

    public function setQuery(string|FHIRBase64Binary|null $value): self
    {
        $this->query = is_string($value) ? (new FHIRBase64Binary())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAuditEventEntityDetail[]
     */
    public function getDetail(): array
    {
        return $this->detail;
    }

    public function setDetail(?FHIRAuditEventEntityDetail ...$value): self
    {
        $this->detail = array_filter($value);

        return $this;
    }

    public function addDetail(?FHIRAuditEventEntityDetail ...$value): self
    {
        $this->detail = array_filter(array_merge($this->detail, $value));

        return $this;
    }

    /**
     * @return FHIRAuditEventAgent[]
     */
    public function getAgent(): array
    {
        return $this->agent;
    }

    public function setAgent(?FHIRAuditEventAgent ...$value): self
    {
        $this->agent = array_filter($value);

        return $this;
    }

    public function addAgent(?FHIRAuditEventAgent ...$value): self
    {
        $this->agent = array_filter(array_merge($this->agent, $value));

        return $this;
    }
}
