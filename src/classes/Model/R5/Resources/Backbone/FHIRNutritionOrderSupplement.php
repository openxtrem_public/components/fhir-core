<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR NutritionOrderSupplement Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRNutritionOrderSupplementInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRNutritionOrderSupplement extends FHIRBackboneElement implements FHIRNutritionOrderSupplementInterface
{
    public const RESOURCE_NAME = 'NutritionOrder.supplement';

    protected ?FHIRCodeableReference $type = null;
    protected ?FHIRString $productName = null;
    protected ?FHIRNutritionOrderSupplementSchedule $schedule = null;
    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRString $instruction = null;

    public function getType(): ?FHIRCodeableReference
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableReference $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getProductName(): ?FHIRString
    {
        return $this->productName;
    }

    public function setProductName(string|FHIRString|null $value): self
    {
        $this->productName = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSchedule(): ?FHIRNutritionOrderSupplementSchedule
    {
        return $this->schedule;
    }

    public function setSchedule(?FHIRNutritionOrderSupplementSchedule $value): self
    {
        $this->schedule = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getInstruction(): ?FHIRString
    {
        return $this->instruction;
    }

    public function setInstruction(string|FHIRString|null $value): self
    {
        $this->instruction = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
