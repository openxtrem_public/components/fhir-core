<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PackagedProductDefinitionPackaging Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRPackagedProductDefinitionPackagingInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRProductShelfLife;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInteger;

class FHIRPackagedProductDefinitionPackaging extends FHIRBackboneElement implements FHIRPackagedProductDefinitionPackagingInterface
{
    public const RESOURCE_NAME = 'PackagedProductDefinition.packaging';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRBoolean $componentPart = null;
    protected ?FHIRInteger $quantity = null;

    /** @var FHIRCodeableConcept[] */
    protected array $material = [];

    /** @var FHIRCodeableConcept[] */
    protected array $alternateMaterial = [];

    /** @var FHIRProductShelfLife[] */
    protected array $shelfLifeStorage = [];

    /** @var FHIRReference[] */
    protected array $manufacturer = [];

    /** @var FHIRPackagedProductDefinitionPackagingProperty[] */
    protected array $property = [];

    /** @var FHIRPackagedProductDefinitionPackagingContainedItem[] */
    protected array $containedItem = [];

    /** @var FHIRPackagedProductDefinitionPackaging[] */
    protected array $packaging = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getComponentPart(): ?FHIRBoolean
    {
        return $this->componentPart;
    }

    public function setComponentPart(bool|FHIRBoolean|null $value): self
    {
        $this->componentPart = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getQuantity(): ?FHIRInteger
    {
        return $this->quantity;
    }

    public function setQuantity(int|FHIRInteger|null $value): self
    {
        $this->quantity = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getMaterial(): array
    {
        return $this->material;
    }

    public function setMaterial(?FHIRCodeableConcept ...$value): self
    {
        $this->material = array_filter($value);

        return $this;
    }

    public function addMaterial(?FHIRCodeableConcept ...$value): self
    {
        $this->material = array_filter(array_merge($this->material, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getAlternateMaterial(): array
    {
        return $this->alternateMaterial;
    }

    public function setAlternateMaterial(?FHIRCodeableConcept ...$value): self
    {
        $this->alternateMaterial = array_filter($value);

        return $this;
    }

    public function addAlternateMaterial(?FHIRCodeableConcept ...$value): self
    {
        $this->alternateMaterial = array_filter(array_merge($this->alternateMaterial, $value));

        return $this;
    }

    /**
     * @return FHIRProductShelfLife[]
     */
    public function getShelfLifeStorage(): array
    {
        return $this->shelfLifeStorage;
    }

    public function setShelfLifeStorage(?FHIRProductShelfLife ...$value): self
    {
        $this->shelfLifeStorage = array_filter($value);

        return $this;
    }

    public function addShelfLifeStorage(?FHIRProductShelfLife ...$value): self
    {
        $this->shelfLifeStorage = array_filter(array_merge($this->shelfLifeStorage, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getManufacturer(): array
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?FHIRReference ...$value): self
    {
        $this->manufacturer = array_filter($value);

        return $this;
    }

    public function addManufacturer(?FHIRReference ...$value): self
    {
        $this->manufacturer = array_filter(array_merge($this->manufacturer, $value));

        return $this;
    }

    /**
     * @return FHIRPackagedProductDefinitionPackagingProperty[]
     */
    public function getProperty(): array
    {
        return $this->property;
    }

    public function setProperty(?FHIRPackagedProductDefinitionPackagingProperty ...$value): self
    {
        $this->property = array_filter($value);

        return $this;
    }

    public function addProperty(?FHIRPackagedProductDefinitionPackagingProperty ...$value): self
    {
        $this->property = array_filter(array_merge($this->property, $value));

        return $this;
    }

    /**
     * @return FHIRPackagedProductDefinitionPackagingContainedItem[]
     */
    public function getContainedItem(): array
    {
        return $this->containedItem;
    }

    public function setContainedItem(?FHIRPackagedProductDefinitionPackagingContainedItem ...$value): self
    {
        $this->containedItem = array_filter($value);

        return $this;
    }

    public function addContainedItem(?FHIRPackagedProductDefinitionPackagingContainedItem ...$value): self
    {
        $this->containedItem = array_filter(array_merge($this->containedItem, $value));

        return $this;
    }

    /**
     * @return FHIRPackagedProductDefinitionPackaging[]
     */
    public function getPackaging(): array
    {
        return $this->packaging;
    }

    public function setPackaging(?FHIRPackagedProductDefinitionPackaging ...$value): self
    {
        $this->packaging = array_filter($value);

        return $this;
    }

    public function addPackaging(?FHIRPackagedProductDefinitionPackaging ...$value): self
    {
        $this->packaging = array_filter(array_merge($this->packaging, $value));

        return $this;
    }
}
