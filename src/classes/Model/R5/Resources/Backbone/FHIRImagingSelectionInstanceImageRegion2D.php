<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImagingSelectionInstanceImageRegion2D Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImagingSelectionInstanceImageRegion2DInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDecimal;

class FHIRImagingSelectionInstanceImageRegion2D extends FHIRBackboneElement implements FHIRImagingSelectionInstanceImageRegion2DInterface
{
    public const RESOURCE_NAME = 'ImagingSelection.instance.imageRegion2D';

    protected ?FHIRCode $regionType = null;

    /** @var FHIRDecimal[] */
    protected array $coordinate = [];

    public function getRegionType(): ?FHIRCode
    {
        return $this->regionType;
    }

    public function setRegionType(string|FHIRCode|null $value): self
    {
        $this->regionType = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRDecimal[]
     */
    public function getCoordinate(): array
    {
        return $this->coordinate;
    }

    public function setCoordinate(float|FHIRDecimal|null ...$value): self
    {
        $this->coordinate = [];
        $this->addCoordinate(...$value);

        return $this;
    }

    public function addCoordinate(float|FHIRDecimal|null ...$value): self
    {
        $values = array_map(fn($v) => is_float($v) ? (new FHIRDecimal())->setValue($v) : $v, $value);

        $this->coordinate = array_filter(array_merge($this->coordinate, $values));

        return $this;
    }
}
