<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CoverageEligibilityResponseError Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCoverageEligibilityResponseErrorInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRCoverageEligibilityResponseError extends FHIRBackboneElement implements FHIRCoverageEligibilityResponseErrorInterface
{
    public const RESOURCE_NAME = 'CoverageEligibilityResponse.error';

    protected ?FHIRCodeableConcept $code = null;

    /** @var FHIRString[] */
    protected array $expression = [];

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getExpression(): array
    {
        return $this->expression;
    }

    public function setExpression(string|FHIRString|null ...$value): self
    {
        $this->expression = [];
        $this->addExpression(...$value);

        return $this;
    }

    public function addExpression(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->expression = array_filter(array_merge($this->expression, $values));

        return $this;
    }
}
