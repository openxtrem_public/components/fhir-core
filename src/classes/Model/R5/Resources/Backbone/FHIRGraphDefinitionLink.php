<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR GraphDefinitionLink Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRGraphDefinitionLinkInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRGraphDefinitionLink extends FHIRBackboneElement implements FHIRGraphDefinitionLinkInterface
{
    public const RESOURCE_NAME = 'GraphDefinition.link';

    protected ?FHIRString $description = null;
    protected ?FHIRInteger $min = null;
    protected ?FHIRString $max = null;
    protected ?FHIRId $sourceId = null;
    protected ?FHIRString $path = null;
    protected ?FHIRString $sliceName = null;
    protected ?FHIRId $targetId = null;
    protected ?FHIRString $params = null;

    /** @var FHIRGraphDefinitionLinkCompartment[] */
    protected array $compartment = [];

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getMin(): ?FHIRInteger
    {
        return $this->min;
    }

    public function setMin(int|FHIRInteger|null $value): self
    {
        $this->min = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getMax(): ?FHIRString
    {
        return $this->max;
    }

    public function setMax(string|FHIRString|null $value): self
    {
        $this->max = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSourceId(): ?FHIRId
    {
        return $this->sourceId;
    }

    public function setSourceId(string|FHIRId|null $value): self
    {
        $this->sourceId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getPath(): ?FHIRString
    {
        return $this->path;
    }

    public function setPath(string|FHIRString|null $value): self
    {
        $this->path = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSliceName(): ?FHIRString
    {
        return $this->sliceName;
    }

    public function setSliceName(string|FHIRString|null $value): self
    {
        $this->sliceName = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTargetId(): ?FHIRId
    {
        return $this->targetId;
    }

    public function setTargetId(string|FHIRId|null $value): self
    {
        $this->targetId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getParams(): ?FHIRString
    {
        return $this->params;
    }

    public function setParams(string|FHIRString|null $value): self
    {
        $this->params = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRGraphDefinitionLinkCompartment[]
     */
    public function getCompartment(): array
    {
        return $this->compartment;
    }

    public function setCompartment(?FHIRGraphDefinitionLinkCompartment ...$value): self
    {
        $this->compartment = array_filter($value);

        return $this;
    }

    public function addCompartment(?FHIRGraphDefinitionLinkCompartment ...$value): self
    {
        $this->compartment = array_filter(array_merge($this->compartment, $value));

        return $this;
    }
}
