<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestPlanTestCaseDependency Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestPlanTestCaseDependencyInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;

class FHIRTestPlanTestCaseDependency extends FHIRBackboneElement implements FHIRTestPlanTestCaseDependencyInterface
{
    public const RESOURCE_NAME = 'TestPlan.testCase.dependency';

    protected ?FHIRMarkdown $description = null;
    protected ?FHIRReference $predecessor = null;

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getPredecessor(): ?FHIRReference
    {
        return $this->predecessor;
    }

    public function setPredecessor(?FHIRReference $value): self
    {
        $this->predecessor = $value;

        return $this;
    }
}
