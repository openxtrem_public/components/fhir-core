<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceReferenceInformationTarget Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceReferenceInformationTargetInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRSubstanceReferenceInformationTarget extends FHIRBackboneElement implements FHIRSubstanceReferenceInformationTargetInterface
{
    public const RESOURCE_NAME = 'SubstanceReferenceInformation.target';

    protected ?FHIRIdentifier $target = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableConcept $interaction = null;
    protected ?FHIRCodeableConcept $organism = null;
    protected ?FHIRCodeableConcept $organismType = null;
    protected FHIRQuantity|FHIRRange|FHIRString|null $amount = null;
    protected ?FHIRCodeableConcept $amountType = null;

    /** @var FHIRReference[] */
    protected array $source = [];

    public function getTarget(): ?FHIRIdentifier
    {
        return $this->target;
    }

    public function setTarget(?FHIRIdentifier $value): self
    {
        $this->target = $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getInteraction(): ?FHIRCodeableConcept
    {
        return $this->interaction;
    }

    public function setInteraction(?FHIRCodeableConcept $value): self
    {
        $this->interaction = $value;

        return $this;
    }

    public function getOrganism(): ?FHIRCodeableConcept
    {
        return $this->organism;
    }

    public function setOrganism(?FHIRCodeableConcept $value): self
    {
        $this->organism = $value;

        return $this;
    }

    public function getOrganismType(): ?FHIRCodeableConcept
    {
        return $this->organismType;
    }

    public function setOrganismType(?FHIRCodeableConcept $value): self
    {
        $this->organismType = $value;

        return $this;
    }

    public function getAmount(): FHIRQuantity|FHIRRange|FHIRString|null
    {
        return $this->amount;
    }

    public function setAmount(FHIRQuantity|FHIRRange|FHIRString|null $value): self
    {
        $this->amount = $value;

        return $this;
    }

    public function getAmountType(): ?FHIRCodeableConcept
    {
        return $this->amountType;
    }

    public function setAmountType(?FHIRCodeableConcept $value): self
    {
        $this->amountType = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSource(): array
    {
        return $this->source;
    }

    public function setSource(?FHIRReference ...$value): self
    {
        $this->source = array_filter($value);

        return $this;
    }

    public function addSource(?FHIRReference ...$value): self
    {
        $this->source = array_filter(array_merge($this->source, $value));

        return $this;
    }
}
