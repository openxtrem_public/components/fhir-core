<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AuditEventOutcome Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAuditEventOutcomeInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;

class FHIRAuditEventOutcome extends FHIRBackboneElement implements FHIRAuditEventOutcomeInterface
{
    public const RESOURCE_NAME = 'AuditEvent.outcome';

    protected ?FHIRCoding $code = null;

    /** @var FHIRCodeableConcept[] */
    protected array $detail = [];

    public function getCode(): ?FHIRCoding
    {
        return $this->code;
    }

    public function setCode(?FHIRCoding $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getDetail(): array
    {
        return $this->detail;
    }

    public function setDetail(?FHIRCodeableConcept ...$value): self
    {
        $this->detail = array_filter($value);

        return $this;
    }

    public function addDetail(?FHIRCodeableConcept ...$value): self
    {
        $this->detail = array_filter(array_merge($this->detail, $value));

        return $this;
    }
}
