<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR LocationPosition Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRLocationPositionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDecimal;

class FHIRLocationPosition extends FHIRBackboneElement implements FHIRLocationPositionInterface
{
    public const RESOURCE_NAME = 'Location.position';

    protected ?FHIRDecimal $longitude = null;
    protected ?FHIRDecimal $latitude = null;
    protected ?FHIRDecimal $altitude = null;

    public function getLongitude(): ?FHIRDecimal
    {
        return $this->longitude;
    }

    public function setLongitude(float|FHIRDecimal|null $value): self
    {
        $this->longitude = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getLatitude(): ?FHIRDecimal
    {
        return $this->latitude;
    }

    public function setLatitude(float|FHIRDecimal|null $value): self
    {
        $this->latitude = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getAltitude(): ?FHIRDecimal
    {
        return $this->altitude;
    }

    public function setAltitude(float|FHIRDecimal|null $value): self
    {
        $this->altitude = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }
}
