<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EvidenceReportRelatesTo Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREvidenceReportRelatesToInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;

class FHIREvidenceReportRelatesTo extends FHIRBackboneElement implements FHIREvidenceReportRelatesToInterface
{
    public const RESOURCE_NAME = 'EvidenceReport.relatesTo';

    protected ?FHIRCode $code = null;
    protected ?FHIREvidenceReportRelatesToTarget $target = null;

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getTarget(): ?FHIREvidenceReportRelatesToTarget
    {
        return $this->target;
    }

    public function setTarget(?FHIREvidenceReportRelatesToTarget $value): self
    {
        $this->target = $value;

        return $this;
    }
}
