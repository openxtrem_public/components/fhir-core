<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestPlanTestCase Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestPlanTestCaseInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInteger;

class FHIRTestPlanTestCase extends FHIRBackboneElement implements FHIRTestPlanTestCaseInterface
{
    public const RESOURCE_NAME = 'TestPlan.testCase';

    protected ?FHIRInteger $sequence = null;

    /** @var FHIRReference[] */
    protected array $scope = [];

    /** @var FHIRTestPlanTestCaseDependency[] */
    protected array $dependency = [];

    /** @var FHIRTestPlanTestCaseTestRun[] */
    protected array $testRun = [];

    /** @var FHIRTestPlanTestCaseTestData[] */
    protected array $testData = [];

    /** @var FHIRTestPlanTestCaseAssertion[] */
    protected array $assertion = [];

    public function getSequence(): ?FHIRInteger
    {
        return $this->sequence;
    }

    public function setSequence(int|FHIRInteger|null $value): self
    {
        $this->sequence = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getScope(): array
    {
        return $this->scope;
    }

    public function setScope(?FHIRReference ...$value): self
    {
        $this->scope = array_filter($value);

        return $this;
    }

    public function addScope(?FHIRReference ...$value): self
    {
        $this->scope = array_filter(array_merge($this->scope, $value));

        return $this;
    }

    /**
     * @return FHIRTestPlanTestCaseDependency[]
     */
    public function getDependency(): array
    {
        return $this->dependency;
    }

    public function setDependency(?FHIRTestPlanTestCaseDependency ...$value): self
    {
        $this->dependency = array_filter($value);

        return $this;
    }

    public function addDependency(?FHIRTestPlanTestCaseDependency ...$value): self
    {
        $this->dependency = array_filter(array_merge($this->dependency, $value));

        return $this;
    }

    /**
     * @return FHIRTestPlanTestCaseTestRun[]
     */
    public function getTestRun(): array
    {
        return $this->testRun;
    }

    public function setTestRun(?FHIRTestPlanTestCaseTestRun ...$value): self
    {
        $this->testRun = array_filter($value);

        return $this;
    }

    public function addTestRun(?FHIRTestPlanTestCaseTestRun ...$value): self
    {
        $this->testRun = array_filter(array_merge($this->testRun, $value));

        return $this;
    }

    /**
     * @return FHIRTestPlanTestCaseTestData[]
     */
    public function getTestData(): array
    {
        return $this->testData;
    }

    public function setTestData(?FHIRTestPlanTestCaseTestData ...$value): self
    {
        $this->testData = array_filter($value);

        return $this;
    }

    public function addTestData(?FHIRTestPlanTestCaseTestData ...$value): self
    {
        $this->testData = array_filter(array_merge($this->testData, $value));

        return $this;
    }

    /**
     * @return FHIRTestPlanTestCaseAssertion[]
     */
    public function getAssertion(): array
    {
        return $this->assertion;
    }

    public function setAssertion(?FHIRTestPlanTestCaseAssertion ...$value): self
    {
        $this->assertion = array_filter($value);

        return $this;
    }

    public function addAssertion(?FHIRTestPlanTestCaseAssertion ...$value): self
    {
        $this->assertion = array_filter(array_merge($this->assertion, $value));

        return $this;
    }
}
