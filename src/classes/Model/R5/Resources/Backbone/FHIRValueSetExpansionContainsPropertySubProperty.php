<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ValueSetExpansionContainsPropertySubProperty Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRValueSetExpansionContainsPropertySubPropertyInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRValueSetExpansionContainsPropertySubProperty extends FHIRBackboneElement implements FHIRValueSetExpansionContainsPropertySubPropertyInterface
{
    public const RESOURCE_NAME = 'ValueSet.expansion.contains.property.subProperty';

    protected ?FHIRCode $code = null;
    protected FHIRCode|FHIRCoding|FHIRString|FHIRInteger|FHIRBoolean|FHIRDateTime|FHIRDecimal|null $value = null;

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getValue(): FHIRCode|FHIRCoding|FHIRString|FHIRInteger|FHIRBoolean|FHIRDateTime|FHIRDecimal|null
    {
        return $this->value;
    }

    public function setValue(
        FHIRCode|FHIRCoding|FHIRString|FHIRInteger|FHIRBoolean|FHIRDateTime|FHIRDecimal|null $value,
    ): self
    {
        $this->value = $value;

        return $this;
    }
}
