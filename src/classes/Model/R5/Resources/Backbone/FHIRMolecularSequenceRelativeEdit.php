<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MolecularSequenceRelativeEdit Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMolecularSequenceRelativeEditInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRMolecularSequenceRelativeEdit extends FHIRBackboneElement implements FHIRMolecularSequenceRelativeEditInterface
{
    public const RESOURCE_NAME = 'MolecularSequence.relative.edit';

    protected ?FHIRInteger $start = null;
    protected ?FHIRInteger $end = null;
    protected ?FHIRString $replacementSequence = null;
    protected ?FHIRString $replacedSequence = null;

    public function getStart(): ?FHIRInteger
    {
        return $this->start;
    }

    public function setStart(int|FHIRInteger|null $value): self
    {
        $this->start = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getEnd(): ?FHIRInteger
    {
        return $this->end;
    }

    public function setEnd(int|FHIRInteger|null $value): self
    {
        $this->end = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getReplacementSequence(): ?FHIRString
    {
        return $this->replacementSequence;
    }

    public function setReplacementSequence(string|FHIRString|null $value): self
    {
        $this->replacementSequence = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getReplacedSequence(): ?FHIRString
    {
        return $this->replacedSequence;
    }

    public function setReplacedSequence(string|FHIRString|null $value): self
    {
        $this->replacedSequence = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
