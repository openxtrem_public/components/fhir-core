<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MessageDefinitionFocus Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMessageDefinitionFocusInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUnsignedInt;

class FHIRMessageDefinitionFocus extends FHIRBackboneElement implements FHIRMessageDefinitionFocusInterface
{
    public const RESOURCE_NAME = 'MessageDefinition.focus';

    protected ?FHIRCode $code = null;
    protected ?FHIRCanonical $profile = null;
    protected ?FHIRUnsignedInt $min = null;
    protected ?FHIRString $max = null;

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getProfile(): ?FHIRCanonical
    {
        return $this->profile;
    }

    public function setProfile(string|FHIRCanonical|null $value): self
    {
        $this->profile = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    public function getMin(): ?FHIRUnsignedInt
    {
        return $this->min;
    }

    public function setMin(int|FHIRUnsignedInt|null $value): self
    {
        $this->min = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getMax(): ?FHIRString
    {
        return $this->max;
    }

    public function setMax(string|FHIRString|null $value): self
    {
        $this->max = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
