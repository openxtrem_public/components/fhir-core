<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ConsentProvision Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRConsentProvisionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRExpression;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;

class FHIRConsentProvision extends FHIRBackboneElement implements FHIRConsentProvisionInterface
{
    public const RESOURCE_NAME = 'Consent.provision';

    protected ?FHIRPeriod $period = null;

    /** @var FHIRConsentProvisionActor[] */
    protected array $actor = [];

    /** @var FHIRCodeableConcept[] */
    protected array $action = [];

    /** @var FHIRCoding[] */
    protected array $securityLabel = [];

    /** @var FHIRCoding[] */
    protected array $purpose = [];

    /** @var FHIRCoding[] */
    protected array $documentType = [];

    /** @var FHIRCoding[] */
    protected array $resourceType = [];

    /** @var FHIRCodeableConcept[] */
    protected array $code = [];
    protected ?FHIRPeriod $dataPeriod = null;

    /** @var FHIRConsentProvisionData[] */
    protected array $data = [];
    protected ?FHIRExpression $expression = null;

    /** @var FHIRConsentProvision[] */
    protected array $provision = [];

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    /**
     * @return FHIRConsentProvisionActor[]
     */
    public function getActor(): array
    {
        return $this->actor;
    }

    public function setActor(?FHIRConsentProvisionActor ...$value): self
    {
        $this->actor = array_filter($value);

        return $this;
    }

    public function addActor(?FHIRConsentProvisionActor ...$value): self
    {
        $this->actor = array_filter(array_merge($this->actor, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getAction(): array
    {
        return $this->action;
    }

    public function setAction(?FHIRCodeableConcept ...$value): self
    {
        $this->action = array_filter($value);

        return $this;
    }

    public function addAction(?FHIRCodeableConcept ...$value): self
    {
        $this->action = array_filter(array_merge($this->action, $value));

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getSecurityLabel(): array
    {
        return $this->securityLabel;
    }

    public function setSecurityLabel(?FHIRCoding ...$value): self
    {
        $this->securityLabel = array_filter($value);

        return $this;
    }

    public function addSecurityLabel(?FHIRCoding ...$value): self
    {
        $this->securityLabel = array_filter(array_merge($this->securityLabel, $value));

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getPurpose(): array
    {
        return $this->purpose;
    }

    public function setPurpose(?FHIRCoding ...$value): self
    {
        $this->purpose = array_filter($value);

        return $this;
    }

    public function addPurpose(?FHIRCoding ...$value): self
    {
        $this->purpose = array_filter(array_merge($this->purpose, $value));

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getDocumentType(): array
    {
        return $this->documentType;
    }

    public function setDocumentType(?FHIRCoding ...$value): self
    {
        $this->documentType = array_filter($value);

        return $this;
    }

    public function addDocumentType(?FHIRCoding ...$value): self
    {
        $this->documentType = array_filter(array_merge($this->documentType, $value));

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getResourceType(): array
    {
        return $this->resourceType;
    }

    public function setResourceType(?FHIRCoding ...$value): self
    {
        $this->resourceType = array_filter($value);

        return $this;
    }

    public function addResourceType(?FHIRCoding ...$value): self
    {
        $this->resourceType = array_filter(array_merge($this->resourceType, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCode(): array
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept ...$value): self
    {
        $this->code = array_filter($value);

        return $this;
    }

    public function addCode(?FHIRCodeableConcept ...$value): self
    {
        $this->code = array_filter(array_merge($this->code, $value));

        return $this;
    }

    public function getDataPeriod(): ?FHIRPeriod
    {
        return $this->dataPeriod;
    }

    public function setDataPeriod(?FHIRPeriod $value): self
    {
        $this->dataPeriod = $value;

        return $this;
    }

    /**
     * @return FHIRConsentProvisionData[]
     */
    public function getData(): array
    {
        return $this->data;
    }

    public function setData(?FHIRConsentProvisionData ...$value): self
    {
        $this->data = array_filter($value);

        return $this;
    }

    public function addData(?FHIRConsentProvisionData ...$value): self
    {
        $this->data = array_filter(array_merge($this->data, $value));

        return $this;
    }

    public function getExpression(): ?FHIRExpression
    {
        return $this->expression;
    }

    public function setExpression(?FHIRExpression $value): self
    {
        $this->expression = $value;

        return $this;
    }

    /**
     * @return FHIRConsentProvision[]
     */
    public function getProvision(): array
    {
        return $this->provision;
    }

    public function setProvision(?FHIRConsentProvision ...$value): self
    {
        $this->provision = array_filter($value);

        return $this;
    }

    public function addProvision(?FHIRConsentProvision ...$value): self
    {
        $this->provision = array_filter(array_merge($this->provision, $value));

        return $this;
    }
}
