<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SpecimenContainer Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSpecimenContainerInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRSpecimenContainer extends FHIRBackboneElement implements FHIRSpecimenContainerInterface
{
    public const RESOURCE_NAME = 'Specimen.container';

    protected ?FHIRReference $device = null;
    protected ?FHIRReference $location = null;
    protected ?FHIRQuantity $specimenQuantity = null;

    public function getDevice(): ?FHIRReference
    {
        return $this->device;
    }

    public function setDevice(?FHIRReference $value): self
    {
        $this->device = $value;

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    public function getSpecimenQuantity(): ?FHIRQuantity
    {
        return $this->specimenQuantity;
    }

    public function setSpecimenQuantity(?FHIRQuantity $value): self
    {
        $this->specimenQuantity = $value;

        return $this;
    }
}
