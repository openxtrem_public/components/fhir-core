<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EndpointPayload Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREndpointPayloadInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;

class FHIREndpointPayload extends FHIRBackboneElement implements FHIREndpointPayloadInterface
{
    public const RESOURCE_NAME = 'Endpoint.payload';

    /** @var FHIRCodeableConcept[] */
    protected array $type = [];

    /** @var FHIRCode[] */
    protected array $mimeType = [];

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getMimeType(): array
    {
        return $this->mimeType;
    }

    public function setMimeType(string|FHIRCode|null ...$value): self
    {
        $this->mimeType = [];
        $this->addMimeType(...$value);

        return $this;
    }

    public function addMimeType(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->mimeType = array_filter(array_merge($this->mimeType, $values));

        return $this;
    }
}
