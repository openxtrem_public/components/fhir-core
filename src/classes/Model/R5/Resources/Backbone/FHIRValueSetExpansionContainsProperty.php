<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ValueSetExpansionContainsProperty Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRValueSetExpansionContainsPropertyInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRValueSetExpansionContainsProperty extends FHIRBackboneElement implements FHIRValueSetExpansionContainsPropertyInterface
{
    public const RESOURCE_NAME = 'ValueSet.expansion.contains.property';

    protected ?FHIRCode $code = null;
    protected FHIRCode|FHIRCoding|FHIRString|FHIRInteger|FHIRBoolean|FHIRDateTime|FHIRDecimal|null $value = null;

    /** @var FHIRValueSetExpansionContainsPropertySubProperty[] */
    protected array $subProperty = [];

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getValue(): FHIRCode|FHIRCoding|FHIRString|FHIRInteger|FHIRBoolean|FHIRDateTime|FHIRDecimal|null
    {
        return $this->value;
    }

    public function setValue(
        FHIRCode|FHIRCoding|FHIRString|FHIRInteger|FHIRBoolean|FHIRDateTime|FHIRDecimal|null $value,
    ): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return FHIRValueSetExpansionContainsPropertySubProperty[]
     */
    public function getSubProperty(): array
    {
        return $this->subProperty;
    }

    public function setSubProperty(?FHIRValueSetExpansionContainsPropertySubProperty ...$value): self
    {
        $this->subProperty = array_filter($value);

        return $this;
    }

    public function addSubProperty(?FHIRValueSetExpansionContainsPropertySubProperty ...$value): self
    {
        $this->subProperty = array_filter(array_merge($this->subProperty, $value));

        return $this;
    }
}
