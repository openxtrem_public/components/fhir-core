<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR BundleEntry Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRBundleEntryInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R5\Resources\FHIRResource;

class FHIRBundleEntry extends FHIRBackboneElement implements FHIRBundleEntryInterface
{
    public const RESOURCE_NAME = 'Bundle.entry';

    /** @var FHIRBundleLink[] */
    protected array $link = [];
    protected ?FHIRUri $fullUrl = null;
    protected ?FHIRResource $resource = null;
    protected ?FHIRBundleEntrySearch $search = null;
    protected ?FHIRBundleEntryRequest $request = null;
    protected ?FHIRBundleEntryResponse $response = null;

    /**
     * @return FHIRBundleLink[]
     */
    public function getLink(): array
    {
        return $this->link;
    }

    public function setLink(?FHIRBundleLink ...$value): self
    {
        $this->link = array_filter($value);

        return $this;
    }

    public function addLink(?FHIRBundleLink ...$value): self
    {
        $this->link = array_filter(array_merge($this->link, $value));

        return $this;
    }

    public function getFullUrl(): ?FHIRUri
    {
        return $this->fullUrl;
    }

    public function setFullUrl(string|FHIRUri|null $value): self
    {
        $this->fullUrl = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getResource(): ?FHIRResource
    {
        return $this->resource;
    }

    public function setResource(?FHIRResource $value): self
    {
        $this->resource = $value;

        return $this;
    }

    public function getSearch(): ?FHIRBundleEntrySearch
    {
        return $this->search;
    }

    public function setSearch(?FHIRBundleEntrySearch $value): self
    {
        $this->search = $value;

        return $this;
    }

    public function getRequest(): ?FHIRBundleEntryRequest
    {
        return $this->request;
    }

    public function setRequest(?FHIRBundleEntryRequest $value): self
    {
        $this->request = $value;

        return $this;
    }

    public function getResponse(): ?FHIRBundleEntryResponse
    {
        return $this->response;
    }

    public function setResponse(?FHIRBundleEntryResponse $value): self
    {
        $this->response = $value;

        return $this;
    }
}
