<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR InventoryItemDescription Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRInventoryItemDescriptionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRInventoryItemDescription extends FHIRBackboneElement implements FHIRInventoryItemDescriptionInterface
{
    public const RESOURCE_NAME = 'InventoryItem.description';

    protected ?FHIRCode $language = null;
    protected ?FHIRString $description = null;

    public function getLanguage(): ?FHIRCode
    {
        return $this->language;
    }

    public function setLanguage(string|FHIRCode|null $value): self
    {
        $this->language = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
