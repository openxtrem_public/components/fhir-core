<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AppointmentRecurrenceTemplateYearlyTemplate Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAppointmentRecurrenceTemplateYearlyTemplateInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;

class FHIRAppointmentRecurrenceTemplateYearlyTemplate extends FHIRBackboneElement implements FHIRAppointmentRecurrenceTemplateYearlyTemplateInterface
{
    public const RESOURCE_NAME = 'Appointment.recurrenceTemplate.yearlyTemplate';

    protected ?FHIRPositiveInt $yearInterval = null;

    public function getYearInterval(): ?FHIRPositiveInt
    {
        return $this->yearInterval;
    }

    public function setYearInterval(int|FHIRPositiveInt|null $value): self
    {
        $this->yearInterval = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }
}
