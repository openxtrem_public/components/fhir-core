<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ObservationDefinitionQualifiedValue Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRObservationDefinitionQualifiedValueInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRObservationDefinitionQualifiedValue extends FHIRBackboneElement implements FHIRObservationDefinitionQualifiedValueInterface
{
    public const RESOURCE_NAME = 'ObservationDefinition.qualifiedValue';

    protected ?FHIRCodeableConcept $context = null;

    /** @var FHIRCodeableConcept[] */
    protected array $appliesTo = [];
    protected ?FHIRCode $gender = null;
    protected ?FHIRRange $age = null;
    protected ?FHIRRange $gestationalAge = null;
    protected ?FHIRString $condition = null;
    protected ?FHIRCode $rangeCategory = null;
    protected ?FHIRRange $range = null;
    protected ?FHIRCanonical $validCodedValueSet = null;
    protected ?FHIRCanonical $normalCodedValueSet = null;
    protected ?FHIRCanonical $abnormalCodedValueSet = null;
    protected ?FHIRCanonical $criticalCodedValueSet = null;

    public function getContext(): ?FHIRCodeableConcept
    {
        return $this->context;
    }

    public function setContext(?FHIRCodeableConcept $value): self
    {
        $this->context = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getAppliesTo(): array
    {
        return $this->appliesTo;
    }

    public function setAppliesTo(?FHIRCodeableConcept ...$value): self
    {
        $this->appliesTo = array_filter($value);

        return $this;
    }

    public function addAppliesTo(?FHIRCodeableConcept ...$value): self
    {
        $this->appliesTo = array_filter(array_merge($this->appliesTo, $value));

        return $this;
    }

    public function getGender(): ?FHIRCode
    {
        return $this->gender;
    }

    public function setGender(string|FHIRCode|null $value): self
    {
        $this->gender = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getAge(): ?FHIRRange
    {
        return $this->age;
    }

    public function setAge(?FHIRRange $value): self
    {
        $this->age = $value;

        return $this;
    }

    public function getGestationalAge(): ?FHIRRange
    {
        return $this->gestationalAge;
    }

    public function setGestationalAge(?FHIRRange $value): self
    {
        $this->gestationalAge = $value;

        return $this;
    }

    public function getCondition(): ?FHIRString
    {
        return $this->condition;
    }

    public function setCondition(string|FHIRString|null $value): self
    {
        $this->condition = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getRangeCategory(): ?FHIRCode
    {
        return $this->rangeCategory;
    }

    public function setRangeCategory(string|FHIRCode|null $value): self
    {
        $this->rangeCategory = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getRange(): ?FHIRRange
    {
        return $this->range;
    }

    public function setRange(?FHIRRange $value): self
    {
        $this->range = $value;

        return $this;
    }

    public function getValidCodedValueSet(): ?FHIRCanonical
    {
        return $this->validCodedValueSet;
    }

    public function setValidCodedValueSet(string|FHIRCanonical|null $value): self
    {
        $this->validCodedValueSet = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    public function getNormalCodedValueSet(): ?FHIRCanonical
    {
        return $this->normalCodedValueSet;
    }

    public function setNormalCodedValueSet(string|FHIRCanonical|null $value): self
    {
        $this->normalCodedValueSet = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    public function getAbnormalCodedValueSet(): ?FHIRCanonical
    {
        return $this->abnormalCodedValueSet;
    }

    public function setAbnormalCodedValueSet(string|FHIRCanonical|null $value): self
    {
        $this->abnormalCodedValueSet = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    public function getCriticalCodedValueSet(): ?FHIRCanonical
    {
        return $this->criticalCodedValueSet;
    }

    public function setCriticalCodedValueSet(string|FHIRCanonical|null $value): self
    {
        $this->criticalCodedValueSet = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }
}
