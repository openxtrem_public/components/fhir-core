<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR BodyStructureIncludedStructureBodyLandmarkOrientation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRBodyStructureIncludedStructureBodyLandmarkOrientationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;

class FHIRBodyStructureIncludedStructureBodyLandmarkOrientation extends FHIRBackboneElement implements FHIRBodyStructureIncludedStructureBodyLandmarkOrientationInterface
{
    public const RESOURCE_NAME = 'BodyStructure.includedStructure.bodyLandmarkOrientation';

    /** @var FHIRCodeableConcept[] */
    protected array $landmarkDescription = [];

    /** @var FHIRCodeableConcept[] */
    protected array $clockFacePosition = [];

    /** @var FHIRBodyStructureIncludedStructureBodyLandmarkOrientationDistanceFromLandmark[] */
    protected array $distanceFromLandmark = [];

    /** @var FHIRCodeableConcept[] */
    protected array $surfaceOrientation = [];

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getLandmarkDescription(): array
    {
        return $this->landmarkDescription;
    }

    public function setLandmarkDescription(?FHIRCodeableConcept ...$value): self
    {
        $this->landmarkDescription = array_filter($value);

        return $this;
    }

    public function addLandmarkDescription(?FHIRCodeableConcept ...$value): self
    {
        $this->landmarkDescription = array_filter(array_merge($this->landmarkDescription, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getClockFacePosition(): array
    {
        return $this->clockFacePosition;
    }

    public function setClockFacePosition(?FHIRCodeableConcept ...$value): self
    {
        $this->clockFacePosition = array_filter($value);

        return $this;
    }

    public function addClockFacePosition(?FHIRCodeableConcept ...$value): self
    {
        $this->clockFacePosition = array_filter(array_merge($this->clockFacePosition, $value));

        return $this;
    }

    /**
     * @return FHIRBodyStructureIncludedStructureBodyLandmarkOrientationDistanceFromLandmark[]
     */
    public function getDistanceFromLandmark(): array
    {
        return $this->distanceFromLandmark;
    }

    public function setDistanceFromLandmark(
        ?FHIRBodyStructureIncludedStructureBodyLandmarkOrientationDistanceFromLandmark ...$value,
    ): self
    {
        $this->distanceFromLandmark = array_filter($value);

        return $this;
    }

    public function addDistanceFromLandmark(
        ?FHIRBodyStructureIncludedStructureBodyLandmarkOrientationDistanceFromLandmark ...$value,
    ): self
    {
        $this->distanceFromLandmark = array_filter(array_merge($this->distanceFromLandmark, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSurfaceOrientation(): array
    {
        return $this->surfaceOrientation;
    }

    public function setSurfaceOrientation(?FHIRCodeableConcept ...$value): self
    {
        $this->surfaceOrientation = array_filter($value);

        return $this;
    }

    public function addSurfaceOrientation(?FHIRCodeableConcept ...$value): self
    {
        $this->surfaceOrientation = array_filter(array_merge($this->surfaceOrientation, $value));

        return $this;
    }
}
