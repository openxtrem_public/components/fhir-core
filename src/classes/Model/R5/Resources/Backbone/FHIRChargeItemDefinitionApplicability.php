<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ChargeItemDefinitionApplicability Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRChargeItemDefinitionApplicabilityInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRExpression;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRelatedArtifact;

class FHIRChargeItemDefinitionApplicability extends FHIRBackboneElement implements FHIRChargeItemDefinitionApplicabilityInterface
{
    public const RESOURCE_NAME = 'ChargeItemDefinition.applicability';

    protected ?FHIRExpression $condition = null;
    protected ?FHIRPeriod $effectivePeriod = null;
    protected ?FHIRRelatedArtifact $relatedArtifact = null;

    public function getCondition(): ?FHIRExpression
    {
        return $this->condition;
    }

    public function setCondition(?FHIRExpression $value): self
    {
        $this->condition = $value;

        return $this;
    }

    public function getEffectivePeriod(): ?FHIRPeriod
    {
        return $this->effectivePeriod;
    }

    public function setEffectivePeriod(?FHIRPeriod $value): self
    {
        $this->effectivePeriod = $value;

        return $this;
    }

    public function getRelatedArtifact(): ?FHIRRelatedArtifact
    {
        return $this->relatedArtifact;
    }

    public function setRelatedArtifact(?FHIRRelatedArtifact $value): self
    {
        $this->relatedArtifact = $value;

        return $this;
    }
}
