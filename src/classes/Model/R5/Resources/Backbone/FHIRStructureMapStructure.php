<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR StructureMapStructure Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRStructureMapStructureInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRStructureMapStructure extends FHIRBackboneElement implements FHIRStructureMapStructureInterface
{
    public const RESOURCE_NAME = 'StructureMap.structure';

    protected ?FHIRCanonical $url = null;
    protected ?FHIRCode $mode = null;
    protected ?FHIRString $alias = null;
    protected ?FHIRString $documentation = null;

    public function getUrl(): ?FHIRCanonical
    {
        return $this->url;
    }

    public function setUrl(string|FHIRCanonical|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    public function getMode(): ?FHIRCode
    {
        return $this->mode;
    }

    public function setMode(string|FHIRCode|null $value): self
    {
        $this->mode = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getAlias(): ?FHIRString
    {
        return $this->alias;
    }

    public function setAlias(string|FHIRString|null $value): self
    {
        $this->alias = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDocumentation(): ?FHIRString
    {
        return $this->documentation;
    }

    public function setDocumentation(string|FHIRString|null $value): self
    {
        $this->documentation = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
