<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExplanationOfBenefitBenefitBalanceFinancial Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExplanationOfBenefitBenefitBalanceFinancialInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUnsignedInt;

class FHIRExplanationOfBenefitBenefitBalanceFinancial extends FHIRBackboneElement implements FHIRExplanationOfBenefitBenefitBalanceFinancialInterface
{
    public const RESOURCE_NAME = 'ExplanationOfBenefit.benefitBalance.financial';

    protected ?FHIRCodeableConcept $type = null;
    protected FHIRUnsignedInt|FHIRString|FHIRMoney|null $allowed = null;
    protected FHIRUnsignedInt|FHIRMoney|null $used = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getAllowed(): FHIRUnsignedInt|FHIRString|FHIRMoney|null
    {
        return $this->allowed;
    }

    public function setAllowed(FHIRUnsignedInt|FHIRString|FHIRMoney|null $value): self
    {
        $this->allowed = $value;

        return $this;
    }

    public function getUsed(): FHIRUnsignedInt|FHIRMoney|null
    {
        return $this->used;
    }

    public function setUsed(FHIRUnsignedInt|FHIRMoney|null $value): self
    {
        $this->used = $value;

        return $this;
    }
}
