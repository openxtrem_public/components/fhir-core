<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceMetricCalibration Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceMetricCalibrationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInstant;

class FHIRDeviceMetricCalibration extends FHIRBackboneElement implements FHIRDeviceMetricCalibrationInterface
{
    public const RESOURCE_NAME = 'DeviceMetric.calibration';

    protected ?FHIRCode $type = null;
    protected ?FHIRCode $state = null;
    protected ?FHIRInstant $time = null;

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getState(): ?FHIRCode
    {
        return $this->state;
    }

    public function setState(string|FHIRCode|null $value): self
    {
        $this->state = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getTime(): ?FHIRInstant
    {
        return $this->time;
    }

    public function setTime(string|FHIRInstant|null $value): self
    {
        $this->time = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }
}
