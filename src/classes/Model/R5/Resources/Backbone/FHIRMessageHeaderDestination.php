<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MessageHeaderDestination Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMessageHeaderDestinationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUrl;

class FHIRMessageHeaderDestination extends FHIRBackboneElement implements FHIRMessageHeaderDestinationInterface
{
    public const RESOURCE_NAME = 'MessageHeader.destination';

    protected FHIRUrl|FHIRReference|null $endpoint = null;
    protected ?FHIRString $name = null;
    protected ?FHIRReference $target = null;
    protected ?FHIRReference $receiver = null;

    public function getEndpoint(): FHIRUrl|FHIRReference|null
    {
        return $this->endpoint;
    }

    public function setEndpoint(FHIRUrl|FHIRReference|null $value): self
    {
        $this->endpoint = $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTarget(): ?FHIRReference
    {
        return $this->target;
    }

    public function setTarget(?FHIRReference $value): self
    {
        $this->target = $value;

        return $this;
    }

    public function getReceiver(): ?FHIRReference
    {
        return $this->receiver;
    }

    public function setReceiver(?FHIRReference $value): self
    {
        $this->receiver = $value;

        return $this;
    }
}
