<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DocumentReferenceContent Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDocumentReferenceContentInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;

class FHIRDocumentReferenceContent extends FHIRBackboneElement implements FHIRDocumentReferenceContentInterface
{
    public const RESOURCE_NAME = 'DocumentReference.content';

    protected ?FHIRAttachment $attachment = null;

    /** @var FHIRDocumentReferenceContentProfile[] */
    protected array $profile = [];

    public function getAttachment(): ?FHIRAttachment
    {
        return $this->attachment;
    }

    public function setAttachment(?FHIRAttachment $value): self
    {
        $this->attachment = $value;

        return $this;
    }

    /**
     * @return FHIRDocumentReferenceContentProfile[]
     */
    public function getProfile(): array
    {
        return $this->profile;
    }

    public function setProfile(?FHIRDocumentReferenceContentProfile ...$value): self
    {
        $this->profile = array_filter($value);

        return $this;
    }

    public function addProfile(?FHIRDocumentReferenceContentProfile ...$value): self
    {
        $this->profile = array_filter(array_merge($this->profile, $value));

        return $this;
    }
}
