<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AppointmentRecurrenceTemplateWeeklyTemplate Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAppointmentRecurrenceTemplateWeeklyTemplateInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;

class FHIRAppointmentRecurrenceTemplateWeeklyTemplate extends FHIRBackboneElement implements FHIRAppointmentRecurrenceTemplateWeeklyTemplateInterface
{
    public const RESOURCE_NAME = 'Appointment.recurrenceTemplate.weeklyTemplate';

    protected ?FHIRBoolean $monday = null;
    protected ?FHIRBoolean $tuesday = null;
    protected ?FHIRBoolean $wednesday = null;
    protected ?FHIRBoolean $thursday = null;
    protected ?FHIRBoolean $friday = null;
    protected ?FHIRBoolean $saturday = null;
    protected ?FHIRBoolean $sunday = null;
    protected ?FHIRPositiveInt $weekInterval = null;

    public function getMonday(): ?FHIRBoolean
    {
        return $this->monday;
    }

    public function setMonday(bool|FHIRBoolean|null $value): self
    {
        $this->monday = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getTuesday(): ?FHIRBoolean
    {
        return $this->tuesday;
    }

    public function setTuesday(bool|FHIRBoolean|null $value): self
    {
        $this->tuesday = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getWednesday(): ?FHIRBoolean
    {
        return $this->wednesday;
    }

    public function setWednesday(bool|FHIRBoolean|null $value): self
    {
        $this->wednesday = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getThursday(): ?FHIRBoolean
    {
        return $this->thursday;
    }

    public function setThursday(bool|FHIRBoolean|null $value): self
    {
        $this->thursday = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getFriday(): ?FHIRBoolean
    {
        return $this->friday;
    }

    public function setFriday(bool|FHIRBoolean|null $value): self
    {
        $this->friday = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getSaturday(): ?FHIRBoolean
    {
        return $this->saturday;
    }

    public function setSaturday(bool|FHIRBoolean|null $value): self
    {
        $this->saturday = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getSunday(): ?FHIRBoolean
    {
        return $this->sunday;
    }

    public function setSunday(bool|FHIRBoolean|null $value): self
    {
        $this->sunday = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getWeekInterval(): ?FHIRPositiveInt
    {
        return $this->weekInterval;
    }

    public function setWeekInterval(int|FHIRPositiveInt|null $value): self
    {
        $this->weekInterval = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }
}
