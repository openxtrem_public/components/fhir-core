<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PackagedProductDefinitionLegalStatusOfSupply Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRPackagedProductDefinitionLegalStatusOfSupplyInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;

class FHIRPackagedProductDefinitionLegalStatusOfSupply extends FHIRBackboneElement implements FHIRPackagedProductDefinitionLegalStatusOfSupplyInterface
{
    public const RESOURCE_NAME = 'PackagedProductDefinition.legalStatusOfSupply';

    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRCodeableConcept $jurisdiction = null;

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getJurisdiction(): ?FHIRCodeableConcept
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(?FHIRCodeableConcept $value): self
    {
        $this->jurisdiction = $value;

        return $this;
    }
}
