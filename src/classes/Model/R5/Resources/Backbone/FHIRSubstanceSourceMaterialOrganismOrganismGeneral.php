<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceSourceMaterialOrganismOrganismGeneral Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceSourceMaterialOrganismOrganismGeneralInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;

class FHIRSubstanceSourceMaterialOrganismOrganismGeneral extends FHIRBackboneElement implements FHIRSubstanceSourceMaterialOrganismOrganismGeneralInterface
{
    public const RESOURCE_NAME = 'SubstanceSourceMaterial.organism.organismGeneral';

    protected ?FHIRCodeableConcept $kingdom = null;
    protected ?FHIRCodeableConcept $phylum = null;
    protected ?FHIRCodeableConcept $class = null;
    protected ?FHIRCodeableConcept $order = null;

    public function getKingdom(): ?FHIRCodeableConcept
    {
        return $this->kingdom;
    }

    public function setKingdom(?FHIRCodeableConcept $value): self
    {
        $this->kingdom = $value;

        return $this;
    }

    public function getPhylum(): ?FHIRCodeableConcept
    {
        return $this->phylum;
    }

    public function setPhylum(?FHIRCodeableConcept $value): self
    {
        $this->phylum = $value;

        return $this;
    }

    public function getClass(): ?FHIRCodeableConcept
    {
        return $this->class;
    }

    public function setClass(?FHIRCodeableConcept $value): self
    {
        $this->class = $value;

        return $this;
    }

    public function getOrder(): ?FHIRCodeableConcept
    {
        return $this->order;
    }

    public function setOrder(?FHIRCodeableConcept $value): self
    {
        $this->order = $value;

        return $this;
    }
}
