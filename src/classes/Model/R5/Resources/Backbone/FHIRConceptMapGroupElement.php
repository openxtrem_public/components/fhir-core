<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ConceptMapGroupElement Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRConceptMapGroupElementInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRConceptMapGroupElement extends FHIRBackboneElement implements FHIRConceptMapGroupElementInterface
{
    public const RESOURCE_NAME = 'ConceptMap.group.element';

    protected ?FHIRCode $code = null;
    protected ?FHIRString $display = null;
    protected ?FHIRCanonical $valueSet = null;
    protected ?FHIRBoolean $noMap = null;

    /** @var FHIRConceptMapGroupElementTarget[] */
    protected array $target = [];

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDisplay(): ?FHIRString
    {
        return $this->display;
    }

    public function setDisplay(string|FHIRString|null $value): self
    {
        $this->display = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getValueSet(): ?FHIRCanonical
    {
        return $this->valueSet;
    }

    public function setValueSet(string|FHIRCanonical|null $value): self
    {
        $this->valueSet = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    public function getNoMap(): ?FHIRBoolean
    {
        return $this->noMap;
    }

    public function setNoMap(bool|FHIRBoolean|null $value): self
    {
        $this->noMap = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRConceptMapGroupElementTarget[]
     */
    public function getTarget(): array
    {
        return $this->target;
    }

    public function setTarget(?FHIRConceptMapGroupElementTarget ...$value): self
    {
        $this->target = array_filter($value);

        return $this;
    }

    public function addTarget(?FHIRConceptMapGroupElementTarget ...$value): self
    {
        $this->target = array_filter(array_merge($this->target, $value));

        return $this;
    }
}
