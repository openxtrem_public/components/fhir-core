<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceDefinitionCharacterization Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceDefinitionCharacterizationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;

class FHIRSubstanceDefinitionCharacterization extends FHIRBackboneElement implements FHIRSubstanceDefinitionCharacterizationInterface
{
    public const RESOURCE_NAME = 'SubstanceDefinition.characterization';

    protected ?FHIRCodeableConcept $technique = null;
    protected ?FHIRCodeableConcept $form = null;
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRAttachment[] */
    protected array $file = [];

    public function getTechnique(): ?FHIRCodeableConcept
    {
        return $this->technique;
    }

    public function setTechnique(?FHIRCodeableConcept $value): self
    {
        $this->technique = $value;

        return $this;
    }

    public function getForm(): ?FHIRCodeableConcept
    {
        return $this->form;
    }

    public function setForm(?FHIRCodeableConcept $value): self
    {
        $this->form = $value;

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAttachment[]
     */
    public function getFile(): array
    {
        return $this->file;
    }

    public function setFile(?FHIRAttachment ...$value): self
    {
        $this->file = array_filter($value);

        return $this;
    }

    public function addFile(?FHIRAttachment ...$value): self
    {
        $this->file = array_filter(array_merge($this->file, $value));

        return $this;
    }
}
