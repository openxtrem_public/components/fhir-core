<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TerminologyCapabilitiesValidateCode Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTerminologyCapabilitiesValidateCodeInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;

class FHIRTerminologyCapabilitiesValidateCode extends FHIRBackboneElement implements FHIRTerminologyCapabilitiesValidateCodeInterface
{
    public const RESOURCE_NAME = 'TerminologyCapabilities.validateCode';

    protected ?FHIRBoolean $translations = null;

    public function getTranslations(): ?FHIRBoolean
    {
        return $this->translations;
    }

    public function setTranslations(bool|FHIRBoolean|null $value): self
    {
        $this->translations = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }
}
