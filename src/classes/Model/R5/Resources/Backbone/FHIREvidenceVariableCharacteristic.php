<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EvidenceVariableCharacteristic Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREvidenceVariableCharacteristicInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRExpression;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;

class FHIREvidenceVariableCharacteristic extends FHIRBackboneElement implements FHIREvidenceVariableCharacteristicInterface
{
    public const RESOURCE_NAME = 'EvidenceVariable.characteristic';

    protected ?FHIRId $linkId = null;
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];
    protected ?FHIRBoolean $exclude = null;
    protected ?FHIRReference $definitionReference = null;
    protected ?FHIRCanonical $definitionCanonical = null;
    protected ?FHIRCodeableConcept $definitionCodeableConcept = null;
    protected ?FHIRExpression $definitionExpression = null;
    protected ?FHIRId $definitionId = null;
    protected ?FHIREvidenceVariableCharacteristicDefinitionByTypeAndValue $definitionByTypeAndValue = null;
    protected ?FHIREvidenceVariableCharacteristicDefinitionByCombination $definitionByCombination = null;
    protected FHIRQuantity|FHIRRange|null $instances = null;
    protected FHIRQuantity|FHIRRange|null $duration = null;

    /** @var FHIREvidenceVariableCharacteristicTimeFromEvent[] */
    protected array $timeFromEvent = [];

    public function getLinkId(): ?FHIRId
    {
        return $this->linkId;
    }

    public function setLinkId(string|FHIRId|null $value): self
    {
        $this->linkId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    public function getExclude(): ?FHIRBoolean
    {
        return $this->exclude;
    }

    public function setExclude(bool|FHIRBoolean|null $value): self
    {
        $this->exclude = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getDefinitionReference(): ?FHIRReference
    {
        return $this->definitionReference;
    }

    public function setDefinitionReference(?FHIRReference $value): self
    {
        $this->definitionReference = $value;

        return $this;
    }

    public function getDefinitionCanonical(): ?FHIRCanonical
    {
        return $this->definitionCanonical;
    }

    public function setDefinitionCanonical(string|FHIRCanonical|null $value): self
    {
        $this->definitionCanonical = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    public function getDefinitionCodeableConcept(): ?FHIRCodeableConcept
    {
        return $this->definitionCodeableConcept;
    }

    public function setDefinitionCodeableConcept(?FHIRCodeableConcept $value): self
    {
        $this->definitionCodeableConcept = $value;

        return $this;
    }

    public function getDefinitionExpression(): ?FHIRExpression
    {
        return $this->definitionExpression;
    }

    public function setDefinitionExpression(?FHIRExpression $value): self
    {
        $this->definitionExpression = $value;

        return $this;
    }

    public function getDefinitionId(): ?FHIRId
    {
        return $this->definitionId;
    }

    public function setDefinitionId(string|FHIRId|null $value): self
    {
        $this->definitionId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getDefinitionByTypeAndValue(): ?FHIREvidenceVariableCharacteristicDefinitionByTypeAndValue
    {
        return $this->definitionByTypeAndValue;
    }

    public function setDefinitionByTypeAndValue(
        ?FHIREvidenceVariableCharacteristicDefinitionByTypeAndValue $value,
    ): self
    {
        $this->definitionByTypeAndValue = $value;

        return $this;
    }

    public function getDefinitionByCombination(): ?FHIREvidenceVariableCharacteristicDefinitionByCombination
    {
        return $this->definitionByCombination;
    }

    public function setDefinitionByCombination(
        ?FHIREvidenceVariableCharacteristicDefinitionByCombination $value,
    ): self
    {
        $this->definitionByCombination = $value;

        return $this;
    }

    public function getInstances(): FHIRQuantity|FHIRRange|null
    {
        return $this->instances;
    }

    public function setInstances(FHIRQuantity|FHIRRange|null $value): self
    {
        $this->instances = $value;

        return $this;
    }

    public function getDuration(): FHIRQuantity|FHIRRange|null
    {
        return $this->duration;
    }

    public function setDuration(FHIRQuantity|FHIRRange|null $value): self
    {
        $this->duration = $value;

        return $this;
    }

    /**
     * @return FHIREvidenceVariableCharacteristicTimeFromEvent[]
     */
    public function getTimeFromEvent(): array
    {
        return $this->timeFromEvent;
    }

    public function setTimeFromEvent(?FHIREvidenceVariableCharacteristicTimeFromEvent ...$value): self
    {
        $this->timeFromEvent = array_filter($value);

        return $this;
    }

    public function addTimeFromEvent(?FHIREvidenceVariableCharacteristicTimeFromEvent ...$value): self
    {
        $this->timeFromEvent = array_filter(array_merge($this->timeFromEvent, $value));

        return $this;
    }
}
