<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ConsentVerification Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRConsentVerificationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;

class FHIRConsentVerification extends FHIRBackboneElement implements FHIRConsentVerificationInterface
{
    public const RESOURCE_NAME = 'Consent.verification';

    protected ?FHIRBoolean $verified = null;
    protected ?FHIRCodeableConcept $verificationType = null;
    protected ?FHIRReference $verifiedBy = null;
    protected ?FHIRReference $verifiedWith = null;

    /** @var FHIRDateTime[] */
    protected array $verificationDate = [];

    public function getVerified(): ?FHIRBoolean
    {
        return $this->verified;
    }

    public function setVerified(bool|FHIRBoolean|null $value): self
    {
        $this->verified = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getVerificationType(): ?FHIRCodeableConcept
    {
        return $this->verificationType;
    }

    public function setVerificationType(?FHIRCodeableConcept $value): self
    {
        $this->verificationType = $value;

        return $this;
    }

    public function getVerifiedBy(): ?FHIRReference
    {
        return $this->verifiedBy;
    }

    public function setVerifiedBy(?FHIRReference $value): self
    {
        $this->verifiedBy = $value;

        return $this;
    }

    public function getVerifiedWith(): ?FHIRReference
    {
        return $this->verifiedWith;
    }

    public function setVerifiedWith(?FHIRReference $value): self
    {
        $this->verifiedWith = $value;

        return $this;
    }

    /**
     * @return FHIRDateTime[]
     */
    public function getVerificationDate(): array
    {
        return $this->verificationDate;
    }

    public function setVerificationDate(string|FHIRDateTime|null ...$value): self
    {
        $this->verificationDate = [];
        $this->addVerificationDate(...$value);

        return $this;
    }

    public function addVerificationDate(string|FHIRDateTime|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRDateTime())->setValue($v) : $v, $value);

        $this->verificationDate = array_filter(array_merge($this->verificationDate, $values));

        return $this;
    }
}
