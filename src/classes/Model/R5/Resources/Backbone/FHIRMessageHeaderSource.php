<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MessageHeaderSource Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMessageHeaderSourceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUrl;

class FHIRMessageHeaderSource extends FHIRBackboneElement implements FHIRMessageHeaderSourceInterface
{
    public const RESOURCE_NAME = 'MessageHeader.source';

    protected FHIRUrl|FHIRReference|null $endpoint = null;
    protected ?FHIRString $name = null;
    protected ?FHIRString $software = null;
    protected ?FHIRString $version = null;
    protected ?FHIRContactPoint $contact = null;

    public function getEndpoint(): FHIRUrl|FHIRReference|null
    {
        return $this->endpoint;
    }

    public function setEndpoint(FHIRUrl|FHIRReference|null $value): self
    {
        $this->endpoint = $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSoftware(): ?FHIRString
    {
        return $this->software;
    }

    public function setSoftware(string|FHIRString|null $value): self
    {
        $this->software = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getContact(): ?FHIRContactPoint
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactPoint $value): self
    {
        $this->contact = $value;

        return $this;
    }
}
