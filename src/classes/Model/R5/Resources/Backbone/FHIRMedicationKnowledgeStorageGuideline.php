<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledgeStorageGuideline Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationKnowledgeStorageGuidelineInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;

class FHIRMedicationKnowledgeStorageGuideline extends FHIRBackboneElement implements FHIRMedicationKnowledgeStorageGuidelineInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge.storageGuideline';

    protected ?FHIRUri $reference = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];
    protected ?FHIRDuration $stabilityDuration = null;

    /** @var FHIRMedicationKnowledgeStorageGuidelineEnvironmentalSetting[] */
    protected array $environmentalSetting = [];

    public function getReference(): ?FHIRUri
    {
        return $this->reference;
    }

    public function setReference(string|FHIRUri|null $value): self
    {
        $this->reference = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    public function getStabilityDuration(): ?FHIRDuration
    {
        return $this->stabilityDuration;
    }

    public function setStabilityDuration(?FHIRDuration $value): self
    {
        $this->stabilityDuration = $value;

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeStorageGuidelineEnvironmentalSetting[]
     */
    public function getEnvironmentalSetting(): array
    {
        return $this->environmentalSetting;
    }

    public function setEnvironmentalSetting(
        ?FHIRMedicationKnowledgeStorageGuidelineEnvironmentalSetting ...$value,
    ): self
    {
        $this->environmentalSetting = array_filter($value);

        return $this;
    }

    public function addEnvironmentalSetting(
        ?FHIRMedicationKnowledgeStorageGuidelineEnvironmentalSetting ...$value,
    ): self
    {
        $this->environmentalSetting = array_filter(array_merge($this->environmentalSetting, $value));

        return $this;
    }
}
