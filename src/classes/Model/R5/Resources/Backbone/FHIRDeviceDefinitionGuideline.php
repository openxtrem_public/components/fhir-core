<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceDefinitionGuideline Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceDefinitionGuidelineInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRelatedArtifact;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRDeviceDefinitionGuideline extends FHIRBackboneElement implements FHIRDeviceDefinitionGuidelineInterface
{
    public const RESOURCE_NAME = 'DeviceDefinition.guideline';

    /** @var FHIRUsageContext[] */
    protected array $useContext = [];
    protected ?FHIRMarkdown $usageInstruction = null;

    /** @var FHIRRelatedArtifact[] */
    protected array $relatedArtifact = [];

    /** @var FHIRCodeableConcept[] */
    protected array $indication = [];

    /** @var FHIRCodeableConcept[] */
    protected array $contraindication = [];

    /** @var FHIRCodeableConcept[] */
    protected array $warning = [];
    protected ?FHIRString $intendedUse = null;

    /**
     * @return FHIRUsageContext[]
     */
    public function getUseContext(): array
    {
        return $this->useContext;
    }

    public function setUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter($value);

        return $this;
    }

    public function addUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter(array_merge($this->useContext, $value));

        return $this;
    }

    public function getUsageInstruction(): ?FHIRMarkdown
    {
        return $this->usageInstruction;
    }

    public function setUsageInstruction(string|FHIRMarkdown|null $value): self
    {
        $this->usageInstruction = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRRelatedArtifact[]
     */
    public function getRelatedArtifact(): array
    {
        return $this->relatedArtifact;
    }

    public function setRelatedArtifact(?FHIRRelatedArtifact ...$value): self
    {
        $this->relatedArtifact = array_filter($value);

        return $this;
    }

    public function addRelatedArtifact(?FHIRRelatedArtifact ...$value): self
    {
        $this->relatedArtifact = array_filter(array_merge($this->relatedArtifact, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getIndication(): array
    {
        return $this->indication;
    }

    public function setIndication(?FHIRCodeableConcept ...$value): self
    {
        $this->indication = array_filter($value);

        return $this;
    }

    public function addIndication(?FHIRCodeableConcept ...$value): self
    {
        $this->indication = array_filter(array_merge($this->indication, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getContraindication(): array
    {
        return $this->contraindication;
    }

    public function setContraindication(?FHIRCodeableConcept ...$value): self
    {
        $this->contraindication = array_filter($value);

        return $this;
    }

    public function addContraindication(?FHIRCodeableConcept ...$value): self
    {
        $this->contraindication = array_filter(array_merge($this->contraindication, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getWarning(): array
    {
        return $this->warning;
    }

    public function setWarning(?FHIRCodeableConcept ...$value): self
    {
        $this->warning = array_filter($value);

        return $this;
    }

    public function addWarning(?FHIRCodeableConcept ...$value): self
    {
        $this->warning = array_filter(array_merge($this->warning, $value));

        return $this;
    }

    public function getIntendedUse(): ?FHIRString
    {
        return $this->intendedUse;
    }

    public function setIntendedUse(string|FHIRString|null $value): self
    {
        $this->intendedUse = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
