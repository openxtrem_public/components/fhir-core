<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceAssociationOperation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceAssociationOperationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRDeviceAssociationOperation extends FHIRBackboneElement implements FHIRDeviceAssociationOperationInterface
{
    public const RESOURCE_NAME = 'DeviceAssociation.operation';

    protected ?FHIRCodeableConcept $status = null;

    /** @var FHIRReference[] */
    protected array $operator = [];
    protected ?FHIRPeriod $period = null;

    public function getStatus(): ?FHIRCodeableConcept
    {
        return $this->status;
    }

    public function setStatus(?FHIRCodeableConcept $value): self
    {
        $this->status = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getOperator(): array
    {
        return $this->operator;
    }

    public function setOperator(?FHIRReference ...$value): self
    {
        $this->operator = array_filter($value);

        return $this;
    }

    public function addOperator(?FHIRReference ...$value): self
    {
        $this->operator = array_filter(array_merge($this->operator, $value));

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }
}
