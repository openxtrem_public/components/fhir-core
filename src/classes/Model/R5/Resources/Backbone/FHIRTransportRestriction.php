<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TransportRestriction Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTransportRestrictionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;

class FHIRTransportRestriction extends FHIRBackboneElement implements FHIRTransportRestrictionInterface
{
    public const RESOURCE_NAME = 'Transport.restriction';

    protected ?FHIRPositiveInt $repetitions = null;
    protected ?FHIRPeriod $period = null;

    /** @var FHIRReference[] */
    protected array $recipient = [];

    public function getRepetitions(): ?FHIRPositiveInt
    {
        return $this->repetitions;
    }

    public function setRepetitions(int|FHIRPositiveInt|null $value): self
    {
        $this->repetitions = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getRecipient(): array
    {
        return $this->recipient;
    }

    public function setRecipient(?FHIRReference ...$value): self
    {
        $this->recipient = array_filter($value);

        return $this;
    }

    public function addRecipient(?FHIRReference ...$value): self
    {
        $this->recipient = array_filter(array_merge($this->recipient, $value));

        return $this;
    }
}
