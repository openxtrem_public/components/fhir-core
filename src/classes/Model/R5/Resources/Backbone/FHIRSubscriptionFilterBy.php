<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubscriptionFilterBy Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubscriptionFilterByInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;

class FHIRSubscriptionFilterBy extends FHIRBackboneElement implements FHIRSubscriptionFilterByInterface
{
    public const RESOURCE_NAME = 'Subscription.filterBy';

    protected ?FHIRUri $resourceType = null;
    protected ?FHIRString $filterParameter = null;
    protected ?FHIRCode $comparator = null;
    protected ?FHIRCode $modifier = null;
    protected ?FHIRString $value = null;

    public function getResourceType(): ?FHIRUri
    {
        return $this->resourceType;
    }

    public function setResourceType(string|FHIRUri|null $value): self
    {
        $this->resourceType = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getFilterParameter(): ?FHIRString
    {
        return $this->filterParameter;
    }

    public function setFilterParameter(string|FHIRString|null $value): self
    {
        $this->filterParameter = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getComparator(): ?FHIRCode
    {
        return $this->comparator;
    }

    public function setComparator(string|FHIRCode|null $value): self
    {
        $this->comparator = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getModifier(): ?FHIRCode
    {
        return $this->modifier;
    }

    public function setModifier(string|FHIRCode|null $value): self
    {
        $this->modifier = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getValue(): ?FHIRString
    {
        return $this->value;
    }

    public function setValue(string|FHIRString|null $value): self
    {
        $this->value = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
