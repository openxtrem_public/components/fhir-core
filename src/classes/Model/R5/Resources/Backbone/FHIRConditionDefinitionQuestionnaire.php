<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ConditionDefinitionQuestionnaire Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRConditionDefinitionQuestionnaireInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;

class FHIRConditionDefinitionQuestionnaire extends FHIRBackboneElement implements FHIRConditionDefinitionQuestionnaireInterface
{
    public const RESOURCE_NAME = 'ConditionDefinition.questionnaire';

    protected ?FHIRCode $purpose = null;
    protected ?FHIRReference $reference = null;

    public function getPurpose(): ?FHIRCode
    {
        return $this->purpose;
    }

    public function setPurpose(string|FHIRCode|null $value): self
    {
        $this->purpose = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getReference(): ?FHIRReference
    {
        return $this->reference;
    }

    public function setReference(?FHIRReference $value): self
    {
        $this->reference = $value;

        return $this;
    }
}
