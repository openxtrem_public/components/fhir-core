<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ConceptMapGroupElementTargetProperty Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRConceptMapGroupElementTargetPropertyInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRConceptMapGroupElementTargetProperty extends FHIRBackboneElement implements FHIRConceptMapGroupElementTargetPropertyInterface
{
    public const RESOURCE_NAME = 'ConceptMap.group.element.target.property';

    protected ?FHIRCode $code = null;
    protected FHIRCoding|FHIRString|FHIRInteger|FHIRBoolean|FHIRDateTime|FHIRDecimal|FHIRCode|null $value = null;

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getValue(): FHIRCoding|FHIRString|FHIRInteger|FHIRBoolean|FHIRDateTime|FHIRDecimal|FHIRCode|null
    {
        return $this->value;
    }

    public function setValue(
        FHIRCoding|FHIRString|FHIRInteger|FHIRBoolean|FHIRDateTime|FHIRDecimal|FHIRCode|null $value,
    ): self
    {
        $this->value = $value;

        return $this;
    }
}
