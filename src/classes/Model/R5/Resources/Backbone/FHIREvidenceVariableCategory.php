<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EvidenceVariableCategory Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREvidenceVariableCategoryInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIREvidenceVariableCategory extends FHIRBackboneElement implements FHIREvidenceVariableCategoryInterface
{
    public const RESOURCE_NAME = 'EvidenceVariable.category';

    protected ?FHIRString $name = null;
    protected FHIRCodeableConcept|FHIRQuantity|FHIRRange|null $value = null;

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getValue(): FHIRCodeableConcept|FHIRQuantity|FHIRRange|null
    {
        return $this->value;
    }

    public function setValue(FHIRCodeableConcept|FHIRQuantity|FHIRRange|null $value): self
    {
        $this->value = $value;

        return $this;
    }
}
