<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExplanationOfBenefitCareTeam Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExplanationOfBenefitCareTeamInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;

class FHIRExplanationOfBenefitCareTeam extends FHIRBackboneElement implements FHIRExplanationOfBenefitCareTeamInterface
{
    public const RESOURCE_NAME = 'ExplanationOfBenefit.careTeam';

    protected ?FHIRPositiveInt $sequence = null;
    protected ?FHIRReference $provider = null;
    protected ?FHIRBoolean $responsible = null;
    protected ?FHIRCodeableConcept $role = null;
    protected ?FHIRCodeableConcept $specialty = null;

    public function getSequence(): ?FHIRPositiveInt
    {
        return $this->sequence;
    }

    public function setSequence(int|FHIRPositiveInt|null $value): self
    {
        $this->sequence = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getProvider(): ?FHIRReference
    {
        return $this->provider;
    }

    public function setProvider(?FHIRReference $value): self
    {
        $this->provider = $value;

        return $this;
    }

    public function getResponsible(): ?FHIRBoolean
    {
        return $this->responsible;
    }

    public function setResponsible(bool|FHIRBoolean|null $value): self
    {
        $this->responsible = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getRole(): ?FHIRCodeableConcept
    {
        return $this->role;
    }

    public function setRole(?FHIRCodeableConcept $value): self
    {
        $this->role = $value;

        return $this;
    }

    public function getSpecialty(): ?FHIRCodeableConcept
    {
        return $this->specialty;
    }

    public function setSpecialty(?FHIRCodeableConcept $value): self
    {
        $this->specialty = $value;

        return $this;
    }
}
