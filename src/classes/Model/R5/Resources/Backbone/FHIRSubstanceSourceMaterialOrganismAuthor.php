<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceSourceMaterialOrganismAuthor Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceSourceMaterialOrganismAuthorInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRSubstanceSourceMaterialOrganismAuthor extends FHIRBackboneElement implements FHIRSubstanceSourceMaterialOrganismAuthorInterface
{
    public const RESOURCE_NAME = 'SubstanceSourceMaterial.organism.author';

    protected ?FHIRCodeableConcept $authorType = null;
    protected ?FHIRString $authorDescription = null;

    public function getAuthorType(): ?FHIRCodeableConcept
    {
        return $this->authorType;
    }

    public function setAuthorType(?FHIRCodeableConcept $value): self
    {
        $this->authorType = $value;

        return $this;
    }

    public function getAuthorDescription(): ?FHIRString
    {
        return $this->authorDescription;
    }

    public function setAuthorDescription(string|FHIRString|null $value): self
    {
        $this->authorDescription = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
