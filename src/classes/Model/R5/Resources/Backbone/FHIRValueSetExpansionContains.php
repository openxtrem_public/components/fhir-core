<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ValueSetExpansionContains Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRValueSetExpansionContainsInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;

class FHIRValueSetExpansionContains extends FHIRBackboneElement implements FHIRValueSetExpansionContainsInterface
{
    public const RESOURCE_NAME = 'ValueSet.expansion.contains';

    protected ?FHIRUri $system = null;
    protected ?FHIRBoolean $abstract = null;
    protected ?FHIRBoolean $inactive = null;
    protected ?FHIRString $version = null;
    protected ?FHIRCode $code = null;
    protected ?FHIRString $display = null;

    /** @var FHIRValueSetComposeIncludeConceptDesignation[] */
    protected array $designation = [];

    /** @var FHIRValueSetExpansionContainsProperty[] */
    protected array $property = [];

    /** @var FHIRValueSetExpansionContains[] */
    protected array $contains = [];

    public function getSystem(): ?FHIRUri
    {
        return $this->system;
    }

    public function setSystem(string|FHIRUri|null $value): self
    {
        $this->system = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getAbstract(): ?FHIRBoolean
    {
        return $this->abstract;
    }

    public function setAbstract(bool|FHIRBoolean|null $value): self
    {
        $this->abstract = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getInactive(): ?FHIRBoolean
    {
        return $this->inactive;
    }

    public function setInactive(bool|FHIRBoolean|null $value): self
    {
        $this->inactive = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDisplay(): ?FHIRString
    {
        return $this->display;
    }

    public function setDisplay(string|FHIRString|null $value): self
    {
        $this->display = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRValueSetComposeIncludeConceptDesignation[]
     */
    public function getDesignation(): array
    {
        return $this->designation;
    }

    public function setDesignation(?FHIRValueSetComposeIncludeConceptDesignation ...$value): self
    {
        $this->designation = array_filter($value);

        return $this;
    }

    public function addDesignation(?FHIRValueSetComposeIncludeConceptDesignation ...$value): self
    {
        $this->designation = array_filter(array_merge($this->designation, $value));

        return $this;
    }

    /**
     * @return FHIRValueSetExpansionContainsProperty[]
     */
    public function getProperty(): array
    {
        return $this->property;
    }

    public function setProperty(?FHIRValueSetExpansionContainsProperty ...$value): self
    {
        $this->property = array_filter($value);

        return $this;
    }

    public function addProperty(?FHIRValueSetExpansionContainsProperty ...$value): self
    {
        $this->property = array_filter(array_merge($this->property, $value));

        return $this;
    }

    /**
     * @return FHIRValueSetExpansionContains[]
     */
    public function getContains(): array
    {
        return $this->contains;
    }

    public function setContains(?FHIRValueSetExpansionContains ...$value): self
    {
        $this->contains = array_filter($value);

        return $this;
    }

    public function addContains(?FHIRValueSetExpansionContains ...$value): self
    {
        $this->contains = array_filter(array_merge($this->contains, $value));

        return $this;
    }
}
