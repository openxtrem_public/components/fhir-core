<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductDefinitionNameUsage Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductDefinitionNameUsageInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;

class FHIRMedicinalProductDefinitionNameUsage extends FHIRBackboneElement implements FHIRMedicinalProductDefinitionNameUsageInterface
{
    public const RESOURCE_NAME = 'MedicinalProductDefinition.name.usage';

    protected ?FHIRCodeableConcept $country = null;
    protected ?FHIRCodeableConcept $jurisdiction = null;
    protected ?FHIRCodeableConcept $language = null;

    public function getCountry(): ?FHIRCodeableConcept
    {
        return $this->country;
    }

    public function setCountry(?FHIRCodeableConcept $value): self
    {
        $this->country = $value;

        return $this;
    }

    public function getJurisdiction(): ?FHIRCodeableConcept
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(?FHIRCodeableConcept $value): self
    {
        $this->jurisdiction = $value;

        return $this;
    }

    public function getLanguage(): ?FHIRCodeableConcept
    {
        return $this->language;
    }

    public function setLanguage(?FHIRCodeableConcept $value): self
    {
        $this->language = $value;

        return $this;
    }
}
