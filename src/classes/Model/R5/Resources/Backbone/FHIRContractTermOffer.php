<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ContractTermOffer Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRContractTermOfferInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUnsignedInt;

class FHIRContractTermOffer extends FHIRBackboneElement implements FHIRContractTermOfferInterface
{
    public const RESOURCE_NAME = 'Contract.term.offer';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRContractTermOfferParty[] */
    protected array $party = [];
    protected ?FHIRReference $topic = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableConcept $decision = null;

    /** @var FHIRCodeableConcept[] */
    protected array $decisionMode = [];

    /** @var FHIRContractTermOfferAnswer[] */
    protected array $answer = [];
    protected ?FHIRString $text = null;

    /** @var FHIRString[] */
    protected array $linkId = [];

    /** @var FHIRUnsignedInt[] */
    protected array $securityLabelNumber = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRContractTermOfferParty[]
     */
    public function getParty(): array
    {
        return $this->party;
    }

    public function setParty(?FHIRContractTermOfferParty ...$value): self
    {
        $this->party = array_filter($value);

        return $this;
    }

    public function addParty(?FHIRContractTermOfferParty ...$value): self
    {
        $this->party = array_filter(array_merge($this->party, $value));

        return $this;
    }

    public function getTopic(): ?FHIRReference
    {
        return $this->topic;
    }

    public function setTopic(?FHIRReference $value): self
    {
        $this->topic = $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getDecision(): ?FHIRCodeableConcept
    {
        return $this->decision;
    }

    public function setDecision(?FHIRCodeableConcept $value): self
    {
        $this->decision = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getDecisionMode(): array
    {
        return $this->decisionMode;
    }

    public function setDecisionMode(?FHIRCodeableConcept ...$value): self
    {
        $this->decisionMode = array_filter($value);

        return $this;
    }

    public function addDecisionMode(?FHIRCodeableConcept ...$value): self
    {
        $this->decisionMode = array_filter(array_merge($this->decisionMode, $value));

        return $this;
    }

    /**
     * @return FHIRContractTermOfferAnswer[]
     */
    public function getAnswer(): array
    {
        return $this->answer;
    }

    public function setAnswer(?FHIRContractTermOfferAnswer ...$value): self
    {
        $this->answer = array_filter($value);

        return $this;
    }

    public function addAnswer(?FHIRContractTermOfferAnswer ...$value): self
    {
        $this->answer = array_filter(array_merge($this->answer, $value));

        return $this;
    }

    public function getText(): ?FHIRString
    {
        return $this->text;
    }

    public function setText(string|FHIRString|null $value): self
    {
        $this->text = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getLinkId(): array
    {
        return $this->linkId;
    }

    public function setLinkId(string|FHIRString|null ...$value): self
    {
        $this->linkId = [];
        $this->addLinkId(...$value);

        return $this;
    }

    public function addLinkId(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->linkId = array_filter(array_merge($this->linkId, $values));

        return $this;
    }

    /**
     * @return FHIRUnsignedInt[]
     */
    public function getSecurityLabelNumber(): array
    {
        return $this->securityLabelNumber;
    }

    public function setSecurityLabelNumber(int|FHIRUnsignedInt|null ...$value): self
    {
        $this->securityLabelNumber = [];
        $this->addSecurityLabelNumber(...$value);

        return $this;
    }

    public function addSecurityLabelNumber(int|FHIRUnsignedInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRUnsignedInt())->setValue($v) : $v, $value);

        $this->securityLabelNumber = array_filter(array_merge($this->securityLabelNumber, $values));

        return $this;
    }
}
