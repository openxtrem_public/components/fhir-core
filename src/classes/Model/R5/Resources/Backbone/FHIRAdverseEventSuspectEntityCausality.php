<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AdverseEventSuspectEntityCausality Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAdverseEventSuspectEntityCausalityInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRAdverseEventSuspectEntityCausality extends FHIRBackboneElement implements FHIRAdverseEventSuspectEntityCausalityInterface
{
    public const RESOURCE_NAME = 'AdverseEvent.suspectEntity.causality';

    protected ?FHIRCodeableConcept $assessmentMethod = null;
    protected ?FHIRCodeableConcept $entityRelatedness = null;
    protected ?FHIRReference $author = null;

    public function getAssessmentMethod(): ?FHIRCodeableConcept
    {
        return $this->assessmentMethod;
    }

    public function setAssessmentMethod(?FHIRCodeableConcept $value): self
    {
        $this->assessmentMethod = $value;

        return $this;
    }

    public function getEntityRelatedness(): ?FHIRCodeableConcept
    {
        return $this->entityRelatedness;
    }

    public function setEntityRelatedness(?FHIRCodeableConcept $value): self
    {
        $this->entityRelatedness = $value;

        return $this;
    }

    public function getAuthor(): ?FHIRReference
    {
        return $this->author;
    }

    public function setAuthor(?FHIRReference $value): self
    {
        $this->author = $value;

        return $this;
    }
}
