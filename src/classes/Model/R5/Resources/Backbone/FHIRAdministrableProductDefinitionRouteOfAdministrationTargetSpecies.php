<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AdministrableProductDefinitionRouteOfAdministrationTargetSpecies Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAdministrableProductDefinitionRouteOfAdministrationTargetSpeciesInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;

class FHIRAdministrableProductDefinitionRouteOfAdministrationTargetSpecies extends FHIRBackboneElement implements FHIRAdministrableProductDefinitionRouteOfAdministrationTargetSpeciesInterface
{
    public const RESOURCE_NAME = 'AdministrableProductDefinition.routeOfAdministration.targetSpecies';

    protected ?FHIRCodeableConcept $code = null;

    /** @var FHIRAdministrableProductDefinitionRouteOfAdministrationTargetSpeciesWithdrawalPeriod[] */
    protected array $withdrawalPeriod = [];

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * @return FHIRAdministrableProductDefinitionRouteOfAdministrationTargetSpeciesWithdrawalPeriod[]
     */
    public function getWithdrawalPeriod(): array
    {
        return $this->withdrawalPeriod;
    }

    public function setWithdrawalPeriod(
        ?FHIRAdministrableProductDefinitionRouteOfAdministrationTargetSpeciesWithdrawalPeriod ...$value,
    ): self
    {
        $this->withdrawalPeriod = array_filter($value);

        return $this;
    }

    public function addWithdrawalPeriod(
        ?FHIRAdministrableProductDefinitionRouteOfAdministrationTargetSpeciesWithdrawalPeriod ...$value,
    ): self
    {
        $this->withdrawalPeriod = array_filter(array_merge($this->withdrawalPeriod, $value));

        return $this;
    }
}
