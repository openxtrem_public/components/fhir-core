<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR BiologicallyDerivedProductCollection Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRBiologicallyDerivedProductCollectionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;

class FHIRBiologicallyDerivedProductCollection extends FHIRBackboneElement implements FHIRBiologicallyDerivedProductCollectionInterface
{
    public const RESOURCE_NAME = 'BiologicallyDerivedProduct.collection';

    protected ?FHIRReference $collector = null;
    protected ?FHIRReference $source = null;
    protected FHIRDateTime|FHIRPeriod|null $collected = null;

    public function getCollector(): ?FHIRReference
    {
        return $this->collector;
    }

    public function setCollector(?FHIRReference $value): self
    {
        $this->collector = $value;

        return $this;
    }

    public function getSource(): ?FHIRReference
    {
        return $this->source;
    }

    public function setSource(?FHIRReference $value): self
    {
        $this->source = $value;

        return $this;
    }

    public function getCollected(): FHIRDateTime|FHIRPeriod|null
    {
        return $this->collected;
    }

    public function setCollected(FHIRDateTime|FHIRPeriod|null $value): self
    {
        $this->collected = $value;

        return $this;
    }
}
