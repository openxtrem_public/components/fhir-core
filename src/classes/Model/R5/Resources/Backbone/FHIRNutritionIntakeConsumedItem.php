<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR NutritionIntakeConsumedItem Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRNutritionIntakeConsumedItemInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;

class FHIRNutritionIntakeConsumedItem extends FHIRBackboneElement implements FHIRNutritionIntakeConsumedItemInterface
{
    public const RESOURCE_NAME = 'NutritionIntake.consumedItem';

    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableReference $nutritionProduct = null;
    protected ?FHIRTiming $schedule = null;
    protected ?FHIRQuantity $amount = null;
    protected ?FHIRQuantity $rate = null;
    protected ?FHIRBoolean $notConsumed = null;
    protected ?FHIRCodeableConcept $notConsumedReason = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getNutritionProduct(): ?FHIRCodeableReference
    {
        return $this->nutritionProduct;
    }

    public function setNutritionProduct(?FHIRCodeableReference $value): self
    {
        $this->nutritionProduct = $value;

        return $this;
    }

    public function getSchedule(): ?FHIRTiming
    {
        return $this->schedule;
    }

    public function setSchedule(?FHIRTiming $value): self
    {
        $this->schedule = $value;

        return $this;
    }

    public function getAmount(): ?FHIRQuantity
    {
        return $this->amount;
    }

    public function setAmount(?FHIRQuantity $value): self
    {
        $this->amount = $value;

        return $this;
    }

    public function getRate(): ?FHIRQuantity
    {
        return $this->rate;
    }

    public function setRate(?FHIRQuantity $value): self
    {
        $this->rate = $value;

        return $this;
    }

    public function getNotConsumed(): ?FHIRBoolean
    {
        return $this->notConsumed;
    }

    public function setNotConsumed(bool|FHIRBoolean|null $value): self
    {
        $this->notConsumed = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getNotConsumedReason(): ?FHIRCodeableConcept
    {
        return $this->notConsumedReason;
    }

    public function setNotConsumedReason(?FHIRCodeableConcept $value): self
    {
        $this->notConsumedReason = $value;

        return $this;
    }
}
