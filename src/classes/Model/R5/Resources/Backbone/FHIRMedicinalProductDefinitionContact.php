<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductDefinitionContact Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductDefinitionContactInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRMedicinalProductDefinitionContact extends FHIRBackboneElement implements FHIRMedicinalProductDefinitionContactInterface
{
    public const RESOURCE_NAME = 'MedicinalProductDefinition.contact';

    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRReference $contact = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getContact(): ?FHIRReference
    {
        return $this->contact;
    }

    public function setContact(?FHIRReference $value): self
    {
        $this->contact = $value;

        return $this;
    }
}
