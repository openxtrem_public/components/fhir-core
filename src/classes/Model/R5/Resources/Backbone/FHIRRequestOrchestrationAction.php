<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR RequestOrchestrationAction Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRRequestOrchestrationActionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAge;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRelatedArtifact;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;

class FHIRRequestOrchestrationAction extends FHIRBackboneElement implements FHIRRequestOrchestrationActionInterface
{
    public const RESOURCE_NAME = 'RequestOrchestration.action';

    protected ?FHIRString $linkId = null;
    protected ?FHIRString $prefix = null;
    protected ?FHIRString $title = null;
    protected ?FHIRMarkdown $description = null;
    protected ?FHIRMarkdown $textEquivalent = null;
    protected ?FHIRCode $priority = null;

    /** @var FHIRCodeableConcept[] */
    protected array $code = [];

    /** @var FHIRRelatedArtifact[] */
    protected array $documentation = [];

    /** @var FHIRReference[] */
    protected array $goal = [];

    /** @var FHIRRequestOrchestrationActionCondition[] */
    protected array $condition = [];

    /** @var FHIRRequestOrchestrationActionInput[] */
    protected array $input = [];

    /** @var FHIRRequestOrchestrationActionOutput[] */
    protected array $output = [];

    /** @var FHIRRequestOrchestrationActionRelatedAction[] */
    protected array $relatedAction = [];
    protected FHIRDateTime|FHIRAge|FHIRPeriod|FHIRDuration|FHIRRange|FHIRTiming|null $timing = null;
    protected ?FHIRCodeableReference $location = null;

    /** @var FHIRRequestOrchestrationActionParticipant[] */
    protected array $participant = [];
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCode $groupingBehavior = null;
    protected ?FHIRCode $selectionBehavior = null;
    protected ?FHIRCode $requiredBehavior = null;
    protected ?FHIRCode $precheckBehavior = null;
    protected ?FHIRCode $cardinalityBehavior = null;
    protected ?FHIRReference $resource = null;
    protected FHIRCanonical|FHIRUri|null $definition = null;
    protected ?FHIRCanonical $transform = null;

    /** @var FHIRRequestOrchestrationActionDynamicValue[] */
    protected array $dynamicValue = [];

    /** @var FHIRRequestOrchestrationAction[] */
    protected array $action = [];

    public function getLinkId(): ?FHIRString
    {
        return $this->linkId;
    }

    public function setLinkId(string|FHIRString|null $value): self
    {
        $this->linkId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPrefix(): ?FHIRString
    {
        return $this->prefix;
    }

    public function setPrefix(string|FHIRString|null $value): self
    {
        $this->prefix = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getTextEquivalent(): ?FHIRMarkdown
    {
        return $this->textEquivalent;
    }

    public function setTextEquivalent(string|FHIRMarkdown|null $value): self
    {
        $this->textEquivalent = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getPriority(): ?FHIRCode
    {
        return $this->priority;
    }

    public function setPriority(string|FHIRCode|null $value): self
    {
        $this->priority = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCode(): array
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept ...$value): self
    {
        $this->code = array_filter($value);

        return $this;
    }

    public function addCode(?FHIRCodeableConcept ...$value): self
    {
        $this->code = array_filter(array_merge($this->code, $value));

        return $this;
    }

    /**
     * @return FHIRRelatedArtifact[]
     */
    public function getDocumentation(): array
    {
        return $this->documentation;
    }

    public function setDocumentation(?FHIRRelatedArtifact ...$value): self
    {
        $this->documentation = array_filter($value);

        return $this;
    }

    public function addDocumentation(?FHIRRelatedArtifact ...$value): self
    {
        $this->documentation = array_filter(array_merge($this->documentation, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getGoal(): array
    {
        return $this->goal;
    }

    public function setGoal(?FHIRReference ...$value): self
    {
        $this->goal = array_filter($value);

        return $this;
    }

    public function addGoal(?FHIRReference ...$value): self
    {
        $this->goal = array_filter(array_merge($this->goal, $value));

        return $this;
    }

    /**
     * @return FHIRRequestOrchestrationActionCondition[]
     */
    public function getCondition(): array
    {
        return $this->condition;
    }

    public function setCondition(?FHIRRequestOrchestrationActionCondition ...$value): self
    {
        $this->condition = array_filter($value);

        return $this;
    }

    public function addCondition(?FHIRRequestOrchestrationActionCondition ...$value): self
    {
        $this->condition = array_filter(array_merge($this->condition, $value));

        return $this;
    }

    /**
     * @return FHIRRequestOrchestrationActionInput[]
     */
    public function getInput(): array
    {
        return $this->input;
    }

    public function setInput(?FHIRRequestOrchestrationActionInput ...$value): self
    {
        $this->input = array_filter($value);

        return $this;
    }

    public function addInput(?FHIRRequestOrchestrationActionInput ...$value): self
    {
        $this->input = array_filter(array_merge($this->input, $value));

        return $this;
    }

    /**
     * @return FHIRRequestOrchestrationActionOutput[]
     */
    public function getOutput(): array
    {
        return $this->output;
    }

    public function setOutput(?FHIRRequestOrchestrationActionOutput ...$value): self
    {
        $this->output = array_filter($value);

        return $this;
    }

    public function addOutput(?FHIRRequestOrchestrationActionOutput ...$value): self
    {
        $this->output = array_filter(array_merge($this->output, $value));

        return $this;
    }

    /**
     * @return FHIRRequestOrchestrationActionRelatedAction[]
     */
    public function getRelatedAction(): array
    {
        return $this->relatedAction;
    }

    public function setRelatedAction(?FHIRRequestOrchestrationActionRelatedAction ...$value): self
    {
        $this->relatedAction = array_filter($value);

        return $this;
    }

    public function addRelatedAction(?FHIRRequestOrchestrationActionRelatedAction ...$value): self
    {
        $this->relatedAction = array_filter(array_merge($this->relatedAction, $value));

        return $this;
    }

    public function getTiming(): FHIRDateTime|FHIRAge|FHIRPeriod|FHIRDuration|FHIRRange|FHIRTiming|null
    {
        return $this->timing;
    }

    public function setTiming(FHIRDateTime|FHIRAge|FHIRPeriod|FHIRDuration|FHIRRange|FHIRTiming|null $value): self
    {
        $this->timing = $value;

        return $this;
    }

    public function getLocation(): ?FHIRCodeableReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRCodeableReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    /**
     * @return FHIRRequestOrchestrationActionParticipant[]
     */
    public function getParticipant(): array
    {
        return $this->participant;
    }

    public function setParticipant(?FHIRRequestOrchestrationActionParticipant ...$value): self
    {
        $this->participant = array_filter($value);

        return $this;
    }

    public function addParticipant(?FHIRRequestOrchestrationActionParticipant ...$value): self
    {
        $this->participant = array_filter(array_merge($this->participant, $value));

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getGroupingBehavior(): ?FHIRCode
    {
        return $this->groupingBehavior;
    }

    public function setGroupingBehavior(string|FHIRCode|null $value): self
    {
        $this->groupingBehavior = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getSelectionBehavior(): ?FHIRCode
    {
        return $this->selectionBehavior;
    }

    public function setSelectionBehavior(string|FHIRCode|null $value): self
    {
        $this->selectionBehavior = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getRequiredBehavior(): ?FHIRCode
    {
        return $this->requiredBehavior;
    }

    public function setRequiredBehavior(string|FHIRCode|null $value): self
    {
        $this->requiredBehavior = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getPrecheckBehavior(): ?FHIRCode
    {
        return $this->precheckBehavior;
    }

    public function setPrecheckBehavior(string|FHIRCode|null $value): self
    {
        $this->precheckBehavior = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCardinalityBehavior(): ?FHIRCode
    {
        return $this->cardinalityBehavior;
    }

    public function setCardinalityBehavior(string|FHIRCode|null $value): self
    {
        $this->cardinalityBehavior = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getResource(): ?FHIRReference
    {
        return $this->resource;
    }

    public function setResource(?FHIRReference $value): self
    {
        $this->resource = $value;

        return $this;
    }

    public function getDefinition(): FHIRCanonical|FHIRUri|null
    {
        return $this->definition;
    }

    public function setDefinition(FHIRCanonical|FHIRUri|null $value): self
    {
        $this->definition = $value;

        return $this;
    }

    public function getTransform(): ?FHIRCanonical
    {
        return $this->transform;
    }

    public function setTransform(string|FHIRCanonical|null $value): self
    {
        $this->transform = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRRequestOrchestrationActionDynamicValue[]
     */
    public function getDynamicValue(): array
    {
        return $this->dynamicValue;
    }

    public function setDynamicValue(?FHIRRequestOrchestrationActionDynamicValue ...$value): self
    {
        $this->dynamicValue = array_filter($value);

        return $this;
    }

    public function addDynamicValue(?FHIRRequestOrchestrationActionDynamicValue ...$value): self
    {
        $this->dynamicValue = array_filter(array_merge($this->dynamicValue, $value));

        return $this;
    }

    /**
     * @return FHIRRequestOrchestrationAction[]
     */
    public function getAction(): array
    {
        return $this->action;
    }

    public function setAction(?FHIRRequestOrchestrationAction ...$value): self
    {
        $this->action = array_filter($value);

        return $this;
    }

    public function addAction(?FHIRRequestOrchestrationAction ...$value): self
    {
        $this->action = array_filter(array_merge($this->action, $value));

        return $this;
    }
}
