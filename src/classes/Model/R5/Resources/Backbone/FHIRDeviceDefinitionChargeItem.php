<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceDefinitionChargeItem Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceDefinitionChargeItemInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRUsageContext;

class FHIRDeviceDefinitionChargeItem extends FHIRBackboneElement implements FHIRDeviceDefinitionChargeItemInterface
{
    public const RESOURCE_NAME = 'DeviceDefinition.chargeItem';

    protected ?FHIRCodeableReference $chargeItemCode = null;
    protected ?FHIRQuantity $count = null;
    protected ?FHIRPeriod $effectivePeriod = null;

    /** @var FHIRUsageContext[] */
    protected array $useContext = [];

    public function getChargeItemCode(): ?FHIRCodeableReference
    {
        return $this->chargeItemCode;
    }

    public function setChargeItemCode(?FHIRCodeableReference $value): self
    {
        $this->chargeItemCode = $value;

        return $this;
    }

    public function getCount(): ?FHIRQuantity
    {
        return $this->count;
    }

    public function setCount(?FHIRQuantity $value): self
    {
        $this->count = $value;

        return $this;
    }

    public function getEffectivePeriod(): ?FHIRPeriod
    {
        return $this->effectivePeriod;
    }

    public function setEffectivePeriod(?FHIRPeriod $value): self
    {
        $this->effectivePeriod = $value;

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUseContext(): array
    {
        return $this->useContext;
    }

    public function setUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter($value);

        return $this;
    }

    public function addUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter(array_merge($this->useContext, $value));

        return $this;
    }
}
