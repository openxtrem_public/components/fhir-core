<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CoveragePaymentBy Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCoveragePaymentByInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRCoveragePaymentBy extends FHIRBackboneElement implements FHIRCoveragePaymentByInterface
{
    public const RESOURCE_NAME = 'Coverage.paymentBy';

    protected ?FHIRReference $party = null;
    protected ?FHIRString $responsibility = null;

    public function getParty(): ?FHIRReference
    {
        return $this->party;
    }

    public function setParty(?FHIRReference $value): self
    {
        $this->party = $value;

        return $this;
    }

    public function getResponsibility(): ?FHIRString
    {
        return $this->responsibility;
    }

    public function setResponsibility(string|FHIRString|null $value): self
    {
        $this->responsibility = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
