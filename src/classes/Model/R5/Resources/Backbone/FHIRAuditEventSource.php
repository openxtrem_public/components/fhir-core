<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AuditEventSource Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAuditEventSourceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRAuditEventSource extends FHIRBackboneElement implements FHIRAuditEventSourceInterface
{
    public const RESOURCE_NAME = 'AuditEvent.source';

    protected ?FHIRReference $site = null;
    protected ?FHIRReference $observer = null;

    /** @var FHIRCodeableConcept[] */
    protected array $type = [];

    public function getSite(): ?FHIRReference
    {
        return $this->site;
    }

    public function setSite(?FHIRReference $value): self
    {
        $this->site = $value;

        return $this;
    }

    public function getObserver(): ?FHIRReference
    {
        return $this->observer;
    }

    public function setObserver(?FHIRReference $value): self
    {
        $this->observer = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }
}
