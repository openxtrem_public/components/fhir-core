<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ConceptMapGroupUnmapped Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRConceptMapGroupUnmappedInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRConceptMapGroupUnmapped extends FHIRBackboneElement implements FHIRConceptMapGroupUnmappedInterface
{
    public const RESOURCE_NAME = 'ConceptMap.group.unmapped';

    protected ?FHIRCode $mode = null;
    protected ?FHIRCode $code = null;
    protected ?FHIRString $display = null;
    protected ?FHIRCanonical $valueSet = null;
    protected ?FHIRCode $relationship = null;
    protected ?FHIRCanonical $otherMap = null;

    public function getMode(): ?FHIRCode
    {
        return $this->mode;
    }

    public function setMode(string|FHIRCode|null $value): self
    {
        $this->mode = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDisplay(): ?FHIRString
    {
        return $this->display;
    }

    public function setDisplay(string|FHIRString|null $value): self
    {
        $this->display = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getValueSet(): ?FHIRCanonical
    {
        return $this->valueSet;
    }

    public function setValueSet(string|FHIRCanonical|null $value): self
    {
        $this->valueSet = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    public function getRelationship(): ?FHIRCode
    {
        return $this->relationship;
    }

    public function setRelationship(string|FHIRCode|null $value): self
    {
        $this->relationship = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getOtherMap(): ?FHIRCanonical
    {
        return $this->otherMap;
    }

    public function setOtherMap(string|FHIRCanonical|null $value): self
    {
        $this->otherMap = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }
}
