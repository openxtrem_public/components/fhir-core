<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR InventoryReportInventoryListing Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRInventoryReportInventoryListingInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;

class FHIRInventoryReportInventoryListing extends FHIRBackboneElement implements FHIRInventoryReportInventoryListingInterface
{
    public const RESOURCE_NAME = 'InventoryReport.inventoryListing';

    protected ?FHIRReference $location = null;
    protected ?FHIRCodeableConcept $itemStatus = null;
    protected ?FHIRDateTime $countingDateTime = null;

    /** @var FHIRInventoryReportInventoryListingItem[] */
    protected array $item = [];

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    public function getItemStatus(): ?FHIRCodeableConcept
    {
        return $this->itemStatus;
    }

    public function setItemStatus(?FHIRCodeableConcept $value): self
    {
        $this->itemStatus = $value;

        return $this;
    }

    public function getCountingDateTime(): ?FHIRDateTime
    {
        return $this->countingDateTime;
    }

    public function setCountingDateTime(string|FHIRDateTime|null $value): self
    {
        $this->countingDateTime = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRInventoryReportInventoryListingItem[]
     */
    public function getItem(): array
    {
        return $this->item;
    }

    public function setItem(?FHIRInventoryReportInventoryListingItem ...$value): self
    {
        $this->item = array_filter($value);

        return $this;
    }

    public function addItem(?FHIRInventoryReportInventoryListingItem ...$value): self
    {
        $this->item = array_filter(array_merge($this->item, $value));

        return $this;
    }
}
