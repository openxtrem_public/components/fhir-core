<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CoverageEligibilityRequestInsurance Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCoverageEligibilityRequestInsuranceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRCoverageEligibilityRequestInsurance extends FHIRBackboneElement implements FHIRCoverageEligibilityRequestInsuranceInterface
{
    public const RESOURCE_NAME = 'CoverageEligibilityRequest.insurance';

    protected ?FHIRBoolean $focal = null;
    protected ?FHIRReference $coverage = null;
    protected ?FHIRString $businessArrangement = null;

    public function getFocal(): ?FHIRBoolean
    {
        return $this->focal;
    }

    public function setFocal(bool|FHIRBoolean|null $value): self
    {
        $this->focal = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getCoverage(): ?FHIRReference
    {
        return $this->coverage;
    }

    public function setCoverage(?FHIRReference $value): self
    {
        $this->coverage = $value;

        return $this;
    }

    public function getBusinessArrangement(): ?FHIRString
    {
        return $this->businessArrangement;
    }

    public function setBusinessArrangement(string|FHIRString|null $value): self
    {
        $this->businessArrangement = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
