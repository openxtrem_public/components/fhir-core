<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImplementationGuideDefinition Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImplementationGuideDefinitionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;

class FHIRImplementationGuideDefinition extends FHIRBackboneElement implements FHIRImplementationGuideDefinitionInterface
{
    public const RESOURCE_NAME = 'ImplementationGuide.definition';

    /** @var FHIRImplementationGuideDefinitionGrouping[] */
    protected array $grouping = [];

    /** @var FHIRImplementationGuideDefinitionResource[] */
    protected array $resource = [];
    protected ?FHIRImplementationGuideDefinitionPage $page = null;

    /** @var FHIRImplementationGuideDefinitionParameter[] */
    protected array $parameter = [];

    /** @var FHIRImplementationGuideDefinitionTemplate[] */
    protected array $template = [];

    /**
     * @return FHIRImplementationGuideDefinitionGrouping[]
     */
    public function getGrouping(): array
    {
        return $this->grouping;
    }

    public function setGrouping(?FHIRImplementationGuideDefinitionGrouping ...$value): self
    {
        $this->grouping = array_filter($value);

        return $this;
    }

    public function addGrouping(?FHIRImplementationGuideDefinitionGrouping ...$value): self
    {
        $this->grouping = array_filter(array_merge($this->grouping, $value));

        return $this;
    }

    /**
     * @return FHIRImplementationGuideDefinitionResource[]
     */
    public function getResource(): array
    {
        return $this->resource;
    }

    public function setResource(?FHIRImplementationGuideDefinitionResource ...$value): self
    {
        $this->resource = array_filter($value);

        return $this;
    }

    public function addResource(?FHIRImplementationGuideDefinitionResource ...$value): self
    {
        $this->resource = array_filter(array_merge($this->resource, $value));

        return $this;
    }

    public function getPage(): ?FHIRImplementationGuideDefinitionPage
    {
        return $this->page;
    }

    public function setPage(?FHIRImplementationGuideDefinitionPage $value): self
    {
        $this->page = $value;

        return $this;
    }

    /**
     * @return FHIRImplementationGuideDefinitionParameter[]
     */
    public function getParameter(): array
    {
        return $this->parameter;
    }

    public function setParameter(?FHIRImplementationGuideDefinitionParameter ...$value): self
    {
        $this->parameter = array_filter($value);

        return $this;
    }

    public function addParameter(?FHIRImplementationGuideDefinitionParameter ...$value): self
    {
        $this->parameter = array_filter(array_merge($this->parameter, $value));

        return $this;
    }

    /**
     * @return FHIRImplementationGuideDefinitionTemplate[]
     */
    public function getTemplate(): array
    {
        return $this->template;
    }

    public function setTemplate(?FHIRImplementationGuideDefinitionTemplate ...$value): self
    {
        $this->template = array_filter($value);

        return $this;
    }

    public function addTemplate(?FHIRImplementationGuideDefinitionTemplate ...$value): self
    {
        $this->template = array_filter(array_merge($this->template, $value));

        return $this;
    }
}
