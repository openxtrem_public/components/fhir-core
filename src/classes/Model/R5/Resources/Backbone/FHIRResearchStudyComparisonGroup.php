<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ResearchStudyComparisonGroup Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRResearchStudyComparisonGroupInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRResearchStudyComparisonGroup extends FHIRBackboneElement implements FHIRResearchStudyComparisonGroupInterface
{
    public const RESOURCE_NAME = 'ResearchStudy.comparisonGroup';

    protected ?FHIRId $linkId = null;
    protected ?FHIRString $name = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRReference[] */
    protected array $intendedExposure = [];
    protected ?FHIRReference $observedGroup = null;

    public function getLinkId(): ?FHIRId
    {
        return $this->linkId;
    }

    public function setLinkId(string|FHIRId|null $value): self
    {
        $this->linkId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getIntendedExposure(): array
    {
        return $this->intendedExposure;
    }

    public function setIntendedExposure(?FHIRReference ...$value): self
    {
        $this->intendedExposure = array_filter($value);

        return $this;
    }

    public function addIntendedExposure(?FHIRReference ...$value): self
    {
        $this->intendedExposure = array_filter(array_merge($this->intendedExposure, $value));

        return $this;
    }

    public function getObservedGroup(): ?FHIRReference
    {
        return $this->observedGroup;
    }

    public function setObservedGroup(?FHIRReference $value): self
    {
        $this->observedGroup = $value;

        return $this;
    }
}
