<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceNucleicAcidSubunit Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceNucleicAcidSubunitInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRSubstanceNucleicAcidSubunit extends FHIRBackboneElement implements FHIRSubstanceNucleicAcidSubunitInterface
{
    public const RESOURCE_NAME = 'SubstanceNucleicAcid.subunit';

    protected ?FHIRInteger $subunit = null;
    protected ?FHIRString $sequence = null;
    protected ?FHIRInteger $length = null;
    protected ?FHIRAttachment $sequenceAttachment = null;
    protected ?FHIRCodeableConcept $fivePrime = null;
    protected ?FHIRCodeableConcept $threePrime = null;

    /** @var FHIRSubstanceNucleicAcidSubunitLinkage[] */
    protected array $linkage = [];

    /** @var FHIRSubstanceNucleicAcidSubunitSugar[] */
    protected array $sugar = [];

    public function getSubunit(): ?FHIRInteger
    {
        return $this->subunit;
    }

    public function setSubunit(int|FHIRInteger|null $value): self
    {
        $this->subunit = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getSequence(): ?FHIRString
    {
        return $this->sequence;
    }

    public function setSequence(string|FHIRString|null $value): self
    {
        $this->sequence = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getLength(): ?FHIRInteger
    {
        return $this->length;
    }

    public function setLength(int|FHIRInteger|null $value): self
    {
        $this->length = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getSequenceAttachment(): ?FHIRAttachment
    {
        return $this->sequenceAttachment;
    }

    public function setSequenceAttachment(?FHIRAttachment $value): self
    {
        $this->sequenceAttachment = $value;

        return $this;
    }

    public function getFivePrime(): ?FHIRCodeableConcept
    {
        return $this->fivePrime;
    }

    public function setFivePrime(?FHIRCodeableConcept $value): self
    {
        $this->fivePrime = $value;

        return $this;
    }

    public function getThreePrime(): ?FHIRCodeableConcept
    {
        return $this->threePrime;
    }

    public function setThreePrime(?FHIRCodeableConcept $value): self
    {
        $this->threePrime = $value;

        return $this;
    }

    /**
     * @return FHIRSubstanceNucleicAcidSubunitLinkage[]
     */
    public function getLinkage(): array
    {
        return $this->linkage;
    }

    public function setLinkage(?FHIRSubstanceNucleicAcidSubunitLinkage ...$value): self
    {
        $this->linkage = array_filter($value);

        return $this;
    }

    public function addLinkage(?FHIRSubstanceNucleicAcidSubunitLinkage ...$value): self
    {
        $this->linkage = array_filter(array_merge($this->linkage, $value));

        return $this;
    }

    /**
     * @return FHIRSubstanceNucleicAcidSubunitSugar[]
     */
    public function getSugar(): array
    {
        return $this->sugar;
    }

    public function setSugar(?FHIRSubstanceNucleicAcidSubunitSugar ...$value): self
    {
        $this->sugar = array_filter($value);

        return $this;
    }

    public function addSugar(?FHIRSubstanceNucleicAcidSubunitSugar ...$value): self
    {
        $this->sugar = array_filter(array_merge($this->sugar, $value));

        return $this;
    }
}
