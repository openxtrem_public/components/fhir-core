<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstancePolymerMonomerSet Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstancePolymerMonomerSetInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;

class FHIRSubstancePolymerMonomerSet extends FHIRBackboneElement implements FHIRSubstancePolymerMonomerSetInterface
{
    public const RESOURCE_NAME = 'SubstancePolymer.monomerSet';

    protected ?FHIRCodeableConcept $ratioType = null;

    /** @var FHIRSubstancePolymerMonomerSetStartingMaterial[] */
    protected array $startingMaterial = [];

    public function getRatioType(): ?FHIRCodeableConcept
    {
        return $this->ratioType;
    }

    public function setRatioType(?FHIRCodeableConcept $value): self
    {
        $this->ratioType = $value;

        return $this;
    }

    /**
     * @return FHIRSubstancePolymerMonomerSetStartingMaterial[]
     */
    public function getStartingMaterial(): array
    {
        return $this->startingMaterial;
    }

    public function setStartingMaterial(?FHIRSubstancePolymerMonomerSetStartingMaterial ...$value): self
    {
        $this->startingMaterial = array_filter($value);

        return $this;
    }

    public function addStartingMaterial(?FHIRSubstancePolymerMonomerSetStartingMaterial ...$value): self
    {
        $this->startingMaterial = array_filter(array_merge($this->startingMaterial, $value));

        return $this;
    }
}
