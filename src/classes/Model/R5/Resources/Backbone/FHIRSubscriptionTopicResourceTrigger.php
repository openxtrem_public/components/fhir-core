<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubscriptionTopicResourceTrigger Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubscriptionTopicResourceTriggerInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;

class FHIRSubscriptionTopicResourceTrigger extends FHIRBackboneElement implements FHIRSubscriptionTopicResourceTriggerInterface
{
    public const RESOURCE_NAME = 'SubscriptionTopic.resourceTrigger';

    protected ?FHIRMarkdown $description = null;
    protected ?FHIRUri $resource = null;

    /** @var FHIRCode[] */
    protected array $supportedInteraction = [];
    protected ?FHIRSubscriptionTopicResourceTriggerQueryCriteria $queryCriteria = null;
    protected ?FHIRString $fhirPathCriteria = null;

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getResource(): ?FHIRUri
    {
        return $this->resource;
    }

    public function setResource(string|FHIRUri|null $value): self
    {
        $this->resource = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getSupportedInteraction(): array
    {
        return $this->supportedInteraction;
    }

    public function setSupportedInteraction(string|FHIRCode|null ...$value): self
    {
        $this->supportedInteraction = [];
        $this->addSupportedInteraction(...$value);

        return $this;
    }

    public function addSupportedInteraction(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->supportedInteraction = array_filter(array_merge($this->supportedInteraction, $values));

        return $this;
    }

    public function getQueryCriteria(): ?FHIRSubscriptionTopicResourceTriggerQueryCriteria
    {
        return $this->queryCriteria;
    }

    public function setQueryCriteria(?FHIRSubscriptionTopicResourceTriggerQueryCriteria $value): self
    {
        $this->queryCriteria = $value;

        return $this;
    }

    public function getFhirPathCriteria(): ?FHIRString
    {
        return $this->fhirPathCriteria;
    }

    public function setFhirPathCriteria(string|FHIRString|null $value): self
    {
        $this->fhirPathCriteria = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
