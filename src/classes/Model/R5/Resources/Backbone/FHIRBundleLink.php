<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR BundleLink Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRBundleLinkInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;

class FHIRBundleLink extends FHIRBackboneElement implements FHIRBundleLinkInterface
{
    public const RESOURCE_NAME = 'Bundle.link';

    protected ?FHIRCode $relation = null;
    protected ?FHIRUri $url = null;

    public function getRelation(): ?FHIRCode
    {
        return $this->relation;
    }

    public function setRelation(string|FHIRCode|null $value): self
    {
        $this->relation = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }
}
