<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ResearchStudyAssociatedParty Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRResearchStudyAssociatedPartyInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRResearchStudyAssociatedParty extends FHIRBackboneElement implements FHIRResearchStudyAssociatedPartyInterface
{
    public const RESOURCE_NAME = 'ResearchStudy.associatedParty';

    protected ?FHIRString $name = null;
    protected ?FHIRCodeableConcept $role = null;

    /** @var FHIRPeriod[] */
    protected array $period = [];

    /** @var FHIRCodeableConcept[] */
    protected array $classifier = [];
    protected ?FHIRReference $party = null;

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getRole(): ?FHIRCodeableConcept
    {
        return $this->role;
    }

    public function setRole(?FHIRCodeableConcept $value): self
    {
        $this->role = $value;

        return $this;
    }

    /**
     * @return FHIRPeriod[]
     */
    public function getPeriod(): array
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod ...$value): self
    {
        $this->period = array_filter($value);

        return $this;
    }

    public function addPeriod(?FHIRPeriod ...$value): self
    {
        $this->period = array_filter(array_merge($this->period, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getClassifier(): array
    {
        return $this->classifier;
    }

    public function setClassifier(?FHIRCodeableConcept ...$value): self
    {
        $this->classifier = array_filter($value);

        return $this;
    }

    public function addClassifier(?FHIRCodeableConcept ...$value): self
    {
        $this->classifier = array_filter(array_merge($this->classifier, $value));

        return $this;
    }

    public function getParty(): ?FHIRReference
    {
        return $this->party;
    }

    public function setParty(?FHIRReference $value): self
    {
        $this->party = $value;

        return $this;
    }
}
