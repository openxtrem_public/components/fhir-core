<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ManufacturedItemDefinitionProperty Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRManufacturedItemDefinitionPropertyInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;

class FHIRManufacturedItemDefinitionProperty extends FHIRBackboneElement implements FHIRManufacturedItemDefinitionPropertyInterface
{
    public const RESOURCE_NAME = 'ManufacturedItemDefinition.property';

    protected ?FHIRCodeableConcept $type = null;
    protected FHIRCodeableConcept|FHIRQuantity|FHIRDate|FHIRBoolean|FHIRMarkdown|FHIRAttachment|FHIRReference|null $value = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getValue(
    ): FHIRCodeableConcept|FHIRQuantity|FHIRDate|FHIRBoolean|FHIRMarkdown|FHIRAttachment|FHIRReference|null
    {
        return $this->value;
    }

    public function setValue(
        FHIRCodeableConcept|FHIRQuantity|FHIRDate|FHIRBoolean|FHIRMarkdown|FHIRAttachment|FHIRReference|null $value,
    ): self
    {
        $this->value = $value;

        return $this;
    }
}
