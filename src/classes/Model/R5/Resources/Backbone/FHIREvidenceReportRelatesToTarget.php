<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EvidenceReportRelatesToTarget Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREvidenceReportRelatesToTargetInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;

class FHIREvidenceReportRelatesToTarget extends FHIRBackboneElement implements FHIREvidenceReportRelatesToTargetInterface
{
    public const RESOURCE_NAME = 'EvidenceReport.relatesTo.target';

    protected ?FHIRUri $url = null;
    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRMarkdown $display = null;
    protected ?FHIRReference $resource = null;

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getDisplay(): ?FHIRMarkdown
    {
        return $this->display;
    }

    public function setDisplay(string|FHIRMarkdown|null $value): self
    {
        $this->display = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getResource(): ?FHIRReference
    {
        return $this->resource;
    }

    public function setResource(?FHIRReference $value): self
    {
        $this->resource = $value;

        return $this;
    }
}
