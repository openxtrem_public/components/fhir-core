<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClaimDiagnosis Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRClaimDiagnosisInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;

class FHIRClaimDiagnosis extends FHIRBackboneElement implements FHIRClaimDiagnosisInterface
{
    public const RESOURCE_NAME = 'Claim.diagnosis';

    protected ?FHIRPositiveInt $sequence = null;
    protected FHIRCodeableConcept|FHIRReference|null $diagnosis = null;

    /** @var FHIRCodeableConcept[] */
    protected array $type = [];
    protected ?FHIRCodeableConcept $onAdmission = null;

    public function getSequence(): ?FHIRPositiveInt
    {
        return $this->sequence;
    }

    public function setSequence(int|FHIRPositiveInt|null $value): self
    {
        $this->sequence = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getDiagnosis(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->diagnosis;
    }

    public function setDiagnosis(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->diagnosis = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }

    public function getOnAdmission(): ?FHIRCodeableConcept
    {
        return $this->onAdmission;
    }

    public function setOnAdmission(?FHIRCodeableConcept $value): self
    {
        $this->onAdmission = $value;

        return $this;
    }
}
