<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MeasureSupplementalData Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMeasureSupplementalDataInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRExpression;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRMeasureSupplementalData extends FHIRBackboneElement implements FHIRMeasureSupplementalDataInterface
{
    public const RESOURCE_NAME = 'Measure.supplementalData';

    protected ?FHIRString $linkId = null;
    protected ?FHIRCodeableConcept $code = null;

    /** @var FHIRCodeableConcept[] */
    protected array $usage = [];
    protected ?FHIRMarkdown $description = null;
    protected ?FHIRExpression $criteria = null;

    public function getLinkId(): ?FHIRString
    {
        return $this->linkId;
    }

    public function setLinkId(string|FHIRString|null $value): self
    {
        $this->linkId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getUsage(): array
    {
        return $this->usage;
    }

    public function setUsage(?FHIRCodeableConcept ...$value): self
    {
        $this->usage = array_filter($value);

        return $this;
    }

    public function addUsage(?FHIRCodeableConcept ...$value): self
    {
        $this->usage = array_filter(array_merge($this->usage, $value));

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getCriteria(): ?FHIRExpression
    {
        return $this->criteria;
    }

    public function setCriteria(?FHIRExpression $value): self
    {
        $this->criteria = $value;

        return $this;
    }
}
