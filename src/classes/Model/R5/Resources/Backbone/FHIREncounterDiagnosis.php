<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EncounterDiagnosis Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREncounterDiagnosisInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;

class FHIREncounterDiagnosis extends FHIRBackboneElement implements FHIREncounterDiagnosisInterface
{
    public const RESOURCE_NAME = 'Encounter.diagnosis';

    /** @var FHIRCodeableReference[] */
    protected array $condition = [];

    /** @var FHIRCodeableConcept[] */
    protected array $use = [];

    /**
     * @return FHIRCodeableReference[]
     */
    public function getCondition(): array
    {
        return $this->condition;
    }

    public function setCondition(?FHIRCodeableReference ...$value): self
    {
        $this->condition = array_filter($value);

        return $this;
    }

    public function addCondition(?FHIRCodeableReference ...$value): self
    {
        $this->condition = array_filter(array_merge($this->condition, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getUse(): array
    {
        return $this->use;
    }

    public function setUse(?FHIRCodeableConcept ...$value): self
    {
        $this->use = array_filter($value);

        return $this;
    }

    public function addUse(?FHIRCodeableConcept ...$value): self
    {
        $this->use = array_filter(array_merge($this->use, $value));

        return $this;
    }
}
