<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExplanationOfBenefitAddItemDetail Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExplanationOfBenefitAddItemDetailInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;

class FHIRExplanationOfBenefitAddItemDetail extends FHIRBackboneElement implements FHIRExplanationOfBenefitAddItemDetailInterface
{
    public const RESOURCE_NAME = 'ExplanationOfBenefit.addItem.detail';

    /** @var FHIRIdentifier[] */
    protected array $traceNumber = [];
    protected ?FHIRCodeableConcept $revenue = null;
    protected ?FHIRCodeableConcept $productOrService = null;
    protected ?FHIRCodeableConcept $productOrServiceEnd = null;

    /** @var FHIRCodeableConcept[] */
    protected array $modifier = [];
    protected ?FHIRMoney $patientPaid = null;
    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRMoney $unitPrice = null;
    protected ?FHIRDecimal $factor = null;
    protected ?FHIRMoney $tax = null;
    protected ?FHIRMoney $net = null;

    /** @var FHIRPositiveInt[] */
    protected array $noteNumber = [];
    protected ?FHIRExplanationOfBenefitItemReviewOutcome $reviewOutcome = null;

    /** @var FHIRExplanationOfBenefitItemAdjudication[] */
    protected array $adjudication = [];

    /** @var FHIRExplanationOfBenefitAddItemDetailSubDetail[] */
    protected array $subDetail = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getTraceNumber(): array
    {
        return $this->traceNumber;
    }

    public function setTraceNumber(?FHIRIdentifier ...$value): self
    {
        $this->traceNumber = array_filter($value);

        return $this;
    }

    public function addTraceNumber(?FHIRIdentifier ...$value): self
    {
        $this->traceNumber = array_filter(array_merge($this->traceNumber, $value));

        return $this;
    }

    public function getRevenue(): ?FHIRCodeableConcept
    {
        return $this->revenue;
    }

    public function setRevenue(?FHIRCodeableConcept $value): self
    {
        $this->revenue = $value;

        return $this;
    }

    public function getProductOrService(): ?FHIRCodeableConcept
    {
        return $this->productOrService;
    }

    public function setProductOrService(?FHIRCodeableConcept $value): self
    {
        $this->productOrService = $value;

        return $this;
    }

    public function getProductOrServiceEnd(): ?FHIRCodeableConcept
    {
        return $this->productOrServiceEnd;
    }

    public function setProductOrServiceEnd(?FHIRCodeableConcept $value): self
    {
        $this->productOrServiceEnd = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getModifier(): array
    {
        return $this->modifier;
    }

    public function setModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->modifier = array_filter($value);

        return $this;
    }

    public function addModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->modifier = array_filter(array_merge($this->modifier, $value));

        return $this;
    }

    public function getPatientPaid(): ?FHIRMoney
    {
        return $this->patientPaid;
    }

    public function setPatientPaid(?FHIRMoney $value): self
    {
        $this->patientPaid = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getUnitPrice(): ?FHIRMoney
    {
        return $this->unitPrice;
    }

    public function setUnitPrice(?FHIRMoney $value): self
    {
        $this->unitPrice = $value;

        return $this;
    }

    public function getFactor(): ?FHIRDecimal
    {
        return $this->factor;
    }

    public function setFactor(float|FHIRDecimal|null $value): self
    {
        $this->factor = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getTax(): ?FHIRMoney
    {
        return $this->tax;
    }

    public function setTax(?FHIRMoney $value): self
    {
        $this->tax = $value;

        return $this;
    }

    public function getNet(): ?FHIRMoney
    {
        return $this->net;
    }

    public function setNet(?FHIRMoney $value): self
    {
        $this->net = $value;

        return $this;
    }

    /**
     * @return FHIRPositiveInt[]
     */
    public function getNoteNumber(): array
    {
        return $this->noteNumber;
    }

    public function setNoteNumber(int|FHIRPositiveInt|null ...$value): self
    {
        $this->noteNumber = [];
        $this->addNoteNumber(...$value);

        return $this;
    }

    public function addNoteNumber(int|FHIRPositiveInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRPositiveInt())->setValue($v) : $v, $value);

        $this->noteNumber = array_filter(array_merge($this->noteNumber, $values));

        return $this;
    }

    public function getReviewOutcome(): ?FHIRExplanationOfBenefitItemReviewOutcome
    {
        return $this->reviewOutcome;
    }

    public function setReviewOutcome(?FHIRExplanationOfBenefitItemReviewOutcome $value): self
    {
        $this->reviewOutcome = $value;

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitItemAdjudication[]
     */
    public function getAdjudication(): array
    {
        return $this->adjudication;
    }

    public function setAdjudication(?FHIRExplanationOfBenefitItemAdjudication ...$value): self
    {
        $this->adjudication = array_filter($value);

        return $this;
    }

    public function addAdjudication(?FHIRExplanationOfBenefitItemAdjudication ...$value): self
    {
        $this->adjudication = array_filter(array_merge($this->adjudication, $value));

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitAddItemDetailSubDetail[]
     */
    public function getSubDetail(): array
    {
        return $this->subDetail;
    }

    public function setSubDetail(?FHIRExplanationOfBenefitAddItemDetailSubDetail ...$value): self
    {
        $this->subDetail = array_filter($value);

        return $this;
    }

    public function addSubDetail(?FHIRExplanationOfBenefitAddItemDetailSubDetail ...$value): self
    {
        $this->subDetail = array_filter(array_merge($this->subDetail, $value));

        return $this;
    }
}
