<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClaimAccident Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRClaimAccidentInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAddress;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDate;

class FHIRClaimAccident extends FHIRBackboneElement implements FHIRClaimAccidentInterface
{
    public const RESOURCE_NAME = 'Claim.accident';

    protected ?FHIRDate $date = null;
    protected ?FHIRCodeableConcept $type = null;
    protected FHIRAddress|FHIRReference|null $location = null;

    public function getDate(): ?FHIRDate
    {
        return $this->date;
    }

    public function setDate(string|FHIRDate|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getLocation(): FHIRAddress|FHIRReference|null
    {
        return $this->location;
    }

    public function setLocation(FHIRAddress|FHIRReference|null $value): self
    {
        $this->location = $value;

        return $this;
    }
}
