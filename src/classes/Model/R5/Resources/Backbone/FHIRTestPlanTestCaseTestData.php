<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestPlanTestCaseTestData Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestPlanTestCaseTestDataInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRTestPlanTestCaseTestData extends FHIRBackboneElement implements FHIRTestPlanTestCaseTestDataInterface
{
    public const RESOURCE_NAME = 'TestPlan.testCase.testData';

    protected ?FHIRCoding $type = null;
    protected ?FHIRReference $content = null;
    protected FHIRString|FHIRReference|null $source = null;

    public function getType(): ?FHIRCoding
    {
        return $this->type;
    }

    public function setType(?FHIRCoding $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getContent(): ?FHIRReference
    {
        return $this->content;
    }

    public function setContent(?FHIRReference $value): self
    {
        $this->content = $value;

        return $this;
    }

    public function getSource(): FHIRString|FHIRReference|null
    {
        return $this->source;
    }

    public function setSource(FHIRString|FHIRReference|null $value): self
    {
        $this->source = $value;

        return $this;
    }
}
