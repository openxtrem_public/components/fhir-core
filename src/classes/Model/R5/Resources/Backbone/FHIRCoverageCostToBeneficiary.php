<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CoverageCostToBeneficiary Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCoverageCostToBeneficiaryInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;

class FHIRCoverageCostToBeneficiary extends FHIRBackboneElement implements FHIRCoverageCostToBeneficiaryInterface
{
    public const RESOURCE_NAME = 'Coverage.costToBeneficiary';

    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableConcept $category = null;
    protected ?FHIRCodeableConcept $network = null;
    protected ?FHIRCodeableConcept $unit = null;
    protected ?FHIRCodeableConcept $term = null;
    protected FHIRQuantity|FHIRMoney|null $value = null;

    /** @var FHIRCoverageCostToBeneficiaryException[] */
    protected array $exception = [];

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getNetwork(): ?FHIRCodeableConcept
    {
        return $this->network;
    }

    public function setNetwork(?FHIRCodeableConcept $value): self
    {
        $this->network = $value;

        return $this;
    }

    public function getUnit(): ?FHIRCodeableConcept
    {
        return $this->unit;
    }

    public function setUnit(?FHIRCodeableConcept $value): self
    {
        $this->unit = $value;

        return $this;
    }

    public function getTerm(): ?FHIRCodeableConcept
    {
        return $this->term;
    }

    public function setTerm(?FHIRCodeableConcept $value): self
    {
        $this->term = $value;

        return $this;
    }

    public function getValue(): FHIRQuantity|FHIRMoney|null
    {
        return $this->value;
    }

    public function setValue(FHIRQuantity|FHIRMoney|null $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return FHIRCoverageCostToBeneficiaryException[]
     */
    public function getException(): array
    {
        return $this->exception;
    }

    public function setException(?FHIRCoverageCostToBeneficiaryException ...$value): self
    {
        $this->exception = array_filter($value);

        return $this;
    }

    public function addException(?FHIRCoverageCostToBeneficiaryException ...$value): self
    {
        $this->exception = array_filter(array_merge($this->exception, $value));

        return $this;
    }
}
