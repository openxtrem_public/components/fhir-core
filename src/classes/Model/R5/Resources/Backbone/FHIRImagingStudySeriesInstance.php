<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImagingStudySeriesInstance Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImagingStudySeriesInstanceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUnsignedInt;

class FHIRImagingStudySeriesInstance extends FHIRBackboneElement implements FHIRImagingStudySeriesInstanceInterface
{
    public const RESOURCE_NAME = 'ImagingStudy.series.instance';

    protected ?FHIRId $uid = null;
    protected ?FHIRCoding $sopClass = null;
    protected ?FHIRUnsignedInt $number = null;
    protected ?FHIRString $title = null;

    public function getUid(): ?FHIRId
    {
        return $this->uid;
    }

    public function setUid(string|FHIRId|null $value): self
    {
        $this->uid = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getSopClass(): ?FHIRCoding
    {
        return $this->sopClass;
    }

    public function setSopClass(?FHIRCoding $value): self
    {
        $this->sopClass = $value;

        return $this;
    }

    public function getNumber(): ?FHIRUnsignedInt
    {
        return $this->number;
    }

    public function setNumber(int|FHIRUnsignedInt|null $value): self
    {
        $this->number = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
