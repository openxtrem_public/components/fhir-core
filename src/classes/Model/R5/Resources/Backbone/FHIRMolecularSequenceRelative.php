<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MolecularSequenceRelative Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMolecularSequenceRelativeInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInteger;

class FHIRMolecularSequenceRelative extends FHIRBackboneElement implements FHIRMolecularSequenceRelativeInterface
{
    public const RESOURCE_NAME = 'MolecularSequence.relative';

    protected ?FHIRCodeableConcept $coordinateSystem = null;
    protected ?FHIRInteger $ordinalPosition = null;
    protected ?FHIRRange $sequenceRange = null;
    protected ?FHIRMolecularSequenceRelativeStartingSequence $startingSequence = null;

    /** @var FHIRMolecularSequenceRelativeEdit[] */
    protected array $edit = [];

    public function getCoordinateSystem(): ?FHIRCodeableConcept
    {
        return $this->coordinateSystem;
    }

    public function setCoordinateSystem(?FHIRCodeableConcept $value): self
    {
        $this->coordinateSystem = $value;

        return $this;
    }

    public function getOrdinalPosition(): ?FHIRInteger
    {
        return $this->ordinalPosition;
    }

    public function setOrdinalPosition(int|FHIRInteger|null $value): self
    {
        $this->ordinalPosition = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getSequenceRange(): ?FHIRRange
    {
        return $this->sequenceRange;
    }

    public function setSequenceRange(?FHIRRange $value): self
    {
        $this->sequenceRange = $value;

        return $this;
    }

    public function getStartingSequence(): ?FHIRMolecularSequenceRelativeStartingSequence
    {
        return $this->startingSequence;
    }

    public function setStartingSequence(?FHIRMolecularSequenceRelativeStartingSequence $value): self
    {
        $this->startingSequence = $value;

        return $this;
    }

    /**
     * @return FHIRMolecularSequenceRelativeEdit[]
     */
    public function getEdit(): array
    {
        return $this->edit;
    }

    public function setEdit(?FHIRMolecularSequenceRelativeEdit ...$value): self
    {
        $this->edit = array_filter($value);

        return $this;
    }

    public function addEdit(?FHIRMolecularSequenceRelativeEdit ...$value): self
    {
        $this->edit = array_filter(array_merge($this->edit, $value));

        return $this;
    }
}
