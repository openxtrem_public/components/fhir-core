<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ConceptMapGroup Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRConceptMapGroupInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;

class FHIRConceptMapGroup extends FHIRBackboneElement implements FHIRConceptMapGroupInterface
{
    public const RESOURCE_NAME = 'ConceptMap.group';

    protected ?FHIRCanonical $source = null;
    protected ?FHIRCanonical $target = null;

    /** @var FHIRConceptMapGroupElement[] */
    protected array $element = [];
    protected ?FHIRConceptMapGroupUnmapped $unmapped = null;

    public function getSource(): ?FHIRCanonical
    {
        return $this->source;
    }

    public function setSource(string|FHIRCanonical|null $value): self
    {
        $this->source = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    public function getTarget(): ?FHIRCanonical
    {
        return $this->target;
    }

    public function setTarget(string|FHIRCanonical|null $value): self
    {
        $this->target = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRConceptMapGroupElement[]
     */
    public function getElement(): array
    {
        return $this->element;
    }

    public function setElement(?FHIRConceptMapGroupElement ...$value): self
    {
        $this->element = array_filter($value);

        return $this;
    }

    public function addElement(?FHIRConceptMapGroupElement ...$value): self
    {
        $this->element = array_filter(array_merge($this->element, $value));

        return $this;
    }

    public function getUnmapped(): ?FHIRConceptMapGroupUnmapped
    {
        return $this->unmapped;
    }

    public function setUnmapped(?FHIRConceptMapGroupUnmapped $value): self
    {
        $this->unmapped = $value;

        return $this;
    }
}
