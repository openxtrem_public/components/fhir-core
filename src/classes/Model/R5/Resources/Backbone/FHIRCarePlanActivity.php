<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CarePlanActivity Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCarePlanActivityInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRCarePlanActivity extends FHIRBackboneElement implements FHIRCarePlanActivityInterface
{
    public const RESOURCE_NAME = 'CarePlan.activity';

    /** @var FHIRCodeableReference[] */
    protected array $performedActivity = [];

    /** @var FHIRAnnotation[] */
    protected array $progress = [];
    protected ?FHIRReference $plannedActivityReference = null;

    /**
     * @return FHIRCodeableReference[]
     */
    public function getPerformedActivity(): array
    {
        return $this->performedActivity;
    }

    public function setPerformedActivity(?FHIRCodeableReference ...$value): self
    {
        $this->performedActivity = array_filter($value);

        return $this;
    }

    public function addPerformedActivity(?FHIRCodeableReference ...$value): self
    {
        $this->performedActivity = array_filter(array_merge($this->performedActivity, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getProgress(): array
    {
        return $this->progress;
    }

    public function setProgress(?FHIRAnnotation ...$value): self
    {
        $this->progress = array_filter($value);

        return $this;
    }

    public function addProgress(?FHIRAnnotation ...$value): self
    {
        $this->progress = array_filter(array_merge($this->progress, $value));

        return $this;
    }

    public function getPlannedActivityReference(): ?FHIRReference
    {
        return $this->plannedActivityReference;
    }

    public function setPlannedActivityReference(?FHIRReference $value): self
    {
        $this->plannedActivityReference = $value;

        return $this;
    }
}
