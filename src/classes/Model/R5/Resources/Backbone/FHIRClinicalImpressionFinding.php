<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClinicalImpressionFinding Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRClinicalImpressionFindingInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRClinicalImpressionFinding extends FHIRBackboneElement implements FHIRClinicalImpressionFindingInterface
{
    public const RESOURCE_NAME = 'ClinicalImpression.finding';

    protected ?FHIRCodeableReference $item = null;
    protected ?FHIRString $basis = null;

    public function getItem(): ?FHIRCodeableReference
    {
        return $this->item;
    }

    public function setItem(?FHIRCodeableReference $value): self
    {
        $this->item = $value;

        return $this;
    }

    public function getBasis(): ?FHIRString
    {
        return $this->basis;
    }

    public function setBasis(string|FHIRString|null $value): self
    {
        $this->basis = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
