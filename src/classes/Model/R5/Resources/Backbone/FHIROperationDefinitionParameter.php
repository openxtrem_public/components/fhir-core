<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR OperationDefinitionParameter Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIROperationDefinitionParameterInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIROperationDefinitionParameter extends FHIRBackboneElement implements FHIROperationDefinitionParameterInterface
{
    public const RESOURCE_NAME = 'OperationDefinition.parameter';

    protected ?FHIRCode $name = null;
    protected ?FHIRCode $use = null;

    /** @var FHIRCode[] */
    protected array $scope = [];
    protected ?FHIRInteger $min = null;
    protected ?FHIRString $max = null;
    protected ?FHIRMarkdown $documentation = null;
    protected ?FHIRCode $type = null;

    /** @var FHIRCode[] */
    protected array $allowedType = [];

    /** @var FHIRCanonical[] */
    protected array $targetProfile = [];
    protected ?FHIRCode $searchType = null;
    protected ?FHIROperationDefinitionParameterBinding $binding = null;

    /** @var FHIROperationDefinitionParameterReferencedFrom[] */
    protected array $referencedFrom = [];

    /** @var FHIROperationDefinitionParameter[] */
    protected array $part = [];

    public function getName(): ?FHIRCode
    {
        return $this->name;
    }

    public function setName(string|FHIRCode|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getUse(): ?FHIRCode
    {
        return $this->use;
    }

    public function setUse(string|FHIRCode|null $value): self
    {
        $this->use = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getScope(): array
    {
        return $this->scope;
    }

    public function setScope(string|FHIRCode|null ...$value): self
    {
        $this->scope = [];
        $this->addScope(...$value);

        return $this;
    }

    public function addScope(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->scope = array_filter(array_merge($this->scope, $values));

        return $this;
    }

    public function getMin(): ?FHIRInteger
    {
        return $this->min;
    }

    public function setMin(int|FHIRInteger|null $value): self
    {
        $this->min = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getMax(): ?FHIRString
    {
        return $this->max;
    }

    public function setMax(string|FHIRString|null $value): self
    {
        $this->max = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDocumentation(): ?FHIRMarkdown
    {
        return $this->documentation;
    }

    public function setDocumentation(string|FHIRMarkdown|null $value): self
    {
        $this->documentation = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getAllowedType(): array
    {
        return $this->allowedType;
    }

    public function setAllowedType(string|FHIRCode|null ...$value): self
    {
        $this->allowedType = [];
        $this->addAllowedType(...$value);

        return $this;
    }

    public function addAllowedType(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->allowedType = array_filter(array_merge($this->allowedType, $values));

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getTargetProfile(): array
    {
        return $this->targetProfile;
    }

    public function setTargetProfile(string|FHIRCanonical|null ...$value): self
    {
        $this->targetProfile = [];
        $this->addTargetProfile(...$value);

        return $this;
    }

    public function addTargetProfile(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->targetProfile = array_filter(array_merge($this->targetProfile, $values));

        return $this;
    }

    public function getSearchType(): ?FHIRCode
    {
        return $this->searchType;
    }

    public function setSearchType(string|FHIRCode|null $value): self
    {
        $this->searchType = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getBinding(): ?FHIROperationDefinitionParameterBinding
    {
        return $this->binding;
    }

    public function setBinding(?FHIROperationDefinitionParameterBinding $value): self
    {
        $this->binding = $value;

        return $this;
    }

    /**
     * @return FHIROperationDefinitionParameterReferencedFrom[]
     */
    public function getReferencedFrom(): array
    {
        return $this->referencedFrom;
    }

    public function setReferencedFrom(?FHIROperationDefinitionParameterReferencedFrom ...$value): self
    {
        $this->referencedFrom = array_filter($value);

        return $this;
    }

    public function addReferencedFrom(?FHIROperationDefinitionParameterReferencedFrom ...$value): self
    {
        $this->referencedFrom = array_filter(array_merge($this->referencedFrom, $value));

        return $this;
    }

    /**
     * @return FHIROperationDefinitionParameter[]
     */
    public function getPart(): array
    {
        return $this->part;
    }

    public function setPart(?FHIROperationDefinitionParameter ...$value): self
    {
        $this->part = array_filter($value);

        return $this;
    }

    public function addPart(?FHIROperationDefinitionParameter ...$value): self
    {
        $this->part = array_filter(array_merge($this->part, $value));

        return $this;
    }
}
