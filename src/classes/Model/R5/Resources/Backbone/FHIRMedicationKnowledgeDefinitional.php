<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledgeDefinitional Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationKnowledgeDefinitionalInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRMedicationKnowledgeDefinitional extends FHIRBackboneElement implements FHIRMedicationKnowledgeDefinitionalInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge.definitional';

    /** @var FHIRReference[] */
    protected array $definition = [];
    protected ?FHIRCodeableConcept $doseForm = null;

    /** @var FHIRCodeableConcept[] */
    protected array $intendedRoute = [];

    /** @var FHIRMedicationKnowledgeDefinitionalIngredient[] */
    protected array $ingredient = [];

    /** @var FHIRMedicationKnowledgeDefinitionalDrugCharacteristic[] */
    protected array $drugCharacteristic = [];

    /**
     * @return FHIRReference[]
     */
    public function getDefinition(): array
    {
        return $this->definition;
    }

    public function setDefinition(?FHIRReference ...$value): self
    {
        $this->definition = array_filter($value);

        return $this;
    }

    public function addDefinition(?FHIRReference ...$value): self
    {
        $this->definition = array_filter(array_merge($this->definition, $value));

        return $this;
    }

    public function getDoseForm(): ?FHIRCodeableConcept
    {
        return $this->doseForm;
    }

    public function setDoseForm(?FHIRCodeableConcept $value): self
    {
        $this->doseForm = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getIntendedRoute(): array
    {
        return $this->intendedRoute;
    }

    public function setIntendedRoute(?FHIRCodeableConcept ...$value): self
    {
        $this->intendedRoute = array_filter($value);

        return $this;
    }

    public function addIntendedRoute(?FHIRCodeableConcept ...$value): self
    {
        $this->intendedRoute = array_filter(array_merge($this->intendedRoute, $value));

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeDefinitionalIngredient[]
     */
    public function getIngredient(): array
    {
        return $this->ingredient;
    }

    public function setIngredient(?FHIRMedicationKnowledgeDefinitionalIngredient ...$value): self
    {
        $this->ingredient = array_filter($value);

        return $this;
    }

    public function addIngredient(?FHIRMedicationKnowledgeDefinitionalIngredient ...$value): self
    {
        $this->ingredient = array_filter(array_merge($this->ingredient, $value));

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeDefinitionalDrugCharacteristic[]
     */
    public function getDrugCharacteristic(): array
    {
        return $this->drugCharacteristic;
    }

    public function setDrugCharacteristic(?FHIRMedicationKnowledgeDefinitionalDrugCharacteristic ...$value): self
    {
        $this->drugCharacteristic = array_filter($value);

        return $this;
    }

    public function addDrugCharacteristic(?FHIRMedicationKnowledgeDefinitionalDrugCharacteristic ...$value): self
    {
        $this->drugCharacteristic = array_filter(array_merge($this->drugCharacteristic, $value));

        return $this;
    }
}
