<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImagingSelectionInstance Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImagingSelectionInstanceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUnsignedInt;

class FHIRImagingSelectionInstance extends FHIRBackboneElement implements FHIRImagingSelectionInstanceInterface
{
    public const RESOURCE_NAME = 'ImagingSelection.instance';

    protected ?FHIRId $uid = null;
    protected ?FHIRUnsignedInt $number = null;
    protected ?FHIRCoding $sopClass = null;

    /** @var FHIRString[] */
    protected array $subset = [];

    /** @var FHIRImagingSelectionInstanceImageRegion2D[] */
    protected array $imageRegion2D = [];

    /** @var FHIRImagingSelectionInstanceImageRegion3D[] */
    protected array $imageRegion3D = [];

    public function getUid(): ?FHIRId
    {
        return $this->uid;
    }

    public function setUid(string|FHIRId|null $value): self
    {
        $this->uid = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getNumber(): ?FHIRUnsignedInt
    {
        return $this->number;
    }

    public function setNumber(int|FHIRUnsignedInt|null $value): self
    {
        $this->number = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getSopClass(): ?FHIRCoding
    {
        return $this->sopClass;
    }

    public function setSopClass(?FHIRCoding $value): self
    {
        $this->sopClass = $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getSubset(): array
    {
        return $this->subset;
    }

    public function setSubset(string|FHIRString|null ...$value): self
    {
        $this->subset = [];
        $this->addSubset(...$value);

        return $this;
    }

    public function addSubset(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->subset = array_filter(array_merge($this->subset, $values));

        return $this;
    }

    /**
     * @return FHIRImagingSelectionInstanceImageRegion2D[]
     */
    public function getImageRegion2D(): array
    {
        return $this->imageRegion2D;
    }

    public function setImageRegion2D(?FHIRImagingSelectionInstanceImageRegion2D ...$value): self
    {
        $this->imageRegion2D = array_filter($value);

        return $this;
    }

    public function addImageRegion2D(?FHIRImagingSelectionInstanceImageRegion2D ...$value): self
    {
        $this->imageRegion2D = array_filter(array_merge($this->imageRegion2D, $value));

        return $this;
    }

    /**
     * @return FHIRImagingSelectionInstanceImageRegion3D[]
     */
    public function getImageRegion3D(): array
    {
        return $this->imageRegion3D;
    }

    public function setImageRegion3D(?FHIRImagingSelectionInstanceImageRegion3D ...$value): self
    {
        $this->imageRegion3D = array_filter($value);

        return $this;
    }

    public function addImageRegion3D(?FHIRImagingSelectionInstanceImageRegion3D ...$value): self
    {
        $this->imageRegion3D = array_filter(array_merge($this->imageRegion3D, $value));

        return $this;
    }
}
