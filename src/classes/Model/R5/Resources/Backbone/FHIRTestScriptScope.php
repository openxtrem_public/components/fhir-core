<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestScriptScope Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestScriptScopeInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;

class FHIRTestScriptScope extends FHIRBackboneElement implements FHIRTestScriptScopeInterface
{
    public const RESOURCE_NAME = 'TestScript.scope';

    protected ?FHIRCanonical $artifact = null;
    protected ?FHIRCodeableConcept $conformance = null;
    protected ?FHIRCodeableConcept $phase = null;

    public function getArtifact(): ?FHIRCanonical
    {
        return $this->artifact;
    }

    public function setArtifact(string|FHIRCanonical|null $value): self
    {
        $this->artifact = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    public function getConformance(): ?FHIRCodeableConcept
    {
        return $this->conformance;
    }

    public function setConformance(?FHIRCodeableConcept $value): self
    {
        $this->conformance = $value;

        return $this;
    }

    public function getPhase(): ?FHIRCodeableConcept
    {
        return $this->phase;
    }

    public function setPhase(?FHIRCodeableConcept $value): self
    {
        $this->phase = $value;

        return $this;
    }
}
