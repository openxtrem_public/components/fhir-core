<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR StructureMapGroup Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRStructureMapGroupInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRStructureMapGroup extends FHIRBackboneElement implements FHIRStructureMapGroupInterface
{
    public const RESOURCE_NAME = 'StructureMap.group';

    protected ?FHIRId $name = null;
    protected ?FHIRId $extends = null;
    protected ?FHIRCode $typeMode = null;
    protected ?FHIRString $documentation = null;

    /** @var FHIRStructureMapGroupInput[] */
    protected array $input = [];

    /** @var FHIRStructureMapGroupRule[] */
    protected array $rule = [];

    public function getName(): ?FHIRId
    {
        return $this->name;
    }

    public function setName(string|FHIRId|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getExtends(): ?FHIRId
    {
        return $this->extends;
    }

    public function setExtends(string|FHIRId|null $value): self
    {
        $this->extends = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getTypeMode(): ?FHIRCode
    {
        return $this->typeMode;
    }

    public function setTypeMode(string|FHIRCode|null $value): self
    {
        $this->typeMode = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDocumentation(): ?FHIRString
    {
        return $this->documentation;
    }

    public function setDocumentation(string|FHIRString|null $value): self
    {
        $this->documentation = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRStructureMapGroupInput[]
     */
    public function getInput(): array
    {
        return $this->input;
    }

    public function setInput(?FHIRStructureMapGroupInput ...$value): self
    {
        $this->input = array_filter($value);

        return $this;
    }

    public function addInput(?FHIRStructureMapGroupInput ...$value): self
    {
        $this->input = array_filter(array_merge($this->input, $value));

        return $this;
    }

    /**
     * @return FHIRStructureMapGroupRule[]
     */
    public function getRule(): array
    {
        return $this->rule;
    }

    public function setRule(?FHIRStructureMapGroupRule ...$value): self
    {
        $this->rule = array_filter($value);

        return $this;
    }

    public function addRule(?FHIRStructureMapGroupRule ...$value): self
    {
        $this->rule = array_filter(array_merge($this->rule, $value));

        return $this;
    }
}
