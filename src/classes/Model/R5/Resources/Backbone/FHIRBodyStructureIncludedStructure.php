<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR BodyStructureIncludedStructure Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRBodyStructureIncludedStructureInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRBodyStructureIncludedStructure extends FHIRBackboneElement implements FHIRBodyStructureIncludedStructureInterface
{
    public const RESOURCE_NAME = 'BodyStructure.includedStructure';

    protected ?FHIRCodeableConcept $structure = null;
    protected ?FHIRCodeableConcept $laterality = null;

    /** @var FHIRBodyStructureIncludedStructureBodyLandmarkOrientation[] */
    protected array $bodyLandmarkOrientation = [];

    /** @var FHIRReference[] */
    protected array $spatialReference = [];

    /** @var FHIRCodeableConcept[] */
    protected array $qualifier = [];

    public function getStructure(): ?FHIRCodeableConcept
    {
        return $this->structure;
    }

    public function setStructure(?FHIRCodeableConcept $value): self
    {
        $this->structure = $value;

        return $this;
    }

    public function getLaterality(): ?FHIRCodeableConcept
    {
        return $this->laterality;
    }

    public function setLaterality(?FHIRCodeableConcept $value): self
    {
        $this->laterality = $value;

        return $this;
    }

    /**
     * @return FHIRBodyStructureIncludedStructureBodyLandmarkOrientation[]
     */
    public function getBodyLandmarkOrientation(): array
    {
        return $this->bodyLandmarkOrientation;
    }

    public function setBodyLandmarkOrientation(
        ?FHIRBodyStructureIncludedStructureBodyLandmarkOrientation ...$value,
    ): self
    {
        $this->bodyLandmarkOrientation = array_filter($value);

        return $this;
    }

    public function addBodyLandmarkOrientation(
        ?FHIRBodyStructureIncludedStructureBodyLandmarkOrientation ...$value,
    ): self
    {
        $this->bodyLandmarkOrientation = array_filter(array_merge($this->bodyLandmarkOrientation, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSpatialReference(): array
    {
        return $this->spatialReference;
    }

    public function setSpatialReference(?FHIRReference ...$value): self
    {
        $this->spatialReference = array_filter($value);

        return $this;
    }

    public function addSpatialReference(?FHIRReference ...$value): self
    {
        $this->spatialReference = array_filter(array_merge($this->spatialReference, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getQualifier(): array
    {
        return $this->qualifier;
    }

    public function setQualifier(?FHIRCodeableConcept ...$value): self
    {
        $this->qualifier = array_filter($value);

        return $this;
    }

    public function addQualifier(?FHIRCodeableConcept ...$value): self
    {
        $this->qualifier = array_filter(array_merge($this->qualifier, $value));

        return $this;
    }
}
