<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EvidenceVariableCharacteristicDefinitionByTypeAndValue Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREvidenceVariableCharacteristicDefinitionByTypeAndValueInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRId;

class FHIREvidenceVariableCharacteristicDefinitionByTypeAndValue extends FHIRBackboneElement implements FHIREvidenceVariableCharacteristicDefinitionByTypeAndValueInterface
{
    public const RESOURCE_NAME = 'EvidenceVariable.characteristic.definitionByTypeAndValue';

    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRCodeableConcept[] */
    protected array $method = [];
    protected ?FHIRReference $device = null;
    protected FHIRCodeableConcept|FHIRBoolean|FHIRQuantity|FHIRRange|FHIRReference|FHIRId|null $value = null;
    protected ?FHIRCodeableConcept $offset = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getMethod(): array
    {
        return $this->method;
    }

    public function setMethod(?FHIRCodeableConcept ...$value): self
    {
        $this->method = array_filter($value);

        return $this;
    }

    public function addMethod(?FHIRCodeableConcept ...$value): self
    {
        $this->method = array_filter(array_merge($this->method, $value));

        return $this;
    }

    public function getDevice(): ?FHIRReference
    {
        return $this->device;
    }

    public function setDevice(?FHIRReference $value): self
    {
        $this->device = $value;

        return $this;
    }

    public function getValue(): FHIRCodeableConcept|FHIRBoolean|FHIRQuantity|FHIRRange|FHIRReference|FHIRId|null
    {
        return $this->value;
    }

    public function setValue(
        FHIRCodeableConcept|FHIRBoolean|FHIRQuantity|FHIRRange|FHIRReference|FHIRId|null $value,
    ): self
    {
        $this->value = $value;

        return $this;
    }

    public function getOffset(): ?FHIRCodeableConcept
    {
        return $this->offset;
    }

    public function setOffset(?FHIRCodeableConcept $value): self
    {
        $this->offset = $value;

        return $this;
    }
}
