<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR InsurancePlanPlanSpecificCostBenefit Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRInsurancePlanPlanSpecificCostBenefitInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;

class FHIRInsurancePlanPlanSpecificCostBenefit extends FHIRBackboneElement implements FHIRInsurancePlanPlanSpecificCostBenefitInterface
{
    public const RESOURCE_NAME = 'InsurancePlan.plan.specificCost.benefit';

    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRInsurancePlanPlanSpecificCostBenefitCost[] */
    protected array $cost = [];

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRInsurancePlanPlanSpecificCostBenefitCost[]
     */
    public function getCost(): array
    {
        return $this->cost;
    }

    public function setCost(?FHIRInsurancePlanPlanSpecificCostBenefitCost ...$value): self
    {
        $this->cost = array_filter($value);

        return $this;
    }

    public function addCost(?FHIRInsurancePlanPlanSpecificCostBenefitCost ...$value): self
    {
        $this->cost = array_filter(array_merge($this->cost, $value));

        return $this;
    }
}
