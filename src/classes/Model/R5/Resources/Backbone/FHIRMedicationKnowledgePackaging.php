<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledgePackaging Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationKnowledgePackagingInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRMedicationKnowledgePackaging extends FHIRBackboneElement implements FHIRMedicationKnowledgePackagingInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge.packaging';

    /** @var FHIRMedicationKnowledgeCost[] */
    protected array $cost = [];
    protected ?FHIRReference $packagedProduct = null;

    /**
     * @return FHIRMedicationKnowledgeCost[]
     */
    public function getCost(): array
    {
        return $this->cost;
    }

    public function setCost(?FHIRMedicationKnowledgeCost ...$value): self
    {
        $this->cost = array_filter($value);

        return $this;
    }

    public function addCost(?FHIRMedicationKnowledgeCost ...$value): self
    {
        $this->cost = array_filter(array_merge($this->cost, $value));

        return $this;
    }

    public function getPackagedProduct(): ?FHIRReference
    {
        return $this->packagedProduct;
    }

    public function setPackagedProduct(?FHIRReference $value): self
    {
        $this->packagedProduct = $value;

        return $this;
    }
}
