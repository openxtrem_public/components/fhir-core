<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ActivityDefinitionParticipant Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRActivityDefinitionParticipantInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;

class FHIRActivityDefinitionParticipant extends FHIRBackboneElement implements FHIRActivityDefinitionParticipantInterface
{
    public const RESOURCE_NAME = 'ActivityDefinition.participant';

    protected ?FHIRCode $type = null;
    protected ?FHIRCanonical $typeCanonical = null;
    protected ?FHIRReference $typeReference = null;
    protected ?FHIRCodeableConcept $role = null;
    protected ?FHIRCodeableConcept $function = null;

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getTypeCanonical(): ?FHIRCanonical
    {
        return $this->typeCanonical;
    }

    public function setTypeCanonical(string|FHIRCanonical|null $value): self
    {
        $this->typeCanonical = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    public function getTypeReference(): ?FHIRReference
    {
        return $this->typeReference;
    }

    public function setTypeReference(?FHIRReference $value): self
    {
        $this->typeReference = $value;

        return $this;
    }

    public function getRole(): ?FHIRCodeableConcept
    {
        return $this->role;
    }

    public function setRole(?FHIRCodeableConcept $value): self
    {
        $this->role = $value;

        return $this;
    }

    public function getFunction(): ?FHIRCodeableConcept
    {
        return $this->function;
    }

    public function setFunction(?FHIRCodeableConcept $value): self
    {
        $this->function = $value;

        return $this;
    }
}
