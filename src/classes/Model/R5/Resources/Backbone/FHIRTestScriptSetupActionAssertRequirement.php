<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestScriptSetupActionAssertRequirement Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestScriptSetupActionAssertRequirementInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;

class FHIRTestScriptSetupActionAssertRequirement extends FHIRBackboneElement implements FHIRTestScriptSetupActionAssertRequirementInterface
{
    public const RESOURCE_NAME = 'TestScript.setup.action.assert.requirement';

    protected FHIRUri|FHIRCanonical|null $link = null;

    public function getLink(): FHIRUri|FHIRCanonical|null
    {
        return $this->link;
    }

    public function setLink(FHIRUri|FHIRCanonical|null $value): self
    {
        $this->link = $value;

        return $this;
    }
}
