<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PaymentReconciliationAllocation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRPaymentReconciliationAllocationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRPaymentReconciliationAllocation extends FHIRBackboneElement implements FHIRPaymentReconciliationAllocationInterface
{
    public const RESOURCE_NAME = 'PaymentReconciliation.allocation';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRIdentifier $predecessor = null;
    protected ?FHIRReference $target = null;
    protected FHIRString|FHIRIdentifier|FHIRPositiveInt|null $targetItem = null;
    protected ?FHIRReference $encounter = null;
    protected ?FHIRReference $account = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRReference $submitter = null;
    protected ?FHIRReference $response = null;
    protected ?FHIRDate $date = null;
    protected ?FHIRReference $responsible = null;
    protected ?FHIRReference $payee = null;
    protected ?FHIRMoney $amount = null;

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getPredecessor(): ?FHIRIdentifier
    {
        return $this->predecessor;
    }

    public function setPredecessor(?FHIRIdentifier $value): self
    {
        $this->predecessor = $value;

        return $this;
    }

    public function getTarget(): ?FHIRReference
    {
        return $this->target;
    }

    public function setTarget(?FHIRReference $value): self
    {
        $this->target = $value;

        return $this;
    }

    public function getTargetItem(): FHIRString|FHIRIdentifier|FHIRPositiveInt|null
    {
        return $this->targetItem;
    }

    public function setTargetItem(FHIRString|FHIRIdentifier|FHIRPositiveInt|null $value): self
    {
        $this->targetItem = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getAccount(): ?FHIRReference
    {
        return $this->account;
    }

    public function setAccount(?FHIRReference $value): self
    {
        $this->account = $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getSubmitter(): ?FHIRReference
    {
        return $this->submitter;
    }

    public function setSubmitter(?FHIRReference $value): self
    {
        $this->submitter = $value;

        return $this;
    }

    public function getResponse(): ?FHIRReference
    {
        return $this->response;
    }

    public function setResponse(?FHIRReference $value): self
    {
        $this->response = $value;

        return $this;
    }

    public function getDate(): ?FHIRDate
    {
        return $this->date;
    }

    public function setDate(string|FHIRDate|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getResponsible(): ?FHIRReference
    {
        return $this->responsible;
    }

    public function setResponsible(?FHIRReference $value): self
    {
        $this->responsible = $value;

        return $this;
    }

    public function getPayee(): ?FHIRReference
    {
        return $this->payee;
    }

    public function setPayee(?FHIRReference $value): self
    {
        $this->payee = $value;

        return $this;
    }

    public function getAmount(): ?FHIRMoney
    {
        return $this->amount;
    }

    public function setAmount(?FHIRMoney $value): self
    {
        $this->amount = $value;

        return $this;
    }
}
