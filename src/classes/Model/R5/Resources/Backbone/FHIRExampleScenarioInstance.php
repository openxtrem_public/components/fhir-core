<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExampleScenarioInstance Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExampleScenarioInstanceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;

class FHIRExampleScenarioInstance extends FHIRBackboneElement implements FHIRExampleScenarioInstanceInterface
{
    public const RESOURCE_NAME = 'ExampleScenario.instance';

    protected ?FHIRString $key = null;
    protected ?FHIRCoding $structureType = null;
    protected ?FHIRString $structureVersion = null;
    protected FHIRCanonical|FHIRUri|null $structureProfile = null;
    protected ?FHIRString $title = null;
    protected ?FHIRMarkdown $description = null;
    protected ?FHIRReference $content = null;

    /** @var FHIRExampleScenarioInstanceVersion[] */
    protected array $version = [];

    /** @var FHIRExampleScenarioInstanceContainedInstance[] */
    protected array $containedInstance = [];

    public function getKey(): ?FHIRString
    {
        return $this->key;
    }

    public function setKey(string|FHIRString|null $value): self
    {
        $this->key = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStructureType(): ?FHIRCoding
    {
        return $this->structureType;
    }

    public function setStructureType(?FHIRCoding $value): self
    {
        $this->structureType = $value;

        return $this;
    }

    public function getStructureVersion(): ?FHIRString
    {
        return $this->structureVersion;
    }

    public function setStructureVersion(string|FHIRString|null $value): self
    {
        $this->structureVersion = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStructureProfile(): FHIRCanonical|FHIRUri|null
    {
        return $this->structureProfile;
    }

    public function setStructureProfile(FHIRCanonical|FHIRUri|null $value): self
    {
        $this->structureProfile = $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getContent(): ?FHIRReference
    {
        return $this->content;
    }

    public function setContent(?FHIRReference $value): self
    {
        $this->content = $value;

        return $this;
    }

    /**
     * @return FHIRExampleScenarioInstanceVersion[]
     */
    public function getVersion(): array
    {
        return $this->version;
    }

    public function setVersion(?FHIRExampleScenarioInstanceVersion ...$value): self
    {
        $this->version = array_filter($value);

        return $this;
    }

    public function addVersion(?FHIRExampleScenarioInstanceVersion ...$value): self
    {
        $this->version = array_filter(array_merge($this->version, $value));

        return $this;
    }

    /**
     * @return FHIRExampleScenarioInstanceContainedInstance[]
     */
    public function getContainedInstance(): array
    {
        return $this->containedInstance;
    }

    public function setContainedInstance(?FHIRExampleScenarioInstanceContainedInstance ...$value): self
    {
        $this->containedInstance = array_filter($value);

        return $this;
    }

    public function addContainedInstance(?FHIRExampleScenarioInstanceContainedInstance ...$value): self
    {
        $this->containedInstance = array_filter(array_merge($this->containedInstance, $value));

        return $this;
    }
}
