<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AccountRelatedAccount Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAccountRelatedAccountInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRAccountRelatedAccount extends FHIRBackboneElement implements FHIRAccountRelatedAccountInterface
{
    public const RESOURCE_NAME = 'Account.relatedAccount';

    protected ?FHIRCodeableConcept $relationship = null;
    protected ?FHIRReference $account = null;

    public function getRelationship(): ?FHIRCodeableConcept
    {
        return $this->relationship;
    }

    public function setRelationship(?FHIRCodeableConcept $value): self
    {
        $this->relationship = $value;

        return $this;
    }

    public function getAccount(): ?FHIRReference
    {
        return $this->account;
    }

    public function setAccount(?FHIRReference $value): self
    {
        $this->account = $value;

        return $this;
    }
}
