<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PermissionJustification Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRPermissionJustificationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRPermissionJustification extends FHIRBackboneElement implements FHIRPermissionJustificationInterface
{
    public const RESOURCE_NAME = 'Permission.justification';

    /** @var FHIRCodeableConcept[] */
    protected array $basis = [];

    /** @var FHIRReference[] */
    protected array $evidence = [];

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getBasis(): array
    {
        return $this->basis;
    }

    public function setBasis(?FHIRCodeableConcept ...$value): self
    {
        $this->basis = array_filter($value);

        return $this;
    }

    public function addBasis(?FHIRCodeableConcept ...$value): self
    {
        $this->basis = array_filter(array_merge($this->basis, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEvidence(): array
    {
        return $this->evidence;
    }

    public function setEvidence(?FHIRReference ...$value): self
    {
        $this->evidence = array_filter($value);

        return $this;
    }

    public function addEvidence(?FHIRReference ...$value): self
    {
        $this->evidence = array_filter(array_merge($this->evidence, $value));

        return $this;
    }
}
