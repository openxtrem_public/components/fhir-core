<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductDefinitionName Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductDefinitionNameInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRMedicinalProductDefinitionName extends FHIRBackboneElement implements FHIRMedicinalProductDefinitionNameInterface
{
    public const RESOURCE_NAME = 'MedicinalProductDefinition.name';

    protected ?FHIRString $productName = null;
    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRMedicinalProductDefinitionNamePart[] */
    protected array $part = [];

    /** @var FHIRMedicinalProductDefinitionNameUsage[] */
    protected array $usage = [];

    public function getProductName(): ?FHIRString
    {
        return $this->productName;
    }

    public function setProductName(string|FHIRString|null $value): self
    {
        $this->productName = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRMedicinalProductDefinitionNamePart[]
     */
    public function getPart(): array
    {
        return $this->part;
    }

    public function setPart(?FHIRMedicinalProductDefinitionNamePart ...$value): self
    {
        $this->part = array_filter($value);

        return $this;
    }

    public function addPart(?FHIRMedicinalProductDefinitionNamePart ...$value): self
    {
        $this->part = array_filter(array_merge($this->part, $value));

        return $this;
    }

    /**
     * @return FHIRMedicinalProductDefinitionNameUsage[]
     */
    public function getUsage(): array
    {
        return $this->usage;
    }

    public function setUsage(?FHIRMedicinalProductDefinitionNameUsage ...$value): self
    {
        $this->usage = array_filter($value);

        return $this;
    }

    public function addUsage(?FHIRMedicinalProductDefinitionNameUsage ...$value): self
    {
        $this->usage = array_filter(array_merge($this->usage, $value));

        return $this;
    }
}
