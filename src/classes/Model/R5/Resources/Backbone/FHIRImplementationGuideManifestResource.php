<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImplementationGuideManifestResource Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImplementationGuideManifestResourceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUrl;

class FHIRImplementationGuideManifestResource extends FHIRBackboneElement implements FHIRImplementationGuideManifestResourceInterface
{
    public const RESOURCE_NAME = 'ImplementationGuide.manifest.resource';

    protected ?FHIRReference $reference = null;
    protected ?FHIRBoolean $isExample = null;

    /** @var FHIRCanonical[] */
    protected array $profile = [];
    protected ?FHIRUrl $relativePath = null;

    public function getReference(): ?FHIRReference
    {
        return $this->reference;
    }

    public function setReference(?FHIRReference $value): self
    {
        $this->reference = $value;

        return $this;
    }

    public function getIsExample(): ?FHIRBoolean
    {
        return $this->isExample;
    }

    public function setIsExample(bool|FHIRBoolean|null $value): self
    {
        $this->isExample = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getProfile(): array
    {
        return $this->profile;
    }

    public function setProfile(string|FHIRCanonical|null ...$value): self
    {
        $this->profile = [];
        $this->addProfile(...$value);

        return $this;
    }

    public function addProfile(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->profile = array_filter(array_merge($this->profile, $values));

        return $this;
    }

    public function getRelativePath(): ?FHIRUrl
    {
        return $this->relativePath;
    }

    public function setRelativePath(string|FHIRUrl|null $value): self
    {
        $this->relativePath = is_string($value) ? (new FHIRUrl())->setValue($value) : $value;

        return $this;
    }
}
