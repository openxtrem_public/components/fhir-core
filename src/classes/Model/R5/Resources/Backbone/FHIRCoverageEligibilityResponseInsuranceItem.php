<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CoverageEligibilityResponseInsuranceItem Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCoverageEligibilityResponseInsuranceItemInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;

class FHIRCoverageEligibilityResponseInsuranceItem extends FHIRBackboneElement implements FHIRCoverageEligibilityResponseInsuranceItemInterface
{
    public const RESOURCE_NAME = 'CoverageEligibilityResponse.insurance.item';

    protected ?FHIRCodeableConcept $category = null;
    protected ?FHIRCodeableConcept $productOrService = null;

    /** @var FHIRCodeableConcept[] */
    protected array $modifier = [];
    protected ?FHIRReference $provider = null;
    protected ?FHIRBoolean $excluded = null;
    protected ?FHIRString $name = null;
    protected ?FHIRString $description = null;
    protected ?FHIRCodeableConcept $network = null;
    protected ?FHIRCodeableConcept $unit = null;
    protected ?FHIRCodeableConcept $term = null;

    /** @var FHIRCoverageEligibilityResponseInsuranceItemBenefit[] */
    protected array $benefit = [];
    protected ?FHIRBoolean $authorizationRequired = null;

    /** @var FHIRCodeableConcept[] */
    protected array $authorizationSupporting = [];
    protected ?FHIRUri $authorizationUrl = null;

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getProductOrService(): ?FHIRCodeableConcept
    {
        return $this->productOrService;
    }

    public function setProductOrService(?FHIRCodeableConcept $value): self
    {
        $this->productOrService = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getModifier(): array
    {
        return $this->modifier;
    }

    public function setModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->modifier = array_filter($value);

        return $this;
    }

    public function addModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->modifier = array_filter(array_merge($this->modifier, $value));

        return $this;
    }

    public function getProvider(): ?FHIRReference
    {
        return $this->provider;
    }

    public function setProvider(?FHIRReference $value): self
    {
        $this->provider = $value;

        return $this;
    }

    public function getExcluded(): ?FHIRBoolean
    {
        return $this->excluded;
    }

    public function setExcluded(bool|FHIRBoolean|null $value): self
    {
        $this->excluded = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getNetwork(): ?FHIRCodeableConcept
    {
        return $this->network;
    }

    public function setNetwork(?FHIRCodeableConcept $value): self
    {
        $this->network = $value;

        return $this;
    }

    public function getUnit(): ?FHIRCodeableConcept
    {
        return $this->unit;
    }

    public function setUnit(?FHIRCodeableConcept $value): self
    {
        $this->unit = $value;

        return $this;
    }

    public function getTerm(): ?FHIRCodeableConcept
    {
        return $this->term;
    }

    public function setTerm(?FHIRCodeableConcept $value): self
    {
        $this->term = $value;

        return $this;
    }

    /**
     * @return FHIRCoverageEligibilityResponseInsuranceItemBenefit[]
     */
    public function getBenefit(): array
    {
        return $this->benefit;
    }

    public function setBenefit(?FHIRCoverageEligibilityResponseInsuranceItemBenefit ...$value): self
    {
        $this->benefit = array_filter($value);

        return $this;
    }

    public function addBenefit(?FHIRCoverageEligibilityResponseInsuranceItemBenefit ...$value): self
    {
        $this->benefit = array_filter(array_merge($this->benefit, $value));

        return $this;
    }

    public function getAuthorizationRequired(): ?FHIRBoolean
    {
        return $this->authorizationRequired;
    }

    public function setAuthorizationRequired(bool|FHIRBoolean|null $value): self
    {
        $this->authorizationRequired = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getAuthorizationSupporting(): array
    {
        return $this->authorizationSupporting;
    }

    public function setAuthorizationSupporting(?FHIRCodeableConcept ...$value): self
    {
        $this->authorizationSupporting = array_filter($value);

        return $this;
    }

    public function addAuthorizationSupporting(?FHIRCodeableConcept ...$value): self
    {
        $this->authorizationSupporting = array_filter(array_merge($this->authorizationSupporting, $value));

        return $this;
    }

    public function getAuthorizationUrl(): ?FHIRUri
    {
        return $this->authorizationUrl;
    }

    public function setAuthorizationUrl(string|FHIRUri|null $value): self
    {
        $this->authorizationUrl = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }
}
