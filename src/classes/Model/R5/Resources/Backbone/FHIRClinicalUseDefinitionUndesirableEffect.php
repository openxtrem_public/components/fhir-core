<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClinicalUseDefinitionUndesirableEffect Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRClinicalUseDefinitionUndesirableEffectInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;

class FHIRClinicalUseDefinitionUndesirableEffect extends FHIRBackboneElement implements FHIRClinicalUseDefinitionUndesirableEffectInterface
{
    public const RESOURCE_NAME = 'ClinicalUseDefinition.undesirableEffect';

    protected ?FHIRCodeableReference $symptomConditionEffect = null;
    protected ?FHIRCodeableConcept $classification = null;
    protected ?FHIRCodeableConcept $frequencyOfOccurrence = null;

    public function getSymptomConditionEffect(): ?FHIRCodeableReference
    {
        return $this->symptomConditionEffect;
    }

    public function setSymptomConditionEffect(?FHIRCodeableReference $value): self
    {
        $this->symptomConditionEffect = $value;

        return $this;
    }

    public function getClassification(): ?FHIRCodeableConcept
    {
        return $this->classification;
    }

    public function setClassification(?FHIRCodeableConcept $value): self
    {
        $this->classification = $value;

        return $this;
    }

    public function getFrequencyOfOccurrence(): ?FHIRCodeableConcept
    {
        return $this->frequencyOfOccurrence;
    }

    public function setFrequencyOfOccurrence(?FHIRCodeableConcept $value): self
    {
        $this->frequencyOfOccurrence = $value;

        return $this;
    }
}
