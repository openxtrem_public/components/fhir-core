<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CitationSummary Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCitationSummaryInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;

class FHIRCitationSummary extends FHIRBackboneElement implements FHIRCitationSummaryInterface
{
    public const RESOURCE_NAME = 'Citation.summary';

    protected ?FHIRCodeableConcept $style = null;
    protected ?FHIRMarkdown $text = null;

    public function getStyle(): ?FHIRCodeableConcept
    {
        return $this->style;
    }

    public function setStyle(?FHIRCodeableConcept $value): self
    {
        $this->style = $value;

        return $this;
    }

    public function getText(): ?FHIRMarkdown
    {
        return $this->text;
    }

    public function setText(string|FHIRMarkdown|null $value): self
    {
        $this->text = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }
}
