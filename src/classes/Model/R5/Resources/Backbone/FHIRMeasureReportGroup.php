<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MeasureReportGroup Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMeasureReportGroupInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRMeasureReportGroup extends FHIRBackboneElement implements FHIRMeasureReportGroupInterface
{
    public const RESOURCE_NAME = 'MeasureReport.group';

    protected ?FHIRString $linkId = null;
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRReference $subject = null;

    /** @var FHIRMeasureReportGroupPopulation[] */
    protected array $population = [];
    protected FHIRQuantity|FHIRDateTime|FHIRCodeableConcept|FHIRPeriod|FHIRRange|FHIRDuration|null $measureScore = null;

    /** @var FHIRMeasureReportGroupStratifier[] */
    protected array $stratifier = [];

    public function getLinkId(): ?FHIRString
    {
        return $this->linkId;
    }

    public function setLinkId(string|FHIRString|null $value): self
    {
        $this->linkId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    /**
     * @return FHIRMeasureReportGroupPopulation[]
     */
    public function getPopulation(): array
    {
        return $this->population;
    }

    public function setPopulation(?FHIRMeasureReportGroupPopulation ...$value): self
    {
        $this->population = array_filter($value);

        return $this;
    }

    public function addPopulation(?FHIRMeasureReportGroupPopulation ...$value): self
    {
        $this->population = array_filter(array_merge($this->population, $value));

        return $this;
    }

    public function getMeasureScore(
    ): FHIRQuantity|FHIRDateTime|FHIRCodeableConcept|FHIRPeriod|FHIRRange|FHIRDuration|null
    {
        return $this->measureScore;
    }

    public function setMeasureScore(
        FHIRQuantity|FHIRDateTime|FHIRCodeableConcept|FHIRPeriod|FHIRRange|FHIRDuration|null $value,
    ): self
    {
        $this->measureScore = $value;

        return $this;
    }

    /**
     * @return FHIRMeasureReportGroupStratifier[]
     */
    public function getStratifier(): array
    {
        return $this->stratifier;
    }

    public function setStratifier(?FHIRMeasureReportGroupStratifier ...$value): self
    {
        $this->stratifier = array_filter($value);

        return $this;
    }

    public function addStratifier(?FHIRMeasureReportGroupStratifier ...$value): self
    {
        $this->stratifier = array_filter(array_merge($this->stratifier, $value));

        return $this;
    }
}
