<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledgeStorageGuidelineEnvironmentalSetting Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationKnowledgeStorageGuidelineEnvironmentalSettingInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRange;

class FHIRMedicationKnowledgeStorageGuidelineEnvironmentalSetting extends FHIRBackboneElement implements FHIRMedicationKnowledgeStorageGuidelineEnvironmentalSettingInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge.storageGuideline.environmentalSetting';

    protected ?FHIRCodeableConcept $type = null;
    protected FHIRQuantity|FHIRRange|FHIRCodeableConcept|null $value = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getValue(): FHIRQuantity|FHIRRange|FHIRCodeableConcept|null
    {
        return $this->value;
    }

    public function setValue(FHIRQuantity|FHIRRange|FHIRCodeableConcept|null $value): self
    {
        $this->value = $value;

        return $this;
    }
}
