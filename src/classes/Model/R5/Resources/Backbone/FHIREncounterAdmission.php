<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EncounterAdmission Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREncounterAdmissionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIREncounterAdmission extends FHIRBackboneElement implements FHIREncounterAdmissionInterface
{
    public const RESOURCE_NAME = 'Encounter.admission';

    protected ?FHIRIdentifier $preAdmissionIdentifier = null;
    protected ?FHIRReference $origin = null;
    protected ?FHIRCodeableConcept $admitSource = null;
    protected ?FHIRCodeableConcept $reAdmission = null;
    protected ?FHIRReference $destination = null;
    protected ?FHIRCodeableConcept $dischargeDisposition = null;

    public function getPreAdmissionIdentifier(): ?FHIRIdentifier
    {
        return $this->preAdmissionIdentifier;
    }

    public function setPreAdmissionIdentifier(?FHIRIdentifier $value): self
    {
        $this->preAdmissionIdentifier = $value;

        return $this;
    }

    public function getOrigin(): ?FHIRReference
    {
        return $this->origin;
    }

    public function setOrigin(?FHIRReference $value): self
    {
        $this->origin = $value;

        return $this;
    }

    public function getAdmitSource(): ?FHIRCodeableConcept
    {
        return $this->admitSource;
    }

    public function setAdmitSource(?FHIRCodeableConcept $value): self
    {
        $this->admitSource = $value;

        return $this;
    }

    public function getReAdmission(): ?FHIRCodeableConcept
    {
        return $this->reAdmission;
    }

    public function setReAdmission(?FHIRCodeableConcept $value): self
    {
        $this->reAdmission = $value;

        return $this;
    }

    public function getDestination(): ?FHIRReference
    {
        return $this->destination;
    }

    public function setDestination(?FHIRReference $value): self
    {
        $this->destination = $value;

        return $this;
    }

    public function getDischargeDisposition(): ?FHIRCodeableConcept
    {
        return $this->dischargeDisposition;
    }

    public function setDischargeDisposition(?FHIRCodeableConcept $value): self
    {
        $this->dischargeDisposition = $value;

        return $this;
    }
}
