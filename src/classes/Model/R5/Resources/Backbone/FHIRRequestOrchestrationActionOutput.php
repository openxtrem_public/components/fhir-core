<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR RequestOrchestrationActionOutput Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRRequestOrchestrationActionOutputInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRDataRequirement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRRequestOrchestrationActionOutput extends FHIRBackboneElement implements FHIRRequestOrchestrationActionOutputInterface
{
    public const RESOURCE_NAME = 'RequestOrchestration.action.output';

    protected ?FHIRString $title = null;
    protected ?FHIRDataRequirement $requirement = null;
    protected ?FHIRString $relatedData = null;

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getRequirement(): ?FHIRDataRequirement
    {
        return $this->requirement;
    }

    public function setRequirement(?FHIRDataRequirement $value): self
    {
        $this->requirement = $value;

        return $this;
    }

    public function getRelatedData(): ?FHIRString
    {
        return $this->relatedData;
    }

    public function setRelatedData(string|FHIRString|null $value): self
    {
        $this->relatedData = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
