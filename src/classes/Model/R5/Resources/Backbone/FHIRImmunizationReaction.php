<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImmunizationReaction Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImmunizationReactionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;

class FHIRImmunizationReaction extends FHIRBackboneElement implements FHIRImmunizationReactionInterface
{
    public const RESOURCE_NAME = 'Immunization.reaction';

    protected ?FHIRDateTime $date = null;
    protected ?FHIRCodeableReference $manifestation = null;
    protected ?FHIRBoolean $reported = null;

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getManifestation(): ?FHIRCodeableReference
    {
        return $this->manifestation;
    }

    public function setManifestation(?FHIRCodeableReference $value): self
    {
        $this->manifestation = $value;

        return $this;
    }

    public function getReported(): ?FHIRBoolean
    {
        return $this->reported;
    }

    public function setReported(bool|FHIRBoolean|null $value): self
    {
        $this->reported = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }
}
