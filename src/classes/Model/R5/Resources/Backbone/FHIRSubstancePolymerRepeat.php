<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstancePolymerRepeat Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstancePolymerRepeatInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRSubstancePolymerRepeat extends FHIRBackboneElement implements FHIRSubstancePolymerRepeatInterface
{
    public const RESOURCE_NAME = 'SubstancePolymer.repeat';

    protected ?FHIRString $averageMolecularFormula = null;
    protected ?FHIRCodeableConcept $repeatUnitAmountType = null;

    /** @var FHIRSubstancePolymerRepeatRepeatUnit[] */
    protected array $repeatUnit = [];

    public function getAverageMolecularFormula(): ?FHIRString
    {
        return $this->averageMolecularFormula;
    }

    public function setAverageMolecularFormula(string|FHIRString|null $value): self
    {
        $this->averageMolecularFormula = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getRepeatUnitAmountType(): ?FHIRCodeableConcept
    {
        return $this->repeatUnitAmountType;
    }

    public function setRepeatUnitAmountType(?FHIRCodeableConcept $value): self
    {
        $this->repeatUnitAmountType = $value;

        return $this;
    }

    /**
     * @return FHIRSubstancePolymerRepeatRepeatUnit[]
     */
    public function getRepeatUnit(): array
    {
        return $this->repeatUnit;
    }

    public function setRepeatUnit(?FHIRSubstancePolymerRepeatRepeatUnit ...$value): self
    {
        $this->repeatUnit = array_filter($value);

        return $this;
    }

    public function addRepeatUnit(?FHIRSubstancePolymerRepeatRepeatUnit ...$value): self
    {
        $this->repeatUnit = array_filter(array_merge($this->repeatUnit, $value));

        return $this;
    }
}
