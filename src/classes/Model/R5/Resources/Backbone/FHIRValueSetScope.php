<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ValueSetScope Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRValueSetScopeInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRValueSetScope extends FHIRBackboneElement implements FHIRValueSetScopeInterface
{
    public const RESOURCE_NAME = 'ValueSet.scope';

    protected ?FHIRString $inclusionCriteria = null;
    protected ?FHIRString $exclusionCriteria = null;

    public function getInclusionCriteria(): ?FHIRString
    {
        return $this->inclusionCriteria;
    }

    public function setInclusionCriteria(string|FHIRString|null $value): self
    {
        $this->inclusionCriteria = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getExclusionCriteria(): ?FHIRString
    {
        return $this->exclusionCriteria;
    }

    public function setExclusionCriteria(string|FHIRString|null $value): self
    {
        $this->exclusionCriteria = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
