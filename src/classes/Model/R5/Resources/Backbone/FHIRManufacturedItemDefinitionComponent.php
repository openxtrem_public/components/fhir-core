<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ManufacturedItemDefinitionComponent Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRManufacturedItemDefinitionComponentInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;

class FHIRManufacturedItemDefinitionComponent extends FHIRBackboneElement implements FHIRManufacturedItemDefinitionComponentInterface
{
    public const RESOURCE_NAME = 'ManufacturedItemDefinition.component';

    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRCodeableConcept[] */
    protected array $function = [];

    /** @var FHIRQuantity[] */
    protected array $amount = [];

    /** @var FHIRManufacturedItemDefinitionComponentConstituent[] */
    protected array $constituent = [];

    /** @var FHIRManufacturedItemDefinitionProperty[] */
    protected array $property = [];

    /** @var FHIRManufacturedItemDefinitionComponent[] */
    protected array $component = [];

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getFunction(): array
    {
        return $this->function;
    }

    public function setFunction(?FHIRCodeableConcept ...$value): self
    {
        $this->function = array_filter($value);

        return $this;
    }

    public function addFunction(?FHIRCodeableConcept ...$value): self
    {
        $this->function = array_filter(array_merge($this->function, $value));

        return $this;
    }

    /**
     * @return FHIRQuantity[]
     */
    public function getAmount(): array
    {
        return $this->amount;
    }

    public function setAmount(?FHIRQuantity ...$value): self
    {
        $this->amount = array_filter($value);

        return $this;
    }

    public function addAmount(?FHIRQuantity ...$value): self
    {
        $this->amount = array_filter(array_merge($this->amount, $value));

        return $this;
    }

    /**
     * @return FHIRManufacturedItemDefinitionComponentConstituent[]
     */
    public function getConstituent(): array
    {
        return $this->constituent;
    }

    public function setConstituent(?FHIRManufacturedItemDefinitionComponentConstituent ...$value): self
    {
        $this->constituent = array_filter($value);

        return $this;
    }

    public function addConstituent(?FHIRManufacturedItemDefinitionComponentConstituent ...$value): self
    {
        $this->constituent = array_filter(array_merge($this->constituent, $value));

        return $this;
    }

    /**
     * @return FHIRManufacturedItemDefinitionProperty[]
     */
    public function getProperty(): array
    {
        return $this->property;
    }

    public function setProperty(?FHIRManufacturedItemDefinitionProperty ...$value): self
    {
        $this->property = array_filter($value);

        return $this;
    }

    public function addProperty(?FHIRManufacturedItemDefinitionProperty ...$value): self
    {
        $this->property = array_filter(array_merge($this->property, $value));

        return $this;
    }

    /**
     * @return FHIRManufacturedItemDefinitionComponent[]
     */
    public function getComponent(): array
    {
        return $this->component;
    }

    public function setComponent(?FHIRManufacturedItemDefinitionComponent ...$value): self
    {
        $this->component = array_filter($value);

        return $this;
    }

    public function addComponent(?FHIRManufacturedItemDefinitionComponent ...$value): self
    {
        $this->component = array_filter(array_merge($this->component, $value));

        return $this;
    }
}
