<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PermissionRuleData Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRPermissionRuleDataInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRExpression;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;

class FHIRPermissionRuleData extends FHIRBackboneElement implements FHIRPermissionRuleDataInterface
{
    public const RESOURCE_NAME = 'Permission.rule.data';

    /** @var FHIRPermissionRuleDataResource[] */
    protected array $resource = [];

    /** @var FHIRCoding[] */
    protected array $security = [];

    /** @var FHIRPeriod[] */
    protected array $period = [];
    protected ?FHIRExpression $expression = null;

    /**
     * @return FHIRPermissionRuleDataResource[]
     */
    public function getResource(): array
    {
        return $this->resource;
    }

    public function setResource(?FHIRPermissionRuleDataResource ...$value): self
    {
        $this->resource = array_filter($value);

        return $this;
    }

    public function addResource(?FHIRPermissionRuleDataResource ...$value): self
    {
        $this->resource = array_filter(array_merge($this->resource, $value));

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getSecurity(): array
    {
        return $this->security;
    }

    public function setSecurity(?FHIRCoding ...$value): self
    {
        $this->security = array_filter($value);

        return $this;
    }

    public function addSecurity(?FHIRCoding ...$value): self
    {
        $this->security = array_filter(array_merge($this->security, $value));

        return $this;
    }

    /**
     * @return FHIRPeriod[]
     */
    public function getPeriod(): array
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod ...$value): self
    {
        $this->period = array_filter($value);

        return $this;
    }

    public function addPeriod(?FHIRPeriod ...$value): self
    {
        $this->period = array_filter(array_merge($this->period, $value));

        return $this;
    }

    public function getExpression(): ?FHIRExpression
    {
        return $this->expression;
    }

    public function setExpression(?FHIRExpression $value): self
    {
        $this->expression = $value;

        return $this;
    }
}
