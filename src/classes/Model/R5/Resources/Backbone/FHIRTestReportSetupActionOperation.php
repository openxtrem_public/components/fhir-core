<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestReportSetupActionOperation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestReportSetupActionOperationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;

class FHIRTestReportSetupActionOperation extends FHIRBackboneElement implements FHIRTestReportSetupActionOperationInterface
{
    public const RESOURCE_NAME = 'TestReport.setup.action.operation';

    protected ?FHIRCode $result = null;
    protected ?FHIRMarkdown $message = null;
    protected ?FHIRUri $detail = null;

    public function getResult(): ?FHIRCode
    {
        return $this->result;
    }

    public function setResult(string|FHIRCode|null $value): self
    {
        $this->result = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getMessage(): ?FHIRMarkdown
    {
        return $this->message;
    }

    public function setMessage(string|FHIRMarkdown|null $value): self
    {
        $this->message = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getDetail(): ?FHIRUri
    {
        return $this->detail;
    }

    public function setDetail(string|FHIRUri|null $value): self
    {
        $this->detail = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }
}
