<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledgeDefinitionalIngredient Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationKnowledgeDefinitionalIngredientInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRatio;

class FHIRMedicationKnowledgeDefinitionalIngredient extends FHIRBackboneElement implements FHIRMedicationKnowledgeDefinitionalIngredientInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge.definitional.ingredient';

    protected ?FHIRCodeableReference $item = null;
    protected ?FHIRCodeableConcept $type = null;
    protected FHIRRatio|FHIRCodeableConcept|FHIRQuantity|null $strength = null;

    public function getItem(): ?FHIRCodeableReference
    {
        return $this->item;
    }

    public function setItem(?FHIRCodeableReference $value): self
    {
        $this->item = $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getStrength(): FHIRRatio|FHIRCodeableConcept|FHIRQuantity|null
    {
        return $this->strength;
    }

    public function setStrength(FHIRRatio|FHIRCodeableConcept|FHIRQuantity|null $value): self
    {
        $this->strength = $value;

        return $this;
    }
}
