<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR RequestOrchestrationActionCondition Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRRequestOrchestrationActionConditionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRExpression;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;

class FHIRRequestOrchestrationActionCondition extends FHIRBackboneElement implements FHIRRequestOrchestrationActionConditionInterface
{
    public const RESOURCE_NAME = 'RequestOrchestration.action.condition';

    protected ?FHIRCode $kind = null;
    protected ?FHIRExpression $expression = null;

    public function getKind(): ?FHIRCode
    {
        return $this->kind;
    }

    public function setKind(string|FHIRCode|null $value): self
    {
        $this->kind = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getExpression(): ?FHIRExpression
    {
        return $this->expression;
    }

    public function setExpression(?FHIRExpression $value): self
    {
        $this->expression = $value;

        return $this;
    }
}
