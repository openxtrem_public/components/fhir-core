<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DocumentReferenceContentProfile Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDocumentReferenceContentProfileInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;

class FHIRDocumentReferenceContentProfile extends FHIRBackboneElement implements FHIRDocumentReferenceContentProfileInterface
{
    public const RESOURCE_NAME = 'DocumentReference.content.profile';

    protected FHIRCoding|FHIRUri|FHIRCanonical|null $value = null;

    public function getValue(): FHIRCoding|FHIRUri|FHIRCanonical|null
    {
        return $this->value;
    }

    public function setValue(FHIRCoding|FHIRUri|FHIRCanonical|null $value): self
    {
        $this->value = $value;

        return $this;
    }
}
