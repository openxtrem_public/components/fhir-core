<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR InventoryItemCharacteristic Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRInventoryItemCharacteristicInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRElement;

class FHIRInventoryItemCharacteristic extends FHIRBackboneElement implements FHIRInventoryItemCharacteristicInterface
{
    public const RESOURCE_NAME = 'InventoryItem.characteristic';

    protected ?FHIRCodeableConcept $characteristicType = null;
    protected ?FHIRElement $value = null;

    public function getCharacteristicType(): ?FHIRCodeableConcept
    {
        return $this->characteristicType;
    }

    public function setCharacteristicType(?FHIRCodeableConcept $value): self
    {
        $this->characteristicType = $value;

        return $this;
    }

    public function getValue(): ?FHIRElement
    {
        return $this->value;
    }

    public function setValue(?FHIRElement $value): self
    {
        $this->value = $value;

        return $this;
    }
}
