<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImplementationGuideGlobal Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImplementationGuideGlobalInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;

class FHIRImplementationGuideGlobal extends FHIRBackboneElement implements FHIRImplementationGuideGlobalInterface
{
    public const RESOURCE_NAME = 'ImplementationGuide.global';

    protected ?FHIRCode $type = null;
    protected ?FHIRCanonical $profile = null;

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getProfile(): ?FHIRCanonical
    {
        return $this->profile;
    }

    public function setProfile(string|FHIRCanonical|null $value): self
    {
        $this->profile = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }
}
