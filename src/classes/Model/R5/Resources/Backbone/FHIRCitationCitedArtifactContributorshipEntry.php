<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CitationCitedArtifactContributorshipEntry Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCitationCitedArtifactContributorshipEntryInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRCitationCitedArtifactContributorshipEntry extends FHIRBackboneElement implements FHIRCitationCitedArtifactContributorshipEntryInterface
{
    public const RESOURCE_NAME = 'Citation.citedArtifact.contributorship.entry';

    protected ?FHIRReference $contributor = null;
    protected ?FHIRString $forenameInitials = null;

    /** @var FHIRReference[] */
    protected array $affiliation = [];

    /** @var FHIRCodeableConcept[] */
    protected array $contributionType = [];
    protected ?FHIRCodeableConcept $role = null;

    /** @var FHIRCitationCitedArtifactContributorshipEntryContributionInstance[] */
    protected array $contributionInstance = [];
    protected ?FHIRBoolean $correspondingContact = null;
    protected ?FHIRPositiveInt $rankingOrder = null;

    public function getContributor(): ?FHIRReference
    {
        return $this->contributor;
    }

    public function setContributor(?FHIRReference $value): self
    {
        $this->contributor = $value;

        return $this;
    }

    public function getForenameInitials(): ?FHIRString
    {
        return $this->forenameInitials;
    }

    public function setForenameInitials(string|FHIRString|null $value): self
    {
        $this->forenameInitials = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAffiliation(): array
    {
        return $this->affiliation;
    }

    public function setAffiliation(?FHIRReference ...$value): self
    {
        $this->affiliation = array_filter($value);

        return $this;
    }

    public function addAffiliation(?FHIRReference ...$value): self
    {
        $this->affiliation = array_filter(array_merge($this->affiliation, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getContributionType(): array
    {
        return $this->contributionType;
    }

    public function setContributionType(?FHIRCodeableConcept ...$value): self
    {
        $this->contributionType = array_filter($value);

        return $this;
    }

    public function addContributionType(?FHIRCodeableConcept ...$value): self
    {
        $this->contributionType = array_filter(array_merge($this->contributionType, $value));

        return $this;
    }

    public function getRole(): ?FHIRCodeableConcept
    {
        return $this->role;
    }

    public function setRole(?FHIRCodeableConcept $value): self
    {
        $this->role = $value;

        return $this;
    }

    /**
     * @return FHIRCitationCitedArtifactContributorshipEntryContributionInstance[]
     */
    public function getContributionInstance(): array
    {
        return $this->contributionInstance;
    }

    public function setContributionInstance(
        ?FHIRCitationCitedArtifactContributorshipEntryContributionInstance ...$value,
    ): self
    {
        $this->contributionInstance = array_filter($value);

        return $this;
    }

    public function addContributionInstance(
        ?FHIRCitationCitedArtifactContributorshipEntryContributionInstance ...$value,
    ): self
    {
        $this->contributionInstance = array_filter(array_merge($this->contributionInstance, $value));

        return $this;
    }

    public function getCorrespondingContact(): ?FHIRBoolean
    {
        return $this->correspondingContact;
    }

    public function setCorrespondingContact(bool|FHIRBoolean|null $value): self
    {
        $this->correspondingContact = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getRankingOrder(): ?FHIRPositiveInt
    {
        return $this->rankingOrder;
    }

    public function setRankingOrder(int|FHIRPositiveInt|null $value): self
    {
        $this->rankingOrder = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }
}
