<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstancePolymerRepeatRepeatUnit Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstancePolymerRepeatRepeatUnitInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRSubstancePolymerRepeatRepeatUnit extends FHIRBackboneElement implements FHIRSubstancePolymerRepeatRepeatUnitInterface
{
    public const RESOURCE_NAME = 'SubstancePolymer.repeat.repeatUnit';

    protected ?FHIRString $unit = null;
    protected ?FHIRCodeableConcept $orientation = null;
    protected ?FHIRInteger $amount = null;

    /** @var FHIRSubstancePolymerRepeatRepeatUnitDegreeOfPolymerisation[] */
    protected array $degreeOfPolymerisation = [];

    /** @var FHIRSubstancePolymerRepeatRepeatUnitStructuralRepresentation[] */
    protected array $structuralRepresentation = [];

    public function getUnit(): ?FHIRString
    {
        return $this->unit;
    }

    public function setUnit(string|FHIRString|null $value): self
    {
        $this->unit = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getOrientation(): ?FHIRCodeableConcept
    {
        return $this->orientation;
    }

    public function setOrientation(?FHIRCodeableConcept $value): self
    {
        $this->orientation = $value;

        return $this;
    }

    public function getAmount(): ?FHIRInteger
    {
        return $this->amount;
    }

    public function setAmount(int|FHIRInteger|null $value): self
    {
        $this->amount = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRSubstancePolymerRepeatRepeatUnitDegreeOfPolymerisation[]
     */
    public function getDegreeOfPolymerisation(): array
    {
        return $this->degreeOfPolymerisation;
    }

    public function setDegreeOfPolymerisation(
        ?FHIRSubstancePolymerRepeatRepeatUnitDegreeOfPolymerisation ...$value,
    ): self
    {
        $this->degreeOfPolymerisation = array_filter($value);

        return $this;
    }

    public function addDegreeOfPolymerisation(
        ?FHIRSubstancePolymerRepeatRepeatUnitDegreeOfPolymerisation ...$value,
    ): self
    {
        $this->degreeOfPolymerisation = array_filter(array_merge($this->degreeOfPolymerisation, $value));

        return $this;
    }

    /**
     * @return FHIRSubstancePolymerRepeatRepeatUnitStructuralRepresentation[]
     */
    public function getStructuralRepresentation(): array
    {
        return $this->structuralRepresentation;
    }

    public function setStructuralRepresentation(
        ?FHIRSubstancePolymerRepeatRepeatUnitStructuralRepresentation ...$value,
    ): self
    {
        $this->structuralRepresentation = array_filter($value);

        return $this;
    }

    public function addStructuralRepresentation(
        ?FHIRSubstancePolymerRepeatRepeatUnitStructuralRepresentation ...$value,
    ): self
    {
        $this->structuralRepresentation = array_filter(array_merge($this->structuralRepresentation, $value));

        return $this;
    }
}
