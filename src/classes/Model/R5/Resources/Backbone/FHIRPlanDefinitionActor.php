<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PlanDefinitionActor Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRPlanDefinitionActorInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRPlanDefinitionActor extends FHIRBackboneElement implements FHIRPlanDefinitionActorInterface
{
    public const RESOURCE_NAME = 'PlanDefinition.actor';

    protected ?FHIRString $title = null;
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRPlanDefinitionActorOption[] */
    protected array $option = [];

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRPlanDefinitionActorOption[]
     */
    public function getOption(): array
    {
        return $this->option;
    }

    public function setOption(?FHIRPlanDefinitionActorOption ...$value): self
    {
        $this->option = array_filter($value);

        return $this;
    }

    public function addOption(?FHIRPlanDefinitionActorOption ...$value): self
    {
        $this->option = array_filter(array_merge($this->option, $value));

        return $this;
    }
}
