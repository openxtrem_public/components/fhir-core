<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CoverageEligibilityRequestItem Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCoverageEligibilityRequestItemInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;

class FHIRCoverageEligibilityRequestItem extends FHIRBackboneElement implements FHIRCoverageEligibilityRequestItemInterface
{
    public const RESOURCE_NAME = 'CoverageEligibilityRequest.item';

    /** @var FHIRPositiveInt[] */
    protected array $supportingInfoSequence = [];
    protected ?FHIRCodeableConcept $category = null;
    protected ?FHIRCodeableConcept $productOrService = null;

    /** @var FHIRCodeableConcept[] */
    protected array $modifier = [];
    protected ?FHIRReference $provider = null;
    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRMoney $unitPrice = null;
    protected ?FHIRReference $facility = null;

    /** @var FHIRCoverageEligibilityRequestItemDiagnosis[] */
    protected array $diagnosis = [];

    /** @var FHIRReference[] */
    protected array $detail = [];

    /**
     * @return FHIRPositiveInt[]
     */
    public function getSupportingInfoSequence(): array
    {
        return $this->supportingInfoSequence;
    }

    public function setSupportingInfoSequence(int|FHIRPositiveInt|null ...$value): self
    {
        $this->supportingInfoSequence = [];
        $this->addSupportingInfoSequence(...$value);

        return $this;
    }

    public function addSupportingInfoSequence(int|FHIRPositiveInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRPositiveInt())->setValue($v) : $v, $value);

        $this->supportingInfoSequence = array_filter(array_merge($this->supportingInfoSequence, $values));

        return $this;
    }

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getProductOrService(): ?FHIRCodeableConcept
    {
        return $this->productOrService;
    }

    public function setProductOrService(?FHIRCodeableConcept $value): self
    {
        $this->productOrService = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getModifier(): array
    {
        return $this->modifier;
    }

    public function setModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->modifier = array_filter($value);

        return $this;
    }

    public function addModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->modifier = array_filter(array_merge($this->modifier, $value));

        return $this;
    }

    public function getProvider(): ?FHIRReference
    {
        return $this->provider;
    }

    public function setProvider(?FHIRReference $value): self
    {
        $this->provider = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getUnitPrice(): ?FHIRMoney
    {
        return $this->unitPrice;
    }

    public function setUnitPrice(?FHIRMoney $value): self
    {
        $this->unitPrice = $value;

        return $this;
    }

    public function getFacility(): ?FHIRReference
    {
        return $this->facility;
    }

    public function setFacility(?FHIRReference $value): self
    {
        $this->facility = $value;

        return $this;
    }

    /**
     * @return FHIRCoverageEligibilityRequestItemDiagnosis[]
     */
    public function getDiagnosis(): array
    {
        return $this->diagnosis;
    }

    public function setDiagnosis(?FHIRCoverageEligibilityRequestItemDiagnosis ...$value): self
    {
        $this->diagnosis = array_filter($value);

        return $this;
    }

    public function addDiagnosis(?FHIRCoverageEligibilityRequestItemDiagnosis ...$value): self
    {
        $this->diagnosis = array_filter(array_merge($this->diagnosis, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getDetail(): array
    {
        return $this->detail;
    }

    public function setDetail(?FHIRReference ...$value): self
    {
        $this->detail = array_filter($value);

        return $this;
    }

    public function addDetail(?FHIRReference ...$value): self
    {
        $this->detail = array_filter(array_merge($this->detail, $value));

        return $this;
    }
}
