<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR NutritionOrderOralDietNutrient Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRNutritionOrderOralDietNutrientInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;

class FHIRNutritionOrderOralDietNutrient extends FHIRBackboneElement implements FHIRNutritionOrderOralDietNutrientInterface
{
    public const RESOURCE_NAME = 'NutritionOrder.oralDiet.nutrient';

    protected ?FHIRCodeableConcept $modifier = null;
    protected ?FHIRQuantity $amount = null;

    public function getModifier(): ?FHIRCodeableConcept
    {
        return $this->modifier;
    }

    public function setModifier(?FHIRCodeableConcept $value): self
    {
        $this->modifier = $value;

        return $this;
    }

    public function getAmount(): ?FHIRQuantity
    {
        return $this->amount;
    }

    public function setAmount(?FHIRQuantity $value): self
    {
        $this->amount = $value;

        return $this;
    }
}
