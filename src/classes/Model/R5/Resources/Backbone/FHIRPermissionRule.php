<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PermissionRule Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRPermissionRuleInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;

class FHIRPermissionRule extends FHIRBackboneElement implements FHIRPermissionRuleInterface
{
    public const RESOURCE_NAME = 'Permission.rule';

    protected ?FHIRCode $type = null;

    /** @var FHIRPermissionRuleData[] */
    protected array $data = [];

    /** @var FHIRPermissionRuleActivity[] */
    protected array $activity = [];

    /** @var FHIRCodeableConcept[] */
    protected array $limit = [];

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRPermissionRuleData[]
     */
    public function getData(): array
    {
        return $this->data;
    }

    public function setData(?FHIRPermissionRuleData ...$value): self
    {
        $this->data = array_filter($value);

        return $this;
    }

    public function addData(?FHIRPermissionRuleData ...$value): self
    {
        $this->data = array_filter(array_merge($this->data, $value));

        return $this;
    }

    /**
     * @return FHIRPermissionRuleActivity[]
     */
    public function getActivity(): array
    {
        return $this->activity;
    }

    public function setActivity(?FHIRPermissionRuleActivity ...$value): self
    {
        $this->activity = array_filter($value);

        return $this;
    }

    public function addActivity(?FHIRPermissionRuleActivity ...$value): self
    {
        $this->activity = array_filter(array_merge($this->activity, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getLimit(): array
    {
        return $this->limit;
    }

    public function setLimit(?FHIRCodeableConcept ...$value): self
    {
        $this->limit = array_filter($value);

        return $this;
    }

    public function addLimit(?FHIRCodeableConcept ...$value): self
    {
        $this->limit = array_filter(array_merge($this->limit, $value));

        return $this;
    }
}
