<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MeasureGroupPopulation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMeasureGroupPopulationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRExpression;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRMeasureGroupPopulation extends FHIRBackboneElement implements FHIRMeasureGroupPopulationInterface
{
    public const RESOURCE_NAME = 'Measure.group.population';

    protected ?FHIRString $linkId = null;
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRMarkdown $description = null;
    protected ?FHIRExpression $criteria = null;
    protected ?FHIRReference $groupDefinition = null;
    protected ?FHIRString $inputPopulationId = null;
    protected ?FHIRCodeableConcept $aggregateMethod = null;

    public function getLinkId(): ?FHIRString
    {
        return $this->linkId;
    }

    public function setLinkId(string|FHIRString|null $value): self
    {
        $this->linkId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getCriteria(): ?FHIRExpression
    {
        return $this->criteria;
    }

    public function setCriteria(?FHIRExpression $value): self
    {
        $this->criteria = $value;

        return $this;
    }

    public function getGroupDefinition(): ?FHIRReference
    {
        return $this->groupDefinition;
    }

    public function setGroupDefinition(?FHIRReference $value): self
    {
        $this->groupDefinition = $value;

        return $this;
    }

    public function getInputPopulationId(): ?FHIRString
    {
        return $this->inputPopulationId;
    }

    public function setInputPopulationId(string|FHIRString|null $value): self
    {
        $this->inputPopulationId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getAggregateMethod(): ?FHIRCodeableConcept
    {
        return $this->aggregateMethod;
    }

    public function setAggregateMethod(?FHIRCodeableConcept $value): self
    {
        $this->aggregateMethod = $value;

        return $this;
    }
}
