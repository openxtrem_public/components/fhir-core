<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR IngredientSubstanceStrengthReferenceStrength Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRIngredientSubstanceStrengthReferenceStrengthInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRatio;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRatioRange;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRIngredientSubstanceStrengthReferenceStrength extends FHIRBackboneElement implements FHIRIngredientSubstanceStrengthReferenceStrengthInterface
{
    public const RESOURCE_NAME = 'Ingredient.substance.strength.referenceStrength';

    protected ?FHIRCodeableReference $substance = null;
    protected FHIRRatio|FHIRRatioRange|FHIRQuantity|null $strength = null;
    protected ?FHIRString $measurementPoint = null;

    /** @var FHIRCodeableConcept[] */
    protected array $country = [];

    public function getSubstance(): ?FHIRCodeableReference
    {
        return $this->substance;
    }

    public function setSubstance(?FHIRCodeableReference $value): self
    {
        $this->substance = $value;

        return $this;
    }

    public function getStrength(): FHIRRatio|FHIRRatioRange|FHIRQuantity|null
    {
        return $this->strength;
    }

    public function setStrength(FHIRRatio|FHIRRatioRange|FHIRQuantity|null $value): self
    {
        $this->strength = $value;

        return $this;
    }

    public function getMeasurementPoint(): ?FHIRString
    {
        return $this->measurementPoint;
    }

    public function setMeasurementPoint(string|FHIRString|null $value): self
    {
        $this->measurementPoint = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCountry(): array
    {
        return $this->country;
    }

    public function setCountry(?FHIRCodeableConcept ...$value): self
    {
        $this->country = array_filter($value);

        return $this;
    }

    public function addCountry(?FHIRCodeableConcept ...$value): self
    {
        $this->country = array_filter(array_merge($this->country, $value));

        return $this;
    }
}
