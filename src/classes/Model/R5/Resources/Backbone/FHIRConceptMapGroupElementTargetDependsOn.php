<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ConceptMapGroupElementTargetDependsOn Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRConceptMapGroupElementTargetDependsOnInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRConceptMapGroupElementTargetDependsOn extends FHIRBackboneElement implements FHIRConceptMapGroupElementTargetDependsOnInterface
{
    public const RESOURCE_NAME = 'ConceptMap.group.element.target.dependsOn';

    protected ?FHIRCode $attribute = null;
    protected FHIRCode|FHIRCoding|FHIRString|FHIRBoolean|FHIRQuantity|null $value = null;
    protected ?FHIRCanonical $valueSet = null;

    public function getAttribute(): ?FHIRCode
    {
        return $this->attribute;
    }

    public function setAttribute(string|FHIRCode|null $value): self
    {
        $this->attribute = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getValue(): FHIRCode|FHIRCoding|FHIRString|FHIRBoolean|FHIRQuantity|null
    {
        return $this->value;
    }

    public function setValue(FHIRCode|FHIRCoding|FHIRString|FHIRBoolean|FHIRQuantity|null $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getValueSet(): ?FHIRCanonical
    {
        return $this->valueSet;
    }

    public function setValueSet(string|FHIRCanonical|null $value): self
    {
        $this->valueSet = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }
}
