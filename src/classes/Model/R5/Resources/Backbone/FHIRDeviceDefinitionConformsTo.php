<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceDefinitionConformsTo Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceDefinitionConformsToInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRelatedArtifact;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRDeviceDefinitionConformsTo extends FHIRBackboneElement implements FHIRDeviceDefinitionConformsToInterface
{
    public const RESOURCE_NAME = 'DeviceDefinition.conformsTo';

    protected ?FHIRCodeableConcept $category = null;
    protected ?FHIRCodeableConcept $specification = null;

    /** @var FHIRString[] */
    protected array $version = [];

    /** @var FHIRRelatedArtifact[] */
    protected array $source = [];

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getSpecification(): ?FHIRCodeableConcept
    {
        return $this->specification;
    }

    public function setSpecification(?FHIRCodeableConcept $value): self
    {
        $this->specification = $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getVersion(): array
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null ...$value): self
    {
        $this->version = [];
        $this->addVersion(...$value);

        return $this;
    }

    public function addVersion(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->version = array_filter(array_merge($this->version, $values));

        return $this;
    }

    /**
     * @return FHIRRelatedArtifact[]
     */
    public function getSource(): array
    {
        return $this->source;
    }

    public function setSource(?FHIRRelatedArtifact ...$value): self
    {
        $this->source = array_filter($value);

        return $this;
    }

    public function addSource(?FHIRRelatedArtifact ...$value): self
    {
        $this->source = array_filter(array_merge($this->source, $value));

        return $this;
    }
}
