<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EvidenceStatistic Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREvidenceStatisticInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUnsignedInt;

class FHIREvidenceStatistic extends FHIRBackboneElement implements FHIREvidenceStatisticInterface
{
    public const RESOURCE_NAME = 'Evidence.statistic';

    protected ?FHIRMarkdown $description = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];
    protected ?FHIRCodeableConcept $statisticType = null;
    protected ?FHIRCodeableConcept $category = null;
    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRUnsignedInt $numberOfEvents = null;
    protected ?FHIRUnsignedInt $numberAffected = null;
    protected ?FHIREvidenceStatisticSampleSize $sampleSize = null;

    /** @var FHIREvidenceStatisticAttributeEstimate[] */
    protected array $attributeEstimate = [];

    /** @var FHIREvidenceStatisticModelCharacteristic[] */
    protected array $modelCharacteristic = [];

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    public function getStatisticType(): ?FHIRCodeableConcept
    {
        return $this->statisticType;
    }

    public function setStatisticType(?FHIRCodeableConcept $value): self
    {
        $this->statisticType = $value;

        return $this;
    }

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getNumberOfEvents(): ?FHIRUnsignedInt
    {
        return $this->numberOfEvents;
    }

    public function setNumberOfEvents(int|FHIRUnsignedInt|null $value): self
    {
        $this->numberOfEvents = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getNumberAffected(): ?FHIRUnsignedInt
    {
        return $this->numberAffected;
    }

    public function setNumberAffected(int|FHIRUnsignedInt|null $value): self
    {
        $this->numberAffected = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getSampleSize(): ?FHIREvidenceStatisticSampleSize
    {
        return $this->sampleSize;
    }

    public function setSampleSize(?FHIREvidenceStatisticSampleSize $value): self
    {
        $this->sampleSize = $value;

        return $this;
    }

    /**
     * @return FHIREvidenceStatisticAttributeEstimate[]
     */
    public function getAttributeEstimate(): array
    {
        return $this->attributeEstimate;
    }

    public function setAttributeEstimate(?FHIREvidenceStatisticAttributeEstimate ...$value): self
    {
        $this->attributeEstimate = array_filter($value);

        return $this;
    }

    public function addAttributeEstimate(?FHIREvidenceStatisticAttributeEstimate ...$value): self
    {
        $this->attributeEstimate = array_filter(array_merge($this->attributeEstimate, $value));

        return $this;
    }

    /**
     * @return FHIREvidenceStatisticModelCharacteristic[]
     */
    public function getModelCharacteristic(): array
    {
        return $this->modelCharacteristic;
    }

    public function setModelCharacteristic(?FHIREvidenceStatisticModelCharacteristic ...$value): self
    {
        $this->modelCharacteristic = array_filter($value);

        return $this;
    }

    public function addModelCharacteristic(?FHIREvidenceStatisticModelCharacteristic ...$value): self
    {
        $this->modelCharacteristic = array_filter(array_merge($this->modelCharacteristic, $value));

        return $this;
    }
}
