<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledgeIndicationGuideline Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationKnowledgeIndicationGuidelineInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;

class FHIRMedicationKnowledgeIndicationGuideline extends FHIRBackboneElement implements FHIRMedicationKnowledgeIndicationGuidelineInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge.indicationGuideline';

    /** @var FHIRCodeableReference[] */
    protected array $indication = [];

    /** @var FHIRMedicationKnowledgeIndicationGuidelineDosingGuideline[] */
    protected array $dosingGuideline = [];

    /**
     * @return FHIRCodeableReference[]
     */
    public function getIndication(): array
    {
        return $this->indication;
    }

    public function setIndication(?FHIRCodeableReference ...$value): self
    {
        $this->indication = array_filter($value);

        return $this;
    }

    public function addIndication(?FHIRCodeableReference ...$value): self
    {
        $this->indication = array_filter(array_merge($this->indication, $value));

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeIndicationGuidelineDosingGuideline[]
     */
    public function getDosingGuideline(): array
    {
        return $this->dosingGuideline;
    }

    public function setDosingGuideline(?FHIRMedicationKnowledgeIndicationGuidelineDosingGuideline ...$value): self
    {
        $this->dosingGuideline = array_filter($value);

        return $this;
    }

    public function addDosingGuideline(?FHIRMedicationKnowledgeIndicationGuidelineDosingGuideline ...$value): self
    {
        $this->dosingGuideline = array_filter(array_merge($this->dosingGuideline, $value));

        return $this;
    }
}
