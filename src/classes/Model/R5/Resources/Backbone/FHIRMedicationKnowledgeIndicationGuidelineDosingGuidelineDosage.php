<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledgeIndicationGuidelineDosingGuidelineDosage Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationKnowledgeIndicationGuidelineDosingGuidelineDosageInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRDosage;

class FHIRMedicationKnowledgeIndicationGuidelineDosingGuidelineDosage extends FHIRBackboneElement implements FHIRMedicationKnowledgeIndicationGuidelineDosingGuidelineDosageInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge.indicationGuideline.dosingGuideline.dosage';

    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRDosage[] */
    protected array $dosage = [];

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRDosage[]
     */
    public function getDosage(): array
    {
        return $this->dosage;
    }

    public function setDosage(?FHIRDosage ...$value): self
    {
        $this->dosage = array_filter($value);

        return $this;
    }

    public function addDosage(?FHIRDosage ...$value): self
    {
        $this->dosage = array_filter(array_merge($this->dosage, $value));

        return $this;
    }
}
