<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR GenomicStudyAnalysisInput Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRGenomicStudyAnalysisInputInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRGenomicStudyAnalysisInput extends FHIRBackboneElement implements FHIRGenomicStudyAnalysisInputInterface
{
    public const RESOURCE_NAME = 'GenomicStudy.analysis.input';

    protected ?FHIRReference $file = null;
    protected ?FHIRCodeableConcept $type = null;
    protected FHIRIdentifier|FHIRReference|null $generatedBy = null;

    public function getFile(): ?FHIRReference
    {
        return $this->file;
    }

    public function setFile(?FHIRReference $value): self
    {
        $this->file = $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getGeneratedBy(): FHIRIdentifier|FHIRReference|null
    {
        return $this->generatedBy;
    }

    public function setGeneratedBy(FHIRIdentifier|FHIRReference|null $value): self
    {
        $this->generatedBy = $value;

        return $this;
    }
}
