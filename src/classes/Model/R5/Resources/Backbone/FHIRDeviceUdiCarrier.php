<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceUdiCarrier Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceUdiCarrierInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBase64Binary;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;

class FHIRDeviceUdiCarrier extends FHIRBackboneElement implements FHIRDeviceUdiCarrierInterface
{
    public const RESOURCE_NAME = 'Device.udiCarrier';

    protected ?FHIRString $deviceIdentifier = null;
    protected ?FHIRUri $issuer = null;
    protected ?FHIRUri $jurisdiction = null;
    protected ?FHIRBase64Binary $carrierAIDC = null;
    protected ?FHIRString $carrierHRF = null;
    protected ?FHIRCode $entryType = null;

    public function getDeviceIdentifier(): ?FHIRString
    {
        return $this->deviceIdentifier;
    }

    public function setDeviceIdentifier(string|FHIRString|null $value): self
    {
        $this->deviceIdentifier = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getIssuer(): ?FHIRUri
    {
        return $this->issuer;
    }

    public function setIssuer(string|FHIRUri|null $value): self
    {
        $this->issuer = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getJurisdiction(): ?FHIRUri
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(string|FHIRUri|null $value): self
    {
        $this->jurisdiction = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getCarrierAIDC(): ?FHIRBase64Binary
    {
        return $this->carrierAIDC;
    }

    public function setCarrierAIDC(string|FHIRBase64Binary|null $value): self
    {
        $this->carrierAIDC = is_string($value) ? (new FHIRBase64Binary())->setValue($value) : $value;

        return $this;
    }

    public function getCarrierHRF(): ?FHIRString
    {
        return $this->carrierHRF;
    }

    public function setCarrierHRF(string|FHIRString|null $value): self
    {
        $this->carrierHRF = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getEntryType(): ?FHIRCode
    {
        return $this->entryType;
    }

    public function setEntryType(string|FHIRCode|null $value): self
    {
        $this->entryType = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }
}
