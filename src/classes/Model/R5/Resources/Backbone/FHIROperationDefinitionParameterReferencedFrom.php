<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR OperationDefinitionParameterReferencedFrom Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIROperationDefinitionParameterReferencedFromInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIROperationDefinitionParameterReferencedFrom extends FHIRBackboneElement implements FHIROperationDefinitionParameterReferencedFromInterface
{
    public const RESOURCE_NAME = 'OperationDefinition.parameter.referencedFrom';

    protected ?FHIRString $source = null;
    protected ?FHIRString $sourceId = null;

    public function getSource(): ?FHIRString
    {
        return $this->source;
    }

    public function setSource(string|FHIRString|null $value): self
    {
        $this->source = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSourceId(): ?FHIRString
    {
        return $this->sourceId;
    }

    public function setSourceId(string|FHIRString|null $value): self
    {
        $this->sourceId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
