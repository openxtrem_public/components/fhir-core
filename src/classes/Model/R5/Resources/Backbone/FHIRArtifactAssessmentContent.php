<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ArtifactAssessmentContent Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRArtifactAssessmentContentInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRelatedArtifact;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;

class FHIRArtifactAssessmentContent extends FHIRBackboneElement implements FHIRArtifactAssessmentContentInterface
{
    public const RESOURCE_NAME = 'ArtifactAssessment.content';

    protected ?FHIRCode $informationType = null;
    protected ?FHIRMarkdown $summary = null;
    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRCodeableConcept[] */
    protected array $classifier = [];
    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRReference $author = null;

    /** @var FHIRUri[] */
    protected array $path = [];

    /** @var FHIRRelatedArtifact[] */
    protected array $relatedArtifact = [];
    protected ?FHIRBoolean $freeToShare = null;

    /** @var FHIRArtifactAssessmentContent[] */
    protected array $component = [];

    public function getInformationType(): ?FHIRCode
    {
        return $this->informationType;
    }

    public function setInformationType(string|FHIRCode|null $value): self
    {
        $this->informationType = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getSummary(): ?FHIRMarkdown
    {
        return $this->summary;
    }

    public function setSummary(string|FHIRMarkdown|null $value): self
    {
        $this->summary = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getClassifier(): array
    {
        return $this->classifier;
    }

    public function setClassifier(?FHIRCodeableConcept ...$value): self
    {
        $this->classifier = array_filter($value);

        return $this;
    }

    public function addClassifier(?FHIRCodeableConcept ...$value): self
    {
        $this->classifier = array_filter(array_merge($this->classifier, $value));

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getAuthor(): ?FHIRReference
    {
        return $this->author;
    }

    public function setAuthor(?FHIRReference $value): self
    {
        $this->author = $value;

        return $this;
    }

    /**
     * @return FHIRUri[]
     */
    public function getPath(): array
    {
        return $this->path;
    }

    public function setPath(string|FHIRUri|null ...$value): self
    {
        $this->path = [];
        $this->addPath(...$value);

        return $this;
    }

    public function addPath(string|FHIRUri|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUri())->setValue($v) : $v, $value);

        $this->path = array_filter(array_merge($this->path, $values));

        return $this;
    }

    /**
     * @return FHIRRelatedArtifact[]
     */
    public function getRelatedArtifact(): array
    {
        return $this->relatedArtifact;
    }

    public function setRelatedArtifact(?FHIRRelatedArtifact ...$value): self
    {
        $this->relatedArtifact = array_filter($value);

        return $this;
    }

    public function addRelatedArtifact(?FHIRRelatedArtifact ...$value): self
    {
        $this->relatedArtifact = array_filter(array_merge($this->relatedArtifact, $value));

        return $this;
    }

    public function getFreeToShare(): ?FHIRBoolean
    {
        return $this->freeToShare;
    }

    public function setFreeToShare(bool|FHIRBoolean|null $value): self
    {
        $this->freeToShare = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRArtifactAssessmentContent[]
     */
    public function getComponent(): array
    {
        return $this->component;
    }

    public function setComponent(?FHIRArtifactAssessmentContent ...$value): self
    {
        $this->component = array_filter($value);

        return $this;
    }

    public function addComponent(?FHIRArtifactAssessmentContent ...$value): self
    {
        $this->component = array_filter(array_merge($this->component, $value));

        return $this;
    }
}
