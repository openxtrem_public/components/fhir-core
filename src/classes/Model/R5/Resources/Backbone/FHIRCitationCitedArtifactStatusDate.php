<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CitationCitedArtifactStatusDate Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCitationCitedArtifactStatusDateInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;

class FHIRCitationCitedArtifactStatusDate extends FHIRBackboneElement implements FHIRCitationCitedArtifactStatusDateInterface
{
    public const RESOURCE_NAME = 'Citation.citedArtifact.statusDate';

    protected ?FHIRCodeableConcept $activity = null;
    protected ?FHIRBoolean $actual = null;
    protected ?FHIRPeriod $period = null;

    public function getActivity(): ?FHIRCodeableConcept
    {
        return $this->activity;
    }

    public function setActivity(?FHIRCodeableConcept $value): self
    {
        $this->activity = $value;

        return $this;
    }

    public function getActual(): ?FHIRBoolean
    {
        return $this->actual;
    }

    public function setActual(bool|FHIRBoolean|null $value): self
    {
        $this->actual = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }
}
