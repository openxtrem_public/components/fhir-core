<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR InsurancePlanCoverageBenefitLimit Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRInsurancePlanCoverageBenefitLimitInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;

class FHIRInsurancePlanCoverageBenefitLimit extends FHIRBackboneElement implements FHIRInsurancePlanCoverageBenefitLimitInterface
{
    public const RESOURCE_NAME = 'InsurancePlan.coverage.benefit.limit';

    protected ?FHIRQuantity $value = null;
    protected ?FHIRCodeableConcept $code = null;

    public function getValue(): ?FHIRQuantity
    {
        return $this->value;
    }

    public function setValue(?FHIRQuantity $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }
}
