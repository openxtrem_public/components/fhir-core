<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AdverseEventMitigatingAction Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAdverseEventMitigatingActionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRAdverseEventMitigatingAction extends FHIRBackboneElement implements FHIRAdverseEventMitigatingActionInterface
{
    public const RESOURCE_NAME = 'AdverseEvent.mitigatingAction';

    protected FHIRReference|FHIRCodeableConcept|null $item = null;

    public function getItem(): FHIRReference|FHIRCodeableConcept|null
    {
        return $this->item;
    }

    public function setItem(FHIRReference|FHIRCodeableConcept|null $value): self
    {
        $this->item = $value;

        return $this;
    }
}
