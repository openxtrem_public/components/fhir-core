<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestReportTeardownAction Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestReportTeardownActionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;

class FHIRTestReportTeardownAction extends FHIRBackboneElement implements FHIRTestReportTeardownActionInterface
{
    public const RESOURCE_NAME = 'TestReport.teardown.action';

    protected ?FHIRTestReportSetupActionOperation $operation = null;

    public function getOperation(): ?FHIRTestReportSetupActionOperation
    {
        return $this->operation;
    }

    public function setOperation(?FHIRTestReportSetupActionOperation $value): self
    {
        $this->operation = $value;

        return $this;
    }
}
