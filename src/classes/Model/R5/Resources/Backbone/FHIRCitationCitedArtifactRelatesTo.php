<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CitationCitedArtifactRelatesTo Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCitationCitedArtifactRelatesToInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRCitationCitedArtifactRelatesTo extends FHIRBackboneElement implements FHIRCitationCitedArtifactRelatesToInterface
{
    public const RESOURCE_NAME = 'Citation.citedArtifact.relatesTo';

    protected ?FHIRCode $type = null;

    /** @var FHIRCodeableConcept[] */
    protected array $classifier = [];
    protected ?FHIRString $label = null;
    protected ?FHIRString $display = null;
    protected ?FHIRMarkdown $citation = null;
    protected ?FHIRAttachment $document = null;
    protected ?FHIRCanonical $resource = null;
    protected ?FHIRReference $resourceReference = null;

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getClassifier(): array
    {
        return $this->classifier;
    }

    public function setClassifier(?FHIRCodeableConcept ...$value): self
    {
        $this->classifier = array_filter($value);

        return $this;
    }

    public function addClassifier(?FHIRCodeableConcept ...$value): self
    {
        $this->classifier = array_filter(array_merge($this->classifier, $value));

        return $this;
    }

    public function getLabel(): ?FHIRString
    {
        return $this->label;
    }

    public function setLabel(string|FHIRString|null $value): self
    {
        $this->label = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDisplay(): ?FHIRString
    {
        return $this->display;
    }

    public function setDisplay(string|FHIRString|null $value): self
    {
        $this->display = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCitation(): ?FHIRMarkdown
    {
        return $this->citation;
    }

    public function setCitation(string|FHIRMarkdown|null $value): self
    {
        $this->citation = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getDocument(): ?FHIRAttachment
    {
        return $this->document;
    }

    public function setDocument(?FHIRAttachment $value): self
    {
        $this->document = $value;

        return $this;
    }

    public function getResource(): ?FHIRCanonical
    {
        return $this->resource;
    }

    public function setResource(string|FHIRCanonical|null $value): self
    {
        $this->resource = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    public function getResourceReference(): ?FHIRReference
    {
        return $this->resourceReference;
    }

    public function setResourceReference(?FHIRReference $value): self
    {
        $this->resourceReference = $value;

        return $this;
    }
}
