<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MeasureReportGroupStratifier Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMeasureReportGroupStratifierInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRMeasureReportGroupStratifier extends FHIRBackboneElement implements FHIRMeasureReportGroupStratifierInterface
{
    public const RESOURCE_NAME = 'MeasureReport.group.stratifier';

    protected ?FHIRString $linkId = null;
    protected ?FHIRCodeableConcept $code = null;

    /** @var FHIRMeasureReportGroupStratifierStratum[] */
    protected array $stratum = [];

    public function getLinkId(): ?FHIRString
    {
        return $this->linkId;
    }

    public function setLinkId(string|FHIRString|null $value): self
    {
        $this->linkId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * @return FHIRMeasureReportGroupStratifierStratum[]
     */
    public function getStratum(): array
    {
        return $this->stratum;
    }

    public function setStratum(?FHIRMeasureReportGroupStratifierStratum ...$value): self
    {
        $this->stratum = array_filter($value);

        return $this;
    }

    public function addStratum(?FHIRMeasureReportGroupStratifierStratum ...$value): self
    {
        $this->stratum = array_filter(array_merge($this->stratum, $value));

        return $this;
    }
}
