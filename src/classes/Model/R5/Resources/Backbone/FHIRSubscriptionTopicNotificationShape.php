<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubscriptionTopicNotificationShape Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubscriptionTopicNotificationShapeInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;

class FHIRSubscriptionTopicNotificationShape extends FHIRBackboneElement implements FHIRSubscriptionTopicNotificationShapeInterface
{
    public const RESOURCE_NAME = 'SubscriptionTopic.notificationShape';

    protected ?FHIRUri $resource = null;

    /** @var FHIRString[] */
    protected array $include = [];

    /** @var FHIRString[] */
    protected array $revInclude = [];

    public function getResource(): ?FHIRUri
    {
        return $this->resource;
    }

    public function setResource(string|FHIRUri|null $value): self
    {
        $this->resource = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getInclude(): array
    {
        return $this->include;
    }

    public function setInclude(string|FHIRString|null ...$value): self
    {
        $this->include = [];
        $this->addInclude(...$value);

        return $this;
    }

    public function addInclude(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->include = array_filter(array_merge($this->include, $values));

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getRevInclude(): array
    {
        return $this->revInclude;
    }

    public function setRevInclude(string|FHIRString|null ...$value): self
    {
        $this->revInclude = [];
        $this->addRevInclude(...$value);

        return $this;
    }

    public function addRevInclude(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->revInclude = array_filter(array_merge($this->revInclude, $values));

        return $this;
    }
}
