<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ServiceRequestOrderDetail Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRServiceRequestOrderDetailInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;

class FHIRServiceRequestOrderDetail extends FHIRBackboneElement implements FHIRServiceRequestOrderDetailInterface
{
    public const RESOURCE_NAME = 'ServiceRequest.orderDetail';

    protected ?FHIRCodeableReference $parameterFocus = null;

    /** @var FHIRServiceRequestOrderDetailParameter[] */
    protected array $parameter = [];

    public function getParameterFocus(): ?FHIRCodeableReference
    {
        return $this->parameterFocus;
    }

    public function setParameterFocus(?FHIRCodeableReference $value): self
    {
        $this->parameterFocus = $value;

        return $this;
    }

    /**
     * @return FHIRServiceRequestOrderDetailParameter[]
     */
    public function getParameter(): array
    {
        return $this->parameter;
    }

    public function setParameter(?FHIRServiceRequestOrderDetailParameter ...$value): self
    {
        $this->parameter = array_filter($value);

        return $this;
    }

    public function addParameter(?FHIRServiceRequestOrderDetailParameter ...$value): self
    {
        $this->parameter = array_filter(array_merge($this->parameter, $value));

        return $this;
    }
}
