<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PlanDefinitionActionRelatedAction Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRPlanDefinitionActionRelatedActionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRId;

class FHIRPlanDefinitionActionRelatedAction extends FHIRBackboneElement implements FHIRPlanDefinitionActionRelatedActionInterface
{
    public const RESOURCE_NAME = 'PlanDefinition.action.relatedAction';

    protected ?FHIRId $targetId = null;
    protected ?FHIRCode $relationship = null;
    protected ?FHIRCode $endRelationship = null;
    protected FHIRDuration|FHIRRange|null $offset = null;

    public function getTargetId(): ?FHIRId
    {
        return $this->targetId;
    }

    public function setTargetId(string|FHIRId|null $value): self
    {
        $this->targetId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getRelationship(): ?FHIRCode
    {
        return $this->relationship;
    }

    public function setRelationship(string|FHIRCode|null $value): self
    {
        $this->relationship = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getEndRelationship(): ?FHIRCode
    {
        return $this->endRelationship;
    }

    public function setEndRelationship(string|FHIRCode|null $value): self
    {
        $this->endRelationship = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getOffset(): FHIRDuration|FHIRRange|null
    {
        return $this->offset;
    }

    public function setOffset(FHIRDuration|FHIRRange|null $value): self
    {
        $this->offset = $value;

        return $this;
    }
}
