<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductDefinitionCrossReference Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductDefinitionCrossReferenceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;

class FHIRMedicinalProductDefinitionCrossReference extends FHIRBackboneElement implements FHIRMedicinalProductDefinitionCrossReferenceInterface
{
    public const RESOURCE_NAME = 'MedicinalProductDefinition.crossReference';

    protected ?FHIRCodeableReference $product = null;
    protected ?FHIRCodeableConcept $type = null;

    public function getProduct(): ?FHIRCodeableReference
    {
        return $this->product;
    }

    public function setProduct(?FHIRCodeableReference $value): self
    {
        $this->product = $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }
}
