<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EvidenceStatisticModelCharacteristicVariable Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREvidenceStatisticModelCharacteristicVariableInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;

class FHIREvidenceStatisticModelCharacteristicVariable extends FHIRBackboneElement implements FHIREvidenceStatisticModelCharacteristicVariableInterface
{
    public const RESOURCE_NAME = 'Evidence.statistic.modelCharacteristic.variable';

    protected ?FHIRReference $variableDefinition = null;
    protected ?FHIRCode $handling = null;

    /** @var FHIRCodeableConcept[] */
    protected array $valueCategory = [];

    /** @var FHIRQuantity[] */
    protected array $valueQuantity = [];

    /** @var FHIRRange[] */
    protected array $valueRange = [];

    public function getVariableDefinition(): ?FHIRReference
    {
        return $this->variableDefinition;
    }

    public function setVariableDefinition(?FHIRReference $value): self
    {
        $this->variableDefinition = $value;

        return $this;
    }

    public function getHandling(): ?FHIRCode
    {
        return $this->handling;
    }

    public function setHandling(string|FHIRCode|null $value): self
    {
        $this->handling = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getValueCategory(): array
    {
        return $this->valueCategory;
    }

    public function setValueCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->valueCategory = array_filter($value);

        return $this;
    }

    public function addValueCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->valueCategory = array_filter(array_merge($this->valueCategory, $value));

        return $this;
    }

    /**
     * @return FHIRQuantity[]
     */
    public function getValueQuantity(): array
    {
        return $this->valueQuantity;
    }

    public function setValueQuantity(?FHIRQuantity ...$value): self
    {
        $this->valueQuantity = array_filter($value);

        return $this;
    }

    public function addValueQuantity(?FHIRQuantity ...$value): self
    {
        $this->valueQuantity = array_filter(array_merge($this->valueQuantity, $value));

        return $this;
    }

    /**
     * @return FHIRRange[]
     */
    public function getValueRange(): array
    {
        return $this->valueRange;
    }

    public function setValueRange(?FHIRRange ...$value): self
    {
        $this->valueRange = array_filter($value);

        return $this;
    }

    public function addValueRange(?FHIRRange ...$value): self
    {
        $this->valueRange = array_filter(array_merge($this->valueRange, $value));

        return $this;
    }
}
