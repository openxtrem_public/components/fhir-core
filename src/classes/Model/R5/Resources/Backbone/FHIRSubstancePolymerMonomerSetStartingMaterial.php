<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstancePolymerMonomerSetStartingMaterial Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstancePolymerMonomerSetStartingMaterialInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;

class FHIRSubstancePolymerMonomerSetStartingMaterial extends FHIRBackboneElement implements FHIRSubstancePolymerMonomerSetStartingMaterialInterface
{
    public const RESOURCE_NAME = 'SubstancePolymer.monomerSet.startingMaterial';

    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRCodeableConcept $category = null;
    protected ?FHIRBoolean $isDefining = null;
    protected ?FHIRQuantity $amount = null;

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getIsDefining(): ?FHIRBoolean
    {
        return $this->isDefining;
    }

    public function setIsDefining(bool|FHIRBoolean|null $value): self
    {
        $this->isDefining = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getAmount(): ?FHIRQuantity
    {
        return $this->amount;
    }

    public function setAmount(?FHIRQuantity $value): self
    {
        $this->amount = $value;

        return $this;
    }
}
