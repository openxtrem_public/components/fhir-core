<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EvidenceStatisticSampleSize Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREvidenceStatisticSampleSizeInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUnsignedInt;

class FHIREvidenceStatisticSampleSize extends FHIRBackboneElement implements FHIREvidenceStatisticSampleSizeInterface
{
    public const RESOURCE_NAME = 'Evidence.statistic.sampleSize';

    protected ?FHIRMarkdown $description = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];
    protected ?FHIRUnsignedInt $numberOfStudies = null;
    protected ?FHIRUnsignedInt $numberOfParticipants = null;
    protected ?FHIRUnsignedInt $knownDataCount = null;

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    public function getNumberOfStudies(): ?FHIRUnsignedInt
    {
        return $this->numberOfStudies;
    }

    public function setNumberOfStudies(int|FHIRUnsignedInt|null $value): self
    {
        $this->numberOfStudies = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getNumberOfParticipants(): ?FHIRUnsignedInt
    {
        return $this->numberOfParticipants;
    }

    public function setNumberOfParticipants(int|FHIRUnsignedInt|null $value): self
    {
        $this->numberOfParticipants = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getKnownDataCount(): ?FHIRUnsignedInt
    {
        return $this->knownDataCount;
    }

    public function setKnownDataCount(int|FHIRUnsignedInt|null $value): self
    {
        $this->knownDataCount = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }
}
