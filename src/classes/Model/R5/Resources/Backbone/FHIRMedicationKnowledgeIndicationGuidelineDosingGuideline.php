<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledgeIndicationGuidelineDosingGuideline Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationKnowledgeIndicationGuidelineDosingGuidelineInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;

class FHIRMedicationKnowledgeIndicationGuidelineDosingGuideline extends FHIRBackboneElement implements FHIRMedicationKnowledgeIndicationGuidelineDosingGuidelineInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge.indicationGuideline.dosingGuideline';

    protected ?FHIRCodeableConcept $treatmentIntent = null;

    /** @var FHIRMedicationKnowledgeIndicationGuidelineDosingGuidelineDosage[] */
    protected array $dosage = [];
    protected ?FHIRCodeableConcept $administrationTreatment = null;

    /** @var FHIRMedicationKnowledgeIndicationGuidelineDosingGuidelinePatientCharacteristic[] */
    protected array $patientCharacteristic = [];

    public function getTreatmentIntent(): ?FHIRCodeableConcept
    {
        return $this->treatmentIntent;
    }

    public function setTreatmentIntent(?FHIRCodeableConcept $value): self
    {
        $this->treatmentIntent = $value;

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeIndicationGuidelineDosingGuidelineDosage[]
     */
    public function getDosage(): array
    {
        return $this->dosage;
    }

    public function setDosage(?FHIRMedicationKnowledgeIndicationGuidelineDosingGuidelineDosage ...$value): self
    {
        $this->dosage = array_filter($value);

        return $this;
    }

    public function addDosage(?FHIRMedicationKnowledgeIndicationGuidelineDosingGuidelineDosage ...$value): self
    {
        $this->dosage = array_filter(array_merge($this->dosage, $value));

        return $this;
    }

    public function getAdministrationTreatment(): ?FHIRCodeableConcept
    {
        return $this->administrationTreatment;
    }

    public function setAdministrationTreatment(?FHIRCodeableConcept $value): self
    {
        $this->administrationTreatment = $value;

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeIndicationGuidelineDosingGuidelinePatientCharacteristic[]
     */
    public function getPatientCharacteristic(): array
    {
        return $this->patientCharacteristic;
    }

    public function setPatientCharacteristic(
        ?FHIRMedicationKnowledgeIndicationGuidelineDosingGuidelinePatientCharacteristic ...$value,
    ): self
    {
        $this->patientCharacteristic = array_filter($value);

        return $this;
    }

    public function addPatientCharacteristic(
        ?FHIRMedicationKnowledgeIndicationGuidelineDosingGuidelinePatientCharacteristic ...$value,
    ): self
    {
        $this->patientCharacteristic = array_filter(array_merge($this->patientCharacteristic, $value));

        return $this;
    }
}
