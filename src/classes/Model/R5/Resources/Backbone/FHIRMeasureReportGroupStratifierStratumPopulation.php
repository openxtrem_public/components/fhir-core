<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MeasureReportGroupStratifierStratumPopulation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMeasureReportGroupStratifierStratumPopulationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRMeasureReportGroupStratifierStratumPopulation extends FHIRBackboneElement implements FHIRMeasureReportGroupStratifierStratumPopulationInterface
{
    public const RESOURCE_NAME = 'MeasureReport.group.stratifier.stratum.population';

    protected ?FHIRString $linkId = null;
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRInteger $count = null;
    protected ?FHIRReference $subjectResults = null;

    /** @var FHIRReference[] */
    protected array $subjectReport = [];
    protected ?FHIRReference $subjects = null;

    public function getLinkId(): ?FHIRString
    {
        return $this->linkId;
    }

    public function setLinkId(string|FHIRString|null $value): self
    {
        $this->linkId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getCount(): ?FHIRInteger
    {
        return $this->count;
    }

    public function setCount(int|FHIRInteger|null $value): self
    {
        $this->count = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getSubjectResults(): ?FHIRReference
    {
        return $this->subjectResults;
    }

    public function setSubjectResults(?FHIRReference $value): self
    {
        $this->subjectResults = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSubjectReport(): array
    {
        return $this->subjectReport;
    }

    public function setSubjectReport(?FHIRReference ...$value): self
    {
        $this->subjectReport = array_filter($value);

        return $this;
    }

    public function addSubjectReport(?FHIRReference ...$value): self
    {
        $this->subjectReport = array_filter(array_merge($this->subjectReport, $value));

        return $this;
    }

    public function getSubjects(): ?FHIRReference
    {
        return $this->subjects;
    }

    public function setSubjects(?FHIRReference $value): self
    {
        $this->subjects = $value;

        return $this;
    }
}
