<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CitationCitedArtifactClassification Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCitationCitedArtifactClassificationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRCitationCitedArtifactClassification extends FHIRBackboneElement implements FHIRCitationCitedArtifactClassificationInterface
{
    public const RESOURCE_NAME = 'Citation.citedArtifact.classification';

    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRCodeableConcept[] */
    protected array $classifier = [];

    /** @var FHIRReference[] */
    protected array $artifactAssessment = [];

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getClassifier(): array
    {
        return $this->classifier;
    }

    public function setClassifier(?FHIRCodeableConcept ...$value): self
    {
        $this->classifier = array_filter($value);

        return $this;
    }

    public function addClassifier(?FHIRCodeableConcept ...$value): self
    {
        $this->classifier = array_filter(array_merge($this->classifier, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getArtifactAssessment(): array
    {
        return $this->artifactAssessment;
    }

    public function setArtifactAssessment(?FHIRReference ...$value): self
    {
        $this->artifactAssessment = array_filter($value);

        return $this;
    }

    public function addArtifactAssessment(?FHIRReference ...$value): self
    {
        $this->artifactAssessment = array_filter(array_merge($this->artifactAssessment, $value));

        return $this;
    }
}
