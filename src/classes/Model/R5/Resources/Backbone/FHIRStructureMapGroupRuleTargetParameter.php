<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR StructureMapGroupRuleTargetParameter Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRStructureMapGroupRuleTargetParameterInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRElement;

class FHIRStructureMapGroupRuleTargetParameter extends FHIRBackboneElement implements FHIRStructureMapGroupRuleTargetParameterInterface
{
    public const RESOURCE_NAME = 'StructureMap.group.rule.target.parameter';

    protected ?FHIRElement $value = null;

    public function getValue(): ?FHIRElement
    {
        return $this->value;
    }

    public function setValue(?FHIRElement $value): self
    {
        $this->value = $value;

        return $this;
    }
}
