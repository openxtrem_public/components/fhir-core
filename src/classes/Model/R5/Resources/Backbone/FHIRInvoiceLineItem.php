<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR InvoiceLineItem Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRInvoiceLineItemInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRMonetaryComponent;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;

class FHIRInvoiceLineItem extends FHIRBackboneElement implements FHIRInvoiceLineItemInterface
{
    public const RESOURCE_NAME = 'Invoice.lineItem';

    protected ?FHIRPositiveInt $sequence = null;
    protected FHIRDate|FHIRPeriod|null $serviced = null;
    protected FHIRReference|FHIRCodeableConcept|null $chargeItem = null;

    /** @var FHIRMonetaryComponent[] */
    protected array $priceComponent = [];

    public function getSequence(): ?FHIRPositiveInt
    {
        return $this->sequence;
    }

    public function setSequence(int|FHIRPositiveInt|null $value): self
    {
        $this->sequence = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getServiced(): FHIRDate|FHIRPeriod|null
    {
        return $this->serviced;
    }

    public function setServiced(FHIRDate|FHIRPeriod|null $value): self
    {
        $this->serviced = $value;

        return $this;
    }

    public function getChargeItem(): FHIRReference|FHIRCodeableConcept|null
    {
        return $this->chargeItem;
    }

    public function setChargeItem(FHIRReference|FHIRCodeableConcept|null $value): self
    {
        $this->chargeItem = $value;

        return $this;
    }

    /**
     * @return FHIRMonetaryComponent[]
     */
    public function getPriceComponent(): array
    {
        return $this->priceComponent;
    }

    public function setPriceComponent(?FHIRMonetaryComponent ...$value): self
    {
        $this->priceComponent = array_filter($value);

        return $this;
    }

    public function addPriceComponent(?FHIRMonetaryComponent ...$value): self
    {
        $this->priceComponent = array_filter(array_merge($this->priceComponent, $value));

        return $this;
    }
}
