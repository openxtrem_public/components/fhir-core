<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ChargeItemDefinitionPropertyGroup Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRChargeItemDefinitionPropertyGroupInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRMonetaryComponent;

class FHIRChargeItemDefinitionPropertyGroup extends FHIRBackboneElement implements FHIRChargeItemDefinitionPropertyGroupInterface
{
    public const RESOURCE_NAME = 'ChargeItemDefinition.propertyGroup';

    /** @var FHIRChargeItemDefinitionApplicability[] */
    protected array $applicability = [];

    /** @var FHIRMonetaryComponent[] */
    protected array $priceComponent = [];

    /**
     * @return FHIRChargeItemDefinitionApplicability[]
     */
    public function getApplicability(): array
    {
        return $this->applicability;
    }

    public function setApplicability(?FHIRChargeItemDefinitionApplicability ...$value): self
    {
        $this->applicability = array_filter($value);

        return $this;
    }

    public function addApplicability(?FHIRChargeItemDefinitionApplicability ...$value): self
    {
        $this->applicability = array_filter(array_merge($this->applicability, $value));

        return $this;
    }

    /**
     * @return FHIRMonetaryComponent[]
     */
    public function getPriceComponent(): array
    {
        return $this->priceComponent;
    }

    public function setPriceComponent(?FHIRMonetaryComponent ...$value): self
    {
        $this->priceComponent = array_filter($value);

        return $this;
    }

    public function addPriceComponent(?FHIRMonetaryComponent ...$value): self
    {
        $this->priceComponent = array_filter(array_merge($this->priceComponent, $value));

        return $this;
    }
}
