<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EncounterReason Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREncounterReasonInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;

class FHIREncounterReason extends FHIRBackboneElement implements FHIREncounterReasonInterface
{
    public const RESOURCE_NAME = 'Encounter.reason';

    /** @var FHIRCodeableConcept[] */
    protected array $use = [];

    /** @var FHIRCodeableReference[] */
    protected array $value = [];

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getUse(): array
    {
        return $this->use;
    }

    public function setUse(?FHIRCodeableConcept ...$value): self
    {
        $this->use = array_filter($value);

        return $this;
    }

    public function addUse(?FHIRCodeableConcept ...$value): self
    {
        $this->use = array_filter(array_merge($this->use, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getValue(): array
    {
        return $this->value;
    }

    public function setValue(?FHIRCodeableReference ...$value): self
    {
        $this->value = array_filter($value);

        return $this;
    }

    public function addValue(?FHIRCodeableReference ...$value): self
    {
        $this->value = array_filter(array_merge($this->value, $value));

        return $this;
    }
}
