<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ConceptMapGroupElementTarget Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRConceptMapGroupElementTargetInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRConceptMapGroupElementTarget extends FHIRBackboneElement implements FHIRConceptMapGroupElementTargetInterface
{
    public const RESOURCE_NAME = 'ConceptMap.group.element.target';

    protected ?FHIRCode $code = null;
    protected ?FHIRString $display = null;
    protected ?FHIRCanonical $valueSet = null;
    protected ?FHIRCode $relationship = null;
    protected ?FHIRString $comment = null;

    /** @var FHIRConceptMapGroupElementTargetProperty[] */
    protected array $property = [];

    /** @var FHIRConceptMapGroupElementTargetDependsOn[] */
    protected array $dependsOn = [];

    /** @var FHIRConceptMapGroupElementTargetDependsOn[] */
    protected array $product = [];

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDisplay(): ?FHIRString
    {
        return $this->display;
    }

    public function setDisplay(string|FHIRString|null $value): self
    {
        $this->display = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getValueSet(): ?FHIRCanonical
    {
        return $this->valueSet;
    }

    public function setValueSet(string|FHIRCanonical|null $value): self
    {
        $this->valueSet = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    public function getRelationship(): ?FHIRCode
    {
        return $this->relationship;
    }

    public function setRelationship(string|FHIRCode|null $value): self
    {
        $this->relationship = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getComment(): ?FHIRString
    {
        return $this->comment;
    }

    public function setComment(string|FHIRString|null $value): self
    {
        $this->comment = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRConceptMapGroupElementTargetProperty[]
     */
    public function getProperty(): array
    {
        return $this->property;
    }

    public function setProperty(?FHIRConceptMapGroupElementTargetProperty ...$value): self
    {
        $this->property = array_filter($value);

        return $this;
    }

    public function addProperty(?FHIRConceptMapGroupElementTargetProperty ...$value): self
    {
        $this->property = array_filter(array_merge($this->property, $value));

        return $this;
    }

    /**
     * @return FHIRConceptMapGroupElementTargetDependsOn[]
     */
    public function getDependsOn(): array
    {
        return $this->dependsOn;
    }

    public function setDependsOn(?FHIRConceptMapGroupElementTargetDependsOn ...$value): self
    {
        $this->dependsOn = array_filter($value);

        return $this;
    }

    public function addDependsOn(?FHIRConceptMapGroupElementTargetDependsOn ...$value): self
    {
        $this->dependsOn = array_filter(array_merge($this->dependsOn, $value));

        return $this;
    }

    /**
     * @return FHIRConceptMapGroupElementTargetDependsOn[]
     */
    public function getProduct(): array
    {
        return $this->product;
    }

    public function setProduct(?FHIRConceptMapGroupElementTargetDependsOn ...$value): self
    {
        $this->product = array_filter($value);

        return $this;
    }

    public function addProduct(?FHIRConceptMapGroupElementTargetDependsOn ...$value): self
    {
        $this->product = array_filter(array_merge($this->product, $value));

        return $this;
    }
}
