<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationIngredient Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationIngredientInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRatio;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;

class FHIRMedicationIngredient extends FHIRBackboneElement implements FHIRMedicationIngredientInterface
{
    public const RESOURCE_NAME = 'Medication.ingredient';

    protected ?FHIRCodeableReference $item = null;
    protected ?FHIRBoolean $isActive = null;
    protected FHIRRatio|FHIRCodeableConcept|FHIRQuantity|null $strength = null;

    public function getItem(): ?FHIRCodeableReference
    {
        return $this->item;
    }

    public function setItem(?FHIRCodeableReference $value): self
    {
        $this->item = $value;

        return $this;
    }

    public function getIsActive(): ?FHIRBoolean
    {
        return $this->isActive;
    }

    public function setIsActive(bool|FHIRBoolean|null $value): self
    {
        $this->isActive = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getStrength(): FHIRRatio|FHIRCodeableConcept|FHIRQuantity|null
    {
        return $this->strength;
    }

    public function setStrength(FHIRRatio|FHIRCodeableConcept|FHIRQuantity|null $value): self
    {
        $this->strength = $value;

        return $this;
    }
}
