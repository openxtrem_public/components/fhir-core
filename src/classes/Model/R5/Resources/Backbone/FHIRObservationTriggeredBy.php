<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ObservationTriggeredBy Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRObservationTriggeredByInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRObservationTriggeredBy extends FHIRBackboneElement implements FHIRObservationTriggeredByInterface
{
    public const RESOURCE_NAME = 'Observation.triggeredBy';

    protected ?FHIRReference $observation = null;
    protected ?FHIRCode $type = null;
    protected ?FHIRString $reason = null;

    public function getObservation(): ?FHIRReference
    {
        return $this->observation;
    }

    public function setObservation(?FHIRReference $value): self
    {
        $this->observation = $value;

        return $this;
    }

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getReason(): ?FHIRString
    {
        return $this->reason;
    }

    public function setReason(string|FHIRString|null $value): self
    {
        $this->reason = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
