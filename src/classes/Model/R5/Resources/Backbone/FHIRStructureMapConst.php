<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR StructureMapConst Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRStructureMapConstInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRStructureMapConst extends FHIRBackboneElement implements FHIRStructureMapConstInterface
{
    public const RESOURCE_NAME = 'StructureMap.const';

    protected ?FHIRId $name = null;
    protected ?FHIRString $value = null;

    public function getName(): ?FHIRId
    {
        return $this->name;
    }

    public function setName(string|FHIRId|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getValue(): ?FHIRString
    {
        return $this->value;
    }

    public function setValue(string|FHIRString|null $value): self
    {
        $this->value = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
