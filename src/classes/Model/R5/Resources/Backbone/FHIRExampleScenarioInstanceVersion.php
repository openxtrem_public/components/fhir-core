<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExampleScenarioInstanceVersion Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExampleScenarioInstanceVersionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRExampleScenarioInstanceVersion extends FHIRBackboneElement implements FHIRExampleScenarioInstanceVersionInterface
{
    public const RESOURCE_NAME = 'ExampleScenario.instance.version';

    protected ?FHIRString $key = null;
    protected ?FHIRString $title = null;
    protected ?FHIRMarkdown $description = null;
    protected ?FHIRReference $content = null;

    public function getKey(): ?FHIRString
    {
        return $this->key;
    }

    public function setKey(string|FHIRString|null $value): self
    {
        $this->key = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getContent(): ?FHIRReference
    {
        return $this->content;
    }

    public function setContent(?FHIRReference $value): self
    {
        $this->content = $value;

        return $this;
    }
}
