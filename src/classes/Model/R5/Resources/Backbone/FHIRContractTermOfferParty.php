<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ContractTermOfferParty Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRContractTermOfferPartyInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRContractTermOfferParty extends FHIRBackboneElement implements FHIRContractTermOfferPartyInterface
{
    public const RESOURCE_NAME = 'Contract.term.offer.party';

    /** @var FHIRReference[] */
    protected array $reference = [];
    protected ?FHIRCodeableConcept $role = null;

    /**
     * @return FHIRReference[]
     */
    public function getReference(): array
    {
        return $this->reference;
    }

    public function setReference(?FHIRReference ...$value): self
    {
        $this->reference = array_filter($value);

        return $this;
    }

    public function addReference(?FHIRReference ...$value): self
    {
        $this->reference = array_filter(array_merge($this->reference, $value));

        return $this;
    }

    public function getRole(): ?FHIRCodeableConcept
    {
        return $this->role;
    }

    public function setRole(?FHIRCodeableConcept $value): self
    {
        $this->role = $value;

        return $this;
    }
}
