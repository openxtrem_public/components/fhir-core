<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CompositionAttester Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCompositionAttesterInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;

class FHIRCompositionAttester extends FHIRBackboneElement implements FHIRCompositionAttesterInterface
{
    public const RESOURCE_NAME = 'Composition.attester';

    protected ?FHIRCodeableConcept $mode = null;
    protected ?FHIRDateTime $time = null;
    protected ?FHIRReference $party = null;

    public function getMode(): ?FHIRCodeableConcept
    {
        return $this->mode;
    }

    public function setMode(?FHIRCodeableConcept $value): self
    {
        $this->mode = $value;

        return $this;
    }

    public function getTime(): ?FHIRDateTime
    {
        return $this->time;
    }

    public function setTime(string|FHIRDateTime|null $value): self
    {
        $this->time = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getParty(): ?FHIRReference
    {
        return $this->party;
    }

    public function setParty(?FHIRReference $value): self
    {
        $this->party = $value;

        return $this;
    }
}
