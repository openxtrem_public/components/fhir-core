<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ServiceRequestPatientInstruction Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRServiceRequestPatientInstructionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;

class FHIRServiceRequestPatientInstruction extends FHIRBackboneElement implements FHIRServiceRequestPatientInstructionInterface
{
    public const RESOURCE_NAME = 'ServiceRequest.patientInstruction';

    protected FHIRMarkdown|FHIRReference|null $instruction = null;

    public function getInstruction(): FHIRMarkdown|FHIRReference|null
    {
        return $this->instruction;
    }

    public function setInstruction(FHIRMarkdown|FHIRReference|null $value): self
    {
        $this->instruction = $value;

        return $this;
    }
}
