<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceDefinitionHasPart Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceDefinitionHasPartInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInteger;

class FHIRDeviceDefinitionHasPart extends FHIRBackboneElement implements FHIRDeviceDefinitionHasPartInterface
{
    public const RESOURCE_NAME = 'DeviceDefinition.hasPart';

    protected ?FHIRReference $reference = null;
    protected ?FHIRInteger $count = null;

    public function getReference(): ?FHIRReference
    {
        return $this->reference;
    }

    public function setReference(?FHIRReference $value): self
    {
        $this->reference = $value;

        return $this;
    }

    public function getCount(): ?FHIRInteger
    {
        return $this->count;
    }

    public function setCount(int|FHIRInteger|null $value): self
    {
        $this->count = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }
}
