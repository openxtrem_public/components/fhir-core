<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MeasureGroupStratifier Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMeasureGroupStratifierInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRExpression;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRMeasureGroupStratifier extends FHIRBackboneElement implements FHIRMeasureGroupStratifierInterface
{
    public const RESOURCE_NAME = 'Measure.group.stratifier';

    protected ?FHIRString $linkId = null;
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRMarkdown $description = null;
    protected ?FHIRExpression $criteria = null;
    protected ?FHIRReference $groupDefinition = null;

    /** @var FHIRMeasureGroupStratifierComponent[] */
    protected array $component = [];

    public function getLinkId(): ?FHIRString
    {
        return $this->linkId;
    }

    public function setLinkId(string|FHIRString|null $value): self
    {
        $this->linkId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getCriteria(): ?FHIRExpression
    {
        return $this->criteria;
    }

    public function setCriteria(?FHIRExpression $value): self
    {
        $this->criteria = $value;

        return $this;
    }

    public function getGroupDefinition(): ?FHIRReference
    {
        return $this->groupDefinition;
    }

    public function setGroupDefinition(?FHIRReference $value): self
    {
        $this->groupDefinition = $value;

        return $this;
    }

    /**
     * @return FHIRMeasureGroupStratifierComponent[]
     */
    public function getComponent(): array
    {
        return $this->component;
    }

    public function setComponent(?FHIRMeasureGroupStratifierComponent ...$value): self
    {
        $this->component = array_filter($value);

        return $this;
    }

    public function addComponent(?FHIRMeasureGroupStratifierComponent ...$value): self
    {
        $this->component = array_filter(array_merge($this->component, $value));

        return $this;
    }
}
