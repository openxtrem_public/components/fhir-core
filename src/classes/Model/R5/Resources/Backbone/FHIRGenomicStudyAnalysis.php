<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR GenomicStudyAnalysis Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRGenomicStudyAnalysisInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;

class FHIRGenomicStudyAnalysis extends FHIRBackboneElement implements FHIRGenomicStudyAnalysisInterface
{
    public const RESOURCE_NAME = 'GenomicStudy.analysis';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRCodeableConcept[] */
    protected array $methodType = [];

    /** @var FHIRCodeableConcept[] */
    protected array $changeType = [];
    protected ?FHIRCodeableConcept $genomeBuild = null;
    protected ?FHIRCanonical $instantiatesCanonical = null;
    protected ?FHIRUri $instantiatesUri = null;
    protected ?FHIRString $title = null;

    /** @var FHIRReference[] */
    protected array $focus = [];

    /** @var FHIRReference[] */
    protected array $specimen = [];
    protected ?FHIRDateTime $date = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];
    protected ?FHIRReference $protocolPerformed = null;

    /** @var FHIRReference[] */
    protected array $regionsStudied = [];

    /** @var FHIRReference[] */
    protected array $regionsCalled = [];

    /** @var FHIRGenomicStudyAnalysisInput[] */
    protected array $input = [];

    /** @var FHIRGenomicStudyAnalysisOutput[] */
    protected array $output = [];

    /** @var FHIRGenomicStudyAnalysisPerformer[] */
    protected array $performer = [];

    /** @var FHIRGenomicStudyAnalysisDevice[] */
    protected array $device = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getMethodType(): array
    {
        return $this->methodType;
    }

    public function setMethodType(?FHIRCodeableConcept ...$value): self
    {
        $this->methodType = array_filter($value);

        return $this;
    }

    public function addMethodType(?FHIRCodeableConcept ...$value): self
    {
        $this->methodType = array_filter(array_merge($this->methodType, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getChangeType(): array
    {
        return $this->changeType;
    }

    public function setChangeType(?FHIRCodeableConcept ...$value): self
    {
        $this->changeType = array_filter($value);

        return $this;
    }

    public function addChangeType(?FHIRCodeableConcept ...$value): self
    {
        $this->changeType = array_filter(array_merge($this->changeType, $value));

        return $this;
    }

    public function getGenomeBuild(): ?FHIRCodeableConcept
    {
        return $this->genomeBuild;
    }

    public function setGenomeBuild(?FHIRCodeableConcept $value): self
    {
        $this->genomeBuild = $value;

        return $this;
    }

    public function getInstantiatesCanonical(): ?FHIRCanonical
    {
        return $this->instantiatesCanonical;
    }

    public function setInstantiatesCanonical(string|FHIRCanonical|null $value): self
    {
        $this->instantiatesCanonical = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    public function getInstantiatesUri(): ?FHIRUri
    {
        return $this->instantiatesUri;
    }

    public function setInstantiatesUri(string|FHIRUri|null $value): self
    {
        $this->instantiatesUri = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getFocus(): array
    {
        return $this->focus;
    }

    public function setFocus(?FHIRReference ...$value): self
    {
        $this->focus = array_filter($value);

        return $this;
    }

    public function addFocus(?FHIRReference ...$value): self
    {
        $this->focus = array_filter(array_merge($this->focus, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSpecimen(): array
    {
        return $this->specimen;
    }

    public function setSpecimen(?FHIRReference ...$value): self
    {
        $this->specimen = array_filter($value);

        return $this;
    }

    public function addSpecimen(?FHIRReference ...$value): self
    {
        $this->specimen = array_filter(array_merge($this->specimen, $value));

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    public function getProtocolPerformed(): ?FHIRReference
    {
        return $this->protocolPerformed;
    }

    public function setProtocolPerformed(?FHIRReference $value): self
    {
        $this->protocolPerformed = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getRegionsStudied(): array
    {
        return $this->regionsStudied;
    }

    public function setRegionsStudied(?FHIRReference ...$value): self
    {
        $this->regionsStudied = array_filter($value);

        return $this;
    }

    public function addRegionsStudied(?FHIRReference ...$value): self
    {
        $this->regionsStudied = array_filter(array_merge($this->regionsStudied, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getRegionsCalled(): array
    {
        return $this->regionsCalled;
    }

    public function setRegionsCalled(?FHIRReference ...$value): self
    {
        $this->regionsCalled = array_filter($value);

        return $this;
    }

    public function addRegionsCalled(?FHIRReference ...$value): self
    {
        $this->regionsCalled = array_filter(array_merge($this->regionsCalled, $value));

        return $this;
    }

    /**
     * @return FHIRGenomicStudyAnalysisInput[]
     */
    public function getInput(): array
    {
        return $this->input;
    }

    public function setInput(?FHIRGenomicStudyAnalysisInput ...$value): self
    {
        $this->input = array_filter($value);

        return $this;
    }

    public function addInput(?FHIRGenomicStudyAnalysisInput ...$value): self
    {
        $this->input = array_filter(array_merge($this->input, $value));

        return $this;
    }

    /**
     * @return FHIRGenomicStudyAnalysisOutput[]
     */
    public function getOutput(): array
    {
        return $this->output;
    }

    public function setOutput(?FHIRGenomicStudyAnalysisOutput ...$value): self
    {
        $this->output = array_filter($value);

        return $this;
    }

    public function addOutput(?FHIRGenomicStudyAnalysisOutput ...$value): self
    {
        $this->output = array_filter(array_merge($this->output, $value));

        return $this;
    }

    /**
     * @return FHIRGenomicStudyAnalysisPerformer[]
     */
    public function getPerformer(): array
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRGenomicStudyAnalysisPerformer ...$value): self
    {
        $this->performer = array_filter($value);

        return $this;
    }

    public function addPerformer(?FHIRGenomicStudyAnalysisPerformer ...$value): self
    {
        $this->performer = array_filter(array_merge($this->performer, $value));

        return $this;
    }

    /**
     * @return FHIRGenomicStudyAnalysisDevice[]
     */
    public function getDevice(): array
    {
        return $this->device;
    }

    public function setDevice(?FHIRGenomicStudyAnalysisDevice ...$value): self
    {
        $this->device = array_filter($value);

        return $this;
    }

    public function addDevice(?FHIRGenomicStudyAnalysisDevice ...$value): self
    {
        $this->device = array_filter(array_merge($this->device, $value));

        return $this;
    }
}
