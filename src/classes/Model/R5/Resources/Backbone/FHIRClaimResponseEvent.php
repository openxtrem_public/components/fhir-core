<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClaimResponseEvent Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRClaimResponseEventInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;

class FHIRClaimResponseEvent extends FHIRBackboneElement implements FHIRClaimResponseEventInterface
{
    public const RESOURCE_NAME = 'ClaimResponse.event';

    protected ?FHIRCodeableConcept $type = null;
    protected FHIRDateTime|FHIRPeriod|null $when = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getWhen(): FHIRDateTime|FHIRPeriod|null
    {
        return $this->when;
    }

    public function setWhen(FHIRDateTime|FHIRPeriod|null $value): self
    {
        $this->when = $value;

        return $this;
    }
}
