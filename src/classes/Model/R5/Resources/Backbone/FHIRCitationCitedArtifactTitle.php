<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CitationCitedArtifactTitle Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCitationCitedArtifactTitleInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;

class FHIRCitationCitedArtifactTitle extends FHIRBackboneElement implements FHIRCitationCitedArtifactTitleInterface
{
    public const RESOURCE_NAME = 'Citation.citedArtifact.title';

    /** @var FHIRCodeableConcept[] */
    protected array $type = [];
    protected ?FHIRCodeableConcept $language = null;
    protected ?FHIRMarkdown $text = null;

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }

    public function getLanguage(): ?FHIRCodeableConcept
    {
        return $this->language;
    }

    public function setLanguage(?FHIRCodeableConcept $value): self
    {
        $this->language = $value;

        return $this;
    }

    public function getText(): ?FHIRMarkdown
    {
        return $this->text;
    }

    public function setText(string|FHIRMarkdown|null $value): self
    {
        $this->text = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }
}
