<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR NutritionOrderSupplementSchedule Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRNutritionOrderSupplementScheduleInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;

class FHIRNutritionOrderSupplementSchedule extends FHIRBackboneElement implements FHIRNutritionOrderSupplementScheduleInterface
{
    public const RESOURCE_NAME = 'NutritionOrder.supplement.schedule';

    /** @var FHIRTiming[] */
    protected array $timing = [];
    protected ?FHIRBoolean $asNeeded = null;
    protected ?FHIRCodeableConcept $asNeededFor = null;

    /**
     * @return FHIRTiming[]
     */
    public function getTiming(): array
    {
        return $this->timing;
    }

    public function setTiming(?FHIRTiming ...$value): self
    {
        $this->timing = array_filter($value);

        return $this;
    }

    public function addTiming(?FHIRTiming ...$value): self
    {
        $this->timing = array_filter(array_merge($this->timing, $value));

        return $this;
    }

    public function getAsNeeded(): ?FHIRBoolean
    {
        return $this->asNeeded;
    }

    public function setAsNeeded(bool|FHIRBoolean|null $value): self
    {
        $this->asNeeded = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getAsNeededFor(): ?FHIRCodeableConcept
    {
        return $this->asNeededFor;
    }

    public function setAsNeededFor(?FHIRCodeableConcept $value): self
    {
        $this->asNeededFor = $value;

        return $this;
    }
}
