<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EvidenceReportSection Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREvidenceReportSectionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRNarrative;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIREvidenceReportSection extends FHIRBackboneElement implements FHIREvidenceReportSectionInterface
{
    public const RESOURCE_NAME = 'EvidenceReport.section';

    protected ?FHIRString $title = null;
    protected ?FHIRCodeableConcept $focus = null;
    protected ?FHIRReference $focusReference = null;

    /** @var FHIRReference[] */
    protected array $author = [];
    protected ?FHIRNarrative $text = null;
    protected ?FHIRCode $mode = null;
    protected ?FHIRCodeableConcept $orderedBy = null;

    /** @var FHIRCodeableConcept[] */
    protected array $entryClassifier = [];

    /** @var FHIRReference[] */
    protected array $entryReference = [];

    /** @var FHIRQuantity[] */
    protected array $entryQuantity = [];
    protected ?FHIRCodeableConcept $emptyReason = null;

    /** @var FHIREvidenceReportSection[] */
    protected array $section = [];

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getFocus(): ?FHIRCodeableConcept
    {
        return $this->focus;
    }

    public function setFocus(?FHIRCodeableConcept $value): self
    {
        $this->focus = $value;

        return $this;
    }

    public function getFocusReference(): ?FHIRReference
    {
        return $this->focusReference;
    }

    public function setFocusReference(?FHIRReference $value): self
    {
        $this->focusReference = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAuthor(): array
    {
        return $this->author;
    }

    public function setAuthor(?FHIRReference ...$value): self
    {
        $this->author = array_filter($value);

        return $this;
    }

    public function addAuthor(?FHIRReference ...$value): self
    {
        $this->author = array_filter(array_merge($this->author, $value));

        return $this;
    }

    public function getText(): ?FHIRNarrative
    {
        return $this->text;
    }

    public function setText(?FHIRNarrative $value): self
    {
        $this->text = $value;

        return $this;
    }

    public function getMode(): ?FHIRCode
    {
        return $this->mode;
    }

    public function setMode(string|FHIRCode|null $value): self
    {
        $this->mode = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getOrderedBy(): ?FHIRCodeableConcept
    {
        return $this->orderedBy;
    }

    public function setOrderedBy(?FHIRCodeableConcept $value): self
    {
        $this->orderedBy = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getEntryClassifier(): array
    {
        return $this->entryClassifier;
    }

    public function setEntryClassifier(?FHIRCodeableConcept ...$value): self
    {
        $this->entryClassifier = array_filter($value);

        return $this;
    }

    public function addEntryClassifier(?FHIRCodeableConcept ...$value): self
    {
        $this->entryClassifier = array_filter(array_merge($this->entryClassifier, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEntryReference(): array
    {
        return $this->entryReference;
    }

    public function setEntryReference(?FHIRReference ...$value): self
    {
        $this->entryReference = array_filter($value);

        return $this;
    }

    public function addEntryReference(?FHIRReference ...$value): self
    {
        $this->entryReference = array_filter(array_merge($this->entryReference, $value));

        return $this;
    }

    /**
     * @return FHIRQuantity[]
     */
    public function getEntryQuantity(): array
    {
        return $this->entryQuantity;
    }

    public function setEntryQuantity(?FHIRQuantity ...$value): self
    {
        $this->entryQuantity = array_filter($value);

        return $this;
    }

    public function addEntryQuantity(?FHIRQuantity ...$value): self
    {
        $this->entryQuantity = array_filter(array_merge($this->entryQuantity, $value));

        return $this;
    }

    public function getEmptyReason(): ?FHIRCodeableConcept
    {
        return $this->emptyReason;
    }

    public function setEmptyReason(?FHIRCodeableConcept $value): self
    {
        $this->emptyReason = $value;

        return $this;
    }

    /**
     * @return FHIREvidenceReportSection[]
     */
    public function getSection(): array
    {
        return $this->section;
    }

    public function setSection(?FHIREvidenceReportSection ...$value): self
    {
        $this->section = array_filter($value);

        return $this;
    }

    public function addSection(?FHIREvidenceReportSection ...$value): self
    {
        $this->section = array_filter(array_merge($this->section, $value));

        return $this;
    }
}
