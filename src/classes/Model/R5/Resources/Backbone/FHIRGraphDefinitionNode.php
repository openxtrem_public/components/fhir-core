<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR GraphDefinitionNode Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRGraphDefinitionNodeInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRGraphDefinitionNode extends FHIRBackboneElement implements FHIRGraphDefinitionNodeInterface
{
    public const RESOURCE_NAME = 'GraphDefinition.node';

    protected ?FHIRId $nodeId = null;
    protected ?FHIRString $description = null;
    protected ?FHIRCode $type = null;
    protected ?FHIRCanonical $profile = null;

    public function getNodeId(): ?FHIRId
    {
        return $this->nodeId;
    }

    public function setNodeId(string|FHIRId|null $value): self
    {
        $this->nodeId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getProfile(): ?FHIRCanonical
    {
        return $this->profile;
    }

    public function setProfile(string|FHIRCanonical|null $value): self
    {
        $this->profile = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }
}
