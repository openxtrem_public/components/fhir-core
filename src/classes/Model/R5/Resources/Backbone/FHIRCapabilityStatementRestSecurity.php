<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CapabilityStatementRestSecurity Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCapabilityStatementRestSecurityInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;

class FHIRCapabilityStatementRestSecurity extends FHIRBackboneElement implements FHIRCapabilityStatementRestSecurityInterface
{
    public const RESOURCE_NAME = 'CapabilityStatement.rest.security';

    protected ?FHIRBoolean $cors = null;

    /** @var FHIRCodeableConcept[] */
    protected array $service = [];
    protected ?FHIRMarkdown $description = null;

    public function getCors(): ?FHIRBoolean
    {
        return $this->cors;
    }

    public function setCors(bool|FHIRBoolean|null $value): self
    {
        $this->cors = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getService(): array
    {
        return $this->service;
    }

    public function setService(?FHIRCodeableConcept ...$value): self
    {
        $this->service = array_filter($value);

        return $this;
    }

    public function addService(?FHIRCodeableConcept ...$value): self
    {
        $this->service = array_filter(array_merge($this->service, $value));

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }
}
