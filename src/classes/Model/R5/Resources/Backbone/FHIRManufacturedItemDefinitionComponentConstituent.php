<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ManufacturedItemDefinitionComponentConstituent Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRManufacturedItemDefinitionComponentConstituentInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;

class FHIRManufacturedItemDefinitionComponentConstituent extends FHIRBackboneElement implements FHIRManufacturedItemDefinitionComponentConstituentInterface
{
    public const RESOURCE_NAME = 'ManufacturedItemDefinition.component.constituent';

    /** @var FHIRQuantity[] */
    protected array $amount = [];

    /** @var FHIRCodeableConcept[] */
    protected array $location = [];

    /** @var FHIRCodeableConcept[] */
    protected array $function = [];

    /** @var FHIRCodeableReference[] */
    protected array $hasIngredient = [];

    /**
     * @return FHIRQuantity[]
     */
    public function getAmount(): array
    {
        return $this->amount;
    }

    public function setAmount(?FHIRQuantity ...$value): self
    {
        $this->amount = array_filter($value);

        return $this;
    }

    public function addAmount(?FHIRQuantity ...$value): self
    {
        $this->amount = array_filter(array_merge($this->amount, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getLocation(): array
    {
        return $this->location;
    }

    public function setLocation(?FHIRCodeableConcept ...$value): self
    {
        $this->location = array_filter($value);

        return $this;
    }

    public function addLocation(?FHIRCodeableConcept ...$value): self
    {
        $this->location = array_filter(array_merge($this->location, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getFunction(): array
    {
        return $this->function;
    }

    public function setFunction(?FHIRCodeableConcept ...$value): self
    {
        $this->function = array_filter($value);

        return $this;
    }

    public function addFunction(?FHIRCodeableConcept ...$value): self
    {
        $this->function = array_filter(array_merge($this->function, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getHasIngredient(): array
    {
        return $this->hasIngredient;
    }

    public function setHasIngredient(?FHIRCodeableReference ...$value): self
    {
        $this->hasIngredient = array_filter($value);

        return $this;
    }

    public function addHasIngredient(?FHIRCodeableReference ...$value): self
    {
        $this->hasIngredient = array_filter(array_merge($this->hasIngredient, $value));

        return $this;
    }
}
