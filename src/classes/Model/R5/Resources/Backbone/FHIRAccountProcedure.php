<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AccountProcedure Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAccountProcedureInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;

class FHIRAccountProcedure extends FHIRBackboneElement implements FHIRAccountProcedureInterface
{
    public const RESOURCE_NAME = 'Account.procedure';

    protected ?FHIRPositiveInt $sequence = null;
    protected ?FHIRCodeableReference $code = null;
    protected ?FHIRDateTime $dateOfService = null;

    /** @var FHIRCodeableConcept[] */
    protected array $type = [];

    /** @var FHIRCodeableConcept[] */
    protected array $packageCode = [];

    /** @var FHIRReference[] */
    protected array $device = [];

    public function getSequence(): ?FHIRPositiveInt
    {
        return $this->sequence;
    }

    public function setSequence(int|FHIRPositiveInt|null $value): self
    {
        $this->sequence = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableReference
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableReference $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getDateOfService(): ?FHIRDateTime
    {
        return $this->dateOfService;
    }

    public function setDateOfService(string|FHIRDateTime|null $value): self
    {
        $this->dateOfService = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getPackageCode(): array
    {
        return $this->packageCode;
    }

    public function setPackageCode(?FHIRCodeableConcept ...$value): self
    {
        $this->packageCode = array_filter($value);

        return $this;
    }

    public function addPackageCode(?FHIRCodeableConcept ...$value): self
    {
        $this->packageCode = array_filter(array_merge($this->packageCode, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getDevice(): array
    {
        return $this->device;
    }

    public function setDevice(?FHIRReference ...$value): self
    {
        $this->device = array_filter($value);

        return $this;
    }

    public function addDevice(?FHIRReference ...$value): self
    {
        $this->device = array_filter(array_merge($this->device, $value));

        return $this;
    }
}
