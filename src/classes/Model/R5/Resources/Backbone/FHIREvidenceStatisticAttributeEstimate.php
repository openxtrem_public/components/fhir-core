<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EvidenceStatisticAttributeEstimate Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREvidenceStatisticAttributeEstimateInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;

class FHIREvidenceStatisticAttributeEstimate extends FHIRBackboneElement implements FHIREvidenceStatisticAttributeEstimateInterface
{
    public const RESOURCE_NAME = 'Evidence.statistic.attributeEstimate';

    protected ?FHIRMarkdown $description = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRDecimal $level = null;
    protected ?FHIRRange $range = null;

    /** @var FHIREvidenceStatisticAttributeEstimate[] */
    protected array $attributeEstimate = [];

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getLevel(): ?FHIRDecimal
    {
        return $this->level;
    }

    public function setLevel(float|FHIRDecimal|null $value): self
    {
        $this->level = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getRange(): ?FHIRRange
    {
        return $this->range;
    }

    public function setRange(?FHIRRange $value): self
    {
        $this->range = $value;

        return $this;
    }

    /**
     * @return FHIREvidenceStatisticAttributeEstimate[]
     */
    public function getAttributeEstimate(): array
    {
        return $this->attributeEstimate;
    }

    public function setAttributeEstimate(?FHIREvidenceStatisticAttributeEstimate ...$value): self
    {
        $this->attributeEstimate = array_filter($value);

        return $this;
    }

    public function addAttributeEstimate(?FHIREvidenceStatisticAttributeEstimate ...$value): self
    {
        $this->attributeEstimate = array_filter(array_merge($this->attributeEstimate, $value));

        return $this;
    }
}
