<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceDefinitionCorrectiveAction Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceDefinitionCorrectiveActionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;

class FHIRDeviceDefinitionCorrectiveAction extends FHIRBackboneElement implements FHIRDeviceDefinitionCorrectiveActionInterface
{
    public const RESOURCE_NAME = 'DeviceDefinition.correctiveAction';

    protected ?FHIRBoolean $recall = null;
    protected ?FHIRCode $scope = null;
    protected ?FHIRPeriod $period = null;

    public function getRecall(): ?FHIRBoolean
    {
        return $this->recall;
    }

    public function setRecall(bool|FHIRBoolean|null $value): self
    {
        $this->recall = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getScope(): ?FHIRCode
    {
        return $this->scope;
    }

    public function setScope(string|FHIRCode|null $value): self
    {
        $this->scope = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }
}
