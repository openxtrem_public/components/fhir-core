<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR InsurancePlanPlanSpecificCost Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRInsurancePlanPlanSpecificCostInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;

class FHIRInsurancePlanPlanSpecificCost extends FHIRBackboneElement implements FHIRInsurancePlanPlanSpecificCostInterface
{
    public const RESOURCE_NAME = 'InsurancePlan.plan.specificCost';

    protected ?FHIRCodeableConcept $category = null;

    /** @var FHIRInsurancePlanPlanSpecificCostBenefit[] */
    protected array $benefit = [];

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    /**
     * @return FHIRInsurancePlanPlanSpecificCostBenefit[]
     */
    public function getBenefit(): array
    {
        return $this->benefit;
    }

    public function setBenefit(?FHIRInsurancePlanPlanSpecificCostBenefit ...$value): self
    {
        $this->benefit = array_filter($value);

        return $this;
    }

    public function addBenefit(?FHIRInsurancePlanPlanSpecificCostBenefit ...$value): self
    {
        $this->benefit = array_filter(array_merge($this->benefit, $value));

        return $this;
    }
}
