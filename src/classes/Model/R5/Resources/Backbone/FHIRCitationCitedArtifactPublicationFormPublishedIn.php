<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CitationCitedArtifactPublicationFormPublishedIn Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCitationCitedArtifactPublicationFormPublishedInInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRCitationCitedArtifactPublicationFormPublishedIn extends FHIRBackboneElement implements FHIRCitationCitedArtifactPublicationFormPublishedInInterface
{
    public const RESOURCE_NAME = 'Citation.citedArtifact.publicationForm.publishedIn';

    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRString $title = null;
    protected ?FHIRReference $publisher = null;
    protected ?FHIRString $publisherLocation = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPublisher(): ?FHIRReference
    {
        return $this->publisher;
    }

    public function setPublisher(?FHIRReference $value): self
    {
        $this->publisher = $value;

        return $this;
    }

    public function getPublisherLocation(): ?FHIRString
    {
        return $this->publisherLocation;
    }

    public function setPublisherLocation(string|FHIRString|null $value): self
    {
        $this->publisherLocation = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
