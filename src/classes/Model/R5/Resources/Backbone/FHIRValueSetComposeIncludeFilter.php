<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ValueSetComposeIncludeFilter Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRValueSetComposeIncludeFilterInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRValueSetComposeIncludeFilter extends FHIRBackboneElement implements FHIRValueSetComposeIncludeFilterInterface
{
    public const RESOURCE_NAME = 'ValueSet.compose.include.filter';

    protected ?FHIRCode $property = null;
    protected ?FHIRCode $op = null;
    protected ?FHIRString $value = null;

    public function getProperty(): ?FHIRCode
    {
        return $this->property;
    }

    public function setProperty(string|FHIRCode|null $value): self
    {
        $this->property = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getOp(): ?FHIRCode
    {
        return $this->op;
    }

    public function setOp(string|FHIRCode|null $value): self
    {
        $this->op = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getValue(): ?FHIRString
    {
        return $this->value;
    }

    public function setValue(string|FHIRString|null $value): self
    {
        $this->value = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
