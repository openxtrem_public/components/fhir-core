<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExplanationOfBenefitSupportingInfo Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExplanationOfBenefitSupportingInfoInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRExplanationOfBenefitSupportingInfo extends FHIRBackboneElement implements FHIRExplanationOfBenefitSupportingInfoInterface
{
    public const RESOURCE_NAME = 'ExplanationOfBenefit.supportingInfo';

    protected ?FHIRPositiveInt $sequence = null;
    protected ?FHIRCodeableConcept $category = null;
    protected ?FHIRCodeableConcept $code = null;
    protected FHIRDate|FHIRPeriod|null $timing = null;
    protected FHIRBoolean|FHIRString|FHIRQuantity|FHIRAttachment|FHIRReference|FHIRIdentifier|null $value = null;
    protected ?FHIRCoding $reason = null;

    public function getSequence(): ?FHIRPositiveInt
    {
        return $this->sequence;
    }

    public function setSequence(int|FHIRPositiveInt|null $value): self
    {
        $this->sequence = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getTiming(): FHIRDate|FHIRPeriod|null
    {
        return $this->timing;
    }

    public function setTiming(FHIRDate|FHIRPeriod|null $value): self
    {
        $this->timing = $value;

        return $this;
    }

    public function getValue(): FHIRBoolean|FHIRString|FHIRQuantity|FHIRAttachment|FHIRReference|FHIRIdentifier|null
    {
        return $this->value;
    }

    public function setValue(
        FHIRBoolean|FHIRString|FHIRQuantity|FHIRAttachment|FHIRReference|FHIRIdentifier|null $value,
    ): self
    {
        $this->value = $value;

        return $this;
    }

    public function getReason(): ?FHIRCoding
    {
        return $this->reason;
    }

    public function setReason(?FHIRCoding $value): self
    {
        $this->reason = $value;

        return $this;
    }
}
