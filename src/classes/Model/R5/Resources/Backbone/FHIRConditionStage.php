<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ConditionStage Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRConditionStageInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRConditionStage extends FHIRBackboneElement implements FHIRConditionStageInterface
{
    public const RESOURCE_NAME = 'Condition.stage';

    protected ?FHIRCodeableConcept $summary = null;

    /** @var FHIRReference[] */
    protected array $assessment = [];
    protected ?FHIRCodeableConcept $type = null;

    public function getSummary(): ?FHIRCodeableConcept
    {
        return $this->summary;
    }

    public function setSummary(?FHIRCodeableConcept $value): self
    {
        $this->summary = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAssessment(): array
    {
        return $this->assessment;
    }

    public function setAssessment(?FHIRReference ...$value): self
    {
        $this->assessment = array_filter($value);

        return $this;
    }

    public function addAssessment(?FHIRReference ...$value): self
    {
        $this->assessment = array_filter(array_merge($this->assessment, $value));

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }
}
