<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledgeDefinitionalDrugCharacteristic Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationKnowledgeDefinitionalDrugCharacteristicInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBase64Binary;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRMedicationKnowledgeDefinitionalDrugCharacteristic extends FHIRBackboneElement implements FHIRMedicationKnowledgeDefinitionalDrugCharacteristicInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge.definitional.drugCharacteristic';

    protected ?FHIRCodeableConcept $type = null;
    protected FHIRCodeableConcept|FHIRString|FHIRQuantity|FHIRBase64Binary|FHIRAttachment|null $value = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getValue(): FHIRCodeableConcept|FHIRString|FHIRQuantity|FHIRBase64Binary|FHIRAttachment|null
    {
        return $this->value;
    }

    public function setValue(
        FHIRCodeableConcept|FHIRString|FHIRQuantity|FHIRBase64Binary|FHIRAttachment|null $value,
    ): self
    {
        $this->value = $value;

        return $this;
    }
}
