<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CareTeamParticipant Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCareTeamParticipantInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRTiming;

class FHIRCareTeamParticipant extends FHIRBackboneElement implements FHIRCareTeamParticipantInterface
{
    public const RESOURCE_NAME = 'CareTeam.participant';

    protected ?FHIRCodeableConcept $role = null;
    protected ?FHIRReference $member = null;
    protected ?FHIRReference $onBehalfOf = null;
    protected FHIRPeriod|FHIRTiming|null $coverage = null;

    public function getRole(): ?FHIRCodeableConcept
    {
        return $this->role;
    }

    public function setRole(?FHIRCodeableConcept $value): self
    {
        $this->role = $value;

        return $this;
    }

    public function getMember(): ?FHIRReference
    {
        return $this->member;
    }

    public function setMember(?FHIRReference $value): self
    {
        $this->member = $value;

        return $this;
    }

    public function getOnBehalfOf(): ?FHIRReference
    {
        return $this->onBehalfOf;
    }

    public function setOnBehalfOf(?FHIRReference $value): self
    {
        $this->onBehalfOf = $value;

        return $this;
    }

    public function getCoverage(): FHIRPeriod|FHIRTiming|null
    {
        return $this->coverage;
    }

    public function setCoverage(FHIRPeriod|FHIRTiming|null $value): self
    {
        $this->coverage = $value;

        return $this;
    }
}
