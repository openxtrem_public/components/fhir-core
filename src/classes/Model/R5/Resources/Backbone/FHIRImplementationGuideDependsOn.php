<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImplementationGuideDependsOn Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImplementationGuideDependsOnInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRImplementationGuideDependsOn extends FHIRBackboneElement implements FHIRImplementationGuideDependsOnInterface
{
    public const RESOURCE_NAME = 'ImplementationGuide.dependsOn';

    protected ?FHIRCanonical $uri = null;
    protected ?FHIRId $packageId = null;
    protected ?FHIRString $version = null;
    protected ?FHIRMarkdown $reason = null;

    public function getUri(): ?FHIRCanonical
    {
        return $this->uri;
    }

    public function setUri(string|FHIRCanonical|null $value): self
    {
        $this->uri = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    public function getPackageId(): ?FHIRId
    {
        return $this->packageId;
    }

    public function setPackageId(string|FHIRId|null $value): self
    {
        $this->packageId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getReason(): ?FHIRMarkdown
    {
        return $this->reason;
    }

    public function setReason(string|FHIRMarkdown|null $value): self
    {
        $this->reason = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }
}
