<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClinicalUseDefinitionIndication Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRClinicalUseDefinitionIndicationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRExpression;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRClinicalUseDefinitionIndication extends FHIRBackboneElement implements FHIRClinicalUseDefinitionIndicationInterface
{
    public const RESOURCE_NAME = 'ClinicalUseDefinition.indication';

    protected ?FHIRCodeableReference $diseaseSymptomProcedure = null;
    protected ?FHIRCodeableReference $diseaseStatus = null;

    /** @var FHIRCodeableReference[] */
    protected array $comorbidity = [];
    protected ?FHIRCodeableReference $intendedEffect = null;
    protected FHIRRange|FHIRString|null $duration = null;

    /** @var FHIRReference[] */
    protected array $undesirableEffect = [];
    protected ?FHIRExpression $applicability = null;

    /** @var FHIRClinicalUseDefinitionContraindicationOtherTherapy[] */
    protected array $otherTherapy = [];

    public function getDiseaseSymptomProcedure(): ?FHIRCodeableReference
    {
        return $this->diseaseSymptomProcedure;
    }

    public function setDiseaseSymptomProcedure(?FHIRCodeableReference $value): self
    {
        $this->diseaseSymptomProcedure = $value;

        return $this;
    }

    public function getDiseaseStatus(): ?FHIRCodeableReference
    {
        return $this->diseaseStatus;
    }

    public function setDiseaseStatus(?FHIRCodeableReference $value): self
    {
        $this->diseaseStatus = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getComorbidity(): array
    {
        return $this->comorbidity;
    }

    public function setComorbidity(?FHIRCodeableReference ...$value): self
    {
        $this->comorbidity = array_filter($value);

        return $this;
    }

    public function addComorbidity(?FHIRCodeableReference ...$value): self
    {
        $this->comorbidity = array_filter(array_merge($this->comorbidity, $value));

        return $this;
    }

    public function getIntendedEffect(): ?FHIRCodeableReference
    {
        return $this->intendedEffect;
    }

    public function setIntendedEffect(?FHIRCodeableReference $value): self
    {
        $this->intendedEffect = $value;

        return $this;
    }

    public function getDuration(): FHIRRange|FHIRString|null
    {
        return $this->duration;
    }

    public function setDuration(FHIRRange|FHIRString|null $value): self
    {
        $this->duration = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getUndesirableEffect(): array
    {
        return $this->undesirableEffect;
    }

    public function setUndesirableEffect(?FHIRReference ...$value): self
    {
        $this->undesirableEffect = array_filter($value);

        return $this;
    }

    public function addUndesirableEffect(?FHIRReference ...$value): self
    {
        $this->undesirableEffect = array_filter(array_merge($this->undesirableEffect, $value));

        return $this;
    }

    public function getApplicability(): ?FHIRExpression
    {
        return $this->applicability;
    }

    public function setApplicability(?FHIRExpression $value): self
    {
        $this->applicability = $value;

        return $this;
    }

    /**
     * @return FHIRClinicalUseDefinitionContraindicationOtherTherapy[]
     */
    public function getOtherTherapy(): array
    {
        return $this->otherTherapy;
    }

    public function setOtherTherapy(?FHIRClinicalUseDefinitionContraindicationOtherTherapy ...$value): self
    {
        $this->otherTherapy = array_filter($value);

        return $this;
    }

    public function addOtherTherapy(?FHIRClinicalUseDefinitionContraindicationOtherTherapy ...$value): self
    {
        $this->otherTherapy = array_filter(array_merge($this->otherTherapy, $value));

        return $this;
    }
}
