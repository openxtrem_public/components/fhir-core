<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CitationCitedArtifactContributorshipEntryContributionInstance Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCitationCitedArtifactContributorshipEntryContributionInstanceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;

class FHIRCitationCitedArtifactContributorshipEntryContributionInstance extends FHIRBackboneElement implements FHIRCitationCitedArtifactContributorshipEntryContributionInstanceInterface
{
    public const RESOURCE_NAME = 'Citation.citedArtifact.contributorship.entry.contributionInstance';

    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRDateTime $time = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getTime(): ?FHIRDateTime
    {
        return $this->time;
    }

    public function setTime(string|FHIRDateTime|null $value): self
    {
        $this->time = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }
}
