<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SpecimenDefinitionTypeTestedHandling Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSpecimenDefinitionTypeTestedHandlingInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;

class FHIRSpecimenDefinitionTypeTestedHandling extends FHIRBackboneElement implements FHIRSpecimenDefinitionTypeTestedHandlingInterface
{
    public const RESOURCE_NAME = 'SpecimenDefinition.typeTested.handling';

    protected ?FHIRCodeableConcept $temperatureQualifier = null;
    protected ?FHIRRange $temperatureRange = null;
    protected ?FHIRDuration $maxDuration = null;
    protected ?FHIRMarkdown $instruction = null;

    public function getTemperatureQualifier(): ?FHIRCodeableConcept
    {
        return $this->temperatureQualifier;
    }

    public function setTemperatureQualifier(?FHIRCodeableConcept $value): self
    {
        $this->temperatureQualifier = $value;

        return $this;
    }

    public function getTemperatureRange(): ?FHIRRange
    {
        return $this->temperatureRange;
    }

    public function setTemperatureRange(?FHIRRange $value): self
    {
        $this->temperatureRange = $value;

        return $this;
    }

    public function getMaxDuration(): ?FHIRDuration
    {
        return $this->maxDuration;
    }

    public function setMaxDuration(?FHIRDuration $value): self
    {
        $this->maxDuration = $value;

        return $this;
    }

    public function getInstruction(): ?FHIRMarkdown
    {
        return $this->instruction;
    }

    public function setInstruction(string|FHIRMarkdown|null $value): self
    {
        $this->instruction = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }
}
