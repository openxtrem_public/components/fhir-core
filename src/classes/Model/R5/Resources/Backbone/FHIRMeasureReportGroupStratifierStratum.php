<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MeasureReportGroupStratifierStratum Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMeasureReportGroupStratifierStratumInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;

class FHIRMeasureReportGroupStratifierStratum extends FHIRBackboneElement implements FHIRMeasureReportGroupStratifierStratumInterface
{
    public const RESOURCE_NAME = 'MeasureReport.group.stratifier.stratum';

    protected FHIRCodeableConcept|FHIRBoolean|FHIRQuantity|FHIRRange|FHIRReference|null $value = null;

    /** @var FHIRMeasureReportGroupStratifierStratumComponent[] */
    protected array $component = [];

    /** @var FHIRMeasureReportGroupStratifierStratumPopulation[] */
    protected array $population = [];
    protected FHIRQuantity|FHIRDateTime|FHIRCodeableConcept|FHIRPeriod|FHIRRange|FHIRDuration|null $measureScore = null;

    public function getValue(): FHIRCodeableConcept|FHIRBoolean|FHIRQuantity|FHIRRange|FHIRReference|null
    {
        return $this->value;
    }

    public function setValue(FHIRCodeableConcept|FHIRBoolean|FHIRQuantity|FHIRRange|FHIRReference|null $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return FHIRMeasureReportGroupStratifierStratumComponent[]
     */
    public function getComponent(): array
    {
        return $this->component;
    }

    public function setComponent(?FHIRMeasureReportGroupStratifierStratumComponent ...$value): self
    {
        $this->component = array_filter($value);

        return $this;
    }

    public function addComponent(?FHIRMeasureReportGroupStratifierStratumComponent ...$value): self
    {
        $this->component = array_filter(array_merge($this->component, $value));

        return $this;
    }

    /**
     * @return FHIRMeasureReportGroupStratifierStratumPopulation[]
     */
    public function getPopulation(): array
    {
        return $this->population;
    }

    public function setPopulation(?FHIRMeasureReportGroupStratifierStratumPopulation ...$value): self
    {
        $this->population = array_filter($value);

        return $this;
    }

    public function addPopulation(?FHIRMeasureReportGroupStratifierStratumPopulation ...$value): self
    {
        $this->population = array_filter(array_merge($this->population, $value));

        return $this;
    }

    public function getMeasureScore(
    ): FHIRQuantity|FHIRDateTime|FHIRCodeableConcept|FHIRPeriod|FHIRRange|FHIRDuration|null
    {
        return $this->measureScore;
    }

    public function setMeasureScore(
        FHIRQuantity|FHIRDateTime|FHIRCodeableConcept|FHIRPeriod|FHIRRange|FHIRDuration|null $value,
    ): self
    {
        $this->measureScore = $value;

        return $this;
    }
}
