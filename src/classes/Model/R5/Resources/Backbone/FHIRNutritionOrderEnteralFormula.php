<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR NutritionOrderEnteralFormula Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRNutritionOrderEnteralFormulaInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRNutritionOrderEnteralFormula extends FHIRBackboneElement implements FHIRNutritionOrderEnteralFormulaInterface
{
    public const RESOURCE_NAME = 'NutritionOrder.enteralFormula';

    protected ?FHIRCodeableReference $baseFormulaType = null;
    protected ?FHIRString $baseFormulaProductName = null;

    /** @var FHIRCodeableReference[] */
    protected array $deliveryDevice = [];

    /** @var FHIRNutritionOrderEnteralFormulaAdditive[] */
    protected array $additive = [];
    protected ?FHIRQuantity $caloricDensity = null;
    protected ?FHIRCodeableConcept $routeOfAdministration = null;

    /** @var FHIRNutritionOrderEnteralFormulaAdministration[] */
    protected array $administration = [];
    protected ?FHIRQuantity $maxVolumeToDeliver = null;
    protected ?FHIRMarkdown $administrationInstruction = null;

    public function getBaseFormulaType(): ?FHIRCodeableReference
    {
        return $this->baseFormulaType;
    }

    public function setBaseFormulaType(?FHIRCodeableReference $value): self
    {
        $this->baseFormulaType = $value;

        return $this;
    }

    public function getBaseFormulaProductName(): ?FHIRString
    {
        return $this->baseFormulaProductName;
    }

    public function setBaseFormulaProductName(string|FHIRString|null $value): self
    {
        $this->baseFormulaProductName = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getDeliveryDevice(): array
    {
        return $this->deliveryDevice;
    }

    public function setDeliveryDevice(?FHIRCodeableReference ...$value): self
    {
        $this->deliveryDevice = array_filter($value);

        return $this;
    }

    public function addDeliveryDevice(?FHIRCodeableReference ...$value): self
    {
        $this->deliveryDevice = array_filter(array_merge($this->deliveryDevice, $value));

        return $this;
    }

    /**
     * @return FHIRNutritionOrderEnteralFormulaAdditive[]
     */
    public function getAdditive(): array
    {
        return $this->additive;
    }

    public function setAdditive(?FHIRNutritionOrderEnteralFormulaAdditive ...$value): self
    {
        $this->additive = array_filter($value);

        return $this;
    }

    public function addAdditive(?FHIRNutritionOrderEnteralFormulaAdditive ...$value): self
    {
        $this->additive = array_filter(array_merge($this->additive, $value));

        return $this;
    }

    public function getCaloricDensity(): ?FHIRQuantity
    {
        return $this->caloricDensity;
    }

    public function setCaloricDensity(?FHIRQuantity $value): self
    {
        $this->caloricDensity = $value;

        return $this;
    }

    public function getRouteOfAdministration(): ?FHIRCodeableConcept
    {
        return $this->routeOfAdministration;
    }

    public function setRouteOfAdministration(?FHIRCodeableConcept $value): self
    {
        $this->routeOfAdministration = $value;

        return $this;
    }

    /**
     * @return FHIRNutritionOrderEnteralFormulaAdministration[]
     */
    public function getAdministration(): array
    {
        return $this->administration;
    }

    public function setAdministration(?FHIRNutritionOrderEnteralFormulaAdministration ...$value): self
    {
        $this->administration = array_filter($value);

        return $this;
    }

    public function addAdministration(?FHIRNutritionOrderEnteralFormulaAdministration ...$value): self
    {
        $this->administration = array_filter(array_merge($this->administration, $value));

        return $this;
    }

    public function getMaxVolumeToDeliver(): ?FHIRQuantity
    {
        return $this->maxVolumeToDeliver;
    }

    public function setMaxVolumeToDeliver(?FHIRQuantity $value): self
    {
        $this->maxVolumeToDeliver = $value;

        return $this;
    }

    public function getAdministrationInstruction(): ?FHIRMarkdown
    {
        return $this->administrationInstruction;
    }

    public function setAdministrationInstruction(string|FHIRMarkdown|null $value): self
    {
        $this->administrationInstruction = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }
}
