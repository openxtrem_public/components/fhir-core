<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AdverseEventSuspectEntity Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAdverseEventSuspectEntityInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRAdverseEventSuspectEntity extends FHIRBackboneElement implements FHIRAdverseEventSuspectEntityInterface
{
    public const RESOURCE_NAME = 'AdverseEvent.suspectEntity';

    protected FHIRCodeableConcept|FHIRReference|null $instance = null;
    protected ?FHIRAdverseEventSuspectEntityCausality $causality = null;

    public function getInstance(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->instance;
    }

    public function setInstance(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->instance = $value;

        return $this;
    }

    public function getCausality(): ?FHIRAdverseEventSuspectEntityCausality
    {
        return $this->causality;
    }

    public function setCausality(?FHIRAdverseEventSuspectEntityCausality $value): self
    {
        $this->causality = $value;

        return $this;
    }
}
