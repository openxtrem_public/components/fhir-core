<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR InsurancePlanPlanGeneralCost Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRInsurancePlanPlanGeneralCostInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRInsurancePlanPlanGeneralCost extends FHIRBackboneElement implements FHIRInsurancePlanPlanGeneralCostInterface
{
    public const RESOURCE_NAME = 'InsurancePlan.plan.generalCost';

    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRPositiveInt $groupSize = null;
    protected ?FHIRMoney $cost = null;
    protected ?FHIRString $comment = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getGroupSize(): ?FHIRPositiveInt
    {
        return $this->groupSize;
    }

    public function setGroupSize(int|FHIRPositiveInt|null $value): self
    {
        $this->groupSize = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getCost(): ?FHIRMoney
    {
        return $this->cost;
    }

    public function setCost(?FHIRMoney $value): self
    {
        $this->cost = $value;

        return $this;
    }

    public function getComment(): ?FHIRString
    {
        return $this->comment;
    }

    public function setComment(string|FHIRString|null $value): self
    {
        $this->comment = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
