<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestPlanTestCaseAssertion Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestPlanTestCaseAssertionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;

class FHIRTestPlanTestCaseAssertion extends FHIRBackboneElement implements FHIRTestPlanTestCaseAssertionInterface
{
    public const RESOURCE_NAME = 'TestPlan.testCase.assertion';

    /** @var FHIRCodeableConcept[] */
    protected array $type = [];

    /** @var FHIRCodeableReference[] */
    protected array $object = [];

    /** @var FHIRCodeableReference[] */
    protected array $result = [];

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getObject(): array
    {
        return $this->object;
    }

    public function setObject(?FHIRCodeableReference ...$value): self
    {
        $this->object = array_filter($value);

        return $this;
    }

    public function addObject(?FHIRCodeableReference ...$value): self
    {
        $this->object = array_filter(array_merge($this->object, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getResult(): array
    {
        return $this->result;
    }

    public function setResult(?FHIRCodeableReference ...$value): self
    {
        $this->result = array_filter($value);

        return $this;
    }

    public function addResult(?FHIRCodeableReference ...$value): self
    {
        $this->result = array_filter(array_merge($this->result, $value));

        return $this;
    }
}
