<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImplementationGuideDefinitionResource Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImplementationGuideDefinitionResourceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRImplementationGuideDefinitionResource extends FHIRBackboneElement implements FHIRImplementationGuideDefinitionResourceInterface
{
    public const RESOURCE_NAME = 'ImplementationGuide.definition.resource';

    protected ?FHIRReference $reference = null;

    /** @var FHIRCode[] */
    protected array $fhirVersion = [];
    protected ?FHIRString $name = null;
    protected ?FHIRMarkdown $description = null;
    protected ?FHIRBoolean $isExample = null;

    /** @var FHIRCanonical[] */
    protected array $profile = [];
    protected ?FHIRId $groupingId = null;

    public function getReference(): ?FHIRReference
    {
        return $this->reference;
    }

    public function setReference(?FHIRReference $value): self
    {
        $this->reference = $value;

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getFhirVersion(): array
    {
        return $this->fhirVersion;
    }

    public function setFhirVersion(string|FHIRCode|null ...$value): self
    {
        $this->fhirVersion = [];
        $this->addFhirVersion(...$value);

        return $this;
    }

    public function addFhirVersion(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->fhirVersion = array_filter(array_merge($this->fhirVersion, $values));

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getIsExample(): ?FHIRBoolean
    {
        return $this->isExample;
    }

    public function setIsExample(bool|FHIRBoolean|null $value): self
    {
        $this->isExample = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getProfile(): array
    {
        return $this->profile;
    }

    public function setProfile(string|FHIRCanonical|null ...$value): self
    {
        $this->profile = [];
        $this->addProfile(...$value);

        return $this;
    }

    public function addProfile(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->profile = array_filter(array_merge($this->profile, $values));

        return $this;
    }

    public function getGroupingId(): ?FHIRId
    {
        return $this->groupingId;
    }

    public function setGroupingId(string|FHIRId|null $value): self
    {
        $this->groupingId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }
}
