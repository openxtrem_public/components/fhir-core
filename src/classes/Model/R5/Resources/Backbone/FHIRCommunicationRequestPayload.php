<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CommunicationRequestPayload Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCommunicationRequestPayloadInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;

class FHIRCommunicationRequestPayload extends FHIRBackboneElement implements FHIRCommunicationRequestPayloadInterface
{
    public const RESOURCE_NAME = 'CommunicationRequest.payload';

    protected FHIRAttachment|FHIRReference|FHIRCodeableConcept|null $content = null;

    public function getContent(): FHIRAttachment|FHIRReference|FHIRCodeableConcept|null
    {
        return $this->content;
    }

    public function setContent(FHIRAttachment|FHIRReference|FHIRCodeableConcept|null $value): self
    {
        $this->content = $value;

        return $this;
    }
}
