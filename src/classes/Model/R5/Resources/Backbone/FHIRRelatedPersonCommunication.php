<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR RelatedPersonCommunication Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRRelatedPersonCommunicationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;

class FHIRRelatedPersonCommunication extends FHIRBackboneElement implements FHIRRelatedPersonCommunicationInterface
{
    public const RESOURCE_NAME = 'RelatedPerson.communication';

    protected ?FHIRCodeableConcept $language = null;
    protected ?FHIRBoolean $preferred = null;

    public function getLanguage(): ?FHIRCodeableConcept
    {
        return $this->language;
    }

    public function setLanguage(?FHIRCodeableConcept $value): self
    {
        $this->language = $value;

        return $this;
    }

    public function getPreferred(): ?FHIRBoolean
    {
        return $this->preferred;
    }

    public function setPreferred(bool|FHIRBoolean|null $value): self
    {
        $this->preferred = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }
}
