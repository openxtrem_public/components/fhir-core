<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceSourceMaterialPartDescription Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceSourceMaterialPartDescriptionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;

class FHIRSubstanceSourceMaterialPartDescription extends FHIRBackboneElement implements FHIRSubstanceSourceMaterialPartDescriptionInterface
{
    public const RESOURCE_NAME = 'SubstanceSourceMaterial.partDescription';

    protected ?FHIRCodeableConcept $part = null;
    protected ?FHIRCodeableConcept $partLocation = null;

    public function getPart(): ?FHIRCodeableConcept
    {
        return $this->part;
    }

    public function setPart(?FHIRCodeableConcept $value): self
    {
        $this->part = $value;

        return $this;
    }

    public function getPartLocation(): ?FHIRCodeableConcept
    {
        return $this->partLocation;
    }

    public function setPartLocation(?FHIRCodeableConcept $value): self
    {
        $this->partLocation = $value;

        return $this;
    }
}
