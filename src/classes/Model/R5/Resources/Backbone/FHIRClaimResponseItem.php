<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClaimResponseItem Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRClaimResponseItemInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;

class FHIRClaimResponseItem extends FHIRBackboneElement implements FHIRClaimResponseItemInterface
{
    public const RESOURCE_NAME = 'ClaimResponse.item';

    protected ?FHIRPositiveInt $itemSequence = null;

    /** @var FHIRIdentifier[] */
    protected array $traceNumber = [];

    /** @var FHIRPositiveInt[] */
    protected array $noteNumber = [];
    protected ?FHIRClaimResponseItemReviewOutcome $reviewOutcome = null;

    /** @var FHIRClaimResponseItemAdjudication[] */
    protected array $adjudication = [];

    /** @var FHIRClaimResponseItemDetail[] */
    protected array $detail = [];

    public function getItemSequence(): ?FHIRPositiveInt
    {
        return $this->itemSequence;
    }

    public function setItemSequence(int|FHIRPositiveInt|null $value): self
    {
        $this->itemSequence = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getTraceNumber(): array
    {
        return $this->traceNumber;
    }

    public function setTraceNumber(?FHIRIdentifier ...$value): self
    {
        $this->traceNumber = array_filter($value);

        return $this;
    }

    public function addTraceNumber(?FHIRIdentifier ...$value): self
    {
        $this->traceNumber = array_filter(array_merge($this->traceNumber, $value));

        return $this;
    }

    /**
     * @return FHIRPositiveInt[]
     */
    public function getNoteNumber(): array
    {
        return $this->noteNumber;
    }

    public function setNoteNumber(int|FHIRPositiveInt|null ...$value): self
    {
        $this->noteNumber = [];
        $this->addNoteNumber(...$value);

        return $this;
    }

    public function addNoteNumber(int|FHIRPositiveInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRPositiveInt())->setValue($v) : $v, $value);

        $this->noteNumber = array_filter(array_merge($this->noteNumber, $values));

        return $this;
    }

    public function getReviewOutcome(): ?FHIRClaimResponseItemReviewOutcome
    {
        return $this->reviewOutcome;
    }

    public function setReviewOutcome(?FHIRClaimResponseItemReviewOutcome $value): self
    {
        $this->reviewOutcome = $value;

        return $this;
    }

    /**
     * @return FHIRClaimResponseItemAdjudication[]
     */
    public function getAdjudication(): array
    {
        return $this->adjudication;
    }

    public function setAdjudication(?FHIRClaimResponseItemAdjudication ...$value): self
    {
        $this->adjudication = array_filter($value);

        return $this;
    }

    public function addAdjudication(?FHIRClaimResponseItemAdjudication ...$value): self
    {
        $this->adjudication = array_filter(array_merge($this->adjudication, $value));

        return $this;
    }

    /**
     * @return FHIRClaimResponseItemDetail[]
     */
    public function getDetail(): array
    {
        return $this->detail;
    }

    public function setDetail(?FHIRClaimResponseItemDetail ...$value): self
    {
        $this->detail = array_filter($value);

        return $this;
    }

    public function addDetail(?FHIRClaimResponseItemDetail ...$value): self
    {
        $this->detail = array_filter(array_merge($this->detail, $value));

        return $this;
    }
}
