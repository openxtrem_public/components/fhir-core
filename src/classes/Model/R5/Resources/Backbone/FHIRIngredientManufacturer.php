<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR IngredientManufacturer Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRIngredientManufacturerInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;

class FHIRIngredientManufacturer extends FHIRBackboneElement implements FHIRIngredientManufacturerInterface
{
    public const RESOURCE_NAME = 'Ingredient.manufacturer';

    protected ?FHIRCode $role = null;
    protected ?FHIRReference $manufacturer = null;

    public function getRole(): ?FHIRCode
    {
        return $this->role;
    }

    public function setRole(string|FHIRCode|null $value): self
    {
        $this->role = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getManufacturer(): ?FHIRReference
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?FHIRReference $value): self
    {
        $this->manufacturer = $value;

        return $this;
    }
}
