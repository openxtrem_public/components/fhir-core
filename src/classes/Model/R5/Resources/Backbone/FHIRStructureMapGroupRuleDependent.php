<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR StructureMapGroupRuleDependent Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRStructureMapGroupRuleDependentInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRId;

class FHIRStructureMapGroupRuleDependent extends FHIRBackboneElement implements FHIRStructureMapGroupRuleDependentInterface
{
    public const RESOURCE_NAME = 'StructureMap.group.rule.dependent';

    protected ?FHIRId $name = null;

    /** @var FHIRStructureMapGroupRuleTargetParameter[] */
    protected array $parameter = [];

    public function getName(): ?FHIRId
    {
        return $this->name;
    }

    public function setName(string|FHIRId|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRStructureMapGroupRuleTargetParameter[]
     */
    public function getParameter(): array
    {
        return $this->parameter;
    }

    public function setParameter(?FHIRStructureMapGroupRuleTargetParameter ...$value): self
    {
        $this->parameter = array_filter($value);

        return $this;
    }

    public function addParameter(?FHIRStructureMapGroupRuleTargetParameter ...$value): self
    {
        $this->parameter = array_filter(array_merge($this->parameter, $value));

        return $this;
    }
}
