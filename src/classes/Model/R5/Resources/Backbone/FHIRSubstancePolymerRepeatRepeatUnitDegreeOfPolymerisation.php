<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstancePolymerRepeatRepeatUnitDegreeOfPolymerisation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstancePolymerRepeatRepeatUnitDegreeOfPolymerisationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInteger;

class FHIRSubstancePolymerRepeatRepeatUnitDegreeOfPolymerisation extends FHIRBackboneElement implements FHIRSubstancePolymerRepeatRepeatUnitDegreeOfPolymerisationInterface
{
    public const RESOURCE_NAME = 'SubstancePolymer.repeat.repeatUnit.degreeOfPolymerisation';

    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRInteger $average = null;
    protected ?FHIRInteger $low = null;
    protected ?FHIRInteger $high = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getAverage(): ?FHIRInteger
    {
        return $this->average;
    }

    public function setAverage(int|FHIRInteger|null $value): self
    {
        $this->average = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getLow(): ?FHIRInteger
    {
        return $this->low;
    }

    public function setLow(int|FHIRInteger|null $value): self
    {
        $this->low = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getHigh(): ?FHIRInteger
    {
        return $this->high;
    }

    public function setHigh(int|FHIRInteger|null $value): self
    {
        $this->high = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }
}
