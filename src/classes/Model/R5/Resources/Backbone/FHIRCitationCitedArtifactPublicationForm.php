<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CitationCitedArtifactPublicationForm Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCitationCitedArtifactPublicationFormInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRCitationCitedArtifactPublicationForm extends FHIRBackboneElement implements FHIRCitationCitedArtifactPublicationFormInterface
{
    public const RESOURCE_NAME = 'Citation.citedArtifact.publicationForm';

    protected ?FHIRCitationCitedArtifactPublicationFormPublishedIn $publishedIn = null;
    protected ?FHIRCodeableConcept $citedMedium = null;
    protected ?FHIRString $volume = null;
    protected ?FHIRString $issue = null;
    protected ?FHIRDateTime $articleDate = null;
    protected ?FHIRString $publicationDateText = null;
    protected ?FHIRString $publicationDateSeason = null;
    protected ?FHIRDateTime $lastRevisionDate = null;

    /** @var FHIRCodeableConcept[] */
    protected array $language = [];
    protected ?FHIRString $accessionNumber = null;
    protected ?FHIRString $pageString = null;
    protected ?FHIRString $firstPage = null;
    protected ?FHIRString $lastPage = null;
    protected ?FHIRString $pageCount = null;
    protected ?FHIRMarkdown $copyright = null;

    public function getPublishedIn(): ?FHIRCitationCitedArtifactPublicationFormPublishedIn
    {
        return $this->publishedIn;
    }

    public function setPublishedIn(?FHIRCitationCitedArtifactPublicationFormPublishedIn $value): self
    {
        $this->publishedIn = $value;

        return $this;
    }

    public function getCitedMedium(): ?FHIRCodeableConcept
    {
        return $this->citedMedium;
    }

    public function setCitedMedium(?FHIRCodeableConcept $value): self
    {
        $this->citedMedium = $value;

        return $this;
    }

    public function getVolume(): ?FHIRString
    {
        return $this->volume;
    }

    public function setVolume(string|FHIRString|null $value): self
    {
        $this->volume = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getIssue(): ?FHIRString
    {
        return $this->issue;
    }

    public function setIssue(string|FHIRString|null $value): self
    {
        $this->issue = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getArticleDate(): ?FHIRDateTime
    {
        return $this->articleDate;
    }

    public function setArticleDate(string|FHIRDateTime|null $value): self
    {
        $this->articleDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPublicationDateText(): ?FHIRString
    {
        return $this->publicationDateText;
    }

    public function setPublicationDateText(string|FHIRString|null $value): self
    {
        $this->publicationDateText = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPublicationDateSeason(): ?FHIRString
    {
        return $this->publicationDateSeason;
    }

    public function setPublicationDateSeason(string|FHIRString|null $value): self
    {
        $this->publicationDateSeason = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getLastRevisionDate(): ?FHIRDateTime
    {
        return $this->lastRevisionDate;
    }

    public function setLastRevisionDate(string|FHIRDateTime|null $value): self
    {
        $this->lastRevisionDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getLanguage(): array
    {
        return $this->language;
    }

    public function setLanguage(?FHIRCodeableConcept ...$value): self
    {
        $this->language = array_filter($value);

        return $this;
    }

    public function addLanguage(?FHIRCodeableConcept ...$value): self
    {
        $this->language = array_filter(array_merge($this->language, $value));

        return $this;
    }

    public function getAccessionNumber(): ?FHIRString
    {
        return $this->accessionNumber;
    }

    public function setAccessionNumber(string|FHIRString|null $value): self
    {
        $this->accessionNumber = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPageString(): ?FHIRString
    {
        return $this->pageString;
    }

    public function setPageString(string|FHIRString|null $value): self
    {
        $this->pageString = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getFirstPage(): ?FHIRString
    {
        return $this->firstPage;
    }

    public function setFirstPage(string|FHIRString|null $value): self
    {
        $this->firstPage = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getLastPage(): ?FHIRString
    {
        return $this->lastPage;
    }

    public function setLastPage(string|FHIRString|null $value): self
    {
        $this->lastPage = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPageCount(): ?FHIRString
    {
        return $this->pageCount;
    }

    public function setPageCount(string|FHIRString|null $value): self
    {
        $this->pageCount = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCopyright(): ?FHIRMarkdown
    {
        return $this->copyright;
    }

    public function setCopyright(string|FHIRMarkdown|null $value): self
    {
        $this->copyright = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }
}
