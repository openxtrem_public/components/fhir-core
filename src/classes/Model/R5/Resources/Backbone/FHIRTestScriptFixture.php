<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestScriptFixture Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestScriptFixtureInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;

class FHIRTestScriptFixture extends FHIRBackboneElement implements FHIRTestScriptFixtureInterface
{
    public const RESOURCE_NAME = 'TestScript.fixture';

    protected ?FHIRBoolean $autocreate = null;
    protected ?FHIRBoolean $autodelete = null;
    protected ?FHIRReference $resource = null;

    public function getAutocreate(): ?FHIRBoolean
    {
        return $this->autocreate;
    }

    public function setAutocreate(bool|FHIRBoolean|null $value): self
    {
        $this->autocreate = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getAutodelete(): ?FHIRBoolean
    {
        return $this->autodelete;
    }

    public function setAutodelete(bool|FHIRBoolean|null $value): self
    {
        $this->autodelete = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getResource(): ?FHIRReference
    {
        return $this->resource;
    }

    public function setResource(?FHIRReference $value): self
    {
        $this->resource = $value;

        return $this;
    }
}
