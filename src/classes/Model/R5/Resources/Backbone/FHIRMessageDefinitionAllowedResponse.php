<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MessageDefinitionAllowedResponse Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMessageDefinitionAllowedResponseInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;

class FHIRMessageDefinitionAllowedResponse extends FHIRBackboneElement implements FHIRMessageDefinitionAllowedResponseInterface
{
    public const RESOURCE_NAME = 'MessageDefinition.allowedResponse';

    protected ?FHIRCanonical $message = null;
    protected ?FHIRMarkdown $situation = null;

    public function getMessage(): ?FHIRCanonical
    {
        return $this->message;
    }

    public function setMessage(string|FHIRCanonical|null $value): self
    {
        $this->message = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    public function getSituation(): ?FHIRMarkdown
    {
        return $this->situation;
    }

    public function setSituation(string|FHIRMarkdown|null $value): self
    {
        $this->situation = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }
}
