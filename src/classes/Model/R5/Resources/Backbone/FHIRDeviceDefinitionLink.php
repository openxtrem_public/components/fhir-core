<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceDefinitionLink Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceDefinitionLinkInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;

class FHIRDeviceDefinitionLink extends FHIRBackboneElement implements FHIRDeviceDefinitionLinkInterface
{
    public const RESOURCE_NAME = 'DeviceDefinition.link';

    protected ?FHIRCoding $relation = null;
    protected ?FHIRCodeableReference $relatedDevice = null;

    public function getRelation(): ?FHIRCoding
    {
        return $this->relation;
    }

    public function setRelation(?FHIRCoding $value): self
    {
        $this->relation = $value;

        return $this;
    }

    public function getRelatedDevice(): ?FHIRCodeableReference
    {
        return $this->relatedDevice;
    }

    public function setRelatedDevice(?FHIRCodeableReference $value): self
    {
        $this->relatedDevice = $value;

        return $this;
    }
}
