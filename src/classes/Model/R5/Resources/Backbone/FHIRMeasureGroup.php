<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MeasureGroup Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMeasureGroupInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRMeasureGroup extends FHIRBackboneElement implements FHIRMeasureGroupInterface
{
    public const RESOURCE_NAME = 'Measure.group';

    protected ?FHIRString $linkId = null;
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRCodeableConcept[] */
    protected array $type = [];
    protected FHIRCodeableConcept|FHIRReference|null $subject = null;
    protected ?FHIRCode $basis = null;
    protected ?FHIRCodeableConcept $scoring = null;
    protected ?FHIRCodeableConcept $scoringUnit = null;
    protected ?FHIRMarkdown $rateAggregation = null;
    protected ?FHIRCodeableConcept $improvementNotation = null;

    /** @var FHIRCanonical[] */
    protected array $library = [];

    /** @var FHIRMeasureGroupPopulation[] */
    protected array $population = [];

    /** @var FHIRMeasureGroupStratifier[] */
    protected array $stratifier = [];

    public function getLinkId(): ?FHIRString
    {
        return $this->linkId;
    }

    public function setLinkId(string|FHIRString|null $value): self
    {
        $this->linkId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }

    public function getSubject(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->subject;
    }

    public function setSubject(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getBasis(): ?FHIRCode
    {
        return $this->basis;
    }

    public function setBasis(string|FHIRCode|null $value): self
    {
        $this->basis = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getScoring(): ?FHIRCodeableConcept
    {
        return $this->scoring;
    }

    public function setScoring(?FHIRCodeableConcept $value): self
    {
        $this->scoring = $value;

        return $this;
    }

    public function getScoringUnit(): ?FHIRCodeableConcept
    {
        return $this->scoringUnit;
    }

    public function setScoringUnit(?FHIRCodeableConcept $value): self
    {
        $this->scoringUnit = $value;

        return $this;
    }

    public function getRateAggregation(): ?FHIRMarkdown
    {
        return $this->rateAggregation;
    }

    public function setRateAggregation(string|FHIRMarkdown|null $value): self
    {
        $this->rateAggregation = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getImprovementNotation(): ?FHIRCodeableConcept
    {
        return $this->improvementNotation;
    }

    public function setImprovementNotation(?FHIRCodeableConcept $value): self
    {
        $this->improvementNotation = $value;

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getLibrary(): array
    {
        return $this->library;
    }

    public function setLibrary(string|FHIRCanonical|null ...$value): self
    {
        $this->library = [];
        $this->addLibrary(...$value);

        return $this;
    }

    public function addLibrary(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->library = array_filter(array_merge($this->library, $values));

        return $this;
    }

    /**
     * @return FHIRMeasureGroupPopulation[]
     */
    public function getPopulation(): array
    {
        return $this->population;
    }

    public function setPopulation(?FHIRMeasureGroupPopulation ...$value): self
    {
        $this->population = array_filter($value);

        return $this;
    }

    public function addPopulation(?FHIRMeasureGroupPopulation ...$value): self
    {
        $this->population = array_filter(array_merge($this->population, $value));

        return $this;
    }

    /**
     * @return FHIRMeasureGroupStratifier[]
     */
    public function getStratifier(): array
    {
        return $this->stratifier;
    }

    public function setStratifier(?FHIRMeasureGroupStratifier ...$value): self
    {
        $this->stratifier = array_filter($value);

        return $this;
    }

    public function addStratifier(?FHIRMeasureGroupStratifier ...$value): self
    {
        $this->stratifier = array_filter(array_merge($this->stratifier, $value));

        return $this;
    }
}
