<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationStatementAdherence Backbone
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationStatementAdherenceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;

class FHIRMedicationStatementAdherence extends FHIRBackboneElement implements FHIRMedicationStatementAdherenceInterface
{
    public const RESOURCE_NAME = 'MedicationStatement.adherence';

    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRCodeableConcept $reason = null;

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getReason(): ?FHIRCodeableConcept
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableConcept $value): self
    {
        $this->reason = $value;

        return $this;
    }
}
