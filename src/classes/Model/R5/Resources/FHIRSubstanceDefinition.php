<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceDefinition Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRSubstanceDefinitionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRSubstanceDefinitionCharacterization;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRSubstanceDefinitionCode;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRSubstanceDefinitionMoiety;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRSubstanceDefinitionMolecularWeight;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRSubstanceDefinitionName;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRSubstanceDefinitionProperty;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRSubstanceDefinitionRelationship;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRSubstanceDefinitionSourceMaterial;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRSubstanceDefinitionStructure;

class FHIRSubstanceDefinition extends FHIRDomainResource implements FHIRSubstanceDefinitionInterface
{
    public const RESOURCE_NAME = 'SubstanceDefinition';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRString $version = null;
    protected ?FHIRCodeableConcept $status = null;

    /** @var FHIRCodeableConcept[] */
    protected array $classification = [];
    protected ?FHIRCodeableConcept $domain = null;

    /** @var FHIRCodeableConcept[] */
    protected array $grade = [];
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRReference[] */
    protected array $informationSource = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRReference[] */
    protected array $manufacturer = [];

    /** @var FHIRReference[] */
    protected array $supplier = [];

    /** @var FHIRSubstanceDefinitionMoiety[] */
    protected array $moiety = [];

    /** @var FHIRSubstanceDefinitionCharacterization[] */
    protected array $characterization = [];

    /** @var FHIRSubstanceDefinitionProperty[] */
    protected array $property = [];
    protected ?FHIRReference $referenceInformation = null;

    /** @var FHIRSubstanceDefinitionMolecularWeight[] */
    protected array $molecularWeight = [];
    protected ?FHIRSubstanceDefinitionStructure $structure = null;

    /** @var FHIRSubstanceDefinitionCode[] */
    protected array $code = [];

    /** @var FHIRSubstanceDefinitionName[] */
    protected array $name = [];

    /** @var FHIRSubstanceDefinitionRelationship[] */
    protected array $relationship = [];
    protected ?FHIRReference $nucleicAcid = null;
    protected ?FHIRReference $polymer = null;
    protected ?FHIRReference $protein = null;
    protected ?FHIRSubstanceDefinitionSourceMaterial $sourceMaterial = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStatus(): ?FHIRCodeableConcept
    {
        return $this->status;
    }

    public function setStatus(?FHIRCodeableConcept $value): self
    {
        $this->status = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getClassification(): array
    {
        return $this->classification;
    }

    public function setClassification(?FHIRCodeableConcept ...$value): self
    {
        $this->classification = array_filter($value);

        return $this;
    }

    public function addClassification(?FHIRCodeableConcept ...$value): self
    {
        $this->classification = array_filter(array_merge($this->classification, $value));

        return $this;
    }

    public function getDomain(): ?FHIRCodeableConcept
    {
        return $this->domain;
    }

    public function setDomain(?FHIRCodeableConcept $value): self
    {
        $this->domain = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getGrade(): array
    {
        return $this->grade;
    }

    public function setGrade(?FHIRCodeableConcept ...$value): self
    {
        $this->grade = array_filter($value);

        return $this;
    }

    public function addGrade(?FHIRCodeableConcept ...$value): self
    {
        $this->grade = array_filter(array_merge($this->grade, $value));

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getInformationSource(): array
    {
        return $this->informationSource;
    }

    public function setInformationSource(?FHIRReference ...$value): self
    {
        $this->informationSource = array_filter($value);

        return $this;
    }

    public function addInformationSource(?FHIRReference ...$value): self
    {
        $this->informationSource = array_filter(array_merge($this->informationSource, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getManufacturer(): array
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?FHIRReference ...$value): self
    {
        $this->manufacturer = array_filter($value);

        return $this;
    }

    public function addManufacturer(?FHIRReference ...$value): self
    {
        $this->manufacturer = array_filter(array_merge($this->manufacturer, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupplier(): array
    {
        return $this->supplier;
    }

    public function setSupplier(?FHIRReference ...$value): self
    {
        $this->supplier = array_filter($value);

        return $this;
    }

    public function addSupplier(?FHIRReference ...$value): self
    {
        $this->supplier = array_filter(array_merge($this->supplier, $value));

        return $this;
    }

    /**
     * @return FHIRSubstanceDefinitionMoiety[]
     */
    public function getMoiety(): array
    {
        return $this->moiety;
    }

    public function setMoiety(?FHIRSubstanceDefinitionMoiety ...$value): self
    {
        $this->moiety = array_filter($value);

        return $this;
    }

    public function addMoiety(?FHIRSubstanceDefinitionMoiety ...$value): self
    {
        $this->moiety = array_filter(array_merge($this->moiety, $value));

        return $this;
    }

    /**
     * @return FHIRSubstanceDefinitionCharacterization[]
     */
    public function getCharacterization(): array
    {
        return $this->characterization;
    }

    public function setCharacterization(?FHIRSubstanceDefinitionCharacterization ...$value): self
    {
        $this->characterization = array_filter($value);

        return $this;
    }

    public function addCharacterization(?FHIRSubstanceDefinitionCharacterization ...$value): self
    {
        $this->characterization = array_filter(array_merge($this->characterization, $value));

        return $this;
    }

    /**
     * @return FHIRSubstanceDefinitionProperty[]
     */
    public function getProperty(): array
    {
        return $this->property;
    }

    public function setProperty(?FHIRSubstanceDefinitionProperty ...$value): self
    {
        $this->property = array_filter($value);

        return $this;
    }

    public function addProperty(?FHIRSubstanceDefinitionProperty ...$value): self
    {
        $this->property = array_filter(array_merge($this->property, $value));

        return $this;
    }

    public function getReferenceInformation(): ?FHIRReference
    {
        return $this->referenceInformation;
    }

    public function setReferenceInformation(?FHIRReference $value): self
    {
        $this->referenceInformation = $value;

        return $this;
    }

    /**
     * @return FHIRSubstanceDefinitionMolecularWeight[]
     */
    public function getMolecularWeight(): array
    {
        return $this->molecularWeight;
    }

    public function setMolecularWeight(?FHIRSubstanceDefinitionMolecularWeight ...$value): self
    {
        $this->molecularWeight = array_filter($value);

        return $this;
    }

    public function addMolecularWeight(?FHIRSubstanceDefinitionMolecularWeight ...$value): self
    {
        $this->molecularWeight = array_filter(array_merge($this->molecularWeight, $value));

        return $this;
    }

    public function getStructure(): ?FHIRSubstanceDefinitionStructure
    {
        return $this->structure;
    }

    public function setStructure(?FHIRSubstanceDefinitionStructure $value): self
    {
        $this->structure = $value;

        return $this;
    }

    /**
     * @return FHIRSubstanceDefinitionCode[]
     */
    public function getCode(): array
    {
        return $this->code;
    }

    public function setCode(?FHIRSubstanceDefinitionCode ...$value): self
    {
        $this->code = array_filter($value);

        return $this;
    }

    public function addCode(?FHIRSubstanceDefinitionCode ...$value): self
    {
        $this->code = array_filter(array_merge($this->code, $value));

        return $this;
    }

    /**
     * @return FHIRSubstanceDefinitionName[]
     */
    public function getName(): array
    {
        return $this->name;
    }

    public function setName(?FHIRSubstanceDefinitionName ...$value): self
    {
        $this->name = array_filter($value);

        return $this;
    }

    public function addName(?FHIRSubstanceDefinitionName ...$value): self
    {
        $this->name = array_filter(array_merge($this->name, $value));

        return $this;
    }

    /**
     * @return FHIRSubstanceDefinitionRelationship[]
     */
    public function getRelationship(): array
    {
        return $this->relationship;
    }

    public function setRelationship(?FHIRSubstanceDefinitionRelationship ...$value): self
    {
        $this->relationship = array_filter($value);

        return $this;
    }

    public function addRelationship(?FHIRSubstanceDefinitionRelationship ...$value): self
    {
        $this->relationship = array_filter(array_merge($this->relationship, $value));

        return $this;
    }

    public function getNucleicAcid(): ?FHIRReference
    {
        return $this->nucleicAcid;
    }

    public function setNucleicAcid(?FHIRReference $value): self
    {
        $this->nucleicAcid = $value;

        return $this;
    }

    public function getPolymer(): ?FHIRReference
    {
        return $this->polymer;
    }

    public function setPolymer(?FHIRReference $value): self
    {
        $this->polymer = $value;

        return $this;
    }

    public function getProtein(): ?FHIRReference
    {
        return $this->protein;
    }

    public function setProtein(?FHIRReference $value): self
    {
        $this->protein = $value;

        return $this;
    }

    public function getSourceMaterial(): ?FHIRSubstanceDefinitionSourceMaterial
    {
        return $this->sourceMaterial;
    }

    public function setSourceMaterial(?FHIRSubstanceDefinitionSourceMaterial $value): self
    {
        $this->sourceMaterial = $value;

        return $this;
    }
}
