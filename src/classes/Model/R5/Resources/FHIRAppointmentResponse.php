<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AppointmentResponse Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRAppointmentResponseInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;

class FHIRAppointmentResponse extends FHIRDomainResource implements FHIRAppointmentResponseInterface
{
    public const RESOURCE_NAME = 'AppointmentResponse';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRReference $appointment = null;
    protected ?FHIRBoolean $proposedNewTime = null;
    protected ?FHIRInstant $start = null;
    protected ?FHIRInstant $end = null;

    /** @var FHIRCodeableConcept[] */
    protected array $participantType = [];
    protected ?FHIRReference $actor = null;
    protected ?FHIRCode $participantStatus = null;
    protected ?FHIRMarkdown $comment = null;
    protected ?FHIRBoolean $recurring = null;
    protected ?FHIRDate $occurrenceDate = null;
    protected ?FHIRPositiveInt $recurrenceId = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getAppointment(): ?FHIRReference
    {
        return $this->appointment;
    }

    public function setAppointment(?FHIRReference $value): self
    {
        $this->appointment = $value;

        return $this;
    }

    public function getProposedNewTime(): ?FHIRBoolean
    {
        return $this->proposedNewTime;
    }

    public function setProposedNewTime(bool|FHIRBoolean|null $value): self
    {
        $this->proposedNewTime = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getStart(): ?FHIRInstant
    {
        return $this->start;
    }

    public function setStart(string|FHIRInstant|null $value): self
    {
        $this->start = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    public function getEnd(): ?FHIRInstant
    {
        return $this->end;
    }

    public function setEnd(string|FHIRInstant|null $value): self
    {
        $this->end = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getParticipantType(): array
    {
        return $this->participantType;
    }

    public function setParticipantType(?FHIRCodeableConcept ...$value): self
    {
        $this->participantType = array_filter($value);

        return $this;
    }

    public function addParticipantType(?FHIRCodeableConcept ...$value): self
    {
        $this->participantType = array_filter(array_merge($this->participantType, $value));

        return $this;
    }

    public function getActor(): ?FHIRReference
    {
        return $this->actor;
    }

    public function setActor(?FHIRReference $value): self
    {
        $this->actor = $value;

        return $this;
    }

    public function getParticipantStatus(): ?FHIRCode
    {
        return $this->participantStatus;
    }

    public function setParticipantStatus(string|FHIRCode|null $value): self
    {
        $this->participantStatus = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getComment(): ?FHIRMarkdown
    {
        return $this->comment;
    }

    public function setComment(string|FHIRMarkdown|null $value): self
    {
        $this->comment = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getRecurring(): ?FHIRBoolean
    {
        return $this->recurring;
    }

    public function setRecurring(bool|FHIRBoolean|null $value): self
    {
        $this->recurring = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getOccurrenceDate(): ?FHIRDate
    {
        return $this->occurrenceDate;
    }

    public function setOccurrenceDate(string|FHIRDate|null $value): self
    {
        $this->occurrenceDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getRecurrenceId(): ?FHIRPositiveInt
    {
        return $this->recurrenceId;
    }

    public function setRecurrenceId(int|FHIRPositiveInt|null $value): self
    {
        $this->recurrenceId = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }
}
