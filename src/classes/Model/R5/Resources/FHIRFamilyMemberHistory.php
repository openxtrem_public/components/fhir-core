<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR FamilyMemberHistory Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRFamilyMemberHistoryInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAge;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRFamilyMemberHistoryCondition;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRFamilyMemberHistoryParticipant;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRFamilyMemberHistoryProcedure;

class FHIRFamilyMemberHistory extends FHIRDomainResource implements FHIRFamilyMemberHistoryInterface
{
    public const RESOURCE_NAME = 'FamilyMemberHistory';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRCanonical[] */
    protected array $instantiatesCanonical = [];

    /** @var FHIRUri[] */
    protected array $instantiatesUri = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $dataAbsentReason = null;
    protected ?FHIRReference $patient = null;
    protected ?FHIRDateTime $date = null;

    /** @var FHIRFamilyMemberHistoryParticipant[] */
    protected array $participant = [];
    protected ?FHIRString $name = null;
    protected ?FHIRCodeableConcept $relationship = null;
    protected ?FHIRCodeableConcept $sex = null;
    protected FHIRPeriod|FHIRDate|FHIRString|null $born = null;
    protected FHIRAge|FHIRRange|FHIRString|null $age = null;
    protected ?FHIRBoolean $estimatedAge = null;
    protected FHIRBoolean|FHIRAge|FHIRRange|FHIRDate|FHIRString|null $deceased = null;

    /** @var FHIRCodeableReference[] */
    protected array $reason = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRFamilyMemberHistoryCondition[] */
    protected array $condition = [];

    /** @var FHIRFamilyMemberHistoryProcedure[] */
    protected array $procedure = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getInstantiatesCanonical(): array
    {
        return $this->instantiatesCanonical;
    }

    public function setInstantiatesCanonical(string|FHIRCanonical|null ...$value): self
    {
        $this->instantiatesCanonical = [];
        $this->addInstantiatesCanonical(...$value);

        return $this;
    }

    public function addInstantiatesCanonical(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->instantiatesCanonical = array_filter(array_merge($this->instantiatesCanonical, $values));

        return $this;
    }

    /**
     * @return FHIRUri[]
     */
    public function getInstantiatesUri(): array
    {
        return $this->instantiatesUri;
    }

    public function setInstantiatesUri(string|FHIRUri|null ...$value): self
    {
        $this->instantiatesUri = [];
        $this->addInstantiatesUri(...$value);

        return $this;
    }

    public function addInstantiatesUri(string|FHIRUri|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUri())->setValue($v) : $v, $value);

        $this->instantiatesUri = array_filter(array_merge($this->instantiatesUri, $values));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDataAbsentReason(): ?FHIRCodeableConcept
    {
        return $this->dataAbsentReason;
    }

    public function setDataAbsentReason(?FHIRCodeableConcept $value): self
    {
        $this->dataAbsentReason = $value;

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRFamilyMemberHistoryParticipant[]
     */
    public function getParticipant(): array
    {
        return $this->participant;
    }

    public function setParticipant(?FHIRFamilyMemberHistoryParticipant ...$value): self
    {
        $this->participant = array_filter($value);

        return $this;
    }

    public function addParticipant(?FHIRFamilyMemberHistoryParticipant ...$value): self
    {
        $this->participant = array_filter(array_merge($this->participant, $value));

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getRelationship(): ?FHIRCodeableConcept
    {
        return $this->relationship;
    }

    public function setRelationship(?FHIRCodeableConcept $value): self
    {
        $this->relationship = $value;

        return $this;
    }

    public function getSex(): ?FHIRCodeableConcept
    {
        return $this->sex;
    }

    public function setSex(?FHIRCodeableConcept $value): self
    {
        $this->sex = $value;

        return $this;
    }

    public function getBorn(): FHIRPeriod|FHIRDate|FHIRString|null
    {
        return $this->born;
    }

    public function setBorn(FHIRPeriod|FHIRDate|FHIRString|null $value): self
    {
        $this->born = $value;

        return $this;
    }

    public function getAge(): FHIRAge|FHIRRange|FHIRString|null
    {
        return $this->age;
    }

    public function setAge(FHIRAge|FHIRRange|FHIRString|null $value): self
    {
        $this->age = $value;

        return $this;
    }

    public function getEstimatedAge(): ?FHIRBoolean
    {
        return $this->estimatedAge;
    }

    public function setEstimatedAge(bool|FHIRBoolean|null $value): self
    {
        $this->estimatedAge = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getDeceased(): FHIRBoolean|FHIRAge|FHIRRange|FHIRDate|FHIRString|null
    {
        return $this->deceased;
    }

    public function setDeceased(FHIRBoolean|FHIRAge|FHIRRange|FHIRDate|FHIRString|null $value): self
    {
        $this->deceased = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getReason(): array
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableReference ...$value): self
    {
        $this->reason = array_filter($value);

        return $this;
    }

    public function addReason(?FHIRCodeableReference ...$value): self
    {
        $this->reason = array_filter(array_merge($this->reason, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRFamilyMemberHistoryCondition[]
     */
    public function getCondition(): array
    {
        return $this->condition;
    }

    public function setCondition(?FHIRFamilyMemberHistoryCondition ...$value): self
    {
        $this->condition = array_filter($value);

        return $this;
    }

    public function addCondition(?FHIRFamilyMemberHistoryCondition ...$value): self
    {
        $this->condition = array_filter(array_merge($this->condition, $value));

        return $this;
    }

    /**
     * @return FHIRFamilyMemberHistoryProcedure[]
     */
    public function getProcedure(): array
    {
        return $this->procedure;
    }

    public function setProcedure(?FHIRFamilyMemberHistoryProcedure ...$value): self
    {
        $this->procedure = array_filter($value);

        return $this;
    }

    public function addProcedure(?FHIRFamilyMemberHistoryProcedure ...$value): self
    {
        $this->procedure = array_filter(array_merge($this->procedure, $value));

        return $this;
    }
}
