<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AuditEvent Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRAuditEventInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRAuditEventAgent;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRAuditEventEntity;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRAuditEventOutcome;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRAuditEventSource;

class FHIRAuditEvent extends FHIRDomainResource implements FHIRAuditEventInterface
{
    public const RESOURCE_NAME = 'AuditEvent';

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRCode $action = null;
    protected ?FHIRCode $severity = null;
    protected FHIRPeriod|FHIRDateTime|null $occurred = null;
    protected ?FHIRInstant $recorded = null;
    protected ?FHIRAuditEventOutcome $outcome = null;

    /** @var FHIRCodeableConcept[] */
    protected array $authorization = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];
    protected ?FHIRReference $patient = null;
    protected ?FHIRReference $encounter = null;

    /** @var FHIRAuditEventAgent[] */
    protected array $agent = [];
    protected ?FHIRAuditEventSource $source = null;

    /** @var FHIRAuditEventEntity[] */
    protected array $entity = [];

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getAction(): ?FHIRCode
    {
        return $this->action;
    }

    public function setAction(string|FHIRCode|null $value): self
    {
        $this->action = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getSeverity(): ?FHIRCode
    {
        return $this->severity;
    }

    public function setSeverity(string|FHIRCode|null $value): self
    {
        $this->severity = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getOccurred(): FHIRPeriod|FHIRDateTime|null
    {
        return $this->occurred;
    }

    public function setOccurred(FHIRPeriod|FHIRDateTime|null $value): self
    {
        $this->occurred = $value;

        return $this;
    }

    public function getRecorded(): ?FHIRInstant
    {
        return $this->recorded;
    }

    public function setRecorded(string|FHIRInstant|null $value): self
    {
        $this->recorded = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    public function getOutcome(): ?FHIRAuditEventOutcome
    {
        return $this->outcome;
    }

    public function setOutcome(?FHIRAuditEventOutcome $value): self
    {
        $this->outcome = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getAuthorization(): array
    {
        return $this->authorization;
    }

    public function setAuthorization(?FHIRCodeableConcept ...$value): self
    {
        $this->authorization = array_filter($value);

        return $this;
    }

    public function addAuthorization(?FHIRCodeableConcept ...$value): self
    {
        $this->authorization = array_filter(array_merge($this->authorization, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    /**
     * @return FHIRAuditEventAgent[]
     */
    public function getAgent(): array
    {
        return $this->agent;
    }

    public function setAgent(?FHIRAuditEventAgent ...$value): self
    {
        $this->agent = array_filter($value);

        return $this;
    }

    public function addAgent(?FHIRAuditEventAgent ...$value): self
    {
        $this->agent = array_filter(array_merge($this->agent, $value));

        return $this;
    }

    public function getSource(): ?FHIRAuditEventSource
    {
        return $this->source;
    }

    public function setSource(?FHIRAuditEventSource $value): self
    {
        $this->source = $value;

        return $this;
    }

    /**
     * @return FHIRAuditEventEntity[]
     */
    public function getEntity(): array
    {
        return $this->entity;
    }

    public function setEntity(?FHIRAuditEventEntity ...$value): self
    {
        $this->entity = array_filter($value);

        return $this;
    }

    public function addEntity(?FHIRAuditEventEntity ...$value): self
    {
        $this->entity = array_filter(array_merge($this->entity, $value));

        return $this;
    }
}
