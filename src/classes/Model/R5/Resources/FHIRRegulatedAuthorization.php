<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR RegulatedAuthorization Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRRegulatedAuthorizationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRRegulatedAuthorizationCase;

class FHIRRegulatedAuthorization extends FHIRDomainResource implements FHIRRegulatedAuthorizationInterface
{
    public const RESOURCE_NAME = 'RegulatedAuthorization';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRReference[] */
    protected array $subject = [];
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRCodeableConcept[] */
    protected array $region = [];
    protected ?FHIRCodeableConcept $status = null;
    protected ?FHIRDateTime $statusDate = null;
    protected ?FHIRPeriod $validityPeriod = null;

    /** @var FHIRCodeableReference[] */
    protected array $indication = [];
    protected ?FHIRCodeableConcept $intendedUse = null;

    /** @var FHIRCodeableConcept[] */
    protected array $basis = [];
    protected ?FHIRReference $holder = null;
    protected ?FHIRReference $regulator = null;

    /** @var FHIRReference[] */
    protected array $attachedDocument = [];
    protected ?FHIRRegulatedAuthorizationCase $case = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSubject(): array
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference ...$value): self
    {
        $this->subject = array_filter($value);

        return $this;
    }

    public function addSubject(?FHIRReference ...$value): self
    {
        $this->subject = array_filter(array_merge($this->subject, $value));

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getRegion(): array
    {
        return $this->region;
    }

    public function setRegion(?FHIRCodeableConcept ...$value): self
    {
        $this->region = array_filter($value);

        return $this;
    }

    public function addRegion(?FHIRCodeableConcept ...$value): self
    {
        $this->region = array_filter(array_merge($this->region, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCodeableConcept
    {
        return $this->status;
    }

    public function setStatus(?FHIRCodeableConcept $value): self
    {
        $this->status = $value;

        return $this;
    }

    public function getStatusDate(): ?FHIRDateTime
    {
        return $this->statusDate;
    }

    public function setStatusDate(string|FHIRDateTime|null $value): self
    {
        $this->statusDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getValidityPeriod(): ?FHIRPeriod
    {
        return $this->validityPeriod;
    }

    public function setValidityPeriod(?FHIRPeriod $value): self
    {
        $this->validityPeriod = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getIndication(): array
    {
        return $this->indication;
    }

    public function setIndication(?FHIRCodeableReference ...$value): self
    {
        $this->indication = array_filter($value);

        return $this;
    }

    public function addIndication(?FHIRCodeableReference ...$value): self
    {
        $this->indication = array_filter(array_merge($this->indication, $value));

        return $this;
    }

    public function getIntendedUse(): ?FHIRCodeableConcept
    {
        return $this->intendedUse;
    }

    public function setIntendedUse(?FHIRCodeableConcept $value): self
    {
        $this->intendedUse = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getBasis(): array
    {
        return $this->basis;
    }

    public function setBasis(?FHIRCodeableConcept ...$value): self
    {
        $this->basis = array_filter($value);

        return $this;
    }

    public function addBasis(?FHIRCodeableConcept ...$value): self
    {
        $this->basis = array_filter(array_merge($this->basis, $value));

        return $this;
    }

    public function getHolder(): ?FHIRReference
    {
        return $this->holder;
    }

    public function setHolder(?FHIRReference $value): self
    {
        $this->holder = $value;

        return $this;
    }

    public function getRegulator(): ?FHIRReference
    {
        return $this->regulator;
    }

    public function setRegulator(?FHIRReference $value): self
    {
        $this->regulator = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAttachedDocument(): array
    {
        return $this->attachedDocument;
    }

    public function setAttachedDocument(?FHIRReference ...$value): self
    {
        $this->attachedDocument = array_filter($value);

        return $this;
    }

    public function addAttachedDocument(?FHIRReference ...$value): self
    {
        $this->attachedDocument = array_filter(array_merge($this->attachedDocument, $value));

        return $this;
    }

    public function getCase(): ?FHIRRegulatedAuthorizationCase
    {
        return $this->case;
    }

    public function setCase(?FHIRRegulatedAuthorizationCase $value): self
    {
        $this->case = $value;

        return $this;
    }
}
