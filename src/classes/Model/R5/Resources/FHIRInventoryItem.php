<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR InventoryItem Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRInventoryItemInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRInventoryItemAssociation;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRInventoryItemCharacteristic;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRInventoryItemDescription;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRInventoryItemInstance;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRInventoryItemName;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRInventoryItemResponsibleOrganization;

class FHIRInventoryItem extends FHIRDomainResource implements FHIRInventoryItemInterface
{
    public const RESOURCE_NAME = 'InventoryItem';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];

    /** @var FHIRCodeableConcept[] */
    protected array $code = [];

    /** @var FHIRInventoryItemName[] */
    protected array $name = [];

    /** @var FHIRInventoryItemResponsibleOrganization[] */
    protected array $responsibleOrganization = [];
    protected ?FHIRInventoryItemDescription $description = null;

    /** @var FHIRCodeableConcept[] */
    protected array $inventoryStatus = [];
    protected ?FHIRCodeableConcept $baseUnit = null;
    protected ?FHIRQuantity $netContent = null;

    /** @var FHIRInventoryItemAssociation[] */
    protected array $association = [];

    /** @var FHIRInventoryItemCharacteristic[] */
    protected array $characteristic = [];
    protected ?FHIRInventoryItemInstance $instance = null;
    protected ?FHIRReference $productReference = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCode(): array
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept ...$value): self
    {
        $this->code = array_filter($value);

        return $this;
    }

    public function addCode(?FHIRCodeableConcept ...$value): self
    {
        $this->code = array_filter(array_merge($this->code, $value));

        return $this;
    }

    /**
     * @return FHIRInventoryItemName[]
     */
    public function getName(): array
    {
        return $this->name;
    }

    public function setName(?FHIRInventoryItemName ...$value): self
    {
        $this->name = array_filter($value);

        return $this;
    }

    public function addName(?FHIRInventoryItemName ...$value): self
    {
        $this->name = array_filter(array_merge($this->name, $value));

        return $this;
    }

    /**
     * @return FHIRInventoryItemResponsibleOrganization[]
     */
    public function getResponsibleOrganization(): array
    {
        return $this->responsibleOrganization;
    }

    public function setResponsibleOrganization(?FHIRInventoryItemResponsibleOrganization ...$value): self
    {
        $this->responsibleOrganization = array_filter($value);

        return $this;
    }

    public function addResponsibleOrganization(?FHIRInventoryItemResponsibleOrganization ...$value): self
    {
        $this->responsibleOrganization = array_filter(array_merge($this->responsibleOrganization, $value));

        return $this;
    }

    public function getDescription(): ?FHIRInventoryItemDescription
    {
        return $this->description;
    }

    public function setDescription(?FHIRInventoryItemDescription $value): self
    {
        $this->description = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getInventoryStatus(): array
    {
        return $this->inventoryStatus;
    }

    public function setInventoryStatus(?FHIRCodeableConcept ...$value): self
    {
        $this->inventoryStatus = array_filter($value);

        return $this;
    }

    public function addInventoryStatus(?FHIRCodeableConcept ...$value): self
    {
        $this->inventoryStatus = array_filter(array_merge($this->inventoryStatus, $value));

        return $this;
    }

    public function getBaseUnit(): ?FHIRCodeableConcept
    {
        return $this->baseUnit;
    }

    public function setBaseUnit(?FHIRCodeableConcept $value): self
    {
        $this->baseUnit = $value;

        return $this;
    }

    public function getNetContent(): ?FHIRQuantity
    {
        return $this->netContent;
    }

    public function setNetContent(?FHIRQuantity $value): self
    {
        $this->netContent = $value;

        return $this;
    }

    /**
     * @return FHIRInventoryItemAssociation[]
     */
    public function getAssociation(): array
    {
        return $this->association;
    }

    public function setAssociation(?FHIRInventoryItemAssociation ...$value): self
    {
        $this->association = array_filter($value);

        return $this;
    }

    public function addAssociation(?FHIRInventoryItemAssociation ...$value): self
    {
        $this->association = array_filter(array_merge($this->association, $value));

        return $this;
    }

    /**
     * @return FHIRInventoryItemCharacteristic[]
     */
    public function getCharacteristic(): array
    {
        return $this->characteristic;
    }

    public function setCharacteristic(?FHIRInventoryItemCharacteristic ...$value): self
    {
        $this->characteristic = array_filter($value);

        return $this;
    }

    public function addCharacteristic(?FHIRInventoryItemCharacteristic ...$value): self
    {
        $this->characteristic = array_filter(array_merge($this->characteristic, $value));

        return $this;
    }

    public function getInstance(): ?FHIRInventoryItemInstance
    {
        return $this->instance;
    }

    public function setInstance(?FHIRInventoryItemInstance $value): self
    {
        $this->instance = $value;

        return $this;
    }

    public function getProductReference(): ?FHIRReference
    {
        return $this->productReference;
    }

    public function setProductReference(?FHIRReference $value): self
    {
        $this->productReference = $value;

        return $this;
    }
}
