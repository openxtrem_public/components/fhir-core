<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR GenomicStudy Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRGenomicStudyInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRGenomicStudyAnalysis;

class FHIRGenomicStudy extends FHIRDomainResource implements FHIRGenomicStudyInterface
{
    public const RESOURCE_NAME = 'GenomicStudy';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;

    /** @var FHIRCodeableConcept[] */
    protected array $type = [];
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $encounter = null;
    protected ?FHIRDateTime $startDate = null;

    /** @var FHIRReference[] */
    protected array $basedOn = [];
    protected ?FHIRReference $referrer = null;

    /** @var FHIRReference[] */
    protected array $interpreter = [];

    /** @var FHIRCodeableReference[] */
    protected array $reason = [];
    protected ?FHIRCanonical $instantiatesCanonical = null;
    protected ?FHIRUri $instantiatesUri = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRGenomicStudyAnalysis[] */
    protected array $analysis = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getStartDate(): ?FHIRDateTime
    {
        return $this->startDate;
    }

    public function setStartDate(string|FHIRDateTime|null $value): self
    {
        $this->startDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    public function getReferrer(): ?FHIRReference
    {
        return $this->referrer;
    }

    public function setReferrer(?FHIRReference $value): self
    {
        $this->referrer = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getInterpreter(): array
    {
        return $this->interpreter;
    }

    public function setInterpreter(?FHIRReference ...$value): self
    {
        $this->interpreter = array_filter($value);

        return $this;
    }

    public function addInterpreter(?FHIRReference ...$value): self
    {
        $this->interpreter = array_filter(array_merge($this->interpreter, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getReason(): array
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableReference ...$value): self
    {
        $this->reason = array_filter($value);

        return $this;
    }

    public function addReason(?FHIRCodeableReference ...$value): self
    {
        $this->reason = array_filter(array_merge($this->reason, $value));

        return $this;
    }

    public function getInstantiatesCanonical(): ?FHIRCanonical
    {
        return $this->instantiatesCanonical;
    }

    public function setInstantiatesCanonical(string|FHIRCanonical|null $value): self
    {
        $this->instantiatesCanonical = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    public function getInstantiatesUri(): ?FHIRUri
    {
        return $this->instantiatesUri;
    }

    public function setInstantiatesUri(string|FHIRUri|null $value): self
    {
        $this->instantiatesUri = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRGenomicStudyAnalysis[]
     */
    public function getAnalysis(): array
    {
        return $this->analysis;
    }

    public function setAnalysis(?FHIRGenomicStudyAnalysis ...$value): self
    {
        $this->analysis = array_filter($value);

        return $this;
    }

    public function addAnalysis(?FHIRGenomicStudyAnalysis ...$value): self
    {
        $this->analysis = array_filter(array_merge($this->analysis, $value));

        return $this;
    }
}
