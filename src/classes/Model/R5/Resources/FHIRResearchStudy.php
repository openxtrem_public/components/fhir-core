<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ResearchStudy Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRResearchStudyInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRelatedArtifact;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRResearchStudyAssociatedParty;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRResearchStudyComparisonGroup;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRResearchStudyLabel;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRResearchStudyObjective;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRResearchStudyOutcomeMeasure;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRResearchStudyProgressStatus;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRResearchStudyRecruitment;

class FHIRResearchStudy extends FHIRDomainResource implements FHIRResearchStudyInterface
{
    public const RESOURCE_NAME = 'ResearchStudy';

    protected ?FHIRUri $url = null;

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRString $version = null;
    protected ?FHIRString $name = null;
    protected ?FHIRString $title = null;

    /** @var FHIRResearchStudyLabel[] */
    protected array $label = [];

    /** @var FHIRReference[] */
    protected array $protocol = [];

    /** @var FHIRReference[] */
    protected array $partOf = [];

    /** @var FHIRRelatedArtifact[] */
    protected array $relatedArtifact = [];
    protected ?FHIRDateTime $date = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $primaryPurposeType = null;
    protected ?FHIRCodeableConcept $phase = null;

    /** @var FHIRCodeableConcept[] */
    protected array $studyDesign = [];

    /** @var FHIRCodeableReference[] */
    protected array $focus = [];

    /** @var FHIRCodeableConcept[] */
    protected array $condition = [];

    /** @var FHIRCodeableConcept[] */
    protected array $keyword = [];

    /** @var FHIRCodeableConcept[] */
    protected array $region = [];
    protected ?FHIRMarkdown $descriptionSummary = null;
    protected ?FHIRMarkdown $description = null;
    protected ?FHIRPeriod $period = null;

    /** @var FHIRReference[] */
    protected array $site = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRCodeableConcept[] */
    protected array $classifier = [];

    /** @var FHIRResearchStudyAssociatedParty[] */
    protected array $associatedParty = [];

    /** @var FHIRResearchStudyProgressStatus[] */
    protected array $progressStatus = [];
    protected ?FHIRCodeableConcept $whyStopped = null;
    protected ?FHIRResearchStudyRecruitment $recruitment = null;

    /** @var FHIRResearchStudyComparisonGroup[] */
    protected array $comparisonGroup = [];

    /** @var FHIRResearchStudyObjective[] */
    protected array $objective = [];

    /** @var FHIRResearchStudyOutcomeMeasure[] */
    protected array $outcomeMeasure = [];

    /** @var FHIRReference[] */
    protected array $result = [];

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRResearchStudyLabel[]
     */
    public function getLabel(): array
    {
        return $this->label;
    }

    public function setLabel(?FHIRResearchStudyLabel ...$value): self
    {
        $this->label = array_filter($value);

        return $this;
    }

    public function addLabel(?FHIRResearchStudyLabel ...$value): self
    {
        $this->label = array_filter(array_merge($this->label, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getProtocol(): array
    {
        return $this->protocol;
    }

    public function setProtocol(?FHIRReference ...$value): self
    {
        $this->protocol = array_filter($value);

        return $this;
    }

    public function addProtocol(?FHIRReference ...$value): self
    {
        $this->protocol = array_filter(array_merge($this->protocol, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPartOf(): array
    {
        return $this->partOf;
    }

    public function setPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter($value);

        return $this;
    }

    public function addPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter(array_merge($this->partOf, $value));

        return $this;
    }

    /**
     * @return FHIRRelatedArtifact[]
     */
    public function getRelatedArtifact(): array
    {
        return $this->relatedArtifact;
    }

    public function setRelatedArtifact(?FHIRRelatedArtifact ...$value): self
    {
        $this->relatedArtifact = array_filter($value);

        return $this;
    }

    public function addRelatedArtifact(?FHIRRelatedArtifact ...$value): self
    {
        $this->relatedArtifact = array_filter(array_merge($this->relatedArtifact, $value));

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getPrimaryPurposeType(): ?FHIRCodeableConcept
    {
        return $this->primaryPurposeType;
    }

    public function setPrimaryPurposeType(?FHIRCodeableConcept $value): self
    {
        $this->primaryPurposeType = $value;

        return $this;
    }

    public function getPhase(): ?FHIRCodeableConcept
    {
        return $this->phase;
    }

    public function setPhase(?FHIRCodeableConcept $value): self
    {
        $this->phase = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getStudyDesign(): array
    {
        return $this->studyDesign;
    }

    public function setStudyDesign(?FHIRCodeableConcept ...$value): self
    {
        $this->studyDesign = array_filter($value);

        return $this;
    }

    public function addStudyDesign(?FHIRCodeableConcept ...$value): self
    {
        $this->studyDesign = array_filter(array_merge($this->studyDesign, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getFocus(): array
    {
        return $this->focus;
    }

    public function setFocus(?FHIRCodeableReference ...$value): self
    {
        $this->focus = array_filter($value);

        return $this;
    }

    public function addFocus(?FHIRCodeableReference ...$value): self
    {
        $this->focus = array_filter(array_merge($this->focus, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCondition(): array
    {
        return $this->condition;
    }

    public function setCondition(?FHIRCodeableConcept ...$value): self
    {
        $this->condition = array_filter($value);

        return $this;
    }

    public function addCondition(?FHIRCodeableConcept ...$value): self
    {
        $this->condition = array_filter(array_merge($this->condition, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getKeyword(): array
    {
        return $this->keyword;
    }

    public function setKeyword(?FHIRCodeableConcept ...$value): self
    {
        $this->keyword = array_filter($value);

        return $this;
    }

    public function addKeyword(?FHIRCodeableConcept ...$value): self
    {
        $this->keyword = array_filter(array_merge($this->keyword, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getRegion(): array
    {
        return $this->region;
    }

    public function setRegion(?FHIRCodeableConcept ...$value): self
    {
        $this->region = array_filter($value);

        return $this;
    }

    public function addRegion(?FHIRCodeableConcept ...$value): self
    {
        $this->region = array_filter(array_merge($this->region, $value));

        return $this;
    }

    public function getDescriptionSummary(): ?FHIRMarkdown
    {
        return $this->descriptionSummary;
    }

    public function setDescriptionSummary(string|FHIRMarkdown|null $value): self
    {
        $this->descriptionSummary = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSite(): array
    {
        return $this->site;
    }

    public function setSite(?FHIRReference ...$value): self
    {
        $this->site = array_filter($value);

        return $this;
    }

    public function addSite(?FHIRReference ...$value): self
    {
        $this->site = array_filter(array_merge($this->site, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getClassifier(): array
    {
        return $this->classifier;
    }

    public function setClassifier(?FHIRCodeableConcept ...$value): self
    {
        $this->classifier = array_filter($value);

        return $this;
    }

    public function addClassifier(?FHIRCodeableConcept ...$value): self
    {
        $this->classifier = array_filter(array_merge($this->classifier, $value));

        return $this;
    }

    /**
     * @return FHIRResearchStudyAssociatedParty[]
     */
    public function getAssociatedParty(): array
    {
        return $this->associatedParty;
    }

    public function setAssociatedParty(?FHIRResearchStudyAssociatedParty ...$value): self
    {
        $this->associatedParty = array_filter($value);

        return $this;
    }

    public function addAssociatedParty(?FHIRResearchStudyAssociatedParty ...$value): self
    {
        $this->associatedParty = array_filter(array_merge($this->associatedParty, $value));

        return $this;
    }

    /**
     * @return FHIRResearchStudyProgressStatus[]
     */
    public function getProgressStatus(): array
    {
        return $this->progressStatus;
    }

    public function setProgressStatus(?FHIRResearchStudyProgressStatus ...$value): self
    {
        $this->progressStatus = array_filter($value);

        return $this;
    }

    public function addProgressStatus(?FHIRResearchStudyProgressStatus ...$value): self
    {
        $this->progressStatus = array_filter(array_merge($this->progressStatus, $value));

        return $this;
    }

    public function getWhyStopped(): ?FHIRCodeableConcept
    {
        return $this->whyStopped;
    }

    public function setWhyStopped(?FHIRCodeableConcept $value): self
    {
        $this->whyStopped = $value;

        return $this;
    }

    public function getRecruitment(): ?FHIRResearchStudyRecruitment
    {
        return $this->recruitment;
    }

    public function setRecruitment(?FHIRResearchStudyRecruitment $value): self
    {
        $this->recruitment = $value;

        return $this;
    }

    /**
     * @return FHIRResearchStudyComparisonGroup[]
     */
    public function getComparisonGroup(): array
    {
        return $this->comparisonGroup;
    }

    public function setComparisonGroup(?FHIRResearchStudyComparisonGroup ...$value): self
    {
        $this->comparisonGroup = array_filter($value);

        return $this;
    }

    public function addComparisonGroup(?FHIRResearchStudyComparisonGroup ...$value): self
    {
        $this->comparisonGroup = array_filter(array_merge($this->comparisonGroup, $value));

        return $this;
    }

    /**
     * @return FHIRResearchStudyObjective[]
     */
    public function getObjective(): array
    {
        return $this->objective;
    }

    public function setObjective(?FHIRResearchStudyObjective ...$value): self
    {
        $this->objective = array_filter($value);

        return $this;
    }

    public function addObjective(?FHIRResearchStudyObjective ...$value): self
    {
        $this->objective = array_filter(array_merge($this->objective, $value));

        return $this;
    }

    /**
     * @return FHIRResearchStudyOutcomeMeasure[]
     */
    public function getOutcomeMeasure(): array
    {
        return $this->outcomeMeasure;
    }

    public function setOutcomeMeasure(?FHIRResearchStudyOutcomeMeasure ...$value): self
    {
        $this->outcomeMeasure = array_filter($value);

        return $this;
    }

    public function addOutcomeMeasure(?FHIRResearchStudyOutcomeMeasure ...$value): self
    {
        $this->outcomeMeasure = array_filter(array_merge($this->outcomeMeasure, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getResult(): array
    {
        return $this->result;
    }

    public function setResult(?FHIRReference ...$value): self
    {
        $this->result = array_filter($value);

        return $this;
    }

    public function addResult(?FHIRReference ...$value): self
    {
        $this->result = array_filter(array_merge($this->result, $value));

        return $this;
    }
}
