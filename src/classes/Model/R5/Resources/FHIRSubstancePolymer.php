<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstancePolymer Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRSubstancePolymerInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRSubstancePolymerMonomerSet;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRSubstancePolymerRepeat;

class FHIRSubstancePolymer extends FHIRDomainResource implements FHIRSubstancePolymerInterface
{
    public const RESOURCE_NAME = 'SubstancePolymer';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRCodeableConcept $class = null;
    protected ?FHIRCodeableConcept $geometry = null;

    /** @var FHIRCodeableConcept[] */
    protected array $copolymerConnectivity = [];
    protected ?FHIRString $modification = null;

    /** @var FHIRSubstancePolymerMonomerSet[] */
    protected array $monomerSet = [];

    /** @var FHIRSubstancePolymerRepeat[] */
    protected array $repeat = [];

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getClass(): ?FHIRCodeableConcept
    {
        return $this->class;
    }

    public function setClass(?FHIRCodeableConcept $value): self
    {
        $this->class = $value;

        return $this;
    }

    public function getGeometry(): ?FHIRCodeableConcept
    {
        return $this->geometry;
    }

    public function setGeometry(?FHIRCodeableConcept $value): self
    {
        $this->geometry = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCopolymerConnectivity(): array
    {
        return $this->copolymerConnectivity;
    }

    public function setCopolymerConnectivity(?FHIRCodeableConcept ...$value): self
    {
        $this->copolymerConnectivity = array_filter($value);

        return $this;
    }

    public function addCopolymerConnectivity(?FHIRCodeableConcept ...$value): self
    {
        $this->copolymerConnectivity = array_filter(array_merge($this->copolymerConnectivity, $value));

        return $this;
    }

    public function getModification(): ?FHIRString
    {
        return $this->modification;
    }

    public function setModification(string|FHIRString|null $value): self
    {
        $this->modification = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRSubstancePolymerMonomerSet[]
     */
    public function getMonomerSet(): array
    {
        return $this->monomerSet;
    }

    public function setMonomerSet(?FHIRSubstancePolymerMonomerSet ...$value): self
    {
        $this->monomerSet = array_filter($value);

        return $this;
    }

    public function addMonomerSet(?FHIRSubstancePolymerMonomerSet ...$value): self
    {
        $this->monomerSet = array_filter(array_merge($this->monomerSet, $value));

        return $this;
    }

    /**
     * @return FHIRSubstancePolymerRepeat[]
     */
    public function getRepeat(): array
    {
        return $this->repeat;
    }

    public function setRepeat(?FHIRSubstancePolymerRepeat ...$value): self
    {
        $this->repeat = array_filter($value);

        return $this;
    }

    public function addRepeat(?FHIRSubstancePolymerRepeat ...$value): self
    {
        $this->repeat = array_filter(array_merge($this->repeat, $value));

        return $this;
    }
}
