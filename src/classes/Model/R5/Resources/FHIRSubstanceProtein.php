<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceProtein Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRSubstanceProteinInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRSubstanceProteinSubunit;

class FHIRSubstanceProtein extends FHIRDomainResource implements FHIRSubstanceProteinInterface
{
    public const RESOURCE_NAME = 'SubstanceProtein';

    protected ?FHIRCodeableConcept $sequenceType = null;
    protected ?FHIRInteger $numberOfSubunits = null;

    /** @var FHIRString[] */
    protected array $disulfideLinkage = [];

    /** @var FHIRSubstanceProteinSubunit[] */
    protected array $subunit = [];

    public function getSequenceType(): ?FHIRCodeableConcept
    {
        return $this->sequenceType;
    }

    public function setSequenceType(?FHIRCodeableConcept $value): self
    {
        $this->sequenceType = $value;

        return $this;
    }

    public function getNumberOfSubunits(): ?FHIRInteger
    {
        return $this->numberOfSubunits;
    }

    public function setNumberOfSubunits(int|FHIRInteger|null $value): self
    {
        $this->numberOfSubunits = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getDisulfideLinkage(): array
    {
        return $this->disulfideLinkage;
    }

    public function setDisulfideLinkage(string|FHIRString|null ...$value): self
    {
        $this->disulfideLinkage = [];
        $this->addDisulfideLinkage(...$value);

        return $this;
    }

    public function addDisulfideLinkage(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->disulfideLinkage = array_filter(array_merge($this->disulfideLinkage, $values));

        return $this;
    }

    /**
     * @return FHIRSubstanceProteinSubunit[]
     */
    public function getSubunit(): array
    {
        return $this->subunit;
    }

    public function setSubunit(?FHIRSubstanceProteinSubunit ...$value): self
    {
        $this->subunit = array_filter($value);

        return $this;
    }

    public function addSubunit(?FHIRSubstanceProteinSubunit ...$value): self
    {
        $this->subunit = array_filter(array_merge($this->subunit, $value));

        return $this;
    }
}
