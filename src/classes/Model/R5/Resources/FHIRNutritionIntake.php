<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR NutritionIntake Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRNutritionIntakeInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRNutritionIntakeConsumedItem;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRNutritionIntakeIngredientLabel;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRNutritionIntakePerformer;

class FHIRNutritionIntake extends FHIRDomainResource implements FHIRNutritionIntakeInterface
{
    public const RESOURCE_NAME = 'NutritionIntake';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRCanonical[] */
    protected array $instantiatesCanonical = [];

    /** @var FHIRUri[] */
    protected array $instantiatesUri = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];

    /** @var FHIRReference[] */
    protected array $partOf = [];
    protected ?FHIRCode $status = null;

    /** @var FHIRCodeableConcept[] */
    protected array $statusReason = [];
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $encounter = null;
    protected FHIRDateTime|FHIRPeriod|null $occurrence = null;
    protected ?FHIRDateTime $recorded = null;
    protected FHIRBoolean|FHIRReference|null $reported = null;

    /** @var FHIRNutritionIntakeConsumedItem[] */
    protected array $consumedItem = [];

    /** @var FHIRNutritionIntakeIngredientLabel[] */
    protected array $ingredientLabel = [];

    /** @var FHIRNutritionIntakePerformer[] */
    protected array $performer = [];
    protected ?FHIRReference $location = null;

    /** @var FHIRReference[] */
    protected array $derivedFrom = [];

    /** @var FHIRCodeableReference[] */
    protected array $reason = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getInstantiatesCanonical(): array
    {
        return $this->instantiatesCanonical;
    }

    public function setInstantiatesCanonical(string|FHIRCanonical|null ...$value): self
    {
        $this->instantiatesCanonical = [];
        $this->addInstantiatesCanonical(...$value);

        return $this;
    }

    public function addInstantiatesCanonical(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->instantiatesCanonical = array_filter(array_merge($this->instantiatesCanonical, $values));

        return $this;
    }

    /**
     * @return FHIRUri[]
     */
    public function getInstantiatesUri(): array
    {
        return $this->instantiatesUri;
    }

    public function setInstantiatesUri(string|FHIRUri|null ...$value): self
    {
        $this->instantiatesUri = [];
        $this->addInstantiatesUri(...$value);

        return $this;
    }

    public function addInstantiatesUri(string|FHIRUri|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUri())->setValue($v) : $v, $value);

        $this->instantiatesUri = array_filter(array_merge($this->instantiatesUri, $values));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPartOf(): array
    {
        return $this->partOf;
    }

    public function setPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter($value);

        return $this;
    }

    public function addPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter(array_merge($this->partOf, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getStatusReason(): array
    {
        return $this->statusReason;
    }

    public function setStatusReason(?FHIRCodeableConcept ...$value): self
    {
        $this->statusReason = array_filter($value);

        return $this;
    }

    public function addStatusReason(?FHIRCodeableConcept ...$value): self
    {
        $this->statusReason = array_filter(array_merge($this->statusReason, $value));

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getOccurrence(): FHIRDateTime|FHIRPeriod|null
    {
        return $this->occurrence;
    }

    public function setOccurrence(FHIRDateTime|FHIRPeriod|null $value): self
    {
        $this->occurrence = $value;

        return $this;
    }

    public function getRecorded(): ?FHIRDateTime
    {
        return $this->recorded;
    }

    public function setRecorded(string|FHIRDateTime|null $value): self
    {
        $this->recorded = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getReported(): FHIRBoolean|FHIRReference|null
    {
        return $this->reported;
    }

    public function setReported(FHIRBoolean|FHIRReference|null $value): self
    {
        $this->reported = $value;

        return $this;
    }

    /**
     * @return FHIRNutritionIntakeConsumedItem[]
     */
    public function getConsumedItem(): array
    {
        return $this->consumedItem;
    }

    public function setConsumedItem(?FHIRNutritionIntakeConsumedItem ...$value): self
    {
        $this->consumedItem = array_filter($value);

        return $this;
    }

    public function addConsumedItem(?FHIRNutritionIntakeConsumedItem ...$value): self
    {
        $this->consumedItem = array_filter(array_merge($this->consumedItem, $value));

        return $this;
    }

    /**
     * @return FHIRNutritionIntakeIngredientLabel[]
     */
    public function getIngredientLabel(): array
    {
        return $this->ingredientLabel;
    }

    public function setIngredientLabel(?FHIRNutritionIntakeIngredientLabel ...$value): self
    {
        $this->ingredientLabel = array_filter($value);

        return $this;
    }

    public function addIngredientLabel(?FHIRNutritionIntakeIngredientLabel ...$value): self
    {
        $this->ingredientLabel = array_filter(array_merge($this->ingredientLabel, $value));

        return $this;
    }

    /**
     * @return FHIRNutritionIntakePerformer[]
     */
    public function getPerformer(): array
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRNutritionIntakePerformer ...$value): self
    {
        $this->performer = array_filter($value);

        return $this;
    }

    public function addPerformer(?FHIRNutritionIntakePerformer ...$value): self
    {
        $this->performer = array_filter(array_merge($this->performer, $value));

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getDerivedFrom(): array
    {
        return $this->derivedFrom;
    }

    public function setDerivedFrom(?FHIRReference ...$value): self
    {
        $this->derivedFrom = array_filter($value);

        return $this;
    }

    public function addDerivedFrom(?FHIRReference ...$value): self
    {
        $this->derivedFrom = array_filter(array_merge($this->derivedFrom, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getReason(): array
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableReference ...$value): self
    {
        $this->reason = array_filter($value);

        return $this;
    }

    public function addReason(?FHIRCodeableReference ...$value): self
    {
        $this->reason = array_filter(array_merge($this->reason, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }
}
