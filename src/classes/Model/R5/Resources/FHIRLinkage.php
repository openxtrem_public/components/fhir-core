<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Linkage Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRLinkageInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRLinkageItem;

class FHIRLinkage extends FHIRDomainResource implements FHIRLinkageInterface
{
    public const RESOURCE_NAME = 'Linkage';

    protected ?FHIRBoolean $active = null;
    protected ?FHIRReference $author = null;

    /** @var FHIRLinkageItem[] */
    protected array $item = [];

    public function getActive(): ?FHIRBoolean
    {
        return $this->active;
    }

    public function setActive(bool|FHIRBoolean|null $value): self
    {
        $this->active = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getAuthor(): ?FHIRReference
    {
        return $this->author;
    }

    public function setAuthor(?FHIRReference $value): self
    {
        $this->author = $value;

        return $this;
    }

    /**
     * @return FHIRLinkageItem[]
     */
    public function getItem(): array
    {
        return $this->item;
    }

    public function setItem(?FHIRLinkageItem ...$value): self
    {
        $this->item = array_filter($value);

        return $this;
    }

    public function addItem(?FHIRLinkageItem ...$value): self
    {
        $this->item = array_filter(array_merge($this->item, $value));

        return $this;
    }
}
