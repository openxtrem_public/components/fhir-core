<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Encounter Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIREncounterInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRVirtualServiceDetail;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIREncounterAdmission;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIREncounterDiagnosis;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIREncounterLocation;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIREncounterParticipant;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIREncounterReason;

class FHIREncounter extends FHIRDomainResource implements FHIREncounterInterface
{
    public const RESOURCE_NAME = 'Encounter';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;

    /** @var FHIRCodeableConcept[] */
    protected array $class = [];
    protected ?FHIRCodeableConcept $priority = null;

    /** @var FHIRCodeableConcept[] */
    protected array $type = [];

    /** @var FHIRCodeableReference[] */
    protected array $serviceType = [];
    protected ?FHIRReference $subject = null;
    protected ?FHIRCodeableConcept $subjectStatus = null;

    /** @var FHIRReference[] */
    protected array $episodeOfCare = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];

    /** @var FHIRReference[] */
    protected array $careTeam = [];
    protected ?FHIRReference $partOf = null;
    protected ?FHIRReference $serviceProvider = null;

    /** @var FHIREncounterParticipant[] */
    protected array $participant = [];

    /** @var FHIRReference[] */
    protected array $appointment = [];

    /** @var FHIRVirtualServiceDetail[] */
    protected array $virtualService = [];
    protected ?FHIRPeriod $actualPeriod = null;
    protected ?FHIRDateTime $plannedStartDate = null;
    protected ?FHIRDateTime $plannedEndDate = null;
    protected ?FHIRDuration $length = null;

    /** @var FHIREncounterReason[] */
    protected array $reason = [];

    /** @var FHIREncounterDiagnosis[] */
    protected array $diagnosis = [];

    /** @var FHIRReference[] */
    protected array $account = [];

    /** @var FHIRCodeableConcept[] */
    protected array $dietPreference = [];

    /** @var FHIRCodeableConcept[] */
    protected array $specialArrangement = [];

    /** @var FHIRCodeableConcept[] */
    protected array $specialCourtesy = [];
    protected ?FHIREncounterAdmission $admission = null;

    /** @var FHIREncounterLocation[] */
    protected array $location = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getClass(): array
    {
        return $this->class;
    }

    public function setClass(?FHIRCodeableConcept ...$value): self
    {
        $this->class = array_filter($value);

        return $this;
    }

    public function addClass(?FHIRCodeableConcept ...$value): self
    {
        $this->class = array_filter(array_merge($this->class, $value));

        return $this;
    }

    public function getPriority(): ?FHIRCodeableConcept
    {
        return $this->priority;
    }

    public function setPriority(?FHIRCodeableConcept $value): self
    {
        $this->priority = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getServiceType(): array
    {
        return $this->serviceType;
    }

    public function setServiceType(?FHIRCodeableReference ...$value): self
    {
        $this->serviceType = array_filter($value);

        return $this;
    }

    public function addServiceType(?FHIRCodeableReference ...$value): self
    {
        $this->serviceType = array_filter(array_merge($this->serviceType, $value));

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getSubjectStatus(): ?FHIRCodeableConcept
    {
        return $this->subjectStatus;
    }

    public function setSubjectStatus(?FHIRCodeableConcept $value): self
    {
        $this->subjectStatus = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEpisodeOfCare(): array
    {
        return $this->episodeOfCare;
    }

    public function setEpisodeOfCare(?FHIRReference ...$value): self
    {
        $this->episodeOfCare = array_filter($value);

        return $this;
    }

    public function addEpisodeOfCare(?FHIRReference ...$value): self
    {
        $this->episodeOfCare = array_filter(array_merge($this->episodeOfCare, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getCareTeam(): array
    {
        return $this->careTeam;
    }

    public function setCareTeam(?FHIRReference ...$value): self
    {
        $this->careTeam = array_filter($value);

        return $this;
    }

    public function addCareTeam(?FHIRReference ...$value): self
    {
        $this->careTeam = array_filter(array_merge($this->careTeam, $value));

        return $this;
    }

    public function getPartOf(): ?FHIRReference
    {
        return $this->partOf;
    }

    public function setPartOf(?FHIRReference $value): self
    {
        $this->partOf = $value;

        return $this;
    }

    public function getServiceProvider(): ?FHIRReference
    {
        return $this->serviceProvider;
    }

    public function setServiceProvider(?FHIRReference $value): self
    {
        $this->serviceProvider = $value;

        return $this;
    }

    /**
     * @return FHIREncounterParticipant[]
     */
    public function getParticipant(): array
    {
        return $this->participant;
    }

    public function setParticipant(?FHIREncounterParticipant ...$value): self
    {
        $this->participant = array_filter($value);

        return $this;
    }

    public function addParticipant(?FHIREncounterParticipant ...$value): self
    {
        $this->participant = array_filter(array_merge($this->participant, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAppointment(): array
    {
        return $this->appointment;
    }

    public function setAppointment(?FHIRReference ...$value): self
    {
        $this->appointment = array_filter($value);

        return $this;
    }

    public function addAppointment(?FHIRReference ...$value): self
    {
        $this->appointment = array_filter(array_merge($this->appointment, $value));

        return $this;
    }

    /**
     * @return FHIRVirtualServiceDetail[]
     */
    public function getVirtualService(): array
    {
        return $this->virtualService;
    }

    public function setVirtualService(?FHIRVirtualServiceDetail ...$value): self
    {
        $this->virtualService = array_filter($value);

        return $this;
    }

    public function addVirtualService(?FHIRVirtualServiceDetail ...$value): self
    {
        $this->virtualService = array_filter(array_merge($this->virtualService, $value));

        return $this;
    }

    public function getActualPeriod(): ?FHIRPeriod
    {
        return $this->actualPeriod;
    }

    public function setActualPeriod(?FHIRPeriod $value): self
    {
        $this->actualPeriod = $value;

        return $this;
    }

    public function getPlannedStartDate(): ?FHIRDateTime
    {
        return $this->plannedStartDate;
    }

    public function setPlannedStartDate(string|FHIRDateTime|null $value): self
    {
        $this->plannedStartDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPlannedEndDate(): ?FHIRDateTime
    {
        return $this->plannedEndDate;
    }

    public function setPlannedEndDate(string|FHIRDateTime|null $value): self
    {
        $this->plannedEndDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getLength(): ?FHIRDuration
    {
        return $this->length;
    }

    public function setLength(?FHIRDuration $value): self
    {
        $this->length = $value;

        return $this;
    }

    /**
     * @return FHIREncounterReason[]
     */
    public function getReason(): array
    {
        return $this->reason;
    }

    public function setReason(?FHIREncounterReason ...$value): self
    {
        $this->reason = array_filter($value);

        return $this;
    }

    public function addReason(?FHIREncounterReason ...$value): self
    {
        $this->reason = array_filter(array_merge($this->reason, $value));

        return $this;
    }

    /**
     * @return FHIREncounterDiagnosis[]
     */
    public function getDiagnosis(): array
    {
        return $this->diagnosis;
    }

    public function setDiagnosis(?FHIREncounterDiagnosis ...$value): self
    {
        $this->diagnosis = array_filter($value);

        return $this;
    }

    public function addDiagnosis(?FHIREncounterDiagnosis ...$value): self
    {
        $this->diagnosis = array_filter(array_merge($this->diagnosis, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAccount(): array
    {
        return $this->account;
    }

    public function setAccount(?FHIRReference ...$value): self
    {
        $this->account = array_filter($value);

        return $this;
    }

    public function addAccount(?FHIRReference ...$value): self
    {
        $this->account = array_filter(array_merge($this->account, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getDietPreference(): array
    {
        return $this->dietPreference;
    }

    public function setDietPreference(?FHIRCodeableConcept ...$value): self
    {
        $this->dietPreference = array_filter($value);

        return $this;
    }

    public function addDietPreference(?FHIRCodeableConcept ...$value): self
    {
        $this->dietPreference = array_filter(array_merge($this->dietPreference, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSpecialArrangement(): array
    {
        return $this->specialArrangement;
    }

    public function setSpecialArrangement(?FHIRCodeableConcept ...$value): self
    {
        $this->specialArrangement = array_filter($value);

        return $this;
    }

    public function addSpecialArrangement(?FHIRCodeableConcept ...$value): self
    {
        $this->specialArrangement = array_filter(array_merge($this->specialArrangement, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSpecialCourtesy(): array
    {
        return $this->specialCourtesy;
    }

    public function setSpecialCourtesy(?FHIRCodeableConcept ...$value): self
    {
        $this->specialCourtesy = array_filter($value);

        return $this;
    }

    public function addSpecialCourtesy(?FHIRCodeableConcept ...$value): self
    {
        $this->specialCourtesy = array_filter(array_merge($this->specialCourtesy, $value));

        return $this;
    }

    public function getAdmission(): ?FHIREncounterAdmission
    {
        return $this->admission;
    }

    public function setAdmission(?FHIREncounterAdmission $value): self
    {
        $this->admission = $value;

        return $this;
    }

    /**
     * @return FHIREncounterLocation[]
     */
    public function getLocation(): array
    {
        return $this->location;
    }

    public function setLocation(?FHIREncounterLocation ...$value): self
    {
        $this->location = array_filter($value);

        return $this;
    }

    public function addLocation(?FHIREncounterLocation ...$value): self
    {
        $this->location = array_filter(array_merge($this->location, $value));

        return $this;
    }
}
