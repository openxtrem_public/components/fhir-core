<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR VerificationResult Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRVerificationResultInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRVerificationResultAttestation;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRVerificationResultPrimarySource;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRVerificationResultValidator;

class FHIRVerificationResult extends FHIRDomainResource implements FHIRVerificationResultInterface
{
    public const RESOURCE_NAME = 'VerificationResult';

    /** @var FHIRReference[] */
    protected array $target = [];

    /** @var FHIRString[] */
    protected array $targetLocation = [];
    protected ?FHIRCodeableConcept $need = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRDateTime $statusDate = null;
    protected ?FHIRCodeableConcept $validationType = null;

    /** @var FHIRCodeableConcept[] */
    protected array $validationProcess = [];
    protected ?FHIRTiming $frequency = null;
    protected ?FHIRDateTime $lastPerformed = null;
    protected ?FHIRDate $nextScheduled = null;
    protected ?FHIRCodeableConcept $failureAction = null;

    /** @var FHIRVerificationResultPrimarySource[] */
    protected array $primarySource = [];
    protected ?FHIRVerificationResultAttestation $attestation = null;

    /** @var FHIRVerificationResultValidator[] */
    protected array $validator = [];

    /**
     * @return FHIRReference[]
     */
    public function getTarget(): array
    {
        return $this->target;
    }

    public function setTarget(?FHIRReference ...$value): self
    {
        $this->target = array_filter($value);

        return $this;
    }

    public function addTarget(?FHIRReference ...$value): self
    {
        $this->target = array_filter(array_merge($this->target, $value));

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getTargetLocation(): array
    {
        return $this->targetLocation;
    }

    public function setTargetLocation(string|FHIRString|null ...$value): self
    {
        $this->targetLocation = [];
        $this->addTargetLocation(...$value);

        return $this;
    }

    public function addTargetLocation(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->targetLocation = array_filter(array_merge($this->targetLocation, $values));

        return $this;
    }

    public function getNeed(): ?FHIRCodeableConcept
    {
        return $this->need;
    }

    public function setNeed(?FHIRCodeableConcept $value): self
    {
        $this->need = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getStatusDate(): ?FHIRDateTime
    {
        return $this->statusDate;
    }

    public function setStatusDate(string|FHIRDateTime|null $value): self
    {
        $this->statusDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getValidationType(): ?FHIRCodeableConcept
    {
        return $this->validationType;
    }

    public function setValidationType(?FHIRCodeableConcept $value): self
    {
        $this->validationType = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getValidationProcess(): array
    {
        return $this->validationProcess;
    }

    public function setValidationProcess(?FHIRCodeableConcept ...$value): self
    {
        $this->validationProcess = array_filter($value);

        return $this;
    }

    public function addValidationProcess(?FHIRCodeableConcept ...$value): self
    {
        $this->validationProcess = array_filter(array_merge($this->validationProcess, $value));

        return $this;
    }

    public function getFrequency(): ?FHIRTiming
    {
        return $this->frequency;
    }

    public function setFrequency(?FHIRTiming $value): self
    {
        $this->frequency = $value;

        return $this;
    }

    public function getLastPerformed(): ?FHIRDateTime
    {
        return $this->lastPerformed;
    }

    public function setLastPerformed(string|FHIRDateTime|null $value): self
    {
        $this->lastPerformed = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getNextScheduled(): ?FHIRDate
    {
        return $this->nextScheduled;
    }

    public function setNextScheduled(string|FHIRDate|null $value): self
    {
        $this->nextScheduled = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getFailureAction(): ?FHIRCodeableConcept
    {
        return $this->failureAction;
    }

    public function setFailureAction(?FHIRCodeableConcept $value): self
    {
        $this->failureAction = $value;

        return $this;
    }

    /**
     * @return FHIRVerificationResultPrimarySource[]
     */
    public function getPrimarySource(): array
    {
        return $this->primarySource;
    }

    public function setPrimarySource(?FHIRVerificationResultPrimarySource ...$value): self
    {
        $this->primarySource = array_filter($value);

        return $this;
    }

    public function addPrimarySource(?FHIRVerificationResultPrimarySource ...$value): self
    {
        $this->primarySource = array_filter(array_merge($this->primarySource, $value));

        return $this;
    }

    public function getAttestation(): ?FHIRVerificationResultAttestation
    {
        return $this->attestation;
    }

    public function setAttestation(?FHIRVerificationResultAttestation $value): self
    {
        $this->attestation = $value;

        return $this;
    }

    /**
     * @return FHIRVerificationResultValidator[]
     */
    public function getValidator(): array
    {
        return $this->validator;
    }

    public function setValidator(?FHIRVerificationResultValidator ...$value): self
    {
        $this->validator = array_filter($value);

        return $this;
    }

    public function addValidator(?FHIRVerificationResultValidator ...$value): self
    {
        $this->validator = array_filter(array_merge($this->validator, $value));

        return $this;
    }
}
