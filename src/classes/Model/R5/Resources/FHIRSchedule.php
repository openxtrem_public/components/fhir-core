<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Schedule Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRScheduleInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRSchedule extends FHIRDomainResource implements FHIRScheduleInterface
{
    public const RESOURCE_NAME = 'Schedule';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRBoolean $active = null;

    /** @var FHIRCodeableConcept[] */
    protected array $serviceCategory = [];

    /** @var FHIRCodeableReference[] */
    protected array $serviceType = [];

    /** @var FHIRCodeableConcept[] */
    protected array $specialty = [];
    protected ?FHIRString $name = null;

    /** @var FHIRReference[] */
    protected array $actor = [];
    protected ?FHIRPeriod $planningHorizon = null;
    protected ?FHIRMarkdown $comment = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getActive(): ?FHIRBoolean
    {
        return $this->active;
    }

    public function setActive(bool|FHIRBoolean|null $value): self
    {
        $this->active = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getServiceCategory(): array
    {
        return $this->serviceCategory;
    }

    public function setServiceCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->serviceCategory = array_filter($value);

        return $this;
    }

    public function addServiceCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->serviceCategory = array_filter(array_merge($this->serviceCategory, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getServiceType(): array
    {
        return $this->serviceType;
    }

    public function setServiceType(?FHIRCodeableReference ...$value): self
    {
        $this->serviceType = array_filter($value);

        return $this;
    }

    public function addServiceType(?FHIRCodeableReference ...$value): self
    {
        $this->serviceType = array_filter(array_merge($this->serviceType, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSpecialty(): array
    {
        return $this->specialty;
    }

    public function setSpecialty(?FHIRCodeableConcept ...$value): self
    {
        $this->specialty = array_filter($value);

        return $this;
    }

    public function addSpecialty(?FHIRCodeableConcept ...$value): self
    {
        $this->specialty = array_filter(array_merge($this->specialty, $value));

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getActor(): array
    {
        return $this->actor;
    }

    public function setActor(?FHIRReference ...$value): self
    {
        $this->actor = array_filter($value);

        return $this;
    }

    public function addActor(?FHIRReference ...$value): self
    {
        $this->actor = array_filter(array_merge($this->actor, $value));

        return $this;
    }

    public function getPlanningHorizon(): ?FHIRPeriod
    {
        return $this->planningHorizon;
    }

    public function setPlanningHorizon(?FHIRPeriod $value): self
    {
        $this->planningHorizon = $value;

        return $this;
    }

    public function getComment(): ?FHIRMarkdown
    {
        return $this->comment;
    }

    public function setComment(string|FHIRMarkdown|null $value): self
    {
        $this->comment = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }
}
