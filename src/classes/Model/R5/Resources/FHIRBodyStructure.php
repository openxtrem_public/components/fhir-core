<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR BodyStructure Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRBodyStructureInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRBodyStructureIncludedStructure;

class FHIRBodyStructure extends FHIRDomainResource implements FHIRBodyStructureInterface
{
    public const RESOURCE_NAME = 'BodyStructure';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRBoolean $active = null;
    protected ?FHIRCodeableConcept $morphology = null;

    /** @var FHIRBodyStructureIncludedStructure[] */
    protected array $includedStructure = [];

    /** @var FHIRBodyStructureIncludedStructure[] */
    protected array $excludedStructure = [];
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRAttachment[] */
    protected array $image = [];
    protected ?FHIRReference $patient = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getActive(): ?FHIRBoolean
    {
        return $this->active;
    }

    public function setActive(bool|FHIRBoolean|null $value): self
    {
        $this->active = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getMorphology(): ?FHIRCodeableConcept
    {
        return $this->morphology;
    }

    public function setMorphology(?FHIRCodeableConcept $value): self
    {
        $this->morphology = $value;

        return $this;
    }

    /**
     * @return FHIRBodyStructureIncludedStructure[]
     */
    public function getIncludedStructure(): array
    {
        return $this->includedStructure;
    }

    public function setIncludedStructure(?FHIRBodyStructureIncludedStructure ...$value): self
    {
        $this->includedStructure = array_filter($value);

        return $this;
    }

    public function addIncludedStructure(?FHIRBodyStructureIncludedStructure ...$value): self
    {
        $this->includedStructure = array_filter(array_merge($this->includedStructure, $value));

        return $this;
    }

    /**
     * @return FHIRBodyStructureIncludedStructure[]
     */
    public function getExcludedStructure(): array
    {
        return $this->excludedStructure;
    }

    public function setExcludedStructure(?FHIRBodyStructureIncludedStructure ...$value): self
    {
        $this->excludedStructure = array_filter($value);

        return $this;
    }

    public function addExcludedStructure(?FHIRBodyStructureIncludedStructure ...$value): self
    {
        $this->excludedStructure = array_filter(array_merge($this->excludedStructure, $value));

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAttachment[]
     */
    public function getImage(): array
    {
        return $this->image;
    }

    public function setImage(?FHIRAttachment ...$value): self
    {
        $this->image = array_filter($value);

        return $this;
    }

    public function addImage(?FHIRAttachment ...$value): self
    {
        $this->image = array_filter(array_merge($this->image, $value));

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }
}
