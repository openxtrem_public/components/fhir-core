<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TerminologyCapabilities Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRTerminologyCapabilitiesInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRTerminologyCapabilitiesClosure;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRTerminologyCapabilitiesCodeSystem;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRTerminologyCapabilitiesExpansion;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRTerminologyCapabilitiesImplementation;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRTerminologyCapabilitiesSoftware;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRTerminologyCapabilitiesTranslation;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRTerminologyCapabilitiesValidateCode;

class FHIRTerminologyCapabilities extends FHIRDomainResource implements FHIRTerminologyCapabilitiesInterface
{
    public const RESOURCE_NAME = 'TerminologyCapabilities';

    protected ?FHIRUri $url = null;

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRString $version = null;
    protected FHIRString|FHIRCoding|null $versionAlgorithm = null;
    protected ?FHIRString $name = null;
    protected ?FHIRString $title = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRBoolean $experimental = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRString $publisher = null;

    /** @var FHIRContactDetail[] */
    protected array $contact = [];
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRUsageContext[] */
    protected array $useContext = [];

    /** @var FHIRCodeableConcept[] */
    protected array $jurisdiction = [];
    protected ?FHIRMarkdown $purpose = null;
    protected ?FHIRMarkdown $copyright = null;
    protected ?FHIRString $copyrightLabel = null;
    protected ?FHIRCode $kind = null;
    protected ?FHIRTerminologyCapabilitiesSoftware $software = null;
    protected ?FHIRTerminologyCapabilitiesImplementation $implementation = null;
    protected ?FHIRBoolean $lockedDate = null;

    /** @var FHIRTerminologyCapabilitiesCodeSystem[] */
    protected array $codeSystem = [];
    protected ?FHIRTerminologyCapabilitiesExpansion $expansion = null;
    protected ?FHIRCode $codeSearch = null;
    protected ?FHIRTerminologyCapabilitiesValidateCode $validateCode = null;
    protected ?FHIRTerminologyCapabilitiesTranslation $translation = null;
    protected ?FHIRTerminologyCapabilitiesClosure $closure = null;

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getVersionAlgorithm(): FHIRString|FHIRCoding|null
    {
        return $this->versionAlgorithm;
    }

    public function setVersionAlgorithm(FHIRString|FHIRCoding|null $value): self
    {
        $this->versionAlgorithm = $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getExperimental(): ?FHIRBoolean
    {
        return $this->experimental;
    }

    public function setExperimental(bool|FHIRBoolean|null $value): self
    {
        $this->experimental = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPublisher(): ?FHIRString
    {
        return $this->publisher;
    }

    public function setPublisher(string|FHIRString|null $value): self
    {
        $this->publisher = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUseContext(): array
    {
        return $this->useContext;
    }

    public function setUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter($value);

        return $this;
    }

    public function addUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter(array_merge($this->useContext, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getJurisdiction(): array
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter($value);

        return $this;
    }

    public function addJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter(array_merge($this->jurisdiction, $value));

        return $this;
    }

    public function getPurpose(): ?FHIRMarkdown
    {
        return $this->purpose;
    }

    public function setPurpose(string|FHIRMarkdown|null $value): self
    {
        $this->purpose = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getCopyright(): ?FHIRMarkdown
    {
        return $this->copyright;
    }

    public function setCopyright(string|FHIRMarkdown|null $value): self
    {
        $this->copyright = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getCopyrightLabel(): ?FHIRString
    {
        return $this->copyrightLabel;
    }

    public function setCopyrightLabel(string|FHIRString|null $value): self
    {
        $this->copyrightLabel = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getKind(): ?FHIRCode
    {
        return $this->kind;
    }

    public function setKind(string|FHIRCode|null $value): self
    {
        $this->kind = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getSoftware(): ?FHIRTerminologyCapabilitiesSoftware
    {
        return $this->software;
    }

    public function setSoftware(?FHIRTerminologyCapabilitiesSoftware $value): self
    {
        $this->software = $value;

        return $this;
    }

    public function getImplementation(): ?FHIRTerminologyCapabilitiesImplementation
    {
        return $this->implementation;
    }

    public function setImplementation(?FHIRTerminologyCapabilitiesImplementation $value): self
    {
        $this->implementation = $value;

        return $this;
    }

    public function getLockedDate(): ?FHIRBoolean
    {
        return $this->lockedDate;
    }

    public function setLockedDate(bool|FHIRBoolean|null $value): self
    {
        $this->lockedDate = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRTerminologyCapabilitiesCodeSystem[]
     */
    public function getCodeSystem(): array
    {
        return $this->codeSystem;
    }

    public function setCodeSystem(?FHIRTerminologyCapabilitiesCodeSystem ...$value): self
    {
        $this->codeSystem = array_filter($value);

        return $this;
    }

    public function addCodeSystem(?FHIRTerminologyCapabilitiesCodeSystem ...$value): self
    {
        $this->codeSystem = array_filter(array_merge($this->codeSystem, $value));

        return $this;
    }

    public function getExpansion(): ?FHIRTerminologyCapabilitiesExpansion
    {
        return $this->expansion;
    }

    public function setExpansion(?FHIRTerminologyCapabilitiesExpansion $value): self
    {
        $this->expansion = $value;

        return $this;
    }

    public function getCodeSearch(): ?FHIRCode
    {
        return $this->codeSearch;
    }

    public function setCodeSearch(string|FHIRCode|null $value): self
    {
        $this->codeSearch = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getValidateCode(): ?FHIRTerminologyCapabilitiesValidateCode
    {
        return $this->validateCode;
    }

    public function setValidateCode(?FHIRTerminologyCapabilitiesValidateCode $value): self
    {
        $this->validateCode = $value;

        return $this;
    }

    public function getTranslation(): ?FHIRTerminologyCapabilitiesTranslation
    {
        return $this->translation;
    }

    public function setTranslation(?FHIRTerminologyCapabilitiesTranslation $value): self
    {
        $this->translation = $value;

        return $this;
    }

    public function getClosure(): ?FHIRTerminologyCapabilitiesClosure
    {
        return $this->closure;
    }

    public function setClosure(?FHIRTerminologyCapabilitiesClosure $value): self
    {
        $this->closure = $value;

        return $this;
    }
}
