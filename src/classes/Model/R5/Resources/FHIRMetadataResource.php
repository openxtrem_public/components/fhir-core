<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MetadataResource Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMetadataResourceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRelatedArtifact;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDate;

abstract class FHIRMetadataResource extends FHIRDomainResource implements FHIRMetadataResourceInterface
{
    public const RESOURCE_NAME = 'MetadataResource';

    protected ?FHIRDate $approvalDate = null;
    protected ?FHIRDate $lastReviewDate = null;
    protected ?FHIRPeriod $effectivePeriod = null;

    /** @var FHIRCodeableConcept[] */
    protected array $topic = [];

    /** @var FHIRContactDetail[] */
    protected array $author = [];

    /** @var FHIRContactDetail[] */
    protected array $editor = [];

    /** @var FHIRContactDetail[] */
    protected array $reviewer = [];

    /** @var FHIRContactDetail[] */
    protected array $endorser = [];

    /** @var FHIRRelatedArtifact[] */
    protected array $relatedArtifact = [];

    public function getApprovalDate(): ?FHIRDate
    {
        return $this->approvalDate;
    }

    public function setApprovalDate(string|FHIRDate|null $value): self
    {
        $this->approvalDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getLastReviewDate(): ?FHIRDate
    {
        return $this->lastReviewDate;
    }

    public function setLastReviewDate(string|FHIRDate|null $value): self
    {
        $this->lastReviewDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getEffectivePeriod(): ?FHIRPeriod
    {
        return $this->effectivePeriod;
    }

    public function setEffectivePeriod(?FHIRPeriod $value): self
    {
        $this->effectivePeriod = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getTopic(): array
    {
        return $this->topic;
    }

    public function setTopic(?FHIRCodeableConcept ...$value): self
    {
        $this->topic = array_filter($value);

        return $this;
    }

    public function addTopic(?FHIRCodeableConcept ...$value): self
    {
        $this->topic = array_filter(array_merge($this->topic, $value));

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getAuthor(): array
    {
        return $this->author;
    }

    public function setAuthor(?FHIRContactDetail ...$value): self
    {
        $this->author = array_filter($value);

        return $this;
    }

    public function addAuthor(?FHIRContactDetail ...$value): self
    {
        $this->author = array_filter(array_merge($this->author, $value));

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getEditor(): array
    {
        return $this->editor;
    }

    public function setEditor(?FHIRContactDetail ...$value): self
    {
        $this->editor = array_filter($value);

        return $this;
    }

    public function addEditor(?FHIRContactDetail ...$value): self
    {
        $this->editor = array_filter(array_merge($this->editor, $value));

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getReviewer(): array
    {
        return $this->reviewer;
    }

    public function setReviewer(?FHIRContactDetail ...$value): self
    {
        $this->reviewer = array_filter($value);

        return $this;
    }

    public function addReviewer(?FHIRContactDetail ...$value): self
    {
        $this->reviewer = array_filter(array_merge($this->reviewer, $value));

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getEndorser(): array
    {
        return $this->endorser;
    }

    public function setEndorser(?FHIRContactDetail ...$value): self
    {
        $this->endorser = array_filter($value);

        return $this;
    }

    public function addEndorser(?FHIRContactDetail ...$value): self
    {
        $this->endorser = array_filter(array_merge($this->endorser, $value));

        return $this;
    }

    /**
     * @return FHIRRelatedArtifact[]
     */
    public function getRelatedArtifact(): array
    {
        return $this->relatedArtifact;
    }

    public function setRelatedArtifact(?FHIRRelatedArtifact ...$value): self
    {
        $this->relatedArtifact = array_filter($value);

        return $this;
    }

    public function addRelatedArtifact(?FHIRRelatedArtifact ...$value): self
    {
        $this->relatedArtifact = array_filter(array_merge($this->relatedArtifact, $value));

        return $this;
    }
}
