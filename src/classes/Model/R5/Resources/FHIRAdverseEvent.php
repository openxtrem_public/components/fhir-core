<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AdverseEvent Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRAdverseEventInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRAdverseEventContributingFactor;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRAdverseEventMitigatingAction;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRAdverseEventParticipant;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRAdverseEventPreventiveAction;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRAdverseEventSupportingInfo;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRAdverseEventSuspectEntity;

class FHIRAdverseEvent extends FHIRDomainResource implements FHIRAdverseEventInterface
{
    public const RESOURCE_NAME = 'AdverseEvent';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCode $actuality = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $encounter = null;
    protected FHIRDateTime|FHIRPeriod|FHIRTiming|null $occurrence = null;
    protected ?FHIRDateTime $detected = null;
    protected ?FHIRDateTime $recordedDate = null;

    /** @var FHIRReference[] */
    protected array $resultingEffect = [];
    protected ?FHIRReference $location = null;
    protected ?FHIRCodeableConcept $seriousness = null;

    /** @var FHIRCodeableConcept[] */
    protected array $outcome = [];
    protected ?FHIRReference $recorder = null;

    /** @var FHIRAdverseEventParticipant[] */
    protected array $participant = [];

    /** @var FHIRReference[] */
    protected array $study = [];
    protected ?FHIRBoolean $expectedInResearchStudy = null;

    /** @var FHIRAdverseEventSuspectEntity[] */
    protected array $suspectEntity = [];

    /** @var FHIRAdverseEventContributingFactor[] */
    protected array $contributingFactor = [];

    /** @var FHIRAdverseEventPreventiveAction[] */
    protected array $preventiveAction = [];

    /** @var FHIRAdverseEventMitigatingAction[] */
    protected array $mitigatingAction = [];

    /** @var FHIRAdverseEventSupportingInfo[] */
    protected array $supportingInfo = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getActuality(): ?FHIRCode
    {
        return $this->actuality;
    }

    public function setActuality(string|FHIRCode|null $value): self
    {
        $this->actuality = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getOccurrence(): FHIRDateTime|FHIRPeriod|FHIRTiming|null
    {
        return $this->occurrence;
    }

    public function setOccurrence(FHIRDateTime|FHIRPeriod|FHIRTiming|null $value): self
    {
        $this->occurrence = $value;

        return $this;
    }

    public function getDetected(): ?FHIRDateTime
    {
        return $this->detected;
    }

    public function setDetected(string|FHIRDateTime|null $value): self
    {
        $this->detected = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getRecordedDate(): ?FHIRDateTime
    {
        return $this->recordedDate;
    }

    public function setRecordedDate(string|FHIRDateTime|null $value): self
    {
        $this->recordedDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getResultingEffect(): array
    {
        return $this->resultingEffect;
    }

    public function setResultingEffect(?FHIRReference ...$value): self
    {
        $this->resultingEffect = array_filter($value);

        return $this;
    }

    public function addResultingEffect(?FHIRReference ...$value): self
    {
        $this->resultingEffect = array_filter(array_merge($this->resultingEffect, $value));

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    public function getSeriousness(): ?FHIRCodeableConcept
    {
        return $this->seriousness;
    }

    public function setSeriousness(?FHIRCodeableConcept $value): self
    {
        $this->seriousness = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getOutcome(): array
    {
        return $this->outcome;
    }

    public function setOutcome(?FHIRCodeableConcept ...$value): self
    {
        $this->outcome = array_filter($value);

        return $this;
    }

    public function addOutcome(?FHIRCodeableConcept ...$value): self
    {
        $this->outcome = array_filter(array_merge($this->outcome, $value));

        return $this;
    }

    public function getRecorder(): ?FHIRReference
    {
        return $this->recorder;
    }

    public function setRecorder(?FHIRReference $value): self
    {
        $this->recorder = $value;

        return $this;
    }

    /**
     * @return FHIRAdverseEventParticipant[]
     */
    public function getParticipant(): array
    {
        return $this->participant;
    }

    public function setParticipant(?FHIRAdverseEventParticipant ...$value): self
    {
        $this->participant = array_filter($value);

        return $this;
    }

    public function addParticipant(?FHIRAdverseEventParticipant ...$value): self
    {
        $this->participant = array_filter(array_merge($this->participant, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getStudy(): array
    {
        return $this->study;
    }

    public function setStudy(?FHIRReference ...$value): self
    {
        $this->study = array_filter($value);

        return $this;
    }

    public function addStudy(?FHIRReference ...$value): self
    {
        $this->study = array_filter(array_merge($this->study, $value));

        return $this;
    }

    public function getExpectedInResearchStudy(): ?FHIRBoolean
    {
        return $this->expectedInResearchStudy;
    }

    public function setExpectedInResearchStudy(bool|FHIRBoolean|null $value): self
    {
        $this->expectedInResearchStudy = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAdverseEventSuspectEntity[]
     */
    public function getSuspectEntity(): array
    {
        return $this->suspectEntity;
    }

    public function setSuspectEntity(?FHIRAdverseEventSuspectEntity ...$value): self
    {
        $this->suspectEntity = array_filter($value);

        return $this;
    }

    public function addSuspectEntity(?FHIRAdverseEventSuspectEntity ...$value): self
    {
        $this->suspectEntity = array_filter(array_merge($this->suspectEntity, $value));

        return $this;
    }

    /**
     * @return FHIRAdverseEventContributingFactor[]
     */
    public function getContributingFactor(): array
    {
        return $this->contributingFactor;
    }

    public function setContributingFactor(?FHIRAdverseEventContributingFactor ...$value): self
    {
        $this->contributingFactor = array_filter($value);

        return $this;
    }

    public function addContributingFactor(?FHIRAdverseEventContributingFactor ...$value): self
    {
        $this->contributingFactor = array_filter(array_merge($this->contributingFactor, $value));

        return $this;
    }

    /**
     * @return FHIRAdverseEventPreventiveAction[]
     */
    public function getPreventiveAction(): array
    {
        return $this->preventiveAction;
    }

    public function setPreventiveAction(?FHIRAdverseEventPreventiveAction ...$value): self
    {
        $this->preventiveAction = array_filter($value);

        return $this;
    }

    public function addPreventiveAction(?FHIRAdverseEventPreventiveAction ...$value): self
    {
        $this->preventiveAction = array_filter(array_merge($this->preventiveAction, $value));

        return $this;
    }

    /**
     * @return FHIRAdverseEventMitigatingAction[]
     */
    public function getMitigatingAction(): array
    {
        return $this->mitigatingAction;
    }

    public function setMitigatingAction(?FHIRAdverseEventMitigatingAction ...$value): self
    {
        $this->mitigatingAction = array_filter($value);

        return $this;
    }

    public function addMitigatingAction(?FHIRAdverseEventMitigatingAction ...$value): self
    {
        $this->mitigatingAction = array_filter(array_merge($this->mitigatingAction, $value));

        return $this;
    }

    /**
     * @return FHIRAdverseEventSupportingInfo[]
     */
    public function getSupportingInfo(): array
    {
        return $this->supportingInfo;
    }

    public function setSupportingInfo(?FHIRAdverseEventSupportingInfo ...$value): self
    {
        $this->supportingInfo = array_filter($value);

        return $this;
    }

    public function addSupportingInfo(?FHIRAdverseEventSupportingInfo ...$value): self
    {
        $this->supportingInfo = array_filter(array_merge($this->supportingInfo, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }
}
