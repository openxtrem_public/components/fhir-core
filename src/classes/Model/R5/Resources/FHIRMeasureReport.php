<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MeasureReport Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMeasureReportInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMeasureReportGroup;

class FHIRMeasureReport extends FHIRDomainResource implements FHIRMeasureReportInterface
{
    public const RESOURCE_NAME = 'MeasureReport';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCode $type = null;
    protected ?FHIRCode $dataUpdateType = null;
    protected ?FHIRCanonical $measure = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRReference $reporter = null;
    protected ?FHIRReference $reportingVendor = null;
    protected ?FHIRReference $location = null;
    protected ?FHIRPeriod $period = null;
    protected ?FHIRReference $inputParameters = null;
    protected ?FHIRCodeableConcept $scoring = null;
    protected ?FHIRCodeableConcept $improvementNotation = null;

    /** @var FHIRMeasureReportGroup[] */
    protected array $group = [];

    /** @var FHIRReference[] */
    protected array $supplementalData = [];

    /** @var FHIRReference[] */
    protected array $evaluatedResource = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDataUpdateType(): ?FHIRCode
    {
        return $this->dataUpdateType;
    }

    public function setDataUpdateType(string|FHIRCode|null $value): self
    {
        $this->dataUpdateType = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getMeasure(): ?FHIRCanonical
    {
        return $this->measure;
    }

    public function setMeasure(string|FHIRCanonical|null $value): self
    {
        $this->measure = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getReporter(): ?FHIRReference
    {
        return $this->reporter;
    }

    public function setReporter(?FHIRReference $value): self
    {
        $this->reporter = $value;

        return $this;
    }

    public function getReportingVendor(): ?FHIRReference
    {
        return $this->reportingVendor;
    }

    public function setReportingVendor(?FHIRReference $value): self
    {
        $this->reportingVendor = $value;

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    public function getInputParameters(): ?FHIRReference
    {
        return $this->inputParameters;
    }

    public function setInputParameters(?FHIRReference $value): self
    {
        $this->inputParameters = $value;

        return $this;
    }

    public function getScoring(): ?FHIRCodeableConcept
    {
        return $this->scoring;
    }

    public function setScoring(?FHIRCodeableConcept $value): self
    {
        $this->scoring = $value;

        return $this;
    }

    public function getImprovementNotation(): ?FHIRCodeableConcept
    {
        return $this->improvementNotation;
    }

    public function setImprovementNotation(?FHIRCodeableConcept $value): self
    {
        $this->improvementNotation = $value;

        return $this;
    }

    /**
     * @return FHIRMeasureReportGroup[]
     */
    public function getGroup(): array
    {
        return $this->group;
    }

    public function setGroup(?FHIRMeasureReportGroup ...$value): self
    {
        $this->group = array_filter($value);

        return $this;
    }

    public function addGroup(?FHIRMeasureReportGroup ...$value): self
    {
        $this->group = array_filter(array_merge($this->group, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupplementalData(): array
    {
        return $this->supplementalData;
    }

    public function setSupplementalData(?FHIRReference ...$value): self
    {
        $this->supplementalData = array_filter($value);

        return $this;
    }

    public function addSupplementalData(?FHIRReference ...$value): self
    {
        $this->supplementalData = array_filter(array_merge($this->supplementalData, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEvaluatedResource(): array
    {
        return $this->evaluatedResource;
    }

    public function setEvaluatedResource(?FHIRReference ...$value): self
    {
        $this->evaluatedResource = array_filter($value);

        return $this;
    }

    public function addEvaluatedResource(?FHIRReference ...$value): self
    {
        $this->evaluatedResource = array_filter(array_merge($this->evaluatedResource, $value));

        return $this;
    }
}
