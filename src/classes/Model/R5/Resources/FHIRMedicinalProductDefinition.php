<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductDefinition Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMedicinalProductDefinitionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRMarketingStatus;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMedicinalProductDefinitionCharacteristic;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMedicinalProductDefinitionContact;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMedicinalProductDefinitionCrossReference;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMedicinalProductDefinitionName;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMedicinalProductDefinitionOperation;

class FHIRMedicinalProductDefinition extends FHIRDomainResource implements FHIRMedicinalProductDefinitionInterface
{
    public const RESOURCE_NAME = 'MedicinalProductDefinition';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableConcept $domain = null;
    protected ?FHIRString $version = null;
    protected ?FHIRCodeableConcept $status = null;
    protected ?FHIRDateTime $statusDate = null;
    protected ?FHIRMarkdown $description = null;
    protected ?FHIRCodeableConcept $combinedPharmaceuticalDoseForm = null;

    /** @var FHIRCodeableConcept[] */
    protected array $route = [];
    protected ?FHIRMarkdown $indication = null;
    protected ?FHIRCodeableConcept $legalStatusOfSupply = null;
    protected ?FHIRCodeableConcept $additionalMonitoringIndicator = null;

    /** @var FHIRCodeableConcept[] */
    protected array $specialMeasures = [];
    protected ?FHIRCodeableConcept $pediatricUseIndicator = null;

    /** @var FHIRCodeableConcept[] */
    protected array $classification = [];

    /** @var FHIRMarketingStatus[] */
    protected array $marketingStatus = [];

    /** @var FHIRCodeableConcept[] */
    protected array $packagedMedicinalProduct = [];

    /** @var FHIRReference[] */
    protected array $comprisedOf = [];

    /** @var FHIRCodeableConcept[] */
    protected array $ingredient = [];

    /** @var FHIRCodeableReference[] */
    protected array $impurity = [];

    /** @var FHIRReference[] */
    protected array $attachedDocument = [];

    /** @var FHIRReference[] */
    protected array $masterFile = [];

    /** @var FHIRMedicinalProductDefinitionContact[] */
    protected array $contact = [];

    /** @var FHIRReference[] */
    protected array $clinicalTrial = [];

    /** @var FHIRCoding[] */
    protected array $code = [];

    /** @var FHIRMedicinalProductDefinitionName[] */
    protected array $name = [];

    /** @var FHIRMedicinalProductDefinitionCrossReference[] */
    protected array $crossReference = [];

    /** @var FHIRMedicinalProductDefinitionOperation[] */
    protected array $operation = [];

    /** @var FHIRMedicinalProductDefinitionCharacteristic[] */
    protected array $characteristic = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getDomain(): ?FHIRCodeableConcept
    {
        return $this->domain;
    }

    public function setDomain(?FHIRCodeableConcept $value): self
    {
        $this->domain = $value;

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStatus(): ?FHIRCodeableConcept
    {
        return $this->status;
    }

    public function setStatus(?FHIRCodeableConcept $value): self
    {
        $this->status = $value;

        return $this;
    }

    public function getStatusDate(): ?FHIRDateTime
    {
        return $this->statusDate;
    }

    public function setStatusDate(string|FHIRDateTime|null $value): self
    {
        $this->statusDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getCombinedPharmaceuticalDoseForm(): ?FHIRCodeableConcept
    {
        return $this->combinedPharmaceuticalDoseForm;
    }

    public function setCombinedPharmaceuticalDoseForm(?FHIRCodeableConcept $value): self
    {
        $this->combinedPharmaceuticalDoseForm = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getRoute(): array
    {
        return $this->route;
    }

    public function setRoute(?FHIRCodeableConcept ...$value): self
    {
        $this->route = array_filter($value);

        return $this;
    }

    public function addRoute(?FHIRCodeableConcept ...$value): self
    {
        $this->route = array_filter(array_merge($this->route, $value));

        return $this;
    }

    public function getIndication(): ?FHIRMarkdown
    {
        return $this->indication;
    }

    public function setIndication(string|FHIRMarkdown|null $value): self
    {
        $this->indication = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getLegalStatusOfSupply(): ?FHIRCodeableConcept
    {
        return $this->legalStatusOfSupply;
    }

    public function setLegalStatusOfSupply(?FHIRCodeableConcept $value): self
    {
        $this->legalStatusOfSupply = $value;

        return $this;
    }

    public function getAdditionalMonitoringIndicator(): ?FHIRCodeableConcept
    {
        return $this->additionalMonitoringIndicator;
    }

    public function setAdditionalMonitoringIndicator(?FHIRCodeableConcept $value): self
    {
        $this->additionalMonitoringIndicator = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSpecialMeasures(): array
    {
        return $this->specialMeasures;
    }

    public function setSpecialMeasures(?FHIRCodeableConcept ...$value): self
    {
        $this->specialMeasures = array_filter($value);

        return $this;
    }

    public function addSpecialMeasures(?FHIRCodeableConcept ...$value): self
    {
        $this->specialMeasures = array_filter(array_merge($this->specialMeasures, $value));

        return $this;
    }

    public function getPediatricUseIndicator(): ?FHIRCodeableConcept
    {
        return $this->pediatricUseIndicator;
    }

    public function setPediatricUseIndicator(?FHIRCodeableConcept $value): self
    {
        $this->pediatricUseIndicator = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getClassification(): array
    {
        return $this->classification;
    }

    public function setClassification(?FHIRCodeableConcept ...$value): self
    {
        $this->classification = array_filter($value);

        return $this;
    }

    public function addClassification(?FHIRCodeableConcept ...$value): self
    {
        $this->classification = array_filter(array_merge($this->classification, $value));

        return $this;
    }

    /**
     * @return FHIRMarketingStatus[]
     */
    public function getMarketingStatus(): array
    {
        return $this->marketingStatus;
    }

    public function setMarketingStatus(?FHIRMarketingStatus ...$value): self
    {
        $this->marketingStatus = array_filter($value);

        return $this;
    }

    public function addMarketingStatus(?FHIRMarketingStatus ...$value): self
    {
        $this->marketingStatus = array_filter(array_merge($this->marketingStatus, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getPackagedMedicinalProduct(): array
    {
        return $this->packagedMedicinalProduct;
    }

    public function setPackagedMedicinalProduct(?FHIRCodeableConcept ...$value): self
    {
        $this->packagedMedicinalProduct = array_filter($value);

        return $this;
    }

    public function addPackagedMedicinalProduct(?FHIRCodeableConcept ...$value): self
    {
        $this->packagedMedicinalProduct = array_filter(array_merge($this->packagedMedicinalProduct, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getComprisedOf(): array
    {
        return $this->comprisedOf;
    }

    public function setComprisedOf(?FHIRReference ...$value): self
    {
        $this->comprisedOf = array_filter($value);

        return $this;
    }

    public function addComprisedOf(?FHIRReference ...$value): self
    {
        $this->comprisedOf = array_filter(array_merge($this->comprisedOf, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getIngredient(): array
    {
        return $this->ingredient;
    }

    public function setIngredient(?FHIRCodeableConcept ...$value): self
    {
        $this->ingredient = array_filter($value);

        return $this;
    }

    public function addIngredient(?FHIRCodeableConcept ...$value): self
    {
        $this->ingredient = array_filter(array_merge($this->ingredient, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getImpurity(): array
    {
        return $this->impurity;
    }

    public function setImpurity(?FHIRCodeableReference ...$value): self
    {
        $this->impurity = array_filter($value);

        return $this;
    }

    public function addImpurity(?FHIRCodeableReference ...$value): self
    {
        $this->impurity = array_filter(array_merge($this->impurity, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAttachedDocument(): array
    {
        return $this->attachedDocument;
    }

    public function setAttachedDocument(?FHIRReference ...$value): self
    {
        $this->attachedDocument = array_filter($value);

        return $this;
    }

    public function addAttachedDocument(?FHIRReference ...$value): self
    {
        $this->attachedDocument = array_filter(array_merge($this->attachedDocument, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getMasterFile(): array
    {
        return $this->masterFile;
    }

    public function setMasterFile(?FHIRReference ...$value): self
    {
        $this->masterFile = array_filter($value);

        return $this;
    }

    public function addMasterFile(?FHIRReference ...$value): self
    {
        $this->masterFile = array_filter(array_merge($this->masterFile, $value));

        return $this;
    }

    /**
     * @return FHIRMedicinalProductDefinitionContact[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRMedicinalProductDefinitionContact ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRMedicinalProductDefinitionContact ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getClinicalTrial(): array
    {
        return $this->clinicalTrial;
    }

    public function setClinicalTrial(?FHIRReference ...$value): self
    {
        $this->clinicalTrial = array_filter($value);

        return $this;
    }

    public function addClinicalTrial(?FHIRReference ...$value): self
    {
        $this->clinicalTrial = array_filter(array_merge($this->clinicalTrial, $value));

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getCode(): array
    {
        return $this->code;
    }

    public function setCode(?FHIRCoding ...$value): self
    {
        $this->code = array_filter($value);

        return $this;
    }

    public function addCode(?FHIRCoding ...$value): self
    {
        $this->code = array_filter(array_merge($this->code, $value));

        return $this;
    }

    /**
     * @return FHIRMedicinalProductDefinitionName[]
     */
    public function getName(): array
    {
        return $this->name;
    }

    public function setName(?FHIRMedicinalProductDefinitionName ...$value): self
    {
        $this->name = array_filter($value);

        return $this;
    }

    public function addName(?FHIRMedicinalProductDefinitionName ...$value): self
    {
        $this->name = array_filter(array_merge($this->name, $value));

        return $this;
    }

    /**
     * @return FHIRMedicinalProductDefinitionCrossReference[]
     */
    public function getCrossReference(): array
    {
        return $this->crossReference;
    }

    public function setCrossReference(?FHIRMedicinalProductDefinitionCrossReference ...$value): self
    {
        $this->crossReference = array_filter($value);

        return $this;
    }

    public function addCrossReference(?FHIRMedicinalProductDefinitionCrossReference ...$value): self
    {
        $this->crossReference = array_filter(array_merge($this->crossReference, $value));

        return $this;
    }

    /**
     * @return FHIRMedicinalProductDefinitionOperation[]
     */
    public function getOperation(): array
    {
        return $this->operation;
    }

    public function setOperation(?FHIRMedicinalProductDefinitionOperation ...$value): self
    {
        $this->operation = array_filter($value);

        return $this;
    }

    public function addOperation(?FHIRMedicinalProductDefinitionOperation ...$value): self
    {
        $this->operation = array_filter(array_merge($this->operation, $value));

        return $this;
    }

    /**
     * @return FHIRMedicinalProductDefinitionCharacteristic[]
     */
    public function getCharacteristic(): array
    {
        return $this->characteristic;
    }

    public function setCharacteristic(?FHIRMedicinalProductDefinitionCharacteristic ...$value): self
    {
        $this->characteristic = array_filter($value);

        return $this;
    }

    public function addCharacteristic(?FHIRMedicinalProductDefinitionCharacteristic ...$value): self
    {
        $this->characteristic = array_filter(array_merge($this->characteristic, $value));

        return $this;
    }
}
