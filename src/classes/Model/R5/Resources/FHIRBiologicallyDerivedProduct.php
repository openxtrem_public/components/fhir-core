<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR BiologicallyDerivedProduct Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRBiologicallyDerivedProductInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRBiologicallyDerivedProductCollection;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRBiologicallyDerivedProductProperty;

class FHIRBiologicallyDerivedProduct extends FHIRDomainResource implements FHIRBiologicallyDerivedProductInterface
{
    public const RESOURCE_NAME = 'BiologicallyDerivedProduct';

    protected ?FHIRCoding $productCategory = null;
    protected ?FHIRCodeableConcept $productCode = null;

    /** @var FHIRReference[] */
    protected array $parent = [];

    /** @var FHIRReference[] */
    protected array $request = [];

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRIdentifier $biologicalSourceEvent = null;

    /** @var FHIRReference[] */
    protected array $processingFacility = [];
    protected ?FHIRString $division = null;
    protected ?FHIRCoding $productStatus = null;
    protected ?FHIRDateTime $expirationDate = null;
    protected ?FHIRBiologicallyDerivedProductCollection $collection = null;
    protected ?FHIRRange $storageTempRequirements = null;

    /** @var FHIRBiologicallyDerivedProductProperty[] */
    protected array $property = [];

    public function getProductCategory(): ?FHIRCoding
    {
        return $this->productCategory;
    }

    public function setProductCategory(?FHIRCoding $value): self
    {
        $this->productCategory = $value;

        return $this;
    }

    public function getProductCode(): ?FHIRCodeableConcept
    {
        return $this->productCode;
    }

    public function setProductCode(?FHIRCodeableConcept $value): self
    {
        $this->productCode = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getParent(): array
    {
        return $this->parent;
    }

    public function setParent(?FHIRReference ...$value): self
    {
        $this->parent = array_filter($value);

        return $this;
    }

    public function addParent(?FHIRReference ...$value): self
    {
        $this->parent = array_filter(array_merge($this->parent, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getRequest(): array
    {
        return $this->request;
    }

    public function setRequest(?FHIRReference ...$value): self
    {
        $this->request = array_filter($value);

        return $this;
    }

    public function addRequest(?FHIRReference ...$value): self
    {
        $this->request = array_filter(array_merge($this->request, $value));

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getBiologicalSourceEvent(): ?FHIRIdentifier
    {
        return $this->biologicalSourceEvent;
    }

    public function setBiologicalSourceEvent(?FHIRIdentifier $value): self
    {
        $this->biologicalSourceEvent = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getProcessingFacility(): array
    {
        return $this->processingFacility;
    }

    public function setProcessingFacility(?FHIRReference ...$value): self
    {
        $this->processingFacility = array_filter($value);

        return $this;
    }

    public function addProcessingFacility(?FHIRReference ...$value): self
    {
        $this->processingFacility = array_filter(array_merge($this->processingFacility, $value));

        return $this;
    }

    public function getDivision(): ?FHIRString
    {
        return $this->division;
    }

    public function setDivision(string|FHIRString|null $value): self
    {
        $this->division = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getProductStatus(): ?FHIRCoding
    {
        return $this->productStatus;
    }

    public function setProductStatus(?FHIRCoding $value): self
    {
        $this->productStatus = $value;

        return $this;
    }

    public function getExpirationDate(): ?FHIRDateTime
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(string|FHIRDateTime|null $value): self
    {
        $this->expirationDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getCollection(): ?FHIRBiologicallyDerivedProductCollection
    {
        return $this->collection;
    }

    public function setCollection(?FHIRBiologicallyDerivedProductCollection $value): self
    {
        $this->collection = $value;

        return $this;
    }

    public function getStorageTempRequirements(): ?FHIRRange
    {
        return $this->storageTempRequirements;
    }

    public function setStorageTempRequirements(?FHIRRange $value): self
    {
        $this->storageTempRequirements = $value;

        return $this;
    }

    /**
     * @return FHIRBiologicallyDerivedProductProperty[]
     */
    public function getProperty(): array
    {
        return $this->property;
    }

    public function setProperty(?FHIRBiologicallyDerivedProductProperty ...$value): self
    {
        $this->property = array_filter($value);

        return $this;
    }

    public function addProperty(?FHIRBiologicallyDerivedProductProperty ...$value): self
    {
        $this->property = array_filter(array_merge($this->property, $value));

        return $this;
    }
}
