<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Subscription Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRSubscriptionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUnsignedInt;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUrl;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRSubscriptionFilterBy;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRSubscriptionParameter;

class FHIRSubscription extends FHIRDomainResource implements FHIRSubscriptionInterface
{
    public const RESOURCE_NAME = 'Subscription';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRString $name = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRCanonical $topic = null;

    /** @var FHIRContactPoint[] */
    protected array $contact = [];
    protected ?FHIRInstant $end = null;
    protected ?FHIRReference $managingEntity = null;
    protected ?FHIRString $reason = null;

    /** @var FHIRSubscriptionFilterBy[] */
    protected array $filterBy = [];
    protected ?FHIRCoding $channelType = null;
    protected ?FHIRUrl $endpoint = null;

    /** @var FHIRSubscriptionParameter[] */
    protected array $parameter = [];
    protected ?FHIRUnsignedInt $heartbeatPeriod = null;
    protected ?FHIRUnsignedInt $timeout = null;
    protected ?FHIRCode $contentType = null;
    protected ?FHIRCode $content = null;
    protected ?FHIRPositiveInt $maxCount = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getTopic(): ?FHIRCanonical
    {
        return $this->topic;
    }

    public function setTopic(string|FHIRCanonical|null $value): self
    {
        $this->topic = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactPoint ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactPoint ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    public function getEnd(): ?FHIRInstant
    {
        return $this->end;
    }

    public function setEnd(string|FHIRInstant|null $value): self
    {
        $this->end = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    public function getManagingEntity(): ?FHIRReference
    {
        return $this->managingEntity;
    }

    public function setManagingEntity(?FHIRReference $value): self
    {
        $this->managingEntity = $value;

        return $this;
    }

    public function getReason(): ?FHIRString
    {
        return $this->reason;
    }

    public function setReason(string|FHIRString|null $value): self
    {
        $this->reason = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRSubscriptionFilterBy[]
     */
    public function getFilterBy(): array
    {
        return $this->filterBy;
    }

    public function setFilterBy(?FHIRSubscriptionFilterBy ...$value): self
    {
        $this->filterBy = array_filter($value);

        return $this;
    }

    public function addFilterBy(?FHIRSubscriptionFilterBy ...$value): self
    {
        $this->filterBy = array_filter(array_merge($this->filterBy, $value));

        return $this;
    }

    public function getChannelType(): ?FHIRCoding
    {
        return $this->channelType;
    }

    public function setChannelType(?FHIRCoding $value): self
    {
        $this->channelType = $value;

        return $this;
    }

    public function getEndpoint(): ?FHIRUrl
    {
        return $this->endpoint;
    }

    public function setEndpoint(string|FHIRUrl|null $value): self
    {
        $this->endpoint = is_string($value) ? (new FHIRUrl())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRSubscriptionParameter[]
     */
    public function getParameter(): array
    {
        return $this->parameter;
    }

    public function setParameter(?FHIRSubscriptionParameter ...$value): self
    {
        $this->parameter = array_filter($value);

        return $this;
    }

    public function addParameter(?FHIRSubscriptionParameter ...$value): self
    {
        $this->parameter = array_filter(array_merge($this->parameter, $value));

        return $this;
    }

    public function getHeartbeatPeriod(): ?FHIRUnsignedInt
    {
        return $this->heartbeatPeriod;
    }

    public function setHeartbeatPeriod(int|FHIRUnsignedInt|null $value): self
    {
        $this->heartbeatPeriod = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getTimeout(): ?FHIRUnsignedInt
    {
        return $this->timeout;
    }

    public function setTimeout(int|FHIRUnsignedInt|null $value): self
    {
        $this->timeout = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getContentType(): ?FHIRCode
    {
        return $this->contentType;
    }

    public function setContentType(string|FHIRCode|null $value): self
    {
        $this->contentType = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getContent(): ?FHIRCode
    {
        return $this->content;
    }

    public function setContent(string|FHIRCode|null $value): self
    {
        $this->content = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getMaxCount(): ?FHIRPositiveInt
    {
        return $this->maxCount;
    }

    public function setMaxCount(int|FHIRPositiveInt|null $value): self
    {
        $this->maxCount = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }
}
