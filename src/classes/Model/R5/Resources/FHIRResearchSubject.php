<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ResearchSubject Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRResearchSubjectInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRResearchSubjectProgress;

class FHIRResearchSubject extends FHIRDomainResource implements FHIRResearchSubjectInterface
{
    public const RESOURCE_NAME = 'ResearchSubject';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;

    /** @var FHIRResearchSubjectProgress[] */
    protected array $progress = [];
    protected ?FHIRPeriod $period = null;
    protected ?FHIRReference $study = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRId $assignedComparisonGroup = null;
    protected ?FHIRId $actualComparisonGroup = null;

    /** @var FHIRReference[] */
    protected array $consent = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRResearchSubjectProgress[]
     */
    public function getProgress(): array
    {
        return $this->progress;
    }

    public function setProgress(?FHIRResearchSubjectProgress ...$value): self
    {
        $this->progress = array_filter($value);

        return $this;
    }

    public function addProgress(?FHIRResearchSubjectProgress ...$value): self
    {
        $this->progress = array_filter(array_merge($this->progress, $value));

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    public function getStudy(): ?FHIRReference
    {
        return $this->study;
    }

    public function setStudy(?FHIRReference $value): self
    {
        $this->study = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getAssignedComparisonGroup(): ?FHIRId
    {
        return $this->assignedComparisonGroup;
    }

    public function setAssignedComparisonGroup(string|FHIRId|null $value): self
    {
        $this->assignedComparisonGroup = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getActualComparisonGroup(): ?FHIRId
    {
        return $this->actualComparisonGroup;
    }

    public function setActualComparisonGroup(string|FHIRId|null $value): self
    {
        $this->actualComparisonGroup = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getConsent(): array
    {
        return $this->consent;
    }

    public function setConsent(?FHIRReference ...$value): self
    {
        $this->consent = array_filter($value);

        return $this;
    }

    public function addConsent(?FHIRReference ...$value): self
    {
        $this->consent = array_filter(array_merge($this->consent, $value));

        return $this;
    }
}
