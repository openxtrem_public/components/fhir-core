<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Measure Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMeasureInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRelatedArtifact;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMeasureGroup;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMeasureSupplementalData;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMeasureTerm;

class FHIRMeasure extends FHIRDomainResource implements FHIRMeasureInterface
{
    public const RESOURCE_NAME = 'Measure';

    protected ?FHIRUri $url = null;

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRString $version = null;
    protected FHIRString|FHIRCoding|null $versionAlgorithm = null;
    protected ?FHIRString $name = null;
    protected ?FHIRString $title = null;
    protected ?FHIRString $subtitle = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRBoolean $experimental = null;
    protected FHIRCodeableConcept|FHIRReference|null $subject = null;
    protected ?FHIRCode $basis = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRString $publisher = null;

    /** @var FHIRContactDetail[] */
    protected array $contact = [];
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRUsageContext[] */
    protected array $useContext = [];

    /** @var FHIRCodeableConcept[] */
    protected array $jurisdiction = [];
    protected ?FHIRMarkdown $purpose = null;
    protected ?FHIRMarkdown $usage = null;
    protected ?FHIRMarkdown $copyright = null;
    protected ?FHIRString $copyrightLabel = null;
    protected ?FHIRDate $approvalDate = null;
    protected ?FHIRDate $lastReviewDate = null;
    protected ?FHIRPeriod $effectivePeriod = null;

    /** @var FHIRCodeableConcept[] */
    protected array $topic = [];

    /** @var FHIRContactDetail[] */
    protected array $author = [];

    /** @var FHIRContactDetail[] */
    protected array $editor = [];

    /** @var FHIRContactDetail[] */
    protected array $reviewer = [];

    /** @var FHIRContactDetail[] */
    protected array $endorser = [];

    /** @var FHIRRelatedArtifact[] */
    protected array $relatedArtifact = [];

    /** @var FHIRCanonical[] */
    protected array $library = [];
    protected ?FHIRMarkdown $disclaimer = null;
    protected ?FHIRCodeableConcept $scoring = null;
    protected ?FHIRCodeableConcept $scoringUnit = null;
    protected ?FHIRCodeableConcept $compositeScoring = null;

    /** @var FHIRCodeableConcept[] */
    protected array $type = [];
    protected ?FHIRMarkdown $riskAdjustment = null;
    protected ?FHIRMarkdown $rateAggregation = null;
    protected ?FHIRMarkdown $rationale = null;
    protected ?FHIRMarkdown $clinicalRecommendationStatement = null;
    protected ?FHIRCodeableConcept $improvementNotation = null;

    /** @var FHIRMeasureTerm[] */
    protected array $term = [];
    protected ?FHIRMarkdown $guidance = null;

    /** @var FHIRMeasureGroup[] */
    protected array $group = [];

    /** @var FHIRMeasureSupplementalData[] */
    protected array $supplementalData = [];

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getVersionAlgorithm(): FHIRString|FHIRCoding|null
    {
        return $this->versionAlgorithm;
    }

    public function setVersionAlgorithm(FHIRString|FHIRCoding|null $value): self
    {
        $this->versionAlgorithm = $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSubtitle(): ?FHIRString
    {
        return $this->subtitle;
    }

    public function setSubtitle(string|FHIRString|null $value): self
    {
        $this->subtitle = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getExperimental(): ?FHIRBoolean
    {
        return $this->experimental;
    }

    public function setExperimental(bool|FHIRBoolean|null $value): self
    {
        $this->experimental = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getSubject(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->subject;
    }

    public function setSubject(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getBasis(): ?FHIRCode
    {
        return $this->basis;
    }

    public function setBasis(string|FHIRCode|null $value): self
    {
        $this->basis = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPublisher(): ?FHIRString
    {
        return $this->publisher;
    }

    public function setPublisher(string|FHIRString|null $value): self
    {
        $this->publisher = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUseContext(): array
    {
        return $this->useContext;
    }

    public function setUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter($value);

        return $this;
    }

    public function addUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter(array_merge($this->useContext, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getJurisdiction(): array
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter($value);

        return $this;
    }

    public function addJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter(array_merge($this->jurisdiction, $value));

        return $this;
    }

    public function getPurpose(): ?FHIRMarkdown
    {
        return $this->purpose;
    }

    public function setPurpose(string|FHIRMarkdown|null $value): self
    {
        $this->purpose = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getUsage(): ?FHIRMarkdown
    {
        return $this->usage;
    }

    public function setUsage(string|FHIRMarkdown|null $value): self
    {
        $this->usage = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getCopyright(): ?FHIRMarkdown
    {
        return $this->copyright;
    }

    public function setCopyright(string|FHIRMarkdown|null $value): self
    {
        $this->copyright = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getCopyrightLabel(): ?FHIRString
    {
        return $this->copyrightLabel;
    }

    public function setCopyrightLabel(string|FHIRString|null $value): self
    {
        $this->copyrightLabel = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getApprovalDate(): ?FHIRDate
    {
        return $this->approvalDate;
    }

    public function setApprovalDate(string|FHIRDate|null $value): self
    {
        $this->approvalDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getLastReviewDate(): ?FHIRDate
    {
        return $this->lastReviewDate;
    }

    public function setLastReviewDate(string|FHIRDate|null $value): self
    {
        $this->lastReviewDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getEffectivePeriod(): ?FHIRPeriod
    {
        return $this->effectivePeriod;
    }

    public function setEffectivePeriod(?FHIRPeriod $value): self
    {
        $this->effectivePeriod = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getTopic(): array
    {
        return $this->topic;
    }

    public function setTopic(?FHIRCodeableConcept ...$value): self
    {
        $this->topic = array_filter($value);

        return $this;
    }

    public function addTopic(?FHIRCodeableConcept ...$value): self
    {
        $this->topic = array_filter(array_merge($this->topic, $value));

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getAuthor(): array
    {
        return $this->author;
    }

    public function setAuthor(?FHIRContactDetail ...$value): self
    {
        $this->author = array_filter($value);

        return $this;
    }

    public function addAuthor(?FHIRContactDetail ...$value): self
    {
        $this->author = array_filter(array_merge($this->author, $value));

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getEditor(): array
    {
        return $this->editor;
    }

    public function setEditor(?FHIRContactDetail ...$value): self
    {
        $this->editor = array_filter($value);

        return $this;
    }

    public function addEditor(?FHIRContactDetail ...$value): self
    {
        $this->editor = array_filter(array_merge($this->editor, $value));

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getReviewer(): array
    {
        return $this->reviewer;
    }

    public function setReviewer(?FHIRContactDetail ...$value): self
    {
        $this->reviewer = array_filter($value);

        return $this;
    }

    public function addReviewer(?FHIRContactDetail ...$value): self
    {
        $this->reviewer = array_filter(array_merge($this->reviewer, $value));

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getEndorser(): array
    {
        return $this->endorser;
    }

    public function setEndorser(?FHIRContactDetail ...$value): self
    {
        $this->endorser = array_filter($value);

        return $this;
    }

    public function addEndorser(?FHIRContactDetail ...$value): self
    {
        $this->endorser = array_filter(array_merge($this->endorser, $value));

        return $this;
    }

    /**
     * @return FHIRRelatedArtifact[]
     */
    public function getRelatedArtifact(): array
    {
        return $this->relatedArtifact;
    }

    public function setRelatedArtifact(?FHIRRelatedArtifact ...$value): self
    {
        $this->relatedArtifact = array_filter($value);

        return $this;
    }

    public function addRelatedArtifact(?FHIRRelatedArtifact ...$value): self
    {
        $this->relatedArtifact = array_filter(array_merge($this->relatedArtifact, $value));

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getLibrary(): array
    {
        return $this->library;
    }

    public function setLibrary(string|FHIRCanonical|null ...$value): self
    {
        $this->library = [];
        $this->addLibrary(...$value);

        return $this;
    }

    public function addLibrary(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->library = array_filter(array_merge($this->library, $values));

        return $this;
    }

    public function getDisclaimer(): ?FHIRMarkdown
    {
        return $this->disclaimer;
    }

    public function setDisclaimer(string|FHIRMarkdown|null $value): self
    {
        $this->disclaimer = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getScoring(): ?FHIRCodeableConcept
    {
        return $this->scoring;
    }

    public function setScoring(?FHIRCodeableConcept $value): self
    {
        $this->scoring = $value;

        return $this;
    }

    public function getScoringUnit(): ?FHIRCodeableConcept
    {
        return $this->scoringUnit;
    }

    public function setScoringUnit(?FHIRCodeableConcept $value): self
    {
        $this->scoringUnit = $value;

        return $this;
    }

    public function getCompositeScoring(): ?FHIRCodeableConcept
    {
        return $this->compositeScoring;
    }

    public function setCompositeScoring(?FHIRCodeableConcept $value): self
    {
        $this->compositeScoring = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }

    public function getRiskAdjustment(): ?FHIRMarkdown
    {
        return $this->riskAdjustment;
    }

    public function setRiskAdjustment(string|FHIRMarkdown|null $value): self
    {
        $this->riskAdjustment = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getRateAggregation(): ?FHIRMarkdown
    {
        return $this->rateAggregation;
    }

    public function setRateAggregation(string|FHIRMarkdown|null $value): self
    {
        $this->rateAggregation = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getRationale(): ?FHIRMarkdown
    {
        return $this->rationale;
    }

    public function setRationale(string|FHIRMarkdown|null $value): self
    {
        $this->rationale = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getClinicalRecommendationStatement(): ?FHIRMarkdown
    {
        return $this->clinicalRecommendationStatement;
    }

    public function setClinicalRecommendationStatement(string|FHIRMarkdown|null $value): self
    {
        $this->clinicalRecommendationStatement = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getImprovementNotation(): ?FHIRCodeableConcept
    {
        return $this->improvementNotation;
    }

    public function setImprovementNotation(?FHIRCodeableConcept $value): self
    {
        $this->improvementNotation = $value;

        return $this;
    }

    /**
     * @return FHIRMeasureTerm[]
     */
    public function getTerm(): array
    {
        return $this->term;
    }

    public function setTerm(?FHIRMeasureTerm ...$value): self
    {
        $this->term = array_filter($value);

        return $this;
    }

    public function addTerm(?FHIRMeasureTerm ...$value): self
    {
        $this->term = array_filter(array_merge($this->term, $value));

        return $this;
    }

    public function getGuidance(): ?FHIRMarkdown
    {
        return $this->guidance;
    }

    public function setGuidance(string|FHIRMarkdown|null $value): self
    {
        $this->guidance = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRMeasureGroup[]
     */
    public function getGroup(): array
    {
        return $this->group;
    }

    public function setGroup(?FHIRMeasureGroup ...$value): self
    {
        $this->group = array_filter($value);

        return $this;
    }

    public function addGroup(?FHIRMeasureGroup ...$value): self
    {
        $this->group = array_filter(array_merge($this->group, $value));

        return $this;
    }

    /**
     * @return FHIRMeasureSupplementalData[]
     */
    public function getSupplementalData(): array
    {
        return $this->supplementalData;
    }

    public function setSupplementalData(?FHIRMeasureSupplementalData ...$value): self
    {
        $this->supplementalData = array_filter($value);

        return $this;
    }

    public function addSupplementalData(?FHIRMeasureSupplementalData ...$value): self
    {
        $this->supplementalData = array_filter(array_merge($this->supplementalData, $value));

        return $this;
    }
}
