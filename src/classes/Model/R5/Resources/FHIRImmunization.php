<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Immunization Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRImmunizationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRImmunizationPerformer;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRImmunizationProgramEligibility;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRImmunizationProtocolApplied;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRImmunizationReaction;

class FHIRImmunization extends FHIRDomainResource implements FHIRImmunizationInterface
{
    public const RESOURCE_NAME = 'Immunization';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $statusReason = null;
    protected ?FHIRCodeableConcept $vaccineCode = null;
    protected ?FHIRCodeableReference $administeredProduct = null;
    protected ?FHIRCodeableReference $manufacturer = null;
    protected ?FHIRString $lotNumber = null;
    protected ?FHIRDate $expirationDate = null;
    protected ?FHIRReference $patient = null;
    protected ?FHIRReference $encounter = null;

    /** @var FHIRReference[] */
    protected array $supportingInformation = [];
    protected FHIRDateTime|FHIRString|null $occurrence = null;
    protected ?FHIRBoolean $primarySource = null;
    protected ?FHIRCodeableReference $informationSource = null;
    protected ?FHIRReference $location = null;
    protected ?FHIRCodeableConcept $site = null;
    protected ?FHIRCodeableConcept $route = null;
    protected ?FHIRQuantity $doseQuantity = null;

    /** @var FHIRImmunizationPerformer[] */
    protected array $performer = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRCodeableReference[] */
    protected array $reason = [];
    protected ?FHIRBoolean $isSubpotent = null;

    /** @var FHIRCodeableConcept[] */
    protected array $subpotentReason = [];

    /** @var FHIRImmunizationProgramEligibility[] */
    protected array $programEligibility = [];
    protected ?FHIRCodeableConcept $fundingSource = null;

    /** @var FHIRImmunizationReaction[] */
    protected array $reaction = [];

    /** @var FHIRImmunizationProtocolApplied[] */
    protected array $protocolApplied = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getStatusReason(): ?FHIRCodeableConcept
    {
        return $this->statusReason;
    }

    public function setStatusReason(?FHIRCodeableConcept $value): self
    {
        $this->statusReason = $value;

        return $this;
    }

    public function getVaccineCode(): ?FHIRCodeableConcept
    {
        return $this->vaccineCode;
    }

    public function setVaccineCode(?FHIRCodeableConcept $value): self
    {
        $this->vaccineCode = $value;

        return $this;
    }

    public function getAdministeredProduct(): ?FHIRCodeableReference
    {
        return $this->administeredProduct;
    }

    public function setAdministeredProduct(?FHIRCodeableReference $value): self
    {
        $this->administeredProduct = $value;

        return $this;
    }

    public function getManufacturer(): ?FHIRCodeableReference
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?FHIRCodeableReference $value): self
    {
        $this->manufacturer = $value;

        return $this;
    }

    public function getLotNumber(): ?FHIRString
    {
        return $this->lotNumber;
    }

    public function setLotNumber(string|FHIRString|null $value): self
    {
        $this->lotNumber = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getExpirationDate(): ?FHIRDate
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(string|FHIRDate|null $value): self
    {
        $this->expirationDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupportingInformation(): array
    {
        return $this->supportingInformation;
    }

    public function setSupportingInformation(?FHIRReference ...$value): self
    {
        $this->supportingInformation = array_filter($value);

        return $this;
    }

    public function addSupportingInformation(?FHIRReference ...$value): self
    {
        $this->supportingInformation = array_filter(array_merge($this->supportingInformation, $value));

        return $this;
    }

    public function getOccurrence(): FHIRDateTime|FHIRString|null
    {
        return $this->occurrence;
    }

    public function setOccurrence(FHIRDateTime|FHIRString|null $value): self
    {
        $this->occurrence = $value;

        return $this;
    }

    public function getPrimarySource(): ?FHIRBoolean
    {
        return $this->primarySource;
    }

    public function setPrimarySource(bool|FHIRBoolean|null $value): self
    {
        $this->primarySource = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getInformationSource(): ?FHIRCodeableReference
    {
        return $this->informationSource;
    }

    public function setInformationSource(?FHIRCodeableReference $value): self
    {
        $this->informationSource = $value;

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    public function getSite(): ?FHIRCodeableConcept
    {
        return $this->site;
    }

    public function setSite(?FHIRCodeableConcept $value): self
    {
        $this->site = $value;

        return $this;
    }

    public function getRoute(): ?FHIRCodeableConcept
    {
        return $this->route;
    }

    public function setRoute(?FHIRCodeableConcept $value): self
    {
        $this->route = $value;

        return $this;
    }

    public function getDoseQuantity(): ?FHIRQuantity
    {
        return $this->doseQuantity;
    }

    public function setDoseQuantity(?FHIRQuantity $value): self
    {
        $this->doseQuantity = $value;

        return $this;
    }

    /**
     * @return FHIRImmunizationPerformer[]
     */
    public function getPerformer(): array
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRImmunizationPerformer ...$value): self
    {
        $this->performer = array_filter($value);

        return $this;
    }

    public function addPerformer(?FHIRImmunizationPerformer ...$value): self
    {
        $this->performer = array_filter(array_merge($this->performer, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getReason(): array
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableReference ...$value): self
    {
        $this->reason = array_filter($value);

        return $this;
    }

    public function addReason(?FHIRCodeableReference ...$value): self
    {
        $this->reason = array_filter(array_merge($this->reason, $value));

        return $this;
    }

    public function getIsSubpotent(): ?FHIRBoolean
    {
        return $this->isSubpotent;
    }

    public function setIsSubpotent(bool|FHIRBoolean|null $value): self
    {
        $this->isSubpotent = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSubpotentReason(): array
    {
        return $this->subpotentReason;
    }

    public function setSubpotentReason(?FHIRCodeableConcept ...$value): self
    {
        $this->subpotentReason = array_filter($value);

        return $this;
    }

    public function addSubpotentReason(?FHIRCodeableConcept ...$value): self
    {
        $this->subpotentReason = array_filter(array_merge($this->subpotentReason, $value));

        return $this;
    }

    /**
     * @return FHIRImmunizationProgramEligibility[]
     */
    public function getProgramEligibility(): array
    {
        return $this->programEligibility;
    }

    public function setProgramEligibility(?FHIRImmunizationProgramEligibility ...$value): self
    {
        $this->programEligibility = array_filter($value);

        return $this;
    }

    public function addProgramEligibility(?FHIRImmunizationProgramEligibility ...$value): self
    {
        $this->programEligibility = array_filter(array_merge($this->programEligibility, $value));

        return $this;
    }

    public function getFundingSource(): ?FHIRCodeableConcept
    {
        return $this->fundingSource;
    }

    public function setFundingSource(?FHIRCodeableConcept $value): self
    {
        $this->fundingSource = $value;

        return $this;
    }

    /**
     * @return FHIRImmunizationReaction[]
     */
    public function getReaction(): array
    {
        return $this->reaction;
    }

    public function setReaction(?FHIRImmunizationReaction ...$value): self
    {
        $this->reaction = array_filter($value);

        return $this;
    }

    public function addReaction(?FHIRImmunizationReaction ...$value): self
    {
        $this->reaction = array_filter(array_merge($this->reaction, $value));

        return $this;
    }

    /**
     * @return FHIRImmunizationProtocolApplied[]
     */
    public function getProtocolApplied(): array
    {
        return $this->protocolApplied;
    }

    public function setProtocolApplied(?FHIRImmunizationProtocolApplied ...$value): self
    {
        $this->protocolApplied = array_filter($value);

        return $this;
    }

    public function addProtocolApplied(?FHIRImmunizationProtocolApplied ...$value): self
    {
        $this->protocolApplied = array_filter(array_merge($this->protocolApplied, $value));

        return $this;
    }
}
