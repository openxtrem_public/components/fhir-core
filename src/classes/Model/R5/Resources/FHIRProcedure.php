<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Procedure Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRProcedureInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAge;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRProcedureFocalDevice;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRProcedurePerformer;

class FHIRProcedure extends FHIRDomainResource implements FHIRProcedureInterface
{
    public const RESOURCE_NAME = 'Procedure';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRCanonical[] */
    protected array $instantiatesCanonical = [];

    /** @var FHIRUri[] */
    protected array $instantiatesUri = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];

    /** @var FHIRReference[] */
    protected array $partOf = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $statusReason = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $focus = null;
    protected ?FHIRReference $encounter = null;
    protected FHIRDateTime|FHIRPeriod|FHIRString|FHIRAge|FHIRRange|FHIRTiming|null $occurrence = null;
    protected ?FHIRDateTime $recorded = null;
    protected ?FHIRReference $recorder = null;
    protected FHIRBoolean|FHIRReference|null $reported = null;

    /** @var FHIRProcedurePerformer[] */
    protected array $performer = [];
    protected ?FHIRReference $location = null;

    /** @var FHIRCodeableReference[] */
    protected array $reason = [];

    /** @var FHIRCodeableConcept[] */
    protected array $bodySite = [];
    protected ?FHIRCodeableConcept $outcome = null;

    /** @var FHIRReference[] */
    protected array $report = [];

    /** @var FHIRCodeableReference[] */
    protected array $complication = [];

    /** @var FHIRCodeableConcept[] */
    protected array $followUp = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRProcedureFocalDevice[] */
    protected array $focalDevice = [];

    /** @var FHIRCodeableReference[] */
    protected array $used = [];

    /** @var FHIRReference[] */
    protected array $supportingInfo = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getInstantiatesCanonical(): array
    {
        return $this->instantiatesCanonical;
    }

    public function setInstantiatesCanonical(string|FHIRCanonical|null ...$value): self
    {
        $this->instantiatesCanonical = [];
        $this->addInstantiatesCanonical(...$value);

        return $this;
    }

    public function addInstantiatesCanonical(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->instantiatesCanonical = array_filter(array_merge($this->instantiatesCanonical, $values));

        return $this;
    }

    /**
     * @return FHIRUri[]
     */
    public function getInstantiatesUri(): array
    {
        return $this->instantiatesUri;
    }

    public function setInstantiatesUri(string|FHIRUri|null ...$value): self
    {
        $this->instantiatesUri = [];
        $this->addInstantiatesUri(...$value);

        return $this;
    }

    public function addInstantiatesUri(string|FHIRUri|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUri())->setValue($v) : $v, $value);

        $this->instantiatesUri = array_filter(array_merge($this->instantiatesUri, $values));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPartOf(): array
    {
        return $this->partOf;
    }

    public function setPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter($value);

        return $this;
    }

    public function addPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter(array_merge($this->partOf, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getStatusReason(): ?FHIRCodeableConcept
    {
        return $this->statusReason;
    }

    public function setStatusReason(?FHIRCodeableConcept $value): self
    {
        $this->statusReason = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getFocus(): ?FHIRReference
    {
        return $this->focus;
    }

    public function setFocus(?FHIRReference $value): self
    {
        $this->focus = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getOccurrence(): FHIRDateTime|FHIRPeriod|FHIRString|FHIRAge|FHIRRange|FHIRTiming|null
    {
        return $this->occurrence;
    }

    public function setOccurrence(FHIRDateTime|FHIRPeriod|FHIRString|FHIRAge|FHIRRange|FHIRTiming|null $value): self
    {
        $this->occurrence = $value;

        return $this;
    }

    public function getRecorded(): ?FHIRDateTime
    {
        return $this->recorded;
    }

    public function setRecorded(string|FHIRDateTime|null $value): self
    {
        $this->recorded = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getRecorder(): ?FHIRReference
    {
        return $this->recorder;
    }

    public function setRecorder(?FHIRReference $value): self
    {
        $this->recorder = $value;

        return $this;
    }

    public function getReported(): FHIRBoolean|FHIRReference|null
    {
        return $this->reported;
    }

    public function setReported(FHIRBoolean|FHIRReference|null $value): self
    {
        $this->reported = $value;

        return $this;
    }

    /**
     * @return FHIRProcedurePerformer[]
     */
    public function getPerformer(): array
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRProcedurePerformer ...$value): self
    {
        $this->performer = array_filter($value);

        return $this;
    }

    public function addPerformer(?FHIRProcedurePerformer ...$value): self
    {
        $this->performer = array_filter(array_merge($this->performer, $value));

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getReason(): array
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableReference ...$value): self
    {
        $this->reason = array_filter($value);

        return $this;
    }

    public function addReason(?FHIRCodeableReference ...$value): self
    {
        $this->reason = array_filter(array_merge($this->reason, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getBodySite(): array
    {
        return $this->bodySite;
    }

    public function setBodySite(?FHIRCodeableConcept ...$value): self
    {
        $this->bodySite = array_filter($value);

        return $this;
    }

    public function addBodySite(?FHIRCodeableConcept ...$value): self
    {
        $this->bodySite = array_filter(array_merge($this->bodySite, $value));

        return $this;
    }

    public function getOutcome(): ?FHIRCodeableConcept
    {
        return $this->outcome;
    }

    public function setOutcome(?FHIRCodeableConcept $value): self
    {
        $this->outcome = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReport(): array
    {
        return $this->report;
    }

    public function setReport(?FHIRReference ...$value): self
    {
        $this->report = array_filter($value);

        return $this;
    }

    public function addReport(?FHIRReference ...$value): self
    {
        $this->report = array_filter(array_merge($this->report, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getComplication(): array
    {
        return $this->complication;
    }

    public function setComplication(?FHIRCodeableReference ...$value): self
    {
        $this->complication = array_filter($value);

        return $this;
    }

    public function addComplication(?FHIRCodeableReference ...$value): self
    {
        $this->complication = array_filter(array_merge($this->complication, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getFollowUp(): array
    {
        return $this->followUp;
    }

    public function setFollowUp(?FHIRCodeableConcept ...$value): self
    {
        $this->followUp = array_filter($value);

        return $this;
    }

    public function addFollowUp(?FHIRCodeableConcept ...$value): self
    {
        $this->followUp = array_filter(array_merge($this->followUp, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRProcedureFocalDevice[]
     */
    public function getFocalDevice(): array
    {
        return $this->focalDevice;
    }

    public function setFocalDevice(?FHIRProcedureFocalDevice ...$value): self
    {
        $this->focalDevice = array_filter($value);

        return $this;
    }

    public function addFocalDevice(?FHIRProcedureFocalDevice ...$value): self
    {
        $this->focalDevice = array_filter(array_merge($this->focalDevice, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getUsed(): array
    {
        return $this->used;
    }

    public function setUsed(?FHIRCodeableReference ...$value): self
    {
        $this->used = array_filter($value);

        return $this;
    }

    public function addUsed(?FHIRCodeableReference ...$value): self
    {
        $this->used = array_filter(array_merge($this->used, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupportingInfo(): array
    {
        return $this->supportingInfo;
    }

    public function setSupportingInfo(?FHIRReference ...$value): self
    {
        $this->supportingInfo = array_filter($value);

        return $this;
    }

    public function addSupportingInfo(?FHIRReference ...$value): self
    {
        $this->supportingInfo = array_filter(array_merge($this->supportingInfo, $value));

        return $this;
    }
}
