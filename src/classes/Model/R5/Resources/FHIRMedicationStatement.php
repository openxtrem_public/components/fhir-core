<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationStatement Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMedicationStatementInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRDosage;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMedicationStatementAdherence;

class FHIRMedicationStatement extends FHIRDomainResource implements FHIRMedicationStatementInterface
{
    public const RESOURCE_NAME = 'MedicationStatement';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRReference[] */
    protected array $partOf = [];
    protected ?FHIRCode $status = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRCodeableReference $medication = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $encounter = null;
    protected FHIRDateTime|FHIRPeriod|FHIRTiming|null $effective = null;
    protected ?FHIRDateTime $dateAsserted = null;

    /** @var FHIRReference[] */
    protected array $informationSource = [];

    /** @var FHIRReference[] */
    protected array $derivedFrom = [];

    /** @var FHIRCodeableReference[] */
    protected array $reason = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRReference[] */
    protected array $relatedClinicalInformation = [];
    protected ?FHIRMarkdown $renderedDosageInstruction = null;

    /** @var FHIRDosage[] */
    protected array $dosage = [];
    protected ?FHIRMedicationStatementAdherence $adherence = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPartOf(): array
    {
        return $this->partOf;
    }

    public function setPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter($value);

        return $this;
    }

    public function addPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter(array_merge($this->partOf, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getMedication(): ?FHIRCodeableReference
    {
        return $this->medication;
    }

    public function setMedication(?FHIRCodeableReference $value): self
    {
        $this->medication = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getEffective(): FHIRDateTime|FHIRPeriod|FHIRTiming|null
    {
        return $this->effective;
    }

    public function setEffective(FHIRDateTime|FHIRPeriod|FHIRTiming|null $value): self
    {
        $this->effective = $value;

        return $this;
    }

    public function getDateAsserted(): ?FHIRDateTime
    {
        return $this->dateAsserted;
    }

    public function setDateAsserted(string|FHIRDateTime|null $value): self
    {
        $this->dateAsserted = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getInformationSource(): array
    {
        return $this->informationSource;
    }

    public function setInformationSource(?FHIRReference ...$value): self
    {
        $this->informationSource = array_filter($value);

        return $this;
    }

    public function addInformationSource(?FHIRReference ...$value): self
    {
        $this->informationSource = array_filter(array_merge($this->informationSource, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getDerivedFrom(): array
    {
        return $this->derivedFrom;
    }

    public function setDerivedFrom(?FHIRReference ...$value): self
    {
        $this->derivedFrom = array_filter($value);

        return $this;
    }

    public function addDerivedFrom(?FHIRReference ...$value): self
    {
        $this->derivedFrom = array_filter(array_merge($this->derivedFrom, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getReason(): array
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableReference ...$value): self
    {
        $this->reason = array_filter($value);

        return $this;
    }

    public function addReason(?FHIRCodeableReference ...$value): self
    {
        $this->reason = array_filter(array_merge($this->reason, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getRelatedClinicalInformation(): array
    {
        return $this->relatedClinicalInformation;
    }

    public function setRelatedClinicalInformation(?FHIRReference ...$value): self
    {
        $this->relatedClinicalInformation = array_filter($value);

        return $this;
    }

    public function addRelatedClinicalInformation(?FHIRReference ...$value): self
    {
        $this->relatedClinicalInformation = array_filter(array_merge($this->relatedClinicalInformation, $value));

        return $this;
    }

    public function getRenderedDosageInstruction(): ?FHIRMarkdown
    {
        return $this->renderedDosageInstruction;
    }

    public function setRenderedDosageInstruction(string|FHIRMarkdown|null $value): self
    {
        $this->renderedDosageInstruction = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRDosage[]
     */
    public function getDosage(): array
    {
        return $this->dosage;
    }

    public function setDosage(?FHIRDosage ...$value): self
    {
        $this->dosage = array_filter($value);

        return $this;
    }

    public function addDosage(?FHIRDosage ...$value): self
    {
        $this->dosage = array_filter(array_merge($this->dosage, $value));

        return $this;
    }

    public function getAdherence(): ?FHIRMedicationStatementAdherence
    {
        return $this->adherence;
    }

    public function setAdherence(?FHIRMedicationStatementAdherence $value): self
    {
        $this->adherence = $value;

        return $this;
    }
}
