<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ChargeItem Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRChargeItemInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRMonetaryComponent;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRChargeItemPerformer;

class FHIRChargeItem extends FHIRDomainResource implements FHIRChargeItemInterface
{
    public const RESOURCE_NAME = 'ChargeItem';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRUri[] */
    protected array $definitionUri = [];

    /** @var FHIRCanonical[] */
    protected array $definitionCanonical = [];
    protected ?FHIRCode $status = null;

    /** @var FHIRReference[] */
    protected array $partOf = [];
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $encounter = null;
    protected FHIRDateTime|FHIRPeriod|FHIRTiming|null $occurrence = null;

    /** @var FHIRChargeItemPerformer[] */
    protected array $performer = [];
    protected ?FHIRReference $performingOrganization = null;
    protected ?FHIRReference $requestingOrganization = null;
    protected ?FHIRReference $costCenter = null;
    protected ?FHIRQuantity $quantity = null;

    /** @var FHIRCodeableConcept[] */
    protected array $bodysite = [];
    protected ?FHIRMonetaryComponent $unitPriceComponent = null;
    protected ?FHIRMonetaryComponent $totalPriceComponent = null;
    protected ?FHIRCodeableConcept $overrideReason = null;
    protected ?FHIRReference $enterer = null;
    protected ?FHIRDateTime $enteredDate = null;

    /** @var FHIRCodeableConcept[] */
    protected array $reason = [];

    /** @var FHIRCodeableReference[] */
    protected array $service = [];

    /** @var FHIRCodeableReference[] */
    protected array $product = [];

    /** @var FHIRReference[] */
    protected array $account = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRReference[] */
    protected array $supportingInformation = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRUri[]
     */
    public function getDefinitionUri(): array
    {
        return $this->definitionUri;
    }

    public function setDefinitionUri(string|FHIRUri|null ...$value): self
    {
        $this->definitionUri = [];
        $this->addDefinitionUri(...$value);

        return $this;
    }

    public function addDefinitionUri(string|FHIRUri|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUri())->setValue($v) : $v, $value);

        $this->definitionUri = array_filter(array_merge($this->definitionUri, $values));

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getDefinitionCanonical(): array
    {
        return $this->definitionCanonical;
    }

    public function setDefinitionCanonical(string|FHIRCanonical|null ...$value): self
    {
        $this->definitionCanonical = [];
        $this->addDefinitionCanonical(...$value);

        return $this;
    }

    public function addDefinitionCanonical(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->definitionCanonical = array_filter(array_merge($this->definitionCanonical, $values));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPartOf(): array
    {
        return $this->partOf;
    }

    public function setPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter($value);

        return $this;
    }

    public function addPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter(array_merge($this->partOf, $value));

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getOccurrence(): FHIRDateTime|FHIRPeriod|FHIRTiming|null
    {
        return $this->occurrence;
    }

    public function setOccurrence(FHIRDateTime|FHIRPeriod|FHIRTiming|null $value): self
    {
        $this->occurrence = $value;

        return $this;
    }

    /**
     * @return FHIRChargeItemPerformer[]
     */
    public function getPerformer(): array
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRChargeItemPerformer ...$value): self
    {
        $this->performer = array_filter($value);

        return $this;
    }

    public function addPerformer(?FHIRChargeItemPerformer ...$value): self
    {
        $this->performer = array_filter(array_merge($this->performer, $value));

        return $this;
    }

    public function getPerformingOrganization(): ?FHIRReference
    {
        return $this->performingOrganization;
    }

    public function setPerformingOrganization(?FHIRReference $value): self
    {
        $this->performingOrganization = $value;

        return $this;
    }

    public function getRequestingOrganization(): ?FHIRReference
    {
        return $this->requestingOrganization;
    }

    public function setRequestingOrganization(?FHIRReference $value): self
    {
        $this->requestingOrganization = $value;

        return $this;
    }

    public function getCostCenter(): ?FHIRReference
    {
        return $this->costCenter;
    }

    public function setCostCenter(?FHIRReference $value): self
    {
        $this->costCenter = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getBodysite(): array
    {
        return $this->bodysite;
    }

    public function setBodysite(?FHIRCodeableConcept ...$value): self
    {
        $this->bodysite = array_filter($value);

        return $this;
    }

    public function addBodysite(?FHIRCodeableConcept ...$value): self
    {
        $this->bodysite = array_filter(array_merge($this->bodysite, $value));

        return $this;
    }

    public function getUnitPriceComponent(): ?FHIRMonetaryComponent
    {
        return $this->unitPriceComponent;
    }

    public function setUnitPriceComponent(?FHIRMonetaryComponent $value): self
    {
        $this->unitPriceComponent = $value;

        return $this;
    }

    public function getTotalPriceComponent(): ?FHIRMonetaryComponent
    {
        return $this->totalPriceComponent;
    }

    public function setTotalPriceComponent(?FHIRMonetaryComponent $value): self
    {
        $this->totalPriceComponent = $value;

        return $this;
    }

    public function getOverrideReason(): ?FHIRCodeableConcept
    {
        return $this->overrideReason;
    }

    public function setOverrideReason(?FHIRCodeableConcept $value): self
    {
        $this->overrideReason = $value;

        return $this;
    }

    public function getEnterer(): ?FHIRReference
    {
        return $this->enterer;
    }

    public function setEnterer(?FHIRReference $value): self
    {
        $this->enterer = $value;

        return $this;
    }

    public function getEnteredDate(): ?FHIRDateTime
    {
        return $this->enteredDate;
    }

    public function setEnteredDate(string|FHIRDateTime|null $value): self
    {
        $this->enteredDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReason(): array
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableConcept ...$value): self
    {
        $this->reason = array_filter($value);

        return $this;
    }

    public function addReason(?FHIRCodeableConcept ...$value): self
    {
        $this->reason = array_filter(array_merge($this->reason, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getService(): array
    {
        return $this->service;
    }

    public function setService(?FHIRCodeableReference ...$value): self
    {
        $this->service = array_filter($value);

        return $this;
    }

    public function addService(?FHIRCodeableReference ...$value): self
    {
        $this->service = array_filter(array_merge($this->service, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getProduct(): array
    {
        return $this->product;
    }

    public function setProduct(?FHIRCodeableReference ...$value): self
    {
        $this->product = array_filter($value);

        return $this;
    }

    public function addProduct(?FHIRCodeableReference ...$value): self
    {
        $this->product = array_filter(array_merge($this->product, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAccount(): array
    {
        return $this->account;
    }

    public function setAccount(?FHIRReference ...$value): self
    {
        $this->account = array_filter($value);

        return $this;
    }

    public function addAccount(?FHIRReference ...$value): self
    {
        $this->account = array_filter(array_merge($this->account, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupportingInformation(): array
    {
        return $this->supportingInformation;
    }

    public function setSupportingInformation(?FHIRReference ...$value): self
    {
        $this->supportingInformation = array_filter($value);

        return $this;
    }

    public function addSupportingInformation(?FHIRReference ...$value): self
    {
        $this->supportingInformation = array_filter(array_merge($this->supportingInformation, $value));

        return $this;
    }
}
