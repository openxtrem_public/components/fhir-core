<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ObservationDefinition Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRObservationDefinitionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRObservationDefinitionComponent;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRObservationDefinitionQualifiedValue;

class FHIRObservationDefinition extends FHIRDomainResource implements FHIRObservationDefinitionInterface
{
    public const RESOURCE_NAME = 'ObservationDefinition';

    protected ?FHIRUri $url = null;
    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRString $version = null;
    protected FHIRString|FHIRCoding|null $versionAlgorithm = null;
    protected ?FHIRString $name = null;
    protected ?FHIRString $title = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRBoolean $experimental = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRString $publisher = null;

    /** @var FHIRContactDetail[] */
    protected array $contact = [];
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRUsageContext[] */
    protected array $useContext = [];

    /** @var FHIRCodeableConcept[] */
    protected array $jurisdiction = [];
    protected ?FHIRMarkdown $purpose = null;
    protected ?FHIRMarkdown $copyright = null;
    protected ?FHIRString $copyrightLabel = null;
    protected ?FHIRDate $approvalDate = null;
    protected ?FHIRDate $lastReviewDate = null;
    protected ?FHIRPeriod $effectivePeriod = null;

    /** @var FHIRCanonical[] */
    protected array $derivedFromCanonical = [];

    /** @var FHIRUri[] */
    protected array $derivedFromUri = [];

    /** @var FHIRCodeableConcept[] */
    protected array $subject = [];
    protected ?FHIRCodeableConcept $performerType = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRCodeableConcept $code = null;

    /** @var FHIRCode[] */
    protected array $permittedDataType = [];
    protected ?FHIRBoolean $multipleResultsAllowed = null;
    protected ?FHIRCodeableConcept $bodySite = null;
    protected ?FHIRCodeableConcept $method = null;

    /** @var FHIRReference[] */
    protected array $specimen = [];

    /** @var FHIRReference[] */
    protected array $device = [];
    protected ?FHIRString $preferredReportName = null;

    /** @var FHIRCoding[] */
    protected array $permittedUnit = [];

    /** @var FHIRObservationDefinitionQualifiedValue[] */
    protected array $qualifiedValue = [];

    /** @var FHIRReference[] */
    protected array $hasMember = [];

    /** @var FHIRObservationDefinitionComponent[] */
    protected array $component = [];

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getVersionAlgorithm(): FHIRString|FHIRCoding|null
    {
        return $this->versionAlgorithm;
    }

    public function setVersionAlgorithm(FHIRString|FHIRCoding|null $value): self
    {
        $this->versionAlgorithm = $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getExperimental(): ?FHIRBoolean
    {
        return $this->experimental;
    }

    public function setExperimental(bool|FHIRBoolean|null $value): self
    {
        $this->experimental = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPublisher(): ?FHIRString
    {
        return $this->publisher;
    }

    public function setPublisher(string|FHIRString|null $value): self
    {
        $this->publisher = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUseContext(): array
    {
        return $this->useContext;
    }

    public function setUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter($value);

        return $this;
    }

    public function addUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter(array_merge($this->useContext, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getJurisdiction(): array
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter($value);

        return $this;
    }

    public function addJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter(array_merge($this->jurisdiction, $value));

        return $this;
    }

    public function getPurpose(): ?FHIRMarkdown
    {
        return $this->purpose;
    }

    public function setPurpose(string|FHIRMarkdown|null $value): self
    {
        $this->purpose = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getCopyright(): ?FHIRMarkdown
    {
        return $this->copyright;
    }

    public function setCopyright(string|FHIRMarkdown|null $value): self
    {
        $this->copyright = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getCopyrightLabel(): ?FHIRString
    {
        return $this->copyrightLabel;
    }

    public function setCopyrightLabel(string|FHIRString|null $value): self
    {
        $this->copyrightLabel = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getApprovalDate(): ?FHIRDate
    {
        return $this->approvalDate;
    }

    public function setApprovalDate(string|FHIRDate|null $value): self
    {
        $this->approvalDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getLastReviewDate(): ?FHIRDate
    {
        return $this->lastReviewDate;
    }

    public function setLastReviewDate(string|FHIRDate|null $value): self
    {
        $this->lastReviewDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getEffectivePeriod(): ?FHIRPeriod
    {
        return $this->effectivePeriod;
    }

    public function setEffectivePeriod(?FHIRPeriod $value): self
    {
        $this->effectivePeriod = $value;

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getDerivedFromCanonical(): array
    {
        return $this->derivedFromCanonical;
    }

    public function setDerivedFromCanonical(string|FHIRCanonical|null ...$value): self
    {
        $this->derivedFromCanonical = [];
        $this->addDerivedFromCanonical(...$value);

        return $this;
    }

    public function addDerivedFromCanonical(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->derivedFromCanonical = array_filter(array_merge($this->derivedFromCanonical, $values));

        return $this;
    }

    /**
     * @return FHIRUri[]
     */
    public function getDerivedFromUri(): array
    {
        return $this->derivedFromUri;
    }

    public function setDerivedFromUri(string|FHIRUri|null ...$value): self
    {
        $this->derivedFromUri = [];
        $this->addDerivedFromUri(...$value);

        return $this;
    }

    public function addDerivedFromUri(string|FHIRUri|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUri())->setValue($v) : $v, $value);

        $this->derivedFromUri = array_filter(array_merge($this->derivedFromUri, $values));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSubject(): array
    {
        return $this->subject;
    }

    public function setSubject(?FHIRCodeableConcept ...$value): self
    {
        $this->subject = array_filter($value);

        return $this;
    }

    public function addSubject(?FHIRCodeableConcept ...$value): self
    {
        $this->subject = array_filter(array_merge($this->subject, $value));

        return $this;
    }

    public function getPerformerType(): ?FHIRCodeableConcept
    {
        return $this->performerType;
    }

    public function setPerformerType(?FHIRCodeableConcept $value): self
    {
        $this->performerType = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getPermittedDataType(): array
    {
        return $this->permittedDataType;
    }

    public function setPermittedDataType(string|FHIRCode|null ...$value): self
    {
        $this->permittedDataType = [];
        $this->addPermittedDataType(...$value);

        return $this;
    }

    public function addPermittedDataType(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->permittedDataType = array_filter(array_merge($this->permittedDataType, $values));

        return $this;
    }

    public function getMultipleResultsAllowed(): ?FHIRBoolean
    {
        return $this->multipleResultsAllowed;
    }

    public function setMultipleResultsAllowed(bool|FHIRBoolean|null $value): self
    {
        $this->multipleResultsAllowed = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getBodySite(): ?FHIRCodeableConcept
    {
        return $this->bodySite;
    }

    public function setBodySite(?FHIRCodeableConcept $value): self
    {
        $this->bodySite = $value;

        return $this;
    }

    public function getMethod(): ?FHIRCodeableConcept
    {
        return $this->method;
    }

    public function setMethod(?FHIRCodeableConcept $value): self
    {
        $this->method = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSpecimen(): array
    {
        return $this->specimen;
    }

    public function setSpecimen(?FHIRReference ...$value): self
    {
        $this->specimen = array_filter($value);

        return $this;
    }

    public function addSpecimen(?FHIRReference ...$value): self
    {
        $this->specimen = array_filter(array_merge($this->specimen, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getDevice(): array
    {
        return $this->device;
    }

    public function setDevice(?FHIRReference ...$value): self
    {
        $this->device = array_filter($value);

        return $this;
    }

    public function addDevice(?FHIRReference ...$value): self
    {
        $this->device = array_filter(array_merge($this->device, $value));

        return $this;
    }

    public function getPreferredReportName(): ?FHIRString
    {
        return $this->preferredReportName;
    }

    public function setPreferredReportName(string|FHIRString|null $value): self
    {
        $this->preferredReportName = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getPermittedUnit(): array
    {
        return $this->permittedUnit;
    }

    public function setPermittedUnit(?FHIRCoding ...$value): self
    {
        $this->permittedUnit = array_filter($value);

        return $this;
    }

    public function addPermittedUnit(?FHIRCoding ...$value): self
    {
        $this->permittedUnit = array_filter(array_merge($this->permittedUnit, $value));

        return $this;
    }

    /**
     * @return FHIRObservationDefinitionQualifiedValue[]
     */
    public function getQualifiedValue(): array
    {
        return $this->qualifiedValue;
    }

    public function setQualifiedValue(?FHIRObservationDefinitionQualifiedValue ...$value): self
    {
        $this->qualifiedValue = array_filter($value);

        return $this;
    }

    public function addQualifiedValue(?FHIRObservationDefinitionQualifiedValue ...$value): self
    {
        $this->qualifiedValue = array_filter(array_merge($this->qualifiedValue, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getHasMember(): array
    {
        return $this->hasMember;
    }

    public function setHasMember(?FHIRReference ...$value): self
    {
        $this->hasMember = array_filter($value);

        return $this;
    }

    public function addHasMember(?FHIRReference ...$value): self
    {
        $this->hasMember = array_filter(array_merge($this->hasMember, $value));

        return $this;
    }

    /**
     * @return FHIRObservationDefinitionComponent[]
     */
    public function getComponent(): array
    {
        return $this->component;
    }

    public function setComponent(?FHIRObservationDefinitionComponent ...$value): self
    {
        $this->component = array_filter($value);

        return $this;
    }

    public function addComponent(?FHIRObservationDefinitionComponent ...$value): self
    {
        $this->component = array_filter(array_merge($this->component, $value));

        return $this;
    }
}
