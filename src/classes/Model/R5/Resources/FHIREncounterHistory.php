<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EncounterHistory Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIREncounterHistoryInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIREncounterHistoryLocation;

class FHIREncounterHistory extends FHIRDomainResource implements FHIREncounterHistoryInterface
{
    public const RESOURCE_NAME = 'EncounterHistory';

    protected ?FHIRReference $encounter = null;

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $class = null;

    /** @var FHIRCodeableConcept[] */
    protected array $type = [];

    /** @var FHIRCodeableReference[] */
    protected array $serviceType = [];
    protected ?FHIRReference $subject = null;
    protected ?FHIRCodeableConcept $subjectStatus = null;
    protected ?FHIRPeriod $actualPeriod = null;
    protected ?FHIRDateTime $plannedStartDate = null;
    protected ?FHIRDateTime $plannedEndDate = null;
    protected ?FHIRDuration $length = null;

    /** @var FHIREncounterHistoryLocation[] */
    protected array $location = [];

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getClass(): ?FHIRCodeableConcept
    {
        return $this->class;
    }

    public function setClass(?FHIRCodeableConcept $value): self
    {
        $this->class = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getServiceType(): array
    {
        return $this->serviceType;
    }

    public function setServiceType(?FHIRCodeableReference ...$value): self
    {
        $this->serviceType = array_filter($value);

        return $this;
    }

    public function addServiceType(?FHIRCodeableReference ...$value): self
    {
        $this->serviceType = array_filter(array_merge($this->serviceType, $value));

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getSubjectStatus(): ?FHIRCodeableConcept
    {
        return $this->subjectStatus;
    }

    public function setSubjectStatus(?FHIRCodeableConcept $value): self
    {
        $this->subjectStatus = $value;

        return $this;
    }

    public function getActualPeriod(): ?FHIRPeriod
    {
        return $this->actualPeriod;
    }

    public function setActualPeriod(?FHIRPeriod $value): self
    {
        $this->actualPeriod = $value;

        return $this;
    }

    public function getPlannedStartDate(): ?FHIRDateTime
    {
        return $this->plannedStartDate;
    }

    public function setPlannedStartDate(string|FHIRDateTime|null $value): self
    {
        $this->plannedStartDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPlannedEndDate(): ?FHIRDateTime
    {
        return $this->plannedEndDate;
    }

    public function setPlannedEndDate(string|FHIRDateTime|null $value): self
    {
        $this->plannedEndDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getLength(): ?FHIRDuration
    {
        return $this->length;
    }

    public function setLength(?FHIRDuration $value): self
    {
        $this->length = $value;

        return $this;
    }

    /**
     * @return FHIREncounterHistoryLocation[]
     */
    public function getLocation(): array
    {
        return $this->location;
    }

    public function setLocation(?FHIREncounterHistoryLocation ...$value): self
    {
        $this->location = array_filter($value);

        return $this;
    }

    public function addLocation(?FHIREncounterHistoryLocation ...$value): self
    {
        $this->location = array_filter(array_merge($this->location, $value));

        return $this;
    }
}
