<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Observation Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRObservationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRObservationComponent;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRObservationReferenceRange;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRObservationTriggeredBy;

class FHIRObservation extends FHIRDomainResource implements FHIRObservationInterface
{
    public const RESOURCE_NAME = 'Observation';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected FHIRCanonical|FHIRReference|null $instantiates = null;

    /** @var FHIRReference[] */
    protected array $basedOn = [];

    /** @var FHIRObservationTriggeredBy[] */
    protected array $triggeredBy = [];

    /** @var FHIRReference[] */
    protected array $partOf = [];
    protected ?FHIRCode $status = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRReference $subject = null;

    /** @var FHIRReference[] */
    protected array $focus = [];
    protected ?FHIRReference $encounter = null;
    protected FHIRDateTime|FHIRPeriod|FHIRTiming|FHIRInstant|null $effective = null;
    protected ?FHIRInstant $issued = null;

    /** @var FHIRReference[] */
    protected array $performer = [];
    protected ?FHIRElement $value = null;
    protected ?FHIRCodeableConcept $dataAbsentReason = null;

    /** @var FHIRCodeableConcept[] */
    protected array $interpretation = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];
    protected ?FHIRCodeableConcept $bodySite = null;
    protected ?FHIRReference $bodyStructure = null;
    protected ?FHIRCodeableConcept $method = null;
    protected ?FHIRReference $specimen = null;
    protected ?FHIRReference $device = null;

    /** @var FHIRObservationReferenceRange[] */
    protected array $referenceRange = [];

    /** @var FHIRReference[] */
    protected array $hasMember = [];

    /** @var FHIRReference[] */
    protected array $derivedFrom = [];

    /** @var FHIRObservationComponent[] */
    protected array $component = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getInstantiates(): FHIRCanonical|FHIRReference|null
    {
        return $this->instantiates;
    }

    public function setInstantiates(FHIRCanonical|FHIRReference|null $value): self
    {
        $this->instantiates = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    /**
     * @return FHIRObservationTriggeredBy[]
     */
    public function getTriggeredBy(): array
    {
        return $this->triggeredBy;
    }

    public function setTriggeredBy(?FHIRObservationTriggeredBy ...$value): self
    {
        $this->triggeredBy = array_filter($value);

        return $this;
    }

    public function addTriggeredBy(?FHIRObservationTriggeredBy ...$value): self
    {
        $this->triggeredBy = array_filter(array_merge($this->triggeredBy, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPartOf(): array
    {
        return $this->partOf;
    }

    public function setPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter($value);

        return $this;
    }

    public function addPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter(array_merge($this->partOf, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getFocus(): array
    {
        return $this->focus;
    }

    public function setFocus(?FHIRReference ...$value): self
    {
        $this->focus = array_filter($value);

        return $this;
    }

    public function addFocus(?FHIRReference ...$value): self
    {
        $this->focus = array_filter(array_merge($this->focus, $value));

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getEffective(): FHIRDateTime|FHIRPeriod|FHIRTiming|FHIRInstant|null
    {
        return $this->effective;
    }

    public function setEffective(FHIRDateTime|FHIRPeriod|FHIRTiming|FHIRInstant|null $value): self
    {
        $this->effective = $value;

        return $this;
    }

    public function getIssued(): ?FHIRInstant
    {
        return $this->issued;
    }

    public function setIssued(string|FHIRInstant|null $value): self
    {
        $this->issued = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPerformer(): array
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRReference ...$value): self
    {
        $this->performer = array_filter($value);

        return $this;
    }

    public function addPerformer(?FHIRReference ...$value): self
    {
        $this->performer = array_filter(array_merge($this->performer, $value));

        return $this;
    }

    public function getValue(): ?FHIRElement
    {
        return $this->value;
    }

    public function setValue(?FHIRElement $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getDataAbsentReason(): ?FHIRCodeableConcept
    {
        return $this->dataAbsentReason;
    }

    public function setDataAbsentReason(?FHIRCodeableConcept $value): self
    {
        $this->dataAbsentReason = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getInterpretation(): array
    {
        return $this->interpretation;
    }

    public function setInterpretation(?FHIRCodeableConcept ...$value): self
    {
        $this->interpretation = array_filter($value);

        return $this;
    }

    public function addInterpretation(?FHIRCodeableConcept ...$value): self
    {
        $this->interpretation = array_filter(array_merge($this->interpretation, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    public function getBodySite(): ?FHIRCodeableConcept
    {
        return $this->bodySite;
    }

    public function setBodySite(?FHIRCodeableConcept $value): self
    {
        $this->bodySite = $value;

        return $this;
    }

    public function getBodyStructure(): ?FHIRReference
    {
        return $this->bodyStructure;
    }

    public function setBodyStructure(?FHIRReference $value): self
    {
        $this->bodyStructure = $value;

        return $this;
    }

    public function getMethod(): ?FHIRCodeableConcept
    {
        return $this->method;
    }

    public function setMethod(?FHIRCodeableConcept $value): self
    {
        $this->method = $value;

        return $this;
    }

    public function getSpecimen(): ?FHIRReference
    {
        return $this->specimen;
    }

    public function setSpecimen(?FHIRReference $value): self
    {
        $this->specimen = $value;

        return $this;
    }

    public function getDevice(): ?FHIRReference
    {
        return $this->device;
    }

    public function setDevice(?FHIRReference $value): self
    {
        $this->device = $value;

        return $this;
    }

    /**
     * @return FHIRObservationReferenceRange[]
     */
    public function getReferenceRange(): array
    {
        return $this->referenceRange;
    }

    public function setReferenceRange(?FHIRObservationReferenceRange ...$value): self
    {
        $this->referenceRange = array_filter($value);

        return $this;
    }

    public function addReferenceRange(?FHIRObservationReferenceRange ...$value): self
    {
        $this->referenceRange = array_filter(array_merge($this->referenceRange, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getHasMember(): array
    {
        return $this->hasMember;
    }

    public function setHasMember(?FHIRReference ...$value): self
    {
        $this->hasMember = array_filter($value);

        return $this;
    }

    public function addHasMember(?FHIRReference ...$value): self
    {
        $this->hasMember = array_filter(array_merge($this->hasMember, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getDerivedFrom(): array
    {
        return $this->derivedFrom;
    }

    public function setDerivedFrom(?FHIRReference ...$value): self
    {
        $this->derivedFrom = array_filter($value);

        return $this;
    }

    public function addDerivedFrom(?FHIRReference ...$value): self
    {
        $this->derivedFrom = array_filter(array_merge($this->derivedFrom, $value));

        return $this;
    }

    /**
     * @return FHIRObservationComponent[]
     */
    public function getComponent(): array
    {
        return $this->component;
    }

    public function setComponent(?FHIRObservationComponent ...$value): self
    {
        $this->component = array_filter($value);

        return $this;
    }

    public function addComponent(?FHIRObservationComponent ...$value): self
    {
        $this->component = array_filter(array_merge($this->component, $value));

        return $this;
    }
}
