<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EvidenceReport Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIREvidenceReportInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRelatedArtifact;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIREvidenceReportRelatesTo;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIREvidenceReportSection;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIREvidenceReportSubject;

class FHIREvidenceReport extends FHIRDomainResource implements FHIREvidenceReportInterface
{
    public const RESOURCE_NAME = 'EvidenceReport';

    protected ?FHIRUri $url = null;
    protected ?FHIRCode $status = null;

    /** @var FHIRUsageContext[] */
    protected array $useContext = [];

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRIdentifier[] */
    protected array $relatedIdentifier = [];
    protected FHIRReference|FHIRMarkdown|null $citeAs = null;
    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRRelatedArtifact[] */
    protected array $relatedArtifact = [];
    protected ?FHIREvidenceReportSubject $subject = null;
    protected ?FHIRString $publisher = null;

    /** @var FHIRContactDetail[] */
    protected array $contact = [];

    /** @var FHIRContactDetail[] */
    protected array $author = [];

    /** @var FHIRContactDetail[] */
    protected array $editor = [];

    /** @var FHIRContactDetail[] */
    protected array $reviewer = [];

    /** @var FHIRContactDetail[] */
    protected array $endorser = [];

    /** @var FHIREvidenceReportRelatesTo[] */
    protected array $relatesTo = [];

    /** @var FHIREvidenceReportSection[] */
    protected array $section = [];

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUseContext(): array
    {
        return $this->useContext;
    }

    public function setUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter($value);

        return $this;
    }

    public function addUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter(array_merge($this->useContext, $value));

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getRelatedIdentifier(): array
    {
        return $this->relatedIdentifier;
    }

    public function setRelatedIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->relatedIdentifier = array_filter($value);

        return $this;
    }

    public function addRelatedIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->relatedIdentifier = array_filter(array_merge($this->relatedIdentifier, $value));

        return $this;
    }

    public function getCiteAs(): FHIRReference|FHIRMarkdown|null
    {
        return $this->citeAs;
    }

    public function setCiteAs(FHIRReference|FHIRMarkdown|null $value): self
    {
        $this->citeAs = $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRRelatedArtifact[]
     */
    public function getRelatedArtifact(): array
    {
        return $this->relatedArtifact;
    }

    public function setRelatedArtifact(?FHIRRelatedArtifact ...$value): self
    {
        $this->relatedArtifact = array_filter($value);

        return $this;
    }

    public function addRelatedArtifact(?FHIRRelatedArtifact ...$value): self
    {
        $this->relatedArtifact = array_filter(array_merge($this->relatedArtifact, $value));

        return $this;
    }

    public function getSubject(): ?FHIREvidenceReportSubject
    {
        return $this->subject;
    }

    public function setSubject(?FHIREvidenceReportSubject $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getPublisher(): ?FHIRString
    {
        return $this->publisher;
    }

    public function setPublisher(string|FHIRString|null $value): self
    {
        $this->publisher = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getAuthor(): array
    {
        return $this->author;
    }

    public function setAuthor(?FHIRContactDetail ...$value): self
    {
        $this->author = array_filter($value);

        return $this;
    }

    public function addAuthor(?FHIRContactDetail ...$value): self
    {
        $this->author = array_filter(array_merge($this->author, $value));

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getEditor(): array
    {
        return $this->editor;
    }

    public function setEditor(?FHIRContactDetail ...$value): self
    {
        $this->editor = array_filter($value);

        return $this;
    }

    public function addEditor(?FHIRContactDetail ...$value): self
    {
        $this->editor = array_filter(array_merge($this->editor, $value));

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getReviewer(): array
    {
        return $this->reviewer;
    }

    public function setReviewer(?FHIRContactDetail ...$value): self
    {
        $this->reviewer = array_filter($value);

        return $this;
    }

    public function addReviewer(?FHIRContactDetail ...$value): self
    {
        $this->reviewer = array_filter(array_merge($this->reviewer, $value));

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getEndorser(): array
    {
        return $this->endorser;
    }

    public function setEndorser(?FHIRContactDetail ...$value): self
    {
        $this->endorser = array_filter($value);

        return $this;
    }

    public function addEndorser(?FHIRContactDetail ...$value): self
    {
        $this->endorser = array_filter(array_merge($this->endorser, $value));

        return $this;
    }

    /**
     * @return FHIREvidenceReportRelatesTo[]
     */
    public function getRelatesTo(): array
    {
        return $this->relatesTo;
    }

    public function setRelatesTo(?FHIREvidenceReportRelatesTo ...$value): self
    {
        $this->relatesTo = array_filter($value);

        return $this;
    }

    public function addRelatesTo(?FHIREvidenceReportRelatesTo ...$value): self
    {
        $this->relatesTo = array_filter(array_merge($this->relatesTo, $value));

        return $this;
    }

    /**
     * @return FHIREvidenceReportSection[]
     */
    public function getSection(): array
    {
        return $this->section;
    }

    public function setSection(?FHIREvidenceReportSection ...$value): self
    {
        $this->section = array_filter($value);

        return $this;
    }

    public function addSection(?FHIREvidenceReportSection ...$value): self
    {
        $this->section = array_filter(array_merge($this->section, $value));

        return $this;
    }
}
