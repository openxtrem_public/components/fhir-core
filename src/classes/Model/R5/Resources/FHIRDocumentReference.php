<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DocumentReference Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRDocumentReferenceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDocumentReferenceAttester;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDocumentReferenceContent;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDocumentReferenceRelatesTo;

class FHIRDocumentReference extends FHIRDomainResource implements FHIRDocumentReferenceInterface
{
    public const RESOURCE_NAME = 'DocumentReference';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRString $version = null;

    /** @var FHIRReference[] */
    protected array $basedOn = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCode $docStatus = null;

    /** @var FHIRCodeableConcept[] */
    protected array $modality = [];
    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRReference $subject = null;

    /** @var FHIRReference[] */
    protected array $context = [];

    /** @var FHIRCodeableReference[] */
    protected array $event = [];

    /** @var FHIRCodeableReference[] */
    protected array $bodySite = [];
    protected ?FHIRCodeableConcept $facilityType = null;
    protected ?FHIRCodeableConcept $practiceSetting = null;
    protected ?FHIRPeriod $period = null;
    protected ?FHIRInstant $date = null;

    /** @var FHIRReference[] */
    protected array $author = [];

    /** @var FHIRDocumentReferenceAttester[] */
    protected array $attester = [];
    protected ?FHIRReference $custodian = null;

    /** @var FHIRDocumentReferenceRelatesTo[] */
    protected array $relatesTo = [];
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRCodeableConcept[] */
    protected array $securityLabel = [];

    /** @var FHIRDocumentReferenceContent[] */
    protected array $content = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDocStatus(): ?FHIRCode
    {
        return $this->docStatus;
    }

    public function setDocStatus(string|FHIRCode|null $value): self
    {
        $this->docStatus = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getModality(): array
    {
        return $this->modality;
    }

    public function setModality(?FHIRCodeableConcept ...$value): self
    {
        $this->modality = array_filter($value);

        return $this;
    }

    public function addModality(?FHIRCodeableConcept ...$value): self
    {
        $this->modality = array_filter(array_merge($this->modality, $value));

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getContext(): array
    {
        return $this->context;
    }

    public function setContext(?FHIRReference ...$value): self
    {
        $this->context = array_filter($value);

        return $this;
    }

    public function addContext(?FHIRReference ...$value): self
    {
        $this->context = array_filter(array_merge($this->context, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getEvent(): array
    {
        return $this->event;
    }

    public function setEvent(?FHIRCodeableReference ...$value): self
    {
        $this->event = array_filter($value);

        return $this;
    }

    public function addEvent(?FHIRCodeableReference ...$value): self
    {
        $this->event = array_filter(array_merge($this->event, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getBodySite(): array
    {
        return $this->bodySite;
    }

    public function setBodySite(?FHIRCodeableReference ...$value): self
    {
        $this->bodySite = array_filter($value);

        return $this;
    }

    public function addBodySite(?FHIRCodeableReference ...$value): self
    {
        $this->bodySite = array_filter(array_merge($this->bodySite, $value));

        return $this;
    }

    public function getFacilityType(): ?FHIRCodeableConcept
    {
        return $this->facilityType;
    }

    public function setFacilityType(?FHIRCodeableConcept $value): self
    {
        $this->facilityType = $value;

        return $this;
    }

    public function getPracticeSetting(): ?FHIRCodeableConcept
    {
        return $this->practiceSetting;
    }

    public function setPracticeSetting(?FHIRCodeableConcept $value): self
    {
        $this->practiceSetting = $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    public function getDate(): ?FHIRInstant
    {
        return $this->date;
    }

    public function setDate(string|FHIRInstant|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAuthor(): array
    {
        return $this->author;
    }

    public function setAuthor(?FHIRReference ...$value): self
    {
        $this->author = array_filter($value);

        return $this;
    }

    public function addAuthor(?FHIRReference ...$value): self
    {
        $this->author = array_filter(array_merge($this->author, $value));

        return $this;
    }

    /**
     * @return FHIRDocumentReferenceAttester[]
     */
    public function getAttester(): array
    {
        return $this->attester;
    }

    public function setAttester(?FHIRDocumentReferenceAttester ...$value): self
    {
        $this->attester = array_filter($value);

        return $this;
    }

    public function addAttester(?FHIRDocumentReferenceAttester ...$value): self
    {
        $this->attester = array_filter(array_merge($this->attester, $value));

        return $this;
    }

    public function getCustodian(): ?FHIRReference
    {
        return $this->custodian;
    }

    public function setCustodian(?FHIRReference $value): self
    {
        $this->custodian = $value;

        return $this;
    }

    /**
     * @return FHIRDocumentReferenceRelatesTo[]
     */
    public function getRelatesTo(): array
    {
        return $this->relatesTo;
    }

    public function setRelatesTo(?FHIRDocumentReferenceRelatesTo ...$value): self
    {
        $this->relatesTo = array_filter($value);

        return $this;
    }

    public function addRelatesTo(?FHIRDocumentReferenceRelatesTo ...$value): self
    {
        $this->relatesTo = array_filter(array_merge($this->relatesTo, $value));

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSecurityLabel(): array
    {
        return $this->securityLabel;
    }

    public function setSecurityLabel(?FHIRCodeableConcept ...$value): self
    {
        $this->securityLabel = array_filter($value);

        return $this;
    }

    public function addSecurityLabel(?FHIRCodeableConcept ...$value): self
    {
        $this->securityLabel = array_filter(array_merge($this->securityLabel, $value));

        return $this;
    }

    /**
     * @return FHIRDocumentReferenceContent[]
     */
    public function getContent(): array
    {
        return $this->content;
    }

    public function setContent(?FHIRDocumentReferenceContent ...$value): self
    {
        $this->content = array_filter($value);

        return $this;
    }

    public function addContent(?FHIRDocumentReferenceContent ...$value): self
    {
        $this->content = array_filter(array_merge($this->content, $value));

        return $this;
    }
}
