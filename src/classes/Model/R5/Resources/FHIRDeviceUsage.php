<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceUsage Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRDeviceUsageInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDeviceUsageAdherence;

class FHIRDeviceUsage extends FHIRDomainResource implements FHIRDeviceUsageInterface
{
    public const RESOURCE_NAME = 'DeviceUsage';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];
    protected ?FHIRCode $status = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRReference $patient = null;

    /** @var FHIRReference[] */
    protected array $derivedFrom = [];
    protected ?FHIRReference $context = null;
    protected FHIRTiming|FHIRPeriod|FHIRDateTime|null $timing = null;
    protected ?FHIRDateTime $dateAsserted = null;
    protected ?FHIRCodeableConcept $usageStatus = null;

    /** @var FHIRCodeableConcept[] */
    protected array $usageReason = [];
    protected ?FHIRDeviceUsageAdherence $adherence = null;
    protected ?FHIRReference $informationSource = null;
    protected ?FHIRCodeableReference $device = null;

    /** @var FHIRCodeableReference[] */
    protected array $reason = [];
    protected ?FHIRCodeableReference $bodySite = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getDerivedFrom(): array
    {
        return $this->derivedFrom;
    }

    public function setDerivedFrom(?FHIRReference ...$value): self
    {
        $this->derivedFrom = array_filter($value);

        return $this;
    }

    public function addDerivedFrom(?FHIRReference ...$value): self
    {
        $this->derivedFrom = array_filter(array_merge($this->derivedFrom, $value));

        return $this;
    }

    public function getContext(): ?FHIRReference
    {
        return $this->context;
    }

    public function setContext(?FHIRReference $value): self
    {
        $this->context = $value;

        return $this;
    }

    public function getTiming(): FHIRTiming|FHIRPeriod|FHIRDateTime|null
    {
        return $this->timing;
    }

    public function setTiming(FHIRTiming|FHIRPeriod|FHIRDateTime|null $value): self
    {
        $this->timing = $value;

        return $this;
    }

    public function getDateAsserted(): ?FHIRDateTime
    {
        return $this->dateAsserted;
    }

    public function setDateAsserted(string|FHIRDateTime|null $value): self
    {
        $this->dateAsserted = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getUsageStatus(): ?FHIRCodeableConcept
    {
        return $this->usageStatus;
    }

    public function setUsageStatus(?FHIRCodeableConcept $value): self
    {
        $this->usageStatus = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getUsageReason(): array
    {
        return $this->usageReason;
    }

    public function setUsageReason(?FHIRCodeableConcept ...$value): self
    {
        $this->usageReason = array_filter($value);

        return $this;
    }

    public function addUsageReason(?FHIRCodeableConcept ...$value): self
    {
        $this->usageReason = array_filter(array_merge($this->usageReason, $value));

        return $this;
    }

    public function getAdherence(): ?FHIRDeviceUsageAdherence
    {
        return $this->adherence;
    }

    public function setAdherence(?FHIRDeviceUsageAdherence $value): self
    {
        $this->adherence = $value;

        return $this;
    }

    public function getInformationSource(): ?FHIRReference
    {
        return $this->informationSource;
    }

    public function setInformationSource(?FHIRReference $value): self
    {
        $this->informationSource = $value;

        return $this;
    }

    public function getDevice(): ?FHIRCodeableReference
    {
        return $this->device;
    }

    public function setDevice(?FHIRCodeableReference $value): self
    {
        $this->device = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getReason(): array
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableReference ...$value): self
    {
        $this->reason = array_filter($value);

        return $this;
    }

    public function addReason(?FHIRCodeableReference ...$value): self
    {
        $this->reason = array_filter(array_merge($this->reason, $value));

        return $this;
    }

    public function getBodySite(): ?FHIRCodeableReference
    {
        return $this->bodySite;
    }

    public function setBodySite(?FHIRCodeableReference $value): self
    {
        $this->bodySite = $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }
}
