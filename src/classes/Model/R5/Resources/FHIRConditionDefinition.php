<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ConditionDefinition Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRConditionDefinitionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRConditionDefinitionMedication;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRConditionDefinitionObservation;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRConditionDefinitionPlan;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRConditionDefinitionPrecondition;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRConditionDefinitionQuestionnaire;

class FHIRConditionDefinition extends FHIRDomainResource implements FHIRConditionDefinitionInterface
{
    public const RESOURCE_NAME = 'ConditionDefinition';

    protected ?FHIRUri $url = null;

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRString $version = null;
    protected FHIRString|FHIRCoding|null $versionAlgorithm = null;
    protected ?FHIRString $name = null;
    protected ?FHIRString $title = null;
    protected ?FHIRString $subtitle = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRBoolean $experimental = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRString $publisher = null;

    /** @var FHIRContactDetail[] */
    protected array $contact = [];
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRUsageContext[] */
    protected array $useContext = [];

    /** @var FHIRCodeableConcept[] */
    protected array $jurisdiction = [];
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRCodeableConcept $severity = null;
    protected ?FHIRCodeableConcept $bodySite = null;
    protected ?FHIRCodeableConcept $stage = null;
    protected ?FHIRBoolean $hasSeverity = null;
    protected ?FHIRBoolean $hasBodySite = null;
    protected ?FHIRBoolean $hasStage = null;

    /** @var FHIRUri[] */
    protected array $definition = [];

    /** @var FHIRConditionDefinitionObservation[] */
    protected array $observation = [];

    /** @var FHIRConditionDefinitionMedication[] */
    protected array $medication = [];

    /** @var FHIRConditionDefinitionPrecondition[] */
    protected array $precondition = [];

    /** @var FHIRReference[] */
    protected array $team = [];

    /** @var FHIRConditionDefinitionQuestionnaire[] */
    protected array $questionnaire = [];

    /** @var FHIRConditionDefinitionPlan[] */
    protected array $plan = [];

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getVersionAlgorithm(): FHIRString|FHIRCoding|null
    {
        return $this->versionAlgorithm;
    }

    public function setVersionAlgorithm(FHIRString|FHIRCoding|null $value): self
    {
        $this->versionAlgorithm = $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSubtitle(): ?FHIRString
    {
        return $this->subtitle;
    }

    public function setSubtitle(string|FHIRString|null $value): self
    {
        $this->subtitle = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getExperimental(): ?FHIRBoolean
    {
        return $this->experimental;
    }

    public function setExperimental(bool|FHIRBoolean|null $value): self
    {
        $this->experimental = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPublisher(): ?FHIRString
    {
        return $this->publisher;
    }

    public function setPublisher(string|FHIRString|null $value): self
    {
        $this->publisher = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUseContext(): array
    {
        return $this->useContext;
    }

    public function setUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter($value);

        return $this;
    }

    public function addUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter(array_merge($this->useContext, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getJurisdiction(): array
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter($value);

        return $this;
    }

    public function addJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter(array_merge($this->jurisdiction, $value));

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getSeverity(): ?FHIRCodeableConcept
    {
        return $this->severity;
    }

    public function setSeverity(?FHIRCodeableConcept $value): self
    {
        $this->severity = $value;

        return $this;
    }

    public function getBodySite(): ?FHIRCodeableConcept
    {
        return $this->bodySite;
    }

    public function setBodySite(?FHIRCodeableConcept $value): self
    {
        $this->bodySite = $value;

        return $this;
    }

    public function getStage(): ?FHIRCodeableConcept
    {
        return $this->stage;
    }

    public function setStage(?FHIRCodeableConcept $value): self
    {
        $this->stage = $value;

        return $this;
    }

    public function getHasSeverity(): ?FHIRBoolean
    {
        return $this->hasSeverity;
    }

    public function setHasSeverity(bool|FHIRBoolean|null $value): self
    {
        $this->hasSeverity = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getHasBodySite(): ?FHIRBoolean
    {
        return $this->hasBodySite;
    }

    public function setHasBodySite(bool|FHIRBoolean|null $value): self
    {
        $this->hasBodySite = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getHasStage(): ?FHIRBoolean
    {
        return $this->hasStage;
    }

    public function setHasStage(bool|FHIRBoolean|null $value): self
    {
        $this->hasStage = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRUri[]
     */
    public function getDefinition(): array
    {
        return $this->definition;
    }

    public function setDefinition(string|FHIRUri|null ...$value): self
    {
        $this->definition = [];
        $this->addDefinition(...$value);

        return $this;
    }

    public function addDefinition(string|FHIRUri|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUri())->setValue($v) : $v, $value);

        $this->definition = array_filter(array_merge($this->definition, $values));

        return $this;
    }

    /**
     * @return FHIRConditionDefinitionObservation[]
     */
    public function getObservation(): array
    {
        return $this->observation;
    }

    public function setObservation(?FHIRConditionDefinitionObservation ...$value): self
    {
        $this->observation = array_filter($value);

        return $this;
    }

    public function addObservation(?FHIRConditionDefinitionObservation ...$value): self
    {
        $this->observation = array_filter(array_merge($this->observation, $value));

        return $this;
    }

    /**
     * @return FHIRConditionDefinitionMedication[]
     */
    public function getMedication(): array
    {
        return $this->medication;
    }

    public function setMedication(?FHIRConditionDefinitionMedication ...$value): self
    {
        $this->medication = array_filter($value);

        return $this;
    }

    public function addMedication(?FHIRConditionDefinitionMedication ...$value): self
    {
        $this->medication = array_filter(array_merge($this->medication, $value));

        return $this;
    }

    /**
     * @return FHIRConditionDefinitionPrecondition[]
     */
    public function getPrecondition(): array
    {
        return $this->precondition;
    }

    public function setPrecondition(?FHIRConditionDefinitionPrecondition ...$value): self
    {
        $this->precondition = array_filter($value);

        return $this;
    }

    public function addPrecondition(?FHIRConditionDefinitionPrecondition ...$value): self
    {
        $this->precondition = array_filter(array_merge($this->precondition, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getTeam(): array
    {
        return $this->team;
    }

    public function setTeam(?FHIRReference ...$value): self
    {
        $this->team = array_filter($value);

        return $this;
    }

    public function addTeam(?FHIRReference ...$value): self
    {
        $this->team = array_filter(array_merge($this->team, $value));

        return $this;
    }

    /**
     * @return FHIRConditionDefinitionQuestionnaire[]
     */
    public function getQuestionnaire(): array
    {
        return $this->questionnaire;
    }

    public function setQuestionnaire(?FHIRConditionDefinitionQuestionnaire ...$value): self
    {
        $this->questionnaire = array_filter($value);

        return $this;
    }

    public function addQuestionnaire(?FHIRConditionDefinitionQuestionnaire ...$value): self
    {
        $this->questionnaire = array_filter(array_merge($this->questionnaire, $value));

        return $this;
    }

    /**
     * @return FHIRConditionDefinitionPlan[]
     */
    public function getPlan(): array
    {
        return $this->plan;
    }

    public function setPlan(?FHIRConditionDefinitionPlan ...$value): self
    {
        $this->plan = array_filter($value);

        return $this;
    }

    public function addPlan(?FHIRConditionDefinitionPlan ...$value): self
    {
        $this->plan = array_filter(array_merge($this->plan, $value));

        return $this;
    }
}
