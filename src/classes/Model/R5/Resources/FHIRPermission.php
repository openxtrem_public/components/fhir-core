<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Permission Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRPermissionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRPermissionJustification;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRPermissionRule;

class FHIRPermission extends FHIRDomainResource implements FHIRPermissionInterface
{
    public const RESOURCE_NAME = 'Permission';

    protected ?FHIRCode $status = null;
    protected ?FHIRReference $asserter = null;

    /** @var FHIRDateTime[] */
    protected array $date = [];
    protected ?FHIRPeriod $validity = null;
    protected ?FHIRPermissionJustification $justification = null;
    protected ?FHIRCode $combining = null;

    /** @var FHIRPermissionRule[] */
    protected array $rule = [];

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getAsserter(): ?FHIRReference
    {
        return $this->asserter;
    }

    public function setAsserter(?FHIRReference $value): self
    {
        $this->asserter = $value;

        return $this;
    }

    /**
     * @return FHIRDateTime[]
     */
    public function getDate(): array
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null ...$value): self
    {
        $this->date = [];
        $this->addDate(...$value);

        return $this;
    }

    public function addDate(string|FHIRDateTime|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRDateTime())->setValue($v) : $v, $value);

        $this->date = array_filter(array_merge($this->date, $values));

        return $this;
    }

    public function getValidity(): ?FHIRPeriod
    {
        return $this->validity;
    }

    public function setValidity(?FHIRPeriod $value): self
    {
        $this->validity = $value;

        return $this;
    }

    public function getJustification(): ?FHIRPermissionJustification
    {
        return $this->justification;
    }

    public function setJustification(?FHIRPermissionJustification $value): self
    {
        $this->justification = $value;

        return $this;
    }

    public function getCombining(): ?FHIRCode
    {
        return $this->combining;
    }

    public function setCombining(string|FHIRCode|null $value): self
    {
        $this->combining = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRPermissionRule[]
     */
    public function getRule(): array
    {
        return $this->rule;
    }

    public function setRule(?FHIRPermissionRule ...$value): self
    {
        $this->rule = array_filter($value);

        return $this;
    }

    public function addRule(?FHIRPermissionRule ...$value): self
    {
        $this->rule = array_filter(array_merge($this->rule, $value));

        return $this;
    }
}
