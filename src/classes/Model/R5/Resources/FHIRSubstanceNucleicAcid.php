<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceNucleicAcid Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRSubstanceNucleicAcidInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRSubstanceNucleicAcidSubunit;

class FHIRSubstanceNucleicAcid extends FHIRDomainResource implements FHIRSubstanceNucleicAcidInterface
{
    public const RESOURCE_NAME = 'SubstanceNucleicAcid';

    protected ?FHIRCodeableConcept $sequenceType = null;
    protected ?FHIRInteger $numberOfSubunits = null;
    protected ?FHIRString $areaOfHybridisation = null;
    protected ?FHIRCodeableConcept $oligoNucleotideType = null;

    /** @var FHIRSubstanceNucleicAcidSubunit[] */
    protected array $subunit = [];

    public function getSequenceType(): ?FHIRCodeableConcept
    {
        return $this->sequenceType;
    }

    public function setSequenceType(?FHIRCodeableConcept $value): self
    {
        $this->sequenceType = $value;

        return $this;
    }

    public function getNumberOfSubunits(): ?FHIRInteger
    {
        return $this->numberOfSubunits;
    }

    public function setNumberOfSubunits(int|FHIRInteger|null $value): self
    {
        $this->numberOfSubunits = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getAreaOfHybridisation(): ?FHIRString
    {
        return $this->areaOfHybridisation;
    }

    public function setAreaOfHybridisation(string|FHIRString|null $value): self
    {
        $this->areaOfHybridisation = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getOligoNucleotideType(): ?FHIRCodeableConcept
    {
        return $this->oligoNucleotideType;
    }

    public function setOligoNucleotideType(?FHIRCodeableConcept $value): self
    {
        $this->oligoNucleotideType = $value;

        return $this;
    }

    /**
     * @return FHIRSubstanceNucleicAcidSubunit[]
     */
    public function getSubunit(): array
    {
        return $this->subunit;
    }

    public function setSubunit(?FHIRSubstanceNucleicAcidSubunit ...$value): self
    {
        $this->subunit = array_filter($value);

        return $this;
    }

    public function addSubunit(?FHIRSubstanceNucleicAcidSubunit ...$value): self
    {
        $this->subunit = array_filter(array_merge($this->subunit, $value));

        return $this;
    }
}
