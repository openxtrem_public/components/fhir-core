<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Device Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRDeviceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCount;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDeviceConformsTo;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDeviceName;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDeviceProperty;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDeviceUdiCarrier;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDeviceVersion;

class FHIRDevice extends FHIRDomainResource implements FHIRDeviceInterface
{
    public const RESOURCE_NAME = 'Device';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRString $displayName = null;
    protected ?FHIRCodeableReference $definition = null;

    /** @var FHIRDeviceUdiCarrier[] */
    protected array $udiCarrier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $availabilityStatus = null;
    protected ?FHIRIdentifier $biologicalSourceEvent = null;
    protected ?FHIRString $manufacturer = null;
    protected ?FHIRDateTime $manufactureDate = null;
    protected ?FHIRDateTime $expirationDate = null;
    protected ?FHIRString $lotNumber = null;
    protected ?FHIRString $serialNumber = null;

    /** @var FHIRDeviceName[] */
    protected array $name = [];
    protected ?FHIRString $modelNumber = null;
    protected ?FHIRString $partNumber = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];

    /** @var FHIRCodeableConcept[] */
    protected array $type = [];

    /** @var FHIRDeviceVersion[] */
    protected array $version = [];

    /** @var FHIRDeviceConformsTo[] */
    protected array $conformsTo = [];

    /** @var FHIRDeviceProperty[] */
    protected array $property = [];
    protected ?FHIRCodeableConcept $mode = null;
    protected ?FHIRCount $cycle = null;
    protected ?FHIRDuration $duration = null;
    protected ?FHIRReference $owner = null;

    /** @var FHIRContactPoint[] */
    protected array $contact = [];
    protected ?FHIRReference $location = null;
    protected ?FHIRUri $url = null;

    /** @var FHIRReference[] */
    protected array $endpoint = [];

    /** @var FHIRCodeableReference[] */
    protected array $gateway = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRCodeableConcept[] */
    protected array $safety = [];
    protected ?FHIRReference $parent = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getDisplayName(): ?FHIRString
    {
        return $this->displayName;
    }

    public function setDisplayName(string|FHIRString|null $value): self
    {
        $this->displayName = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDefinition(): ?FHIRCodeableReference
    {
        return $this->definition;
    }

    public function setDefinition(?FHIRCodeableReference $value): self
    {
        $this->definition = $value;

        return $this;
    }

    /**
     * @return FHIRDeviceUdiCarrier[]
     */
    public function getUdiCarrier(): array
    {
        return $this->udiCarrier;
    }

    public function setUdiCarrier(?FHIRDeviceUdiCarrier ...$value): self
    {
        $this->udiCarrier = array_filter($value);

        return $this;
    }

    public function addUdiCarrier(?FHIRDeviceUdiCarrier ...$value): self
    {
        $this->udiCarrier = array_filter(array_merge($this->udiCarrier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getAvailabilityStatus(): ?FHIRCodeableConcept
    {
        return $this->availabilityStatus;
    }

    public function setAvailabilityStatus(?FHIRCodeableConcept $value): self
    {
        $this->availabilityStatus = $value;

        return $this;
    }

    public function getBiologicalSourceEvent(): ?FHIRIdentifier
    {
        return $this->biologicalSourceEvent;
    }

    public function setBiologicalSourceEvent(?FHIRIdentifier $value): self
    {
        $this->biologicalSourceEvent = $value;

        return $this;
    }

    public function getManufacturer(): ?FHIRString
    {
        return $this->manufacturer;
    }

    public function setManufacturer(string|FHIRString|null $value): self
    {
        $this->manufacturer = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getManufactureDate(): ?FHIRDateTime
    {
        return $this->manufactureDate;
    }

    public function setManufactureDate(string|FHIRDateTime|null $value): self
    {
        $this->manufactureDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getExpirationDate(): ?FHIRDateTime
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(string|FHIRDateTime|null $value): self
    {
        $this->expirationDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getLotNumber(): ?FHIRString
    {
        return $this->lotNumber;
    }

    public function setLotNumber(string|FHIRString|null $value): self
    {
        $this->lotNumber = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSerialNumber(): ?FHIRString
    {
        return $this->serialNumber;
    }

    public function setSerialNumber(string|FHIRString|null $value): self
    {
        $this->serialNumber = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRDeviceName[]
     */
    public function getName(): array
    {
        return $this->name;
    }

    public function setName(?FHIRDeviceName ...$value): self
    {
        $this->name = array_filter($value);

        return $this;
    }

    public function addName(?FHIRDeviceName ...$value): self
    {
        $this->name = array_filter(array_merge($this->name, $value));

        return $this;
    }

    public function getModelNumber(): ?FHIRString
    {
        return $this->modelNumber;
    }

    public function setModelNumber(string|FHIRString|null $value): self
    {
        $this->modelNumber = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPartNumber(): ?FHIRString
    {
        return $this->partNumber;
    }

    public function setPartNumber(string|FHIRString|null $value): self
    {
        $this->partNumber = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }

    /**
     * @return FHIRDeviceVersion[]
     */
    public function getVersion(): array
    {
        return $this->version;
    }

    public function setVersion(?FHIRDeviceVersion ...$value): self
    {
        $this->version = array_filter($value);

        return $this;
    }

    public function addVersion(?FHIRDeviceVersion ...$value): self
    {
        $this->version = array_filter(array_merge($this->version, $value));

        return $this;
    }

    /**
     * @return FHIRDeviceConformsTo[]
     */
    public function getConformsTo(): array
    {
        return $this->conformsTo;
    }

    public function setConformsTo(?FHIRDeviceConformsTo ...$value): self
    {
        $this->conformsTo = array_filter($value);

        return $this;
    }

    public function addConformsTo(?FHIRDeviceConformsTo ...$value): self
    {
        $this->conformsTo = array_filter(array_merge($this->conformsTo, $value));

        return $this;
    }

    /**
     * @return FHIRDeviceProperty[]
     */
    public function getProperty(): array
    {
        return $this->property;
    }

    public function setProperty(?FHIRDeviceProperty ...$value): self
    {
        $this->property = array_filter($value);

        return $this;
    }

    public function addProperty(?FHIRDeviceProperty ...$value): self
    {
        $this->property = array_filter(array_merge($this->property, $value));

        return $this;
    }

    public function getMode(): ?FHIRCodeableConcept
    {
        return $this->mode;
    }

    public function setMode(?FHIRCodeableConcept $value): self
    {
        $this->mode = $value;

        return $this;
    }

    public function getCycle(): ?FHIRCount
    {
        return $this->cycle;
    }

    public function setCycle(?FHIRCount $value): self
    {
        $this->cycle = $value;

        return $this;
    }

    public function getDuration(): ?FHIRDuration
    {
        return $this->duration;
    }

    public function setDuration(?FHIRDuration $value): self
    {
        $this->duration = $value;

        return $this;
    }

    public function getOwner(): ?FHIRReference
    {
        return $this->owner;
    }

    public function setOwner(?FHIRReference $value): self
    {
        $this->owner = $value;

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactPoint ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactPoint ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEndpoint(): array
    {
        return $this->endpoint;
    }

    public function setEndpoint(?FHIRReference ...$value): self
    {
        $this->endpoint = array_filter($value);

        return $this;
    }

    public function addEndpoint(?FHIRReference ...$value): self
    {
        $this->endpoint = array_filter(array_merge($this->endpoint, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getGateway(): array
    {
        return $this->gateway;
    }

    public function setGateway(?FHIRCodeableReference ...$value): self
    {
        $this->gateway = array_filter($value);

        return $this;
    }

    public function addGateway(?FHIRCodeableReference ...$value): self
    {
        $this->gateway = array_filter(array_merge($this->gateway, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSafety(): array
    {
        return $this->safety;
    }

    public function setSafety(?FHIRCodeableConcept ...$value): self
    {
        $this->safety = array_filter($value);

        return $this;
    }

    public function addSafety(?FHIRCodeableConcept ...$value): self
    {
        $this->safety = array_filter(array_merge($this->safety, $value));

        return $this;
    }

    public function getParent(): ?FHIRReference
    {
        return $this->parent;
    }

    public function setParent(?FHIRReference $value): self
    {
        $this->parent = $value;

        return $this;
    }
}
