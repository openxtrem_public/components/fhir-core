<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MolecularSequence Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMolecularSequenceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMolecularSequenceRelative;

class FHIRMolecularSequence extends FHIRDomainResource implements FHIRMolecularSequenceInterface
{
    public const RESOURCE_NAME = 'MolecularSequence';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $type = null;
    protected ?FHIRReference $subject = null;

    /** @var FHIRReference[] */
    protected array $focus = [];
    protected ?FHIRReference $specimen = null;
    protected ?FHIRReference $device = null;
    protected ?FHIRReference $performer = null;
    protected ?FHIRString $literal = null;

    /** @var FHIRAttachment[] */
    protected array $formatted = [];

    /** @var FHIRMolecularSequenceRelative[] */
    protected array $relative = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getFocus(): array
    {
        return $this->focus;
    }

    public function setFocus(?FHIRReference ...$value): self
    {
        $this->focus = array_filter($value);

        return $this;
    }

    public function addFocus(?FHIRReference ...$value): self
    {
        $this->focus = array_filter(array_merge($this->focus, $value));

        return $this;
    }

    public function getSpecimen(): ?FHIRReference
    {
        return $this->specimen;
    }

    public function setSpecimen(?FHIRReference $value): self
    {
        $this->specimen = $value;

        return $this;
    }

    public function getDevice(): ?FHIRReference
    {
        return $this->device;
    }

    public function setDevice(?FHIRReference $value): self
    {
        $this->device = $value;

        return $this;
    }

    public function getPerformer(): ?FHIRReference
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRReference $value): self
    {
        $this->performer = $value;

        return $this;
    }

    public function getLiteral(): ?FHIRString
    {
        return $this->literal;
    }

    public function setLiteral(string|FHIRString|null $value): self
    {
        $this->literal = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAttachment[]
     */
    public function getFormatted(): array
    {
        return $this->formatted;
    }

    public function setFormatted(?FHIRAttachment ...$value): self
    {
        $this->formatted = array_filter($value);

        return $this;
    }

    public function addFormatted(?FHIRAttachment ...$value): self
    {
        $this->formatted = array_filter(array_merge($this->formatted, $value));

        return $this;
    }

    /**
     * @return FHIRMolecularSequenceRelative[]
     */
    public function getRelative(): array
    {
        return $this->relative;
    }

    public function setRelative(?FHIRMolecularSequenceRelative ...$value): self
    {
        $this->relative = array_filter($value);

        return $this;
    }

    public function addRelative(?FHIRMolecularSequenceRelative ...$value): self
    {
        $this->relative = array_filter(array_merge($this->relative, $value));

        return $this;
    }
}
