<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Endpoint Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIREndpointInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUrl;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIREndpointPayload;

class FHIREndpoint extends FHIRDomainResource implements FHIREndpointInterface
{
    public const RESOURCE_NAME = 'Endpoint';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;

    /** @var FHIRCodeableConcept[] */
    protected array $connectionType = [];
    protected ?FHIRString $name = null;
    protected ?FHIRString $description = null;

    /** @var FHIRCodeableConcept[] */
    protected array $environmentType = [];
    protected ?FHIRReference $managingOrganization = null;

    /** @var FHIRContactPoint[] */
    protected array $contact = [];
    protected ?FHIRPeriod $period = null;

    /** @var FHIREndpointPayload[] */
    protected array $payload = [];
    protected ?FHIRUrl $address = null;

    /** @var FHIRString[] */
    protected array $header = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getConnectionType(): array
    {
        return $this->connectionType;
    }

    public function setConnectionType(?FHIRCodeableConcept ...$value): self
    {
        $this->connectionType = array_filter($value);

        return $this;
    }

    public function addConnectionType(?FHIRCodeableConcept ...$value): self
    {
        $this->connectionType = array_filter(array_merge($this->connectionType, $value));

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getEnvironmentType(): array
    {
        return $this->environmentType;
    }

    public function setEnvironmentType(?FHIRCodeableConcept ...$value): self
    {
        $this->environmentType = array_filter($value);

        return $this;
    }

    public function addEnvironmentType(?FHIRCodeableConcept ...$value): self
    {
        $this->environmentType = array_filter(array_merge($this->environmentType, $value));

        return $this;
    }

    public function getManagingOrganization(): ?FHIRReference
    {
        return $this->managingOrganization;
    }

    public function setManagingOrganization(?FHIRReference $value): self
    {
        $this->managingOrganization = $value;

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactPoint ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactPoint ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    /**
     * @return FHIREndpointPayload[]
     */
    public function getPayload(): array
    {
        return $this->payload;
    }

    public function setPayload(?FHIREndpointPayload ...$value): self
    {
        $this->payload = array_filter($value);

        return $this;
    }

    public function addPayload(?FHIREndpointPayload ...$value): self
    {
        $this->payload = array_filter(array_merge($this->payload, $value));

        return $this;
    }

    public function getAddress(): ?FHIRUrl
    {
        return $this->address;
    }

    public function setAddress(string|FHIRUrl|null $value): self
    {
        $this->address = is_string($value) ? (new FHIRUrl())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getHeader(): array
    {
        return $this->header;
    }

    public function setHeader(string|FHIRString|null ...$value): self
    {
        $this->header = [];
        $this->addHeader(...$value);

        return $this;
    }

    public function addHeader(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->header = array_filter(array_merge($this->header, $values));

        return $this;
    }
}
