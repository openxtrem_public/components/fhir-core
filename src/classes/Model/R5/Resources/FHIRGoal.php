<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Goal Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRGoalInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRGoalTarget;

class FHIRGoal extends FHIRDomainResource implements FHIRGoalInterface
{
    public const RESOURCE_NAME = 'Goal';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $lifecycleStatus = null;
    protected ?FHIRCodeableConcept $achievementStatus = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRBoolean $continuous = null;
    protected ?FHIRCodeableConcept $priority = null;
    protected ?FHIRCodeableConcept $description = null;
    protected ?FHIRReference $subject = null;
    protected FHIRDate|FHIRCodeableConcept|null $start = null;

    /** @var FHIRGoalTarget[] */
    protected array $target = [];
    protected ?FHIRDate $statusDate = null;
    protected ?FHIRString $statusReason = null;
    protected ?FHIRReference $source = null;

    /** @var FHIRReference[] */
    protected array $addresses = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRCodeableReference[] */
    protected array $outcome = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getLifecycleStatus(): ?FHIRCode
    {
        return $this->lifecycleStatus;
    }

    public function setLifecycleStatus(string|FHIRCode|null $value): self
    {
        $this->lifecycleStatus = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getAchievementStatus(): ?FHIRCodeableConcept
    {
        return $this->achievementStatus;
    }

    public function setAchievementStatus(?FHIRCodeableConcept $value): self
    {
        $this->achievementStatus = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getContinuous(): ?FHIRBoolean
    {
        return $this->continuous;
    }

    public function setContinuous(bool|FHIRBoolean|null $value): self
    {
        $this->continuous = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getPriority(): ?FHIRCodeableConcept
    {
        return $this->priority;
    }

    public function setPriority(?FHIRCodeableConcept $value): self
    {
        $this->priority = $value;

        return $this;
    }

    public function getDescription(): ?FHIRCodeableConcept
    {
        return $this->description;
    }

    public function setDescription(?FHIRCodeableConcept $value): self
    {
        $this->description = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getStart(): FHIRDate|FHIRCodeableConcept|null
    {
        return $this->start;
    }

    public function setStart(FHIRDate|FHIRCodeableConcept|null $value): self
    {
        $this->start = $value;

        return $this;
    }

    /**
     * @return FHIRGoalTarget[]
     */
    public function getTarget(): array
    {
        return $this->target;
    }

    public function setTarget(?FHIRGoalTarget ...$value): self
    {
        $this->target = array_filter($value);

        return $this;
    }

    public function addTarget(?FHIRGoalTarget ...$value): self
    {
        $this->target = array_filter(array_merge($this->target, $value));

        return $this;
    }

    public function getStatusDate(): ?FHIRDate
    {
        return $this->statusDate;
    }

    public function setStatusDate(string|FHIRDate|null $value): self
    {
        $this->statusDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getStatusReason(): ?FHIRString
    {
        return $this->statusReason;
    }

    public function setStatusReason(string|FHIRString|null $value): self
    {
        $this->statusReason = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSource(): ?FHIRReference
    {
        return $this->source;
    }

    public function setSource(?FHIRReference $value): self
    {
        $this->source = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAddresses(): array
    {
        return $this->addresses;
    }

    public function setAddresses(?FHIRReference ...$value): self
    {
        $this->addresses = array_filter($value);

        return $this;
    }

    public function addAddresses(?FHIRReference ...$value): self
    {
        $this->addresses = array_filter(array_merge($this->addresses, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getOutcome(): array
    {
        return $this->outcome;
    }

    public function setOutcome(?FHIRCodeableReference ...$value): self
    {
        $this->outcome = array_filter($value);

        return $this;
    }

    public function addOutcome(?FHIRCodeableReference ...$value): self
    {
        $this->outcome = array_filter(array_merge($this->outcome, $value));

        return $this;
    }
}
