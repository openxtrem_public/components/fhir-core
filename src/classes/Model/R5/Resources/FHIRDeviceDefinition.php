<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceDefinition Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRDeviceDefinitionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRProductShelfLife;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDeviceDefinitionChargeItem;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDeviceDefinitionClassification;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDeviceDefinitionConformsTo;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDeviceDefinitionCorrectiveAction;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDeviceDefinitionDeviceName;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDeviceDefinitionGuideline;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDeviceDefinitionHasPart;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDeviceDefinitionLink;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDeviceDefinitionMaterial;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDeviceDefinitionPackaging;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDeviceDefinitionProperty;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDeviceDefinitionRegulatoryIdentifier;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDeviceDefinitionUdiDeviceIdentifier;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDeviceDefinitionVersion;

class FHIRDeviceDefinition extends FHIRDomainResource implements FHIRDeviceDefinitionInterface
{
    public const RESOURCE_NAME = 'DeviceDefinition';

    protected ?FHIRMarkdown $description = null;

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRDeviceDefinitionUdiDeviceIdentifier[] */
    protected array $udiDeviceIdentifier = [];

    /** @var FHIRDeviceDefinitionRegulatoryIdentifier[] */
    protected array $regulatoryIdentifier = [];
    protected ?FHIRString $partNumber = null;
    protected ?FHIRReference $manufacturer = null;

    /** @var FHIRDeviceDefinitionDeviceName[] */
    protected array $deviceName = [];
    protected ?FHIRString $modelNumber = null;

    /** @var FHIRDeviceDefinitionClassification[] */
    protected array $classification = [];

    /** @var FHIRDeviceDefinitionConformsTo[] */
    protected array $conformsTo = [];

    /** @var FHIRDeviceDefinitionHasPart[] */
    protected array $hasPart = [];

    /** @var FHIRDeviceDefinitionPackaging[] */
    protected array $packaging = [];

    /** @var FHIRDeviceDefinitionVersion[] */
    protected array $version = [];

    /** @var FHIRCodeableConcept[] */
    protected array $safety = [];

    /** @var FHIRProductShelfLife[] */
    protected array $shelfLifeStorage = [];

    /** @var FHIRCodeableConcept[] */
    protected array $languageCode = [];

    /** @var FHIRDeviceDefinitionProperty[] */
    protected array $property = [];
    protected ?FHIRReference $owner = null;

    /** @var FHIRContactPoint[] */
    protected array $contact = [];

    /** @var FHIRDeviceDefinitionLink[] */
    protected array $link = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRDeviceDefinitionMaterial[] */
    protected array $material = [];

    /** @var FHIRCode[] */
    protected array $productionIdentifierInUDI = [];
    protected ?FHIRDeviceDefinitionGuideline $guideline = null;
    protected ?FHIRDeviceDefinitionCorrectiveAction $correctiveAction = null;

    /** @var FHIRDeviceDefinitionChargeItem[] */
    protected array $chargeItem = [];

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRDeviceDefinitionUdiDeviceIdentifier[]
     */
    public function getUdiDeviceIdentifier(): array
    {
        return $this->udiDeviceIdentifier;
    }

    public function setUdiDeviceIdentifier(?FHIRDeviceDefinitionUdiDeviceIdentifier ...$value): self
    {
        $this->udiDeviceIdentifier = array_filter($value);

        return $this;
    }

    public function addUdiDeviceIdentifier(?FHIRDeviceDefinitionUdiDeviceIdentifier ...$value): self
    {
        $this->udiDeviceIdentifier = array_filter(array_merge($this->udiDeviceIdentifier, $value));

        return $this;
    }

    /**
     * @return FHIRDeviceDefinitionRegulatoryIdentifier[]
     */
    public function getRegulatoryIdentifier(): array
    {
        return $this->regulatoryIdentifier;
    }

    public function setRegulatoryIdentifier(?FHIRDeviceDefinitionRegulatoryIdentifier ...$value): self
    {
        $this->regulatoryIdentifier = array_filter($value);

        return $this;
    }

    public function addRegulatoryIdentifier(?FHIRDeviceDefinitionRegulatoryIdentifier ...$value): self
    {
        $this->regulatoryIdentifier = array_filter(array_merge($this->regulatoryIdentifier, $value));

        return $this;
    }

    public function getPartNumber(): ?FHIRString
    {
        return $this->partNumber;
    }

    public function setPartNumber(string|FHIRString|null $value): self
    {
        $this->partNumber = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getManufacturer(): ?FHIRReference
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?FHIRReference $value): self
    {
        $this->manufacturer = $value;

        return $this;
    }

    /**
     * @return FHIRDeviceDefinitionDeviceName[]
     */
    public function getDeviceName(): array
    {
        return $this->deviceName;
    }

    public function setDeviceName(?FHIRDeviceDefinitionDeviceName ...$value): self
    {
        $this->deviceName = array_filter($value);

        return $this;
    }

    public function addDeviceName(?FHIRDeviceDefinitionDeviceName ...$value): self
    {
        $this->deviceName = array_filter(array_merge($this->deviceName, $value));

        return $this;
    }

    public function getModelNumber(): ?FHIRString
    {
        return $this->modelNumber;
    }

    public function setModelNumber(string|FHIRString|null $value): self
    {
        $this->modelNumber = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRDeviceDefinitionClassification[]
     */
    public function getClassification(): array
    {
        return $this->classification;
    }

    public function setClassification(?FHIRDeviceDefinitionClassification ...$value): self
    {
        $this->classification = array_filter($value);

        return $this;
    }

    public function addClassification(?FHIRDeviceDefinitionClassification ...$value): self
    {
        $this->classification = array_filter(array_merge($this->classification, $value));

        return $this;
    }

    /**
     * @return FHIRDeviceDefinitionConformsTo[]
     */
    public function getConformsTo(): array
    {
        return $this->conformsTo;
    }

    public function setConformsTo(?FHIRDeviceDefinitionConformsTo ...$value): self
    {
        $this->conformsTo = array_filter($value);

        return $this;
    }

    public function addConformsTo(?FHIRDeviceDefinitionConformsTo ...$value): self
    {
        $this->conformsTo = array_filter(array_merge($this->conformsTo, $value));

        return $this;
    }

    /**
     * @return FHIRDeviceDefinitionHasPart[]
     */
    public function getHasPart(): array
    {
        return $this->hasPart;
    }

    public function setHasPart(?FHIRDeviceDefinitionHasPart ...$value): self
    {
        $this->hasPart = array_filter($value);

        return $this;
    }

    public function addHasPart(?FHIRDeviceDefinitionHasPart ...$value): self
    {
        $this->hasPart = array_filter(array_merge($this->hasPart, $value));

        return $this;
    }

    /**
     * @return FHIRDeviceDefinitionPackaging[]
     */
    public function getPackaging(): array
    {
        return $this->packaging;
    }

    public function setPackaging(?FHIRDeviceDefinitionPackaging ...$value): self
    {
        $this->packaging = array_filter($value);

        return $this;
    }

    public function addPackaging(?FHIRDeviceDefinitionPackaging ...$value): self
    {
        $this->packaging = array_filter(array_merge($this->packaging, $value));

        return $this;
    }

    /**
     * @return FHIRDeviceDefinitionVersion[]
     */
    public function getVersion(): array
    {
        return $this->version;
    }

    public function setVersion(?FHIRDeviceDefinitionVersion ...$value): self
    {
        $this->version = array_filter($value);

        return $this;
    }

    public function addVersion(?FHIRDeviceDefinitionVersion ...$value): self
    {
        $this->version = array_filter(array_merge($this->version, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSafety(): array
    {
        return $this->safety;
    }

    public function setSafety(?FHIRCodeableConcept ...$value): self
    {
        $this->safety = array_filter($value);

        return $this;
    }

    public function addSafety(?FHIRCodeableConcept ...$value): self
    {
        $this->safety = array_filter(array_merge($this->safety, $value));

        return $this;
    }

    /**
     * @return FHIRProductShelfLife[]
     */
    public function getShelfLifeStorage(): array
    {
        return $this->shelfLifeStorage;
    }

    public function setShelfLifeStorage(?FHIRProductShelfLife ...$value): self
    {
        $this->shelfLifeStorage = array_filter($value);

        return $this;
    }

    public function addShelfLifeStorage(?FHIRProductShelfLife ...$value): self
    {
        $this->shelfLifeStorage = array_filter(array_merge($this->shelfLifeStorage, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getLanguageCode(): array
    {
        return $this->languageCode;
    }

    public function setLanguageCode(?FHIRCodeableConcept ...$value): self
    {
        $this->languageCode = array_filter($value);

        return $this;
    }

    public function addLanguageCode(?FHIRCodeableConcept ...$value): self
    {
        $this->languageCode = array_filter(array_merge($this->languageCode, $value));

        return $this;
    }

    /**
     * @return FHIRDeviceDefinitionProperty[]
     */
    public function getProperty(): array
    {
        return $this->property;
    }

    public function setProperty(?FHIRDeviceDefinitionProperty ...$value): self
    {
        $this->property = array_filter($value);

        return $this;
    }

    public function addProperty(?FHIRDeviceDefinitionProperty ...$value): self
    {
        $this->property = array_filter(array_merge($this->property, $value));

        return $this;
    }

    public function getOwner(): ?FHIRReference
    {
        return $this->owner;
    }

    public function setOwner(?FHIRReference $value): self
    {
        $this->owner = $value;

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactPoint ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactPoint ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    /**
     * @return FHIRDeviceDefinitionLink[]
     */
    public function getLink(): array
    {
        return $this->link;
    }

    public function setLink(?FHIRDeviceDefinitionLink ...$value): self
    {
        $this->link = array_filter($value);

        return $this;
    }

    public function addLink(?FHIRDeviceDefinitionLink ...$value): self
    {
        $this->link = array_filter(array_merge($this->link, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRDeviceDefinitionMaterial[]
     */
    public function getMaterial(): array
    {
        return $this->material;
    }

    public function setMaterial(?FHIRDeviceDefinitionMaterial ...$value): self
    {
        $this->material = array_filter($value);

        return $this;
    }

    public function addMaterial(?FHIRDeviceDefinitionMaterial ...$value): self
    {
        $this->material = array_filter(array_merge($this->material, $value));

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getProductionIdentifierInUDI(): array
    {
        return $this->productionIdentifierInUDI;
    }

    public function setProductionIdentifierInUDI(string|FHIRCode|null ...$value): self
    {
        $this->productionIdentifierInUDI = [];
        $this->addProductionIdentifierInUDI(...$value);

        return $this;
    }

    public function addProductionIdentifierInUDI(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->productionIdentifierInUDI = array_filter(array_merge($this->productionIdentifierInUDI, $values));

        return $this;
    }

    public function getGuideline(): ?FHIRDeviceDefinitionGuideline
    {
        return $this->guideline;
    }

    public function setGuideline(?FHIRDeviceDefinitionGuideline $value): self
    {
        $this->guideline = $value;

        return $this;
    }

    public function getCorrectiveAction(): ?FHIRDeviceDefinitionCorrectiveAction
    {
        return $this->correctiveAction;
    }

    public function setCorrectiveAction(?FHIRDeviceDefinitionCorrectiveAction $value): self
    {
        $this->correctiveAction = $value;

        return $this;
    }

    /**
     * @return FHIRDeviceDefinitionChargeItem[]
     */
    public function getChargeItem(): array
    {
        return $this->chargeItem;
    }

    public function setChargeItem(?FHIRDeviceDefinitionChargeItem ...$value): self
    {
        $this->chargeItem = array_filter($value);

        return $this;
    }

    public function addChargeItem(?FHIRDeviceDefinitionChargeItem ...$value): self
    {
        $this->chargeItem = array_filter(array_merge($this->chargeItem, $value));

        return $this;
    }
}
