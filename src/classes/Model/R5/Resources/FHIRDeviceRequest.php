<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceRequest Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRDeviceRequestInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDeviceRequestParameter;

class FHIRDeviceRequest extends FHIRDomainResource implements FHIRDeviceRequestInterface
{
    public const RESOURCE_NAME = 'DeviceRequest';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRCanonical[] */
    protected array $instantiatesCanonical = [];

    /** @var FHIRUri[] */
    protected array $instantiatesUri = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];

    /** @var FHIRReference[] */
    protected array $replaces = [];
    protected ?FHIRIdentifier $groupIdentifier = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRCode $intent = null;
    protected ?FHIRCode $priority = null;
    protected ?FHIRBoolean $doNotPerform = null;
    protected ?FHIRCodeableReference $code = null;
    protected ?FHIRInteger $quantity = null;

    /** @var FHIRDeviceRequestParameter[] */
    protected array $parameter = [];
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $encounter = null;
    protected FHIRDateTime|FHIRPeriod|FHIRTiming|null $occurrence = null;
    protected ?FHIRDateTime $authoredOn = null;
    protected ?FHIRReference $requester = null;
    protected ?FHIRCodeableReference $performer = null;

    /** @var FHIRCodeableReference[] */
    protected array $reason = [];
    protected ?FHIRBoolean $asNeeded = null;
    protected ?FHIRCodeableConcept $asNeededFor = null;

    /** @var FHIRReference[] */
    protected array $insurance = [];

    /** @var FHIRReference[] */
    protected array $supportingInfo = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRReference[] */
    protected array $relevantHistory = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getInstantiatesCanonical(): array
    {
        return $this->instantiatesCanonical;
    }

    public function setInstantiatesCanonical(string|FHIRCanonical|null ...$value): self
    {
        $this->instantiatesCanonical = [];
        $this->addInstantiatesCanonical(...$value);

        return $this;
    }

    public function addInstantiatesCanonical(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->instantiatesCanonical = array_filter(array_merge($this->instantiatesCanonical, $values));

        return $this;
    }

    /**
     * @return FHIRUri[]
     */
    public function getInstantiatesUri(): array
    {
        return $this->instantiatesUri;
    }

    public function setInstantiatesUri(string|FHIRUri|null ...$value): self
    {
        $this->instantiatesUri = [];
        $this->addInstantiatesUri(...$value);

        return $this;
    }

    public function addInstantiatesUri(string|FHIRUri|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUri())->setValue($v) : $v, $value);

        $this->instantiatesUri = array_filter(array_merge($this->instantiatesUri, $values));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReplaces(): array
    {
        return $this->replaces;
    }

    public function setReplaces(?FHIRReference ...$value): self
    {
        $this->replaces = array_filter($value);

        return $this;
    }

    public function addReplaces(?FHIRReference ...$value): self
    {
        $this->replaces = array_filter(array_merge($this->replaces, $value));

        return $this;
    }

    public function getGroupIdentifier(): ?FHIRIdentifier
    {
        return $this->groupIdentifier;
    }

    public function setGroupIdentifier(?FHIRIdentifier $value): self
    {
        $this->groupIdentifier = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getIntent(): ?FHIRCode
    {
        return $this->intent;
    }

    public function setIntent(string|FHIRCode|null $value): self
    {
        $this->intent = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getPriority(): ?FHIRCode
    {
        return $this->priority;
    }

    public function setPriority(string|FHIRCode|null $value): self
    {
        $this->priority = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDoNotPerform(): ?FHIRBoolean
    {
        return $this->doNotPerform;
    }

    public function setDoNotPerform(bool|FHIRBoolean|null $value): self
    {
        $this->doNotPerform = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableReference
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableReference $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRInteger
    {
        return $this->quantity;
    }

    public function setQuantity(int|FHIRInteger|null $value): self
    {
        $this->quantity = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRDeviceRequestParameter[]
     */
    public function getParameter(): array
    {
        return $this->parameter;
    }

    public function setParameter(?FHIRDeviceRequestParameter ...$value): self
    {
        $this->parameter = array_filter($value);

        return $this;
    }

    public function addParameter(?FHIRDeviceRequestParameter ...$value): self
    {
        $this->parameter = array_filter(array_merge($this->parameter, $value));

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getOccurrence(): FHIRDateTime|FHIRPeriod|FHIRTiming|null
    {
        return $this->occurrence;
    }

    public function setOccurrence(FHIRDateTime|FHIRPeriod|FHIRTiming|null $value): self
    {
        $this->occurrence = $value;

        return $this;
    }

    public function getAuthoredOn(): ?FHIRDateTime
    {
        return $this->authoredOn;
    }

    public function setAuthoredOn(string|FHIRDateTime|null $value): self
    {
        $this->authoredOn = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getRequester(): ?FHIRReference
    {
        return $this->requester;
    }

    public function setRequester(?FHIRReference $value): self
    {
        $this->requester = $value;

        return $this;
    }

    public function getPerformer(): ?FHIRCodeableReference
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRCodeableReference $value): self
    {
        $this->performer = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getReason(): array
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableReference ...$value): self
    {
        $this->reason = array_filter($value);

        return $this;
    }

    public function addReason(?FHIRCodeableReference ...$value): self
    {
        $this->reason = array_filter(array_merge($this->reason, $value));

        return $this;
    }

    public function getAsNeeded(): ?FHIRBoolean
    {
        return $this->asNeeded;
    }

    public function setAsNeeded(bool|FHIRBoolean|null $value): self
    {
        $this->asNeeded = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getAsNeededFor(): ?FHIRCodeableConcept
    {
        return $this->asNeededFor;
    }

    public function setAsNeededFor(?FHIRCodeableConcept $value): self
    {
        $this->asNeededFor = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getInsurance(): array
    {
        return $this->insurance;
    }

    public function setInsurance(?FHIRReference ...$value): self
    {
        $this->insurance = array_filter($value);

        return $this;
    }

    public function addInsurance(?FHIRReference ...$value): self
    {
        $this->insurance = array_filter(array_merge($this->insurance, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupportingInfo(): array
    {
        return $this->supportingInfo;
    }

    public function setSupportingInfo(?FHIRReference ...$value): self
    {
        $this->supportingInfo = array_filter($value);

        return $this;
    }

    public function addSupportingInfo(?FHIRReference ...$value): self
    {
        $this->supportingInfo = array_filter(array_merge($this->supportingInfo, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getRelevantHistory(): array
    {
        return $this->relevantHistory;
    }

    public function setRelevantHistory(?FHIRReference ...$value): self
    {
        $this->relevantHistory = array_filter($value);

        return $this;
    }

    public function addRelevantHistory(?FHIRReference ...$value): self
    {
        $this->relevantHistory = array_filter(array_merge($this->relevantHistory, $value));

        return $this;
    }
}
