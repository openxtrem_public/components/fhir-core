<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PractitionerRole Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRPractitionerRoleInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAvailability;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRExtendedContactDetail;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;

class FHIRPractitionerRole extends FHIRDomainResource implements FHIRPractitionerRoleInterface
{
    public const RESOURCE_NAME = 'PractitionerRole';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRBoolean $active = null;
    protected ?FHIRPeriod $period = null;
    protected ?FHIRReference $practitioner = null;
    protected ?FHIRReference $organization = null;

    /** @var FHIRCodeableConcept[] */
    protected array $code = [];

    /** @var FHIRCodeableConcept[] */
    protected array $specialty = [];

    /** @var FHIRReference[] */
    protected array $location = [];

    /** @var FHIRReference[] */
    protected array $healthcareService = [];

    /** @var FHIRExtendedContactDetail[] */
    protected array $contact = [];

    /** @var FHIRCodeableConcept[] */
    protected array $characteristic = [];

    /** @var FHIRCodeableConcept[] */
    protected array $communication = [];

    /** @var FHIRAvailability[] */
    protected array $availability = [];

    /** @var FHIRReference[] */
    protected array $endpoint = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getActive(): ?FHIRBoolean
    {
        return $this->active;
    }

    public function setActive(bool|FHIRBoolean|null $value): self
    {
        $this->active = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    public function getPractitioner(): ?FHIRReference
    {
        return $this->practitioner;
    }

    public function setPractitioner(?FHIRReference $value): self
    {
        $this->practitioner = $value;

        return $this;
    }

    public function getOrganization(): ?FHIRReference
    {
        return $this->organization;
    }

    public function setOrganization(?FHIRReference $value): self
    {
        $this->organization = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCode(): array
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept ...$value): self
    {
        $this->code = array_filter($value);

        return $this;
    }

    public function addCode(?FHIRCodeableConcept ...$value): self
    {
        $this->code = array_filter(array_merge($this->code, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSpecialty(): array
    {
        return $this->specialty;
    }

    public function setSpecialty(?FHIRCodeableConcept ...$value): self
    {
        $this->specialty = array_filter($value);

        return $this;
    }

    public function addSpecialty(?FHIRCodeableConcept ...$value): self
    {
        $this->specialty = array_filter(array_merge($this->specialty, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getLocation(): array
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference ...$value): self
    {
        $this->location = array_filter($value);

        return $this;
    }

    public function addLocation(?FHIRReference ...$value): self
    {
        $this->location = array_filter(array_merge($this->location, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getHealthcareService(): array
    {
        return $this->healthcareService;
    }

    public function setHealthcareService(?FHIRReference ...$value): self
    {
        $this->healthcareService = array_filter($value);

        return $this;
    }

    public function addHealthcareService(?FHIRReference ...$value): self
    {
        $this->healthcareService = array_filter(array_merge($this->healthcareService, $value));

        return $this;
    }

    /**
     * @return FHIRExtendedContactDetail[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRExtendedContactDetail ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRExtendedContactDetail ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCharacteristic(): array
    {
        return $this->characteristic;
    }

    public function setCharacteristic(?FHIRCodeableConcept ...$value): self
    {
        $this->characteristic = array_filter($value);

        return $this;
    }

    public function addCharacteristic(?FHIRCodeableConcept ...$value): self
    {
        $this->characteristic = array_filter(array_merge($this->characteristic, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCommunication(): array
    {
        return $this->communication;
    }

    public function setCommunication(?FHIRCodeableConcept ...$value): self
    {
        $this->communication = array_filter($value);

        return $this;
    }

    public function addCommunication(?FHIRCodeableConcept ...$value): self
    {
        $this->communication = array_filter(array_merge($this->communication, $value));

        return $this;
    }

    /**
     * @return FHIRAvailability[]
     */
    public function getAvailability(): array
    {
        return $this->availability;
    }

    public function setAvailability(?FHIRAvailability ...$value): self
    {
        $this->availability = array_filter($value);

        return $this;
    }

    public function addAvailability(?FHIRAvailability ...$value): self
    {
        $this->availability = array_filter(array_merge($this->availability, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEndpoint(): array
    {
        return $this->endpoint;
    }

    public function setEndpoint(?FHIRReference ...$value): self
    {
        $this->endpoint = array_filter($value);

        return $this;
    }

    public function addEndpoint(?FHIRReference ...$value): self
    {
        $this->endpoint = array_filter(array_merge($this->endpoint, $value));

        return $this;
    }
}
