<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PaymentReconciliation Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRPaymentReconciliationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRPaymentReconciliationAllocation;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRPaymentReconciliationProcessNote;

class FHIRPaymentReconciliation extends FHIRDomainResource implements FHIRPaymentReconciliationInterface
{
    public const RESOURCE_NAME = 'PaymentReconciliation';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $kind = null;
    protected ?FHIRPeriod $period = null;
    protected ?FHIRDateTime $created = null;
    protected ?FHIRReference $enterer = null;
    protected ?FHIRCodeableConcept $issuerType = null;
    protected ?FHIRReference $paymentIssuer = null;
    protected ?FHIRReference $request = null;
    protected ?FHIRReference $requestor = null;
    protected ?FHIRCode $outcome = null;
    protected ?FHIRString $disposition = null;
    protected ?FHIRDate $date = null;
    protected ?FHIRReference $location = null;
    protected ?FHIRCodeableConcept $method = null;
    protected ?FHIRString $cardBrand = null;
    protected ?FHIRString $accountNumber = null;
    protected ?FHIRDate $expirationDate = null;
    protected ?FHIRString $processor = null;
    protected ?FHIRString $referenceNumber = null;
    protected ?FHIRString $authorization = null;
    protected ?FHIRMoney $tenderedAmount = null;
    protected ?FHIRMoney $returnedAmount = null;
    protected ?FHIRMoney $amount = null;
    protected ?FHIRIdentifier $paymentIdentifier = null;

    /** @var FHIRPaymentReconciliationAllocation[] */
    protected array $allocation = [];
    protected ?FHIRCodeableConcept $formCode = null;

    /** @var FHIRPaymentReconciliationProcessNote[] */
    protected array $processNote = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getKind(): ?FHIRCodeableConcept
    {
        return $this->kind;
    }

    public function setKind(?FHIRCodeableConcept $value): self
    {
        $this->kind = $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    public function getCreated(): ?FHIRDateTime
    {
        return $this->created;
    }

    public function setCreated(string|FHIRDateTime|null $value): self
    {
        $this->created = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getEnterer(): ?FHIRReference
    {
        return $this->enterer;
    }

    public function setEnterer(?FHIRReference $value): self
    {
        $this->enterer = $value;

        return $this;
    }

    public function getIssuerType(): ?FHIRCodeableConcept
    {
        return $this->issuerType;
    }

    public function setIssuerType(?FHIRCodeableConcept $value): self
    {
        $this->issuerType = $value;

        return $this;
    }

    public function getPaymentIssuer(): ?FHIRReference
    {
        return $this->paymentIssuer;
    }

    public function setPaymentIssuer(?FHIRReference $value): self
    {
        $this->paymentIssuer = $value;

        return $this;
    }

    public function getRequest(): ?FHIRReference
    {
        return $this->request;
    }

    public function setRequest(?FHIRReference $value): self
    {
        $this->request = $value;

        return $this;
    }

    public function getRequestor(): ?FHIRReference
    {
        return $this->requestor;
    }

    public function setRequestor(?FHIRReference $value): self
    {
        $this->requestor = $value;

        return $this;
    }

    public function getOutcome(): ?FHIRCode
    {
        return $this->outcome;
    }

    public function setOutcome(string|FHIRCode|null $value): self
    {
        $this->outcome = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDisposition(): ?FHIRString
    {
        return $this->disposition;
    }

    public function setDisposition(string|FHIRString|null $value): self
    {
        $this->disposition = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDate(): ?FHIRDate
    {
        return $this->date;
    }

    public function setDate(string|FHIRDate|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    public function getMethod(): ?FHIRCodeableConcept
    {
        return $this->method;
    }

    public function setMethod(?FHIRCodeableConcept $value): self
    {
        $this->method = $value;

        return $this;
    }

    public function getCardBrand(): ?FHIRString
    {
        return $this->cardBrand;
    }

    public function setCardBrand(string|FHIRString|null $value): self
    {
        $this->cardBrand = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getAccountNumber(): ?FHIRString
    {
        return $this->accountNumber;
    }

    public function setAccountNumber(string|FHIRString|null $value): self
    {
        $this->accountNumber = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getExpirationDate(): ?FHIRDate
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(string|FHIRDate|null $value): self
    {
        $this->expirationDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getProcessor(): ?FHIRString
    {
        return $this->processor;
    }

    public function setProcessor(string|FHIRString|null $value): self
    {
        $this->processor = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getReferenceNumber(): ?FHIRString
    {
        return $this->referenceNumber;
    }

    public function setReferenceNumber(string|FHIRString|null $value): self
    {
        $this->referenceNumber = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getAuthorization(): ?FHIRString
    {
        return $this->authorization;
    }

    public function setAuthorization(string|FHIRString|null $value): self
    {
        $this->authorization = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTenderedAmount(): ?FHIRMoney
    {
        return $this->tenderedAmount;
    }

    public function setTenderedAmount(?FHIRMoney $value): self
    {
        $this->tenderedAmount = $value;

        return $this;
    }

    public function getReturnedAmount(): ?FHIRMoney
    {
        return $this->returnedAmount;
    }

    public function setReturnedAmount(?FHIRMoney $value): self
    {
        $this->returnedAmount = $value;

        return $this;
    }

    public function getAmount(): ?FHIRMoney
    {
        return $this->amount;
    }

    public function setAmount(?FHIRMoney $value): self
    {
        $this->amount = $value;

        return $this;
    }

    public function getPaymentIdentifier(): ?FHIRIdentifier
    {
        return $this->paymentIdentifier;
    }

    public function setPaymentIdentifier(?FHIRIdentifier $value): self
    {
        $this->paymentIdentifier = $value;

        return $this;
    }

    /**
     * @return FHIRPaymentReconciliationAllocation[]
     */
    public function getAllocation(): array
    {
        return $this->allocation;
    }

    public function setAllocation(?FHIRPaymentReconciliationAllocation ...$value): self
    {
        $this->allocation = array_filter($value);

        return $this;
    }

    public function addAllocation(?FHIRPaymentReconciliationAllocation ...$value): self
    {
        $this->allocation = array_filter(array_merge($this->allocation, $value));

        return $this;
    }

    public function getFormCode(): ?FHIRCodeableConcept
    {
        return $this->formCode;
    }

    public function setFormCode(?FHIRCodeableConcept $value): self
    {
        $this->formCode = $value;

        return $this;
    }

    /**
     * @return FHIRPaymentReconciliationProcessNote[]
     */
    public function getProcessNote(): array
    {
        return $this->processNote;
    }

    public function setProcessNote(?FHIRPaymentReconciliationProcessNote ...$value): self
    {
        $this->processNote = array_filter($value);

        return $this;
    }

    public function addProcessNote(?FHIRPaymentReconciliationProcessNote ...$value): self
    {
        $this->processNote = array_filter(array_merge($this->processNote, $value));

        return $this;
    }
}
