<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestScript Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRTestScriptInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRTestScriptDestination;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRTestScriptFixture;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRTestScriptMetadata;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRTestScriptOrigin;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRTestScriptScope;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRTestScriptSetup;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRTestScriptTeardown;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRTestScriptTest;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRTestScriptVariable;

class FHIRTestScript extends FHIRDomainResource implements FHIRTestScriptInterface
{
    public const RESOURCE_NAME = 'TestScript';

    protected ?FHIRUri $url = null;

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRString $version = null;
    protected FHIRString|FHIRCoding|null $versionAlgorithm = null;
    protected ?FHIRString $name = null;
    protected ?FHIRString $title = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRBoolean $experimental = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRString $publisher = null;

    /** @var FHIRContactDetail[] */
    protected array $contact = [];
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRUsageContext[] */
    protected array $useContext = [];

    /** @var FHIRCodeableConcept[] */
    protected array $jurisdiction = [];
    protected ?FHIRMarkdown $purpose = null;
    protected ?FHIRMarkdown $copyright = null;
    protected ?FHIRString $copyrightLabel = null;

    /** @var FHIRTestScriptOrigin[] */
    protected array $origin = [];

    /** @var FHIRTestScriptDestination[] */
    protected array $destination = [];
    protected ?FHIRTestScriptMetadata $metadata = null;

    /** @var FHIRTestScriptScope[] */
    protected array $scope = [];

    /** @var FHIRTestScriptFixture[] */
    protected array $fixture = [];

    /** @var FHIRCanonical[] */
    protected array $profile = [];

    /** @var FHIRTestScriptVariable[] */
    protected array $variable = [];
    protected ?FHIRTestScriptSetup $setup = null;

    /** @var FHIRTestScriptTest[] */
    protected array $test = [];
    protected ?FHIRTestScriptTeardown $teardown = null;

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getVersionAlgorithm(): FHIRString|FHIRCoding|null
    {
        return $this->versionAlgorithm;
    }

    public function setVersionAlgorithm(FHIRString|FHIRCoding|null $value): self
    {
        $this->versionAlgorithm = $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getExperimental(): ?FHIRBoolean
    {
        return $this->experimental;
    }

    public function setExperimental(bool|FHIRBoolean|null $value): self
    {
        $this->experimental = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPublisher(): ?FHIRString
    {
        return $this->publisher;
    }

    public function setPublisher(string|FHIRString|null $value): self
    {
        $this->publisher = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUseContext(): array
    {
        return $this->useContext;
    }

    public function setUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter($value);

        return $this;
    }

    public function addUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter(array_merge($this->useContext, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getJurisdiction(): array
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter($value);

        return $this;
    }

    public function addJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter(array_merge($this->jurisdiction, $value));

        return $this;
    }

    public function getPurpose(): ?FHIRMarkdown
    {
        return $this->purpose;
    }

    public function setPurpose(string|FHIRMarkdown|null $value): self
    {
        $this->purpose = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getCopyright(): ?FHIRMarkdown
    {
        return $this->copyright;
    }

    public function setCopyright(string|FHIRMarkdown|null $value): self
    {
        $this->copyright = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getCopyrightLabel(): ?FHIRString
    {
        return $this->copyrightLabel;
    }

    public function setCopyrightLabel(string|FHIRString|null $value): self
    {
        $this->copyrightLabel = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRTestScriptOrigin[]
     */
    public function getOrigin(): array
    {
        return $this->origin;
    }

    public function setOrigin(?FHIRTestScriptOrigin ...$value): self
    {
        $this->origin = array_filter($value);

        return $this;
    }

    public function addOrigin(?FHIRTestScriptOrigin ...$value): self
    {
        $this->origin = array_filter(array_merge($this->origin, $value));

        return $this;
    }

    /**
     * @return FHIRTestScriptDestination[]
     */
    public function getDestination(): array
    {
        return $this->destination;
    }

    public function setDestination(?FHIRTestScriptDestination ...$value): self
    {
        $this->destination = array_filter($value);

        return $this;
    }

    public function addDestination(?FHIRTestScriptDestination ...$value): self
    {
        $this->destination = array_filter(array_merge($this->destination, $value));

        return $this;
    }

    public function getMetadata(): ?FHIRTestScriptMetadata
    {
        return $this->metadata;
    }

    public function setMetadata(?FHIRTestScriptMetadata $value): self
    {
        $this->metadata = $value;

        return $this;
    }

    /**
     * @return FHIRTestScriptScope[]
     */
    public function getScope(): array
    {
        return $this->scope;
    }

    public function setScope(?FHIRTestScriptScope ...$value): self
    {
        $this->scope = array_filter($value);

        return $this;
    }

    public function addScope(?FHIRTestScriptScope ...$value): self
    {
        $this->scope = array_filter(array_merge($this->scope, $value));

        return $this;
    }

    /**
     * @return FHIRTestScriptFixture[]
     */
    public function getFixture(): array
    {
        return $this->fixture;
    }

    public function setFixture(?FHIRTestScriptFixture ...$value): self
    {
        $this->fixture = array_filter($value);

        return $this;
    }

    public function addFixture(?FHIRTestScriptFixture ...$value): self
    {
        $this->fixture = array_filter(array_merge($this->fixture, $value));

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getProfile(): array
    {
        return $this->profile;
    }

    public function setProfile(string|FHIRCanonical|null ...$value): self
    {
        $this->profile = [];
        $this->addProfile(...$value);

        return $this;
    }

    public function addProfile(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->profile = array_filter(array_merge($this->profile, $values));

        return $this;
    }

    /**
     * @return FHIRTestScriptVariable[]
     */
    public function getVariable(): array
    {
        return $this->variable;
    }

    public function setVariable(?FHIRTestScriptVariable ...$value): self
    {
        $this->variable = array_filter($value);

        return $this;
    }

    public function addVariable(?FHIRTestScriptVariable ...$value): self
    {
        $this->variable = array_filter(array_merge($this->variable, $value));

        return $this;
    }

    public function getSetup(): ?FHIRTestScriptSetup
    {
        return $this->setup;
    }

    public function setSetup(?FHIRTestScriptSetup $value): self
    {
        $this->setup = $value;

        return $this;
    }

    /**
     * @return FHIRTestScriptTest[]
     */
    public function getTest(): array
    {
        return $this->test;
    }

    public function setTest(?FHIRTestScriptTest ...$value): self
    {
        $this->test = array_filter($value);

        return $this;
    }

    public function addTest(?FHIRTestScriptTest ...$value): self
    {
        $this->test = array_filter(array_merge($this->test, $value));

        return $this;
    }

    public function getTeardown(): ?FHIRTestScriptTeardown
    {
        return $this->teardown;
    }

    public function setTeardown(?FHIRTestScriptTeardown $value): self
    {
        $this->teardown = $value;

        return $this;
    }
}
