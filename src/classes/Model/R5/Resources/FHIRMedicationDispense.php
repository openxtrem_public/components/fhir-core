<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationDispense Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMedicationDispenseInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRDosage;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMedicationDispensePerformer;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMedicationDispenseSubstitution;

class FHIRMedicationDispense extends FHIRDomainResource implements FHIRMedicationDispenseInterface
{
    public const RESOURCE_NAME = 'MedicationDispense';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];

    /** @var FHIRReference[] */
    protected array $partOf = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableReference $notPerformedReason = null;
    protected ?FHIRDateTime $statusChanged = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRCodeableReference $medication = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $encounter = null;

    /** @var FHIRReference[] */
    protected array $supportingInformation = [];

    /** @var FHIRMedicationDispensePerformer[] */
    protected array $performer = [];
    protected ?FHIRReference $location = null;

    /** @var FHIRReference[] */
    protected array $authorizingPrescription = [];
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRQuantity $daysSupply = null;
    protected ?FHIRDateTime $recorded = null;
    protected ?FHIRDateTime $whenPrepared = null;
    protected ?FHIRDateTime $whenHandedOver = null;
    protected ?FHIRReference $destination = null;

    /** @var FHIRReference[] */
    protected array $receiver = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];
    protected ?FHIRMarkdown $renderedDosageInstruction = null;

    /** @var FHIRDosage[] */
    protected array $dosageInstruction = [];
    protected ?FHIRMedicationDispenseSubstitution $substitution = null;

    /** @var FHIRReference[] */
    protected array $eventHistory = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPartOf(): array
    {
        return $this->partOf;
    }

    public function setPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter($value);

        return $this;
    }

    public function addPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter(array_merge($this->partOf, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getNotPerformedReason(): ?FHIRCodeableReference
    {
        return $this->notPerformedReason;
    }

    public function setNotPerformedReason(?FHIRCodeableReference $value): self
    {
        $this->notPerformedReason = $value;

        return $this;
    }

    public function getStatusChanged(): ?FHIRDateTime
    {
        return $this->statusChanged;
    }

    public function setStatusChanged(string|FHIRDateTime|null $value): self
    {
        $this->statusChanged = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getMedication(): ?FHIRCodeableReference
    {
        return $this->medication;
    }

    public function setMedication(?FHIRCodeableReference $value): self
    {
        $this->medication = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupportingInformation(): array
    {
        return $this->supportingInformation;
    }

    public function setSupportingInformation(?FHIRReference ...$value): self
    {
        $this->supportingInformation = array_filter($value);

        return $this;
    }

    public function addSupportingInformation(?FHIRReference ...$value): self
    {
        $this->supportingInformation = array_filter(array_merge($this->supportingInformation, $value));

        return $this;
    }

    /**
     * @return FHIRMedicationDispensePerformer[]
     */
    public function getPerformer(): array
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRMedicationDispensePerformer ...$value): self
    {
        $this->performer = array_filter($value);

        return $this;
    }

    public function addPerformer(?FHIRMedicationDispensePerformer ...$value): self
    {
        $this->performer = array_filter(array_merge($this->performer, $value));

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAuthorizingPrescription(): array
    {
        return $this->authorizingPrescription;
    }

    public function setAuthorizingPrescription(?FHIRReference ...$value): self
    {
        $this->authorizingPrescription = array_filter($value);

        return $this;
    }

    public function addAuthorizingPrescription(?FHIRReference ...$value): self
    {
        $this->authorizingPrescription = array_filter(array_merge($this->authorizingPrescription, $value));

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getDaysSupply(): ?FHIRQuantity
    {
        return $this->daysSupply;
    }

    public function setDaysSupply(?FHIRQuantity $value): self
    {
        $this->daysSupply = $value;

        return $this;
    }

    public function getRecorded(): ?FHIRDateTime
    {
        return $this->recorded;
    }

    public function setRecorded(string|FHIRDateTime|null $value): self
    {
        $this->recorded = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getWhenPrepared(): ?FHIRDateTime
    {
        return $this->whenPrepared;
    }

    public function setWhenPrepared(string|FHIRDateTime|null $value): self
    {
        $this->whenPrepared = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getWhenHandedOver(): ?FHIRDateTime
    {
        return $this->whenHandedOver;
    }

    public function setWhenHandedOver(string|FHIRDateTime|null $value): self
    {
        $this->whenHandedOver = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getDestination(): ?FHIRReference
    {
        return $this->destination;
    }

    public function setDestination(?FHIRReference $value): self
    {
        $this->destination = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReceiver(): array
    {
        return $this->receiver;
    }

    public function setReceiver(?FHIRReference ...$value): self
    {
        $this->receiver = array_filter($value);

        return $this;
    }

    public function addReceiver(?FHIRReference ...$value): self
    {
        $this->receiver = array_filter(array_merge($this->receiver, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    public function getRenderedDosageInstruction(): ?FHIRMarkdown
    {
        return $this->renderedDosageInstruction;
    }

    public function setRenderedDosageInstruction(string|FHIRMarkdown|null $value): self
    {
        $this->renderedDosageInstruction = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRDosage[]
     */
    public function getDosageInstruction(): array
    {
        return $this->dosageInstruction;
    }

    public function setDosageInstruction(?FHIRDosage ...$value): self
    {
        $this->dosageInstruction = array_filter($value);

        return $this;
    }

    public function addDosageInstruction(?FHIRDosage ...$value): self
    {
        $this->dosageInstruction = array_filter(array_merge($this->dosageInstruction, $value));

        return $this;
    }

    public function getSubstitution(): ?FHIRMedicationDispenseSubstitution
    {
        return $this->substitution;
    }

    public function setSubstitution(?FHIRMedicationDispenseSubstitution $value): self
    {
        $this->substitution = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEventHistory(): array
    {
        return $this->eventHistory;
    }

    public function setEventHistory(?FHIRReference ...$value): self
    {
        $this->eventHistory = array_filter($value);

        return $this;
    }

    public function addEventHistory(?FHIRReference ...$value): self
    {
        $this->eventHistory = array_filter(array_merge($this->eventHistory, $value));

        return $this;
    }
}
