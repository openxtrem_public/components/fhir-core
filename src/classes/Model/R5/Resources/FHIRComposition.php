<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Composition Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRCompositionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRRelatedArtifact;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRCompositionAttester;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRCompositionEvent;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRCompositionSection;

class FHIRComposition extends FHIRDomainResource implements FHIRCompositionInterface
{
    public const RESOURCE_NAME = 'Composition';

    protected ?FHIRUri $url = null;

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRString $version = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];

    /** @var FHIRReference[] */
    protected array $subject = [];
    protected ?FHIRReference $encounter = null;
    protected ?FHIRDateTime $date = null;

    /** @var FHIRUsageContext[] */
    protected array $useContext = [];

    /** @var FHIRReference[] */
    protected array $author = [];
    protected ?FHIRString $name = null;
    protected ?FHIRString $title = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRCompositionAttester[] */
    protected array $attester = [];
    protected ?FHIRReference $custodian = null;

    /** @var FHIRRelatedArtifact[] */
    protected array $relatesTo = [];

    /** @var FHIRCompositionEvent[] */
    protected array $event = [];

    /** @var FHIRCompositionSection[] */
    protected array $section = [];

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSubject(): array
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference ...$value): self
    {
        $this->subject = array_filter($value);

        return $this;
    }

    public function addSubject(?FHIRReference ...$value): self
    {
        $this->subject = array_filter(array_merge($this->subject, $value));

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUseContext(): array
    {
        return $this->useContext;
    }

    public function setUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter($value);

        return $this;
    }

    public function addUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter(array_merge($this->useContext, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAuthor(): array
    {
        return $this->author;
    }

    public function setAuthor(?FHIRReference ...$value): self
    {
        $this->author = array_filter($value);

        return $this;
    }

    public function addAuthor(?FHIRReference ...$value): self
    {
        $this->author = array_filter(array_merge($this->author, $value));

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRCompositionAttester[]
     */
    public function getAttester(): array
    {
        return $this->attester;
    }

    public function setAttester(?FHIRCompositionAttester ...$value): self
    {
        $this->attester = array_filter($value);

        return $this;
    }

    public function addAttester(?FHIRCompositionAttester ...$value): self
    {
        $this->attester = array_filter(array_merge($this->attester, $value));

        return $this;
    }

    public function getCustodian(): ?FHIRReference
    {
        return $this->custodian;
    }

    public function setCustodian(?FHIRReference $value): self
    {
        $this->custodian = $value;

        return $this;
    }

    /**
     * @return FHIRRelatedArtifact[]
     */
    public function getRelatesTo(): array
    {
        return $this->relatesTo;
    }

    public function setRelatesTo(?FHIRRelatedArtifact ...$value): self
    {
        $this->relatesTo = array_filter($value);

        return $this;
    }

    public function addRelatesTo(?FHIRRelatedArtifact ...$value): self
    {
        $this->relatesTo = array_filter(array_merge($this->relatesTo, $value));

        return $this;
    }

    /**
     * @return FHIRCompositionEvent[]
     */
    public function getEvent(): array
    {
        return $this->event;
    }

    public function setEvent(?FHIRCompositionEvent ...$value): self
    {
        $this->event = array_filter($value);

        return $this;
    }

    public function addEvent(?FHIRCompositionEvent ...$value): self
    {
        $this->event = array_filter(array_merge($this->event, $value));

        return $this;
    }

    /**
     * @return FHIRCompositionSection[]
     */
    public function getSection(): array
    {
        return $this->section;
    }

    public function setSection(?FHIRCompositionSection ...$value): self
    {
        $this->section = array_filter($value);

        return $this;
    }

    public function addSection(?FHIRCompositionSection ...$value): self
    {
        $this->section = array_filter(array_merge($this->section, $value));

        return $this;
    }
}
