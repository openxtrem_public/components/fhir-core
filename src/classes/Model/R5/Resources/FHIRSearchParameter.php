<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SearchParameter Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRSearchParameterInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRSearchParameterComponent;

class FHIRSearchParameter extends FHIRDomainResource implements FHIRSearchParameterInterface
{
    public const RESOURCE_NAME = 'SearchParameter';

    protected ?FHIRUri $url = null;

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRString $version = null;
    protected FHIRString|FHIRCoding|null $versionAlgorithm = null;
    protected ?FHIRString $name = null;
    protected ?FHIRString $title = null;
    protected ?FHIRCanonical $derivedFrom = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRBoolean $experimental = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRString $publisher = null;

    /** @var FHIRContactDetail[] */
    protected array $contact = [];
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRUsageContext[] */
    protected array $useContext = [];

    /** @var FHIRCodeableConcept[] */
    protected array $jurisdiction = [];
    protected ?FHIRMarkdown $purpose = null;
    protected ?FHIRMarkdown $copyright = null;
    protected ?FHIRString $copyrightLabel = null;
    protected ?FHIRCode $code = null;

    /** @var FHIRCode[] */
    protected array $base = [];
    protected ?FHIRCode $type = null;
    protected ?FHIRString $expression = null;
    protected ?FHIRCode $processingMode = null;
    protected ?FHIRString $constraint = null;

    /** @var FHIRCode[] */
    protected array $target = [];
    protected ?FHIRBoolean $multipleOr = null;
    protected ?FHIRBoolean $multipleAnd = null;

    /** @var FHIRCode[] */
    protected array $comparator = [];

    /** @var FHIRCode[] */
    protected array $modifier = [];

    /** @var FHIRString[] */
    protected array $chain = [];

    /** @var FHIRSearchParameterComponent[] */
    protected array $component = [];

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getVersionAlgorithm(): FHIRString|FHIRCoding|null
    {
        return $this->versionAlgorithm;
    }

    public function setVersionAlgorithm(FHIRString|FHIRCoding|null $value): self
    {
        $this->versionAlgorithm = $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDerivedFrom(): ?FHIRCanonical
    {
        return $this->derivedFrom;
    }

    public function setDerivedFrom(string|FHIRCanonical|null $value): self
    {
        $this->derivedFrom = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getExperimental(): ?FHIRBoolean
    {
        return $this->experimental;
    }

    public function setExperimental(bool|FHIRBoolean|null $value): self
    {
        $this->experimental = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPublisher(): ?FHIRString
    {
        return $this->publisher;
    }

    public function setPublisher(string|FHIRString|null $value): self
    {
        $this->publisher = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUseContext(): array
    {
        return $this->useContext;
    }

    public function setUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter($value);

        return $this;
    }

    public function addUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter(array_merge($this->useContext, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getJurisdiction(): array
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter($value);

        return $this;
    }

    public function addJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter(array_merge($this->jurisdiction, $value));

        return $this;
    }

    public function getPurpose(): ?FHIRMarkdown
    {
        return $this->purpose;
    }

    public function setPurpose(string|FHIRMarkdown|null $value): self
    {
        $this->purpose = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getCopyright(): ?FHIRMarkdown
    {
        return $this->copyright;
    }

    public function setCopyright(string|FHIRMarkdown|null $value): self
    {
        $this->copyright = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getCopyrightLabel(): ?FHIRString
    {
        return $this->copyrightLabel;
    }

    public function setCopyrightLabel(string|FHIRString|null $value): self
    {
        $this->copyrightLabel = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getBase(): array
    {
        return $this->base;
    }

    public function setBase(string|FHIRCode|null ...$value): self
    {
        $this->base = [];
        $this->addBase(...$value);

        return $this;
    }

    public function addBase(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->base = array_filter(array_merge($this->base, $values));

        return $this;
    }

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getExpression(): ?FHIRString
    {
        return $this->expression;
    }

    public function setExpression(string|FHIRString|null $value): self
    {
        $this->expression = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getProcessingMode(): ?FHIRCode
    {
        return $this->processingMode;
    }

    public function setProcessingMode(string|FHIRCode|null $value): self
    {
        $this->processingMode = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getConstraint(): ?FHIRString
    {
        return $this->constraint;
    }

    public function setConstraint(string|FHIRString|null $value): self
    {
        $this->constraint = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getTarget(): array
    {
        return $this->target;
    }

    public function setTarget(string|FHIRCode|null ...$value): self
    {
        $this->target = [];
        $this->addTarget(...$value);

        return $this;
    }

    public function addTarget(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->target = array_filter(array_merge($this->target, $values));

        return $this;
    }

    public function getMultipleOr(): ?FHIRBoolean
    {
        return $this->multipleOr;
    }

    public function setMultipleOr(bool|FHIRBoolean|null $value): self
    {
        $this->multipleOr = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getMultipleAnd(): ?FHIRBoolean
    {
        return $this->multipleAnd;
    }

    public function setMultipleAnd(bool|FHIRBoolean|null $value): self
    {
        $this->multipleAnd = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getComparator(): array
    {
        return $this->comparator;
    }

    public function setComparator(string|FHIRCode|null ...$value): self
    {
        $this->comparator = [];
        $this->addComparator(...$value);

        return $this;
    }

    public function addComparator(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->comparator = array_filter(array_merge($this->comparator, $values));

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getModifier(): array
    {
        return $this->modifier;
    }

    public function setModifier(string|FHIRCode|null ...$value): self
    {
        $this->modifier = [];
        $this->addModifier(...$value);

        return $this;
    }

    public function addModifier(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->modifier = array_filter(array_merge($this->modifier, $values));

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getChain(): array
    {
        return $this->chain;
    }

    public function setChain(string|FHIRString|null ...$value): self
    {
        $this->chain = [];
        $this->addChain(...$value);

        return $this;
    }

    public function addChain(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->chain = array_filter(array_merge($this->chain, $values));

        return $this;
    }

    /**
     * @return FHIRSearchParameterComponent[]
     */
    public function getComponent(): array
    {
        return $this->component;
    }

    public function setComponent(?FHIRSearchParameterComponent ...$value): self
    {
        $this->component = array_filter($value);

        return $this;
    }

    public function addComponent(?FHIRSearchParameterComponent ...$value): self
    {
        $this->component = array_filter(array_merge($this->component, $value));

        return $this;
    }
}
