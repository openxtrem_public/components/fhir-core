<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Provenance Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRProvenanceInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRSignature;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRProvenanceAgent;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRProvenanceEntity;

class FHIRProvenance extends FHIRDomainResource implements FHIRProvenanceInterface
{
    public const RESOURCE_NAME = 'Provenance';

    /** @var FHIRReference[] */
    protected array $target = [];
    protected FHIRPeriod|FHIRDateTime|null $occurred = null;
    protected ?FHIRInstant $recorded = null;

    /** @var FHIRUri[] */
    protected array $policy = [];
    protected ?FHIRReference $location = null;

    /** @var FHIRCodeableReference[] */
    protected array $authorization = [];
    protected ?FHIRCodeableConcept $activity = null;

    /** @var FHIRReference[] */
    protected array $basedOn = [];
    protected ?FHIRReference $patient = null;
    protected ?FHIRReference $encounter = null;

    /** @var FHIRProvenanceAgent[] */
    protected array $agent = [];

    /** @var FHIRProvenanceEntity[] */
    protected array $entity = [];

    /** @var FHIRSignature[] */
    protected array $signature = [];

    /**
     * @return FHIRReference[]
     */
    public function getTarget(): array
    {
        return $this->target;
    }

    public function setTarget(?FHIRReference ...$value): self
    {
        $this->target = array_filter($value);

        return $this;
    }

    public function addTarget(?FHIRReference ...$value): self
    {
        $this->target = array_filter(array_merge($this->target, $value));

        return $this;
    }

    public function getOccurred(): FHIRPeriod|FHIRDateTime|null
    {
        return $this->occurred;
    }

    public function setOccurred(FHIRPeriod|FHIRDateTime|null $value): self
    {
        $this->occurred = $value;

        return $this;
    }

    public function getRecorded(): ?FHIRInstant
    {
        return $this->recorded;
    }

    public function setRecorded(string|FHIRInstant|null $value): self
    {
        $this->recorded = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRUri[]
     */
    public function getPolicy(): array
    {
        return $this->policy;
    }

    public function setPolicy(string|FHIRUri|null ...$value): self
    {
        $this->policy = [];
        $this->addPolicy(...$value);

        return $this;
    }

    public function addPolicy(string|FHIRUri|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUri())->setValue($v) : $v, $value);

        $this->policy = array_filter(array_merge($this->policy, $values));

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getAuthorization(): array
    {
        return $this->authorization;
    }

    public function setAuthorization(?FHIRCodeableReference ...$value): self
    {
        $this->authorization = array_filter($value);

        return $this;
    }

    public function addAuthorization(?FHIRCodeableReference ...$value): self
    {
        $this->authorization = array_filter(array_merge($this->authorization, $value));

        return $this;
    }

    public function getActivity(): ?FHIRCodeableConcept
    {
        return $this->activity;
    }

    public function setActivity(?FHIRCodeableConcept $value): self
    {
        $this->activity = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    /**
     * @return FHIRProvenanceAgent[]
     */
    public function getAgent(): array
    {
        return $this->agent;
    }

    public function setAgent(?FHIRProvenanceAgent ...$value): self
    {
        $this->agent = array_filter($value);

        return $this;
    }

    public function addAgent(?FHIRProvenanceAgent ...$value): self
    {
        $this->agent = array_filter(array_merge($this->agent, $value));

        return $this;
    }

    /**
     * @return FHIRProvenanceEntity[]
     */
    public function getEntity(): array
    {
        return $this->entity;
    }

    public function setEntity(?FHIRProvenanceEntity ...$value): self
    {
        $this->entity = array_filter($value);

        return $this;
    }

    public function addEntity(?FHIRProvenanceEntity ...$value): self
    {
        $this->entity = array_filter(array_merge($this->entity, $value));

        return $this;
    }

    /**
     * @return FHIRSignature[]
     */
    public function getSignature(): array
    {
        return $this->signature;
    }

    public function setSignature(?FHIRSignature ...$value): self
    {
        $this->signature = array_filter($value);

        return $this;
    }

    public function addSignature(?FHIRSignature ...$value): self
    {
        $this->signature = array_filter(array_merge($this->signature, $value));

        return $this;
    }
}
