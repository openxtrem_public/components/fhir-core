<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ArtifactAssessment Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRArtifactAssessmentInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRArtifactAssessmentContent;

class FHIRArtifactAssessment extends FHIRDomainResource implements FHIRArtifactAssessmentInterface
{
    public const RESOURCE_NAME = 'ArtifactAssessment';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRString $title = null;
    protected FHIRReference|FHIRMarkdown|null $citeAs = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRMarkdown $copyright = null;
    protected ?FHIRDate $approvalDate = null;
    protected ?FHIRDate $lastReviewDate = null;
    protected FHIRReference|FHIRCanonical|FHIRUri|null $artifact = null;

    /** @var FHIRArtifactAssessmentContent[] */
    protected array $content = [];
    protected ?FHIRCode $workflowStatus = null;
    protected ?FHIRCode $disposition = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCiteAs(): FHIRReference|FHIRMarkdown|null
    {
        return $this->citeAs;
    }

    public function setCiteAs(FHIRReference|FHIRMarkdown|null $value): self
    {
        $this->citeAs = $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getCopyright(): ?FHIRMarkdown
    {
        return $this->copyright;
    }

    public function setCopyright(string|FHIRMarkdown|null $value): self
    {
        $this->copyright = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getApprovalDate(): ?FHIRDate
    {
        return $this->approvalDate;
    }

    public function setApprovalDate(string|FHIRDate|null $value): self
    {
        $this->approvalDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getLastReviewDate(): ?FHIRDate
    {
        return $this->lastReviewDate;
    }

    public function setLastReviewDate(string|FHIRDate|null $value): self
    {
        $this->lastReviewDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getArtifact(): FHIRReference|FHIRCanonical|FHIRUri|null
    {
        return $this->artifact;
    }

    public function setArtifact(FHIRReference|FHIRCanonical|FHIRUri|null $value): self
    {
        $this->artifact = $value;

        return $this;
    }

    /**
     * @return FHIRArtifactAssessmentContent[]
     */
    public function getContent(): array
    {
        return $this->content;
    }

    public function setContent(?FHIRArtifactAssessmentContent ...$value): self
    {
        $this->content = array_filter($value);

        return $this;
    }

    public function addContent(?FHIRArtifactAssessmentContent ...$value): self
    {
        $this->content = array_filter(array_merge($this->content, $value));

        return $this;
    }

    public function getWorkflowStatus(): ?FHIRCode
    {
        return $this->workflowStatus;
    }

    public function setWorkflowStatus(string|FHIRCode|null $value): self
    {
        $this->workflowStatus = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDisposition(): ?FHIRCode
    {
        return $this->disposition;
    }

    public function setDisposition(string|FHIRCode|null $value): self
    {
        $this->disposition = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }
}
