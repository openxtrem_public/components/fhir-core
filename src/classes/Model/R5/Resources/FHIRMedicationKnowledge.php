<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledge Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMedicationKnowledgeInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMedicationKnowledgeCost;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMedicationKnowledgeDefinitional;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMedicationKnowledgeIndicationGuideline;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMedicationKnowledgeMedicineClassification;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMedicationKnowledgeMonitoringProgram;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMedicationKnowledgeMonograph;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMedicationKnowledgePackaging;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMedicationKnowledgeRegulatory;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMedicationKnowledgeRelatedMedicationKnowledge;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMedicationKnowledgeStorageGuideline;

class FHIRMedicationKnowledge extends FHIRDomainResource implements FHIRMedicationKnowledgeInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRReference $author = null;

    /** @var FHIRCodeableConcept[] */
    protected array $intendedJurisdiction = [];

    /** @var FHIRString[] */
    protected array $name = [];

    /** @var FHIRMedicationKnowledgeRelatedMedicationKnowledge[] */
    protected array $relatedMedicationKnowledge = [];

    /** @var FHIRReference[] */
    protected array $associatedMedication = [];

    /** @var FHIRCodeableConcept[] */
    protected array $productType = [];

    /** @var FHIRMedicationKnowledgeMonograph[] */
    protected array $monograph = [];
    protected ?FHIRMarkdown $preparationInstruction = null;

    /** @var FHIRMedicationKnowledgeCost[] */
    protected array $cost = [];

    /** @var FHIRMedicationKnowledgeMonitoringProgram[] */
    protected array $monitoringProgram = [];

    /** @var FHIRMedicationKnowledgeIndicationGuideline[] */
    protected array $indicationGuideline = [];

    /** @var FHIRMedicationKnowledgeMedicineClassification[] */
    protected array $medicineClassification = [];

    /** @var FHIRMedicationKnowledgePackaging[] */
    protected array $packaging = [];

    /** @var FHIRReference[] */
    protected array $clinicalUseIssue = [];

    /** @var FHIRMedicationKnowledgeStorageGuideline[] */
    protected array $storageGuideline = [];

    /** @var FHIRMedicationKnowledgeRegulatory[] */
    protected array $regulatory = [];
    protected ?FHIRMedicationKnowledgeDefinitional $definitional = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getAuthor(): ?FHIRReference
    {
        return $this->author;
    }

    public function setAuthor(?FHIRReference $value): self
    {
        $this->author = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getIntendedJurisdiction(): array
    {
        return $this->intendedJurisdiction;
    }

    public function setIntendedJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->intendedJurisdiction = array_filter($value);

        return $this;
    }

    public function addIntendedJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->intendedJurisdiction = array_filter(array_merge($this->intendedJurisdiction, $value));

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getName(): array
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null ...$value): self
    {
        $this->name = [];
        $this->addName(...$value);

        return $this;
    }

    public function addName(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->name = array_filter(array_merge($this->name, $values));

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeRelatedMedicationKnowledge[]
     */
    public function getRelatedMedicationKnowledge(): array
    {
        return $this->relatedMedicationKnowledge;
    }

    public function setRelatedMedicationKnowledge(?FHIRMedicationKnowledgeRelatedMedicationKnowledge ...$value): self
    {
        $this->relatedMedicationKnowledge = array_filter($value);

        return $this;
    }

    public function addRelatedMedicationKnowledge(?FHIRMedicationKnowledgeRelatedMedicationKnowledge ...$value): self
    {
        $this->relatedMedicationKnowledge = array_filter(array_merge($this->relatedMedicationKnowledge, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAssociatedMedication(): array
    {
        return $this->associatedMedication;
    }

    public function setAssociatedMedication(?FHIRReference ...$value): self
    {
        $this->associatedMedication = array_filter($value);

        return $this;
    }

    public function addAssociatedMedication(?FHIRReference ...$value): self
    {
        $this->associatedMedication = array_filter(array_merge($this->associatedMedication, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getProductType(): array
    {
        return $this->productType;
    }

    public function setProductType(?FHIRCodeableConcept ...$value): self
    {
        $this->productType = array_filter($value);

        return $this;
    }

    public function addProductType(?FHIRCodeableConcept ...$value): self
    {
        $this->productType = array_filter(array_merge($this->productType, $value));

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeMonograph[]
     */
    public function getMonograph(): array
    {
        return $this->monograph;
    }

    public function setMonograph(?FHIRMedicationKnowledgeMonograph ...$value): self
    {
        $this->monograph = array_filter($value);

        return $this;
    }

    public function addMonograph(?FHIRMedicationKnowledgeMonograph ...$value): self
    {
        $this->monograph = array_filter(array_merge($this->monograph, $value));

        return $this;
    }

    public function getPreparationInstruction(): ?FHIRMarkdown
    {
        return $this->preparationInstruction;
    }

    public function setPreparationInstruction(string|FHIRMarkdown|null $value): self
    {
        $this->preparationInstruction = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeCost[]
     */
    public function getCost(): array
    {
        return $this->cost;
    }

    public function setCost(?FHIRMedicationKnowledgeCost ...$value): self
    {
        $this->cost = array_filter($value);

        return $this;
    }

    public function addCost(?FHIRMedicationKnowledgeCost ...$value): self
    {
        $this->cost = array_filter(array_merge($this->cost, $value));

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeMonitoringProgram[]
     */
    public function getMonitoringProgram(): array
    {
        return $this->monitoringProgram;
    }

    public function setMonitoringProgram(?FHIRMedicationKnowledgeMonitoringProgram ...$value): self
    {
        $this->monitoringProgram = array_filter($value);

        return $this;
    }

    public function addMonitoringProgram(?FHIRMedicationKnowledgeMonitoringProgram ...$value): self
    {
        $this->monitoringProgram = array_filter(array_merge($this->monitoringProgram, $value));

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeIndicationGuideline[]
     */
    public function getIndicationGuideline(): array
    {
        return $this->indicationGuideline;
    }

    public function setIndicationGuideline(?FHIRMedicationKnowledgeIndicationGuideline ...$value): self
    {
        $this->indicationGuideline = array_filter($value);

        return $this;
    }

    public function addIndicationGuideline(?FHIRMedicationKnowledgeIndicationGuideline ...$value): self
    {
        $this->indicationGuideline = array_filter(array_merge($this->indicationGuideline, $value));

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeMedicineClassification[]
     */
    public function getMedicineClassification(): array
    {
        return $this->medicineClassification;
    }

    public function setMedicineClassification(?FHIRMedicationKnowledgeMedicineClassification ...$value): self
    {
        $this->medicineClassification = array_filter($value);

        return $this;
    }

    public function addMedicineClassification(?FHIRMedicationKnowledgeMedicineClassification ...$value): self
    {
        $this->medicineClassification = array_filter(array_merge($this->medicineClassification, $value));

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgePackaging[]
     */
    public function getPackaging(): array
    {
        return $this->packaging;
    }

    public function setPackaging(?FHIRMedicationKnowledgePackaging ...$value): self
    {
        $this->packaging = array_filter($value);

        return $this;
    }

    public function addPackaging(?FHIRMedicationKnowledgePackaging ...$value): self
    {
        $this->packaging = array_filter(array_merge($this->packaging, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getClinicalUseIssue(): array
    {
        return $this->clinicalUseIssue;
    }

    public function setClinicalUseIssue(?FHIRReference ...$value): self
    {
        $this->clinicalUseIssue = array_filter($value);

        return $this;
    }

    public function addClinicalUseIssue(?FHIRReference ...$value): self
    {
        $this->clinicalUseIssue = array_filter(array_merge($this->clinicalUseIssue, $value));

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeStorageGuideline[]
     */
    public function getStorageGuideline(): array
    {
        return $this->storageGuideline;
    }

    public function setStorageGuideline(?FHIRMedicationKnowledgeStorageGuideline ...$value): self
    {
        $this->storageGuideline = array_filter($value);

        return $this;
    }

    public function addStorageGuideline(?FHIRMedicationKnowledgeStorageGuideline ...$value): self
    {
        $this->storageGuideline = array_filter(array_merge($this->storageGuideline, $value));

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeRegulatory[]
     */
    public function getRegulatory(): array
    {
        return $this->regulatory;
    }

    public function setRegulatory(?FHIRMedicationKnowledgeRegulatory ...$value): self
    {
        $this->regulatory = array_filter($value);

        return $this;
    }

    public function addRegulatory(?FHIRMedicationKnowledgeRegulatory ...$value): self
    {
        $this->regulatory = array_filter(array_merge($this->regulatory, $value));

        return $this;
    }

    public function getDefinitional(): ?FHIRMedicationKnowledgeDefinitional
    {
        return $this->definitional;
    }

    public function setDefinitional(?FHIRMedicationKnowledgeDefinitional $value): self
    {
        $this->definitional = $value;

        return $this;
    }
}
