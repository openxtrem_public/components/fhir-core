<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR InventoryReport Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRInventoryReportInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRInventoryReportInventoryListing;

class FHIRInventoryReport extends FHIRDomainResource implements FHIRInventoryReportInterface
{
    public const RESOURCE_NAME = 'InventoryReport';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCode $countType = null;
    protected ?FHIRCodeableConcept $operationType = null;
    protected ?FHIRCodeableConcept $operationTypeReason = null;
    protected ?FHIRDateTime $reportedDateTime = null;
    protected ?FHIRReference $reporter = null;
    protected ?FHIRPeriod $reportingPeriod = null;

    /** @var FHIRInventoryReportInventoryListing[] */
    protected array $inventoryListing = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCountType(): ?FHIRCode
    {
        return $this->countType;
    }

    public function setCountType(string|FHIRCode|null $value): self
    {
        $this->countType = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getOperationType(): ?FHIRCodeableConcept
    {
        return $this->operationType;
    }

    public function setOperationType(?FHIRCodeableConcept $value): self
    {
        $this->operationType = $value;

        return $this;
    }

    public function getOperationTypeReason(): ?FHIRCodeableConcept
    {
        return $this->operationTypeReason;
    }

    public function setOperationTypeReason(?FHIRCodeableConcept $value): self
    {
        $this->operationTypeReason = $value;

        return $this;
    }

    public function getReportedDateTime(): ?FHIRDateTime
    {
        return $this->reportedDateTime;
    }

    public function setReportedDateTime(string|FHIRDateTime|null $value): self
    {
        $this->reportedDateTime = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getReporter(): ?FHIRReference
    {
        return $this->reporter;
    }

    public function setReporter(?FHIRReference $value): self
    {
        $this->reporter = $value;

        return $this;
    }

    public function getReportingPeriod(): ?FHIRPeriod
    {
        return $this->reportingPeriod;
    }

    public function setReportingPeriod(?FHIRPeriod $value): self
    {
        $this->reportingPeriod = $value;

        return $this;
    }

    /**
     * @return FHIRInventoryReportInventoryListing[]
     */
    public function getInventoryListing(): array
    {
        return $this->inventoryListing;
    }

    public function setInventoryListing(?FHIRInventoryReportInventoryListing ...$value): self
    {
        $this->inventoryListing = array_filter($value);

        return $this;
    }

    public function addInventoryListing(?FHIRInventoryReportInventoryListing ...$value): self
    {
        $this->inventoryListing = array_filter(array_merge($this->inventoryListing, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }
}
