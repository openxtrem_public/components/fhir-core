<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Consent Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRConsentInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRConsentPolicyBasis;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRConsentProvision;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRConsentVerification;

class FHIRConsent extends FHIRDomainResource implements FHIRConsentInterface
{
    public const RESOURCE_NAME = 'Consent';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRReference $subject = null;
    protected ?FHIRDate $date = null;
    protected ?FHIRPeriod $period = null;

    /** @var FHIRReference[] */
    protected array $grantor = [];

    /** @var FHIRReference[] */
    protected array $grantee = [];

    /** @var FHIRReference[] */
    protected array $manager = [];

    /** @var FHIRReference[] */
    protected array $controller = [];

    /** @var FHIRAttachment[] */
    protected array $sourceAttachment = [];

    /** @var FHIRReference[] */
    protected array $sourceReference = [];

    /** @var FHIRCodeableConcept[] */
    protected array $regulatoryBasis = [];
    protected ?FHIRConsentPolicyBasis $policyBasis = null;

    /** @var FHIRReference[] */
    protected array $policyText = [];

    /** @var FHIRConsentVerification[] */
    protected array $verification = [];
    protected ?FHIRCode $decision = null;

    /** @var FHIRConsentProvision[] */
    protected array $provision = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getDate(): ?FHIRDate
    {
        return $this->date;
    }

    public function setDate(string|FHIRDate|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getGrantor(): array
    {
        return $this->grantor;
    }

    public function setGrantor(?FHIRReference ...$value): self
    {
        $this->grantor = array_filter($value);

        return $this;
    }

    public function addGrantor(?FHIRReference ...$value): self
    {
        $this->grantor = array_filter(array_merge($this->grantor, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getGrantee(): array
    {
        return $this->grantee;
    }

    public function setGrantee(?FHIRReference ...$value): self
    {
        $this->grantee = array_filter($value);

        return $this;
    }

    public function addGrantee(?FHIRReference ...$value): self
    {
        $this->grantee = array_filter(array_merge($this->grantee, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getManager(): array
    {
        return $this->manager;
    }

    public function setManager(?FHIRReference ...$value): self
    {
        $this->manager = array_filter($value);

        return $this;
    }

    public function addManager(?FHIRReference ...$value): self
    {
        $this->manager = array_filter(array_merge($this->manager, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getController(): array
    {
        return $this->controller;
    }

    public function setController(?FHIRReference ...$value): self
    {
        $this->controller = array_filter($value);

        return $this;
    }

    public function addController(?FHIRReference ...$value): self
    {
        $this->controller = array_filter(array_merge($this->controller, $value));

        return $this;
    }

    /**
     * @return FHIRAttachment[]
     */
    public function getSourceAttachment(): array
    {
        return $this->sourceAttachment;
    }

    public function setSourceAttachment(?FHIRAttachment ...$value): self
    {
        $this->sourceAttachment = array_filter($value);

        return $this;
    }

    public function addSourceAttachment(?FHIRAttachment ...$value): self
    {
        $this->sourceAttachment = array_filter(array_merge($this->sourceAttachment, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSourceReference(): array
    {
        return $this->sourceReference;
    }

    public function setSourceReference(?FHIRReference ...$value): self
    {
        $this->sourceReference = array_filter($value);

        return $this;
    }

    public function addSourceReference(?FHIRReference ...$value): self
    {
        $this->sourceReference = array_filter(array_merge($this->sourceReference, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getRegulatoryBasis(): array
    {
        return $this->regulatoryBasis;
    }

    public function setRegulatoryBasis(?FHIRCodeableConcept ...$value): self
    {
        $this->regulatoryBasis = array_filter($value);

        return $this;
    }

    public function addRegulatoryBasis(?FHIRCodeableConcept ...$value): self
    {
        $this->regulatoryBasis = array_filter(array_merge($this->regulatoryBasis, $value));

        return $this;
    }

    public function getPolicyBasis(): ?FHIRConsentPolicyBasis
    {
        return $this->policyBasis;
    }

    public function setPolicyBasis(?FHIRConsentPolicyBasis $value): self
    {
        $this->policyBasis = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPolicyText(): array
    {
        return $this->policyText;
    }

    public function setPolicyText(?FHIRReference ...$value): self
    {
        $this->policyText = array_filter($value);

        return $this;
    }

    public function addPolicyText(?FHIRReference ...$value): self
    {
        $this->policyText = array_filter(array_merge($this->policyText, $value));

        return $this;
    }

    /**
     * @return FHIRConsentVerification[]
     */
    public function getVerification(): array
    {
        return $this->verification;
    }

    public function setVerification(?FHIRConsentVerification ...$value): self
    {
        $this->verification = array_filter($value);

        return $this;
    }

    public function addVerification(?FHIRConsentVerification ...$value): self
    {
        $this->verification = array_filter(array_merge($this->verification, $value));

        return $this;
    }

    public function getDecision(): ?FHIRCode
    {
        return $this->decision;
    }

    public function setDecision(string|FHIRCode|null $value): self
    {
        $this->decision = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRConsentProvision[]
     */
    public function getProvision(): array
    {
        return $this->provision;
    }

    public function setProvision(?FHIRConsentProvision ...$value): self
    {
        $this->provision = array_filter($value);

        return $this;
    }

    public function addProvision(?FHIRConsentProvision ...$value): self
    {
        $this->provision = array_filter(array_merge($this->provision, $value));

        return $this;
    }
}
