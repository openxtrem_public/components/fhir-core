<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationRequest Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMedicationRequestInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRDosage;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMedicationRequestDispenseRequest;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRMedicationRequestSubstitution;

class FHIRMedicationRequest extends FHIRDomainResource implements FHIRMedicationRequestInterface
{
    public const RESOURCE_NAME = 'MedicationRequest';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];
    protected ?FHIRReference $priorPrescription = null;
    protected ?FHIRIdentifier $groupIdentifier = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $statusReason = null;
    protected ?FHIRDateTime $statusChanged = null;
    protected ?FHIRCode $intent = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRCode $priority = null;
    protected ?FHIRBoolean $doNotPerform = null;
    protected ?FHIRCodeableReference $medication = null;
    protected ?FHIRReference $subject = null;

    /** @var FHIRReference[] */
    protected array $informationSource = [];
    protected ?FHIRReference $encounter = null;

    /** @var FHIRReference[] */
    protected array $supportingInformation = [];
    protected ?FHIRDateTime $authoredOn = null;
    protected ?FHIRReference $requester = null;
    protected ?FHIRBoolean $reported = null;
    protected ?FHIRCodeableConcept $performerType = null;

    /** @var FHIRReference[] */
    protected array $performer = [];

    /** @var FHIRCodeableReference[] */
    protected array $device = [];
    protected ?FHIRReference $recorder = null;

    /** @var FHIRCodeableReference[] */
    protected array $reason = [];
    protected ?FHIRCodeableConcept $courseOfTherapyType = null;

    /** @var FHIRReference[] */
    protected array $insurance = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];
    protected ?FHIRMarkdown $renderedDosageInstruction = null;
    protected ?FHIRPeriod $effectiveDosePeriod = null;

    /** @var FHIRDosage[] */
    protected array $dosageInstruction = [];
    protected ?FHIRMedicationRequestDispenseRequest $dispenseRequest = null;
    protected ?FHIRMedicationRequestSubstitution $substitution = null;

    /** @var FHIRReference[] */
    protected array $eventHistory = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    public function getPriorPrescription(): ?FHIRReference
    {
        return $this->priorPrescription;
    }

    public function setPriorPrescription(?FHIRReference $value): self
    {
        $this->priorPrescription = $value;

        return $this;
    }

    public function getGroupIdentifier(): ?FHIRIdentifier
    {
        return $this->groupIdentifier;
    }

    public function setGroupIdentifier(?FHIRIdentifier $value): self
    {
        $this->groupIdentifier = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getStatusReason(): ?FHIRCodeableConcept
    {
        return $this->statusReason;
    }

    public function setStatusReason(?FHIRCodeableConcept $value): self
    {
        $this->statusReason = $value;

        return $this;
    }

    public function getStatusChanged(): ?FHIRDateTime
    {
        return $this->statusChanged;
    }

    public function setStatusChanged(string|FHIRDateTime|null $value): self
    {
        $this->statusChanged = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getIntent(): ?FHIRCode
    {
        return $this->intent;
    }

    public function setIntent(string|FHIRCode|null $value): self
    {
        $this->intent = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getPriority(): ?FHIRCode
    {
        return $this->priority;
    }

    public function setPriority(string|FHIRCode|null $value): self
    {
        $this->priority = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDoNotPerform(): ?FHIRBoolean
    {
        return $this->doNotPerform;
    }

    public function setDoNotPerform(bool|FHIRBoolean|null $value): self
    {
        $this->doNotPerform = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getMedication(): ?FHIRCodeableReference
    {
        return $this->medication;
    }

    public function setMedication(?FHIRCodeableReference $value): self
    {
        $this->medication = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getInformationSource(): array
    {
        return $this->informationSource;
    }

    public function setInformationSource(?FHIRReference ...$value): self
    {
        $this->informationSource = array_filter($value);

        return $this;
    }

    public function addInformationSource(?FHIRReference ...$value): self
    {
        $this->informationSource = array_filter(array_merge($this->informationSource, $value));

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupportingInformation(): array
    {
        return $this->supportingInformation;
    }

    public function setSupportingInformation(?FHIRReference ...$value): self
    {
        $this->supportingInformation = array_filter($value);

        return $this;
    }

    public function addSupportingInformation(?FHIRReference ...$value): self
    {
        $this->supportingInformation = array_filter(array_merge($this->supportingInformation, $value));

        return $this;
    }

    public function getAuthoredOn(): ?FHIRDateTime
    {
        return $this->authoredOn;
    }

    public function setAuthoredOn(string|FHIRDateTime|null $value): self
    {
        $this->authoredOn = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getRequester(): ?FHIRReference
    {
        return $this->requester;
    }

    public function setRequester(?FHIRReference $value): self
    {
        $this->requester = $value;

        return $this;
    }

    public function getReported(): ?FHIRBoolean
    {
        return $this->reported;
    }

    public function setReported(bool|FHIRBoolean|null $value): self
    {
        $this->reported = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getPerformerType(): ?FHIRCodeableConcept
    {
        return $this->performerType;
    }

    public function setPerformerType(?FHIRCodeableConcept $value): self
    {
        $this->performerType = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPerformer(): array
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRReference ...$value): self
    {
        $this->performer = array_filter($value);

        return $this;
    }

    public function addPerformer(?FHIRReference ...$value): self
    {
        $this->performer = array_filter(array_merge($this->performer, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getDevice(): array
    {
        return $this->device;
    }

    public function setDevice(?FHIRCodeableReference ...$value): self
    {
        $this->device = array_filter($value);

        return $this;
    }

    public function addDevice(?FHIRCodeableReference ...$value): self
    {
        $this->device = array_filter(array_merge($this->device, $value));

        return $this;
    }

    public function getRecorder(): ?FHIRReference
    {
        return $this->recorder;
    }

    public function setRecorder(?FHIRReference $value): self
    {
        $this->recorder = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getReason(): array
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableReference ...$value): self
    {
        $this->reason = array_filter($value);

        return $this;
    }

    public function addReason(?FHIRCodeableReference ...$value): self
    {
        $this->reason = array_filter(array_merge($this->reason, $value));

        return $this;
    }

    public function getCourseOfTherapyType(): ?FHIRCodeableConcept
    {
        return $this->courseOfTherapyType;
    }

    public function setCourseOfTherapyType(?FHIRCodeableConcept $value): self
    {
        $this->courseOfTherapyType = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getInsurance(): array
    {
        return $this->insurance;
    }

    public function setInsurance(?FHIRReference ...$value): self
    {
        $this->insurance = array_filter($value);

        return $this;
    }

    public function addInsurance(?FHIRReference ...$value): self
    {
        $this->insurance = array_filter(array_merge($this->insurance, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    public function getRenderedDosageInstruction(): ?FHIRMarkdown
    {
        return $this->renderedDosageInstruction;
    }

    public function setRenderedDosageInstruction(string|FHIRMarkdown|null $value): self
    {
        $this->renderedDosageInstruction = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getEffectiveDosePeriod(): ?FHIRPeriod
    {
        return $this->effectiveDosePeriod;
    }

    public function setEffectiveDosePeriod(?FHIRPeriod $value): self
    {
        $this->effectiveDosePeriod = $value;

        return $this;
    }

    /**
     * @return FHIRDosage[]
     */
    public function getDosageInstruction(): array
    {
        return $this->dosageInstruction;
    }

    public function setDosageInstruction(?FHIRDosage ...$value): self
    {
        $this->dosageInstruction = array_filter($value);

        return $this;
    }

    public function addDosageInstruction(?FHIRDosage ...$value): self
    {
        $this->dosageInstruction = array_filter(array_merge($this->dosageInstruction, $value));

        return $this;
    }

    public function getDispenseRequest(): ?FHIRMedicationRequestDispenseRequest
    {
        return $this->dispenseRequest;
    }

    public function setDispenseRequest(?FHIRMedicationRequestDispenseRequest $value): self
    {
        $this->dispenseRequest = $value;

        return $this;
    }

    public function getSubstitution(): ?FHIRMedicationRequestSubstitution
    {
        return $this->substitution;
    }

    public function setSubstitution(?FHIRMedicationRequestSubstitution $value): self
    {
        $this->substitution = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEventHistory(): array
    {
        return $this->eventHistory;
    }

    public function setEventHistory(?FHIRReference ...$value): self
    {
        $this->eventHistory = array_filter($value);

        return $this;
    }

    public function addEventHistory(?FHIRReference ...$value): self
    {
        $this->eventHistory = array_filter(array_merge($this->eventHistory, $value));

        return $this;
    }
}
