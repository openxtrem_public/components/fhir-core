<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Appointment Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRAppointmentInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRVirtualServiceDetail;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRAppointmentParticipant;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRAppointmentRecurrenceTemplate;

class FHIRAppointment extends FHIRDomainResource implements FHIRAppointmentInterface
{
    public const RESOURCE_NAME = 'Appointment';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $cancellationReason = null;

    /** @var FHIRCodeableConcept[] */
    protected array $class = [];

    /** @var FHIRCodeableConcept[] */
    protected array $serviceCategory = [];

    /** @var FHIRCodeableReference[] */
    protected array $serviceType = [];

    /** @var FHIRCodeableConcept[] */
    protected array $specialty = [];
    protected ?FHIRCodeableConcept $appointmentType = null;

    /** @var FHIRCodeableReference[] */
    protected array $reason = [];
    protected ?FHIRCodeableConcept $priority = null;
    protected ?FHIRString $description = null;

    /** @var FHIRReference[] */
    protected array $replaces = [];

    /** @var FHIRVirtualServiceDetail[] */
    protected array $virtualService = [];

    /** @var FHIRReference[] */
    protected array $supportingInformation = [];
    protected ?FHIRReference $previousAppointment = null;
    protected ?FHIRReference $originatingAppointment = null;
    protected ?FHIRInstant $start = null;
    protected ?FHIRInstant $end = null;
    protected ?FHIRPositiveInt $minutesDuration = null;

    /** @var FHIRPeriod[] */
    protected array $requestedPeriod = [];

    /** @var FHIRReference[] */
    protected array $slot = [];

    /** @var FHIRReference[] */
    protected array $account = [];
    protected ?FHIRDateTime $created = null;
    protected ?FHIRDateTime $cancellationDate = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRCodeableReference[] */
    protected array $patientInstruction = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];
    protected ?FHIRReference $subject = null;

    /** @var FHIRAppointmentParticipant[] */
    protected array $participant = [];
    protected ?FHIRPositiveInt $recurrenceId = null;
    protected ?FHIRBoolean $occurrenceChanged = null;

    /** @var FHIRAppointmentRecurrenceTemplate[] */
    protected array $recurrenceTemplate = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCancellationReason(): ?FHIRCodeableConcept
    {
        return $this->cancellationReason;
    }

    public function setCancellationReason(?FHIRCodeableConcept $value): self
    {
        $this->cancellationReason = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getClass(): array
    {
        return $this->class;
    }

    public function setClass(?FHIRCodeableConcept ...$value): self
    {
        $this->class = array_filter($value);

        return $this;
    }

    public function addClass(?FHIRCodeableConcept ...$value): self
    {
        $this->class = array_filter(array_merge($this->class, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getServiceCategory(): array
    {
        return $this->serviceCategory;
    }

    public function setServiceCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->serviceCategory = array_filter($value);

        return $this;
    }

    public function addServiceCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->serviceCategory = array_filter(array_merge($this->serviceCategory, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getServiceType(): array
    {
        return $this->serviceType;
    }

    public function setServiceType(?FHIRCodeableReference ...$value): self
    {
        $this->serviceType = array_filter($value);

        return $this;
    }

    public function addServiceType(?FHIRCodeableReference ...$value): self
    {
        $this->serviceType = array_filter(array_merge($this->serviceType, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSpecialty(): array
    {
        return $this->specialty;
    }

    public function setSpecialty(?FHIRCodeableConcept ...$value): self
    {
        $this->specialty = array_filter($value);

        return $this;
    }

    public function addSpecialty(?FHIRCodeableConcept ...$value): self
    {
        $this->specialty = array_filter(array_merge($this->specialty, $value));

        return $this;
    }

    public function getAppointmentType(): ?FHIRCodeableConcept
    {
        return $this->appointmentType;
    }

    public function setAppointmentType(?FHIRCodeableConcept $value): self
    {
        $this->appointmentType = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getReason(): array
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableReference ...$value): self
    {
        $this->reason = array_filter($value);

        return $this;
    }

    public function addReason(?FHIRCodeableReference ...$value): self
    {
        $this->reason = array_filter(array_merge($this->reason, $value));

        return $this;
    }

    public function getPriority(): ?FHIRCodeableConcept
    {
        return $this->priority;
    }

    public function setPriority(?FHIRCodeableConcept $value): self
    {
        $this->priority = $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReplaces(): array
    {
        return $this->replaces;
    }

    public function setReplaces(?FHIRReference ...$value): self
    {
        $this->replaces = array_filter($value);

        return $this;
    }

    public function addReplaces(?FHIRReference ...$value): self
    {
        $this->replaces = array_filter(array_merge($this->replaces, $value));

        return $this;
    }

    /**
     * @return FHIRVirtualServiceDetail[]
     */
    public function getVirtualService(): array
    {
        return $this->virtualService;
    }

    public function setVirtualService(?FHIRVirtualServiceDetail ...$value): self
    {
        $this->virtualService = array_filter($value);

        return $this;
    }

    public function addVirtualService(?FHIRVirtualServiceDetail ...$value): self
    {
        $this->virtualService = array_filter(array_merge($this->virtualService, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupportingInformation(): array
    {
        return $this->supportingInformation;
    }

    public function setSupportingInformation(?FHIRReference ...$value): self
    {
        $this->supportingInformation = array_filter($value);

        return $this;
    }

    public function addSupportingInformation(?FHIRReference ...$value): self
    {
        $this->supportingInformation = array_filter(array_merge($this->supportingInformation, $value));

        return $this;
    }

    public function getPreviousAppointment(): ?FHIRReference
    {
        return $this->previousAppointment;
    }

    public function setPreviousAppointment(?FHIRReference $value): self
    {
        $this->previousAppointment = $value;

        return $this;
    }

    public function getOriginatingAppointment(): ?FHIRReference
    {
        return $this->originatingAppointment;
    }

    public function setOriginatingAppointment(?FHIRReference $value): self
    {
        $this->originatingAppointment = $value;

        return $this;
    }

    public function getStart(): ?FHIRInstant
    {
        return $this->start;
    }

    public function setStart(string|FHIRInstant|null $value): self
    {
        $this->start = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    public function getEnd(): ?FHIRInstant
    {
        return $this->end;
    }

    public function setEnd(string|FHIRInstant|null $value): self
    {
        $this->end = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    public function getMinutesDuration(): ?FHIRPositiveInt
    {
        return $this->minutesDuration;
    }

    public function setMinutesDuration(int|FHIRPositiveInt|null $value): self
    {
        $this->minutesDuration = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRPeriod[]
     */
    public function getRequestedPeriod(): array
    {
        return $this->requestedPeriod;
    }

    public function setRequestedPeriod(?FHIRPeriod ...$value): self
    {
        $this->requestedPeriod = array_filter($value);

        return $this;
    }

    public function addRequestedPeriod(?FHIRPeriod ...$value): self
    {
        $this->requestedPeriod = array_filter(array_merge($this->requestedPeriod, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSlot(): array
    {
        return $this->slot;
    }

    public function setSlot(?FHIRReference ...$value): self
    {
        $this->slot = array_filter($value);

        return $this;
    }

    public function addSlot(?FHIRReference ...$value): self
    {
        $this->slot = array_filter(array_merge($this->slot, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAccount(): array
    {
        return $this->account;
    }

    public function setAccount(?FHIRReference ...$value): self
    {
        $this->account = array_filter($value);

        return $this;
    }

    public function addAccount(?FHIRReference ...$value): self
    {
        $this->account = array_filter(array_merge($this->account, $value));

        return $this;
    }

    public function getCreated(): ?FHIRDateTime
    {
        return $this->created;
    }

    public function setCreated(string|FHIRDateTime|null $value): self
    {
        $this->created = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getCancellationDate(): ?FHIRDateTime
    {
        return $this->cancellationDate;
    }

    public function setCancellationDate(string|FHIRDateTime|null $value): self
    {
        $this->cancellationDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getPatientInstruction(): array
    {
        return $this->patientInstruction;
    }

    public function setPatientInstruction(?FHIRCodeableReference ...$value): self
    {
        $this->patientInstruction = array_filter($value);

        return $this;
    }

    public function addPatientInstruction(?FHIRCodeableReference ...$value): self
    {
        $this->patientInstruction = array_filter(array_merge($this->patientInstruction, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    /**
     * @return FHIRAppointmentParticipant[]
     */
    public function getParticipant(): array
    {
        return $this->participant;
    }

    public function setParticipant(?FHIRAppointmentParticipant ...$value): self
    {
        $this->participant = array_filter($value);

        return $this;
    }

    public function addParticipant(?FHIRAppointmentParticipant ...$value): self
    {
        $this->participant = array_filter(array_merge($this->participant, $value));

        return $this;
    }

    public function getRecurrenceId(): ?FHIRPositiveInt
    {
        return $this->recurrenceId;
    }

    public function setRecurrenceId(int|FHIRPositiveInt|null $value): self
    {
        $this->recurrenceId = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getOccurrenceChanged(): ?FHIRBoolean
    {
        return $this->occurrenceChanged;
    }

    public function setOccurrenceChanged(bool|FHIRBoolean|null $value): self
    {
        $this->occurrenceChanged = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAppointmentRecurrenceTemplate[]
     */
    public function getRecurrenceTemplate(): array
    {
        return $this->recurrenceTemplate;
    }

    public function setRecurrenceTemplate(?FHIRAppointmentRecurrenceTemplate ...$value): self
    {
        $this->recurrenceTemplate = array_filter($value);

        return $this;
    }

    public function addRecurrenceTemplate(?FHIRAppointmentRecurrenceTemplate ...$value): self
    {
        $this->recurrenceTemplate = array_filter(array_merge($this->recurrenceTemplate, $value));

        return $this;
    }
}
