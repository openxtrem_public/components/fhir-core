<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DiagnosticReport Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRDiagnosticReportInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDiagnosticReportMedia;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRDiagnosticReportSupportingInfo;

class FHIRDiagnosticReport extends FHIRDomainResource implements FHIRDiagnosticReportInterface
{
    public const RESOURCE_NAME = 'DiagnosticReport';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];
    protected ?FHIRCode $status = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $encounter = null;
    protected FHIRDateTime|FHIRPeriod|null $effective = null;
    protected ?FHIRInstant $issued = null;

    /** @var FHIRReference[] */
    protected array $performer = [];

    /** @var FHIRReference[] */
    protected array $resultsInterpreter = [];

    /** @var FHIRReference[] */
    protected array $specimen = [];

    /** @var FHIRReference[] */
    protected array $result = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRReference[] */
    protected array $study = [];

    /** @var FHIRDiagnosticReportSupportingInfo[] */
    protected array $supportingInfo = [];

    /** @var FHIRDiagnosticReportMedia[] */
    protected array $media = [];
    protected ?FHIRReference $composition = null;
    protected ?FHIRMarkdown $conclusion = null;

    /** @var FHIRCodeableConcept[] */
    protected array $conclusionCode = [];

    /** @var FHIRAttachment[] */
    protected array $presentedForm = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getEffective(): FHIRDateTime|FHIRPeriod|null
    {
        return $this->effective;
    }

    public function setEffective(FHIRDateTime|FHIRPeriod|null $value): self
    {
        $this->effective = $value;

        return $this;
    }

    public function getIssued(): ?FHIRInstant
    {
        return $this->issued;
    }

    public function setIssued(string|FHIRInstant|null $value): self
    {
        $this->issued = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPerformer(): array
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRReference ...$value): self
    {
        $this->performer = array_filter($value);

        return $this;
    }

    public function addPerformer(?FHIRReference ...$value): self
    {
        $this->performer = array_filter(array_merge($this->performer, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getResultsInterpreter(): array
    {
        return $this->resultsInterpreter;
    }

    public function setResultsInterpreter(?FHIRReference ...$value): self
    {
        $this->resultsInterpreter = array_filter($value);

        return $this;
    }

    public function addResultsInterpreter(?FHIRReference ...$value): self
    {
        $this->resultsInterpreter = array_filter(array_merge($this->resultsInterpreter, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSpecimen(): array
    {
        return $this->specimen;
    }

    public function setSpecimen(?FHIRReference ...$value): self
    {
        $this->specimen = array_filter($value);

        return $this;
    }

    public function addSpecimen(?FHIRReference ...$value): self
    {
        $this->specimen = array_filter(array_merge($this->specimen, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getResult(): array
    {
        return $this->result;
    }

    public function setResult(?FHIRReference ...$value): self
    {
        $this->result = array_filter($value);

        return $this;
    }

    public function addResult(?FHIRReference ...$value): self
    {
        $this->result = array_filter(array_merge($this->result, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getStudy(): array
    {
        return $this->study;
    }

    public function setStudy(?FHIRReference ...$value): self
    {
        $this->study = array_filter($value);

        return $this;
    }

    public function addStudy(?FHIRReference ...$value): self
    {
        $this->study = array_filter(array_merge($this->study, $value));

        return $this;
    }

    /**
     * @return FHIRDiagnosticReportSupportingInfo[]
     */
    public function getSupportingInfo(): array
    {
        return $this->supportingInfo;
    }

    public function setSupportingInfo(?FHIRDiagnosticReportSupportingInfo ...$value): self
    {
        $this->supportingInfo = array_filter($value);

        return $this;
    }

    public function addSupportingInfo(?FHIRDiagnosticReportSupportingInfo ...$value): self
    {
        $this->supportingInfo = array_filter(array_merge($this->supportingInfo, $value));

        return $this;
    }

    /**
     * @return FHIRDiagnosticReportMedia[]
     */
    public function getMedia(): array
    {
        return $this->media;
    }

    public function setMedia(?FHIRDiagnosticReportMedia ...$value): self
    {
        $this->media = array_filter($value);

        return $this;
    }

    public function addMedia(?FHIRDiagnosticReportMedia ...$value): self
    {
        $this->media = array_filter(array_merge($this->media, $value));

        return $this;
    }

    public function getComposition(): ?FHIRReference
    {
        return $this->composition;
    }

    public function setComposition(?FHIRReference $value): self
    {
        $this->composition = $value;

        return $this;
    }

    public function getConclusion(): ?FHIRMarkdown
    {
        return $this->conclusion;
    }

    public function setConclusion(string|FHIRMarkdown|null $value): self
    {
        $this->conclusion = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getConclusionCode(): array
    {
        return $this->conclusionCode;
    }

    public function setConclusionCode(?FHIRCodeableConcept ...$value): self
    {
        $this->conclusionCode = array_filter($value);

        return $this;
    }

    public function addConclusionCode(?FHIRCodeableConcept ...$value): self
    {
        $this->conclusionCode = array_filter(array_merge($this->conclusionCode, $value));

        return $this;
    }

    /**
     * @return FHIRAttachment[]
     */
    public function getPresentedForm(): array
    {
        return $this->presentedForm;
    }

    public function setPresentedForm(?FHIRAttachment ...$value): self
    {
        $this->presentedForm = array_filter($value);

        return $this;
    }

    public function addPresentedForm(?FHIRAttachment ...$value): self
    {
        $this->presentedForm = array_filter(array_merge($this->presentedForm, $value));

        return $this;
    }
}
