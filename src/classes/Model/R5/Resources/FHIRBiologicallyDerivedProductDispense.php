<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR BiologicallyDerivedProductDispense Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRBiologicallyDerivedProductDispenseInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRBiologicallyDerivedProductDispensePerformer;

class FHIRBiologicallyDerivedProductDispense extends FHIRDomainResource implements FHIRBiologicallyDerivedProductDispenseInterface
{
    public const RESOURCE_NAME = 'BiologicallyDerivedProductDispense';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];

    /** @var FHIRReference[] */
    protected array $partOf = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $originRelationshipType = null;
    protected ?FHIRReference $product = null;
    protected ?FHIRReference $patient = null;
    protected ?FHIRCodeableConcept $matchStatus = null;

    /** @var FHIRBiologicallyDerivedProductDispensePerformer[] */
    protected array $performer = [];
    protected ?FHIRReference $location = null;
    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRDateTime $preparedDate = null;
    protected ?FHIRDateTime $whenHandedOver = null;
    protected ?FHIRReference $destination = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];
    protected ?FHIRString $usageInstruction = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPartOf(): array
    {
        return $this->partOf;
    }

    public function setPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter($value);

        return $this;
    }

    public function addPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter(array_merge($this->partOf, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getOriginRelationshipType(): ?FHIRCodeableConcept
    {
        return $this->originRelationshipType;
    }

    public function setOriginRelationshipType(?FHIRCodeableConcept $value): self
    {
        $this->originRelationshipType = $value;

        return $this;
    }

    public function getProduct(): ?FHIRReference
    {
        return $this->product;
    }

    public function setProduct(?FHIRReference $value): self
    {
        $this->product = $value;

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getMatchStatus(): ?FHIRCodeableConcept
    {
        return $this->matchStatus;
    }

    public function setMatchStatus(?FHIRCodeableConcept $value): self
    {
        $this->matchStatus = $value;

        return $this;
    }

    /**
     * @return FHIRBiologicallyDerivedProductDispensePerformer[]
     */
    public function getPerformer(): array
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRBiologicallyDerivedProductDispensePerformer ...$value): self
    {
        $this->performer = array_filter($value);

        return $this;
    }

    public function addPerformer(?FHIRBiologicallyDerivedProductDispensePerformer ...$value): self
    {
        $this->performer = array_filter(array_merge($this->performer, $value));

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getPreparedDate(): ?FHIRDateTime
    {
        return $this->preparedDate;
    }

    public function setPreparedDate(string|FHIRDateTime|null $value): self
    {
        $this->preparedDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getWhenHandedOver(): ?FHIRDateTime
    {
        return $this->whenHandedOver;
    }

    public function setWhenHandedOver(string|FHIRDateTime|null $value): self
    {
        $this->whenHandedOver = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getDestination(): ?FHIRReference
    {
        return $this->destination;
    }

    public function setDestination(?FHIRReference $value): self
    {
        $this->destination = $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    public function getUsageInstruction(): ?FHIRString
    {
        return $this->usageInstruction;
    }

    public function setUsageInstruction(string|FHIRString|null $value): self
    {
        $this->usageInstruction = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
