<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImmunizationRecommendation Resource
 */

namespace Ox\Components\FHIRCore\Model\R5\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRImmunizationRecommendationInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Resources\Backbone\FHIRImmunizationRecommendationRecommendation;

class FHIRImmunizationRecommendation extends FHIRDomainResource implements FHIRImmunizationRecommendationInterface
{
    public const RESOURCE_NAME = 'ImmunizationRecommendation';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRReference $patient = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRReference $authority = null;

    /** @var FHIRImmunizationRecommendationRecommendation[] */
    protected array $recommendation = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getAuthority(): ?FHIRReference
    {
        return $this->authority;
    }

    public function setAuthority(?FHIRReference $value): self
    {
        $this->authority = $value;

        return $this;
    }

    /**
     * @return FHIRImmunizationRecommendationRecommendation[]
     */
    public function getRecommendation(): array
    {
        return $this->recommendation;
    }

    public function setRecommendation(?FHIRImmunizationRecommendationRecommendation ...$value): self
    {
        $this->recommendation = array_filter($value);

        return $this;
    }

    public function addRecommendation(?FHIRImmunizationRecommendationRecommendation ...$value): self
    {
        $this->recommendation = array_filter(array_merge($this->recommendation, $value));

        return $this;
    }
}
