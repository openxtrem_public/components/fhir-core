<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR integer64 Primitive
 */

namespace Ox\Components\FHIRCore\Model\R5\Datatypes;

use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRInteger64Interface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPrimitiveType;

class FHIRInteger64 extends FHIRPrimitiveType implements FHIRInteger64Interface
{
    public const RESOURCE_NAME = 'integer64';

    protected ?int $value = null;

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(?int $value): self
    {
        $this->value = $value;

        return $this;
    }
}
