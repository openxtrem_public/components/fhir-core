<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR integer Primitive
 */

namespace Ox\Components\FHIRCore\Model\R5\Datatypes;

use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRIntegerInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPrimitiveType;

class FHIRInteger extends FHIRPrimitiveType implements FHIRIntegerInterface
{
    public const RESOURCE_NAME = 'integer';

    protected ?int $value = null;

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(?int $value): self
    {
        $this->value = $value;

        return $this;
    }
}
