<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR code Primitive
 */

namespace Ox\Components\FHIRCore\Model\R5\Datatypes;

use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRCodeInterface;

class FHIRCode extends FHIRString implements FHIRCodeInterface
{
    public const RESOURCE_NAME = 'code';
}
