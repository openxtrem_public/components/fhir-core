<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Element Complex
 */

namespace Ox\Components\FHIRCore\Model\R5\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRElementInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

abstract class FHIRElement extends FHIRBase implements FHIRElementInterface
{
    public const RESOURCE_NAME = 'Element';

    protected ?FHIRString $id = null;

    /** @var FHIRExtension[] */
    protected array $extension = [];

    public function getId(): ?FHIRString
    {
        return $this->id;
    }

    public function setId(string|FHIRString|null $value): self
    {
        $this->id = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRExtension[]
     */
    public function getExtension(): array
    {
        return $this->extension;
    }

    public function setExtension(?FHIRExtension ...$value): self
    {
        $this->extension = array_filter($value);

        return $this;
    }

    public function addExtension(?FHIRExtension ...$value): self
    {
        $this->extension = array_filter(array_merge($this->extension, $value));

        return $this;
    }

    public function getExtensionFromUrls(string ...$urls): ?FHIRExtension
    {
        foreach ($this->extension ?? [] as $extension) {
            if ($extension = $extension->getFromUrls(...$urls)) {
                return $extension;
            }
        }

        return null;
    }
}
