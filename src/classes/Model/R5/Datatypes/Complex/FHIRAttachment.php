<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Attachment Complex
 */

namespace Ox\Components\FHIRCore\Model\R5\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRAttachmentInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBase64Binary;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInteger64;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUrl;

class FHIRAttachment extends FHIRDataType implements FHIRAttachmentInterface
{
    public const RESOURCE_NAME = 'Attachment';

    protected ?FHIRCode $contentType = null;
    protected ?FHIRCode $language = null;
    protected ?FHIRBase64Binary $data = null;
    protected ?FHIRUrl $url = null;
    protected ?FHIRInteger64 $size = null;
    protected ?FHIRBase64Binary $hash = null;
    protected ?FHIRString $title = null;
    protected ?FHIRDateTime $creation = null;
    protected ?FHIRPositiveInt $height = null;
    protected ?FHIRPositiveInt $width = null;
    protected ?FHIRPositiveInt $frames = null;
    protected ?FHIRDecimal $duration = null;
    protected ?FHIRPositiveInt $pages = null;

    public function getContentType(): ?FHIRCode
    {
        return $this->contentType;
    }

    public function setContentType(string|FHIRCode|null $value): self
    {
        $this->contentType = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getLanguage(): ?FHIRCode
    {
        return $this->language;
    }

    public function setLanguage(string|FHIRCode|null $value): self
    {
        $this->language = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getData(): ?FHIRBase64Binary
    {
        return $this->data;
    }

    public function setData(string|FHIRBase64Binary|null $value): self
    {
        $this->data = is_string($value) ? (new FHIRBase64Binary())->setValue($value) : $value;

        return $this;
    }

    public function getUrl(): ?FHIRUrl
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUrl|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUrl())->setValue($value) : $value;

        return $this;
    }

    public function getSize(): ?FHIRInteger64
    {
        return $this->size;
    }

    public function setSize(int|FHIRInteger64|null $value): self
    {
        $this->size = is_int($value) ? (new FHIRInteger64())->setValue($value) : $value;

        return $this;
    }

    public function getHash(): ?FHIRBase64Binary
    {
        return $this->hash;
    }

    public function setHash(string|FHIRBase64Binary|null $value): self
    {
        $this->hash = is_string($value) ? (new FHIRBase64Binary())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCreation(): ?FHIRDateTime
    {
        return $this->creation;
    }

    public function setCreation(string|FHIRDateTime|null $value): self
    {
        $this->creation = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getHeight(): ?FHIRPositiveInt
    {
        return $this->height;
    }

    public function setHeight(int|FHIRPositiveInt|null $value): self
    {
        $this->height = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getWidth(): ?FHIRPositiveInt
    {
        return $this->width;
    }

    public function setWidth(int|FHIRPositiveInt|null $value): self
    {
        $this->width = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getFrames(): ?FHIRPositiveInt
    {
        return $this->frames;
    }

    public function setFrames(int|FHIRPositiveInt|null $value): self
    {
        $this->frames = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getDuration(): ?FHIRDecimal
    {
        return $this->duration;
    }

    public function setDuration(float|FHIRDecimal|null $value): self
    {
        $this->duration = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getPages(): ?FHIRPositiveInt
    {
        return $this->pages;
    }

    public function setPages(int|FHIRPositiveInt|null $value): self
    {
        $this->pages = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }
}
