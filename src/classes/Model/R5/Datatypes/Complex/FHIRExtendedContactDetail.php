<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExtendedContactDetail Complex
 */

namespace Ox\Components\FHIRCore\Model\R5\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRExtendedContactDetailInterface;

class FHIRExtendedContactDetail extends FHIRDataType implements FHIRExtendedContactDetailInterface
{
    public const RESOURCE_NAME = 'ExtendedContactDetail';

    protected ?FHIRCodeableConcept $purpose = null;

    /** @var FHIRHumanName[] */
    protected array $name = [];

    /** @var FHIRContactPoint[] */
    protected array $telecom = [];
    protected ?FHIRAddress $address = null;
    protected ?FHIRReference $organization = null;
    protected ?FHIRPeriod $period = null;

    public function getPurpose(): ?FHIRCodeableConcept
    {
        return $this->purpose;
    }

    public function setPurpose(?FHIRCodeableConcept $value): self
    {
        $this->purpose = $value;

        return $this;
    }

    /**
     * @return FHIRHumanName[]
     */
    public function getName(): array
    {
        return $this->name;
    }

    public function setName(?FHIRHumanName ...$value): self
    {
        $this->name = array_filter($value);

        return $this;
    }

    public function addName(?FHIRHumanName ...$value): self
    {
        $this->name = array_filter(array_merge($this->name, $value));

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getTelecom(): array
    {
        return $this->telecom;
    }

    public function setTelecom(?FHIRContactPoint ...$value): self
    {
        $this->telecom = array_filter($value);

        return $this;
    }

    public function addTelecom(?FHIRContactPoint ...$value): self
    {
        $this->telecom = array_filter(array_merge($this->telecom, $value));

        return $this;
    }

    public function getAddress(): ?FHIRAddress
    {
        return $this->address;
    }

    public function setAddress(?FHIRAddress $value): self
    {
        $this->address = $value;

        return $this;
    }

    public function getOrganization(): ?FHIRReference
    {
        return $this->organization;
    }

    public function setOrganization(?FHIRReference $value): self
    {
        $this->organization = $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }
}
