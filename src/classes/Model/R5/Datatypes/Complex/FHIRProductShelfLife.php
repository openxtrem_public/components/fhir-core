<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ProductShelfLife Complex
 */

namespace Ox\Components\FHIRCore\Model\R5\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRProductShelfLifeInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRProductShelfLife extends FHIRBackboneType implements FHIRProductShelfLifeInterface
{
    public const RESOURCE_NAME = 'ProductShelfLife';

    protected ?FHIRCodeableConcept $type = null;
    protected FHIRDuration|FHIRString|null $period = null;

    /** @var FHIRCodeableConcept[] */
    protected array $specialPrecautionsForStorage = [];

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getPeriod(): FHIRDuration|FHIRString|null
    {
        return $this->period;
    }

    public function setPeriod(FHIRDuration|FHIRString|null $value): self
    {
        $this->period = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSpecialPrecautionsForStorage(): array
    {
        return $this->specialPrecautionsForStorage;
    }

    public function setSpecialPrecautionsForStorage(?FHIRCodeableConcept ...$value): self
    {
        $this->specialPrecautionsForStorage = array_filter($value);

        return $this;
    }

    public function addSpecialPrecautionsForStorage(?FHIRCodeableConcept ...$value): self
    {
        $this->specialPrecautionsForStorage = array_filter(array_merge($this->specialPrecautionsForStorage, $value));

        return $this;
    }
}
