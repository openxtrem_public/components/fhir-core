<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Coding Complex
 */

namespace Ox\Components\FHIRCore\Model\R5\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRCodingInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;

class FHIRCoding extends FHIRDataType implements FHIRCodingInterface
{
    public const RESOURCE_NAME = 'Coding';

    protected ?FHIRUri $system = null;
    protected ?FHIRString $version = null;
    protected ?FHIRCode $code = null;
    protected ?FHIRString $display = null;
    protected ?FHIRBoolean $userSelected = null;

    public function getSystem(): ?FHIRUri
    {
        return $this->system;
    }

    public function setSystem(string|FHIRUri|null $value): self
    {
        $this->system = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDisplay(): ?FHIRString
    {
        return $this->display;
    }

    public function setDisplay(string|FHIRString|null $value): self
    {
        $this->display = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getUserSelected(): ?FHIRBoolean
    {
        return $this->userSelected;
    }

    public function setUserSelected(bool|FHIRBoolean|null $value): self
    {
        $this->userSelected = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function isMatch(string $system, string $code): bool
    {
        if (!$this->code || !$this->system) {
            return false;
        }

        return $this->system->isMatch($system) && $this->code->getValue() === $code;
    }
}
