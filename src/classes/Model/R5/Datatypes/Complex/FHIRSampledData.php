<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SampledData Complex
 */

namespace Ox\Components\FHIRCore\Model\R5\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRSampledDataInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRSampledData extends FHIRDataType implements FHIRSampledDataInterface
{
    public const RESOURCE_NAME = 'SampledData';

    protected ?FHIRQuantity $origin = null;
    protected ?FHIRDecimal $interval = null;
    protected ?FHIRCode $intervalUnit = null;
    protected ?FHIRDecimal $factor = null;
    protected ?FHIRDecimal $lowerLimit = null;
    protected ?FHIRDecimal $upperLimit = null;
    protected ?FHIRPositiveInt $dimensions = null;
    protected ?FHIRCanonical $codeMap = null;
    protected ?FHIRString $offsets = null;
    protected ?FHIRString $data = null;

    public function getOrigin(): ?FHIRQuantity
    {
        return $this->origin;
    }

    public function setOrigin(?FHIRQuantity $value): self
    {
        $this->origin = $value;

        return $this;
    }

    public function getInterval(): ?FHIRDecimal
    {
        return $this->interval;
    }

    public function setInterval(float|FHIRDecimal|null $value): self
    {
        $this->interval = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getIntervalUnit(): ?FHIRCode
    {
        return $this->intervalUnit;
    }

    public function setIntervalUnit(string|FHIRCode|null $value): self
    {
        $this->intervalUnit = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getFactor(): ?FHIRDecimal
    {
        return $this->factor;
    }

    public function setFactor(float|FHIRDecimal|null $value): self
    {
        $this->factor = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getLowerLimit(): ?FHIRDecimal
    {
        return $this->lowerLimit;
    }

    public function setLowerLimit(float|FHIRDecimal|null $value): self
    {
        $this->lowerLimit = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getUpperLimit(): ?FHIRDecimal
    {
        return $this->upperLimit;
    }

    public function setUpperLimit(float|FHIRDecimal|null $value): self
    {
        $this->upperLimit = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getDimensions(): ?FHIRPositiveInt
    {
        return $this->dimensions;
    }

    public function setDimensions(int|FHIRPositiveInt|null $value): self
    {
        $this->dimensions = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getCodeMap(): ?FHIRCanonical
    {
        return $this->codeMap;
    }

    public function setCodeMap(string|FHIRCanonical|null $value): self
    {
        $this->codeMap = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    public function getOffsets(): ?FHIRString
    {
        return $this->offsets;
    }

    public function setOffsets(string|FHIRString|null $value): self
    {
        $this->offsets = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getData(): ?FHIRString
    {
        return $this->data;
    }

    public function setData(string|FHIRString|null $value): self
    {
        $this->data = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
