<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Timing Complex
 */

namespace Ox\Components\FHIRCore\Model\R5\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRTimingInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\Element\FHIRTimingRepeat;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;

class FHIRTiming extends FHIRBackboneType implements FHIRTimingInterface
{
    public const RESOURCE_NAME = 'Timing';

    /** @var FHIRDateTime[] */
    protected array $event = [];
    protected ?FHIRTimingRepeat $repeat = null;
    protected ?FHIRCodeableConcept $code = null;

    /**
     * @return FHIRDateTime[]
     */
    public function getEvent(): array
    {
        return $this->event;
    }

    public function setEvent(string|FHIRDateTime|null ...$value): self
    {
        $this->event = [];
        $this->addEvent(...$value);

        return $this;
    }

    public function addEvent(string|FHIRDateTime|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRDateTime())->setValue($v) : $v, $value);

        $this->event = array_filter(array_merge($this->event, $values));

        return $this;
    }

    public function getRepeat(): ?FHIRTimingRepeat
    {
        return $this->repeat;
    }

    public function setRepeat(?FHIRTimingRepeat $value): self
    {
        $this->repeat = $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }
}
