<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR UsageContext Complex
 */

namespace Ox\Components\FHIRCore\Model\R5\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRUsageContextInterface;

class FHIRUsageContext extends FHIRDataType implements FHIRUsageContextInterface
{
    public const RESOURCE_NAME = 'UsageContext';

    protected ?FHIRCoding $code = null;
    protected FHIRCodeableConcept|FHIRQuantity|FHIRRange|FHIRReference|null $value = null;

    public function getCode(): ?FHIRCoding
    {
        return $this->code;
    }

    public function setCode(?FHIRCoding $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getValue(): FHIRCodeableConcept|FHIRQuantity|FHIRRange|FHIRReference|null
    {
        return $this->value;
    }

    public function setValue(FHIRCodeableConcept|FHIRQuantity|FHIRRange|FHIRReference|null $value): self
    {
        $this->value = $value;

        return $this;
    }
}
