<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Signature Complex
 */

namespace Ox\Components\FHIRCore\Model\R5\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRSignatureInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBase64Binary;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInstant;

class FHIRSignature extends FHIRDataType implements FHIRSignatureInterface
{
    public const RESOURCE_NAME = 'Signature';

    /** @var FHIRCoding[] */
    protected array $type = [];
    protected ?FHIRInstant $when = null;
    protected ?FHIRReference $who = null;
    protected ?FHIRReference $onBehalfOf = null;
    protected ?FHIRCode $targetFormat = null;
    protected ?FHIRCode $sigFormat = null;
    protected ?FHIRBase64Binary $data = null;

    /**
     * @return FHIRCoding[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRCoding ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRCoding ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }

    public function getWhen(): ?FHIRInstant
    {
        return $this->when;
    }

    public function setWhen(string|FHIRInstant|null $value): self
    {
        $this->when = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    public function getWho(): ?FHIRReference
    {
        return $this->who;
    }

    public function setWho(?FHIRReference $value): self
    {
        $this->who = $value;

        return $this;
    }

    public function getOnBehalfOf(): ?FHIRReference
    {
        return $this->onBehalfOf;
    }

    public function setOnBehalfOf(?FHIRReference $value): self
    {
        $this->onBehalfOf = $value;

        return $this;
    }

    public function getTargetFormat(): ?FHIRCode
    {
        return $this->targetFormat;
    }

    public function setTargetFormat(string|FHIRCode|null $value): self
    {
        $this->targetFormat = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getSigFormat(): ?FHIRCode
    {
        return $this->sigFormat;
    }

    public function setSigFormat(string|FHIRCode|null $value): self
    {
        $this->sigFormat = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getData(): ?FHIRBase64Binary
    {
        return $this->data;
    }

    public function setData(string|FHIRBase64Binary|null $value): self
    {
        $this->data = is_string($value) ? (new FHIRBase64Binary())->setValue($value) : $value;

        return $this;
    }
}
