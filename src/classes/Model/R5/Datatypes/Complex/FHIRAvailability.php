<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Availability Complex
 */

namespace Ox\Components\FHIRCore\Model\R5\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRAvailabilityInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\Element\FHIRAvailabilityAvailableTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\Element\FHIRAvailabilityNotAvailableTime;

class FHIRAvailability extends FHIRDataType implements FHIRAvailabilityInterface
{
    public const RESOURCE_NAME = 'Availability';

    /** @var FHIRAvailabilityAvailableTime[] */
    protected array $availableTime = [];

    /** @var FHIRAvailabilityNotAvailableTime[] */
    protected array $notAvailableTime = [];

    /**
     * @return FHIRAvailabilityAvailableTime[]
     */
    public function getAvailableTime(): array
    {
        return $this->availableTime;
    }

    public function setAvailableTime(?FHIRAvailabilityAvailableTime ...$value): self
    {
        $this->availableTime = array_filter($value);

        return $this;
    }

    public function addAvailableTime(?FHIRAvailabilityAvailableTime ...$value): self
    {
        $this->availableTime = array_filter(array_merge($this->availableTime, $value));

        return $this;
    }

    /**
     * @return FHIRAvailabilityNotAvailableTime[]
     */
    public function getNotAvailableTime(): array
    {
        return $this->notAvailableTime;
    }

    public function setNotAvailableTime(?FHIRAvailabilityNotAvailableTime ...$value): self
    {
        $this->notAvailableTime = array_filter($value);

        return $this;
    }

    public function addNotAvailableTime(?FHIRAvailabilityNotAvailableTime ...$value): self
    {
        $this->notAvailableTime = array_filter(array_merge($this->notAvailableTime, $value));

        return $this;
    }
}
