<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PrimitiveType Complex
 */

namespace Ox\Components\FHIRCore\Model\R5\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRPrimitiveTypeInterface;

abstract class FHIRPrimitiveType extends FHIRDataType implements FHIRPrimitiveTypeInterface
{
    public const RESOURCE_NAME = 'PrimitiveType';
}
