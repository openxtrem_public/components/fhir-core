<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR BackboneType Complex
 */

namespace Ox\Components\FHIRCore\Model\R5\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRBackboneTypeInterface;

abstract class FHIRBackboneType extends FHIRDataType implements FHIRBackboneTypeInterface
{
    public const RESOURCE_NAME = 'BackboneType';

    /** @var FHIRExtension[] */
    protected array $modifierExtension = [];

    /**
     * @return FHIRExtension[]
     */
    public function getModifierExtension(): array
    {
        return $this->modifierExtension;
    }

    public function setModifierExtension(?FHIRExtension ...$value): self
    {
        $this->modifierExtension = array_filter($value);

        return $this;
    }

    public function addModifierExtension(?FHIRExtension ...$value): self
    {
        $this->modifierExtension = array_filter(array_merge($this->modifierExtension, $value));

        return $this;
    }
}
