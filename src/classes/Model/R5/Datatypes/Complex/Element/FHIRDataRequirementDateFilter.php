<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DataRequirementDateFilter Element
 */

namespace Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\Element;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\Element\FHIRDataRequirementDateFilterInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRDataRequirementDateFilter extends FHIRElement implements FHIRDataRequirementDateFilterInterface
{
    public const RESOURCE_NAME = 'DataRequirement.dateFilter';

    protected ?FHIRString $path = null;
    protected ?FHIRString $searchParam = null;
    protected FHIRDateTime|FHIRPeriod|FHIRDuration|null $value = null;

    public function getPath(): ?FHIRString
    {
        return $this->path;
    }

    public function setPath(string|FHIRString|null $value): self
    {
        $this->path = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSearchParam(): ?FHIRString
    {
        return $this->searchParam;
    }

    public function setSearchParam(string|FHIRString|null $value): self
    {
        $this->searchParam = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getValue(): FHIRDateTime|FHIRPeriod|FHIRDuration|null
    {
        return $this->value;
    }

    public function setValue(FHIRDateTime|FHIRPeriod|FHIRDuration|null $value): self
    {
        $this->value = $value;

        return $this;
    }
}
