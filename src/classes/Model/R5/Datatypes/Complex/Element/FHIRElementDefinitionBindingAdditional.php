<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ElementDefinitionBindingAdditional Element
 */

namespace Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\Element;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\Element\FHIRElementDefinitionBindingAdditionalInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRElementDefinitionBindingAdditional extends FHIRElement implements FHIRElementDefinitionBindingAdditionalInterface
{
    public const RESOURCE_NAME = 'ElementDefinition.binding.additional';

    protected ?FHIRCode $purpose = null;
    protected ?FHIRCanonical $valueSet = null;
    protected ?FHIRMarkdown $documentation = null;
    protected ?FHIRString $shortDoco = null;

    /** @var FHIRUsageContext[] */
    protected array $usage = [];
    protected ?FHIRBoolean $any = null;

    public function getPurpose(): ?FHIRCode
    {
        return $this->purpose;
    }

    public function setPurpose(string|FHIRCode|null $value): self
    {
        $this->purpose = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getValueSet(): ?FHIRCanonical
    {
        return $this->valueSet;
    }

    public function setValueSet(string|FHIRCanonical|null $value): self
    {
        $this->valueSet = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    public function getDocumentation(): ?FHIRMarkdown
    {
        return $this->documentation;
    }

    public function setDocumentation(string|FHIRMarkdown|null $value): self
    {
        $this->documentation = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getShortDoco(): ?FHIRString
    {
        return $this->shortDoco;
    }

    public function setShortDoco(string|FHIRString|null $value): self
    {
        $this->shortDoco = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUsage(): array
    {
        return $this->usage;
    }

    public function setUsage(?FHIRUsageContext ...$value): self
    {
        $this->usage = array_filter($value);

        return $this;
    }

    public function addUsage(?FHIRUsageContext ...$value): self
    {
        $this->usage = array_filter(array_merge($this->usage, $value));

        return $this;
    }

    public function getAny(): ?FHIRBoolean
    {
        return $this->any;
    }

    public function setAny(bool|FHIRBoolean|null $value): self
    {
        $this->any = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }
}
