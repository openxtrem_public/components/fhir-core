<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ElementDefinitionConstraint Element
 */

namespace Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\Element;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\Element\FHIRElementDefinitionConstraintInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRElementDefinitionConstraint extends FHIRElement implements FHIRElementDefinitionConstraintInterface
{
    public const RESOURCE_NAME = 'ElementDefinition.constraint';

    protected ?FHIRId $key = null;
    protected ?FHIRMarkdown $requirements = null;
    protected ?FHIRCode $severity = null;
    protected ?FHIRBoolean $suppress = null;
    protected ?FHIRString $human = null;
    protected ?FHIRString $expression = null;
    protected ?FHIRCanonical $source = null;

    public function getKey(): ?FHIRId
    {
        return $this->key;
    }

    public function setKey(string|FHIRId|null $value): self
    {
        $this->key = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getRequirements(): ?FHIRMarkdown
    {
        return $this->requirements;
    }

    public function setRequirements(string|FHIRMarkdown|null $value): self
    {
        $this->requirements = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getSeverity(): ?FHIRCode
    {
        return $this->severity;
    }

    public function setSeverity(string|FHIRCode|null $value): self
    {
        $this->severity = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getSuppress(): ?FHIRBoolean
    {
        return $this->suppress;
    }

    public function setSuppress(bool|FHIRBoolean|null $value): self
    {
        $this->suppress = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getHuman(): ?FHIRString
    {
        return $this->human;
    }

    public function setHuman(string|FHIRString|null $value): self
    {
        $this->human = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getExpression(): ?FHIRString
    {
        return $this->expression;
    }

    public function setExpression(string|FHIRString|null $value): self
    {
        $this->expression = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSource(): ?FHIRCanonical
    {
        return $this->source;
    }

    public function setSource(string|FHIRCanonical|null $value): self
    {
        $this->source = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }
}
