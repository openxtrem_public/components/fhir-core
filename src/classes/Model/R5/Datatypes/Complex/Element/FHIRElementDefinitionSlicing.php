<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ElementDefinitionSlicing Element
 */

namespace Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\Element;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\Element\FHIRElementDefinitionSlicingInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRElementDefinitionSlicing extends FHIRElement implements FHIRElementDefinitionSlicingInterface
{
    public const RESOURCE_NAME = 'ElementDefinition.slicing';

    /** @var FHIRElementDefinitionSlicingDiscriminator[] */
    protected array $discriminator = [];
    protected ?FHIRString $description = null;
    protected ?FHIRBoolean $ordered = null;
    protected ?FHIRCode $rules = null;

    /**
     * @return FHIRElementDefinitionSlicingDiscriminator[]
     */
    public function getDiscriminator(): array
    {
        return $this->discriminator;
    }

    public function setDiscriminator(?FHIRElementDefinitionSlicingDiscriminator ...$value): self
    {
        $this->discriminator = array_filter($value);

        return $this;
    }

    public function addDiscriminator(?FHIRElementDefinitionSlicingDiscriminator ...$value): self
    {
        $this->discriminator = array_filter(array_merge($this->discriminator, $value));

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getOrdered(): ?FHIRBoolean
    {
        return $this->ordered;
    }

    public function setOrdered(bool|FHIRBoolean|null $value): self
    {
        $this->ordered = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getRules(): ?FHIRCode
    {
        return $this->rules;
    }

    public function setRules(string|FHIRCode|null $value): self
    {
        $this->rules = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }
}
