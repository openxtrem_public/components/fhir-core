<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AvailabilityNotAvailableTime Element
 */

namespace Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\Element;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\Element\FHIRAvailabilityNotAvailableTimeInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRElement;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;

class FHIRAvailabilityNotAvailableTime extends FHIRElement implements FHIRAvailabilityNotAvailableTimeInterface
{
    public const RESOURCE_NAME = 'Availability.notAvailableTime';

    protected ?FHIRString $description = null;
    protected ?FHIRPeriod $during = null;

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDuring(): ?FHIRPeriod
    {
        return $this->during;
    }

    public function setDuring(?FHIRPeriod $value): self
    {
        $this->during = $value;

        return $this;
    }
}
