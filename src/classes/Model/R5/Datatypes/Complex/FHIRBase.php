<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Base Complex
 */

namespace Ox\Components\FHIRCore\Model\R5\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRBaseInterface;
use Ox\Components\FHIRCore\Model\R5\StructureDefinition;
use Ox\Components\FHIRCore\Definition\ResourceDefinition;

abstract class FHIRBase implements FHIRBaseInterface
{
    public const RESOURCE_NAME = 'Base';

    protected ResourceDefinition $fhir_definition;

    public static function isArrayNull(array $array): bool
    {
        foreach ($array as $item) {
            if (!$item->isNull()) {
                return false;
            }
        }

        return true;
    }

    public function isNull(): bool
    {
        foreach ($this->fhir_definition->getFields() as $field) {
            $gotValue = $this->{"get" . ucfirst($field->getLabel())}();
            if ($field->isUnique()) {
                if (!is_null($gotValue) && (!is_object($gotValue) || !$gotValue->isNull())) {
                    return false;
                }
            } elseif (!self::isArrayNull($gotValue)) {
                return false;
            }
        }

        return true;
    }

    public function __construct()
    {
        $this->fhir_definition = StructureDefinition::getResourceDefinition($this);
    }

    public function getFhirResourceDefinition(): ResourceDefinition
    {
        return $this->fhir_definition;
    }

    public static function getFhirStructureDefinition(): string
    {
        return StructureDefinition::class;
    }
}
