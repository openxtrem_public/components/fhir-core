<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ElementDefinition Complex
 */

namespace Ox\Components\FHIRCore\Model\R5\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRElementDefinitionInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\Element\FHIRElementDefinitionBase;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\Element\FHIRElementDefinitionBinding;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\Element\FHIRElementDefinitionConstraint;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\Element\FHIRElementDefinitionExample;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\Element\FHIRElementDefinitionMapping;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\Element\FHIRElementDefinitionSlicing;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\Element\FHIRElementDefinitionType;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUnsignedInt;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUri;

class FHIRElementDefinition extends FHIRBackboneType implements FHIRElementDefinitionInterface
{
    public const RESOURCE_NAME = 'ElementDefinition';

    protected ?FHIRString $path = null;

    /** @var FHIRCode[] */
    protected array $representation = [];
    protected ?FHIRString $sliceName = null;
    protected ?FHIRBoolean $sliceIsConstraining = null;
    protected ?FHIRString $label = null;

    /** @var FHIRCoding[] */
    protected array $code = [];
    protected ?FHIRElementDefinitionSlicing $slicing = null;
    protected ?FHIRString $short = null;
    protected ?FHIRMarkdown $definition = null;
    protected ?FHIRMarkdown $comment = null;
    protected ?FHIRMarkdown $requirements = null;

    /** @var FHIRString[] */
    protected array $alias = [];
    protected ?FHIRUnsignedInt $min = null;
    protected ?FHIRString $max = null;
    protected ?FHIRElementDefinitionBase $base = null;
    protected ?FHIRUri $contentReference = null;

    /** @var FHIRElementDefinitionType[] */
    protected array $type = [];
    protected ?FHIRElement $defaultValue = null;
    protected ?FHIRMarkdown $meaningWhenMissing = null;
    protected ?FHIRString $orderMeaning = null;
    protected ?FHIRElement $fixed = null;
    protected ?FHIRElement $pattern = null;

    /** @var FHIRElementDefinitionExample[] */
    protected array $example = [];
    protected ?FHIRElement $minValue = null;
    protected ?FHIRElement $maxValue = null;
    protected ?FHIRInteger $maxLength = null;

    /** @var FHIRId[] */
    protected array $condition = [];

    /** @var FHIRElementDefinitionConstraint[] */
    protected array $constraint = [];
    protected ?FHIRBoolean $mustHaveValue = null;

    /** @var FHIRCanonical[] */
    protected array $valueAlternatives = [];
    protected ?FHIRBoolean $mustSupport = null;
    protected ?FHIRBoolean $isModifier = null;
    protected ?FHIRString $isModifierReason = null;
    protected ?FHIRBoolean $isSummary = null;
    protected ?FHIRElementDefinitionBinding $binding = null;

    /** @var FHIRElementDefinitionMapping[] */
    protected array $mapping = [];

    public function getPath(): ?FHIRString
    {
        return $this->path;
    }

    public function setPath(string|FHIRString|null $value): self
    {
        $this->path = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getRepresentation(): array
    {
        return $this->representation;
    }

    public function setRepresentation(string|FHIRCode|null ...$value): self
    {
        $this->representation = [];
        $this->addRepresentation(...$value);

        return $this;
    }

    public function addRepresentation(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->representation = array_filter(array_merge($this->representation, $values));

        return $this;
    }

    public function getSliceName(): ?FHIRString
    {
        return $this->sliceName;
    }

    public function setSliceName(string|FHIRString|null $value): self
    {
        $this->sliceName = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSliceIsConstraining(): ?FHIRBoolean
    {
        return $this->sliceIsConstraining;
    }

    public function setSliceIsConstraining(bool|FHIRBoolean|null $value): self
    {
        $this->sliceIsConstraining = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getLabel(): ?FHIRString
    {
        return $this->label;
    }

    public function setLabel(string|FHIRString|null $value): self
    {
        $this->label = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getCode(): array
    {
        return $this->code;
    }

    public function setCode(?FHIRCoding ...$value): self
    {
        $this->code = array_filter($value);

        return $this;
    }

    public function addCode(?FHIRCoding ...$value): self
    {
        $this->code = array_filter(array_merge($this->code, $value));

        return $this;
    }

    public function getSlicing(): ?FHIRElementDefinitionSlicing
    {
        return $this->slicing;
    }

    public function setSlicing(?FHIRElementDefinitionSlicing $value): self
    {
        $this->slicing = $value;

        return $this;
    }

    public function getShort(): ?FHIRString
    {
        return $this->short;
    }

    public function setShort(string|FHIRString|null $value): self
    {
        $this->short = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDefinition(): ?FHIRMarkdown
    {
        return $this->definition;
    }

    public function setDefinition(string|FHIRMarkdown|null $value): self
    {
        $this->definition = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getComment(): ?FHIRMarkdown
    {
        return $this->comment;
    }

    public function setComment(string|FHIRMarkdown|null $value): self
    {
        $this->comment = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getRequirements(): ?FHIRMarkdown
    {
        return $this->requirements;
    }

    public function setRequirements(string|FHIRMarkdown|null $value): self
    {
        $this->requirements = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getAlias(): array
    {
        return $this->alias;
    }

    public function setAlias(string|FHIRString|null ...$value): self
    {
        $this->alias = [];
        $this->addAlias(...$value);

        return $this;
    }

    public function addAlias(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->alias = array_filter(array_merge($this->alias, $values));

        return $this;
    }

    public function getMin(): ?FHIRUnsignedInt
    {
        return $this->min;
    }

    public function setMin(int|FHIRUnsignedInt|null $value): self
    {
        $this->min = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getMax(): ?FHIRString
    {
        return $this->max;
    }

    public function setMax(string|FHIRString|null $value): self
    {
        $this->max = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getBase(): ?FHIRElementDefinitionBase
    {
        return $this->base;
    }

    public function setBase(?FHIRElementDefinitionBase $value): self
    {
        $this->base = $value;

        return $this;
    }

    public function getContentReference(): ?FHIRUri
    {
        return $this->contentReference;
    }

    public function setContentReference(string|FHIRUri|null $value): self
    {
        $this->contentReference = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRElementDefinitionType[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRElementDefinitionType ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRElementDefinitionType ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }

    public function getDefaultValue(): ?FHIRElement
    {
        return $this->defaultValue;
    }

    public function setDefaultValue(?FHIRElement $value): self
    {
        $this->defaultValue = $value;

        return $this;
    }

    public function getMeaningWhenMissing(): ?FHIRMarkdown
    {
        return $this->meaningWhenMissing;
    }

    public function setMeaningWhenMissing(string|FHIRMarkdown|null $value): self
    {
        $this->meaningWhenMissing = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getOrderMeaning(): ?FHIRString
    {
        return $this->orderMeaning;
    }

    public function setOrderMeaning(string|FHIRString|null $value): self
    {
        $this->orderMeaning = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getFixed(): ?FHIRElement
    {
        return $this->fixed;
    }

    public function setFixed(?FHIRElement $value): self
    {
        $this->fixed = $value;

        return $this;
    }

    public function getPattern(): ?FHIRElement
    {
        return $this->pattern;
    }

    public function setPattern(?FHIRElement $value): self
    {
        $this->pattern = $value;

        return $this;
    }

    /**
     * @return FHIRElementDefinitionExample[]
     */
    public function getExample(): array
    {
        return $this->example;
    }

    public function setExample(?FHIRElementDefinitionExample ...$value): self
    {
        $this->example = array_filter($value);

        return $this;
    }

    public function addExample(?FHIRElementDefinitionExample ...$value): self
    {
        $this->example = array_filter(array_merge($this->example, $value));

        return $this;
    }

    public function getMinValue(): ?FHIRElement
    {
        return $this->minValue;
    }

    public function setMinValue(?FHIRElement $value): self
    {
        $this->minValue = $value;

        return $this;
    }

    public function getMaxValue(): ?FHIRElement
    {
        return $this->maxValue;
    }

    public function setMaxValue(?FHIRElement $value): self
    {
        $this->maxValue = $value;

        return $this;
    }

    public function getMaxLength(): ?FHIRInteger
    {
        return $this->maxLength;
    }

    public function setMaxLength(int|FHIRInteger|null $value): self
    {
        $this->maxLength = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRId[]
     */
    public function getCondition(): array
    {
        return $this->condition;
    }

    public function setCondition(string|FHIRId|null ...$value): self
    {
        $this->condition = [];
        $this->addCondition(...$value);

        return $this;
    }

    public function addCondition(string|FHIRId|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRId())->setValue($v) : $v, $value);

        $this->condition = array_filter(array_merge($this->condition, $values));

        return $this;
    }

    /**
     * @return FHIRElementDefinitionConstraint[]
     */
    public function getConstraint(): array
    {
        return $this->constraint;
    }

    public function setConstraint(?FHIRElementDefinitionConstraint ...$value): self
    {
        $this->constraint = array_filter($value);

        return $this;
    }

    public function addConstraint(?FHIRElementDefinitionConstraint ...$value): self
    {
        $this->constraint = array_filter(array_merge($this->constraint, $value));

        return $this;
    }

    public function getMustHaveValue(): ?FHIRBoolean
    {
        return $this->mustHaveValue;
    }

    public function setMustHaveValue(bool|FHIRBoolean|null $value): self
    {
        $this->mustHaveValue = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getValueAlternatives(): array
    {
        return $this->valueAlternatives;
    }

    public function setValueAlternatives(string|FHIRCanonical|null ...$value): self
    {
        $this->valueAlternatives = [];
        $this->addValueAlternatives(...$value);

        return $this;
    }

    public function addValueAlternatives(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->valueAlternatives = array_filter(array_merge($this->valueAlternatives, $values));

        return $this;
    }

    public function getMustSupport(): ?FHIRBoolean
    {
        return $this->mustSupport;
    }

    public function setMustSupport(bool|FHIRBoolean|null $value): self
    {
        $this->mustSupport = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getIsModifier(): ?FHIRBoolean
    {
        return $this->isModifier;
    }

    public function setIsModifier(bool|FHIRBoolean|null $value): self
    {
        $this->isModifier = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getIsModifierReason(): ?FHIRString
    {
        return $this->isModifierReason;
    }

    public function setIsModifierReason(string|FHIRString|null $value): self
    {
        $this->isModifierReason = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getIsSummary(): ?FHIRBoolean
    {
        return $this->isSummary;
    }

    public function setIsSummary(bool|FHIRBoolean|null $value): self
    {
        $this->isSummary = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getBinding(): ?FHIRElementDefinitionBinding
    {
        return $this->binding;
    }

    public function setBinding(?FHIRElementDefinitionBinding $value): self
    {
        $this->binding = $value;

        return $this;
    }

    /**
     * @return FHIRElementDefinitionMapping[]
     */
    public function getMapping(): array
    {
        return $this->mapping;
    }

    public function setMapping(?FHIRElementDefinitionMapping ...$value): self
    {
        $this->mapping = array_filter($value);

        return $this;
    }

    public function addMapping(?FHIRElementDefinitionMapping ...$value): self
    {
        $this->mapping = array_filter(array_merge($this->mapping, $value));

        return $this;
    }
}
