<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR VirtualServiceDetail Complex
 */

namespace Ox\Components\FHIRCore\Model\R5\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRVirtualServiceDetailInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R5\Datatypes\FHIRUrl;

class FHIRVirtualServiceDetail extends FHIRDataType implements FHIRVirtualServiceDetailInterface
{
    public const RESOURCE_NAME = 'VirtualServiceDetail';

    protected ?FHIRCoding $channelType = null;
    protected FHIRUrl|FHIRString|FHIRContactPoint|FHIRExtendedContactDetail|null $address = null;

    /** @var FHIRUrl[] */
    protected array $additionalInfo = [];
    protected ?FHIRPositiveInt $maxParticipants = null;
    protected ?FHIRString $sessionKey = null;

    public function getChannelType(): ?FHIRCoding
    {
        return $this->channelType;
    }

    public function setChannelType(?FHIRCoding $value): self
    {
        $this->channelType = $value;

        return $this;
    }

    public function getAddress(): FHIRUrl|FHIRString|FHIRContactPoint|FHIRExtendedContactDetail|null
    {
        return $this->address;
    }

    public function setAddress(FHIRUrl|FHIRString|FHIRContactPoint|FHIRExtendedContactDetail|null $value): self
    {
        $this->address = $value;

        return $this;
    }

    /**
     * @return FHIRUrl[]
     */
    public function getAdditionalInfo(): array
    {
        return $this->additionalInfo;
    }

    public function setAdditionalInfo(string|FHIRUrl|null ...$value): self
    {
        $this->additionalInfo = [];
        $this->addAdditionalInfo(...$value);

        return $this;
    }

    public function addAdditionalInfo(string|FHIRUrl|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUrl())->setValue($v) : $v, $value);

        $this->additionalInfo = array_filter(array_merge($this->additionalInfo, $values));

        return $this;
    }

    public function getMaxParticipants(): ?FHIRPositiveInt
    {
        return $this->maxParticipants;
    }

    public function setMaxParticipants(int|FHIRPositiveInt|null $value): self
    {
        $this->maxParticipants = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getSessionKey(): ?FHIRString
    {
        return $this->sessionKey;
    }

    public function setSessionKey(string|FHIRString|null $value): self
    {
        $this->sessionKey = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
