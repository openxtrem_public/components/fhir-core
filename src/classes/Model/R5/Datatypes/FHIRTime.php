<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR time Primitive
 */

namespace Ox\Components\FHIRCore\Model\R5\Datatypes;

use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRTimeInterface;
use Ox\Components\FHIRCore\Model\R5\Datatypes\Complex\FHIRPrimitiveType;

class FHIRTime extends FHIRPrimitiveType implements FHIRTimeInterface
{
    public const RESOURCE_NAME = 'time';

    protected ?string $value = null;

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
