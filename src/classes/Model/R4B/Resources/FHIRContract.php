<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Contract Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRContractInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRContractContentDefinition;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRContractFriendly;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRContractLegal;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRContractRule;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRContractSigner;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRContractTerm;

class FHIRContract extends FHIRDomainResource implements FHIRContractInterface
{
    public const RESOURCE_NAME = 'Contract';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRUri $url = null;
    protected ?FHIRString $version = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $legalState = null;
    protected ?FHIRReference $instantiatesCanonical = null;
    protected ?FHIRUri $instantiatesUri = null;
    protected ?FHIRCodeableConcept $contentDerivative = null;
    protected ?FHIRDateTime $issued = null;
    protected ?FHIRPeriod $applies = null;
    protected ?FHIRCodeableConcept $expirationType = null;

    /** @var FHIRReference[] */
    protected array $subject = [];

    /** @var FHIRReference[] */
    protected array $authority = [];

    /** @var FHIRReference[] */
    protected array $domain = [];

    /** @var FHIRReference[] */
    protected array $site = [];
    protected ?FHIRString $name = null;
    protected ?FHIRString $title = null;
    protected ?FHIRString $subtitle = null;

    /** @var FHIRString[] */
    protected array $alias = [];
    protected ?FHIRReference $author = null;
    protected ?FHIRCodeableConcept $scope = null;
    protected FHIRCodeableConcept|FHIRReference|null $topic = null;
    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRCodeableConcept[] */
    protected array $subType = [];
    protected ?FHIRContractContentDefinition $contentDefinition = null;

    /** @var FHIRContractTerm[] */
    protected array $term = [];

    /** @var FHIRReference[] */
    protected array $supportingInfo = [];

    /** @var FHIRReference[] */
    protected array $relevantHistory = [];

    /** @var FHIRContractSigner[] */
    protected array $signer = [];

    /** @var FHIRContractFriendly[] */
    protected array $friendly = [];

    /** @var FHIRContractLegal[] */
    protected array $legal = [];

    /** @var FHIRContractRule[] */
    protected array $rule = [];
    protected FHIRAttachment|FHIRReference|null $legallyBinding = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getLegalState(): ?FHIRCodeableConcept
    {
        return $this->legalState;
    }

    public function setLegalState(?FHIRCodeableConcept $value): self
    {
        $this->legalState = $value;

        return $this;
    }

    public function getInstantiatesCanonical(): ?FHIRReference
    {
        return $this->instantiatesCanonical;
    }

    public function setInstantiatesCanonical(?FHIRReference $value): self
    {
        $this->instantiatesCanonical = $value;

        return $this;
    }

    public function getInstantiatesUri(): ?FHIRUri
    {
        return $this->instantiatesUri;
    }

    public function setInstantiatesUri(string|FHIRUri|null $value): self
    {
        $this->instantiatesUri = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getContentDerivative(): ?FHIRCodeableConcept
    {
        return $this->contentDerivative;
    }

    public function setContentDerivative(?FHIRCodeableConcept $value): self
    {
        $this->contentDerivative = $value;

        return $this;
    }

    public function getIssued(): ?FHIRDateTime
    {
        return $this->issued;
    }

    public function setIssued(string|FHIRDateTime|null $value): self
    {
        $this->issued = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getApplies(): ?FHIRPeriod
    {
        return $this->applies;
    }

    public function setApplies(?FHIRPeriod $value): self
    {
        $this->applies = $value;

        return $this;
    }

    public function getExpirationType(): ?FHIRCodeableConcept
    {
        return $this->expirationType;
    }

    public function setExpirationType(?FHIRCodeableConcept $value): self
    {
        $this->expirationType = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSubject(): array
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference ...$value): self
    {
        $this->subject = array_filter($value);

        return $this;
    }

    public function addSubject(?FHIRReference ...$value): self
    {
        $this->subject = array_filter(array_merge($this->subject, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAuthority(): array
    {
        return $this->authority;
    }

    public function setAuthority(?FHIRReference ...$value): self
    {
        $this->authority = array_filter($value);

        return $this;
    }

    public function addAuthority(?FHIRReference ...$value): self
    {
        $this->authority = array_filter(array_merge($this->authority, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getDomain(): array
    {
        return $this->domain;
    }

    public function setDomain(?FHIRReference ...$value): self
    {
        $this->domain = array_filter($value);

        return $this;
    }

    public function addDomain(?FHIRReference ...$value): self
    {
        $this->domain = array_filter(array_merge($this->domain, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSite(): array
    {
        return $this->site;
    }

    public function setSite(?FHIRReference ...$value): self
    {
        $this->site = array_filter($value);

        return $this;
    }

    public function addSite(?FHIRReference ...$value): self
    {
        $this->site = array_filter(array_merge($this->site, $value));

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSubtitle(): ?FHIRString
    {
        return $this->subtitle;
    }

    public function setSubtitle(string|FHIRString|null $value): self
    {
        $this->subtitle = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getAlias(): array
    {
        return $this->alias;
    }

    public function setAlias(string|FHIRString|null ...$value): self
    {
        $this->alias = [];
        $this->addAlias(...$value);

        return $this;
    }

    public function addAlias(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->alias = array_filter(array_merge($this->alias, $values));

        return $this;
    }

    public function getAuthor(): ?FHIRReference
    {
        return $this->author;
    }

    public function setAuthor(?FHIRReference $value): self
    {
        $this->author = $value;

        return $this;
    }

    public function getScope(): ?FHIRCodeableConcept
    {
        return $this->scope;
    }

    public function setScope(?FHIRCodeableConcept $value): self
    {
        $this->scope = $value;

        return $this;
    }

    public function getTopic(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->topic;
    }

    public function setTopic(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->topic = $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSubType(): array
    {
        return $this->subType;
    }

    public function setSubType(?FHIRCodeableConcept ...$value): self
    {
        $this->subType = array_filter($value);

        return $this;
    }

    public function addSubType(?FHIRCodeableConcept ...$value): self
    {
        $this->subType = array_filter(array_merge($this->subType, $value));

        return $this;
    }

    public function getContentDefinition(): ?FHIRContractContentDefinition
    {
        return $this->contentDefinition;
    }

    public function setContentDefinition(?FHIRContractContentDefinition $value): self
    {
        $this->contentDefinition = $value;

        return $this;
    }

    /**
     * @return FHIRContractTerm[]
     */
    public function getTerm(): array
    {
        return $this->term;
    }

    public function setTerm(?FHIRContractTerm ...$value): self
    {
        $this->term = array_filter($value);

        return $this;
    }

    public function addTerm(?FHIRContractTerm ...$value): self
    {
        $this->term = array_filter(array_merge($this->term, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupportingInfo(): array
    {
        return $this->supportingInfo;
    }

    public function setSupportingInfo(?FHIRReference ...$value): self
    {
        $this->supportingInfo = array_filter($value);

        return $this;
    }

    public function addSupportingInfo(?FHIRReference ...$value): self
    {
        $this->supportingInfo = array_filter(array_merge($this->supportingInfo, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getRelevantHistory(): array
    {
        return $this->relevantHistory;
    }

    public function setRelevantHistory(?FHIRReference ...$value): self
    {
        $this->relevantHistory = array_filter($value);

        return $this;
    }

    public function addRelevantHistory(?FHIRReference ...$value): self
    {
        $this->relevantHistory = array_filter(array_merge($this->relevantHistory, $value));

        return $this;
    }

    /**
     * @return FHIRContractSigner[]
     */
    public function getSigner(): array
    {
        return $this->signer;
    }

    public function setSigner(?FHIRContractSigner ...$value): self
    {
        $this->signer = array_filter($value);

        return $this;
    }

    public function addSigner(?FHIRContractSigner ...$value): self
    {
        $this->signer = array_filter(array_merge($this->signer, $value));

        return $this;
    }

    /**
     * @return FHIRContractFriendly[]
     */
    public function getFriendly(): array
    {
        return $this->friendly;
    }

    public function setFriendly(?FHIRContractFriendly ...$value): self
    {
        $this->friendly = array_filter($value);

        return $this;
    }

    public function addFriendly(?FHIRContractFriendly ...$value): self
    {
        $this->friendly = array_filter(array_merge($this->friendly, $value));

        return $this;
    }

    /**
     * @return FHIRContractLegal[]
     */
    public function getLegal(): array
    {
        return $this->legal;
    }

    public function setLegal(?FHIRContractLegal ...$value): self
    {
        $this->legal = array_filter($value);

        return $this;
    }

    public function addLegal(?FHIRContractLegal ...$value): self
    {
        $this->legal = array_filter(array_merge($this->legal, $value));

        return $this;
    }

    /**
     * @return FHIRContractRule[]
     */
    public function getRule(): array
    {
        return $this->rule;
    }

    public function setRule(?FHIRContractRule ...$value): self
    {
        $this->rule = array_filter($value);

        return $this;
    }

    public function addRule(?FHIRContractRule ...$value): self
    {
        $this->rule = array_filter(array_merge($this->rule, $value));

        return $this;
    }

    public function getLegallyBinding(): FHIRAttachment|FHIRReference|null
    {
        return $this->legallyBinding;
    }

    public function setLegallyBinding(FHIRAttachment|FHIRReference|null $value): self
    {
        $this->legallyBinding = $value;

        return $this;
    }
}
