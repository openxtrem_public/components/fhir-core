<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Endpoint Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIREndpointInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUrl;

class FHIREndpoint extends FHIRDomainResource implements FHIREndpointInterface
{
    public const RESOURCE_NAME = 'Endpoint';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCoding $connectionType = null;
    protected ?FHIRString $name = null;
    protected ?FHIRReference $managingOrganization = null;

    /** @var FHIRContactPoint[] */
    protected array $contact = [];
    protected ?FHIRPeriod $period = null;

    /** @var FHIRCodeableConcept[] */
    protected array $payloadType = [];

    /** @var FHIRCode[] */
    protected array $payloadMimeType = [];
    protected ?FHIRUrl $address = null;

    /** @var FHIRString[] */
    protected array $header = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getConnectionType(): ?FHIRCoding
    {
        return $this->connectionType;
    }

    public function setConnectionType(?FHIRCoding $value): self
    {
        $this->connectionType = $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getManagingOrganization(): ?FHIRReference
    {
        return $this->managingOrganization;
    }

    public function setManagingOrganization(?FHIRReference $value): self
    {
        $this->managingOrganization = $value;

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactPoint ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactPoint ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getPayloadType(): array
    {
        return $this->payloadType;
    }

    public function setPayloadType(?FHIRCodeableConcept ...$value): self
    {
        $this->payloadType = array_filter($value);

        return $this;
    }

    public function addPayloadType(?FHIRCodeableConcept ...$value): self
    {
        $this->payloadType = array_filter(array_merge($this->payloadType, $value));

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getPayloadMimeType(): array
    {
        return $this->payloadMimeType;
    }

    public function setPayloadMimeType(string|FHIRCode|null ...$value): self
    {
        $this->payloadMimeType = [];
        $this->addPayloadMimeType(...$value);

        return $this;
    }

    public function addPayloadMimeType(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->payloadMimeType = array_filter(array_merge($this->payloadMimeType, $values));

        return $this;
    }

    public function getAddress(): ?FHIRUrl
    {
        return $this->address;
    }

    public function setAddress(string|FHIRUrl|null $value): self
    {
        $this->address = is_string($value) ? (new FHIRUrl())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getHeader(): array
    {
        return $this->header;
    }

    public function setHeader(string|FHIRString|null ...$value): self
    {
        $this->header = [];
        $this->addHeader(...$value);

        return $this;
    }

    public function addHeader(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->header = array_filter(array_merge($this->header, $values));

        return $this;
    }
}
