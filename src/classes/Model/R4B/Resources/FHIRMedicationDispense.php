<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationDispense Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMedicationDispenseInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRDosage;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRMedicationDispensePerformer;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRMedicationDispenseSubstitution;

class FHIRMedicationDispense extends FHIRDomainResource implements FHIRMedicationDispenseInterface
{
    public const RESOURCE_NAME = 'MedicationDispense';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRReference[] */
    protected array $partOf = [];
    protected ?FHIRCode $status = null;
    protected FHIRCodeableConcept|FHIRReference|null $statusReason = null;
    protected ?FHIRCodeableConcept $category = null;
    protected FHIRCodeableConcept|FHIRReference|null $medication = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $context = null;

    /** @var FHIRReference[] */
    protected array $supportingInformation = [];

    /** @var FHIRMedicationDispensePerformer[] */
    protected array $performer = [];
    protected ?FHIRReference $location = null;

    /** @var FHIRReference[] */
    protected array $authorizingPrescription = [];
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRQuantity $daysSupply = null;
    protected ?FHIRDateTime $whenPrepared = null;
    protected ?FHIRDateTime $whenHandedOver = null;
    protected ?FHIRReference $destination = null;

    /** @var FHIRReference[] */
    protected array $receiver = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRDosage[] */
    protected array $dosageInstruction = [];
    protected ?FHIRMedicationDispenseSubstitution $substitution = null;

    /** @var FHIRReference[] */
    protected array $detectedIssue = [];

    /** @var FHIRReference[] */
    protected array $eventHistory = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPartOf(): array
    {
        return $this->partOf;
    }

    public function setPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter($value);

        return $this;
    }

    public function addPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter(array_merge($this->partOf, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getStatusReason(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->statusReason;
    }

    public function setStatusReason(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->statusReason = $value;

        return $this;
    }

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getMedication(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->medication;
    }

    public function setMedication(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->medication = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getContext(): ?FHIRReference
    {
        return $this->context;
    }

    public function setContext(?FHIRReference $value): self
    {
        $this->context = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupportingInformation(): array
    {
        return $this->supportingInformation;
    }

    public function setSupportingInformation(?FHIRReference ...$value): self
    {
        $this->supportingInformation = array_filter($value);

        return $this;
    }

    public function addSupportingInformation(?FHIRReference ...$value): self
    {
        $this->supportingInformation = array_filter(array_merge($this->supportingInformation, $value));

        return $this;
    }

    /**
     * @return FHIRMedicationDispensePerformer[]
     */
    public function getPerformer(): array
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRMedicationDispensePerformer ...$value): self
    {
        $this->performer = array_filter($value);

        return $this;
    }

    public function addPerformer(?FHIRMedicationDispensePerformer ...$value): self
    {
        $this->performer = array_filter(array_merge($this->performer, $value));

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAuthorizingPrescription(): array
    {
        return $this->authorizingPrescription;
    }

    public function setAuthorizingPrescription(?FHIRReference ...$value): self
    {
        $this->authorizingPrescription = array_filter($value);

        return $this;
    }

    public function addAuthorizingPrescription(?FHIRReference ...$value): self
    {
        $this->authorizingPrescription = array_filter(array_merge($this->authorizingPrescription, $value));

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getDaysSupply(): ?FHIRQuantity
    {
        return $this->daysSupply;
    }

    public function setDaysSupply(?FHIRQuantity $value): self
    {
        $this->daysSupply = $value;

        return $this;
    }

    public function getWhenPrepared(): ?FHIRDateTime
    {
        return $this->whenPrepared;
    }

    public function setWhenPrepared(string|FHIRDateTime|null $value): self
    {
        $this->whenPrepared = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getWhenHandedOver(): ?FHIRDateTime
    {
        return $this->whenHandedOver;
    }

    public function setWhenHandedOver(string|FHIRDateTime|null $value): self
    {
        $this->whenHandedOver = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getDestination(): ?FHIRReference
    {
        return $this->destination;
    }

    public function setDestination(?FHIRReference $value): self
    {
        $this->destination = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReceiver(): array
    {
        return $this->receiver;
    }

    public function setReceiver(?FHIRReference ...$value): self
    {
        $this->receiver = array_filter($value);

        return $this;
    }

    public function addReceiver(?FHIRReference ...$value): self
    {
        $this->receiver = array_filter(array_merge($this->receiver, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRDosage[]
     */
    public function getDosageInstruction(): array
    {
        return $this->dosageInstruction;
    }

    public function setDosageInstruction(?FHIRDosage ...$value): self
    {
        $this->dosageInstruction = array_filter($value);

        return $this;
    }

    public function addDosageInstruction(?FHIRDosage ...$value): self
    {
        $this->dosageInstruction = array_filter(array_merge($this->dosageInstruction, $value));

        return $this;
    }

    public function getSubstitution(): ?FHIRMedicationDispenseSubstitution
    {
        return $this->substitution;
    }

    public function setSubstitution(?FHIRMedicationDispenseSubstitution $value): self
    {
        $this->substitution = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getDetectedIssue(): array
    {
        return $this->detectedIssue;
    }

    public function setDetectedIssue(?FHIRReference ...$value): self
    {
        $this->detectedIssue = array_filter($value);

        return $this;
    }

    public function addDetectedIssue(?FHIRReference ...$value): self
    {
        $this->detectedIssue = array_filter(array_merge($this->detectedIssue, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEventHistory(): array
    {
        return $this->eventHistory;
    }

    public function setEventHistory(?FHIRReference ...$value): self
    {
        $this->eventHistory = array_filter($value);

        return $this;
    }

    public function addEventHistory(?FHIRReference ...$value): self
    {
        $this->eventHistory = array_filter(array_merge($this->eventHistory, $value));

        return $this;
    }
}
