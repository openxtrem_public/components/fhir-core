<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR NutritionOrder Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRNutritionOrderInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRNutritionOrderEnteralFormula;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRNutritionOrderOralDiet;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRNutritionOrderSupplement;

class FHIRNutritionOrder extends FHIRDomainResource implements FHIRNutritionOrderInterface
{
    public const RESOURCE_NAME = 'NutritionOrder';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRCanonical[] */
    protected array $instantiatesCanonical = [];

    /** @var FHIRUri[] */
    protected array $instantiatesUri = [];

    /** @var FHIRUri[] */
    protected array $instantiates = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCode $intent = null;
    protected ?FHIRReference $patient = null;
    protected ?FHIRReference $encounter = null;
    protected ?FHIRDateTime $dateTime = null;
    protected ?FHIRReference $orderer = null;

    /** @var FHIRReference[] */
    protected array $allergyIntolerance = [];

    /** @var FHIRCodeableConcept[] */
    protected array $foodPreferenceModifier = [];

    /** @var FHIRCodeableConcept[] */
    protected array $excludeFoodModifier = [];
    protected ?FHIRNutritionOrderOralDiet $oralDiet = null;

    /** @var FHIRNutritionOrderSupplement[] */
    protected array $supplement = [];
    protected ?FHIRNutritionOrderEnteralFormula $enteralFormula = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getInstantiatesCanonical(): array
    {
        return $this->instantiatesCanonical;
    }

    public function setInstantiatesCanonical(string|FHIRCanonical|null ...$value): self
    {
        $this->instantiatesCanonical = [];
        $this->addInstantiatesCanonical(...$value);

        return $this;
    }

    public function addInstantiatesCanonical(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->instantiatesCanonical = array_filter(array_merge($this->instantiatesCanonical, $values));

        return $this;
    }

    /**
     * @return FHIRUri[]
     */
    public function getInstantiatesUri(): array
    {
        return $this->instantiatesUri;
    }

    public function setInstantiatesUri(string|FHIRUri|null ...$value): self
    {
        $this->instantiatesUri = [];
        $this->addInstantiatesUri(...$value);

        return $this;
    }

    public function addInstantiatesUri(string|FHIRUri|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUri())->setValue($v) : $v, $value);

        $this->instantiatesUri = array_filter(array_merge($this->instantiatesUri, $values));

        return $this;
    }

    /**
     * @return FHIRUri[]
     */
    public function getInstantiates(): array
    {
        return $this->instantiates;
    }

    public function setInstantiates(string|FHIRUri|null ...$value): self
    {
        $this->instantiates = [];
        $this->addInstantiates(...$value);

        return $this;
    }

    public function addInstantiates(string|FHIRUri|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUri())->setValue($v) : $v, $value);

        $this->instantiates = array_filter(array_merge($this->instantiates, $values));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getIntent(): ?FHIRCode
    {
        return $this->intent;
    }

    public function setIntent(string|FHIRCode|null $value): self
    {
        $this->intent = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getDateTime(): ?FHIRDateTime
    {
        return $this->dateTime;
    }

    public function setDateTime(string|FHIRDateTime|null $value): self
    {
        $this->dateTime = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getOrderer(): ?FHIRReference
    {
        return $this->orderer;
    }

    public function setOrderer(?FHIRReference $value): self
    {
        $this->orderer = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAllergyIntolerance(): array
    {
        return $this->allergyIntolerance;
    }

    public function setAllergyIntolerance(?FHIRReference ...$value): self
    {
        $this->allergyIntolerance = array_filter($value);

        return $this;
    }

    public function addAllergyIntolerance(?FHIRReference ...$value): self
    {
        $this->allergyIntolerance = array_filter(array_merge($this->allergyIntolerance, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getFoodPreferenceModifier(): array
    {
        return $this->foodPreferenceModifier;
    }

    public function setFoodPreferenceModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->foodPreferenceModifier = array_filter($value);

        return $this;
    }

    public function addFoodPreferenceModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->foodPreferenceModifier = array_filter(array_merge($this->foodPreferenceModifier, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getExcludeFoodModifier(): array
    {
        return $this->excludeFoodModifier;
    }

    public function setExcludeFoodModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->excludeFoodModifier = array_filter($value);

        return $this;
    }

    public function addExcludeFoodModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->excludeFoodModifier = array_filter(array_merge($this->excludeFoodModifier, $value));

        return $this;
    }

    public function getOralDiet(): ?FHIRNutritionOrderOralDiet
    {
        return $this->oralDiet;
    }

    public function setOralDiet(?FHIRNutritionOrderOralDiet $value): self
    {
        $this->oralDiet = $value;

        return $this;
    }

    /**
     * @return FHIRNutritionOrderSupplement[]
     */
    public function getSupplement(): array
    {
        return $this->supplement;
    }

    public function setSupplement(?FHIRNutritionOrderSupplement ...$value): self
    {
        $this->supplement = array_filter($value);

        return $this;
    }

    public function addSupplement(?FHIRNutritionOrderSupplement ...$value): self
    {
        $this->supplement = array_filter(array_merge($this->supplement, $value));

        return $this;
    }

    public function getEnteralFormula(): ?FHIRNutritionOrderEnteralFormula
    {
        return $this->enteralFormula;
    }

    public function setEnteralFormula(?FHIRNutritionOrderEnteralFormula $value): self
    {
        $this->enteralFormula = $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }
}
