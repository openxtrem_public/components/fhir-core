<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AdverseEvent Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRAdverseEventInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRAdverseEventSuspectEntity;

class FHIRAdverseEvent extends FHIRDomainResource implements FHIRAdverseEventInterface
{
    public const RESOURCE_NAME = 'AdverseEvent';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRCode $actuality = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRCodeableConcept $event = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $encounter = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRDateTime $detected = null;
    protected ?FHIRDateTime $recordedDate = null;

    /** @var FHIRReference[] */
    protected array $resultingCondition = [];
    protected ?FHIRReference $location = null;
    protected ?FHIRCodeableConcept $seriousness = null;
    protected ?FHIRCodeableConcept $severity = null;
    protected ?FHIRCodeableConcept $outcome = null;
    protected ?FHIRReference $recorder = null;

    /** @var FHIRReference[] */
    protected array $contributor = [];

    /** @var FHIRAdverseEventSuspectEntity[] */
    protected array $suspectEntity = [];

    /** @var FHIRReference[] */
    protected array $subjectMedicalHistory = [];

    /** @var FHIRReference[] */
    protected array $referenceDocument = [];

    /** @var FHIRReference[] */
    protected array $study = [];

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getActuality(): ?FHIRCode
    {
        return $this->actuality;
    }

    public function setActuality(string|FHIRCode|null $value): self
    {
        $this->actuality = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getEvent(): ?FHIRCodeableConcept
    {
        return $this->event;
    }

    public function setEvent(?FHIRCodeableConcept $value): self
    {
        $this->event = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getDetected(): ?FHIRDateTime
    {
        return $this->detected;
    }

    public function setDetected(string|FHIRDateTime|null $value): self
    {
        $this->detected = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getRecordedDate(): ?FHIRDateTime
    {
        return $this->recordedDate;
    }

    public function setRecordedDate(string|FHIRDateTime|null $value): self
    {
        $this->recordedDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getResultingCondition(): array
    {
        return $this->resultingCondition;
    }

    public function setResultingCondition(?FHIRReference ...$value): self
    {
        $this->resultingCondition = array_filter($value);

        return $this;
    }

    public function addResultingCondition(?FHIRReference ...$value): self
    {
        $this->resultingCondition = array_filter(array_merge($this->resultingCondition, $value));

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    public function getSeriousness(): ?FHIRCodeableConcept
    {
        return $this->seriousness;
    }

    public function setSeriousness(?FHIRCodeableConcept $value): self
    {
        $this->seriousness = $value;

        return $this;
    }

    public function getSeverity(): ?FHIRCodeableConcept
    {
        return $this->severity;
    }

    public function setSeverity(?FHIRCodeableConcept $value): self
    {
        $this->severity = $value;

        return $this;
    }

    public function getOutcome(): ?FHIRCodeableConcept
    {
        return $this->outcome;
    }

    public function setOutcome(?FHIRCodeableConcept $value): self
    {
        $this->outcome = $value;

        return $this;
    }

    public function getRecorder(): ?FHIRReference
    {
        return $this->recorder;
    }

    public function setRecorder(?FHIRReference $value): self
    {
        $this->recorder = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getContributor(): array
    {
        return $this->contributor;
    }

    public function setContributor(?FHIRReference ...$value): self
    {
        $this->contributor = array_filter($value);

        return $this;
    }

    public function addContributor(?FHIRReference ...$value): self
    {
        $this->contributor = array_filter(array_merge($this->contributor, $value));

        return $this;
    }

    /**
     * @return FHIRAdverseEventSuspectEntity[]
     */
    public function getSuspectEntity(): array
    {
        return $this->suspectEntity;
    }

    public function setSuspectEntity(?FHIRAdverseEventSuspectEntity ...$value): self
    {
        $this->suspectEntity = array_filter($value);

        return $this;
    }

    public function addSuspectEntity(?FHIRAdverseEventSuspectEntity ...$value): self
    {
        $this->suspectEntity = array_filter(array_merge($this->suspectEntity, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSubjectMedicalHistory(): array
    {
        return $this->subjectMedicalHistory;
    }

    public function setSubjectMedicalHistory(?FHIRReference ...$value): self
    {
        $this->subjectMedicalHistory = array_filter($value);

        return $this;
    }

    public function addSubjectMedicalHistory(?FHIRReference ...$value): self
    {
        $this->subjectMedicalHistory = array_filter(array_merge($this->subjectMedicalHistory, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReferenceDocument(): array
    {
        return $this->referenceDocument;
    }

    public function setReferenceDocument(?FHIRReference ...$value): self
    {
        $this->referenceDocument = array_filter($value);

        return $this;
    }

    public function addReferenceDocument(?FHIRReference ...$value): self
    {
        $this->referenceDocument = array_filter(array_merge($this->referenceDocument, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getStudy(): array
    {
        return $this->study;
    }

    public function setStudy(?FHIRReference ...$value): self
    {
        $this->study = array_filter($value);

        return $this;
    }

    public function addStudy(?FHIRReference ...$value): self
    {
        $this->study = array_filter(array_merge($this->study, $value));

        return $this;
    }
}
