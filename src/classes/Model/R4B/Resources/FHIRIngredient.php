<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Ingredient Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRIngredientInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRIngredientManufacturer;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRIngredientSubstance;

class FHIRIngredient extends FHIRDomainResource implements FHIRIngredientInterface
{
    public const RESOURCE_NAME = 'Ingredient';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRCode $status = null;

    /** @var FHIRReference[] */
    protected array $for = [];
    protected ?FHIRCodeableConcept $role = null;

    /** @var FHIRCodeableConcept[] */
    protected array $function = [];
    protected ?FHIRBoolean $allergenicIndicator = null;

    /** @var FHIRIngredientManufacturer[] */
    protected array $manufacturer = [];
    protected ?FHIRIngredientSubstance $substance = null;

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getFor(): array
    {
        return $this->for;
    }

    public function setFor(?FHIRReference ...$value): self
    {
        $this->for = array_filter($value);

        return $this;
    }

    public function addFor(?FHIRReference ...$value): self
    {
        $this->for = array_filter(array_merge($this->for, $value));

        return $this;
    }

    public function getRole(): ?FHIRCodeableConcept
    {
        return $this->role;
    }

    public function setRole(?FHIRCodeableConcept $value): self
    {
        $this->role = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getFunction(): array
    {
        return $this->function;
    }

    public function setFunction(?FHIRCodeableConcept ...$value): self
    {
        $this->function = array_filter($value);

        return $this;
    }

    public function addFunction(?FHIRCodeableConcept ...$value): self
    {
        $this->function = array_filter(array_merge($this->function, $value));

        return $this;
    }

    public function getAllergenicIndicator(): ?FHIRBoolean
    {
        return $this->allergenicIndicator;
    }

    public function setAllergenicIndicator(bool|FHIRBoolean|null $value): self
    {
        $this->allergenicIndicator = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRIngredientManufacturer[]
     */
    public function getManufacturer(): array
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?FHIRIngredientManufacturer ...$value): self
    {
        $this->manufacturer = array_filter($value);

        return $this;
    }

    public function addManufacturer(?FHIRIngredientManufacturer ...$value): self
    {
        $this->manufacturer = array_filter(array_merge($this->manufacturer, $value));

        return $this;
    }

    public function getSubstance(): ?FHIRIngredientSubstance
    {
        return $this->substance;
    }

    public function setSubstance(?FHIRIngredientSubstance $value): self
    {
        $this->substance = $value;

        return $this;
    }
}
