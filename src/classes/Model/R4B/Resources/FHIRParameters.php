<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Parameters Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRParametersInterface;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRParametersParameter;

class FHIRParameters extends FHIRResource implements FHIRParametersInterface
{
    public const RESOURCE_NAME = 'Parameters';

    /** @var FHIRParametersParameter[] */
    protected array $parameter = [];

    /**
     * @return FHIRParametersParameter[]
     */
    public function getParameter(): array
    {
        return $this->parameter;
    }

    public function setParameter(?FHIRParametersParameter ...$value): self
    {
        $this->parameter = array_filter($value);

        return $this;
    }

    public function addParameter(?FHIRParametersParameter ...$value): self
    {
        $this->parameter = array_filter(array_merge($this->parameter, $value));

        return $this;
    }
}
