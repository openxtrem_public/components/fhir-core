<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Subscription Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRSubscriptionInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRSubscriptionChannel;

class FHIRSubscription extends FHIRDomainResource implements FHIRSubscriptionInterface
{
    public const RESOURCE_NAME = 'Subscription';

    protected ?FHIRCode $status = null;

    /** @var FHIRContactPoint[] */
    protected array $contact = [];
    protected ?FHIRInstant $end = null;
    protected ?FHIRString $reason = null;
    protected ?FHIRString $criteria = null;
    protected ?FHIRString $error = null;
    protected ?FHIRSubscriptionChannel $channel = null;

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactPoint ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactPoint ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    public function getEnd(): ?FHIRInstant
    {
        return $this->end;
    }

    public function setEnd(string|FHIRInstant|null $value): self
    {
        $this->end = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    public function getReason(): ?FHIRString
    {
        return $this->reason;
    }

    public function setReason(string|FHIRString|null $value): self
    {
        $this->reason = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCriteria(): ?FHIRString
    {
        return $this->criteria;
    }

    public function setCriteria(string|FHIRString|null $value): self
    {
        $this->criteria = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getError(): ?FHIRString
    {
        return $this->error;
    }

    public function setError(string|FHIRString|null $value): self
    {
        $this->error = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getChannel(): ?FHIRSubscriptionChannel
    {
        return $this->channel;
    }

    public function setChannel(?FHIRSubscriptionChannel $value): self
    {
        $this->channel = $value;

        return $this;
    }
}
