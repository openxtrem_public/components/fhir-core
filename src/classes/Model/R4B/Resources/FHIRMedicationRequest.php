<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationRequest Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMedicationRequestInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRDosage;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRMedicationRequestDispenseRequest;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRMedicationRequestSubstitution;

class FHIRMedicationRequest extends FHIRDomainResource implements FHIRMedicationRequestInterface
{
    public const RESOURCE_NAME = 'MedicationRequest';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $statusReason = null;
    protected ?FHIRCode $intent = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRCode $priority = null;
    protected ?FHIRBoolean $doNotPerform = null;
    protected FHIRBoolean|FHIRReference|null $reported = null;
    protected FHIRCodeableConcept|FHIRReference|null $medication = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $encounter = null;

    /** @var FHIRReference[] */
    protected array $supportingInformation = [];
    protected ?FHIRDateTime $authoredOn = null;
    protected ?FHIRReference $requester = null;
    protected ?FHIRReference $performer = null;
    protected ?FHIRCodeableConcept $performerType = null;
    protected ?FHIRReference $recorder = null;

    /** @var FHIRCodeableConcept[] */
    protected array $reasonCode = [];

    /** @var FHIRReference[] */
    protected array $reasonReference = [];

    /** @var FHIRCanonical[] */
    protected array $instantiatesCanonical = [];

    /** @var FHIRUri[] */
    protected array $instantiatesUri = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];
    protected ?FHIRIdentifier $groupIdentifier = null;
    protected ?FHIRCodeableConcept $courseOfTherapyType = null;

    /** @var FHIRReference[] */
    protected array $insurance = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRDosage[] */
    protected array $dosageInstruction = [];
    protected ?FHIRMedicationRequestDispenseRequest $dispenseRequest = null;
    protected ?FHIRMedicationRequestSubstitution $substitution = null;
    protected ?FHIRReference $priorPrescription = null;

    /** @var FHIRReference[] */
    protected array $detectedIssue = [];

    /** @var FHIRReference[] */
    protected array $eventHistory = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getStatusReason(): ?FHIRCodeableConcept
    {
        return $this->statusReason;
    }

    public function setStatusReason(?FHIRCodeableConcept $value): self
    {
        $this->statusReason = $value;

        return $this;
    }

    public function getIntent(): ?FHIRCode
    {
        return $this->intent;
    }

    public function setIntent(string|FHIRCode|null $value): self
    {
        $this->intent = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getPriority(): ?FHIRCode
    {
        return $this->priority;
    }

    public function setPriority(string|FHIRCode|null $value): self
    {
        $this->priority = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDoNotPerform(): ?FHIRBoolean
    {
        return $this->doNotPerform;
    }

    public function setDoNotPerform(bool|FHIRBoolean|null $value): self
    {
        $this->doNotPerform = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getReported(): FHIRBoolean|FHIRReference|null
    {
        return $this->reported;
    }

    public function setReported(FHIRBoolean|FHIRReference|null $value): self
    {
        $this->reported = $value;

        return $this;
    }

    public function getMedication(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->medication;
    }

    public function setMedication(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->medication = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupportingInformation(): array
    {
        return $this->supportingInformation;
    }

    public function setSupportingInformation(?FHIRReference ...$value): self
    {
        $this->supportingInformation = array_filter($value);

        return $this;
    }

    public function addSupportingInformation(?FHIRReference ...$value): self
    {
        $this->supportingInformation = array_filter(array_merge($this->supportingInformation, $value));

        return $this;
    }

    public function getAuthoredOn(): ?FHIRDateTime
    {
        return $this->authoredOn;
    }

    public function setAuthoredOn(string|FHIRDateTime|null $value): self
    {
        $this->authoredOn = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getRequester(): ?FHIRReference
    {
        return $this->requester;
    }

    public function setRequester(?FHIRReference $value): self
    {
        $this->requester = $value;

        return $this;
    }

    public function getPerformer(): ?FHIRReference
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRReference $value): self
    {
        $this->performer = $value;

        return $this;
    }

    public function getPerformerType(): ?FHIRCodeableConcept
    {
        return $this->performerType;
    }

    public function setPerformerType(?FHIRCodeableConcept $value): self
    {
        $this->performerType = $value;

        return $this;
    }

    public function getRecorder(): ?FHIRReference
    {
        return $this->recorder;
    }

    public function setRecorder(?FHIRReference $value): self
    {
        $this->recorder = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReasonCode(): array
    {
        return $this->reasonCode;
    }

    public function setReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter($value);

        return $this;
    }

    public function addReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter(array_merge($this->reasonCode, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReasonReference(): array
    {
        return $this->reasonReference;
    }

    public function setReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter($value);

        return $this;
    }

    public function addReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter(array_merge($this->reasonReference, $value));

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getInstantiatesCanonical(): array
    {
        return $this->instantiatesCanonical;
    }

    public function setInstantiatesCanonical(string|FHIRCanonical|null ...$value): self
    {
        $this->instantiatesCanonical = [];
        $this->addInstantiatesCanonical(...$value);

        return $this;
    }

    public function addInstantiatesCanonical(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->instantiatesCanonical = array_filter(array_merge($this->instantiatesCanonical, $values));

        return $this;
    }

    /**
     * @return FHIRUri[]
     */
    public function getInstantiatesUri(): array
    {
        return $this->instantiatesUri;
    }

    public function setInstantiatesUri(string|FHIRUri|null ...$value): self
    {
        $this->instantiatesUri = [];
        $this->addInstantiatesUri(...$value);

        return $this;
    }

    public function addInstantiatesUri(string|FHIRUri|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUri())->setValue($v) : $v, $value);

        $this->instantiatesUri = array_filter(array_merge($this->instantiatesUri, $values));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    public function getGroupIdentifier(): ?FHIRIdentifier
    {
        return $this->groupIdentifier;
    }

    public function setGroupIdentifier(?FHIRIdentifier $value): self
    {
        $this->groupIdentifier = $value;

        return $this;
    }

    public function getCourseOfTherapyType(): ?FHIRCodeableConcept
    {
        return $this->courseOfTherapyType;
    }

    public function setCourseOfTherapyType(?FHIRCodeableConcept $value): self
    {
        $this->courseOfTherapyType = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getInsurance(): array
    {
        return $this->insurance;
    }

    public function setInsurance(?FHIRReference ...$value): self
    {
        $this->insurance = array_filter($value);

        return $this;
    }

    public function addInsurance(?FHIRReference ...$value): self
    {
        $this->insurance = array_filter(array_merge($this->insurance, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRDosage[]
     */
    public function getDosageInstruction(): array
    {
        return $this->dosageInstruction;
    }

    public function setDosageInstruction(?FHIRDosage ...$value): self
    {
        $this->dosageInstruction = array_filter($value);

        return $this;
    }

    public function addDosageInstruction(?FHIRDosage ...$value): self
    {
        $this->dosageInstruction = array_filter(array_merge($this->dosageInstruction, $value));

        return $this;
    }

    public function getDispenseRequest(): ?FHIRMedicationRequestDispenseRequest
    {
        return $this->dispenseRequest;
    }

    public function setDispenseRequest(?FHIRMedicationRequestDispenseRequest $value): self
    {
        $this->dispenseRequest = $value;

        return $this;
    }

    public function getSubstitution(): ?FHIRMedicationRequestSubstitution
    {
        return $this->substitution;
    }

    public function setSubstitution(?FHIRMedicationRequestSubstitution $value): self
    {
        $this->substitution = $value;

        return $this;
    }

    public function getPriorPrescription(): ?FHIRReference
    {
        return $this->priorPrescription;
    }

    public function setPriorPrescription(?FHIRReference $value): self
    {
        $this->priorPrescription = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getDetectedIssue(): array
    {
        return $this->detectedIssue;
    }

    public function setDetectedIssue(?FHIRReference ...$value): self
    {
        $this->detectedIssue = array_filter($value);

        return $this;
    }

    public function addDetectedIssue(?FHIRReference ...$value): self
    {
        $this->detectedIssue = array_filter(array_merge($this->detectedIssue, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEventHistory(): array
    {
        return $this->eventHistory;
    }

    public function setEventHistory(?FHIRReference ...$value): self
    {
        $this->eventHistory = array_filter($value);

        return $this;
    }

    public function addEventHistory(?FHIRReference ...$value): self
    {
        $this->eventHistory = array_filter(array_merge($this->eventHistory, $value));

        return $this;
    }
}
