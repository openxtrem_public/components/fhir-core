<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PaymentNotice Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRPaymentNoticeInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;

class FHIRPaymentNotice extends FHIRDomainResource implements FHIRPaymentNoticeInterface
{
    public const RESOURCE_NAME = 'PaymentNotice';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRReference $request = null;
    protected ?FHIRReference $response = null;
    protected ?FHIRDateTime $created = null;
    protected ?FHIRReference $provider = null;
    protected ?FHIRReference $payment = null;
    protected ?FHIRDate $paymentDate = null;
    protected ?FHIRReference $payee = null;
    protected ?FHIRReference $recipient = null;
    protected ?FHIRMoney $amount = null;
    protected ?FHIRCodeableConcept $paymentStatus = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getRequest(): ?FHIRReference
    {
        return $this->request;
    }

    public function setRequest(?FHIRReference $value): self
    {
        $this->request = $value;

        return $this;
    }

    public function getResponse(): ?FHIRReference
    {
        return $this->response;
    }

    public function setResponse(?FHIRReference $value): self
    {
        $this->response = $value;

        return $this;
    }

    public function getCreated(): ?FHIRDateTime
    {
        return $this->created;
    }

    public function setCreated(string|FHIRDateTime|null $value): self
    {
        $this->created = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getProvider(): ?FHIRReference
    {
        return $this->provider;
    }

    public function setProvider(?FHIRReference $value): self
    {
        $this->provider = $value;

        return $this;
    }

    public function getPayment(): ?FHIRReference
    {
        return $this->payment;
    }

    public function setPayment(?FHIRReference $value): self
    {
        $this->payment = $value;

        return $this;
    }

    public function getPaymentDate(): ?FHIRDate
    {
        return $this->paymentDate;
    }

    public function setPaymentDate(string|FHIRDate|null $value): self
    {
        $this->paymentDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getPayee(): ?FHIRReference
    {
        return $this->payee;
    }

    public function setPayee(?FHIRReference $value): self
    {
        $this->payee = $value;

        return $this;
    }

    public function getRecipient(): ?FHIRReference
    {
        return $this->recipient;
    }

    public function setRecipient(?FHIRReference $value): self
    {
        $this->recipient = $value;

        return $this;
    }

    public function getAmount(): ?FHIRMoney
    {
        return $this->amount;
    }

    public function setAmount(?FHIRMoney $value): self
    {
        $this->amount = $value;

        return $this;
    }

    public function getPaymentStatus(): ?FHIRCodeableConcept
    {
        return $this->paymentStatus;
    }

    public function setPaymentStatus(?FHIRCodeableConcept $value): self
    {
        $this->paymentStatus = $value;

        return $this;
    }
}
