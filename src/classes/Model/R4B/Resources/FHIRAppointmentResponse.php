<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AppointmentResponse Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRAppointmentResponseInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRAppointmentResponse extends FHIRDomainResource implements FHIRAppointmentResponseInterface
{
    public const RESOURCE_NAME = 'AppointmentResponse';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRReference $appointment = null;
    protected ?FHIRInstant $start = null;
    protected ?FHIRInstant $end = null;

    /** @var FHIRCodeableConcept[] */
    protected array $participantType = [];
    protected ?FHIRReference $actor = null;
    protected ?FHIRCode $participantStatus = null;
    protected ?FHIRString $comment = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getAppointment(): ?FHIRReference
    {
        return $this->appointment;
    }

    public function setAppointment(?FHIRReference $value): self
    {
        $this->appointment = $value;

        return $this;
    }

    public function getStart(): ?FHIRInstant
    {
        return $this->start;
    }

    public function setStart(string|FHIRInstant|null $value): self
    {
        $this->start = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    public function getEnd(): ?FHIRInstant
    {
        return $this->end;
    }

    public function setEnd(string|FHIRInstant|null $value): self
    {
        $this->end = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getParticipantType(): array
    {
        return $this->participantType;
    }

    public function setParticipantType(?FHIRCodeableConcept ...$value): self
    {
        $this->participantType = array_filter($value);

        return $this;
    }

    public function addParticipantType(?FHIRCodeableConcept ...$value): self
    {
        $this->participantType = array_filter(array_merge($this->participantType, $value));

        return $this;
    }

    public function getActor(): ?FHIRReference
    {
        return $this->actor;
    }

    public function setActor(?FHIRReference $value): self
    {
        $this->actor = $value;

        return $this;
    }

    public function getParticipantStatus(): ?FHIRCode
    {
        return $this->participantStatus;
    }

    public function setParticipantStatus(string|FHIRCode|null $value): self
    {
        $this->participantStatus = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getComment(): ?FHIRString
    {
        return $this->comment;
    }

    public function setComment(string|FHIRString|null $value): self
    {
        $this->comment = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
