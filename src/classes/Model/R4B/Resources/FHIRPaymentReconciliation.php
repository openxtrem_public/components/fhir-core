<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PaymentReconciliation Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRPaymentReconciliationInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRPaymentReconciliationDetail;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRPaymentReconciliationProcessNote;

class FHIRPaymentReconciliation extends FHIRDomainResource implements FHIRPaymentReconciliationInterface
{
    public const RESOURCE_NAME = 'PaymentReconciliation';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRPeriod $period = null;
    protected ?FHIRDateTime $created = null;
    protected ?FHIRReference $paymentIssuer = null;
    protected ?FHIRReference $request = null;
    protected ?FHIRReference $requestor = null;
    protected ?FHIRCode $outcome = null;
    protected ?FHIRString $disposition = null;
    protected ?FHIRDate $paymentDate = null;
    protected ?FHIRMoney $paymentAmount = null;
    protected ?FHIRIdentifier $paymentIdentifier = null;

    /** @var FHIRPaymentReconciliationDetail[] */
    protected array $detail = [];
    protected ?FHIRCodeableConcept $formCode = null;

    /** @var FHIRPaymentReconciliationProcessNote[] */
    protected array $processNote = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    public function getCreated(): ?FHIRDateTime
    {
        return $this->created;
    }

    public function setCreated(string|FHIRDateTime|null $value): self
    {
        $this->created = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPaymentIssuer(): ?FHIRReference
    {
        return $this->paymentIssuer;
    }

    public function setPaymentIssuer(?FHIRReference $value): self
    {
        $this->paymentIssuer = $value;

        return $this;
    }

    public function getRequest(): ?FHIRReference
    {
        return $this->request;
    }

    public function setRequest(?FHIRReference $value): self
    {
        $this->request = $value;

        return $this;
    }

    public function getRequestor(): ?FHIRReference
    {
        return $this->requestor;
    }

    public function setRequestor(?FHIRReference $value): self
    {
        $this->requestor = $value;

        return $this;
    }

    public function getOutcome(): ?FHIRCode
    {
        return $this->outcome;
    }

    public function setOutcome(string|FHIRCode|null $value): self
    {
        $this->outcome = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDisposition(): ?FHIRString
    {
        return $this->disposition;
    }

    public function setDisposition(string|FHIRString|null $value): self
    {
        $this->disposition = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPaymentDate(): ?FHIRDate
    {
        return $this->paymentDate;
    }

    public function setPaymentDate(string|FHIRDate|null $value): self
    {
        $this->paymentDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getPaymentAmount(): ?FHIRMoney
    {
        return $this->paymentAmount;
    }

    public function setPaymentAmount(?FHIRMoney $value): self
    {
        $this->paymentAmount = $value;

        return $this;
    }

    public function getPaymentIdentifier(): ?FHIRIdentifier
    {
        return $this->paymentIdentifier;
    }

    public function setPaymentIdentifier(?FHIRIdentifier $value): self
    {
        $this->paymentIdentifier = $value;

        return $this;
    }

    /**
     * @return FHIRPaymentReconciliationDetail[]
     */
    public function getDetail(): array
    {
        return $this->detail;
    }

    public function setDetail(?FHIRPaymentReconciliationDetail ...$value): self
    {
        $this->detail = array_filter($value);

        return $this;
    }

    public function addDetail(?FHIRPaymentReconciliationDetail ...$value): self
    {
        $this->detail = array_filter(array_merge($this->detail, $value));

        return $this;
    }

    public function getFormCode(): ?FHIRCodeableConcept
    {
        return $this->formCode;
    }

    public function setFormCode(?FHIRCodeableConcept $value): self
    {
        $this->formCode = $value;

        return $this;
    }

    /**
     * @return FHIRPaymentReconciliationProcessNote[]
     */
    public function getProcessNote(): array
    {
        return $this->processNote;
    }

    public function setProcessNote(?FHIRPaymentReconciliationProcessNote ...$value): self
    {
        $this->processNote = array_filter($value);

        return $this;
    }

    public function addProcessNote(?FHIRPaymentReconciliationProcessNote ...$value): self
    {
        $this->processNote = array_filter(array_merge($this->processNote, $value));

        return $this;
    }
}
