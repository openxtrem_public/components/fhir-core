<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubscriptionStatus Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRSubscriptionStatusInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRSubscriptionStatusNotificationEvent;

class FHIRSubscriptionStatus extends FHIRDomainResource implements FHIRSubscriptionStatusInterface
{
    public const RESOURCE_NAME = 'SubscriptionStatus';

    protected ?FHIRCode $status = null;
    protected ?FHIRCode $type = null;
    protected ?FHIRString $eventsSinceSubscriptionStart = null;

    /** @var FHIRSubscriptionStatusNotificationEvent[] */
    protected array $notificationEvent = [];
    protected ?FHIRReference $subscription = null;
    protected ?FHIRCanonical $topic = null;

    /** @var FHIRCodeableConcept[] */
    protected array $error = [];

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getEventsSinceSubscriptionStart(): ?FHIRString
    {
        return $this->eventsSinceSubscriptionStart;
    }

    public function setEventsSinceSubscriptionStart(string|FHIRString|null $value): self
    {
        $this->eventsSinceSubscriptionStart = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRSubscriptionStatusNotificationEvent[]
     */
    public function getNotificationEvent(): array
    {
        return $this->notificationEvent;
    }

    public function setNotificationEvent(?FHIRSubscriptionStatusNotificationEvent ...$value): self
    {
        $this->notificationEvent = array_filter($value);

        return $this;
    }

    public function addNotificationEvent(?FHIRSubscriptionStatusNotificationEvent ...$value): self
    {
        $this->notificationEvent = array_filter(array_merge($this->notificationEvent, $value));

        return $this;
    }

    public function getSubscription(): ?FHIRReference
    {
        return $this->subscription;
    }

    public function setSubscription(?FHIRReference $value): self
    {
        $this->subscription = $value;

        return $this;
    }

    public function getTopic(): ?FHIRCanonical
    {
        return $this->topic;
    }

    public function setTopic(string|FHIRCanonical|null $value): self
    {
        $this->topic = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getError(): array
    {
        return $this->error;
    }

    public function setError(?FHIRCodeableConcept ...$value): self
    {
        $this->error = array_filter($value);

        return $this;
    }

    public function addError(?FHIRCodeableConcept ...$value): self
    {
        $this->error = array_filter(array_merge($this->error, $value));

        return $this;
    }
}
