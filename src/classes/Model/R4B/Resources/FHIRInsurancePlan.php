<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR InsurancePlan Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRInsurancePlanInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRInsurancePlanContact;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRInsurancePlanCoverage;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRInsurancePlanPlan;

class FHIRInsurancePlan extends FHIRDomainResource implements FHIRInsurancePlanInterface
{
    public const RESOURCE_NAME = 'InsurancePlan';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;

    /** @var FHIRCodeableConcept[] */
    protected array $type = [];
    protected ?FHIRString $name = null;

    /** @var FHIRString[] */
    protected array $alias = [];
    protected ?FHIRPeriod $period = null;
    protected ?FHIRReference $ownedBy = null;
    protected ?FHIRReference $administeredBy = null;

    /** @var FHIRReference[] */
    protected array $coverageArea = [];

    /** @var FHIRInsurancePlanContact[] */
    protected array $contact = [];

    /** @var FHIRReference[] */
    protected array $endpoint = [];

    /** @var FHIRReference[] */
    protected array $network = [];

    /** @var FHIRInsurancePlanCoverage[] */
    protected array $coverage = [];

    /** @var FHIRInsurancePlanPlan[] */
    protected array $plan = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getAlias(): array
    {
        return $this->alias;
    }

    public function setAlias(string|FHIRString|null ...$value): self
    {
        $this->alias = [];
        $this->addAlias(...$value);

        return $this;
    }

    public function addAlias(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->alias = array_filter(array_merge($this->alias, $values));

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    public function getOwnedBy(): ?FHIRReference
    {
        return $this->ownedBy;
    }

    public function setOwnedBy(?FHIRReference $value): self
    {
        $this->ownedBy = $value;

        return $this;
    }

    public function getAdministeredBy(): ?FHIRReference
    {
        return $this->administeredBy;
    }

    public function setAdministeredBy(?FHIRReference $value): self
    {
        $this->administeredBy = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getCoverageArea(): array
    {
        return $this->coverageArea;
    }

    public function setCoverageArea(?FHIRReference ...$value): self
    {
        $this->coverageArea = array_filter($value);

        return $this;
    }

    public function addCoverageArea(?FHIRReference ...$value): self
    {
        $this->coverageArea = array_filter(array_merge($this->coverageArea, $value));

        return $this;
    }

    /**
     * @return FHIRInsurancePlanContact[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRInsurancePlanContact ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRInsurancePlanContact ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEndpoint(): array
    {
        return $this->endpoint;
    }

    public function setEndpoint(?FHIRReference ...$value): self
    {
        $this->endpoint = array_filter($value);

        return $this;
    }

    public function addEndpoint(?FHIRReference ...$value): self
    {
        $this->endpoint = array_filter(array_merge($this->endpoint, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getNetwork(): array
    {
        return $this->network;
    }

    public function setNetwork(?FHIRReference ...$value): self
    {
        $this->network = array_filter($value);

        return $this;
    }

    public function addNetwork(?FHIRReference ...$value): self
    {
        $this->network = array_filter(array_merge($this->network, $value));

        return $this;
    }

    /**
     * @return FHIRInsurancePlanCoverage[]
     */
    public function getCoverage(): array
    {
        return $this->coverage;
    }

    public function setCoverage(?FHIRInsurancePlanCoverage ...$value): self
    {
        $this->coverage = array_filter($value);

        return $this;
    }

    public function addCoverage(?FHIRInsurancePlanCoverage ...$value): self
    {
        $this->coverage = array_filter(array_merge($this->coverage, $value));

        return $this;
    }

    /**
     * @return FHIRInsurancePlanPlan[]
     */
    public function getPlan(): array
    {
        return $this->plan;
    }

    public function setPlan(?FHIRInsurancePlanPlan ...$value): self
    {
        $this->plan = array_filter($value);

        return $this;
    }

    public function addPlan(?FHIRInsurancePlanPlan ...$value): self
    {
        $this->plan = array_filter(array_merge($this->plan, $value));

        return $this;
    }
}
