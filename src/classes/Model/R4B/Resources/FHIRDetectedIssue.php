<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DetectedIssue Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRDetectedIssueInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRDetectedIssueEvidence;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRDetectedIssueMitigation;

class FHIRDetectedIssue extends FHIRDomainResource implements FHIRDetectedIssueInterface
{
    public const RESOURCE_NAME = 'DetectedIssue';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRCode $severity = null;
    protected ?FHIRReference $patient = null;
    protected FHIRDateTime|FHIRPeriod|null $identified = null;
    protected ?FHIRReference $author = null;

    /** @var FHIRReference[] */
    protected array $implicated = [];

    /** @var FHIRDetectedIssueEvidence[] */
    protected array $evidence = [];
    protected ?FHIRString $detail = null;
    protected ?FHIRUri $reference = null;

    /** @var FHIRDetectedIssueMitigation[] */
    protected array $mitigation = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getSeverity(): ?FHIRCode
    {
        return $this->severity;
    }

    public function setSeverity(string|FHIRCode|null $value): self
    {
        $this->severity = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getIdentified(): FHIRDateTime|FHIRPeriod|null
    {
        return $this->identified;
    }

    public function setIdentified(FHIRDateTime|FHIRPeriod|null $value): self
    {
        $this->identified = $value;

        return $this;
    }

    public function getAuthor(): ?FHIRReference
    {
        return $this->author;
    }

    public function setAuthor(?FHIRReference $value): self
    {
        $this->author = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getImplicated(): array
    {
        return $this->implicated;
    }

    public function setImplicated(?FHIRReference ...$value): self
    {
        $this->implicated = array_filter($value);

        return $this;
    }

    public function addImplicated(?FHIRReference ...$value): self
    {
        $this->implicated = array_filter(array_merge($this->implicated, $value));

        return $this;
    }

    /**
     * @return FHIRDetectedIssueEvidence[]
     */
    public function getEvidence(): array
    {
        return $this->evidence;
    }

    public function setEvidence(?FHIRDetectedIssueEvidence ...$value): self
    {
        $this->evidence = array_filter($value);

        return $this;
    }

    public function addEvidence(?FHIRDetectedIssueEvidence ...$value): self
    {
        $this->evidence = array_filter(array_merge($this->evidence, $value));

        return $this;
    }

    public function getDetail(): ?FHIRString
    {
        return $this->detail;
    }

    public function setDetail(string|FHIRString|null $value): self
    {
        $this->detail = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getReference(): ?FHIRUri
    {
        return $this->reference;
    }

    public function setReference(string|FHIRUri|null $value): self
    {
        $this->reference = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRDetectedIssueMitigation[]
     */
    public function getMitigation(): array
    {
        return $this->mitigation;
    }

    public function setMitigation(?FHIRDetectedIssueMitigation ...$value): self
    {
        $this->mitigation = array_filter($value);

        return $this;
    }

    public function addMitigation(?FHIRDetectedIssueMitigation ...$value): self
    {
        $this->mitigation = array_filter(array_merge($this->mitigation, $value));

        return $this;
    }
}
