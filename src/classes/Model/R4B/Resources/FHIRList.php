<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR List Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRListInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRListEntry;

class FHIRList extends FHIRDomainResource implements FHIRListInterface
{
    public const RESOURCE_NAME = 'List';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCode $mode = null;
    protected ?FHIRString $title = null;
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $encounter = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRReference $source = null;
    protected ?FHIRCodeableConcept $orderedBy = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRListEntry[] */
    protected array $entry = [];
    protected ?FHIRCodeableConcept $emptyReason = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getMode(): ?FHIRCode
    {
        return $this->mode;
    }

    public function setMode(string|FHIRCode|null $value): self
    {
        $this->mode = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getSource(): ?FHIRReference
    {
        return $this->source;
    }

    public function setSource(?FHIRReference $value): self
    {
        $this->source = $value;

        return $this;
    }

    public function getOrderedBy(): ?FHIRCodeableConcept
    {
        return $this->orderedBy;
    }

    public function setOrderedBy(?FHIRCodeableConcept $value): self
    {
        $this->orderedBy = $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRListEntry[]
     */
    public function getEntry(): array
    {
        return $this->entry;
    }

    public function setEntry(?FHIRListEntry ...$value): self
    {
        $this->entry = array_filter($value);

        return $this;
    }

    public function addEntry(?FHIRListEntry ...$value): self
    {
        $this->entry = array_filter(array_merge($this->entry, $value));

        return $this;
    }

    public function getEmptyReason(): ?FHIRCodeableConcept
    {
        return $this->emptyReason;
    }

    public function setEmptyReason(?FHIRCodeableConcept $value): self
    {
        $this->emptyReason = $value;

        return $this;
    }
}
