<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationStatement Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMedicationStatementInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRDosage;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;

class FHIRMedicationStatement extends FHIRDomainResource implements FHIRMedicationStatementInterface
{
    public const RESOURCE_NAME = 'MedicationStatement';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];

    /** @var FHIRReference[] */
    protected array $partOf = [];
    protected ?FHIRCode $status = null;

    /** @var FHIRCodeableConcept[] */
    protected array $statusReason = [];
    protected ?FHIRCodeableConcept $category = null;
    protected FHIRCodeableConcept|FHIRReference|null $medication = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $context = null;
    protected FHIRDateTime|FHIRPeriod|null $effective = null;
    protected ?FHIRDateTime $dateAsserted = null;
    protected ?FHIRReference $informationSource = null;

    /** @var FHIRReference[] */
    protected array $derivedFrom = [];

    /** @var FHIRCodeableConcept[] */
    protected array $reasonCode = [];

    /** @var FHIRReference[] */
    protected array $reasonReference = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRDosage[] */
    protected array $dosage = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPartOf(): array
    {
        return $this->partOf;
    }

    public function setPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter($value);

        return $this;
    }

    public function addPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter(array_merge($this->partOf, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getStatusReason(): array
    {
        return $this->statusReason;
    }

    public function setStatusReason(?FHIRCodeableConcept ...$value): self
    {
        $this->statusReason = array_filter($value);

        return $this;
    }

    public function addStatusReason(?FHIRCodeableConcept ...$value): self
    {
        $this->statusReason = array_filter(array_merge($this->statusReason, $value));

        return $this;
    }

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getMedication(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->medication;
    }

    public function setMedication(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->medication = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getContext(): ?FHIRReference
    {
        return $this->context;
    }

    public function setContext(?FHIRReference $value): self
    {
        $this->context = $value;

        return $this;
    }

    public function getEffective(): FHIRDateTime|FHIRPeriod|null
    {
        return $this->effective;
    }

    public function setEffective(FHIRDateTime|FHIRPeriod|null $value): self
    {
        $this->effective = $value;

        return $this;
    }

    public function getDateAsserted(): ?FHIRDateTime
    {
        return $this->dateAsserted;
    }

    public function setDateAsserted(string|FHIRDateTime|null $value): self
    {
        $this->dateAsserted = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getInformationSource(): ?FHIRReference
    {
        return $this->informationSource;
    }

    public function setInformationSource(?FHIRReference $value): self
    {
        $this->informationSource = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getDerivedFrom(): array
    {
        return $this->derivedFrom;
    }

    public function setDerivedFrom(?FHIRReference ...$value): self
    {
        $this->derivedFrom = array_filter($value);

        return $this;
    }

    public function addDerivedFrom(?FHIRReference ...$value): self
    {
        $this->derivedFrom = array_filter(array_merge($this->derivedFrom, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReasonCode(): array
    {
        return $this->reasonCode;
    }

    public function setReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter($value);

        return $this;
    }

    public function addReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter(array_merge($this->reasonCode, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReasonReference(): array
    {
        return $this->reasonReference;
    }

    public function setReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter($value);

        return $this;
    }

    public function addReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter(array_merge($this->reasonReference, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRDosage[]
     */
    public function getDosage(): array
    {
        return $this->dosage;
    }

    public function setDosage(?FHIRDosage ...$value): self
    {
        $this->dosage = array_filter($value);

        return $this;
    }

    public function addDosage(?FHIRDosage ...$value): self
    {
        $this->dosage = array_filter(array_merge($this->dosage, $value));

        return $this;
    }
}
