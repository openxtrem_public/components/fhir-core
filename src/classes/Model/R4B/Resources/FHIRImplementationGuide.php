<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImplementationGuide Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRImplementationGuideInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRImplementationGuideDefinition;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRImplementationGuideDependsOn;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRImplementationGuideGlobal;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRImplementationGuideManifest;

class FHIRImplementationGuide extends FHIRDomainResource implements FHIRImplementationGuideInterface
{
    public const RESOURCE_NAME = 'ImplementationGuide';

    protected ?FHIRUri $url = null;
    protected ?FHIRString $version = null;
    protected ?FHIRString $name = null;
    protected ?FHIRString $title = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRBoolean $experimental = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRString $publisher = null;

    /** @var FHIRContactDetail[] */
    protected array $contact = [];
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRUsageContext[] */
    protected array $useContext = [];

    /** @var FHIRCodeableConcept[] */
    protected array $jurisdiction = [];
    protected ?FHIRMarkdown $copyright = null;
    protected ?FHIRId $packageId = null;
    protected ?FHIRCode $license = null;

    /** @var FHIRCode[] */
    protected array $fhirVersion = [];

    /** @var FHIRImplementationGuideDependsOn[] */
    protected array $dependsOn = [];

    /** @var FHIRImplementationGuideGlobal[] */
    protected array $global = [];
    protected ?FHIRImplementationGuideDefinition $definition = null;
    protected ?FHIRImplementationGuideManifest $manifest = null;

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getExperimental(): ?FHIRBoolean
    {
        return $this->experimental;
    }

    public function setExperimental(bool|FHIRBoolean|null $value): self
    {
        $this->experimental = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPublisher(): ?FHIRString
    {
        return $this->publisher;
    }

    public function setPublisher(string|FHIRString|null $value): self
    {
        $this->publisher = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUseContext(): array
    {
        return $this->useContext;
    }

    public function setUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter($value);

        return $this;
    }

    public function addUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter(array_merge($this->useContext, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getJurisdiction(): array
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter($value);

        return $this;
    }

    public function addJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter(array_merge($this->jurisdiction, $value));

        return $this;
    }

    public function getCopyright(): ?FHIRMarkdown
    {
        return $this->copyright;
    }

    public function setCopyright(string|FHIRMarkdown|null $value): self
    {
        $this->copyright = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getPackageId(): ?FHIRId
    {
        return $this->packageId;
    }

    public function setPackageId(string|FHIRId|null $value): self
    {
        $this->packageId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getLicense(): ?FHIRCode
    {
        return $this->license;
    }

    public function setLicense(string|FHIRCode|null $value): self
    {
        $this->license = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getFhirVersion(): array
    {
        return $this->fhirVersion;
    }

    public function setFhirVersion(string|FHIRCode|null ...$value): self
    {
        $this->fhirVersion = [];
        $this->addFhirVersion(...$value);

        return $this;
    }

    public function addFhirVersion(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->fhirVersion = array_filter(array_merge($this->fhirVersion, $values));

        return $this;
    }

    /**
     * @return FHIRImplementationGuideDependsOn[]
     */
    public function getDependsOn(): array
    {
        return $this->dependsOn;
    }

    public function setDependsOn(?FHIRImplementationGuideDependsOn ...$value): self
    {
        $this->dependsOn = array_filter($value);

        return $this;
    }

    public function addDependsOn(?FHIRImplementationGuideDependsOn ...$value): self
    {
        $this->dependsOn = array_filter(array_merge($this->dependsOn, $value));

        return $this;
    }

    /**
     * @return FHIRImplementationGuideGlobal[]
     */
    public function getGlobal(): array
    {
        return $this->global;
    }

    public function setGlobal(?FHIRImplementationGuideGlobal ...$value): self
    {
        $this->global = array_filter($value);

        return $this;
    }

    public function addGlobal(?FHIRImplementationGuideGlobal ...$value): self
    {
        $this->global = array_filter(array_merge($this->global, $value));

        return $this;
    }

    public function getDefinition(): ?FHIRImplementationGuideDefinition
    {
        return $this->definition;
    }

    public function setDefinition(?FHIRImplementationGuideDefinition $value): self
    {
        $this->definition = $value;

        return $this;
    }

    public function getManifest(): ?FHIRImplementationGuideManifest
    {
        return $this->manifest;
    }

    public function setManifest(?FHIRImplementationGuideManifest $value): self
    {
        $this->manifest = $value;

        return $this;
    }
}
