<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Evidence Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIREvidenceInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRRelatedArtifact;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIREvidenceCertainty;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIREvidenceStatistic;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIREvidenceVariableDefinition;

class FHIREvidence extends FHIRDomainResource implements FHIREvidenceInterface
{
    public const RESOURCE_NAME = 'Evidence';

    protected ?FHIRUri $url = null;

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRString $version = null;
    protected ?FHIRString $title = null;
    protected FHIRReference|FHIRMarkdown|null $citeAs = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRDateTime $date = null;

    /** @var FHIRUsageContext[] */
    protected array $useContext = [];
    protected ?FHIRDate $approvalDate = null;
    protected ?FHIRDate $lastReviewDate = null;
    protected ?FHIRString $publisher = null;

    /** @var FHIRContactDetail[] */
    protected array $contact = [];

    /** @var FHIRContactDetail[] */
    protected array $author = [];

    /** @var FHIRContactDetail[] */
    protected array $editor = [];

    /** @var FHIRContactDetail[] */
    protected array $reviewer = [];

    /** @var FHIRContactDetail[] */
    protected array $endorser = [];

    /** @var FHIRRelatedArtifact[] */
    protected array $relatedArtifact = [];
    protected ?FHIRMarkdown $description = null;
    protected ?FHIRMarkdown $assertion = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIREvidenceVariableDefinition[] */
    protected array $variableDefinition = [];
    protected ?FHIRCodeableConcept $synthesisType = null;
    protected ?FHIRCodeableConcept $studyType = null;

    /** @var FHIREvidenceStatistic[] */
    protected array $statistic = [];

    /** @var FHIREvidenceCertainty[] */
    protected array $certainty = [];

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCiteAs(): FHIRReference|FHIRMarkdown|null
    {
        return $this->citeAs;
    }

    public function setCiteAs(FHIRReference|FHIRMarkdown|null $value): self
    {
        $this->citeAs = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUseContext(): array
    {
        return $this->useContext;
    }

    public function setUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter($value);

        return $this;
    }

    public function addUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter(array_merge($this->useContext, $value));

        return $this;
    }

    public function getApprovalDate(): ?FHIRDate
    {
        return $this->approvalDate;
    }

    public function setApprovalDate(string|FHIRDate|null $value): self
    {
        $this->approvalDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getLastReviewDate(): ?FHIRDate
    {
        return $this->lastReviewDate;
    }

    public function setLastReviewDate(string|FHIRDate|null $value): self
    {
        $this->lastReviewDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getPublisher(): ?FHIRString
    {
        return $this->publisher;
    }

    public function setPublisher(string|FHIRString|null $value): self
    {
        $this->publisher = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getAuthor(): array
    {
        return $this->author;
    }

    public function setAuthor(?FHIRContactDetail ...$value): self
    {
        $this->author = array_filter($value);

        return $this;
    }

    public function addAuthor(?FHIRContactDetail ...$value): self
    {
        $this->author = array_filter(array_merge($this->author, $value));

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getEditor(): array
    {
        return $this->editor;
    }

    public function setEditor(?FHIRContactDetail ...$value): self
    {
        $this->editor = array_filter($value);

        return $this;
    }

    public function addEditor(?FHIRContactDetail ...$value): self
    {
        $this->editor = array_filter(array_merge($this->editor, $value));

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getReviewer(): array
    {
        return $this->reviewer;
    }

    public function setReviewer(?FHIRContactDetail ...$value): self
    {
        $this->reviewer = array_filter($value);

        return $this;
    }

    public function addReviewer(?FHIRContactDetail ...$value): self
    {
        $this->reviewer = array_filter(array_merge($this->reviewer, $value));

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getEndorser(): array
    {
        return $this->endorser;
    }

    public function setEndorser(?FHIRContactDetail ...$value): self
    {
        $this->endorser = array_filter($value);

        return $this;
    }

    public function addEndorser(?FHIRContactDetail ...$value): self
    {
        $this->endorser = array_filter(array_merge($this->endorser, $value));

        return $this;
    }

    /**
     * @return FHIRRelatedArtifact[]
     */
    public function getRelatedArtifact(): array
    {
        return $this->relatedArtifact;
    }

    public function setRelatedArtifact(?FHIRRelatedArtifact ...$value): self
    {
        $this->relatedArtifact = array_filter($value);

        return $this;
    }

    public function addRelatedArtifact(?FHIRRelatedArtifact ...$value): self
    {
        $this->relatedArtifact = array_filter(array_merge($this->relatedArtifact, $value));

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getAssertion(): ?FHIRMarkdown
    {
        return $this->assertion;
    }

    public function setAssertion(string|FHIRMarkdown|null $value): self
    {
        $this->assertion = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIREvidenceVariableDefinition[]
     */
    public function getVariableDefinition(): array
    {
        return $this->variableDefinition;
    }

    public function setVariableDefinition(?FHIREvidenceVariableDefinition ...$value): self
    {
        $this->variableDefinition = array_filter($value);

        return $this;
    }

    public function addVariableDefinition(?FHIREvidenceVariableDefinition ...$value): self
    {
        $this->variableDefinition = array_filter(array_merge($this->variableDefinition, $value));

        return $this;
    }

    public function getSynthesisType(): ?FHIRCodeableConcept
    {
        return $this->synthesisType;
    }

    public function setSynthesisType(?FHIRCodeableConcept $value): self
    {
        $this->synthesisType = $value;

        return $this;
    }

    public function getStudyType(): ?FHIRCodeableConcept
    {
        return $this->studyType;
    }

    public function setStudyType(?FHIRCodeableConcept $value): self
    {
        $this->studyType = $value;

        return $this;
    }

    /**
     * @return FHIREvidenceStatistic[]
     */
    public function getStatistic(): array
    {
        return $this->statistic;
    }

    public function setStatistic(?FHIREvidenceStatistic ...$value): self
    {
        $this->statistic = array_filter($value);

        return $this;
    }

    public function addStatistic(?FHIREvidenceStatistic ...$value): self
    {
        $this->statistic = array_filter(array_merge($this->statistic, $value));

        return $this;
    }

    /**
     * @return FHIREvidenceCertainty[]
     */
    public function getCertainty(): array
    {
        return $this->certainty;
    }

    public function setCertainty(?FHIREvidenceCertainty ...$value): self
    {
        $this->certainty = array_filter($value);

        return $this;
    }

    public function addCertainty(?FHIREvidenceCertainty ...$value): self
    {
        $this->certainty = array_filter(array_merge($this->certainty, $value));

        return $this;
    }
}
