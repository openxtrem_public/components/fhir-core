<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PackagedProductDefinition Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRPackagedProductDefinitionInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRMarketingStatus;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRPackagedProductDefinitionLegalStatusOfSupply;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRPackagedProductDefinitionPackage;

class FHIRPackagedProductDefinition extends FHIRDomainResource implements FHIRPackagedProductDefinitionInterface
{
    public const RESOURCE_NAME = 'PackagedProductDefinition';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRString $name = null;
    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRReference[] */
    protected array $packageFor = [];
    protected ?FHIRCodeableConcept $status = null;
    protected ?FHIRDateTime $statusDate = null;

    /** @var FHIRQuantity[] */
    protected array $containedItemQuantity = [];
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRPackagedProductDefinitionLegalStatusOfSupply[] */
    protected array $legalStatusOfSupply = [];

    /** @var FHIRMarketingStatus[] */
    protected array $marketingStatus = [];

    /** @var FHIRCodeableConcept[] */
    protected array $characteristic = [];
    protected ?FHIRBoolean $copackagedIndicator = null;

    /** @var FHIRReference[] */
    protected array $manufacturer = [];
    protected ?FHIRPackagedProductDefinitionPackage $package = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPackageFor(): array
    {
        return $this->packageFor;
    }

    public function setPackageFor(?FHIRReference ...$value): self
    {
        $this->packageFor = array_filter($value);

        return $this;
    }

    public function addPackageFor(?FHIRReference ...$value): self
    {
        $this->packageFor = array_filter(array_merge($this->packageFor, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCodeableConcept
    {
        return $this->status;
    }

    public function setStatus(?FHIRCodeableConcept $value): self
    {
        $this->status = $value;

        return $this;
    }

    public function getStatusDate(): ?FHIRDateTime
    {
        return $this->statusDate;
    }

    public function setStatusDate(string|FHIRDateTime|null $value): self
    {
        $this->statusDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRQuantity[]
     */
    public function getContainedItemQuantity(): array
    {
        return $this->containedItemQuantity;
    }

    public function setContainedItemQuantity(?FHIRQuantity ...$value): self
    {
        $this->containedItemQuantity = array_filter($value);

        return $this;
    }

    public function addContainedItemQuantity(?FHIRQuantity ...$value): self
    {
        $this->containedItemQuantity = array_filter(array_merge($this->containedItemQuantity, $value));

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRPackagedProductDefinitionLegalStatusOfSupply[]
     */
    public function getLegalStatusOfSupply(): array
    {
        return $this->legalStatusOfSupply;
    }

    public function setLegalStatusOfSupply(?FHIRPackagedProductDefinitionLegalStatusOfSupply ...$value): self
    {
        $this->legalStatusOfSupply = array_filter($value);

        return $this;
    }

    public function addLegalStatusOfSupply(?FHIRPackagedProductDefinitionLegalStatusOfSupply ...$value): self
    {
        $this->legalStatusOfSupply = array_filter(array_merge($this->legalStatusOfSupply, $value));

        return $this;
    }

    /**
     * @return FHIRMarketingStatus[]
     */
    public function getMarketingStatus(): array
    {
        return $this->marketingStatus;
    }

    public function setMarketingStatus(?FHIRMarketingStatus ...$value): self
    {
        $this->marketingStatus = array_filter($value);

        return $this;
    }

    public function addMarketingStatus(?FHIRMarketingStatus ...$value): self
    {
        $this->marketingStatus = array_filter(array_merge($this->marketingStatus, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCharacteristic(): array
    {
        return $this->characteristic;
    }

    public function setCharacteristic(?FHIRCodeableConcept ...$value): self
    {
        $this->characteristic = array_filter($value);

        return $this;
    }

    public function addCharacteristic(?FHIRCodeableConcept ...$value): self
    {
        $this->characteristic = array_filter(array_merge($this->characteristic, $value));

        return $this;
    }

    public function getCopackagedIndicator(): ?FHIRBoolean
    {
        return $this->copackagedIndicator;
    }

    public function setCopackagedIndicator(bool|FHIRBoolean|null $value): self
    {
        $this->copackagedIndicator = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getManufacturer(): array
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?FHIRReference ...$value): self
    {
        $this->manufacturer = array_filter($value);

        return $this;
    }

    public function addManufacturer(?FHIRReference ...$value): self
    {
        $this->manufacturer = array_filter(array_merge($this->manufacturer, $value));

        return $this;
    }

    public function getPackage(): ?FHIRPackagedProductDefinitionPackage
    {
        return $this->package;
    }

    public function setPackage(?FHIRPackagedProductDefinitionPackage $value): self
    {
        $this->package = $value;

        return $this;
    }
}
