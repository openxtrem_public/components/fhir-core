<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExampleScenarioProcessStep Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExampleScenarioProcessStepInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;

class FHIRExampleScenarioProcessStep extends FHIRBackboneElement implements FHIRExampleScenarioProcessStepInterface
{
    public const RESOURCE_NAME = 'ExampleScenario.process.step';

    /** @var FHIRExampleScenarioProcess[] */
    protected array $process = [];
    protected ?FHIRBoolean $pause = null;
    protected ?FHIRExampleScenarioProcessStepOperation $operation = null;

    /** @var FHIRExampleScenarioProcessStepAlternative[] */
    protected array $alternative = [];

    /**
     * @return FHIRExampleScenarioProcess[]
     */
    public function getProcess(): array
    {
        return $this->process;
    }

    public function setProcess(?FHIRExampleScenarioProcess ...$value): self
    {
        $this->process = array_filter($value);

        return $this;
    }

    public function addProcess(?FHIRExampleScenarioProcess ...$value): self
    {
        $this->process = array_filter(array_merge($this->process, $value));

        return $this;
    }

    public function getPause(): ?FHIRBoolean
    {
        return $this->pause;
    }

    public function setPause(bool|FHIRBoolean|null $value): self
    {
        $this->pause = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getOperation(): ?FHIRExampleScenarioProcessStepOperation
    {
        return $this->operation;
    }

    public function setOperation(?FHIRExampleScenarioProcessStepOperation $value): self
    {
        $this->operation = $value;

        return $this;
    }

    /**
     * @return FHIRExampleScenarioProcessStepAlternative[]
     */
    public function getAlternative(): array
    {
        return $this->alternative;
    }

    public function setAlternative(?FHIRExampleScenarioProcessStepAlternative ...$value): self
    {
        $this->alternative = array_filter($value);

        return $this;
    }

    public function addAlternative(?FHIRExampleScenarioProcessStepAlternative ...$value): self
    {
        $this->alternative = array_filter(array_merge($this->alternative, $value));

        return $this;
    }
}
