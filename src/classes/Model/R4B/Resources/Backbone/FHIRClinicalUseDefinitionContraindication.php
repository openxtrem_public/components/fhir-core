<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClinicalUseDefinitionContraindication Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRClinicalUseDefinitionContraindicationInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;

class FHIRClinicalUseDefinitionContraindication extends FHIRBackboneElement implements FHIRClinicalUseDefinitionContraindicationInterface
{
    public const RESOURCE_NAME = 'ClinicalUseDefinition.contraindication';

    protected ?FHIRCodeableReference $diseaseSymptomProcedure = null;
    protected ?FHIRCodeableReference $diseaseStatus = null;

    /** @var FHIRCodeableReference[] */
    protected array $comorbidity = [];

    /** @var FHIRReference[] */
    protected array $indication = [];

    /** @var FHIRClinicalUseDefinitionContraindicationOtherTherapy[] */
    protected array $otherTherapy = [];

    public function getDiseaseSymptomProcedure(): ?FHIRCodeableReference
    {
        return $this->diseaseSymptomProcedure;
    }

    public function setDiseaseSymptomProcedure(?FHIRCodeableReference $value): self
    {
        $this->diseaseSymptomProcedure = $value;

        return $this;
    }

    public function getDiseaseStatus(): ?FHIRCodeableReference
    {
        return $this->diseaseStatus;
    }

    public function setDiseaseStatus(?FHIRCodeableReference $value): self
    {
        $this->diseaseStatus = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableReference[]
     */
    public function getComorbidity(): array
    {
        return $this->comorbidity;
    }

    public function setComorbidity(?FHIRCodeableReference ...$value): self
    {
        $this->comorbidity = array_filter($value);

        return $this;
    }

    public function addComorbidity(?FHIRCodeableReference ...$value): self
    {
        $this->comorbidity = array_filter(array_merge($this->comorbidity, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getIndication(): array
    {
        return $this->indication;
    }

    public function setIndication(?FHIRReference ...$value): self
    {
        $this->indication = array_filter($value);

        return $this;
    }

    public function addIndication(?FHIRReference ...$value): self
    {
        $this->indication = array_filter(array_merge($this->indication, $value));

        return $this;
    }

    /**
     * @return FHIRClinicalUseDefinitionContraindicationOtherTherapy[]
     */
    public function getOtherTherapy(): array
    {
        return $this->otherTherapy;
    }

    public function setOtherTherapy(?FHIRClinicalUseDefinitionContraindicationOtherTherapy ...$value): self
    {
        $this->otherTherapy = array_filter($value);

        return $this;
    }

    public function addOtherTherapy(?FHIRClinicalUseDefinitionContraindicationOtherTherapy ...$value): self
    {
        $this->otherTherapy = array_filter(array_merge($this->otherTherapy, $value));

        return $this;
    }
}
