<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImplementationGuideManifest Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImplementationGuideManifestInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUrl;

class FHIRImplementationGuideManifest extends FHIRBackboneElement implements FHIRImplementationGuideManifestInterface
{
    public const RESOURCE_NAME = 'ImplementationGuide.manifest';

    protected ?FHIRUrl $rendering = null;

    /** @var FHIRImplementationGuideManifestResource[] */
    protected array $resource = [];

    /** @var FHIRImplementationGuideManifestPage[] */
    protected array $page = [];

    /** @var FHIRString[] */
    protected array $image = [];

    /** @var FHIRString[] */
    protected array $other = [];

    public function getRendering(): ?FHIRUrl
    {
        return $this->rendering;
    }

    public function setRendering(string|FHIRUrl|null $value): self
    {
        $this->rendering = is_string($value) ? (new FHIRUrl())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRImplementationGuideManifestResource[]
     */
    public function getResource(): array
    {
        return $this->resource;
    }

    public function setResource(?FHIRImplementationGuideManifestResource ...$value): self
    {
        $this->resource = array_filter($value);

        return $this;
    }

    public function addResource(?FHIRImplementationGuideManifestResource ...$value): self
    {
        $this->resource = array_filter(array_merge($this->resource, $value));

        return $this;
    }

    /**
     * @return FHIRImplementationGuideManifestPage[]
     */
    public function getPage(): array
    {
        return $this->page;
    }

    public function setPage(?FHIRImplementationGuideManifestPage ...$value): self
    {
        $this->page = array_filter($value);

        return $this;
    }

    public function addPage(?FHIRImplementationGuideManifestPage ...$value): self
    {
        $this->page = array_filter(array_merge($this->page, $value));

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getImage(): array
    {
        return $this->image;
    }

    public function setImage(string|FHIRString|null ...$value): self
    {
        $this->image = [];
        $this->addImage(...$value);

        return $this;
    }

    public function addImage(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->image = array_filter(array_merge($this->image, $values));

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getOther(): array
    {
        return $this->other;
    }

    public function setOther(string|FHIRString|null ...$value): self
    {
        $this->other = [];
        $this->addOther(...$value);

        return $this;
    }

    public function addOther(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->other = array_filter(array_merge($this->other, $values));

        return $this;
    }
}
