<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR InsurancePlanCoverage Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRInsurancePlanCoverageInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;

class FHIRInsurancePlanCoverage extends FHIRBackboneElement implements FHIRInsurancePlanCoverageInterface
{
    public const RESOURCE_NAME = 'InsurancePlan.coverage';

    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRReference[] */
    protected array $network = [];

    /** @var FHIRInsurancePlanCoverageBenefit[] */
    protected array $benefit = [];

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getNetwork(): array
    {
        return $this->network;
    }

    public function setNetwork(?FHIRReference ...$value): self
    {
        $this->network = array_filter($value);

        return $this;
    }

    public function addNetwork(?FHIRReference ...$value): self
    {
        $this->network = array_filter(array_merge($this->network, $value));

        return $this;
    }

    /**
     * @return FHIRInsurancePlanCoverageBenefit[]
     */
    public function getBenefit(): array
    {
        return $this->benefit;
    }

    public function setBenefit(?FHIRInsurancePlanCoverageBenefit ...$value): self
    {
        $this->benefit = array_filter($value);

        return $this;
    }

    public function addBenefit(?FHIRInsurancePlanCoverageBenefit ...$value): self
    {
        $this->benefit = array_filter(array_merge($this->benefit, $value));

        return $this;
    }
}
