<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MolecularSequenceQualityRoc Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMolecularSequenceQualityRocInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRInteger;

class FHIRMolecularSequenceQualityRoc extends FHIRBackboneElement implements FHIRMolecularSequenceQualityRocInterface
{
    public const RESOURCE_NAME = 'MolecularSequence.quality.roc';

    /** @var FHIRInteger[] */
    protected array $score = [];

    /** @var FHIRInteger[] */
    protected array $numTP = [];

    /** @var FHIRInteger[] */
    protected array $numFP = [];

    /** @var FHIRInteger[] */
    protected array $numFN = [];

    /** @var FHIRDecimal[] */
    protected array $precision = [];

    /** @var FHIRDecimal[] */
    protected array $sensitivity = [];

    /** @var FHIRDecimal[] */
    protected array $fMeasure = [];

    /**
     * @return FHIRInteger[]
     */
    public function getScore(): array
    {
        return $this->score;
    }

    public function setScore(int|FHIRInteger|null ...$value): self
    {
        $this->score = [];
        $this->addScore(...$value);

        return $this;
    }

    public function addScore(int|FHIRInteger|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRInteger())->setValue($v) : $v, $value);

        $this->score = array_filter(array_merge($this->score, $values));

        return $this;
    }

    /**
     * @return FHIRInteger[]
     */
    public function getNumTP(): array
    {
        return $this->numTP;
    }

    public function setNumTP(int|FHIRInteger|null ...$value): self
    {
        $this->numTP = [];
        $this->addNumTP(...$value);

        return $this;
    }

    public function addNumTP(int|FHIRInteger|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRInteger())->setValue($v) : $v, $value);

        $this->numTP = array_filter(array_merge($this->numTP, $values));

        return $this;
    }

    /**
     * @return FHIRInteger[]
     */
    public function getNumFP(): array
    {
        return $this->numFP;
    }

    public function setNumFP(int|FHIRInteger|null ...$value): self
    {
        $this->numFP = [];
        $this->addNumFP(...$value);

        return $this;
    }

    public function addNumFP(int|FHIRInteger|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRInteger())->setValue($v) : $v, $value);

        $this->numFP = array_filter(array_merge($this->numFP, $values));

        return $this;
    }

    /**
     * @return FHIRInteger[]
     */
    public function getNumFN(): array
    {
        return $this->numFN;
    }

    public function setNumFN(int|FHIRInteger|null ...$value): self
    {
        $this->numFN = [];
        $this->addNumFN(...$value);

        return $this;
    }

    public function addNumFN(int|FHIRInteger|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRInteger())->setValue($v) : $v, $value);

        $this->numFN = array_filter(array_merge($this->numFN, $values));

        return $this;
    }

    /**
     * @return FHIRDecimal[]
     */
    public function getPrecision(): array
    {
        return $this->precision;
    }

    public function setPrecision(float|FHIRDecimal|null ...$value): self
    {
        $this->precision = [];
        $this->addPrecision(...$value);

        return $this;
    }

    public function addPrecision(float|FHIRDecimal|null ...$value): self
    {
        $values = array_map(fn($v) => is_float($v) ? (new FHIRDecimal())->setValue($v) : $v, $value);

        $this->precision = array_filter(array_merge($this->precision, $values));

        return $this;
    }

    /**
     * @return FHIRDecimal[]
     */
    public function getSensitivity(): array
    {
        return $this->sensitivity;
    }

    public function setSensitivity(float|FHIRDecimal|null ...$value): self
    {
        $this->sensitivity = [];
        $this->addSensitivity(...$value);

        return $this;
    }

    public function addSensitivity(float|FHIRDecimal|null ...$value): self
    {
        $values = array_map(fn($v) => is_float($v) ? (new FHIRDecimal())->setValue($v) : $v, $value);

        $this->sensitivity = array_filter(array_merge($this->sensitivity, $values));

        return $this;
    }

    /**
     * @return FHIRDecimal[]
     */
    public function getFMeasure(): array
    {
        return $this->fMeasure;
    }

    public function setFMeasure(float|FHIRDecimal|null ...$value): self
    {
        $this->fMeasure = [];
        $this->addFMeasure(...$value);

        return $this;
    }

    public function addFMeasure(float|FHIRDecimal|null ...$value): self
    {
        $values = array_map(fn($v) => is_float($v) ? (new FHIRDecimal())->setValue($v) : $v, $value);

        $this->fMeasure = array_filter(array_merge($this->fMeasure, $values));

        return $this;
    }
}
