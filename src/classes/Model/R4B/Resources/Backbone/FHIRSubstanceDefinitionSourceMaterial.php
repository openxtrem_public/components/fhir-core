<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceDefinitionSourceMaterial Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceDefinitionSourceMaterialInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;

class FHIRSubstanceDefinitionSourceMaterial extends FHIRBackboneElement implements FHIRSubstanceDefinitionSourceMaterialInterface
{
    public const RESOURCE_NAME = 'SubstanceDefinition.sourceMaterial';

    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableConcept $genus = null;
    protected ?FHIRCodeableConcept $species = null;
    protected ?FHIRCodeableConcept $part = null;

    /** @var FHIRCodeableConcept[] */
    protected array $countryOfOrigin = [];

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getGenus(): ?FHIRCodeableConcept
    {
        return $this->genus;
    }

    public function setGenus(?FHIRCodeableConcept $value): self
    {
        $this->genus = $value;

        return $this;
    }

    public function getSpecies(): ?FHIRCodeableConcept
    {
        return $this->species;
    }

    public function setSpecies(?FHIRCodeableConcept $value): self
    {
        $this->species = $value;

        return $this;
    }

    public function getPart(): ?FHIRCodeableConcept
    {
        return $this->part;
    }

    public function setPart(?FHIRCodeableConcept $value): self
    {
        $this->part = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCountryOfOrigin(): array
    {
        return $this->countryOfOrigin;
    }

    public function setCountryOfOrigin(?FHIRCodeableConcept ...$value): self
    {
        $this->countryOfOrigin = array_filter($value);

        return $this;
    }

    public function addCountryOfOrigin(?FHIRCodeableConcept ...$value): self
    {
        $this->countryOfOrigin = array_filter(array_merge($this->countryOfOrigin, $value));

        return $this;
    }
}
