<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CommunicationRequestPayload Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCommunicationRequestPayloadInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRCommunicationRequestPayload extends FHIRBackboneElement implements FHIRCommunicationRequestPayloadInterface
{
    public const RESOURCE_NAME = 'CommunicationRequest.payload';

    protected FHIRString|FHIRAttachment|FHIRReference|null $content = null;

    public function getContent(): FHIRString|FHIRAttachment|FHIRReference|null
    {
        return $this->content;
    }

    public function setContent(FHIRString|FHIRAttachment|FHIRReference|null $value): self
    {
        $this->content = $value;

        return $this;
    }
}
