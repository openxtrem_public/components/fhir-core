<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR QuestionnaireResponseItem Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRQuestionnaireResponseItemInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUri;

class FHIRQuestionnaireResponseItem extends FHIRBackboneElement implements FHIRQuestionnaireResponseItemInterface
{
    public const RESOURCE_NAME = 'QuestionnaireResponse.item';

    protected ?FHIRString $linkId = null;
    protected ?FHIRUri $definition = null;
    protected ?FHIRString $text = null;

    /** @var FHIRQuestionnaireResponseItemAnswer[] */
    protected array $answer = [];

    /** @var FHIRQuestionnaireResponseItem[] */
    protected array $item = [];

    public function getLinkId(): ?FHIRString
    {
        return $this->linkId;
    }

    public function setLinkId(string|FHIRString|null $value): self
    {
        $this->linkId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDefinition(): ?FHIRUri
    {
        return $this->definition;
    }

    public function setDefinition(string|FHIRUri|null $value): self
    {
        $this->definition = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getText(): ?FHIRString
    {
        return $this->text;
    }

    public function setText(string|FHIRString|null $value): self
    {
        $this->text = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRQuestionnaireResponseItemAnswer[]
     */
    public function getAnswer(): array
    {
        return $this->answer;
    }

    public function setAnswer(?FHIRQuestionnaireResponseItemAnswer ...$value): self
    {
        $this->answer = array_filter($value);

        return $this;
    }

    public function addAnswer(?FHIRQuestionnaireResponseItemAnswer ...$value): self
    {
        $this->answer = array_filter(array_merge($this->answer, $value));

        return $this;
    }

    /**
     * @return FHIRQuestionnaireResponseItem[]
     */
    public function getItem(): array
    {
        return $this->item;
    }

    public function setItem(?FHIRQuestionnaireResponseItem ...$value): self
    {
        $this->item = array_filter($value);

        return $this;
    }

    public function addItem(?FHIRQuestionnaireResponseItem ...$value): self
    {
        $this->item = array_filter(array_merge($this->item, $value));

        return $this;
    }
}
