<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CapabilityStatementRest Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCapabilityStatementRestInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRMarkdown;

class FHIRCapabilityStatementRest extends FHIRBackboneElement implements FHIRCapabilityStatementRestInterface
{
    public const RESOURCE_NAME = 'CapabilityStatement.rest';

    protected ?FHIRCode $mode = null;
    protected ?FHIRMarkdown $documentation = null;
    protected ?FHIRCapabilityStatementRestSecurity $security = null;

    /** @var FHIRCapabilityStatementRestResource[] */
    protected array $resource = [];

    /** @var FHIRCapabilityStatementRestInteraction[] */
    protected array $interaction = [];

    /** @var FHIRCapabilityStatementRestResourceSearchParam[] */
    protected array $searchParam = [];

    /** @var FHIRCapabilityStatementRestResourceOperation[] */
    protected array $operation = [];

    /** @var FHIRCanonical[] */
    protected array $compartment = [];

    public function getMode(): ?FHIRCode
    {
        return $this->mode;
    }

    public function setMode(string|FHIRCode|null $value): self
    {
        $this->mode = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDocumentation(): ?FHIRMarkdown
    {
        return $this->documentation;
    }

    public function setDocumentation(string|FHIRMarkdown|null $value): self
    {
        $this->documentation = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getSecurity(): ?FHIRCapabilityStatementRestSecurity
    {
        return $this->security;
    }

    public function setSecurity(?FHIRCapabilityStatementRestSecurity $value): self
    {
        $this->security = $value;

        return $this;
    }

    /**
     * @return FHIRCapabilityStatementRestResource[]
     */
    public function getResource(): array
    {
        return $this->resource;
    }

    public function setResource(?FHIRCapabilityStatementRestResource ...$value): self
    {
        $this->resource = array_filter($value);

        return $this;
    }

    public function addResource(?FHIRCapabilityStatementRestResource ...$value): self
    {
        $this->resource = array_filter(array_merge($this->resource, $value));

        return $this;
    }

    /**
     * @return FHIRCapabilityStatementRestInteraction[]
     */
    public function getInteraction(): array
    {
        return $this->interaction;
    }

    public function setInteraction(?FHIRCapabilityStatementRestInteraction ...$value): self
    {
        $this->interaction = array_filter($value);

        return $this;
    }

    public function addInteraction(?FHIRCapabilityStatementRestInteraction ...$value): self
    {
        $this->interaction = array_filter(array_merge($this->interaction, $value));

        return $this;
    }

    /**
     * @return FHIRCapabilityStatementRestResourceSearchParam[]
     */
    public function getSearchParam(): array
    {
        return $this->searchParam;
    }

    public function setSearchParam(?FHIRCapabilityStatementRestResourceSearchParam ...$value): self
    {
        $this->searchParam = array_filter($value);

        return $this;
    }

    public function addSearchParam(?FHIRCapabilityStatementRestResourceSearchParam ...$value): self
    {
        $this->searchParam = array_filter(array_merge($this->searchParam, $value));

        return $this;
    }

    /**
     * @return FHIRCapabilityStatementRestResourceOperation[]
     */
    public function getOperation(): array
    {
        return $this->operation;
    }

    public function setOperation(?FHIRCapabilityStatementRestResourceOperation ...$value): self
    {
        $this->operation = array_filter($value);

        return $this;
    }

    public function addOperation(?FHIRCapabilityStatementRestResourceOperation ...$value): self
    {
        $this->operation = array_filter(array_merge($this->operation, $value));

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getCompartment(): array
    {
        return $this->compartment;
    }

    public function setCompartment(string|FHIRCanonical|null ...$value): self
    {
        $this->compartment = [];
        $this->addCompartment(...$value);

        return $this;
    }

    public function addCompartment(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->compartment = array_filter(array_merge($this->compartment, $values));

        return $this;
    }
}
