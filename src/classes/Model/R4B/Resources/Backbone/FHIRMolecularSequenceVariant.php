<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MolecularSequenceVariant Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMolecularSequenceVariantInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRMolecularSequenceVariant extends FHIRBackboneElement implements FHIRMolecularSequenceVariantInterface
{
    public const RESOURCE_NAME = 'MolecularSequence.variant';

    protected ?FHIRInteger $start = null;
    protected ?FHIRInteger $end = null;
    protected ?FHIRString $observedAllele = null;
    protected ?FHIRString $referenceAllele = null;
    protected ?FHIRString $cigar = null;
    protected ?FHIRReference $variantPointer = null;

    public function getStart(): ?FHIRInteger
    {
        return $this->start;
    }

    public function setStart(int|FHIRInteger|null $value): self
    {
        $this->start = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getEnd(): ?FHIRInteger
    {
        return $this->end;
    }

    public function setEnd(int|FHIRInteger|null $value): self
    {
        $this->end = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getObservedAllele(): ?FHIRString
    {
        return $this->observedAllele;
    }

    public function setObservedAllele(string|FHIRString|null $value): self
    {
        $this->observedAllele = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getReferenceAllele(): ?FHIRString
    {
        return $this->referenceAllele;
    }

    public function setReferenceAllele(string|FHIRString|null $value): self
    {
        $this->referenceAllele = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCigar(): ?FHIRString
    {
        return $this->cigar;
    }

    public function setCigar(string|FHIRString|null $value): self
    {
        $this->cigar = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getVariantPointer(): ?FHIRReference
    {
        return $this->variantPointer;
    }

    public function setVariantPointer(?FHIRReference $value): self
    {
        $this->variantPointer = $value;

        return $this;
    }
}
