<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledgeRelatedMedicationKnowledge Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationKnowledgeRelatedMedicationKnowledgeInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;

class FHIRMedicationKnowledgeRelatedMedicationKnowledge extends FHIRBackboneElement implements FHIRMedicationKnowledgeRelatedMedicationKnowledgeInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge.relatedMedicationKnowledge';

    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRReference[] */
    protected array $reference = [];

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReference(): array
    {
        return $this->reference;
    }

    public function setReference(?FHIRReference ...$value): self
    {
        $this->reference = array_filter($value);

        return $this;
    }

    public function addReference(?FHIRReference ...$value): self
    {
        $this->reference = array_filter(array_merge($this->reference, $value));

        return $this;
    }
}
