<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EvidenceVariableDefinition Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREvidenceVariableDefinitionInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRMarkdown;

class FHIREvidenceVariableDefinition extends FHIRBackboneElement implements FHIREvidenceVariableDefinitionInterface
{
    public const RESOURCE_NAME = 'Evidence.variableDefinition';

    protected ?FHIRMarkdown $description = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];
    protected ?FHIRCodeableConcept $variableRole = null;
    protected ?FHIRReference $observed = null;
    protected ?FHIRReference $intended = null;
    protected ?FHIRCodeableConcept $directnessMatch = null;

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    public function getVariableRole(): ?FHIRCodeableConcept
    {
        return $this->variableRole;
    }

    public function setVariableRole(?FHIRCodeableConcept $value): self
    {
        $this->variableRole = $value;

        return $this;
    }

    public function getObserved(): ?FHIRReference
    {
        return $this->observed;
    }

    public function setObserved(?FHIRReference $value): self
    {
        $this->observed = $value;

        return $this;
    }

    public function getIntended(): ?FHIRReference
    {
        return $this->intended;
    }

    public function setIntended(?FHIRReference $value): self
    {
        $this->intended = $value;

        return $this;
    }

    public function getDirectnessMatch(): ?FHIRCodeableConcept
    {
        return $this->directnessMatch;
    }

    public function setDirectnessMatch(?FHIRCodeableConcept $value): self
    {
        $this->directnessMatch = $value;

        return $this;
    }
}
