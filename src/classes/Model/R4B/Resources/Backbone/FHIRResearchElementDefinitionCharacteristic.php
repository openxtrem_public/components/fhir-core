<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ResearchElementDefinitionCharacteristic Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRResearchElementDefinitionCharacteristicInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRDataRequirement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRExpression;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRResearchElementDefinitionCharacteristic extends FHIRBackboneElement implements FHIRResearchElementDefinitionCharacteristicInterface
{
    public const RESOURCE_NAME = 'ResearchElementDefinition.characteristic';

    protected FHIRCodeableConcept|FHIRCanonical|FHIRExpression|FHIRDataRequirement|null $definition = null;

    /** @var FHIRUsageContext[] */
    protected array $usageContext = [];
    protected ?FHIRBoolean $exclude = null;
    protected ?FHIRCodeableConcept $unitOfMeasure = null;
    protected ?FHIRString $studyEffectiveDescription = null;
    protected FHIRDateTime|FHIRPeriod|FHIRDuration|FHIRTiming|null $studyEffective = null;
    protected ?FHIRDuration $studyEffectiveTimeFromStart = null;
    protected ?FHIRCode $studyEffectiveGroupMeasure = null;
    protected ?FHIRString $participantEffectiveDescription = null;
    protected FHIRDateTime|FHIRPeriod|FHIRDuration|FHIRTiming|null $participantEffective = null;
    protected ?FHIRDuration $participantEffectiveTimeFromStart = null;
    protected ?FHIRCode $participantEffectiveGroupMeasure = null;

    public function getDefinition(): FHIRCodeableConcept|FHIRCanonical|FHIRExpression|FHIRDataRequirement|null
    {
        return $this->definition;
    }

    public function setDefinition(
        FHIRCodeableConcept|FHIRCanonical|FHIRExpression|FHIRDataRequirement|null $value,
    ): self
    {
        $this->definition = $value;

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUsageContext(): array
    {
        return $this->usageContext;
    }

    public function setUsageContext(?FHIRUsageContext ...$value): self
    {
        $this->usageContext = array_filter($value);

        return $this;
    }

    public function addUsageContext(?FHIRUsageContext ...$value): self
    {
        $this->usageContext = array_filter(array_merge($this->usageContext, $value));

        return $this;
    }

    public function getExclude(): ?FHIRBoolean
    {
        return $this->exclude;
    }

    public function setExclude(bool|FHIRBoolean|null $value): self
    {
        $this->exclude = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getUnitOfMeasure(): ?FHIRCodeableConcept
    {
        return $this->unitOfMeasure;
    }

    public function setUnitOfMeasure(?FHIRCodeableConcept $value): self
    {
        $this->unitOfMeasure = $value;

        return $this;
    }

    public function getStudyEffectiveDescription(): ?FHIRString
    {
        return $this->studyEffectiveDescription;
    }

    public function setStudyEffectiveDescription(string|FHIRString|null $value): self
    {
        $this->studyEffectiveDescription = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStudyEffective(): FHIRDateTime|FHIRPeriod|FHIRDuration|FHIRTiming|null
    {
        return $this->studyEffective;
    }

    public function setStudyEffective(FHIRDateTime|FHIRPeriod|FHIRDuration|FHIRTiming|null $value): self
    {
        $this->studyEffective = $value;

        return $this;
    }

    public function getStudyEffectiveTimeFromStart(): ?FHIRDuration
    {
        return $this->studyEffectiveTimeFromStart;
    }

    public function setStudyEffectiveTimeFromStart(?FHIRDuration $value): self
    {
        $this->studyEffectiveTimeFromStart = $value;

        return $this;
    }

    public function getStudyEffectiveGroupMeasure(): ?FHIRCode
    {
        return $this->studyEffectiveGroupMeasure;
    }

    public function setStudyEffectiveGroupMeasure(string|FHIRCode|null $value): self
    {
        $this->studyEffectiveGroupMeasure = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getParticipantEffectiveDescription(): ?FHIRString
    {
        return $this->participantEffectiveDescription;
    }

    public function setParticipantEffectiveDescription(string|FHIRString|null $value): self
    {
        $this->participantEffectiveDescription = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getParticipantEffective(): FHIRDateTime|FHIRPeriod|FHIRDuration|FHIRTiming|null
    {
        return $this->participantEffective;
    }

    public function setParticipantEffective(FHIRDateTime|FHIRPeriod|FHIRDuration|FHIRTiming|null $value): self
    {
        $this->participantEffective = $value;

        return $this;
    }

    public function getParticipantEffectiveTimeFromStart(): ?FHIRDuration
    {
        return $this->participantEffectiveTimeFromStart;
    }

    public function setParticipantEffectiveTimeFromStart(?FHIRDuration $value): self
    {
        $this->participantEffectiveTimeFromStart = $value;

        return $this;
    }

    public function getParticipantEffectiveGroupMeasure(): ?FHIRCode
    {
        return $this->participantEffectiveGroupMeasure;
    }

    public function setParticipantEffectiveGroupMeasure(string|FHIRCode|null $value): self
    {
        $this->participantEffectiveGroupMeasure = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }
}
