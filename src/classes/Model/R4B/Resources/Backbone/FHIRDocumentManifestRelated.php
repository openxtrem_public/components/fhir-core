<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DocumentManifestRelated Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDocumentManifestRelatedInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;

class FHIRDocumentManifestRelated extends FHIRBackboneElement implements FHIRDocumentManifestRelatedInterface
{
    public const RESOURCE_NAME = 'DocumentManifest.related';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRReference $ref = null;

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getRef(): ?FHIRReference
    {
        return $this->ref;
    }

    public function setRef(?FHIRReference $value): self
    {
        $this->ref = $value;

        return $this;
    }
}
