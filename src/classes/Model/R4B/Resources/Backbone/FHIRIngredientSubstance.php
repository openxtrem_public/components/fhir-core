<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR IngredientSubstance Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRIngredientSubstanceInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableReference;

class FHIRIngredientSubstance extends FHIRBackboneElement implements FHIRIngredientSubstanceInterface
{
    public const RESOURCE_NAME = 'Ingredient.substance';

    protected ?FHIRCodeableReference $code = null;

    /** @var FHIRIngredientSubstanceStrength[] */
    protected array $strength = [];

    public function getCode(): ?FHIRCodeableReference
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableReference $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * @return FHIRIngredientSubstanceStrength[]
     */
    public function getStrength(): array
    {
        return $this->strength;
    }

    public function setStrength(?FHIRIngredientSubstanceStrength ...$value): self
    {
        $this->strength = array_filter($value);

        return $this;
    }

    public function addStrength(?FHIRIngredientSubstanceStrength ...$value): self
    {
        $this->strength = array_filter(array_merge($this->strength, $value));

        return $this;
    }
}
