<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImplementationGuideDependsOn Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImplementationGuideDependsOnInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRImplementationGuideDependsOn extends FHIRBackboneElement implements FHIRImplementationGuideDependsOnInterface
{
    public const RESOURCE_NAME = 'ImplementationGuide.dependsOn';

    protected ?FHIRCanonical $uri = null;
    protected ?FHIRId $packageId = null;
    protected ?FHIRString $version = null;

    public function getUri(): ?FHIRCanonical
    {
        return $this->uri;
    }

    public function setUri(string|FHIRCanonical|null $value): self
    {
        $this->uri = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    public function getPackageId(): ?FHIRId
    {
        return $this->packageId;
    }

    public function setPackageId(string|FHIRId|null $value): self
    {
        $this->packageId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
