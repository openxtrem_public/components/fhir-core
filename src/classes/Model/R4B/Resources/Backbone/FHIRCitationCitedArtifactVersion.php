<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CitationCitedArtifactVersion Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCitationCitedArtifactVersionInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRCitationCitedArtifactVersion extends FHIRBackboneElement implements FHIRCitationCitedArtifactVersionInterface
{
    public const RESOURCE_NAME = 'Citation.citedArtifact.version';

    protected ?FHIRString $value = null;
    protected ?FHIRReference $baseCitation = null;

    public function getValue(): ?FHIRString
    {
        return $this->value;
    }

    public function setValue(string|FHIRString|null $value): self
    {
        $this->value = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getBaseCitation(): ?FHIRReference
    {
        return $this->baseCitation;
    }

    public function setBaseCitation(?FHIRReference $value): self
    {
        $this->baseCitation = $value;

        return $this;
    }
}
