<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceDefinitionCapability Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceDefinitionCapabilityInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;

class FHIRDeviceDefinitionCapability extends FHIRBackboneElement implements FHIRDeviceDefinitionCapabilityInterface
{
    public const RESOURCE_NAME = 'DeviceDefinition.capability';

    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRCodeableConcept[] */
    protected array $description = [];

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getDescription(): array
    {
        return $this->description;
    }

    public function setDescription(?FHIRCodeableConcept ...$value): self
    {
        $this->description = array_filter($value);

        return $this;
    }

    public function addDescription(?FHIRCodeableConcept ...$value): self
    {
        $this->description = array_filter(array_merge($this->description, $value));

        return $this;
    }
}
