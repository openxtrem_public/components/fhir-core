<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR VerificationResultAttestation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRVerificationResultAttestationInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRSignature;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRVerificationResultAttestation extends FHIRBackboneElement implements FHIRVerificationResultAttestationInterface
{
    public const RESOURCE_NAME = 'VerificationResult.attestation';

    protected ?FHIRReference $who = null;
    protected ?FHIRReference $onBehalfOf = null;
    protected ?FHIRCodeableConcept $communicationMethod = null;
    protected ?FHIRDate $date = null;
    protected ?FHIRString $sourceIdentityCertificate = null;
    protected ?FHIRString $proxyIdentityCertificate = null;
    protected ?FHIRSignature $proxySignature = null;
    protected ?FHIRSignature $sourceSignature = null;

    public function getWho(): ?FHIRReference
    {
        return $this->who;
    }

    public function setWho(?FHIRReference $value): self
    {
        $this->who = $value;

        return $this;
    }

    public function getOnBehalfOf(): ?FHIRReference
    {
        return $this->onBehalfOf;
    }

    public function setOnBehalfOf(?FHIRReference $value): self
    {
        $this->onBehalfOf = $value;

        return $this;
    }

    public function getCommunicationMethod(): ?FHIRCodeableConcept
    {
        return $this->communicationMethod;
    }

    public function setCommunicationMethod(?FHIRCodeableConcept $value): self
    {
        $this->communicationMethod = $value;

        return $this;
    }

    public function getDate(): ?FHIRDate
    {
        return $this->date;
    }

    public function setDate(string|FHIRDate|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getSourceIdentityCertificate(): ?FHIRString
    {
        return $this->sourceIdentityCertificate;
    }

    public function setSourceIdentityCertificate(string|FHIRString|null $value): self
    {
        $this->sourceIdentityCertificate = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getProxyIdentityCertificate(): ?FHIRString
    {
        return $this->proxyIdentityCertificate;
    }

    public function setProxyIdentityCertificate(string|FHIRString|null $value): self
    {
        $this->proxyIdentityCertificate = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getProxySignature(): ?FHIRSignature
    {
        return $this->proxySignature;
    }

    public function setProxySignature(?FHIRSignature $value): self
    {
        $this->proxySignature = $value;

        return $this;
    }

    public function getSourceSignature(): ?FHIRSignature
    {
        return $this->sourceSignature;
    }

    public function setSourceSignature(?FHIRSignature $value): self
    {
        $this->sourceSignature = $value;

        return $this;
    }
}
