<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ConceptMapGroup Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRConceptMapGroupInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUri;

class FHIRConceptMapGroup extends FHIRBackboneElement implements FHIRConceptMapGroupInterface
{
    public const RESOURCE_NAME = 'ConceptMap.group';

    protected ?FHIRUri $source = null;
    protected ?FHIRString $sourceVersion = null;
    protected ?FHIRUri $target = null;
    protected ?FHIRString $targetVersion = null;

    /** @var FHIRConceptMapGroupElement[] */
    protected array $element = [];
    protected ?FHIRConceptMapGroupUnmapped $unmapped = null;

    public function getSource(): ?FHIRUri
    {
        return $this->source;
    }

    public function setSource(string|FHIRUri|null $value): self
    {
        $this->source = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getSourceVersion(): ?FHIRString
    {
        return $this->sourceVersion;
    }

    public function setSourceVersion(string|FHIRString|null $value): self
    {
        $this->sourceVersion = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTarget(): ?FHIRUri
    {
        return $this->target;
    }

    public function setTarget(string|FHIRUri|null $value): self
    {
        $this->target = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getTargetVersion(): ?FHIRString
    {
        return $this->targetVersion;
    }

    public function setTargetVersion(string|FHIRString|null $value): self
    {
        $this->targetVersion = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRConceptMapGroupElement[]
     */
    public function getElement(): array
    {
        return $this->element;
    }

    public function setElement(?FHIRConceptMapGroupElement ...$value): self
    {
        $this->element = array_filter($value);

        return $this;
    }

    public function addElement(?FHIRConceptMapGroupElement ...$value): self
    {
        $this->element = array_filter(array_merge($this->element, $value));

        return $this;
    }

    public function getUnmapped(): ?FHIRConceptMapGroupUnmapped
    {
        return $this->unmapped;
    }

    public function setUnmapped(?FHIRConceptMapGroupUnmapped $value): self
    {
        $this->unmapped = $value;

        return $this;
    }
}
