<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR InsurancePlanPlanSpecificCostBenefitCost Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRInsurancePlanPlanSpecificCostBenefitCostInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRQuantity;

class FHIRInsurancePlanPlanSpecificCostBenefitCost extends FHIRBackboneElement implements FHIRInsurancePlanPlanSpecificCostBenefitCostInterface
{
    public const RESOURCE_NAME = 'InsurancePlan.plan.specificCost.benefit.cost';

    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableConcept $applicability = null;

    /** @var FHIRCodeableConcept[] */
    protected array $qualifiers = [];
    protected ?FHIRQuantity $value = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getApplicability(): ?FHIRCodeableConcept
    {
        return $this->applicability;
    }

    public function setApplicability(?FHIRCodeableConcept $value): self
    {
        $this->applicability = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getQualifiers(): array
    {
        return $this->qualifiers;
    }

    public function setQualifiers(?FHIRCodeableConcept ...$value): self
    {
        $this->qualifiers = array_filter($value);

        return $this;
    }

    public function addQualifiers(?FHIRCodeableConcept ...$value): self
    {
        $this->qualifiers = array_filter(array_merge($this->qualifiers, $value));

        return $this;
    }

    public function getValue(): ?FHIRQuantity
    {
        return $this->value;
    }

    public function setValue(?FHIRQuantity $value): self
    {
        $this->value = $value;

        return $this;
    }
}
