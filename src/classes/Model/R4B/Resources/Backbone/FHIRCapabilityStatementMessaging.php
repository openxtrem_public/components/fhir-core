<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CapabilityStatementMessaging Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCapabilityStatementMessagingInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUnsignedInt;

class FHIRCapabilityStatementMessaging extends FHIRBackboneElement implements FHIRCapabilityStatementMessagingInterface
{
    public const RESOURCE_NAME = 'CapabilityStatement.messaging';

    /** @var FHIRCapabilityStatementMessagingEndpoint[] */
    protected array $endpoint = [];
    protected ?FHIRUnsignedInt $reliableCache = null;
    protected ?FHIRMarkdown $documentation = null;

    /** @var FHIRCapabilityStatementMessagingSupportedMessage[] */
    protected array $supportedMessage = [];

    /**
     * @return FHIRCapabilityStatementMessagingEndpoint[]
     */
    public function getEndpoint(): array
    {
        return $this->endpoint;
    }

    public function setEndpoint(?FHIRCapabilityStatementMessagingEndpoint ...$value): self
    {
        $this->endpoint = array_filter($value);

        return $this;
    }

    public function addEndpoint(?FHIRCapabilityStatementMessagingEndpoint ...$value): self
    {
        $this->endpoint = array_filter(array_merge($this->endpoint, $value));

        return $this;
    }

    public function getReliableCache(): ?FHIRUnsignedInt
    {
        return $this->reliableCache;
    }

    public function setReliableCache(int|FHIRUnsignedInt|null $value): self
    {
        $this->reliableCache = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getDocumentation(): ?FHIRMarkdown
    {
        return $this->documentation;
    }

    public function setDocumentation(string|FHIRMarkdown|null $value): self
    {
        $this->documentation = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCapabilityStatementMessagingSupportedMessage[]
     */
    public function getSupportedMessage(): array
    {
        return $this->supportedMessage;
    }

    public function setSupportedMessage(?FHIRCapabilityStatementMessagingSupportedMessage ...$value): self
    {
        $this->supportedMessage = array_filter($value);

        return $this;
    }

    public function addSupportedMessage(?FHIRCapabilityStatementMessagingSupportedMessage ...$value): self
    {
        $this->supportedMessage = array_filter(array_merge($this->supportedMessage, $value));

        return $this;
    }
}
