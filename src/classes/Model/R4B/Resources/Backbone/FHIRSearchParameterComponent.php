<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SearchParameterComponent Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSearchParameterComponentInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRSearchParameterComponent extends FHIRBackboneElement implements FHIRSearchParameterComponentInterface
{
    public const RESOURCE_NAME = 'SearchParameter.component';

    protected ?FHIRCanonical $definition = null;
    protected ?FHIRString $expression = null;

    public function getDefinition(): ?FHIRCanonical
    {
        return $this->definition;
    }

    public function setDefinition(string|FHIRCanonical|null $value): self
    {
        $this->definition = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    public function getExpression(): ?FHIRString
    {
        return $this->expression;
    }

    public function setExpression(string|FHIRString|null $value): self
    {
        $this->expression = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
