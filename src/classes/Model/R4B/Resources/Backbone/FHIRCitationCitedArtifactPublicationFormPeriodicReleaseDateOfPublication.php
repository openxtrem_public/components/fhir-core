<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CitationCitedArtifactPublicationFormPeriodicReleaseDateOfPublication Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCitationCitedArtifactPublicationFormPeriodicReleaseDateOfPublicationInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRCitationCitedArtifactPublicationFormPeriodicReleaseDateOfPublication extends FHIRBackboneElement implements FHIRCitationCitedArtifactPublicationFormPeriodicReleaseDateOfPublicationInterface
{
    public const RESOURCE_NAME = 'Citation.citedArtifact.publicationForm.periodicRelease.dateOfPublication';

    protected ?FHIRDate $date = null;
    protected ?FHIRString $year = null;
    protected ?FHIRString $month = null;
    protected ?FHIRString $day = null;
    protected ?FHIRString $season = null;
    protected ?FHIRString $text = null;

    public function getDate(): ?FHIRDate
    {
        return $this->date;
    }

    public function setDate(string|FHIRDate|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getYear(): ?FHIRString
    {
        return $this->year;
    }

    public function setYear(string|FHIRString|null $value): self
    {
        $this->year = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getMonth(): ?FHIRString
    {
        return $this->month;
    }

    public function setMonth(string|FHIRString|null $value): self
    {
        $this->month = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDay(): ?FHIRString
    {
        return $this->day;
    }

    public function setDay(string|FHIRString|null $value): self
    {
        $this->day = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSeason(): ?FHIRString
    {
        return $this->season;
    }

    public function setSeason(string|FHIRString|null $value): self
    {
        $this->season = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getText(): ?FHIRString
    {
        return $this->text;
    }

    public function setText(string|FHIRString|null $value): self
    {
        $this->text = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
