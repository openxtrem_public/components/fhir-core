<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MeasureGroupStratifier Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMeasureGroupStratifierInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRExpression;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRMeasureGroupStratifier extends FHIRBackboneElement implements FHIRMeasureGroupStratifierInterface
{
    public const RESOURCE_NAME = 'Measure.group.stratifier';

    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRString $description = null;
    protected ?FHIRExpression $criteria = null;

    /** @var FHIRMeasureGroupStratifierComponent[] */
    protected array $component = [];

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCriteria(): ?FHIRExpression
    {
        return $this->criteria;
    }

    public function setCriteria(?FHIRExpression $value): self
    {
        $this->criteria = $value;

        return $this;
    }

    /**
     * @return FHIRMeasureGroupStratifierComponent[]
     */
    public function getComponent(): array
    {
        return $this->component;
    }

    public function setComponent(?FHIRMeasureGroupStratifierComponent ...$value): self
    {
        $this->component = array_filter($value);

        return $this;
    }

    public function addComponent(?FHIRMeasureGroupStratifierComponent ...$value): self
    {
        $this->component = array_filter(array_merge($this->component, $value));

        return $this;
    }
}
