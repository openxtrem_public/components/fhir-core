<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TerminologyCapabilitiesTranslation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTerminologyCapabilitiesTranslationInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;

class FHIRTerminologyCapabilitiesTranslation extends FHIRBackboneElement implements FHIRTerminologyCapabilitiesTranslationInterface
{
    public const RESOURCE_NAME = 'TerminologyCapabilities.translation';

    protected ?FHIRBoolean $needsMap = null;

    public function getNeedsMap(): ?FHIRBoolean
    {
        return $this->needsMap;
    }

    public function setNeedsMap(bool|FHIRBoolean|null $value): self
    {
        $this->needsMap = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }
}
