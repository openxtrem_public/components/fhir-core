<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MeasureReportGroupStratifierStratum Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMeasureReportGroupStratifierStratumInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRQuantity;

class FHIRMeasureReportGroupStratifierStratum extends FHIRBackboneElement implements FHIRMeasureReportGroupStratifierStratumInterface
{
    public const RESOURCE_NAME = 'MeasureReport.group.stratifier.stratum';

    protected ?FHIRCodeableConcept $value = null;

    /** @var FHIRMeasureReportGroupStratifierStratumComponent[] */
    protected array $component = [];

    /** @var FHIRMeasureReportGroupStratifierStratumPopulation[] */
    protected array $population = [];
    protected ?FHIRQuantity $measureScore = null;

    public function getValue(): ?FHIRCodeableConcept
    {
        return $this->value;
    }

    public function setValue(?FHIRCodeableConcept $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return FHIRMeasureReportGroupStratifierStratumComponent[]
     */
    public function getComponent(): array
    {
        return $this->component;
    }

    public function setComponent(?FHIRMeasureReportGroupStratifierStratumComponent ...$value): self
    {
        $this->component = array_filter($value);

        return $this;
    }

    public function addComponent(?FHIRMeasureReportGroupStratifierStratumComponent ...$value): self
    {
        $this->component = array_filter(array_merge($this->component, $value));

        return $this;
    }

    /**
     * @return FHIRMeasureReportGroupStratifierStratumPopulation[]
     */
    public function getPopulation(): array
    {
        return $this->population;
    }

    public function setPopulation(?FHIRMeasureReportGroupStratifierStratumPopulation ...$value): self
    {
        $this->population = array_filter($value);

        return $this;
    }

    public function addPopulation(?FHIRMeasureReportGroupStratifierStratumPopulation ...$value): self
    {
        $this->population = array_filter(array_merge($this->population, $value));

        return $this;
    }

    public function getMeasureScore(): ?FHIRQuantity
    {
        return $this->measureScore;
    }

    public function setMeasureScore(?FHIRQuantity $value): self
    {
        $this->measureScore = $value;

        return $this;
    }
}
