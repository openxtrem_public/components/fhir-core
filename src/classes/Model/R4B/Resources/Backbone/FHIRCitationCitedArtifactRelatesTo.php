<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CitationCitedArtifactRelatesTo Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCitationCitedArtifactRelatesToInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUri;

class FHIRCitationCitedArtifactRelatesTo extends FHIRBackboneElement implements FHIRCitationCitedArtifactRelatesToInterface
{
    public const RESOURCE_NAME = 'Citation.citedArtifact.relatesTo';

    protected ?FHIRCodeableConcept $relationshipType = null;

    /** @var FHIRCodeableConcept[] */
    protected array $targetClassifier = [];
    protected FHIRUri|FHIRIdentifier|FHIRReference|FHIRAttachment|null $target = null;

    public function getRelationshipType(): ?FHIRCodeableConcept
    {
        return $this->relationshipType;
    }

    public function setRelationshipType(?FHIRCodeableConcept $value): self
    {
        $this->relationshipType = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getTargetClassifier(): array
    {
        return $this->targetClassifier;
    }

    public function setTargetClassifier(?FHIRCodeableConcept ...$value): self
    {
        $this->targetClassifier = array_filter($value);

        return $this;
    }

    public function addTargetClassifier(?FHIRCodeableConcept ...$value): self
    {
        $this->targetClassifier = array_filter(array_merge($this->targetClassifier, $value));

        return $this;
    }

    public function getTarget(): FHIRUri|FHIRIdentifier|FHIRReference|FHIRAttachment|null
    {
        return $this->target;
    }

    public function setTarget(FHIRUri|FHIRIdentifier|FHIRReference|FHIRAttachment|null $value): self
    {
        $this->target = $value;

        return $this;
    }
}
