<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExampleScenarioInstance Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExampleScenarioInstanceInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRExampleScenarioInstance extends FHIRBackboneElement implements FHIRExampleScenarioInstanceInterface
{
    public const RESOURCE_NAME = 'ExampleScenario.instance';

    protected ?FHIRString $resourceId = null;
    protected ?FHIRCode $resourceType = null;
    protected ?FHIRString $name = null;
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRExampleScenarioInstanceVersion[] */
    protected array $version = [];

    /** @var FHIRExampleScenarioInstanceContainedInstance[] */
    protected array $containedInstance = [];

    public function getResourceId(): ?FHIRString
    {
        return $this->resourceId;
    }

    public function setResourceId(string|FHIRString|null $value): self
    {
        $this->resourceId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getResourceType(): ?FHIRCode
    {
        return $this->resourceType;
    }

    public function setResourceType(string|FHIRCode|null $value): self
    {
        $this->resourceType = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRExampleScenarioInstanceVersion[]
     */
    public function getVersion(): array
    {
        return $this->version;
    }

    public function setVersion(?FHIRExampleScenarioInstanceVersion ...$value): self
    {
        $this->version = array_filter($value);

        return $this;
    }

    public function addVersion(?FHIRExampleScenarioInstanceVersion ...$value): self
    {
        $this->version = array_filter(array_merge($this->version, $value));

        return $this;
    }

    /**
     * @return FHIRExampleScenarioInstanceContainedInstance[]
     */
    public function getContainedInstance(): array
    {
        return $this->containedInstance;
    }

    public function setContainedInstance(?FHIRExampleScenarioInstanceContainedInstance ...$value): self
    {
        $this->containedInstance = array_filter($value);

        return $this;
    }

    public function addContainedInstance(?FHIRExampleScenarioInstanceContainedInstance ...$value): self
    {
        $this->containedInstance = array_filter(array_merge($this->containedInstance, $value));

        return $this;
    }
}
