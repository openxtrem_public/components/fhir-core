<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ContractSigner Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRContractSignerInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRSignature;

class FHIRContractSigner extends FHIRBackboneElement implements FHIRContractSignerInterface
{
    public const RESOURCE_NAME = 'Contract.signer';

    protected ?FHIRCoding $type = null;
    protected ?FHIRReference $party = null;

    /** @var FHIRSignature[] */
    protected array $signature = [];

    public function getType(): ?FHIRCoding
    {
        return $this->type;
    }

    public function setType(?FHIRCoding $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getParty(): ?FHIRReference
    {
        return $this->party;
    }

    public function setParty(?FHIRReference $value): self
    {
        $this->party = $value;

        return $this;
    }

    /**
     * @return FHIRSignature[]
     */
    public function getSignature(): array
    {
        return $this->signature;
    }

    public function setSignature(?FHIRSignature ...$value): self
    {
        $this->signature = array_filter($value);

        return $this;
    }

    public function addSignature(?FHIRSignature ...$value): self
    {
        $this->signature = array_filter(array_merge($this->signature, $value));

        return $this;
    }
}
