<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR NutritionOrderEnteralFormulaAdministration Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRNutritionOrderEnteralFormulaAdministrationInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRRatio;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRTiming;

class FHIRNutritionOrderEnteralFormulaAdministration extends FHIRBackboneElement implements FHIRNutritionOrderEnteralFormulaAdministrationInterface
{
    public const RESOURCE_NAME = 'NutritionOrder.enteralFormula.administration';

    protected ?FHIRTiming $schedule = null;
    protected ?FHIRQuantity $quantity = null;
    protected FHIRQuantity|FHIRRatio|null $rate = null;

    public function getSchedule(): ?FHIRTiming
    {
        return $this->schedule;
    }

    public function setSchedule(?FHIRTiming $value): self
    {
        $this->schedule = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getRate(): FHIRQuantity|FHIRRatio|null
    {
        return $this->rate;
    }

    public function setRate(FHIRQuantity|FHIRRatio|null $value): self
    {
        $this->rate = $value;

        return $this;
    }
}
