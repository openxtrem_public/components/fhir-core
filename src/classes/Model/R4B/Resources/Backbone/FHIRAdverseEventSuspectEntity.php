<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AdverseEventSuspectEntity Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAdverseEventSuspectEntityInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;

class FHIRAdverseEventSuspectEntity extends FHIRBackboneElement implements FHIRAdverseEventSuspectEntityInterface
{
    public const RESOURCE_NAME = 'AdverseEvent.suspectEntity';

    protected ?FHIRReference $instance = null;

    /** @var FHIRAdverseEventSuspectEntityCausality[] */
    protected array $causality = [];

    public function getInstance(): ?FHIRReference
    {
        return $this->instance;
    }

    public function setInstance(?FHIRReference $value): self
    {
        $this->instance = $value;

        return $this;
    }

    /**
     * @return FHIRAdverseEventSuspectEntityCausality[]
     */
    public function getCausality(): array
    {
        return $this->causality;
    }

    public function setCausality(?FHIRAdverseEventSuspectEntityCausality ...$value): self
    {
        $this->causality = array_filter($value);

        return $this;
    }

    public function addCausality(?FHIRAdverseEventSuspectEntityCausality ...$value): self
    {
        $this->causality = array_filter(array_merge($this->causality, $value));

        return $this;
    }
}
