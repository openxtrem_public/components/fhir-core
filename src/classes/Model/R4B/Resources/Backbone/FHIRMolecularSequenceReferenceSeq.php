<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MolecularSequenceReferenceSeq Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMolecularSequenceReferenceSeqInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRMolecularSequenceReferenceSeq extends FHIRBackboneElement implements FHIRMolecularSequenceReferenceSeqInterface
{
    public const RESOURCE_NAME = 'MolecularSequence.referenceSeq';

    protected ?FHIRCodeableConcept $chromosome = null;
    protected ?FHIRString $genomeBuild = null;
    protected ?FHIRCode $orientation = null;
    protected ?FHIRCodeableConcept $referenceSeqId = null;
    protected ?FHIRReference $referenceSeqPointer = null;
    protected ?FHIRString $referenceSeqString = null;
    protected ?FHIRCode $strand = null;
    protected ?FHIRInteger $windowStart = null;
    protected ?FHIRInteger $windowEnd = null;

    public function getChromosome(): ?FHIRCodeableConcept
    {
        return $this->chromosome;
    }

    public function setChromosome(?FHIRCodeableConcept $value): self
    {
        $this->chromosome = $value;

        return $this;
    }

    public function getGenomeBuild(): ?FHIRString
    {
        return $this->genomeBuild;
    }

    public function setGenomeBuild(string|FHIRString|null $value): self
    {
        $this->genomeBuild = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getOrientation(): ?FHIRCode
    {
        return $this->orientation;
    }

    public function setOrientation(string|FHIRCode|null $value): self
    {
        $this->orientation = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getReferenceSeqId(): ?FHIRCodeableConcept
    {
        return $this->referenceSeqId;
    }

    public function setReferenceSeqId(?FHIRCodeableConcept $value): self
    {
        $this->referenceSeqId = $value;

        return $this;
    }

    public function getReferenceSeqPointer(): ?FHIRReference
    {
        return $this->referenceSeqPointer;
    }

    public function setReferenceSeqPointer(?FHIRReference $value): self
    {
        $this->referenceSeqPointer = $value;

        return $this;
    }

    public function getReferenceSeqString(): ?FHIRString
    {
        return $this->referenceSeqString;
    }

    public function setReferenceSeqString(string|FHIRString|null $value): self
    {
        $this->referenceSeqString = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStrand(): ?FHIRCode
    {
        return $this->strand;
    }

    public function setStrand(string|FHIRCode|null $value): self
    {
        $this->strand = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getWindowStart(): ?FHIRInteger
    {
        return $this->windowStart;
    }

    public function setWindowStart(int|FHIRInteger|null $value): self
    {
        $this->windowStart = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getWindowEnd(): ?FHIRInteger
    {
        return $this->windowEnd;
    }

    public function setWindowEnd(int|FHIRInteger|null $value): self
    {
        $this->windowEnd = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }
}
