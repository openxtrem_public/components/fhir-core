<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR InsurancePlanCoverageBenefit Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRInsurancePlanCoverageBenefitInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRInsurancePlanCoverageBenefit extends FHIRBackboneElement implements FHIRInsurancePlanCoverageBenefitInterface
{
    public const RESOURCE_NAME = 'InsurancePlan.coverage.benefit';

    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRString $requirement = null;

    /** @var FHIRInsurancePlanCoverageBenefitLimit[] */
    protected array $limit = [];

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getRequirement(): ?FHIRString
    {
        return $this->requirement;
    }

    public function setRequirement(string|FHIRString|null $value): self
    {
        $this->requirement = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRInsurancePlanCoverageBenefitLimit[]
     */
    public function getLimit(): array
    {
        return $this->limit;
    }

    public function setLimit(?FHIRInsurancePlanCoverageBenefitLimit ...$value): self
    {
        $this->limit = array_filter($value);

        return $this;
    }

    public function addLimit(?FHIRInsurancePlanCoverageBenefitLimit ...$value): self
    {
        $this->limit = array_filter(array_merge($this->limit, $value));

        return $this;
    }
}
