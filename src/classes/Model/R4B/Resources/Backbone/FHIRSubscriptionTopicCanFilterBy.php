<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubscriptionTopicCanFilterBy Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubscriptionTopicCanFilterByInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUri;

class FHIRSubscriptionTopicCanFilterBy extends FHIRBackboneElement implements FHIRSubscriptionTopicCanFilterByInterface
{
    public const RESOURCE_NAME = 'SubscriptionTopic.canFilterBy';

    protected ?FHIRMarkdown $description = null;
    protected ?FHIRUri $resource = null;
    protected ?FHIRString $filterParameter = null;
    protected ?FHIRUri $filterDefinition = null;

    /** @var FHIRCode[] */
    protected array $modifier = [];

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getResource(): ?FHIRUri
    {
        return $this->resource;
    }

    public function setResource(string|FHIRUri|null $value): self
    {
        $this->resource = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getFilterParameter(): ?FHIRString
    {
        return $this->filterParameter;
    }

    public function setFilterParameter(string|FHIRString|null $value): self
    {
        $this->filterParameter = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getFilterDefinition(): ?FHIRUri
    {
        return $this->filterDefinition;
    }

    public function setFilterDefinition(string|FHIRUri|null $value): self
    {
        $this->filterDefinition = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getModifier(): array
    {
        return $this->modifier;
    }

    public function setModifier(string|FHIRCode|null ...$value): self
    {
        $this->modifier = [];
        $this->addModifier(...$value);

        return $this;
    }

    public function addModifier(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->modifier = array_filter(array_merge($this->modifier, $values));

        return $this;
    }
}
