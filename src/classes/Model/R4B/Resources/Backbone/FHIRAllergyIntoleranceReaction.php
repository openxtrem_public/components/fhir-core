<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AllergyIntoleranceReaction Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAllergyIntoleranceReactionInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRAllergyIntoleranceReaction extends FHIRBackboneElement implements FHIRAllergyIntoleranceReactionInterface
{
    public const RESOURCE_NAME = 'AllergyIntolerance.reaction';

    protected ?FHIRCodeableConcept $substance = null;

    /** @var FHIRCodeableConcept[] */
    protected array $manifestation = [];
    protected ?FHIRString $description = null;
    protected ?FHIRDateTime $onset = null;
    protected ?FHIRCode $severity = null;
    protected ?FHIRCodeableConcept $exposureRoute = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    public function getSubstance(): ?FHIRCodeableConcept
    {
        return $this->substance;
    }

    public function setSubstance(?FHIRCodeableConcept $value): self
    {
        $this->substance = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getManifestation(): array
    {
        return $this->manifestation;
    }

    public function setManifestation(?FHIRCodeableConcept ...$value): self
    {
        $this->manifestation = array_filter($value);

        return $this;
    }

    public function addManifestation(?FHIRCodeableConcept ...$value): self
    {
        $this->manifestation = array_filter(array_merge($this->manifestation, $value));

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getOnset(): ?FHIRDateTime
    {
        return $this->onset;
    }

    public function setOnset(string|FHIRDateTime|null $value): self
    {
        $this->onset = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getSeverity(): ?FHIRCode
    {
        return $this->severity;
    }

    public function setSeverity(string|FHIRCode|null $value): self
    {
        $this->severity = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getExposureRoute(): ?FHIRCodeableConcept
    {
        return $this->exposureRoute;
    }

    public function setExposureRoute(?FHIRCodeableConcept $value): self
    {
        $this->exposureRoute = $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }
}
