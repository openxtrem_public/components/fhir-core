<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CitationCitedArtifact Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCitationCitedArtifactInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;

class FHIRCitationCitedArtifact extends FHIRBackboneElement implements FHIRCitationCitedArtifactInterface
{
    public const RESOURCE_NAME = 'Citation.citedArtifact';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRIdentifier[] */
    protected array $relatedIdentifier = [];
    protected ?FHIRDateTime $dateAccessed = null;
    protected ?FHIRCitationCitedArtifactVersion $version = null;

    /** @var FHIRCodeableConcept[] */
    protected array $currentState = [];

    /** @var FHIRCitationCitedArtifactStatusDate[] */
    protected array $statusDate = [];

    /** @var FHIRCitationCitedArtifactTitle[] */
    protected array $title = [];

    /** @var FHIRCitationCitedArtifactAbstract[] */
    protected array $abstract = [];
    protected ?FHIRCitationCitedArtifactPart $part = null;

    /** @var FHIRCitationCitedArtifactRelatesTo[] */
    protected array $relatesTo = [];

    /** @var FHIRCitationCitedArtifactPublicationForm[] */
    protected array $publicationForm = [];

    /** @var FHIRCitationCitedArtifactWebLocation[] */
    protected array $webLocation = [];

    /** @var FHIRCitationCitedArtifactClassification[] */
    protected array $classification = [];
    protected ?FHIRCitationCitedArtifactContributorship $contributorship = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getRelatedIdentifier(): array
    {
        return $this->relatedIdentifier;
    }

    public function setRelatedIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->relatedIdentifier = array_filter($value);

        return $this;
    }

    public function addRelatedIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->relatedIdentifier = array_filter(array_merge($this->relatedIdentifier, $value));

        return $this;
    }

    public function getDateAccessed(): ?FHIRDateTime
    {
        return $this->dateAccessed;
    }

    public function setDateAccessed(string|FHIRDateTime|null $value): self
    {
        $this->dateAccessed = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getVersion(): ?FHIRCitationCitedArtifactVersion
    {
        return $this->version;
    }

    public function setVersion(?FHIRCitationCitedArtifactVersion $value): self
    {
        $this->version = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCurrentState(): array
    {
        return $this->currentState;
    }

    public function setCurrentState(?FHIRCodeableConcept ...$value): self
    {
        $this->currentState = array_filter($value);

        return $this;
    }

    public function addCurrentState(?FHIRCodeableConcept ...$value): self
    {
        $this->currentState = array_filter(array_merge($this->currentState, $value));

        return $this;
    }

    /**
     * @return FHIRCitationCitedArtifactStatusDate[]
     */
    public function getStatusDate(): array
    {
        return $this->statusDate;
    }

    public function setStatusDate(?FHIRCitationCitedArtifactStatusDate ...$value): self
    {
        $this->statusDate = array_filter($value);

        return $this;
    }

    public function addStatusDate(?FHIRCitationCitedArtifactStatusDate ...$value): self
    {
        $this->statusDate = array_filter(array_merge($this->statusDate, $value));

        return $this;
    }

    /**
     * @return FHIRCitationCitedArtifactTitle[]
     */
    public function getTitle(): array
    {
        return $this->title;
    }

    public function setTitle(?FHIRCitationCitedArtifactTitle ...$value): self
    {
        $this->title = array_filter($value);

        return $this;
    }

    public function addTitle(?FHIRCitationCitedArtifactTitle ...$value): self
    {
        $this->title = array_filter(array_merge($this->title, $value));

        return $this;
    }

    /**
     * @return FHIRCitationCitedArtifactAbstract[]
     */
    public function getAbstract(): array
    {
        return $this->abstract;
    }

    public function setAbstract(?FHIRCitationCitedArtifactAbstract ...$value): self
    {
        $this->abstract = array_filter($value);

        return $this;
    }

    public function addAbstract(?FHIRCitationCitedArtifactAbstract ...$value): self
    {
        $this->abstract = array_filter(array_merge($this->abstract, $value));

        return $this;
    }

    public function getPart(): ?FHIRCitationCitedArtifactPart
    {
        return $this->part;
    }

    public function setPart(?FHIRCitationCitedArtifactPart $value): self
    {
        $this->part = $value;

        return $this;
    }

    /**
     * @return FHIRCitationCitedArtifactRelatesTo[]
     */
    public function getRelatesTo(): array
    {
        return $this->relatesTo;
    }

    public function setRelatesTo(?FHIRCitationCitedArtifactRelatesTo ...$value): self
    {
        $this->relatesTo = array_filter($value);

        return $this;
    }

    public function addRelatesTo(?FHIRCitationCitedArtifactRelatesTo ...$value): self
    {
        $this->relatesTo = array_filter(array_merge($this->relatesTo, $value));

        return $this;
    }

    /**
     * @return FHIRCitationCitedArtifactPublicationForm[]
     */
    public function getPublicationForm(): array
    {
        return $this->publicationForm;
    }

    public function setPublicationForm(?FHIRCitationCitedArtifactPublicationForm ...$value): self
    {
        $this->publicationForm = array_filter($value);

        return $this;
    }

    public function addPublicationForm(?FHIRCitationCitedArtifactPublicationForm ...$value): self
    {
        $this->publicationForm = array_filter(array_merge($this->publicationForm, $value));

        return $this;
    }

    /**
     * @return FHIRCitationCitedArtifactWebLocation[]
     */
    public function getWebLocation(): array
    {
        return $this->webLocation;
    }

    public function setWebLocation(?FHIRCitationCitedArtifactWebLocation ...$value): self
    {
        $this->webLocation = array_filter($value);

        return $this;
    }

    public function addWebLocation(?FHIRCitationCitedArtifactWebLocation ...$value): self
    {
        $this->webLocation = array_filter(array_merge($this->webLocation, $value));

        return $this;
    }

    /**
     * @return FHIRCitationCitedArtifactClassification[]
     */
    public function getClassification(): array
    {
        return $this->classification;
    }

    public function setClassification(?FHIRCitationCitedArtifactClassification ...$value): self
    {
        $this->classification = array_filter($value);

        return $this;
    }

    public function addClassification(?FHIRCitationCitedArtifactClassification ...$value): self
    {
        $this->classification = array_filter(array_merge($this->classification, $value));

        return $this;
    }

    public function getContributorship(): ?FHIRCitationCitedArtifactContributorship
    {
        return $this->contributorship;
    }

    public function setContributorship(?FHIRCitationCitedArtifactContributorship $value): self
    {
        $this->contributorship = $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }
}
