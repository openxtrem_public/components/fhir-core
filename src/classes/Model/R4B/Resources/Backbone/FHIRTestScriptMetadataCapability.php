<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestScriptMetadataCapability Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestScriptMetadataCapabilityInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUri;

class FHIRTestScriptMetadataCapability extends FHIRBackboneElement implements FHIRTestScriptMetadataCapabilityInterface
{
    public const RESOURCE_NAME = 'TestScript.metadata.capability';

    protected ?FHIRBoolean $required = null;
    protected ?FHIRBoolean $validated = null;
    protected ?FHIRString $description = null;

    /** @var FHIRInteger[] */
    protected array $origin = [];
    protected ?FHIRInteger $destination = null;

    /** @var FHIRUri[] */
    protected array $link = [];
    protected ?FHIRCanonical $capabilities = null;

    public function getRequired(): ?FHIRBoolean
    {
        return $this->required;
    }

    public function setRequired(bool|FHIRBoolean|null $value): self
    {
        $this->required = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getValidated(): ?FHIRBoolean
    {
        return $this->validated;
    }

    public function setValidated(bool|FHIRBoolean|null $value): self
    {
        $this->validated = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRInteger[]
     */
    public function getOrigin(): array
    {
        return $this->origin;
    }

    public function setOrigin(int|FHIRInteger|null ...$value): self
    {
        $this->origin = [];
        $this->addOrigin(...$value);

        return $this;
    }

    public function addOrigin(int|FHIRInteger|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRInteger())->setValue($v) : $v, $value);

        $this->origin = array_filter(array_merge($this->origin, $values));

        return $this;
    }

    public function getDestination(): ?FHIRInteger
    {
        return $this->destination;
    }

    public function setDestination(int|FHIRInteger|null $value): self
    {
        $this->destination = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRUri[]
     */
    public function getLink(): array
    {
        return $this->link;
    }

    public function setLink(string|FHIRUri|null ...$value): self
    {
        $this->link = [];
        $this->addLink(...$value);

        return $this;
    }

    public function addLink(string|FHIRUri|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUri())->setValue($v) : $v, $value);

        $this->link = array_filter(array_merge($this->link, $values));

        return $this;
    }

    public function getCapabilities(): ?FHIRCanonical
    {
        return $this->capabilities;
    }

    public function setCapabilities(string|FHIRCanonical|null $value): self
    {
        $this->capabilities = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }
}
