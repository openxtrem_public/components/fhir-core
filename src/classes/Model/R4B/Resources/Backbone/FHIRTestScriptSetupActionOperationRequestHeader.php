<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestScriptSetupActionOperationRequestHeader Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestScriptSetupActionOperationRequestHeaderInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRTestScriptSetupActionOperationRequestHeader extends FHIRBackboneElement implements FHIRTestScriptSetupActionOperationRequestHeaderInterface
{
    public const RESOURCE_NAME = 'TestScript.setup.action.operation.requestHeader';

    protected ?FHIRString $field = null;
    protected ?FHIRString $value = null;

    public function getField(): ?FHIRString
    {
        return $this->field;
    }

    public function setField(string|FHIRString|null $value): self
    {
        $this->field = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getValue(): ?FHIRString
    {
        return $this->value;
    }

    public function setValue(string|FHIRString|null $value): self
    {
        $this->value = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
