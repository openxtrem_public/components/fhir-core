<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR StructureMapGroupRuleTargetParameter Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRStructureMapGroupRuleTargetParameterInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRStructureMapGroupRuleTargetParameter extends FHIRBackboneElement implements FHIRStructureMapGroupRuleTargetParameterInterface
{
    public const RESOURCE_NAME = 'StructureMap.group.rule.target.parameter';

    protected FHIRId|FHIRString|FHIRBoolean|FHIRInteger|FHIRDecimal|null $value = null;

    public function getValue(): FHIRId|FHIRString|FHIRBoolean|FHIRInteger|FHIRDecimal|null
    {
        return $this->value;
    }

    public function setValue(FHIRId|FHIRString|FHIRBoolean|FHIRInteger|FHIRDecimal|null $value): self
    {
        $this->value = $value;

        return $this;
    }
}
