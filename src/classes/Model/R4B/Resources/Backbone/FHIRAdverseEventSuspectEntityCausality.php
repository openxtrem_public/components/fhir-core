<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AdverseEventSuspectEntityCausality Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAdverseEventSuspectEntityCausalityInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRAdverseEventSuspectEntityCausality extends FHIRBackboneElement implements FHIRAdverseEventSuspectEntityCausalityInterface
{
    public const RESOURCE_NAME = 'AdverseEvent.suspectEntity.causality';

    protected ?FHIRCodeableConcept $assessment = null;
    protected ?FHIRString $productRelatedness = null;
    protected ?FHIRReference $author = null;
    protected ?FHIRCodeableConcept $method = null;

    public function getAssessment(): ?FHIRCodeableConcept
    {
        return $this->assessment;
    }

    public function setAssessment(?FHIRCodeableConcept $value): self
    {
        $this->assessment = $value;

        return $this;
    }

    public function getProductRelatedness(): ?FHIRString
    {
        return $this->productRelatedness;
    }

    public function setProductRelatedness(string|FHIRString|null $value): self
    {
        $this->productRelatedness = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getAuthor(): ?FHIRReference
    {
        return $this->author;
    }

    public function setAuthor(?FHIRReference $value): self
    {
        $this->author = $value;

        return $this;
    }

    public function getMethod(): ?FHIRCodeableConcept
    {
        return $this->method;
    }

    public function setMethod(?FHIRCodeableConcept $value): self
    {
        $this->method = $value;

        return $this;
    }
}
