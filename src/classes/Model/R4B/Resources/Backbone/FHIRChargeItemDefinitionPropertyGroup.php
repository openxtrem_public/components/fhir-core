<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ChargeItemDefinitionPropertyGroup Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRChargeItemDefinitionPropertyGroupInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;

class FHIRChargeItemDefinitionPropertyGroup extends FHIRBackboneElement implements FHIRChargeItemDefinitionPropertyGroupInterface
{
    public const RESOURCE_NAME = 'ChargeItemDefinition.propertyGroup';

    /** @var FHIRChargeItemDefinitionApplicability[] */
    protected array $applicability = [];

    /** @var FHIRChargeItemDefinitionPropertyGroupPriceComponent[] */
    protected array $priceComponent = [];

    /**
     * @return FHIRChargeItemDefinitionApplicability[]
     */
    public function getApplicability(): array
    {
        return $this->applicability;
    }

    public function setApplicability(?FHIRChargeItemDefinitionApplicability ...$value): self
    {
        $this->applicability = array_filter($value);

        return $this;
    }

    public function addApplicability(?FHIRChargeItemDefinitionApplicability ...$value): self
    {
        $this->applicability = array_filter(array_merge($this->applicability, $value));

        return $this;
    }

    /**
     * @return FHIRChargeItemDefinitionPropertyGroupPriceComponent[]
     */
    public function getPriceComponent(): array
    {
        return $this->priceComponent;
    }

    public function setPriceComponent(?FHIRChargeItemDefinitionPropertyGroupPriceComponent ...$value): self
    {
        $this->priceComponent = array_filter($value);

        return $this;
    }

    public function addPriceComponent(?FHIRChargeItemDefinitionPropertyGroupPriceComponent ...$value): self
    {
        $this->priceComponent = array_filter(array_merge($this->priceComponent, $value));

        return $this;
    }
}
