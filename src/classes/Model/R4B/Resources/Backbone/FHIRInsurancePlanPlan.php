<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR InsurancePlanPlan Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRInsurancePlanPlanInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;

class FHIRInsurancePlanPlan extends FHIRBackboneElement implements FHIRInsurancePlanPlanInterface
{
    public const RESOURCE_NAME = 'InsurancePlan.plan';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRReference[] */
    protected array $coverageArea = [];

    /** @var FHIRReference[] */
    protected array $network = [];

    /** @var FHIRInsurancePlanPlanGeneralCost[] */
    protected array $generalCost = [];

    /** @var FHIRInsurancePlanPlanSpecificCost[] */
    protected array $specificCost = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getCoverageArea(): array
    {
        return $this->coverageArea;
    }

    public function setCoverageArea(?FHIRReference ...$value): self
    {
        $this->coverageArea = array_filter($value);

        return $this;
    }

    public function addCoverageArea(?FHIRReference ...$value): self
    {
        $this->coverageArea = array_filter(array_merge($this->coverageArea, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getNetwork(): array
    {
        return $this->network;
    }

    public function setNetwork(?FHIRReference ...$value): self
    {
        $this->network = array_filter($value);

        return $this;
    }

    public function addNetwork(?FHIRReference ...$value): self
    {
        $this->network = array_filter(array_merge($this->network, $value));

        return $this;
    }

    /**
     * @return FHIRInsurancePlanPlanGeneralCost[]
     */
    public function getGeneralCost(): array
    {
        return $this->generalCost;
    }

    public function setGeneralCost(?FHIRInsurancePlanPlanGeneralCost ...$value): self
    {
        $this->generalCost = array_filter($value);

        return $this;
    }

    public function addGeneralCost(?FHIRInsurancePlanPlanGeneralCost ...$value): self
    {
        $this->generalCost = array_filter(array_merge($this->generalCost, $value));

        return $this;
    }

    /**
     * @return FHIRInsurancePlanPlanSpecificCost[]
     */
    public function getSpecificCost(): array
    {
        return $this->specificCost;
    }

    public function setSpecificCost(?FHIRInsurancePlanPlanSpecificCost ...$value): self
    {
        $this->specificCost = array_filter($value);

        return $this;
    }

    public function addSpecificCost(?FHIRInsurancePlanPlanSpecificCost ...$value): self
    {
        $this->specificCost = array_filter(array_merge($this->specificCost, $value));

        return $this;
    }
}
