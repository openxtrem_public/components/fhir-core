<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ObservationDefinitionQualifiedInterval Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRObservationDefinitionQualifiedIntervalInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRObservationDefinitionQualifiedInterval extends FHIRBackboneElement implements FHIRObservationDefinitionQualifiedIntervalInterface
{
    public const RESOURCE_NAME = 'ObservationDefinition.qualifiedInterval';

    protected ?FHIRCode $category = null;
    protected ?FHIRRange $range = null;
    protected ?FHIRCodeableConcept $context = null;

    /** @var FHIRCodeableConcept[] */
    protected array $appliesTo = [];
    protected ?FHIRCode $gender = null;
    protected ?FHIRRange $age = null;
    protected ?FHIRRange $gestationalAge = null;
    protected ?FHIRString $condition = null;

    public function getCategory(): ?FHIRCode
    {
        return $this->category;
    }

    public function setCategory(string|FHIRCode|null $value): self
    {
        $this->category = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getRange(): ?FHIRRange
    {
        return $this->range;
    }

    public function setRange(?FHIRRange $value): self
    {
        $this->range = $value;

        return $this;
    }

    public function getContext(): ?FHIRCodeableConcept
    {
        return $this->context;
    }

    public function setContext(?FHIRCodeableConcept $value): self
    {
        $this->context = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getAppliesTo(): array
    {
        return $this->appliesTo;
    }

    public function setAppliesTo(?FHIRCodeableConcept ...$value): self
    {
        $this->appliesTo = array_filter($value);

        return $this;
    }

    public function addAppliesTo(?FHIRCodeableConcept ...$value): self
    {
        $this->appliesTo = array_filter(array_merge($this->appliesTo, $value));

        return $this;
    }

    public function getGender(): ?FHIRCode
    {
        return $this->gender;
    }

    public function setGender(string|FHIRCode|null $value): self
    {
        $this->gender = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getAge(): ?FHIRRange
    {
        return $this->age;
    }

    public function setAge(?FHIRRange $value): self
    {
        $this->age = $value;

        return $this;
    }

    public function getGestationalAge(): ?FHIRRange
    {
        return $this->gestationalAge;
    }

    public function setGestationalAge(?FHIRRange $value): self
    {
        $this->gestationalAge = $value;

        return $this;
    }

    public function getCondition(): ?FHIRString
    {
        return $this->condition;
    }

    public function setCondition(string|FHIRString|null $value): self
    {
        $this->condition = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
