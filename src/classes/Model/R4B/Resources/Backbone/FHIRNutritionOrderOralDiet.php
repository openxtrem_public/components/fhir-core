<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR NutritionOrderOralDiet Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRNutritionOrderOralDietInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRNutritionOrderOralDiet extends FHIRBackboneElement implements FHIRNutritionOrderOralDietInterface
{
    public const RESOURCE_NAME = 'NutritionOrder.oralDiet';

    /** @var FHIRCodeableConcept[] */
    protected array $type = [];

    /** @var FHIRTiming[] */
    protected array $schedule = [];

    /** @var FHIRNutritionOrderOralDietNutrient[] */
    protected array $nutrient = [];

    /** @var FHIRNutritionOrderOralDietTexture[] */
    protected array $texture = [];

    /** @var FHIRCodeableConcept[] */
    protected array $fluidConsistencyType = [];
    protected ?FHIRString $instruction = null;

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }

    /**
     * @return FHIRTiming[]
     */
    public function getSchedule(): array
    {
        return $this->schedule;
    }

    public function setSchedule(?FHIRTiming ...$value): self
    {
        $this->schedule = array_filter($value);

        return $this;
    }

    public function addSchedule(?FHIRTiming ...$value): self
    {
        $this->schedule = array_filter(array_merge($this->schedule, $value));

        return $this;
    }

    /**
     * @return FHIRNutritionOrderOralDietNutrient[]
     */
    public function getNutrient(): array
    {
        return $this->nutrient;
    }

    public function setNutrient(?FHIRNutritionOrderOralDietNutrient ...$value): self
    {
        $this->nutrient = array_filter($value);

        return $this;
    }

    public function addNutrient(?FHIRNutritionOrderOralDietNutrient ...$value): self
    {
        $this->nutrient = array_filter(array_merge($this->nutrient, $value));

        return $this;
    }

    /**
     * @return FHIRNutritionOrderOralDietTexture[]
     */
    public function getTexture(): array
    {
        return $this->texture;
    }

    public function setTexture(?FHIRNutritionOrderOralDietTexture ...$value): self
    {
        $this->texture = array_filter($value);

        return $this;
    }

    public function addTexture(?FHIRNutritionOrderOralDietTexture ...$value): self
    {
        $this->texture = array_filter(array_merge($this->texture, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getFluidConsistencyType(): array
    {
        return $this->fluidConsistencyType;
    }

    public function setFluidConsistencyType(?FHIRCodeableConcept ...$value): self
    {
        $this->fluidConsistencyType = array_filter($value);

        return $this;
    }

    public function addFluidConsistencyType(?FHIRCodeableConcept ...$value): self
    {
        $this->fluidConsistencyType = array_filter(array_merge($this->fluidConsistencyType, $value));

        return $this;
    }

    public function getInstruction(): ?FHIRString
    {
        return $this->instruction;
    }

    public function setInstruction(string|FHIRString|null $value): self
    {
        $this->instruction = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
