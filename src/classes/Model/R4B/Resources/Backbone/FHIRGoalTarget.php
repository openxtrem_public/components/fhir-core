<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR GoalTarget Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRGoalTargetInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRRatio;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRGoalTarget extends FHIRBackboneElement implements FHIRGoalTargetInterface
{
    public const RESOURCE_NAME = 'Goal.target';

    protected ?FHIRCodeableConcept $measure = null;
    protected FHIRQuantity|FHIRRange|FHIRCodeableConcept|FHIRString|FHIRBoolean|FHIRInteger|FHIRRatio|null $detail = null;
    protected FHIRDate|FHIRDuration|null $due = null;

    public function getMeasure(): ?FHIRCodeableConcept
    {
        return $this->measure;
    }

    public function setMeasure(?FHIRCodeableConcept $value): self
    {
        $this->measure = $value;

        return $this;
    }

    public function getDetail(
    ): FHIRQuantity|FHIRRange|FHIRCodeableConcept|FHIRString|FHIRBoolean|FHIRInteger|FHIRRatio|null
    {
        return $this->detail;
    }

    public function setDetail(
        FHIRQuantity|FHIRRange|FHIRCodeableConcept|FHIRString|FHIRBoolean|FHIRInteger|FHIRRatio|null $value,
    ): self
    {
        $this->detail = $value;

        return $this;
    }

    public function getDue(): FHIRDate|FHIRDuration|null
    {
        return $this->due;
    }

    public function setDue(FHIRDate|FHIRDuration|null $value): self
    {
        $this->due = $value;

        return $this;
    }
}
