<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EvidenceCertainty Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREvidenceCertaintyInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIREvidenceCertainty extends FHIRBackboneElement implements FHIREvidenceCertaintyInterface
{
    public const RESOURCE_NAME = 'Evidence.certainty';

    protected ?FHIRString $description = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableConcept $rating = null;
    protected ?FHIRString $rater = null;

    /** @var FHIREvidenceCertainty[] */
    protected array $subcomponent = [];

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getRating(): ?FHIRCodeableConcept
    {
        return $this->rating;
    }

    public function setRating(?FHIRCodeableConcept $value): self
    {
        $this->rating = $value;

        return $this;
    }

    public function getRater(): ?FHIRString
    {
        return $this->rater;
    }

    public function setRater(string|FHIRString|null $value): self
    {
        $this->rater = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIREvidenceCertainty[]
     */
    public function getSubcomponent(): array
    {
        return $this->subcomponent;
    }

    public function setSubcomponent(?FHIREvidenceCertainty ...$value): self
    {
        $this->subcomponent = array_filter($value);

        return $this;
    }

    public function addSubcomponent(?FHIREvidenceCertainty ...$value): self
    {
        $this->subcomponent = array_filter(array_merge($this->subcomponent, $value));

        return $this;
    }
}
