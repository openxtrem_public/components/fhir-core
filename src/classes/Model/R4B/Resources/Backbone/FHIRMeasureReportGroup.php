<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MeasureReportGroup Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMeasureReportGroupInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRQuantity;

class FHIRMeasureReportGroup extends FHIRBackboneElement implements FHIRMeasureReportGroupInterface
{
    public const RESOURCE_NAME = 'MeasureReport.group';

    protected ?FHIRCodeableConcept $code = null;

    /** @var FHIRMeasureReportGroupPopulation[] */
    protected array $population = [];
    protected ?FHIRQuantity $measureScore = null;

    /** @var FHIRMeasureReportGroupStratifier[] */
    protected array $stratifier = [];

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * @return FHIRMeasureReportGroupPopulation[]
     */
    public function getPopulation(): array
    {
        return $this->population;
    }

    public function setPopulation(?FHIRMeasureReportGroupPopulation ...$value): self
    {
        $this->population = array_filter($value);

        return $this;
    }

    public function addPopulation(?FHIRMeasureReportGroupPopulation ...$value): self
    {
        $this->population = array_filter(array_merge($this->population, $value));

        return $this;
    }

    public function getMeasureScore(): ?FHIRQuantity
    {
        return $this->measureScore;
    }

    public function setMeasureScore(?FHIRQuantity $value): self
    {
        $this->measureScore = $value;

        return $this;
    }

    /**
     * @return FHIRMeasureReportGroupStratifier[]
     */
    public function getStratifier(): array
    {
        return $this->stratifier;
    }

    public function setStratifier(?FHIRMeasureReportGroupStratifier ...$value): self
    {
        $this->stratifier = array_filter($value);

        return $this;
    }

    public function addStratifier(?FHIRMeasureReportGroupStratifier ...$value): self
    {
        $this->stratifier = array_filter(array_merge($this->stratifier, $value));

        return $this;
    }
}
