<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SpecimenDefinitionTypeTestedHandling Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSpecimenDefinitionTypeTestedHandlingInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRSpecimenDefinitionTypeTestedHandling extends FHIRBackboneElement implements FHIRSpecimenDefinitionTypeTestedHandlingInterface
{
    public const RESOURCE_NAME = 'SpecimenDefinition.typeTested.handling';

    protected ?FHIRCodeableConcept $temperatureQualifier = null;
    protected ?FHIRRange $temperatureRange = null;
    protected ?FHIRDuration $maxDuration = null;
    protected ?FHIRString $instruction = null;

    public function getTemperatureQualifier(): ?FHIRCodeableConcept
    {
        return $this->temperatureQualifier;
    }

    public function setTemperatureQualifier(?FHIRCodeableConcept $value): self
    {
        $this->temperatureQualifier = $value;

        return $this;
    }

    public function getTemperatureRange(): ?FHIRRange
    {
        return $this->temperatureRange;
    }

    public function setTemperatureRange(?FHIRRange $value): self
    {
        $this->temperatureRange = $value;

        return $this;
    }

    public function getMaxDuration(): ?FHIRDuration
    {
        return $this->maxDuration;
    }

    public function setMaxDuration(?FHIRDuration $value): self
    {
        $this->maxDuration = $value;

        return $this;
    }

    public function getInstruction(): ?FHIRString
    {
        return $this->instruction;
    }

    public function setInstruction(string|FHIRString|null $value): self
    {
        $this->instruction = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
