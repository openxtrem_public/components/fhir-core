<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledgeAdministrationGuidelines Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationKnowledgeAdministrationGuidelinesInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;

class FHIRMedicationKnowledgeAdministrationGuidelines extends FHIRBackboneElement implements FHIRMedicationKnowledgeAdministrationGuidelinesInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge.administrationGuidelines';

    /** @var FHIRMedicationKnowledgeAdministrationGuidelinesDosage[] */
    protected array $dosage = [];
    protected FHIRCodeableConcept|FHIRReference|null $indication = null;

    /** @var FHIRMedicationKnowledgeAdministrationGuidelinesPatientCharacteristics[] */
    protected array $patientCharacteristics = [];

    /**
     * @return FHIRMedicationKnowledgeAdministrationGuidelinesDosage[]
     */
    public function getDosage(): array
    {
        return $this->dosage;
    }

    public function setDosage(?FHIRMedicationKnowledgeAdministrationGuidelinesDosage ...$value): self
    {
        $this->dosage = array_filter($value);

        return $this;
    }

    public function addDosage(?FHIRMedicationKnowledgeAdministrationGuidelinesDosage ...$value): self
    {
        $this->dosage = array_filter(array_merge($this->dosage, $value));

        return $this;
    }

    public function getIndication(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->indication;
    }

    public function setIndication(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->indication = $value;

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeAdministrationGuidelinesPatientCharacteristics[]
     */
    public function getPatientCharacteristics(): array
    {
        return $this->patientCharacteristics;
    }

    public function setPatientCharacteristics(
        ?FHIRMedicationKnowledgeAdministrationGuidelinesPatientCharacteristics ...$value,
    ): self
    {
        $this->patientCharacteristics = array_filter($value);

        return $this;
    }

    public function addPatientCharacteristics(
        ?FHIRMedicationKnowledgeAdministrationGuidelinesPatientCharacteristics ...$value,
    ): self
    {
        $this->patientCharacteristics = array_filter(array_merge($this->patientCharacteristics, $value));

        return $this;
    }
}
