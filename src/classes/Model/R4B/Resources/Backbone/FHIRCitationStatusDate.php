<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CitationStatusDate Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCitationStatusDateInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;

class FHIRCitationStatusDate extends FHIRBackboneElement implements FHIRCitationStatusDateInterface
{
    public const RESOURCE_NAME = 'Citation.statusDate';

    protected ?FHIRCodeableConcept $activity = null;
    protected ?FHIRBoolean $actual = null;
    protected ?FHIRPeriod $period = null;

    public function getActivity(): ?FHIRCodeableConcept
    {
        return $this->activity;
    }

    public function setActivity(?FHIRCodeableConcept $value): self
    {
        $this->activity = $value;

        return $this;
    }

    public function getActual(): ?FHIRBoolean
    {
        return $this->actual;
    }

    public function setActual(bool|FHIRBoolean|null $value): self
    {
        $this->actual = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }
}
