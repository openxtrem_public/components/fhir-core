<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MolecularSequenceStructureVariant Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMolecularSequenceStructureVariantInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRInteger;

class FHIRMolecularSequenceStructureVariant extends FHIRBackboneElement implements FHIRMolecularSequenceStructureVariantInterface
{
    public const RESOURCE_NAME = 'MolecularSequence.structureVariant';

    protected ?FHIRCodeableConcept $variantType = null;
    protected ?FHIRBoolean $exact = null;
    protected ?FHIRInteger $length = null;
    protected ?FHIRMolecularSequenceStructureVariantOuter $outer = null;
    protected ?FHIRMolecularSequenceStructureVariantInner $inner = null;

    public function getVariantType(): ?FHIRCodeableConcept
    {
        return $this->variantType;
    }

    public function setVariantType(?FHIRCodeableConcept $value): self
    {
        $this->variantType = $value;

        return $this;
    }

    public function getExact(): ?FHIRBoolean
    {
        return $this->exact;
    }

    public function setExact(bool|FHIRBoolean|null $value): self
    {
        $this->exact = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getLength(): ?FHIRInteger
    {
        return $this->length;
    }

    public function setLength(int|FHIRInteger|null $value): self
    {
        $this->length = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getOuter(): ?FHIRMolecularSequenceStructureVariantOuter
    {
        return $this->outer;
    }

    public function setOuter(?FHIRMolecularSequenceStructureVariantOuter $value): self
    {
        $this->outer = $value;

        return $this;
    }

    public function getInner(): ?FHIRMolecularSequenceStructureVariantInner
    {
        return $this->inner;
    }

    public function setInner(?FHIRMolecularSequenceStructureVariantInner $value): self
    {
        $this->inner = $value;

        return $this;
    }
}
