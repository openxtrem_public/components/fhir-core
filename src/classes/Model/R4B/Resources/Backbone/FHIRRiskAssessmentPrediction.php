<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR RiskAssessmentPrediction Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRRiskAssessmentPredictionInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRRiskAssessmentPrediction extends FHIRBackboneElement implements FHIRRiskAssessmentPredictionInterface
{
    public const RESOURCE_NAME = 'RiskAssessment.prediction';

    protected ?FHIRCodeableConcept $outcome = null;
    protected FHIRDecimal|FHIRRange|null $probability = null;
    protected ?FHIRCodeableConcept $qualitativeRisk = null;
    protected ?FHIRDecimal $relativeRisk = null;
    protected FHIRPeriod|FHIRRange|null $when = null;
    protected ?FHIRString $rationale = null;

    public function getOutcome(): ?FHIRCodeableConcept
    {
        return $this->outcome;
    }

    public function setOutcome(?FHIRCodeableConcept $value): self
    {
        $this->outcome = $value;

        return $this;
    }

    public function getProbability(): FHIRDecimal|FHIRRange|null
    {
        return $this->probability;
    }

    public function setProbability(FHIRDecimal|FHIRRange|null $value): self
    {
        $this->probability = $value;

        return $this;
    }

    public function getQualitativeRisk(): ?FHIRCodeableConcept
    {
        return $this->qualitativeRisk;
    }

    public function setQualitativeRisk(?FHIRCodeableConcept $value): self
    {
        $this->qualitativeRisk = $value;

        return $this;
    }

    public function getRelativeRisk(): ?FHIRDecimal
    {
        return $this->relativeRisk;
    }

    public function setRelativeRisk(float|FHIRDecimal|null $value): self
    {
        $this->relativeRisk = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getWhen(): FHIRPeriod|FHIRRange|null
    {
        return $this->when;
    }

    public function setWhen(FHIRPeriod|FHIRRange|null $value): self
    {
        $this->when = $value;

        return $this;
    }

    public function getRationale(): ?FHIRString
    {
        return $this->rationale;
    }

    public function setRationale(string|FHIRString|null $value): self
    {
        $this->rationale = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
