<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExampleScenarioInstanceContainedInstance Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExampleScenarioInstanceContainedInstanceInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRExampleScenarioInstanceContainedInstance extends FHIRBackboneElement implements FHIRExampleScenarioInstanceContainedInstanceInterface
{
    public const RESOURCE_NAME = 'ExampleScenario.instance.containedInstance';

    protected ?FHIRString $resourceId = null;
    protected ?FHIRString $versionId = null;

    public function getResourceId(): ?FHIRString
    {
        return $this->resourceId;
    }

    public function setResourceId(string|FHIRString|null $value): self
    {
        $this->resourceId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getVersionId(): ?FHIRString
    {
        return $this->versionId;
    }

    public function setVersionId(string|FHIRString|null $value): self
    {
        $this->versionId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
