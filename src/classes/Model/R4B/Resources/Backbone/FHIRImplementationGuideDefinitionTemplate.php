<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImplementationGuideDefinitionTemplate Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImplementationGuideDefinitionTemplateInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRImplementationGuideDefinitionTemplate extends FHIRBackboneElement implements FHIRImplementationGuideDefinitionTemplateInterface
{
    public const RESOURCE_NAME = 'ImplementationGuide.definition.template';

    protected ?FHIRCode $code = null;
    protected ?FHIRString $source = null;
    protected ?FHIRString $scope = null;

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getSource(): ?FHIRString
    {
        return $this->source;
    }

    public function setSource(string|FHIRString|null $value): self
    {
        $this->source = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getScope(): ?FHIRString
    {
        return $this->scope;
    }

    public function setScope(string|FHIRString|null $value): self
    {
        $this->scope = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
