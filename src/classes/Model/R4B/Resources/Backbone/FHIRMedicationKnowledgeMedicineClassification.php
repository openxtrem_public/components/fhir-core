<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledgeMedicineClassification Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationKnowledgeMedicineClassificationInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;

class FHIRMedicationKnowledgeMedicineClassification extends FHIRBackboneElement implements FHIRMedicationKnowledgeMedicineClassificationInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge.medicineClassification';

    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRCodeableConcept[] */
    protected array $classification = [];

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getClassification(): array
    {
        return $this->classification;
    }

    public function setClassification(?FHIRCodeableConcept ...$value): self
    {
        $this->classification = array_filter($value);

        return $this;
    }

    public function addClassification(?FHIRCodeableConcept ...$value): self
    {
        $this->classification = array_filter(array_merge($this->classification, $value));

        return $this;
    }
}
