<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledgeKinetics Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationKnowledgeKineticsInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRQuantity;

class FHIRMedicationKnowledgeKinetics extends FHIRBackboneElement implements FHIRMedicationKnowledgeKineticsInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge.kinetics';

    /** @var FHIRQuantity[] */
    protected array $areaUnderCurve = [];

    /** @var FHIRQuantity[] */
    protected array $lethalDose50 = [];
    protected ?FHIRDuration $halfLifePeriod = null;

    /**
     * @return FHIRQuantity[]
     */
    public function getAreaUnderCurve(): array
    {
        return $this->areaUnderCurve;
    }

    public function setAreaUnderCurve(?FHIRQuantity ...$value): self
    {
        $this->areaUnderCurve = array_filter($value);

        return $this;
    }

    public function addAreaUnderCurve(?FHIRQuantity ...$value): self
    {
        $this->areaUnderCurve = array_filter(array_merge($this->areaUnderCurve, $value));

        return $this;
    }

    /**
     * @return FHIRQuantity[]
     */
    public function getLethalDose50(): array
    {
        return $this->lethalDose50;
    }

    public function setLethalDose50(?FHIRQuantity ...$value): self
    {
        $this->lethalDose50 = array_filter($value);

        return $this;
    }

    public function addLethalDose50(?FHIRQuantity ...$value): self
    {
        $this->lethalDose50 = array_filter(array_merge($this->lethalDose50, $value));

        return $this;
    }

    public function getHalfLifePeriod(): ?FHIRDuration
    {
        return $this->halfLifePeriod;
    }

    public function setHalfLifePeriod(?FHIRDuration $value): self
    {
        $this->halfLifePeriod = $value;

        return $this;
    }
}
