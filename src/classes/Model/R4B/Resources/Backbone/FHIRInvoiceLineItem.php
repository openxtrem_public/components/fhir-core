<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR InvoiceLineItem Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRInvoiceLineItemInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRPositiveInt;

class FHIRInvoiceLineItem extends FHIRBackboneElement implements FHIRInvoiceLineItemInterface
{
    public const RESOURCE_NAME = 'Invoice.lineItem';

    protected ?FHIRPositiveInt $sequence = null;
    protected FHIRReference|FHIRCodeableConcept|null $chargeItem = null;

    /** @var FHIRInvoiceLineItemPriceComponent[] */
    protected array $priceComponent = [];

    public function getSequence(): ?FHIRPositiveInt
    {
        return $this->sequence;
    }

    public function setSequence(int|FHIRPositiveInt|null $value): self
    {
        $this->sequence = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getChargeItem(): FHIRReference|FHIRCodeableConcept|null
    {
        return $this->chargeItem;
    }

    public function setChargeItem(FHIRReference|FHIRCodeableConcept|null $value): self
    {
        $this->chargeItem = $value;

        return $this;
    }

    /**
     * @return FHIRInvoiceLineItemPriceComponent[]
     */
    public function getPriceComponent(): array
    {
        return $this->priceComponent;
    }

    public function setPriceComponent(?FHIRInvoiceLineItemPriceComponent ...$value): self
    {
        $this->priceComponent = array_filter($value);

        return $this;
    }

    public function addPriceComponent(?FHIRInvoiceLineItemPriceComponent ...$value): self
    {
        $this->priceComponent = array_filter(array_merge($this->priceComponent, $value));

        return $this;
    }
}
