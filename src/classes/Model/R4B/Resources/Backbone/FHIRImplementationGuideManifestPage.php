<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImplementationGuideManifestPage Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImplementationGuideManifestPageInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRImplementationGuideManifestPage extends FHIRBackboneElement implements FHIRImplementationGuideManifestPageInterface
{
    public const RESOURCE_NAME = 'ImplementationGuide.manifest.page';

    protected ?FHIRString $name = null;
    protected ?FHIRString $title = null;

    /** @var FHIRString[] */
    protected array $anchor = [];

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getAnchor(): array
    {
        return $this->anchor;
    }

    public function setAnchor(string|FHIRString|null ...$value): self
    {
        $this->anchor = [];
        $this->addAnchor(...$value);

        return $this;
    }

    public function addAnchor(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->anchor = array_filter(array_merge($this->anchor, $values));

        return $this;
    }
}
