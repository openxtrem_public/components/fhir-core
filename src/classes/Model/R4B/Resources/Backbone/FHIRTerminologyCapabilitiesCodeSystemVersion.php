<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TerminologyCapabilitiesCodeSystemVersion Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTerminologyCapabilitiesCodeSystemVersionInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRTerminologyCapabilitiesCodeSystemVersion extends FHIRBackboneElement implements FHIRTerminologyCapabilitiesCodeSystemVersionInterface
{
    public const RESOURCE_NAME = 'TerminologyCapabilities.codeSystem.version';

    protected ?FHIRString $code = null;
    protected ?FHIRBoolean $isDefault = null;
    protected ?FHIRBoolean $compositional = null;

    /** @var FHIRCode[] */
    protected array $language = [];

    /** @var FHIRTerminologyCapabilitiesCodeSystemVersionFilter[] */
    protected array $filter = [];

    /** @var FHIRCode[] */
    protected array $property = [];

    public function getCode(): ?FHIRString
    {
        return $this->code;
    }

    public function setCode(string|FHIRString|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getIsDefault(): ?FHIRBoolean
    {
        return $this->isDefault;
    }

    public function setIsDefault(bool|FHIRBoolean|null $value): self
    {
        $this->isDefault = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getCompositional(): ?FHIRBoolean
    {
        return $this->compositional;
    }

    public function setCompositional(bool|FHIRBoolean|null $value): self
    {
        $this->compositional = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getLanguage(): array
    {
        return $this->language;
    }

    public function setLanguage(string|FHIRCode|null ...$value): self
    {
        $this->language = [];
        $this->addLanguage(...$value);

        return $this;
    }

    public function addLanguage(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->language = array_filter(array_merge($this->language, $values));

        return $this;
    }

    /**
     * @return FHIRTerminologyCapabilitiesCodeSystemVersionFilter[]
     */
    public function getFilter(): array
    {
        return $this->filter;
    }

    public function setFilter(?FHIRTerminologyCapabilitiesCodeSystemVersionFilter ...$value): self
    {
        $this->filter = array_filter($value);

        return $this;
    }

    public function addFilter(?FHIRTerminologyCapabilitiesCodeSystemVersionFilter ...$value): self
    {
        $this->filter = array_filter(array_merge($this->filter, $value));

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getProperty(): array
    {
        return $this->property;
    }

    public function setProperty(string|FHIRCode|null ...$value): self
    {
        $this->property = [];
        $this->addProperty(...$value);

        return $this;
    }

    public function addProperty(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->property = array_filter(array_merge($this->property, $values));

        return $this;
    }
}
