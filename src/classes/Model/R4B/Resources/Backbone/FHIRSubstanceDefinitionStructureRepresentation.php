<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceDefinitionStructureRepresentation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceDefinitionStructureRepresentationInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRSubstanceDefinitionStructureRepresentation extends FHIRBackboneElement implements FHIRSubstanceDefinitionStructureRepresentationInterface
{
    public const RESOURCE_NAME = 'SubstanceDefinition.structure.representation';

    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRString $representation = null;
    protected ?FHIRCodeableConcept $format = null;
    protected ?FHIRReference $document = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getRepresentation(): ?FHIRString
    {
        return $this->representation;
    }

    public function setRepresentation(string|FHIRString|null $value): self
    {
        $this->representation = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getFormat(): ?FHIRCodeableConcept
    {
        return $this->format;
    }

    public function setFormat(?FHIRCodeableConcept $value): self
    {
        $this->format = $value;

        return $this;
    }

    public function getDocument(): ?FHIRReference
    {
        return $this->document;
    }

    public function setDocument(?FHIRReference $value): self
    {
        $this->document = $value;

        return $this;
    }
}
