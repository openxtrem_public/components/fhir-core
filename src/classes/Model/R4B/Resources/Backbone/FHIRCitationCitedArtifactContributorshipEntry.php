<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CitationCitedArtifactContributorshipEntry Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCitationCitedArtifactContributorshipEntryInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRAddress;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRHumanName;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRCitationCitedArtifactContributorshipEntry extends FHIRBackboneElement implements FHIRCitationCitedArtifactContributorshipEntryInterface
{
    public const RESOURCE_NAME = 'Citation.citedArtifact.contributorship.entry';

    protected ?FHIRHumanName $name = null;
    protected ?FHIRString $initials = null;
    protected ?FHIRString $collectiveName = null;

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRCitationCitedArtifactContributorshipEntryAffiliationInfo[] */
    protected array $affiliationInfo = [];

    /** @var FHIRAddress[] */
    protected array $address = [];

    /** @var FHIRContactPoint[] */
    protected array $telecom = [];

    /** @var FHIRCodeableConcept[] */
    protected array $contributionType = [];
    protected ?FHIRCodeableConcept $role = null;

    /** @var FHIRCitationCitedArtifactContributorshipEntryContributionInstance[] */
    protected array $contributionInstance = [];
    protected ?FHIRBoolean $correspondingContact = null;
    protected ?FHIRPositiveInt $listOrder = null;

    public function getName(): ?FHIRHumanName
    {
        return $this->name;
    }

    public function setName(?FHIRHumanName $value): self
    {
        $this->name = $value;

        return $this;
    }

    public function getInitials(): ?FHIRString
    {
        return $this->initials;
    }

    public function setInitials(string|FHIRString|null $value): self
    {
        $this->initials = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCollectiveName(): ?FHIRString
    {
        return $this->collectiveName;
    }

    public function setCollectiveName(string|FHIRString|null $value): self
    {
        $this->collectiveName = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRCitationCitedArtifactContributorshipEntryAffiliationInfo[]
     */
    public function getAffiliationInfo(): array
    {
        return $this->affiliationInfo;
    }

    public function setAffiliationInfo(?FHIRCitationCitedArtifactContributorshipEntryAffiliationInfo ...$value): self
    {
        $this->affiliationInfo = array_filter($value);

        return $this;
    }

    public function addAffiliationInfo(?FHIRCitationCitedArtifactContributorshipEntryAffiliationInfo ...$value): self
    {
        $this->affiliationInfo = array_filter(array_merge($this->affiliationInfo, $value));

        return $this;
    }

    /**
     * @return FHIRAddress[]
     */
    public function getAddress(): array
    {
        return $this->address;
    }

    public function setAddress(?FHIRAddress ...$value): self
    {
        $this->address = array_filter($value);

        return $this;
    }

    public function addAddress(?FHIRAddress ...$value): self
    {
        $this->address = array_filter(array_merge($this->address, $value));

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getTelecom(): array
    {
        return $this->telecom;
    }

    public function setTelecom(?FHIRContactPoint ...$value): self
    {
        $this->telecom = array_filter($value);

        return $this;
    }

    public function addTelecom(?FHIRContactPoint ...$value): self
    {
        $this->telecom = array_filter(array_merge($this->telecom, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getContributionType(): array
    {
        return $this->contributionType;
    }

    public function setContributionType(?FHIRCodeableConcept ...$value): self
    {
        $this->contributionType = array_filter($value);

        return $this;
    }

    public function addContributionType(?FHIRCodeableConcept ...$value): self
    {
        $this->contributionType = array_filter(array_merge($this->contributionType, $value));

        return $this;
    }

    public function getRole(): ?FHIRCodeableConcept
    {
        return $this->role;
    }

    public function setRole(?FHIRCodeableConcept $value): self
    {
        $this->role = $value;

        return $this;
    }

    /**
     * @return FHIRCitationCitedArtifactContributorshipEntryContributionInstance[]
     */
    public function getContributionInstance(): array
    {
        return $this->contributionInstance;
    }

    public function setContributionInstance(
        ?FHIRCitationCitedArtifactContributorshipEntryContributionInstance ...$value,
    ): self
    {
        $this->contributionInstance = array_filter($value);

        return $this;
    }

    public function addContributionInstance(
        ?FHIRCitationCitedArtifactContributorshipEntryContributionInstance ...$value,
    ): self
    {
        $this->contributionInstance = array_filter(array_merge($this->contributionInstance, $value));

        return $this;
    }

    public function getCorrespondingContact(): ?FHIRBoolean
    {
        return $this->correspondingContact;
    }

    public function setCorrespondingContact(bool|FHIRBoolean|null $value): self
    {
        $this->correspondingContact = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getListOrder(): ?FHIRPositiveInt
    {
        return $this->listOrder;
    }

    public function setListOrder(int|FHIRPositiveInt|null $value): self
    {
        $this->listOrder = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }
}
