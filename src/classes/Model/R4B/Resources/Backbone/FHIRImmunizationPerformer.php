<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImmunizationPerformer Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImmunizationPerformerInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;

class FHIRImmunizationPerformer extends FHIRBackboneElement implements FHIRImmunizationPerformerInterface
{
    public const RESOURCE_NAME = 'Immunization.performer';

    protected ?FHIRCodeableConcept $function = null;
    protected ?FHIRReference $actor = null;

    public function getFunction(): ?FHIRCodeableConcept
    {
        return $this->function;
    }

    public function setFunction(?FHIRCodeableConcept $value): self
    {
        $this->function = $value;

        return $this;
    }

    public function getActor(): ?FHIRReference
    {
        return $this->actor;
    }

    public function setActor(?FHIRReference $value): self
    {
        $this->actor = $value;

        return $this;
    }
}
