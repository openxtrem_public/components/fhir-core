<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CitationCitedArtifactPublicationFormPeriodicRelease Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCitationCitedArtifactPublicationFormPeriodicReleaseInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRCitationCitedArtifactPublicationFormPeriodicRelease extends FHIRBackboneElement implements FHIRCitationCitedArtifactPublicationFormPeriodicReleaseInterface
{
    public const RESOURCE_NAME = 'Citation.citedArtifact.publicationForm.periodicRelease';

    protected ?FHIRCodeableConcept $citedMedium = null;
    protected ?FHIRString $volume = null;
    protected ?FHIRString $issue = null;
    protected ?FHIRCitationCitedArtifactPublicationFormPeriodicReleaseDateOfPublication $dateOfPublication = null;

    public function getCitedMedium(): ?FHIRCodeableConcept
    {
        return $this->citedMedium;
    }

    public function setCitedMedium(?FHIRCodeableConcept $value): self
    {
        $this->citedMedium = $value;

        return $this;
    }

    public function getVolume(): ?FHIRString
    {
        return $this->volume;
    }

    public function setVolume(string|FHIRString|null $value): self
    {
        $this->volume = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getIssue(): ?FHIRString
    {
        return $this->issue;
    }

    public function setIssue(string|FHIRString|null $value): self
    {
        $this->issue = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDateOfPublication(): ?FHIRCitationCitedArtifactPublicationFormPeriodicReleaseDateOfPublication
    {
        return $this->dateOfPublication;
    }

    public function setDateOfPublication(
        ?FHIRCitationCitedArtifactPublicationFormPeriodicReleaseDateOfPublication $value,
    ): self
    {
        $this->dateOfPublication = $value;

        return $this;
    }
}
