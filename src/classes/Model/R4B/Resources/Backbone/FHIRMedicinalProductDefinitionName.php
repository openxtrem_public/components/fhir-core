<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductDefinitionName Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductDefinitionNameInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRMedicinalProductDefinitionName extends FHIRBackboneElement implements FHIRMedicinalProductDefinitionNameInterface
{
    public const RESOURCE_NAME = 'MedicinalProductDefinition.name';

    protected ?FHIRString $productName = null;
    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRMedicinalProductDefinitionNameNamePart[] */
    protected array $namePart = [];

    /** @var FHIRMedicinalProductDefinitionNameCountryLanguage[] */
    protected array $countryLanguage = [];

    public function getProductName(): ?FHIRString
    {
        return $this->productName;
    }

    public function setProductName(string|FHIRString|null $value): self
    {
        $this->productName = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRMedicinalProductDefinitionNameNamePart[]
     */
    public function getNamePart(): array
    {
        return $this->namePart;
    }

    public function setNamePart(?FHIRMedicinalProductDefinitionNameNamePart ...$value): self
    {
        $this->namePart = array_filter($value);

        return $this;
    }

    public function addNamePart(?FHIRMedicinalProductDefinitionNameNamePart ...$value): self
    {
        $this->namePart = array_filter(array_merge($this->namePart, $value));

        return $this;
    }

    /**
     * @return FHIRMedicinalProductDefinitionNameCountryLanguage[]
     */
    public function getCountryLanguage(): array
    {
        return $this->countryLanguage;
    }

    public function setCountryLanguage(?FHIRMedicinalProductDefinitionNameCountryLanguage ...$value): self
    {
        $this->countryLanguage = array_filter($value);

        return $this;
    }

    public function addCountryLanguage(?FHIRMedicinalProductDefinitionNameCountryLanguage ...$value): self
    {
        $this->countryLanguage = array_filter(array_merge($this->countryLanguage, $value));

        return $this;
    }
}
