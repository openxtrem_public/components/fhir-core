<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CoverageEligibilityRequestItemDiagnosis Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCoverageEligibilityRequestItemDiagnosisInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;

class FHIRCoverageEligibilityRequestItemDiagnosis extends FHIRBackboneElement implements FHIRCoverageEligibilityRequestItemDiagnosisInterface
{
    public const RESOURCE_NAME = 'CoverageEligibilityRequest.item.diagnosis';

    protected FHIRCodeableConcept|FHIRReference|null $diagnosis = null;

    public function getDiagnosis(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->diagnosis;
    }

    public function setDiagnosis(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->diagnosis = $value;

        return $this;
    }
}
