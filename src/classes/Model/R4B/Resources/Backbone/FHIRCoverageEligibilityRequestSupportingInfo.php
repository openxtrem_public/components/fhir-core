<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CoverageEligibilityRequestSupportingInfo Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCoverageEligibilityRequestSupportingInfoInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRPositiveInt;

class FHIRCoverageEligibilityRequestSupportingInfo extends FHIRBackboneElement implements FHIRCoverageEligibilityRequestSupportingInfoInterface
{
    public const RESOURCE_NAME = 'CoverageEligibilityRequest.supportingInfo';

    protected ?FHIRPositiveInt $sequence = null;
    protected ?FHIRReference $information = null;
    protected ?FHIRBoolean $appliesToAll = null;

    public function getSequence(): ?FHIRPositiveInt
    {
        return $this->sequence;
    }

    public function setSequence(int|FHIRPositiveInt|null $value): self
    {
        $this->sequence = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getInformation(): ?FHIRReference
    {
        return $this->information;
    }

    public function setInformation(?FHIRReference $value): self
    {
        $this->information = $value;

        return $this;
    }

    public function getAppliesToAll(): ?FHIRBoolean
    {
        return $this->appliesToAll;
    }

    public function setAppliesToAll(bool|FHIRBoolean|null $value): self
    {
        $this->appliesToAll = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }
}
