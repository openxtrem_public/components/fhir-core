<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MeasureReportGroupStratifierStratumPopulation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMeasureReportGroupStratifierStratumPopulationInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRInteger;

class FHIRMeasureReportGroupStratifierStratumPopulation extends FHIRBackboneElement implements FHIRMeasureReportGroupStratifierStratumPopulationInterface
{
    public const RESOURCE_NAME = 'MeasureReport.group.stratifier.stratum.population';

    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRInteger $count = null;
    protected ?FHIRReference $subjectResults = null;

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getCount(): ?FHIRInteger
    {
        return $this->count;
    }

    public function setCount(int|FHIRInteger|null $value): self
    {
        $this->count = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getSubjectResults(): ?FHIRReference
    {
        return $this->subjectResults;
    }

    public function setSubjectResults(?FHIRReference $value): self
    {
        $this->subjectResults = $value;

        return $this;
    }
}
