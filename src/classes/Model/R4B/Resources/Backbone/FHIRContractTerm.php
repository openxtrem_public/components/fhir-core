<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ContractTerm Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRContractTermInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRContractTerm extends FHIRBackboneElement implements FHIRContractTermInterface
{
    public const RESOURCE_NAME = 'Contract.term';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRDateTime $issued = null;
    protected ?FHIRPeriod $applies = null;
    protected FHIRCodeableConcept|FHIRReference|null $topic = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableConcept $subType = null;
    protected ?FHIRString $text = null;

    /** @var FHIRContractTermSecurityLabel[] */
    protected array $securityLabel = [];
    protected ?FHIRContractTermOffer $offer = null;

    /** @var FHIRContractTermAsset[] */
    protected array $asset = [];

    /** @var FHIRContractTermAction[] */
    protected array $action = [];

    /** @var FHIRContractTerm[] */
    protected array $group = [];

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getIssued(): ?FHIRDateTime
    {
        return $this->issued;
    }

    public function setIssued(string|FHIRDateTime|null $value): self
    {
        $this->issued = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getApplies(): ?FHIRPeriod
    {
        return $this->applies;
    }

    public function setApplies(?FHIRPeriod $value): self
    {
        $this->applies = $value;

        return $this;
    }

    public function getTopic(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->topic;
    }

    public function setTopic(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->topic = $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getSubType(): ?FHIRCodeableConcept
    {
        return $this->subType;
    }

    public function setSubType(?FHIRCodeableConcept $value): self
    {
        $this->subType = $value;

        return $this;
    }

    public function getText(): ?FHIRString
    {
        return $this->text;
    }

    public function setText(string|FHIRString|null $value): self
    {
        $this->text = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRContractTermSecurityLabel[]
     */
    public function getSecurityLabel(): array
    {
        return $this->securityLabel;
    }

    public function setSecurityLabel(?FHIRContractTermSecurityLabel ...$value): self
    {
        $this->securityLabel = array_filter($value);

        return $this;
    }

    public function addSecurityLabel(?FHIRContractTermSecurityLabel ...$value): self
    {
        $this->securityLabel = array_filter(array_merge($this->securityLabel, $value));

        return $this;
    }

    public function getOffer(): ?FHIRContractTermOffer
    {
        return $this->offer;
    }

    public function setOffer(?FHIRContractTermOffer $value): self
    {
        $this->offer = $value;

        return $this;
    }

    /**
     * @return FHIRContractTermAsset[]
     */
    public function getAsset(): array
    {
        return $this->asset;
    }

    public function setAsset(?FHIRContractTermAsset ...$value): self
    {
        $this->asset = array_filter($value);

        return $this;
    }

    public function addAsset(?FHIRContractTermAsset ...$value): self
    {
        $this->asset = array_filter(array_merge($this->asset, $value));

        return $this;
    }

    /**
     * @return FHIRContractTermAction[]
     */
    public function getAction(): array
    {
        return $this->action;
    }

    public function setAction(?FHIRContractTermAction ...$value): self
    {
        $this->action = array_filter($value);

        return $this;
    }

    public function addAction(?FHIRContractTermAction ...$value): self
    {
        $this->action = array_filter(array_merge($this->action, $value));

        return $this;
    }

    /**
     * @return FHIRContractTerm[]
     */
    public function getGroup(): array
    {
        return $this->group;
    }

    public function setGroup(?FHIRContractTerm ...$value): self
    {
        $this->group = array_filter($value);

        return $this;
    }

    public function addGroup(?FHIRContractTerm ...$value): self
    {
        $this->group = array_filter(array_merge($this->group, $value));

        return $this;
    }
}
