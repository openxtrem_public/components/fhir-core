<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CitationCitedArtifactClassificationWhoClassified Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCitationCitedArtifactClassificationWhoClassifiedInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRCitationCitedArtifactClassificationWhoClassified extends FHIRBackboneElement implements FHIRCitationCitedArtifactClassificationWhoClassifiedInterface
{
    public const RESOURCE_NAME = 'Citation.citedArtifact.classification.whoClassified';

    protected ?FHIRReference $person = null;
    protected ?FHIRReference $organization = null;
    protected ?FHIRReference $publisher = null;
    protected ?FHIRString $classifierCopyright = null;
    protected ?FHIRBoolean $freeToShare = null;

    public function getPerson(): ?FHIRReference
    {
        return $this->person;
    }

    public function setPerson(?FHIRReference $value): self
    {
        $this->person = $value;

        return $this;
    }

    public function getOrganization(): ?FHIRReference
    {
        return $this->organization;
    }

    public function setOrganization(?FHIRReference $value): self
    {
        $this->organization = $value;

        return $this;
    }

    public function getPublisher(): ?FHIRReference
    {
        return $this->publisher;
    }

    public function setPublisher(?FHIRReference $value): self
    {
        $this->publisher = $value;

        return $this;
    }

    public function getClassifierCopyright(): ?FHIRString
    {
        return $this->classifierCopyright;
    }

    public function setClassifierCopyright(string|FHIRString|null $value): self
    {
        $this->classifierCopyright = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getFreeToShare(): ?FHIRBoolean
    {
        return $this->freeToShare;
    }

    public function setFreeToShare(bool|FHIRBoolean|null $value): self
    {
        $this->freeToShare = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }
}
