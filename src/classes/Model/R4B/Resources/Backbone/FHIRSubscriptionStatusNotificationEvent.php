<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubscriptionStatusNotificationEvent Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubscriptionStatusNotificationEventInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRSubscriptionStatusNotificationEvent extends FHIRBackboneElement implements FHIRSubscriptionStatusNotificationEventInterface
{
    public const RESOURCE_NAME = 'SubscriptionStatus.notificationEvent';

    protected ?FHIRString $eventNumber = null;
    protected ?FHIRInstant $timestamp = null;
    protected ?FHIRReference $focus = null;

    /** @var FHIRReference[] */
    protected array $additionalContext = [];

    public function getEventNumber(): ?FHIRString
    {
        return $this->eventNumber;
    }

    public function setEventNumber(string|FHIRString|null $value): self
    {
        $this->eventNumber = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTimestamp(): ?FHIRInstant
    {
        return $this->timestamp;
    }

    public function setTimestamp(string|FHIRInstant|null $value): self
    {
        $this->timestamp = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    public function getFocus(): ?FHIRReference
    {
        return $this->focus;
    }

    public function setFocus(?FHIRReference $value): self
    {
        $this->focus = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAdditionalContext(): array
    {
        return $this->additionalContext;
    }

    public function setAdditionalContext(?FHIRReference ...$value): self
    {
        $this->additionalContext = array_filter($value);

        return $this;
    }

    public function addAdditionalContext(?FHIRReference ...$value): self
    {
        $this->additionalContext = array_filter(array_merge($this->additionalContext, $value));

        return $this;
    }
}
