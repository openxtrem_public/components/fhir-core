<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TerminologyCapabilitiesCodeSystemVersionFilter Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTerminologyCapabilitiesCodeSystemVersionFilterInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;

class FHIRTerminologyCapabilitiesCodeSystemVersionFilter extends FHIRBackboneElement implements FHIRTerminologyCapabilitiesCodeSystemVersionFilterInterface
{
    public const RESOURCE_NAME = 'TerminologyCapabilities.codeSystem.version.filter';

    protected ?FHIRCode $code = null;

    /** @var FHIRCode[] */
    protected array $op = [];

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getOp(): array
    {
        return $this->op;
    }

    public function setOp(string|FHIRCode|null ...$value): self
    {
        $this->op = [];
        $this->addOp(...$value);

        return $this;
    }

    public function addOp(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->op = array_filter(array_merge($this->op, $values));

        return $this;
    }
}
