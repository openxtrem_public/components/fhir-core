<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestScriptTestAction Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestScriptTestActionInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;

class FHIRTestScriptTestAction extends FHIRBackboneElement implements FHIRTestScriptTestActionInterface
{
    public const RESOURCE_NAME = 'TestScript.test.action';

    protected ?FHIRTestScriptSetupActionOperation $operation = null;
    protected ?FHIRTestScriptSetupActionAssert $assert = null;

    public function getOperation(): ?FHIRTestScriptSetupActionOperation
    {
        return $this->operation;
    }

    public function setOperation(?FHIRTestScriptSetupActionOperation $value): self
    {
        $this->operation = $value;

        return $this;
    }

    public function getAssert(): ?FHIRTestScriptSetupActionAssert
    {
        return $this->assert;
    }

    public function setAssert(?FHIRTestScriptSetupActionAssert $value): self
    {
        $this->assert = $value;

        return $this;
    }
}
