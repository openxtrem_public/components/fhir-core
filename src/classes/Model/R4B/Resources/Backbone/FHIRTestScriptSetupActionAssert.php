<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestScriptSetupActionAssert Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestScriptSetupActionAssertInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRTestScriptSetupActionAssert extends FHIRBackboneElement implements FHIRTestScriptSetupActionAssertInterface
{
    public const RESOURCE_NAME = 'TestScript.setup.action.assert';

    protected ?FHIRString $label = null;
    protected ?FHIRString $description = null;
    protected ?FHIRCode $direction = null;
    protected ?FHIRString $compareToSourceId = null;
    protected ?FHIRString $compareToSourceExpression = null;
    protected ?FHIRString $compareToSourcePath = null;
    protected ?FHIRCode $contentType = null;
    protected ?FHIRString $expression = null;
    protected ?FHIRString $headerField = null;
    protected ?FHIRString $minimumId = null;
    protected ?FHIRBoolean $navigationLinks = null;
    protected ?FHIRCode $operator = null;
    protected ?FHIRString $path = null;
    protected ?FHIRCode $requestMethod = null;
    protected ?FHIRString $requestURL = null;
    protected ?FHIRCode $resource = null;
    protected ?FHIRCode $response = null;
    protected ?FHIRString $responseCode = null;
    protected ?FHIRId $sourceId = null;
    protected ?FHIRId $validateProfileId = null;
    protected ?FHIRString $value = null;
    protected ?FHIRBoolean $warningOnly = null;

    public function getLabel(): ?FHIRString
    {
        return $this->label;
    }

    public function setLabel(string|FHIRString|null $value): self
    {
        $this->label = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDirection(): ?FHIRCode
    {
        return $this->direction;
    }

    public function setDirection(string|FHIRCode|null $value): self
    {
        $this->direction = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCompareToSourceId(): ?FHIRString
    {
        return $this->compareToSourceId;
    }

    public function setCompareToSourceId(string|FHIRString|null $value): self
    {
        $this->compareToSourceId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCompareToSourceExpression(): ?FHIRString
    {
        return $this->compareToSourceExpression;
    }

    public function setCompareToSourceExpression(string|FHIRString|null $value): self
    {
        $this->compareToSourceExpression = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCompareToSourcePath(): ?FHIRString
    {
        return $this->compareToSourcePath;
    }

    public function setCompareToSourcePath(string|FHIRString|null $value): self
    {
        $this->compareToSourcePath = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getContentType(): ?FHIRCode
    {
        return $this->contentType;
    }

    public function setContentType(string|FHIRCode|null $value): self
    {
        $this->contentType = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getExpression(): ?FHIRString
    {
        return $this->expression;
    }

    public function setExpression(string|FHIRString|null $value): self
    {
        $this->expression = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getHeaderField(): ?FHIRString
    {
        return $this->headerField;
    }

    public function setHeaderField(string|FHIRString|null $value): self
    {
        $this->headerField = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getMinimumId(): ?FHIRString
    {
        return $this->minimumId;
    }

    public function setMinimumId(string|FHIRString|null $value): self
    {
        $this->minimumId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getNavigationLinks(): ?FHIRBoolean
    {
        return $this->navigationLinks;
    }

    public function setNavigationLinks(bool|FHIRBoolean|null $value): self
    {
        $this->navigationLinks = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getOperator(): ?FHIRCode
    {
        return $this->operator;
    }

    public function setOperator(string|FHIRCode|null $value): self
    {
        $this->operator = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getPath(): ?FHIRString
    {
        return $this->path;
    }

    public function setPath(string|FHIRString|null $value): self
    {
        $this->path = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getRequestMethod(): ?FHIRCode
    {
        return $this->requestMethod;
    }

    public function setRequestMethod(string|FHIRCode|null $value): self
    {
        $this->requestMethod = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getRequestURL(): ?FHIRString
    {
        return $this->requestURL;
    }

    public function setRequestURL(string|FHIRString|null $value): self
    {
        $this->requestURL = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getResource(): ?FHIRCode
    {
        return $this->resource;
    }

    public function setResource(string|FHIRCode|null $value): self
    {
        $this->resource = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getResponse(): ?FHIRCode
    {
        return $this->response;
    }

    public function setResponse(string|FHIRCode|null $value): self
    {
        $this->response = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getResponseCode(): ?FHIRString
    {
        return $this->responseCode;
    }

    public function setResponseCode(string|FHIRString|null $value): self
    {
        $this->responseCode = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSourceId(): ?FHIRId
    {
        return $this->sourceId;
    }

    public function setSourceId(string|FHIRId|null $value): self
    {
        $this->sourceId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getValidateProfileId(): ?FHIRId
    {
        return $this->validateProfileId;
    }

    public function setValidateProfileId(string|FHIRId|null $value): self
    {
        $this->validateProfileId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getValue(): ?FHIRString
    {
        return $this->value;
    }

    public function setValue(string|FHIRString|null $value): self
    {
        $this->value = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getWarningOnly(): ?FHIRBoolean
    {
        return $this->warningOnly;
    }

    public function setWarningOnly(bool|FHIRBoolean|null $value): self
    {
        $this->warningOnly = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }
}
