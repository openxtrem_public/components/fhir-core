<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TerminologyCapabilitiesCodeSystem Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTerminologyCapabilitiesCodeSystemInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCanonical;

class FHIRTerminologyCapabilitiesCodeSystem extends FHIRBackboneElement implements FHIRTerminologyCapabilitiesCodeSystemInterface
{
    public const RESOURCE_NAME = 'TerminologyCapabilities.codeSystem';

    protected ?FHIRCanonical $uri = null;

    /** @var FHIRTerminologyCapabilitiesCodeSystemVersion[] */
    protected array $version = [];
    protected ?FHIRBoolean $subsumption = null;

    public function getUri(): ?FHIRCanonical
    {
        return $this->uri;
    }

    public function setUri(string|FHIRCanonical|null $value): self
    {
        $this->uri = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRTerminologyCapabilitiesCodeSystemVersion[]
     */
    public function getVersion(): array
    {
        return $this->version;
    }

    public function setVersion(?FHIRTerminologyCapabilitiesCodeSystemVersion ...$value): self
    {
        $this->version = array_filter($value);

        return $this;
    }

    public function addVersion(?FHIRTerminologyCapabilitiesCodeSystemVersion ...$value): self
    {
        $this->version = array_filter(array_merge($this->version, $value));

        return $this;
    }

    public function getSubsumption(): ?FHIRBoolean
    {
        return $this->subsumption;
    }

    public function setSubsumption(bool|FHIRBoolean|null $value): self
    {
        $this->subsumption = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }
}
