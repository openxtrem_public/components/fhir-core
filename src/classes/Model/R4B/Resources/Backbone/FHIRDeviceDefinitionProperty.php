<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceDefinitionProperty Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceDefinitionPropertyInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRQuantity;

class FHIRDeviceDefinitionProperty extends FHIRBackboneElement implements FHIRDeviceDefinitionPropertyInterface
{
    public const RESOURCE_NAME = 'DeviceDefinition.property';

    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRQuantity[] */
    protected array $valueQuantity = [];

    /** @var FHIRCodeableConcept[] */
    protected array $valueCode = [];

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRQuantity[]
     */
    public function getValueQuantity(): array
    {
        return $this->valueQuantity;
    }

    public function setValueQuantity(?FHIRQuantity ...$value): self
    {
        $this->valueQuantity = array_filter($value);

        return $this;
    }

    public function addValueQuantity(?FHIRQuantity ...$value): self
    {
        $this->valueQuantity = array_filter(array_merge($this->valueQuantity, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getValueCode(): array
    {
        return $this->valueCode;
    }

    public function setValueCode(?FHIRCodeableConcept ...$value): self
    {
        $this->valueCode = array_filter($value);

        return $this;
    }

    public function addValueCode(?FHIRCodeableConcept ...$value): self
    {
        $this->valueCode = array_filter(array_merge($this->valueCode, $value));

        return $this;
    }
}
