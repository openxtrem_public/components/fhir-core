<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledgeIngredient Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationKnowledgeIngredientInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRRatio;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;

class FHIRMedicationKnowledgeIngredient extends FHIRBackboneElement implements FHIRMedicationKnowledgeIngredientInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge.ingredient';

    protected FHIRCodeableConcept|FHIRReference|null $item = null;
    protected ?FHIRBoolean $isActive = null;
    protected ?FHIRRatio $strength = null;

    public function getItem(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->item;
    }

    public function setItem(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->item = $value;

        return $this;
    }

    public function getIsActive(): ?FHIRBoolean
    {
        return $this->isActive;
    }

    public function setIsActive(bool|FHIRBoolean|null $value): self
    {
        $this->isActive = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getStrength(): ?FHIRRatio
    {
        return $this->strength;
    }

    public function setStrength(?FHIRRatio $value): self
    {
        $this->strength = $value;

        return $this;
    }
}
