<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ConsentVerification Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRConsentVerificationInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;

class FHIRConsentVerification extends FHIRBackboneElement implements FHIRConsentVerificationInterface
{
    public const RESOURCE_NAME = 'Consent.verification';

    protected ?FHIRBoolean $verified = null;
    protected ?FHIRReference $verifiedWith = null;
    protected ?FHIRDateTime $verificationDate = null;

    public function getVerified(): ?FHIRBoolean
    {
        return $this->verified;
    }

    public function setVerified(bool|FHIRBoolean|null $value): self
    {
        $this->verified = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getVerifiedWith(): ?FHIRReference
    {
        return $this->verifiedWith;
    }

    public function setVerifiedWith(?FHIRReference $value): self
    {
        $this->verifiedWith = $value;

        return $this;
    }

    public function getVerificationDate(): ?FHIRDateTime
    {
        return $this->verificationDate;
    }

    public function setVerificationDate(string|FHIRDateTime|null $value): self
    {
        $this->verificationDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }
}
