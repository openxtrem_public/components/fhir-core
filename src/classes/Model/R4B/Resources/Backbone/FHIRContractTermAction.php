<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ContractTermAction Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRContractTermActionInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUnsignedInt;

class FHIRContractTermAction extends FHIRBackboneElement implements FHIRContractTermActionInterface
{
    public const RESOURCE_NAME = 'Contract.term.action';

    protected ?FHIRBoolean $doNotPerform = null;
    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRContractTermActionSubject[] */
    protected array $subject = [];
    protected ?FHIRCodeableConcept $intent = null;

    /** @var FHIRString[] */
    protected array $linkId = [];
    protected ?FHIRCodeableConcept $status = null;
    protected ?FHIRReference $context = null;

    /** @var FHIRString[] */
    protected array $contextLinkId = [];
    protected FHIRDateTime|FHIRPeriod|FHIRTiming|null $occurrence = null;

    /** @var FHIRReference[] */
    protected array $requester = [];

    /** @var FHIRString[] */
    protected array $requesterLinkId = [];

    /** @var FHIRCodeableConcept[] */
    protected array $performerType = [];
    protected ?FHIRCodeableConcept $performerRole = null;
    protected ?FHIRReference $performer = null;

    /** @var FHIRString[] */
    protected array $performerLinkId = [];

    /** @var FHIRCodeableConcept[] */
    protected array $reasonCode = [];

    /** @var FHIRReference[] */
    protected array $reasonReference = [];

    /** @var FHIRString[] */
    protected array $reason = [];

    /** @var FHIRString[] */
    protected array $reasonLinkId = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRUnsignedInt[] */
    protected array $securityLabelNumber = [];

    public function getDoNotPerform(): ?FHIRBoolean
    {
        return $this->doNotPerform;
    }

    public function setDoNotPerform(bool|FHIRBoolean|null $value): self
    {
        $this->doNotPerform = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRContractTermActionSubject[]
     */
    public function getSubject(): array
    {
        return $this->subject;
    }

    public function setSubject(?FHIRContractTermActionSubject ...$value): self
    {
        $this->subject = array_filter($value);

        return $this;
    }

    public function addSubject(?FHIRContractTermActionSubject ...$value): self
    {
        $this->subject = array_filter(array_merge($this->subject, $value));

        return $this;
    }

    public function getIntent(): ?FHIRCodeableConcept
    {
        return $this->intent;
    }

    public function setIntent(?FHIRCodeableConcept $value): self
    {
        $this->intent = $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getLinkId(): array
    {
        return $this->linkId;
    }

    public function setLinkId(string|FHIRString|null ...$value): self
    {
        $this->linkId = [];
        $this->addLinkId(...$value);

        return $this;
    }

    public function addLinkId(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->linkId = array_filter(array_merge($this->linkId, $values));

        return $this;
    }

    public function getStatus(): ?FHIRCodeableConcept
    {
        return $this->status;
    }

    public function setStatus(?FHIRCodeableConcept $value): self
    {
        $this->status = $value;

        return $this;
    }

    public function getContext(): ?FHIRReference
    {
        return $this->context;
    }

    public function setContext(?FHIRReference $value): self
    {
        $this->context = $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getContextLinkId(): array
    {
        return $this->contextLinkId;
    }

    public function setContextLinkId(string|FHIRString|null ...$value): self
    {
        $this->contextLinkId = [];
        $this->addContextLinkId(...$value);

        return $this;
    }

    public function addContextLinkId(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->contextLinkId = array_filter(array_merge($this->contextLinkId, $values));

        return $this;
    }

    public function getOccurrence(): FHIRDateTime|FHIRPeriod|FHIRTiming|null
    {
        return $this->occurrence;
    }

    public function setOccurrence(FHIRDateTime|FHIRPeriod|FHIRTiming|null $value): self
    {
        $this->occurrence = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getRequester(): array
    {
        return $this->requester;
    }

    public function setRequester(?FHIRReference ...$value): self
    {
        $this->requester = array_filter($value);

        return $this;
    }

    public function addRequester(?FHIRReference ...$value): self
    {
        $this->requester = array_filter(array_merge($this->requester, $value));

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getRequesterLinkId(): array
    {
        return $this->requesterLinkId;
    }

    public function setRequesterLinkId(string|FHIRString|null ...$value): self
    {
        $this->requesterLinkId = [];
        $this->addRequesterLinkId(...$value);

        return $this;
    }

    public function addRequesterLinkId(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->requesterLinkId = array_filter(array_merge($this->requesterLinkId, $values));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getPerformerType(): array
    {
        return $this->performerType;
    }

    public function setPerformerType(?FHIRCodeableConcept ...$value): self
    {
        $this->performerType = array_filter($value);

        return $this;
    }

    public function addPerformerType(?FHIRCodeableConcept ...$value): self
    {
        $this->performerType = array_filter(array_merge($this->performerType, $value));

        return $this;
    }

    public function getPerformerRole(): ?FHIRCodeableConcept
    {
        return $this->performerRole;
    }

    public function setPerformerRole(?FHIRCodeableConcept $value): self
    {
        $this->performerRole = $value;

        return $this;
    }

    public function getPerformer(): ?FHIRReference
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRReference $value): self
    {
        $this->performer = $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getPerformerLinkId(): array
    {
        return $this->performerLinkId;
    }

    public function setPerformerLinkId(string|FHIRString|null ...$value): self
    {
        $this->performerLinkId = [];
        $this->addPerformerLinkId(...$value);

        return $this;
    }

    public function addPerformerLinkId(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->performerLinkId = array_filter(array_merge($this->performerLinkId, $values));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReasonCode(): array
    {
        return $this->reasonCode;
    }

    public function setReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter($value);

        return $this;
    }

    public function addReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter(array_merge($this->reasonCode, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReasonReference(): array
    {
        return $this->reasonReference;
    }

    public function setReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter($value);

        return $this;
    }

    public function addReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter(array_merge($this->reasonReference, $value));

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getReason(): array
    {
        return $this->reason;
    }

    public function setReason(string|FHIRString|null ...$value): self
    {
        $this->reason = [];
        $this->addReason(...$value);

        return $this;
    }

    public function addReason(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->reason = array_filter(array_merge($this->reason, $values));

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getReasonLinkId(): array
    {
        return $this->reasonLinkId;
    }

    public function setReasonLinkId(string|FHIRString|null ...$value): self
    {
        $this->reasonLinkId = [];
        $this->addReasonLinkId(...$value);

        return $this;
    }

    public function addReasonLinkId(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->reasonLinkId = array_filter(array_merge($this->reasonLinkId, $values));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRUnsignedInt[]
     */
    public function getSecurityLabelNumber(): array
    {
        return $this->securityLabelNumber;
    }

    public function setSecurityLabelNumber(int|FHIRUnsignedInt|null ...$value): self
    {
        $this->securityLabelNumber = [];
        $this->addSecurityLabelNumber(...$value);

        return $this;
    }

    public function addSecurityLabelNumber(int|FHIRUnsignedInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRUnsignedInt())->setValue($v) : $v, $value);

        $this->securityLabelNumber = array_filter(array_merge($this->securityLabelNumber, $values));

        return $this;
    }
}
