<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceDefinitionStructure Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceDefinitionStructureInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRSubstanceDefinitionStructure extends FHIRBackboneElement implements FHIRSubstanceDefinitionStructureInterface
{
    public const RESOURCE_NAME = 'SubstanceDefinition.structure';

    protected ?FHIRCodeableConcept $stereochemistry = null;
    protected ?FHIRCodeableConcept $opticalActivity = null;
    protected ?FHIRString $molecularFormula = null;
    protected ?FHIRString $molecularFormulaByMoiety = null;
    protected ?FHIRSubstanceDefinitionMolecularWeight $molecularWeight = null;

    /** @var FHIRCodeableConcept[] */
    protected array $technique = [];

    /** @var FHIRReference[] */
    protected array $sourceDocument = [];

    /** @var FHIRSubstanceDefinitionStructureRepresentation[] */
    protected array $representation = [];

    public function getStereochemistry(): ?FHIRCodeableConcept
    {
        return $this->stereochemistry;
    }

    public function setStereochemistry(?FHIRCodeableConcept $value): self
    {
        $this->stereochemistry = $value;

        return $this;
    }

    public function getOpticalActivity(): ?FHIRCodeableConcept
    {
        return $this->opticalActivity;
    }

    public function setOpticalActivity(?FHIRCodeableConcept $value): self
    {
        $this->opticalActivity = $value;

        return $this;
    }

    public function getMolecularFormula(): ?FHIRString
    {
        return $this->molecularFormula;
    }

    public function setMolecularFormula(string|FHIRString|null $value): self
    {
        $this->molecularFormula = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getMolecularFormulaByMoiety(): ?FHIRString
    {
        return $this->molecularFormulaByMoiety;
    }

    public function setMolecularFormulaByMoiety(string|FHIRString|null $value): self
    {
        $this->molecularFormulaByMoiety = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getMolecularWeight(): ?FHIRSubstanceDefinitionMolecularWeight
    {
        return $this->molecularWeight;
    }

    public function setMolecularWeight(?FHIRSubstanceDefinitionMolecularWeight $value): self
    {
        $this->molecularWeight = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getTechnique(): array
    {
        return $this->technique;
    }

    public function setTechnique(?FHIRCodeableConcept ...$value): self
    {
        $this->technique = array_filter($value);

        return $this;
    }

    public function addTechnique(?FHIRCodeableConcept ...$value): self
    {
        $this->technique = array_filter(array_merge($this->technique, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSourceDocument(): array
    {
        return $this->sourceDocument;
    }

    public function setSourceDocument(?FHIRReference ...$value): self
    {
        $this->sourceDocument = array_filter($value);

        return $this;
    }

    public function addSourceDocument(?FHIRReference ...$value): self
    {
        $this->sourceDocument = array_filter(array_merge($this->sourceDocument, $value));

        return $this;
    }

    /**
     * @return FHIRSubstanceDefinitionStructureRepresentation[]
     */
    public function getRepresentation(): array
    {
        return $this->representation;
    }

    public function setRepresentation(?FHIRSubstanceDefinitionStructureRepresentation ...$value): self
    {
        $this->representation = array_filter($value);

        return $this;
    }

    public function addRepresentation(?FHIRSubstanceDefinitionStructureRepresentation ...$value): self
    {
        $this->representation = array_filter(array_merge($this->representation, $value));

        return $this;
    }
}
