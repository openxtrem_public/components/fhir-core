<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CareTeamParticipant Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCareTeamParticipantInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;

class FHIRCareTeamParticipant extends FHIRBackboneElement implements FHIRCareTeamParticipantInterface
{
    public const RESOURCE_NAME = 'CareTeam.participant';

    /** @var FHIRCodeableConcept[] */
    protected array $role = [];
    protected ?FHIRReference $member = null;
    protected ?FHIRReference $onBehalfOf = null;
    protected ?FHIRPeriod $period = null;

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getRole(): array
    {
        return $this->role;
    }

    public function setRole(?FHIRCodeableConcept ...$value): self
    {
        $this->role = array_filter($value);

        return $this;
    }

    public function addRole(?FHIRCodeableConcept ...$value): self
    {
        $this->role = array_filter(array_merge($this->role, $value));

        return $this;
    }

    public function getMember(): ?FHIRReference
    {
        return $this->member;
    }

    public function setMember(?FHIRReference $value): self
    {
        $this->member = $value;

        return $this;
    }

    public function getOnBehalfOf(): ?FHIRReference
    {
        return $this->onBehalfOf;
    }

    public function setOnBehalfOf(?FHIRReference $value): self
    {
        $this->onBehalfOf = $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }
}
