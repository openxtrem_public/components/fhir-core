<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PackagedProductDefinitionPackage Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRPackagedProductDefinitionPackageInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRInteger;

class FHIRPackagedProductDefinitionPackage extends FHIRBackboneElement implements FHIRPackagedProductDefinitionPackageInterface
{
    public const RESOURCE_NAME = 'PackagedProductDefinition.package';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRInteger $quantity = null;

    /** @var FHIRCodeableConcept[] */
    protected array $material = [];

    /** @var FHIRCodeableConcept[] */
    protected array $alternateMaterial = [];

    /** @var FHIRPackagedProductDefinitionPackageShelfLifeStorage[] */
    protected array $shelfLifeStorage = [];

    /** @var FHIRReference[] */
    protected array $manufacturer = [];

    /** @var FHIRPackagedProductDefinitionPackageProperty[] */
    protected array $property = [];

    /** @var FHIRPackagedProductDefinitionPackageContainedItem[] */
    protected array $containedItem = [];

    /** @var FHIRPackagedProductDefinitionPackage[] */
    protected array $package = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRInteger
    {
        return $this->quantity;
    }

    public function setQuantity(int|FHIRInteger|null $value): self
    {
        $this->quantity = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getMaterial(): array
    {
        return $this->material;
    }

    public function setMaterial(?FHIRCodeableConcept ...$value): self
    {
        $this->material = array_filter($value);

        return $this;
    }

    public function addMaterial(?FHIRCodeableConcept ...$value): self
    {
        $this->material = array_filter(array_merge($this->material, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getAlternateMaterial(): array
    {
        return $this->alternateMaterial;
    }

    public function setAlternateMaterial(?FHIRCodeableConcept ...$value): self
    {
        $this->alternateMaterial = array_filter($value);

        return $this;
    }

    public function addAlternateMaterial(?FHIRCodeableConcept ...$value): self
    {
        $this->alternateMaterial = array_filter(array_merge($this->alternateMaterial, $value));

        return $this;
    }

    /**
     * @return FHIRPackagedProductDefinitionPackageShelfLifeStorage[]
     */
    public function getShelfLifeStorage(): array
    {
        return $this->shelfLifeStorage;
    }

    public function setShelfLifeStorage(?FHIRPackagedProductDefinitionPackageShelfLifeStorage ...$value): self
    {
        $this->shelfLifeStorage = array_filter($value);

        return $this;
    }

    public function addShelfLifeStorage(?FHIRPackagedProductDefinitionPackageShelfLifeStorage ...$value): self
    {
        $this->shelfLifeStorage = array_filter(array_merge($this->shelfLifeStorage, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getManufacturer(): array
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?FHIRReference ...$value): self
    {
        $this->manufacturer = array_filter($value);

        return $this;
    }

    public function addManufacturer(?FHIRReference ...$value): self
    {
        $this->manufacturer = array_filter(array_merge($this->manufacturer, $value));

        return $this;
    }

    /**
     * @return FHIRPackagedProductDefinitionPackageProperty[]
     */
    public function getProperty(): array
    {
        return $this->property;
    }

    public function setProperty(?FHIRPackagedProductDefinitionPackageProperty ...$value): self
    {
        $this->property = array_filter($value);

        return $this;
    }

    public function addProperty(?FHIRPackagedProductDefinitionPackageProperty ...$value): self
    {
        $this->property = array_filter(array_merge($this->property, $value));

        return $this;
    }

    /**
     * @return FHIRPackagedProductDefinitionPackageContainedItem[]
     */
    public function getContainedItem(): array
    {
        return $this->containedItem;
    }

    public function setContainedItem(?FHIRPackagedProductDefinitionPackageContainedItem ...$value): self
    {
        $this->containedItem = array_filter($value);

        return $this;
    }

    public function addContainedItem(?FHIRPackagedProductDefinitionPackageContainedItem ...$value): self
    {
        $this->containedItem = array_filter(array_merge($this->containedItem, $value));

        return $this;
    }

    /**
     * @return FHIRPackagedProductDefinitionPackage[]
     */
    public function getPackage(): array
    {
        return $this->package;
    }

    public function setPackage(?FHIRPackagedProductDefinitionPackage ...$value): self
    {
        $this->package = array_filter($value);

        return $this;
    }

    public function addPackage(?FHIRPackagedProductDefinitionPackage ...$value): self
    {
        $this->package = array_filter(array_merge($this->package, $value));

        return $this;
    }
}
