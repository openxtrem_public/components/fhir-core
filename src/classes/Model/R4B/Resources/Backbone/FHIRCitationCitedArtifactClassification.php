<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CitationCitedArtifactClassification Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCitationCitedArtifactClassificationInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;

class FHIRCitationCitedArtifactClassification extends FHIRBackboneElement implements FHIRCitationCitedArtifactClassificationInterface
{
    public const RESOURCE_NAME = 'Citation.citedArtifact.classification';

    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRCodeableConcept[] */
    protected array $classifier = [];
    protected ?FHIRCitationCitedArtifactClassificationWhoClassified $whoClassified = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getClassifier(): array
    {
        return $this->classifier;
    }

    public function setClassifier(?FHIRCodeableConcept ...$value): self
    {
        $this->classifier = array_filter($value);

        return $this;
    }

    public function addClassifier(?FHIRCodeableConcept ...$value): self
    {
        $this->classifier = array_filter(array_merge($this->classifier, $value));

        return $this;
    }

    public function getWhoClassified(): ?FHIRCitationCitedArtifactClassificationWhoClassified
    {
        return $this->whoClassified;
    }

    public function setWhoClassified(?FHIRCitationCitedArtifactClassificationWhoClassified $value): self
    {
        $this->whoClassified = $value;

        return $this;
    }
}
