<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR BiologicallyDerivedProductStorage Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRBiologicallyDerivedProductStorageInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRBiologicallyDerivedProductStorage extends FHIRBackboneElement implements FHIRBiologicallyDerivedProductStorageInterface
{
    public const RESOURCE_NAME = 'BiologicallyDerivedProduct.storage';

    protected ?FHIRString $description = null;
    protected ?FHIRDecimal $temperature = null;
    protected ?FHIRCode $scale = null;
    protected ?FHIRPeriod $duration = null;

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTemperature(): ?FHIRDecimal
    {
        return $this->temperature;
    }

    public function setTemperature(float|FHIRDecimal|null $value): self
    {
        $this->temperature = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getScale(): ?FHIRCode
    {
        return $this->scale;
    }

    public function setScale(string|FHIRCode|null $value): self
    {
        $this->scale = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDuration(): ?FHIRPeriod
    {
        return $this->duration;
    }

    public function setDuration(?FHIRPeriod $value): self
    {
        $this->duration = $value;

        return $this;
    }
}
