<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceDefinitionRelationship Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceDefinitionRelationshipInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRRatio;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRSubstanceDefinitionRelationship extends FHIRBackboneElement implements FHIRSubstanceDefinitionRelationshipInterface
{
    public const RESOURCE_NAME = 'SubstanceDefinition.relationship';

    protected FHIRReference|FHIRCodeableConcept|null $substanceDefinition = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRBoolean $isDefining = null;
    protected FHIRQuantity|FHIRRatio|FHIRString|null $amount = null;
    protected ?FHIRRatio $ratioHighLimitAmount = null;
    protected ?FHIRCodeableConcept $comparator = null;

    /** @var FHIRReference[] */
    protected array $source = [];

    public function getSubstanceDefinition(): FHIRReference|FHIRCodeableConcept|null
    {
        return $this->substanceDefinition;
    }

    public function setSubstanceDefinition(FHIRReference|FHIRCodeableConcept|null $value): self
    {
        $this->substanceDefinition = $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getIsDefining(): ?FHIRBoolean
    {
        return $this->isDefining;
    }

    public function setIsDefining(bool|FHIRBoolean|null $value): self
    {
        $this->isDefining = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getAmount(): FHIRQuantity|FHIRRatio|FHIRString|null
    {
        return $this->amount;
    }

    public function setAmount(FHIRQuantity|FHIRRatio|FHIRString|null $value): self
    {
        $this->amount = $value;

        return $this;
    }

    public function getRatioHighLimitAmount(): ?FHIRRatio
    {
        return $this->ratioHighLimitAmount;
    }

    public function setRatioHighLimitAmount(?FHIRRatio $value): self
    {
        $this->ratioHighLimitAmount = $value;

        return $this;
    }

    public function getComparator(): ?FHIRCodeableConcept
    {
        return $this->comparator;
    }

    public function setComparator(?FHIRCodeableConcept $value): self
    {
        $this->comparator = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSource(): array
    {
        return $this->source;
    }

    public function setSource(?FHIRReference ...$value): self
    {
        $this->source = array_filter($value);

        return $this;
    }

    public function addSource(?FHIRReference ...$value): self
    {
        $this->source = array_filter(array_merge($this->source, $value));

        return $this;
    }
}
