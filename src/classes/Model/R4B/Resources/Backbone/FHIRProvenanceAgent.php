<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ProvenanceAgent Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRProvenanceAgentInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;

class FHIRProvenanceAgent extends FHIRBackboneElement implements FHIRProvenanceAgentInterface
{
    public const RESOURCE_NAME = 'Provenance.agent';

    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRCodeableConcept[] */
    protected array $role = [];
    protected ?FHIRReference $who = null;
    protected ?FHIRReference $onBehalfOf = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getRole(): array
    {
        return $this->role;
    }

    public function setRole(?FHIRCodeableConcept ...$value): self
    {
        $this->role = array_filter($value);

        return $this;
    }

    public function addRole(?FHIRCodeableConcept ...$value): self
    {
        $this->role = array_filter(array_merge($this->role, $value));

        return $this;
    }

    public function getWho(): ?FHIRReference
    {
        return $this->who;
    }

    public function setWho(?FHIRReference $value): self
    {
        $this->who = $value;

        return $this;
    }

    public function getOnBehalfOf(): ?FHIRReference
    {
        return $this->onBehalfOf;
    }

    public function setOnBehalfOf(?FHIRReference $value): self
    {
        $this->onBehalfOf = $value;

        return $this;
    }
}
