<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR IngredientSubstanceStrength Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRIngredientSubstanceStrengthInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRRatio;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRRatioRange;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRIngredientSubstanceStrength extends FHIRBackboneElement implements FHIRIngredientSubstanceStrengthInterface
{
    public const RESOURCE_NAME = 'Ingredient.substance.strength';

    protected FHIRRatio|FHIRRatioRange|null $presentation = null;
    protected ?FHIRString $textPresentation = null;
    protected FHIRRatio|FHIRRatioRange|null $concentration = null;
    protected ?FHIRString $textConcentration = null;
    protected ?FHIRString $measurementPoint = null;

    /** @var FHIRCodeableConcept[] */
    protected array $country = [];

    /** @var FHIRIngredientSubstanceStrengthReferenceStrength[] */
    protected array $referenceStrength = [];

    public function getPresentation(): FHIRRatio|FHIRRatioRange|null
    {
        return $this->presentation;
    }

    public function setPresentation(FHIRRatio|FHIRRatioRange|null $value): self
    {
        $this->presentation = $value;

        return $this;
    }

    public function getTextPresentation(): ?FHIRString
    {
        return $this->textPresentation;
    }

    public function setTextPresentation(string|FHIRString|null $value): self
    {
        $this->textPresentation = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getConcentration(): FHIRRatio|FHIRRatioRange|null
    {
        return $this->concentration;
    }

    public function setConcentration(FHIRRatio|FHIRRatioRange|null $value): self
    {
        $this->concentration = $value;

        return $this;
    }

    public function getTextConcentration(): ?FHIRString
    {
        return $this->textConcentration;
    }

    public function setTextConcentration(string|FHIRString|null $value): self
    {
        $this->textConcentration = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getMeasurementPoint(): ?FHIRString
    {
        return $this->measurementPoint;
    }

    public function setMeasurementPoint(string|FHIRString|null $value): self
    {
        $this->measurementPoint = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCountry(): array
    {
        return $this->country;
    }

    public function setCountry(?FHIRCodeableConcept ...$value): self
    {
        $this->country = array_filter($value);

        return $this;
    }

    public function addCountry(?FHIRCodeableConcept ...$value): self
    {
        $this->country = array_filter(array_merge($this->country, $value));

        return $this;
    }

    /**
     * @return FHIRIngredientSubstanceStrengthReferenceStrength[]
     */
    public function getReferenceStrength(): array
    {
        return $this->referenceStrength;
    }

    public function setReferenceStrength(?FHIRIngredientSubstanceStrengthReferenceStrength ...$value): self
    {
        $this->referenceStrength = array_filter($value);

        return $this;
    }

    public function addReferenceStrength(?FHIRIngredientSubstanceStrengthReferenceStrength ...$value): self
    {
        $this->referenceStrength = array_filter(array_merge($this->referenceStrength, $value));

        return $this;
    }
}
