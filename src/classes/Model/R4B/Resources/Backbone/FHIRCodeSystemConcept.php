<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CodeSystemConcept Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCodeSystemConceptInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRCodeSystemConcept extends FHIRBackboneElement implements FHIRCodeSystemConceptInterface
{
    public const RESOURCE_NAME = 'CodeSystem.concept';

    protected ?FHIRCode $code = null;
    protected ?FHIRString $display = null;
    protected ?FHIRString $definition = null;

    /** @var FHIRCodeSystemConceptDesignation[] */
    protected array $designation = [];

    /** @var FHIRCodeSystemConceptProperty[] */
    protected array $property = [];

    /** @var FHIRCodeSystemConcept[] */
    protected array $concept = [];

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDisplay(): ?FHIRString
    {
        return $this->display;
    }

    public function setDisplay(string|FHIRString|null $value): self
    {
        $this->display = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDefinition(): ?FHIRString
    {
        return $this->definition;
    }

    public function setDefinition(string|FHIRString|null $value): self
    {
        $this->definition = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeSystemConceptDesignation[]
     */
    public function getDesignation(): array
    {
        return $this->designation;
    }

    public function setDesignation(?FHIRCodeSystemConceptDesignation ...$value): self
    {
        $this->designation = array_filter($value);

        return $this;
    }

    public function addDesignation(?FHIRCodeSystemConceptDesignation ...$value): self
    {
        $this->designation = array_filter(array_merge($this->designation, $value));

        return $this;
    }

    /**
     * @return FHIRCodeSystemConceptProperty[]
     */
    public function getProperty(): array
    {
        return $this->property;
    }

    public function setProperty(?FHIRCodeSystemConceptProperty ...$value): self
    {
        $this->property = array_filter($value);

        return $this;
    }

    public function addProperty(?FHIRCodeSystemConceptProperty ...$value): self
    {
        $this->property = array_filter(array_merge($this->property, $value));

        return $this;
    }

    /**
     * @return FHIRCodeSystemConcept[]
     */
    public function getConcept(): array
    {
        return $this->concept;
    }

    public function setConcept(?FHIRCodeSystemConcept ...$value): self
    {
        $this->concept = array_filter($value);

        return $this;
    }

    public function addConcept(?FHIRCodeSystemConcept ...$value): self
    {
        $this->concept = array_filter(array_merge($this->concept, $value));

        return $this;
    }
}
