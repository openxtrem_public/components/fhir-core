<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ConsentProvision Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRConsentProvisionInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;

class FHIRConsentProvision extends FHIRBackboneElement implements FHIRConsentProvisionInterface
{
    public const RESOURCE_NAME = 'Consent.provision';

    protected ?FHIRCode $type = null;
    protected ?FHIRPeriod $period = null;

    /** @var FHIRConsentProvisionActor[] */
    protected array $actor = [];

    /** @var FHIRCodeableConcept[] */
    protected array $action = [];

    /** @var FHIRCoding[] */
    protected array $securityLabel = [];

    /** @var FHIRCoding[] */
    protected array $purpose = [];

    /** @var FHIRCoding[] */
    protected array $class = [];

    /** @var FHIRCodeableConcept[] */
    protected array $code = [];
    protected ?FHIRPeriod $dataPeriod = null;

    /** @var FHIRConsentProvisionData[] */
    protected array $data = [];

    /** @var FHIRConsentProvision[] */
    protected array $provision = [];

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    /**
     * @return FHIRConsentProvisionActor[]
     */
    public function getActor(): array
    {
        return $this->actor;
    }

    public function setActor(?FHIRConsentProvisionActor ...$value): self
    {
        $this->actor = array_filter($value);

        return $this;
    }

    public function addActor(?FHIRConsentProvisionActor ...$value): self
    {
        $this->actor = array_filter(array_merge($this->actor, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getAction(): array
    {
        return $this->action;
    }

    public function setAction(?FHIRCodeableConcept ...$value): self
    {
        $this->action = array_filter($value);

        return $this;
    }

    public function addAction(?FHIRCodeableConcept ...$value): self
    {
        $this->action = array_filter(array_merge($this->action, $value));

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getSecurityLabel(): array
    {
        return $this->securityLabel;
    }

    public function setSecurityLabel(?FHIRCoding ...$value): self
    {
        $this->securityLabel = array_filter($value);

        return $this;
    }

    public function addSecurityLabel(?FHIRCoding ...$value): self
    {
        $this->securityLabel = array_filter(array_merge($this->securityLabel, $value));

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getPurpose(): array
    {
        return $this->purpose;
    }

    public function setPurpose(?FHIRCoding ...$value): self
    {
        $this->purpose = array_filter($value);

        return $this;
    }

    public function addPurpose(?FHIRCoding ...$value): self
    {
        $this->purpose = array_filter(array_merge($this->purpose, $value));

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getClass(): array
    {
        return $this->class;
    }

    public function setClass(?FHIRCoding ...$value): self
    {
        $this->class = array_filter($value);

        return $this;
    }

    public function addClass(?FHIRCoding ...$value): self
    {
        $this->class = array_filter(array_merge($this->class, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCode(): array
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept ...$value): self
    {
        $this->code = array_filter($value);

        return $this;
    }

    public function addCode(?FHIRCodeableConcept ...$value): self
    {
        $this->code = array_filter(array_merge($this->code, $value));

        return $this;
    }

    public function getDataPeriod(): ?FHIRPeriod
    {
        return $this->dataPeriod;
    }

    public function setDataPeriod(?FHIRPeriod $value): self
    {
        $this->dataPeriod = $value;

        return $this;
    }

    /**
     * @return FHIRConsentProvisionData[]
     */
    public function getData(): array
    {
        return $this->data;
    }

    public function setData(?FHIRConsentProvisionData ...$value): self
    {
        $this->data = array_filter($value);

        return $this;
    }

    public function addData(?FHIRConsentProvisionData ...$value): self
    {
        $this->data = array_filter(array_merge($this->data, $value));

        return $this;
    }

    /**
     * @return FHIRConsentProvision[]
     */
    public function getProvision(): array
    {
        return $this->provision;
    }

    public function setProvision(?FHIRConsentProvision ...$value): self
    {
        $this->provision = array_filter($value);

        return $this;
    }

    public function addProvision(?FHIRConsentProvision ...$value): self
    {
        $this->provision = array_filter(array_merge($this->provision, $value));

        return $this;
    }
}
