<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImmunizationEducation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImmunizationEducationInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUri;

class FHIRImmunizationEducation extends FHIRBackboneElement implements FHIRImmunizationEducationInterface
{
    public const RESOURCE_NAME = 'Immunization.education';

    protected ?FHIRString $documentType = null;
    protected ?FHIRUri $reference = null;
    protected ?FHIRDateTime $publicationDate = null;
    protected ?FHIRDateTime $presentationDate = null;

    public function getDocumentType(): ?FHIRString
    {
        return $this->documentType;
    }

    public function setDocumentType(string|FHIRString|null $value): self
    {
        $this->documentType = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getReference(): ?FHIRUri
    {
        return $this->reference;
    }

    public function setReference(string|FHIRUri|null $value): self
    {
        $this->reference = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getPublicationDate(): ?FHIRDateTime
    {
        return $this->publicationDate;
    }

    public function setPublicationDate(string|FHIRDateTime|null $value): self
    {
        $this->publicationDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPresentationDate(): ?FHIRDateTime
    {
        return $this->presentationDate;
    }

    public function setPresentationDate(string|FHIRDateTime|null $value): self
    {
        $this->presentationDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }
}
