<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ValueSetComposeInclude Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRValueSetComposeIncludeInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUri;

class FHIRValueSetComposeInclude extends FHIRBackboneElement implements FHIRValueSetComposeIncludeInterface
{
    public const RESOURCE_NAME = 'ValueSet.compose.include';

    protected ?FHIRUri $system = null;
    protected ?FHIRString $version = null;

    /** @var FHIRValueSetComposeIncludeConcept[] */
    protected array $concept = [];

    /** @var FHIRValueSetComposeIncludeFilter[] */
    protected array $filter = [];

    /** @var FHIRCanonical[] */
    protected array $valueSet = [];

    public function getSystem(): ?FHIRUri
    {
        return $this->system;
    }

    public function setSystem(string|FHIRUri|null $value): self
    {
        $this->system = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRValueSetComposeIncludeConcept[]
     */
    public function getConcept(): array
    {
        return $this->concept;
    }

    public function setConcept(?FHIRValueSetComposeIncludeConcept ...$value): self
    {
        $this->concept = array_filter($value);

        return $this;
    }

    public function addConcept(?FHIRValueSetComposeIncludeConcept ...$value): self
    {
        $this->concept = array_filter(array_merge($this->concept, $value));

        return $this;
    }

    /**
     * @return FHIRValueSetComposeIncludeFilter[]
     */
    public function getFilter(): array
    {
        return $this->filter;
    }

    public function setFilter(?FHIRValueSetComposeIncludeFilter ...$value): self
    {
        $this->filter = array_filter($value);

        return $this;
    }

    public function addFilter(?FHIRValueSetComposeIncludeFilter ...$value): self
    {
        $this->filter = array_filter(array_merge($this->filter, $value));

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getValueSet(): array
    {
        return $this->valueSet;
    }

    public function setValueSet(string|FHIRCanonical|null ...$value): self
    {
        $this->valueSet = [];
        $this->addValueSet(...$value);

        return $this;
    }

    public function addValueSet(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->valueSet = array_filter(array_merge($this->valueSet, $values));

        return $this;
    }
}
