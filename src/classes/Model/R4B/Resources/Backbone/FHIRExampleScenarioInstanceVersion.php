<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExampleScenarioInstanceVersion Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExampleScenarioInstanceVersionInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRExampleScenarioInstanceVersion extends FHIRBackboneElement implements FHIRExampleScenarioInstanceVersionInterface
{
    public const RESOURCE_NAME = 'ExampleScenario.instance.version';

    protected ?FHIRString $versionId = null;
    protected ?FHIRMarkdown $description = null;

    public function getVersionId(): ?FHIRString
    {
        return $this->versionId;
    }

    public function setVersionId(string|FHIRString|null $value): self
    {
        $this->versionId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }
}
