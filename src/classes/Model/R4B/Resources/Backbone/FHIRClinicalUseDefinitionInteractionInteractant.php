<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClinicalUseDefinitionInteractionInteractant Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRClinicalUseDefinitionInteractionInteractantInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;

class FHIRClinicalUseDefinitionInteractionInteractant extends FHIRBackboneElement implements FHIRClinicalUseDefinitionInteractionInteractantInterface
{
    public const RESOURCE_NAME = 'ClinicalUseDefinition.interaction.interactant';

    protected FHIRReference|FHIRCodeableConcept|null $item = null;

    public function getItem(): FHIRReference|FHIRCodeableConcept|null
    {
        return $this->item;
    }

    public function setItem(FHIRReference|FHIRCodeableConcept|null $value): self
    {
        $this->item = $value;

        return $this;
    }
}
