<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR InsurancePlanContact Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRInsurancePlanContactInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRAddress;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRHumanName;

class FHIRInsurancePlanContact extends FHIRBackboneElement implements FHIRInsurancePlanContactInterface
{
    public const RESOURCE_NAME = 'InsurancePlan.contact';

    protected ?FHIRCodeableConcept $purpose = null;
    protected ?FHIRHumanName $name = null;

    /** @var FHIRContactPoint[] */
    protected array $telecom = [];
    protected ?FHIRAddress $address = null;

    public function getPurpose(): ?FHIRCodeableConcept
    {
        return $this->purpose;
    }

    public function setPurpose(?FHIRCodeableConcept $value): self
    {
        $this->purpose = $value;

        return $this;
    }

    public function getName(): ?FHIRHumanName
    {
        return $this->name;
    }

    public function setName(?FHIRHumanName $value): self
    {
        $this->name = $value;

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getTelecom(): array
    {
        return $this->telecom;
    }

    public function setTelecom(?FHIRContactPoint ...$value): self
    {
        $this->telecom = array_filter($value);

        return $this;
    }

    public function addTelecom(?FHIRContactPoint ...$value): self
    {
        $this->telecom = array_filter(array_merge($this->telecom, $value));

        return $this;
    }

    public function getAddress(): ?FHIRAddress
    {
        return $this->address;
    }

    public function setAddress(?FHIRAddress $value): self
    {
        $this->address = $value;

        return $this;
    }
}
