<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EvidenceVariableCharacteristic Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREvidenceVariableCharacteristicInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRExpression;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIREvidenceVariableCharacteristic extends FHIRBackboneElement implements FHIREvidenceVariableCharacteristicInterface
{
    public const RESOURCE_NAME = 'EvidenceVariable.characteristic';

    protected ?FHIRString $description = null;
    protected FHIRReference|FHIRCanonical|FHIRCodeableConcept|FHIRExpression|null $definition = null;
    protected ?FHIRCodeableConcept $method = null;
    protected ?FHIRReference $device = null;
    protected ?FHIRBoolean $exclude = null;
    protected ?FHIREvidenceVariableCharacteristicTimeFromStart $timeFromStart = null;
    protected ?FHIRCode $groupMeasure = null;

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDefinition(): FHIRReference|FHIRCanonical|FHIRCodeableConcept|FHIRExpression|null
    {
        return $this->definition;
    }

    public function setDefinition(FHIRReference|FHIRCanonical|FHIRCodeableConcept|FHIRExpression|null $value): self
    {
        $this->definition = $value;

        return $this;
    }

    public function getMethod(): ?FHIRCodeableConcept
    {
        return $this->method;
    }

    public function setMethod(?FHIRCodeableConcept $value): self
    {
        $this->method = $value;

        return $this;
    }

    public function getDevice(): ?FHIRReference
    {
        return $this->device;
    }

    public function setDevice(?FHIRReference $value): self
    {
        $this->device = $value;

        return $this;
    }

    public function getExclude(): ?FHIRBoolean
    {
        return $this->exclude;
    }

    public function setExclude(bool|FHIRBoolean|null $value): self
    {
        $this->exclude = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getTimeFromStart(): ?FHIREvidenceVariableCharacteristicTimeFromStart
    {
        return $this->timeFromStart;
    }

    public function setTimeFromStart(?FHIREvidenceVariableCharacteristicTimeFromStart $value): self
    {
        $this->timeFromStart = $value;

        return $this;
    }

    public function getGroupMeasure(): ?FHIRCode
    {
        return $this->groupMeasure;
    }

    public function setGroupMeasure(string|FHIRCode|null $value): self
    {
        $this->groupMeasure = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }
}
