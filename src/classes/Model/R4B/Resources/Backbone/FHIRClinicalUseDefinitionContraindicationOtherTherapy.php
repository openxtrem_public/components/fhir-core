<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClinicalUseDefinitionContraindicationOtherTherapy Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRClinicalUseDefinitionContraindicationOtherTherapyInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableReference;

class FHIRClinicalUseDefinitionContraindicationOtherTherapy extends FHIRBackboneElement implements FHIRClinicalUseDefinitionContraindicationOtherTherapyInterface
{
    public const RESOURCE_NAME = 'ClinicalUseDefinition.contraindication.otherTherapy';

    protected ?FHIRCodeableConcept $relationshipType = null;
    protected ?FHIRCodeableReference $therapy = null;

    public function getRelationshipType(): ?FHIRCodeableConcept
    {
        return $this->relationshipType;
    }

    public function setRelationshipType(?FHIRCodeableConcept $value): self
    {
        $this->relationshipType = $value;

        return $this;
    }

    public function getTherapy(): ?FHIRCodeableReference
    {
        return $this->therapy;
    }

    public function setTherapy(?FHIRCodeableReference $value): self
    {
        $this->therapy = $value;

        return $this;
    }
}
