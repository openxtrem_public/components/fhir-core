<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SupplyDeliverySuppliedItem Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSupplyDeliverySuppliedItemInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;

class FHIRSupplyDeliverySuppliedItem extends FHIRBackboneElement implements FHIRSupplyDeliverySuppliedItemInterface
{
    public const RESOURCE_NAME = 'SupplyDelivery.suppliedItem';

    protected ?FHIRQuantity $quantity = null;
    protected FHIRCodeableConcept|FHIRReference|null $item = null;

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getItem(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->item;
    }

    public function setItem(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->item = $value;

        return $this;
    }
}
