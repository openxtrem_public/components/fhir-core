<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MolecularSequenceStructureVariantInner Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMolecularSequenceStructureVariantInnerInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRInteger;

class FHIRMolecularSequenceStructureVariantInner extends FHIRBackboneElement implements FHIRMolecularSequenceStructureVariantInnerInterface
{
    public const RESOURCE_NAME = 'MolecularSequence.structureVariant.inner';

    protected ?FHIRInteger $start = null;
    protected ?FHIRInteger $end = null;

    public function getStart(): ?FHIRInteger
    {
        return $this->start;
    }

    public function setStart(int|FHIRInteger|null $value): self
    {
        $this->start = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getEnd(): ?FHIRInteger
    {
        return $this->end;
    }

    public function setEnd(int|FHIRInteger|null $value): self
    {
        $this->end = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }
}
