<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledgeRegulatorySchedule Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationKnowledgeRegulatoryScheduleInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;

class FHIRMedicationKnowledgeRegulatorySchedule extends FHIRBackboneElement implements FHIRMedicationKnowledgeRegulatoryScheduleInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge.regulatory.schedule';

    protected ?FHIRCodeableConcept $schedule = null;

    public function getSchedule(): ?FHIRCodeableConcept
    {
        return $this->schedule;
    }

    public function setSchedule(?FHIRCodeableConcept $value): self
    {
        $this->schedule = $value;

        return $this;
    }
}
