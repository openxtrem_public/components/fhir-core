<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClaimResponseError Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRClaimResponseErrorInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRPositiveInt;

class FHIRClaimResponseError extends FHIRBackboneElement implements FHIRClaimResponseErrorInterface
{
    public const RESOURCE_NAME = 'ClaimResponse.error';

    protected ?FHIRPositiveInt $itemSequence = null;
    protected ?FHIRPositiveInt $detailSequence = null;
    protected ?FHIRPositiveInt $subDetailSequence = null;
    protected ?FHIRCodeableConcept $code = null;

    public function getItemSequence(): ?FHIRPositiveInt
    {
        return $this->itemSequence;
    }

    public function setItemSequence(int|FHIRPositiveInt|null $value): self
    {
        $this->itemSequence = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getDetailSequence(): ?FHIRPositiveInt
    {
        return $this->detailSequence;
    }

    public function setDetailSequence(int|FHIRPositiveInt|null $value): self
    {
        $this->detailSequence = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getSubDetailSequence(): ?FHIRPositiveInt
    {
        return $this->subDetailSequence;
    }

    public function setSubDetailSequence(int|FHIRPositiveInt|null $value): self
    {
        $this->subDetailSequence = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }
}
