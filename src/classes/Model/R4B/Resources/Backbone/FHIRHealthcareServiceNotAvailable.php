<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR HealthcareServiceNotAvailable Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRHealthcareServiceNotAvailableInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRHealthcareServiceNotAvailable extends FHIRBackboneElement implements FHIRHealthcareServiceNotAvailableInterface
{
    public const RESOURCE_NAME = 'HealthcareService.notAvailable';

    protected ?FHIRString $description = null;
    protected ?FHIRPeriod $during = null;

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDuring(): ?FHIRPeriod
    {
        return $this->during;
    }

    public function setDuring(?FHIRPeriod $value): self
    {
        $this->during = $value;

        return $this;
    }
}
