<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MeasureReportGroupStratifierStratumComponent Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMeasureReportGroupStratifierStratumComponentInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;

class FHIRMeasureReportGroupStratifierStratumComponent extends FHIRBackboneElement implements FHIRMeasureReportGroupStratifierStratumComponentInterface
{
    public const RESOURCE_NAME = 'MeasureReport.group.stratifier.stratum.component';

    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRCodeableConcept $value = null;

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getValue(): ?FHIRCodeableConcept
    {
        return $this->value;
    }

    public function setValue(?FHIRCodeableConcept $value): self
    {
        $this->value = $value;

        return $this;
    }
}
