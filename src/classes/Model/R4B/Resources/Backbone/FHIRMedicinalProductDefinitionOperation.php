<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductDefinitionOperation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductDefinitionOperationInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;

class FHIRMedicinalProductDefinitionOperation extends FHIRBackboneElement implements FHIRMedicinalProductDefinitionOperationInterface
{
    public const RESOURCE_NAME = 'MedicinalProductDefinition.operation';

    protected ?FHIRCodeableReference $type = null;
    protected ?FHIRPeriod $effectiveDate = null;

    /** @var FHIRReference[] */
    protected array $organization = [];
    protected ?FHIRCodeableConcept $confidentialityIndicator = null;

    public function getType(): ?FHIRCodeableReference
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableReference $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getEffectiveDate(): ?FHIRPeriod
    {
        return $this->effectiveDate;
    }

    public function setEffectiveDate(?FHIRPeriod $value): self
    {
        $this->effectiveDate = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getOrganization(): array
    {
        return $this->organization;
    }

    public function setOrganization(?FHIRReference ...$value): self
    {
        $this->organization = array_filter($value);

        return $this;
    }

    public function addOrganization(?FHIRReference ...$value): self
    {
        $this->organization = array_filter(array_merge($this->organization, $value));

        return $this;
    }

    public function getConfidentialityIndicator(): ?FHIRCodeableConcept
    {
        return $this->confidentialityIndicator;
    }

    public function setConfidentialityIndicator(?FHIRCodeableConcept $value): self
    {
        $this->confidentialityIndicator = $value;

        return $this;
    }
}
