<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CitationCitedArtifactContributorshipEntryAffiliationInfo Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCitationCitedArtifactContributorshipEntryAffiliationInfoInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRCitationCitedArtifactContributorshipEntryAffiliationInfo extends FHIRBackboneElement implements FHIRCitationCitedArtifactContributorshipEntryAffiliationInfoInterface
{
    public const RESOURCE_NAME = 'Citation.citedArtifact.contributorship.entry.affiliationInfo';

    protected ?FHIRString $affiliation = null;
    protected ?FHIRString $role = null;

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    public function getAffiliation(): ?FHIRString
    {
        return $this->affiliation;
    }

    public function setAffiliation(string|FHIRString|null $value): self
    {
        $this->affiliation = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getRole(): ?FHIRString
    {
        return $this->role;
    }

    public function setRole(string|FHIRString|null $value): self
    {
        $this->role = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }
}
