<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR QuestionnaireItem Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRQuestionnaireItemInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUri;

class FHIRQuestionnaireItem extends FHIRBackboneElement implements FHIRQuestionnaireItemInterface
{
    public const RESOURCE_NAME = 'Questionnaire.item';

    protected ?FHIRString $linkId = null;
    protected ?FHIRUri $definition = null;

    /** @var FHIRCoding[] */
    protected array $code = [];
    protected ?FHIRString $prefix = null;
    protected ?FHIRString $text = null;
    protected ?FHIRCode $type = null;

    /** @var FHIRQuestionnaireItemEnableWhen[] */
    protected array $enableWhen = [];
    protected ?FHIRCode $enableBehavior = null;
    protected ?FHIRBoolean $required = null;
    protected ?FHIRBoolean $repeats = null;
    protected ?FHIRBoolean $readOnly = null;
    protected ?FHIRInteger $maxLength = null;
    protected ?FHIRCanonical $answerValueSet = null;

    /** @var FHIRQuestionnaireItemAnswerOption[] */
    protected array $answerOption = [];

    /** @var FHIRQuestionnaireItemInitial[] */
    protected array $initial = [];

    /** @var FHIRQuestionnaireItem[] */
    protected array $item = [];

    public function getLinkId(): ?FHIRString
    {
        return $this->linkId;
    }

    public function setLinkId(string|FHIRString|null $value): self
    {
        $this->linkId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDefinition(): ?FHIRUri
    {
        return $this->definition;
    }

    public function setDefinition(string|FHIRUri|null $value): self
    {
        $this->definition = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getCode(): array
    {
        return $this->code;
    }

    public function setCode(?FHIRCoding ...$value): self
    {
        $this->code = array_filter($value);

        return $this;
    }

    public function addCode(?FHIRCoding ...$value): self
    {
        $this->code = array_filter(array_merge($this->code, $value));

        return $this;
    }

    public function getPrefix(): ?FHIRString
    {
        return $this->prefix;
    }

    public function setPrefix(string|FHIRString|null $value): self
    {
        $this->prefix = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getText(): ?FHIRString
    {
        return $this->text;
    }

    public function setText(string|FHIRString|null $value): self
    {
        $this->text = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRQuestionnaireItemEnableWhen[]
     */
    public function getEnableWhen(): array
    {
        return $this->enableWhen;
    }

    public function setEnableWhen(?FHIRQuestionnaireItemEnableWhen ...$value): self
    {
        $this->enableWhen = array_filter($value);

        return $this;
    }

    public function addEnableWhen(?FHIRQuestionnaireItemEnableWhen ...$value): self
    {
        $this->enableWhen = array_filter(array_merge($this->enableWhen, $value));

        return $this;
    }

    public function getEnableBehavior(): ?FHIRCode
    {
        return $this->enableBehavior;
    }

    public function setEnableBehavior(string|FHIRCode|null $value): self
    {
        $this->enableBehavior = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getRequired(): ?FHIRBoolean
    {
        return $this->required;
    }

    public function setRequired(bool|FHIRBoolean|null $value): self
    {
        $this->required = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getRepeats(): ?FHIRBoolean
    {
        return $this->repeats;
    }

    public function setRepeats(bool|FHIRBoolean|null $value): self
    {
        $this->repeats = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getReadOnly(): ?FHIRBoolean
    {
        return $this->readOnly;
    }

    public function setReadOnly(bool|FHIRBoolean|null $value): self
    {
        $this->readOnly = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getMaxLength(): ?FHIRInteger
    {
        return $this->maxLength;
    }

    public function setMaxLength(int|FHIRInteger|null $value): self
    {
        $this->maxLength = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getAnswerValueSet(): ?FHIRCanonical
    {
        return $this->answerValueSet;
    }

    public function setAnswerValueSet(string|FHIRCanonical|null $value): self
    {
        $this->answerValueSet = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRQuestionnaireItemAnswerOption[]
     */
    public function getAnswerOption(): array
    {
        return $this->answerOption;
    }

    public function setAnswerOption(?FHIRQuestionnaireItemAnswerOption ...$value): self
    {
        $this->answerOption = array_filter($value);

        return $this;
    }

    public function addAnswerOption(?FHIRQuestionnaireItemAnswerOption ...$value): self
    {
        $this->answerOption = array_filter(array_merge($this->answerOption, $value));

        return $this;
    }

    /**
     * @return FHIRQuestionnaireItemInitial[]
     */
    public function getInitial(): array
    {
        return $this->initial;
    }

    public function setInitial(?FHIRQuestionnaireItemInitial ...$value): self
    {
        $this->initial = array_filter($value);

        return $this;
    }

    public function addInitial(?FHIRQuestionnaireItemInitial ...$value): self
    {
        $this->initial = array_filter(array_merge($this->initial, $value));

        return $this;
    }

    /**
     * @return FHIRQuestionnaireItem[]
     */
    public function getItem(): array
    {
        return $this->item;
    }

    public function setItem(?FHIRQuestionnaireItem ...$value): self
    {
        $this->item = array_filter($value);

        return $this;
    }

    public function addItem(?FHIRQuestionnaireItem ...$value): self
    {
        $this->item = array_filter(array_merge($this->item, $value));

        return $this;
    }
}
