<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DocumentReferenceRelatesTo Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDocumentReferenceRelatesToInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;

class FHIRDocumentReferenceRelatesTo extends FHIRBackboneElement implements FHIRDocumentReferenceRelatesToInterface
{
    public const RESOURCE_NAME = 'DocumentReference.relatesTo';

    protected ?FHIRCode $code = null;
    protected ?FHIRReference $target = null;

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getTarget(): ?FHIRReference
    {
        return $this->target;
    }

    public function setTarget(?FHIRReference $value): self
    {
        $this->target = $value;

        return $this;
    }
}
