<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MeasureSupplementalData Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMeasureSupplementalDataInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRExpression;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRMeasureSupplementalData extends FHIRBackboneElement implements FHIRMeasureSupplementalDataInterface
{
    public const RESOURCE_NAME = 'Measure.supplementalData';

    protected ?FHIRCodeableConcept $code = null;

    /** @var FHIRCodeableConcept[] */
    protected array $usage = [];
    protected ?FHIRString $description = null;
    protected ?FHIRExpression $criteria = null;

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getUsage(): array
    {
        return $this->usage;
    }

    public function setUsage(?FHIRCodeableConcept ...$value): self
    {
        $this->usage = array_filter($value);

        return $this;
    }

    public function addUsage(?FHIRCodeableConcept ...$value): self
    {
        $this->usage = array_filter(array_merge($this->usage, $value));

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCriteria(): ?FHIRExpression
    {
        return $this->criteria;
    }

    public function setCriteria(?FHIRExpression $value): self
    {
        $this->criteria = $value;

        return $this;
    }
}
