<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubscriptionTopicResourceTriggerQueryCriteria Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubscriptionTopicResourceTriggerQueryCriteriaInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRSubscriptionTopicResourceTriggerQueryCriteria extends FHIRBackboneElement implements FHIRSubscriptionTopicResourceTriggerQueryCriteriaInterface
{
    public const RESOURCE_NAME = 'SubscriptionTopic.resourceTrigger.queryCriteria';

    protected ?FHIRString $previous = null;
    protected ?FHIRCode $resultForCreate = null;
    protected ?FHIRString $current = null;
    protected ?FHIRCode $resultForDelete = null;
    protected ?FHIRBoolean $requireBoth = null;

    public function getPrevious(): ?FHIRString
    {
        return $this->previous;
    }

    public function setPrevious(string|FHIRString|null $value): self
    {
        $this->previous = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getResultForCreate(): ?FHIRCode
    {
        return $this->resultForCreate;
    }

    public function setResultForCreate(string|FHIRCode|null $value): self
    {
        $this->resultForCreate = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCurrent(): ?FHIRString
    {
        return $this->current;
    }

    public function setCurrent(string|FHIRString|null $value): self
    {
        $this->current = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getResultForDelete(): ?FHIRCode
    {
        return $this->resultForDelete;
    }

    public function setResultForDelete(string|FHIRCode|null $value): self
    {
        $this->resultForDelete = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getRequireBoth(): ?FHIRBoolean
    {
        return $this->requireBoth;
    }

    public function setRequireBoth(bool|FHIRBoolean|null $value): self
    {
        $this->requireBoth = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }
}
