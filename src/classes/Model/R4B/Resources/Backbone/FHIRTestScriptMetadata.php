<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestScriptMetadata Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestScriptMetadataInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;

class FHIRTestScriptMetadata extends FHIRBackboneElement implements FHIRTestScriptMetadataInterface
{
    public const RESOURCE_NAME = 'TestScript.metadata';

    /** @var FHIRTestScriptMetadataLink[] */
    protected array $link = [];

    /** @var FHIRTestScriptMetadataCapability[] */
    protected array $capability = [];

    /**
     * @return FHIRTestScriptMetadataLink[]
     */
    public function getLink(): array
    {
        return $this->link;
    }

    public function setLink(?FHIRTestScriptMetadataLink ...$value): self
    {
        $this->link = array_filter($value);

        return $this;
    }

    public function addLink(?FHIRTestScriptMetadataLink ...$value): self
    {
        $this->link = array_filter(array_merge($this->link, $value));

        return $this;
    }

    /**
     * @return FHIRTestScriptMetadataCapability[]
     */
    public function getCapability(): array
    {
        return $this->capability;
    }

    public function setCapability(?FHIRTestScriptMetadataCapability ...$value): self
    {
        $this->capability = array_filter($value);

        return $this;
    }

    public function addCapability(?FHIRTestScriptMetadataCapability ...$value): self
    {
        $this->capability = array_filter(array_merge($this->capability, $value));

        return $this;
    }
}
