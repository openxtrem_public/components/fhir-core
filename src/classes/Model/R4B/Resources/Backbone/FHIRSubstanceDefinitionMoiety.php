<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceDefinitionMoiety Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceDefinitionMoietyInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRSubstanceDefinitionMoiety extends FHIRBackboneElement implements FHIRSubstanceDefinitionMoietyInterface
{
    public const RESOURCE_NAME = 'SubstanceDefinition.moiety';

    protected ?FHIRCodeableConcept $role = null;
    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRString $name = null;
    protected ?FHIRCodeableConcept $stereochemistry = null;
    protected ?FHIRCodeableConcept $opticalActivity = null;
    protected ?FHIRString $molecularFormula = null;
    protected FHIRQuantity|FHIRString|null $amount = null;
    protected ?FHIRCodeableConcept $measurementType = null;

    public function getRole(): ?FHIRCodeableConcept
    {
        return $this->role;
    }

    public function setRole(?FHIRCodeableConcept $value): self
    {
        $this->role = $value;

        return $this;
    }

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStereochemistry(): ?FHIRCodeableConcept
    {
        return $this->stereochemistry;
    }

    public function setStereochemistry(?FHIRCodeableConcept $value): self
    {
        $this->stereochemistry = $value;

        return $this;
    }

    public function getOpticalActivity(): ?FHIRCodeableConcept
    {
        return $this->opticalActivity;
    }

    public function setOpticalActivity(?FHIRCodeableConcept $value): self
    {
        $this->opticalActivity = $value;

        return $this;
    }

    public function getMolecularFormula(): ?FHIRString
    {
        return $this->molecularFormula;
    }

    public function setMolecularFormula(string|FHIRString|null $value): self
    {
        $this->molecularFormula = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getAmount(): FHIRQuantity|FHIRString|null
    {
        return $this->amount;
    }

    public function setAmount(FHIRQuantity|FHIRString|null $value): self
    {
        $this->amount = $value;

        return $this;
    }

    public function getMeasurementType(): ?FHIRCodeableConcept
    {
        return $this->measurementType;
    }

    public function setMeasurementType(?FHIRCodeableConcept $value): self
    {
        $this->measurementType = $value;

        return $this;
    }
}
