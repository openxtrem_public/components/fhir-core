<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImmunizationProtocolApplied Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImmunizationProtocolAppliedInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRImmunizationProtocolApplied extends FHIRBackboneElement implements FHIRImmunizationProtocolAppliedInterface
{
    public const RESOURCE_NAME = 'Immunization.protocolApplied';

    protected ?FHIRString $series = null;
    protected ?FHIRReference $authority = null;

    /** @var FHIRCodeableConcept[] */
    protected array $targetDisease = [];
    protected FHIRPositiveInt|FHIRString|null $doseNumber = null;
    protected FHIRPositiveInt|FHIRString|null $seriesDoses = null;

    public function getSeries(): ?FHIRString
    {
        return $this->series;
    }

    public function setSeries(string|FHIRString|null $value): self
    {
        $this->series = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getAuthority(): ?FHIRReference
    {
        return $this->authority;
    }

    public function setAuthority(?FHIRReference $value): self
    {
        $this->authority = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getTargetDisease(): array
    {
        return $this->targetDisease;
    }

    public function setTargetDisease(?FHIRCodeableConcept ...$value): self
    {
        $this->targetDisease = array_filter($value);

        return $this;
    }

    public function addTargetDisease(?FHIRCodeableConcept ...$value): self
    {
        $this->targetDisease = array_filter(array_merge($this->targetDisease, $value));

        return $this;
    }

    public function getDoseNumber(): FHIRPositiveInt|FHIRString|null
    {
        return $this->doseNumber;
    }

    public function setDoseNumber(FHIRPositiveInt|FHIRString|null $value): self
    {
        $this->doseNumber = $value;

        return $this;
    }

    public function getSeriesDoses(): FHIRPositiveInt|FHIRString|null
    {
        return $this->seriesDoses;
    }

    public function setSeriesDoses(FHIRPositiveInt|FHIRString|null $value): self
    {
        $this->seriesDoses = $value;

        return $this;
    }
}
