<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PackagedProductDefinitionPackageProperty Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRPackagedProductDefinitionPackagePropertyInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDate;

class FHIRPackagedProductDefinitionPackageProperty extends FHIRBackboneElement implements FHIRPackagedProductDefinitionPackagePropertyInterface
{
    public const RESOURCE_NAME = 'PackagedProductDefinition.package.property';

    protected ?FHIRCodeableConcept $type = null;
    protected FHIRCodeableConcept|FHIRQuantity|FHIRDate|FHIRBoolean|FHIRAttachment|null $value = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getValue(): FHIRCodeableConcept|FHIRQuantity|FHIRDate|FHIRBoolean|FHIRAttachment|null
    {
        return $this->value;
    }

    public function setValue(FHIRCodeableConcept|FHIRQuantity|FHIRDate|FHIRBoolean|FHIRAttachment|null $value): self
    {
        $this->value = $value;

        return $this;
    }
}
