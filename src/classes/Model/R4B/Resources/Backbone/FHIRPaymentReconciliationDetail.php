<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PaymentReconciliationDetail Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRPaymentReconciliationDetailInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDate;

class FHIRPaymentReconciliationDetail extends FHIRBackboneElement implements FHIRPaymentReconciliationDetailInterface
{
    public const RESOURCE_NAME = 'PaymentReconciliation.detail';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRIdentifier $predecessor = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRReference $request = null;
    protected ?FHIRReference $submitter = null;
    protected ?FHIRReference $response = null;
    protected ?FHIRDate $date = null;
    protected ?FHIRReference $responsible = null;
    protected ?FHIRReference $payee = null;
    protected ?FHIRMoney $amount = null;

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getPredecessor(): ?FHIRIdentifier
    {
        return $this->predecessor;
    }

    public function setPredecessor(?FHIRIdentifier $value): self
    {
        $this->predecessor = $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getRequest(): ?FHIRReference
    {
        return $this->request;
    }

    public function setRequest(?FHIRReference $value): self
    {
        $this->request = $value;

        return $this;
    }

    public function getSubmitter(): ?FHIRReference
    {
        return $this->submitter;
    }

    public function setSubmitter(?FHIRReference $value): self
    {
        $this->submitter = $value;

        return $this;
    }

    public function getResponse(): ?FHIRReference
    {
        return $this->response;
    }

    public function setResponse(?FHIRReference $value): self
    {
        $this->response = $value;

        return $this;
    }

    public function getDate(): ?FHIRDate
    {
        return $this->date;
    }

    public function setDate(string|FHIRDate|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getResponsible(): ?FHIRReference
    {
        return $this->responsible;
    }

    public function setResponsible(?FHIRReference $value): self
    {
        $this->responsible = $value;

        return $this;
    }

    public function getPayee(): ?FHIRReference
    {
        return $this->payee;
    }

    public function setPayee(?FHIRReference $value): self
    {
        $this->payee = $value;

        return $this;
    }

    public function getAmount(): ?FHIRMoney
    {
        return $this->amount;
    }

    public function setAmount(?FHIRMoney $value): self
    {
        $this->amount = $value;

        return $this;
    }
}
