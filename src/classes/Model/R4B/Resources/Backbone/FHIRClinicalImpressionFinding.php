<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClinicalImpressionFinding Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRClinicalImpressionFindingInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRClinicalImpressionFinding extends FHIRBackboneElement implements FHIRClinicalImpressionFindingInterface
{
    public const RESOURCE_NAME = 'ClinicalImpression.finding';

    protected ?FHIRCodeableConcept $itemCodeableConcept = null;
    protected ?FHIRReference $itemReference = null;
    protected ?FHIRString $basis = null;

    public function getItemCodeableConcept(): ?FHIRCodeableConcept
    {
        return $this->itemCodeableConcept;
    }

    public function setItemCodeableConcept(?FHIRCodeableConcept $value): self
    {
        $this->itemCodeableConcept = $value;

        return $this;
    }

    public function getItemReference(): ?FHIRReference
    {
        return $this->itemReference;
    }

    public function setItemReference(?FHIRReference $value): self
    {
        $this->itemReference = $value;

        return $this;
    }

    public function getBasis(): ?FHIRString
    {
        return $this->basis;
    }

    public function setBasis(string|FHIRString|null $value): self
    {
        $this->basis = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
