<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductDefinitionNameNamePart Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductDefinitionNameNamePartInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRMedicinalProductDefinitionNameNamePart extends FHIRBackboneElement implements FHIRMedicinalProductDefinitionNameNamePartInterface
{
    public const RESOURCE_NAME = 'MedicinalProductDefinition.name.namePart';

    protected ?FHIRString $part = null;
    protected ?FHIRCodeableConcept $type = null;

    public function getPart(): ?FHIRString
    {
        return $this->part;
    }

    public function setPart(string|FHIRString|null $value): self
    {
        $this->part = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }
}
