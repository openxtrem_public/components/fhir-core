<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImmunizationRecommendationRecommendation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImmunizationRecommendationRecommendationInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRImmunizationRecommendationRecommendation extends FHIRBackboneElement implements FHIRImmunizationRecommendationRecommendationInterface
{
    public const RESOURCE_NAME = 'ImmunizationRecommendation.recommendation';

    /** @var FHIRCodeableConcept[] */
    protected array $vaccineCode = [];
    protected ?FHIRCodeableConcept $targetDisease = null;

    /** @var FHIRCodeableConcept[] */
    protected array $contraindicatedVaccineCode = [];
    protected ?FHIRCodeableConcept $forecastStatus = null;

    /** @var FHIRCodeableConcept[] */
    protected array $forecastReason = [];

    /** @var FHIRImmunizationRecommendationRecommendationDateCriterion[] */
    protected array $dateCriterion = [];
    protected ?FHIRString $description = null;
    protected ?FHIRString $series = null;
    protected FHIRPositiveInt|FHIRString|null $doseNumber = null;
    protected FHIRPositiveInt|FHIRString|null $seriesDoses = null;

    /** @var FHIRReference[] */
    protected array $supportingImmunization = [];

    /** @var FHIRReference[] */
    protected array $supportingPatientInformation = [];

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getVaccineCode(): array
    {
        return $this->vaccineCode;
    }

    public function setVaccineCode(?FHIRCodeableConcept ...$value): self
    {
        $this->vaccineCode = array_filter($value);

        return $this;
    }

    public function addVaccineCode(?FHIRCodeableConcept ...$value): self
    {
        $this->vaccineCode = array_filter(array_merge($this->vaccineCode, $value));

        return $this;
    }

    public function getTargetDisease(): ?FHIRCodeableConcept
    {
        return $this->targetDisease;
    }

    public function setTargetDisease(?FHIRCodeableConcept $value): self
    {
        $this->targetDisease = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getContraindicatedVaccineCode(): array
    {
        return $this->contraindicatedVaccineCode;
    }

    public function setContraindicatedVaccineCode(?FHIRCodeableConcept ...$value): self
    {
        $this->contraindicatedVaccineCode = array_filter($value);

        return $this;
    }

    public function addContraindicatedVaccineCode(?FHIRCodeableConcept ...$value): self
    {
        $this->contraindicatedVaccineCode = array_filter(array_merge($this->contraindicatedVaccineCode, $value));

        return $this;
    }

    public function getForecastStatus(): ?FHIRCodeableConcept
    {
        return $this->forecastStatus;
    }

    public function setForecastStatus(?FHIRCodeableConcept $value): self
    {
        $this->forecastStatus = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getForecastReason(): array
    {
        return $this->forecastReason;
    }

    public function setForecastReason(?FHIRCodeableConcept ...$value): self
    {
        $this->forecastReason = array_filter($value);

        return $this;
    }

    public function addForecastReason(?FHIRCodeableConcept ...$value): self
    {
        $this->forecastReason = array_filter(array_merge($this->forecastReason, $value));

        return $this;
    }

    /**
     * @return FHIRImmunizationRecommendationRecommendationDateCriterion[]
     */
    public function getDateCriterion(): array
    {
        return $this->dateCriterion;
    }

    public function setDateCriterion(?FHIRImmunizationRecommendationRecommendationDateCriterion ...$value): self
    {
        $this->dateCriterion = array_filter($value);

        return $this;
    }

    public function addDateCriterion(?FHIRImmunizationRecommendationRecommendationDateCriterion ...$value): self
    {
        $this->dateCriterion = array_filter(array_merge($this->dateCriterion, $value));

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSeries(): ?FHIRString
    {
        return $this->series;
    }

    public function setSeries(string|FHIRString|null $value): self
    {
        $this->series = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDoseNumber(): FHIRPositiveInt|FHIRString|null
    {
        return $this->doseNumber;
    }

    public function setDoseNumber(FHIRPositiveInt|FHIRString|null $value): self
    {
        $this->doseNumber = $value;

        return $this;
    }

    public function getSeriesDoses(): FHIRPositiveInt|FHIRString|null
    {
        return $this->seriesDoses;
    }

    public function setSeriesDoses(FHIRPositiveInt|FHIRString|null $value): self
    {
        $this->seriesDoses = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupportingImmunization(): array
    {
        return $this->supportingImmunization;
    }

    public function setSupportingImmunization(?FHIRReference ...$value): self
    {
        $this->supportingImmunization = array_filter($value);

        return $this;
    }

    public function addSupportingImmunization(?FHIRReference ...$value): self
    {
        $this->supportingImmunization = array_filter(array_merge($this->supportingImmunization, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupportingPatientInformation(): array
    {
        return $this->supportingPatientInformation;
    }

    public function setSupportingPatientInformation(?FHIRReference ...$value): self
    {
        $this->supportingPatientInformation = array_filter($value);

        return $this;
    }

    public function addSupportingPatientInformation(?FHIRReference ...$value): self
    {
        $this->supportingPatientInformation = array_filter(array_merge($this->supportingPatientInformation, $value));

        return $this;
    }
}
