<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CitationCitedArtifactContributorshipSummary Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCitationCitedArtifactContributorshipSummaryInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRMarkdown;

class FHIRCitationCitedArtifactContributorshipSummary extends FHIRBackboneElement implements FHIRCitationCitedArtifactContributorshipSummaryInterface
{
    public const RESOURCE_NAME = 'Citation.citedArtifact.contributorship.summary';

    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableConcept $style = null;
    protected ?FHIRCodeableConcept $source = null;
    protected ?FHIRMarkdown $value = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getStyle(): ?FHIRCodeableConcept
    {
        return $this->style;
    }

    public function setStyle(?FHIRCodeableConcept $value): self
    {
        $this->style = $value;

        return $this;
    }

    public function getSource(): ?FHIRCodeableConcept
    {
        return $this->source;
    }

    public function setSource(?FHIRCodeableConcept $value): self
    {
        $this->source = $value;

        return $this;
    }

    public function getValue(): ?FHIRMarkdown
    {
        return $this->value;
    }

    public function setValue(string|FHIRMarkdown|null $value): self
    {
        $this->value = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }
}
