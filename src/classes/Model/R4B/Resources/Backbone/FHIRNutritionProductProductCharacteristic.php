<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR NutritionProductProductCharacteristic Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRNutritionProductProductCharacteristicInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBase64Binary;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRNutritionProductProductCharacteristic extends FHIRBackboneElement implements FHIRNutritionProductProductCharacteristicInterface
{
    public const RESOURCE_NAME = 'NutritionProduct.productCharacteristic';

    protected ?FHIRCodeableConcept $type = null;
    protected FHIRCodeableConcept|FHIRString|FHIRQuantity|FHIRBase64Binary|FHIRAttachment|FHIRBoolean|null $value = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getValue(
    ): FHIRCodeableConcept|FHIRString|FHIRQuantity|FHIRBase64Binary|FHIRAttachment|FHIRBoolean|null
    {
        return $this->value;
    }

    public function setValue(
        FHIRCodeableConcept|FHIRString|FHIRQuantity|FHIRBase64Binary|FHIRAttachment|FHIRBoolean|null $value,
    ): self
    {
        $this->value = $value;

        return $this;
    }
}
