<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EvidenceStatisticModelCharacteristic Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREvidenceStatisticModelCharacteristicInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRQuantity;

class FHIREvidenceStatisticModelCharacteristic extends FHIRBackboneElement implements FHIREvidenceStatisticModelCharacteristicInterface
{
    public const RESOURCE_NAME = 'Evidence.statistic.modelCharacteristic';

    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRQuantity $value = null;

    /** @var FHIREvidenceStatisticModelCharacteristicVariable[] */
    protected array $variable = [];

    /** @var FHIREvidenceStatisticAttributeEstimate[] */
    protected array $attributeEstimate = [];

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getValue(): ?FHIRQuantity
    {
        return $this->value;
    }

    public function setValue(?FHIRQuantity $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return FHIREvidenceStatisticModelCharacteristicVariable[]
     */
    public function getVariable(): array
    {
        return $this->variable;
    }

    public function setVariable(?FHIREvidenceStatisticModelCharacteristicVariable ...$value): self
    {
        $this->variable = array_filter($value);

        return $this;
    }

    public function addVariable(?FHIREvidenceStatisticModelCharacteristicVariable ...$value): self
    {
        $this->variable = array_filter(array_merge($this->variable, $value));

        return $this;
    }

    /**
     * @return FHIREvidenceStatisticAttributeEstimate[]
     */
    public function getAttributeEstimate(): array
    {
        return $this->attributeEstimate;
    }

    public function setAttributeEstimate(?FHIREvidenceStatisticAttributeEstimate ...$value): self
    {
        $this->attributeEstimate = array_filter($value);

        return $this;
    }

    public function addAttributeEstimate(?FHIREvidenceStatisticAttributeEstimate ...$value): self
    {
        $this->attributeEstimate = array_filter(array_merge($this->attributeEstimate, $value));

        return $this;
    }
}
