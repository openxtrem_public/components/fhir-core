<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR RegulatedAuthorizationCase Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRRegulatedAuthorizationCaseInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;

class FHIRRegulatedAuthorizationCase extends FHIRBackboneElement implements FHIRRegulatedAuthorizationCaseInterface
{
    public const RESOURCE_NAME = 'RegulatedAuthorization.case';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableConcept $status = null;
    protected FHIRPeriod|FHIRDateTime|null $date = null;

    /** @var FHIRRegulatedAuthorizationCase[] */
    protected array $application = [];

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCodeableConcept
    {
        return $this->status;
    }

    public function setStatus(?FHIRCodeableConcept $value): self
    {
        $this->status = $value;

        return $this;
    }

    public function getDate(): FHIRPeriod|FHIRDateTime|null
    {
        return $this->date;
    }

    public function setDate(FHIRPeriod|FHIRDateTime|null $value): self
    {
        $this->date = $value;

        return $this;
    }

    /**
     * @return FHIRRegulatedAuthorizationCase[]
     */
    public function getApplication(): array
    {
        return $this->application;
    }

    public function setApplication(?FHIRRegulatedAuthorizationCase ...$value): self
    {
        $this->application = array_filter($value);

        return $this;
    }

    public function addApplication(?FHIRRegulatedAuthorizationCase ...$value): self
    {
        $this->application = array_filter(array_merge($this->application, $value));

        return $this;
    }
}
