<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EvidenceReportSubjectCharacteristic Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREvidenceReportSubjectCharacteristicInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;

class FHIREvidenceReportSubjectCharacteristic extends FHIRBackboneElement implements FHIREvidenceReportSubjectCharacteristicInterface
{
    public const RESOURCE_NAME = 'EvidenceReport.subject.characteristic';

    protected ?FHIRCodeableConcept $code = null;
    protected FHIRReference|FHIRCodeableConcept|FHIRBoolean|FHIRQuantity|FHIRRange|null $value = null;
    protected ?FHIRBoolean $exclude = null;
    protected ?FHIRPeriod $period = null;

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getValue(): FHIRReference|FHIRCodeableConcept|FHIRBoolean|FHIRQuantity|FHIRRange|null
    {
        return $this->value;
    }

    public function setValue(FHIRReference|FHIRCodeableConcept|FHIRBoolean|FHIRQuantity|FHIRRange|null $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getExclude(): ?FHIRBoolean
    {
        return $this->exclude;
    }

    public function setExclude(bool|FHIRBoolean|null $value): self
    {
        $this->exclude = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }
}
