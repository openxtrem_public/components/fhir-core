<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CoverageCostToBeneficiary Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCoverageCostToBeneficiaryInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRQuantity;

class FHIRCoverageCostToBeneficiary extends FHIRBackboneElement implements FHIRCoverageCostToBeneficiaryInterface
{
    public const RESOURCE_NAME = 'Coverage.costToBeneficiary';

    protected ?FHIRCodeableConcept $type = null;
    protected FHIRQuantity|FHIRMoney|null $value = null;

    /** @var FHIRCoverageCostToBeneficiaryException[] */
    protected array $exception = [];

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getValue(): FHIRQuantity|FHIRMoney|null
    {
        return $this->value;
    }

    public function setValue(FHIRQuantity|FHIRMoney|null $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return FHIRCoverageCostToBeneficiaryException[]
     */
    public function getException(): array
    {
        return $this->exception;
    }

    public function setException(?FHIRCoverageCostToBeneficiaryException ...$value): self
    {
        $this->exception = array_filter($value);

        return $this;
    }

    public function addException(?FHIRCoverageCostToBeneficiaryException ...$value): self
    {
        $this->exception = array_filter(array_merge($this->exception, $value));

        return $this;
    }
}
