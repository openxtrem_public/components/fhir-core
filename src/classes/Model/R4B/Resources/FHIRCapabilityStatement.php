<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CapabilityStatement Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRCapabilityStatementInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRCapabilityStatementDocument;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRCapabilityStatementImplementation;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRCapabilityStatementMessaging;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRCapabilityStatementRest;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRCapabilityStatementSoftware;

class FHIRCapabilityStatement extends FHIRDomainResource implements FHIRCapabilityStatementInterface
{
    public const RESOURCE_NAME = 'CapabilityStatement';

    protected ?FHIRUri $url = null;
    protected ?FHIRString $version = null;
    protected ?FHIRString $name = null;
    protected ?FHIRString $title = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRBoolean $experimental = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRString $publisher = null;

    /** @var FHIRContactDetail[] */
    protected array $contact = [];
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRUsageContext[] */
    protected array $useContext = [];

    /** @var FHIRCodeableConcept[] */
    protected array $jurisdiction = [];
    protected ?FHIRMarkdown $purpose = null;
    protected ?FHIRMarkdown $copyright = null;
    protected ?FHIRCode $kind = null;

    /** @var FHIRCanonical[] */
    protected array $instantiates = [];

    /** @var FHIRCanonical[] */
    protected array $imports = [];
    protected ?FHIRCapabilityStatementSoftware $software = null;
    protected ?FHIRCapabilityStatementImplementation $implementation = null;
    protected ?FHIRCode $fhirVersion = null;

    /** @var FHIRCode[] */
    protected array $format = [];

    /** @var FHIRCode[] */
    protected array $patchFormat = [];

    /** @var FHIRCanonical[] */
    protected array $implementationGuide = [];

    /** @var FHIRCapabilityStatementRest[] */
    protected array $rest = [];

    /** @var FHIRCapabilityStatementMessaging[] */
    protected array $messaging = [];

    /** @var FHIRCapabilityStatementDocument[] */
    protected array $document = [];

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getExperimental(): ?FHIRBoolean
    {
        return $this->experimental;
    }

    public function setExperimental(bool|FHIRBoolean|null $value): self
    {
        $this->experimental = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPublisher(): ?FHIRString
    {
        return $this->publisher;
    }

    public function setPublisher(string|FHIRString|null $value): self
    {
        $this->publisher = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUseContext(): array
    {
        return $this->useContext;
    }

    public function setUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter($value);

        return $this;
    }

    public function addUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter(array_merge($this->useContext, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getJurisdiction(): array
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter($value);

        return $this;
    }

    public function addJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter(array_merge($this->jurisdiction, $value));

        return $this;
    }

    public function getPurpose(): ?FHIRMarkdown
    {
        return $this->purpose;
    }

    public function setPurpose(string|FHIRMarkdown|null $value): self
    {
        $this->purpose = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getCopyright(): ?FHIRMarkdown
    {
        return $this->copyright;
    }

    public function setCopyright(string|FHIRMarkdown|null $value): self
    {
        $this->copyright = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getKind(): ?FHIRCode
    {
        return $this->kind;
    }

    public function setKind(string|FHIRCode|null $value): self
    {
        $this->kind = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getInstantiates(): array
    {
        return $this->instantiates;
    }

    public function setInstantiates(string|FHIRCanonical|null ...$value): self
    {
        $this->instantiates = [];
        $this->addInstantiates(...$value);

        return $this;
    }

    public function addInstantiates(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->instantiates = array_filter(array_merge($this->instantiates, $values));

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getImports(): array
    {
        return $this->imports;
    }

    public function setImports(string|FHIRCanonical|null ...$value): self
    {
        $this->imports = [];
        $this->addImports(...$value);

        return $this;
    }

    public function addImports(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->imports = array_filter(array_merge($this->imports, $values));

        return $this;
    }

    public function getSoftware(): ?FHIRCapabilityStatementSoftware
    {
        return $this->software;
    }

    public function setSoftware(?FHIRCapabilityStatementSoftware $value): self
    {
        $this->software = $value;

        return $this;
    }

    public function getImplementation(): ?FHIRCapabilityStatementImplementation
    {
        return $this->implementation;
    }

    public function setImplementation(?FHIRCapabilityStatementImplementation $value): self
    {
        $this->implementation = $value;

        return $this;
    }

    public function getFhirVersion(): ?FHIRCode
    {
        return $this->fhirVersion;
    }

    public function setFhirVersion(string|FHIRCode|null $value): self
    {
        $this->fhirVersion = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getFormat(): array
    {
        return $this->format;
    }

    public function setFormat(string|FHIRCode|null ...$value): self
    {
        $this->format = [];
        $this->addFormat(...$value);

        return $this;
    }

    public function addFormat(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->format = array_filter(array_merge($this->format, $values));

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getPatchFormat(): array
    {
        return $this->patchFormat;
    }

    public function setPatchFormat(string|FHIRCode|null ...$value): self
    {
        $this->patchFormat = [];
        $this->addPatchFormat(...$value);

        return $this;
    }

    public function addPatchFormat(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->patchFormat = array_filter(array_merge($this->patchFormat, $values));

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getImplementationGuide(): array
    {
        return $this->implementationGuide;
    }

    public function setImplementationGuide(string|FHIRCanonical|null ...$value): self
    {
        $this->implementationGuide = [];
        $this->addImplementationGuide(...$value);

        return $this;
    }

    public function addImplementationGuide(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->implementationGuide = array_filter(array_merge($this->implementationGuide, $values));

        return $this;
    }

    /**
     * @return FHIRCapabilityStatementRest[]
     */
    public function getRest(): array
    {
        return $this->rest;
    }

    public function setRest(?FHIRCapabilityStatementRest ...$value): self
    {
        $this->rest = array_filter($value);

        return $this;
    }

    public function addRest(?FHIRCapabilityStatementRest ...$value): self
    {
        $this->rest = array_filter(array_merge($this->rest, $value));

        return $this;
    }

    /**
     * @return FHIRCapabilityStatementMessaging[]
     */
    public function getMessaging(): array
    {
        return $this->messaging;
    }

    public function setMessaging(?FHIRCapabilityStatementMessaging ...$value): self
    {
        $this->messaging = array_filter($value);

        return $this;
    }

    public function addMessaging(?FHIRCapabilityStatementMessaging ...$value): self
    {
        $this->messaging = array_filter(array_merge($this->messaging, $value));

        return $this;
    }

    /**
     * @return FHIRCapabilityStatementDocument[]
     */
    public function getDocument(): array
    {
        return $this->document;
    }

    public function setDocument(?FHIRCapabilityStatementDocument ...$value): self
    {
        $this->document = array_filter($value);

        return $this;
    }

    public function addDocument(?FHIRCapabilityStatementDocument ...$value): self
    {
        $this->document = array_filter(array_merge($this->document, $value));

        return $this;
    }
}
