<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AdministrableProductDefinition Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRAdministrableProductDefinitionInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRAdministrableProductDefinitionProperty;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRAdministrableProductDefinitionRouteOfAdministration;

class FHIRAdministrableProductDefinition extends FHIRDomainResource implements FHIRAdministrableProductDefinitionInterface
{
    public const RESOURCE_NAME = 'AdministrableProductDefinition';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;

    /** @var FHIRReference[] */
    protected array $formOf = [];
    protected ?FHIRCodeableConcept $administrableDoseForm = null;
    protected ?FHIRCodeableConcept $unitOfPresentation = null;

    /** @var FHIRReference[] */
    protected array $producedFrom = [];

    /** @var FHIRCodeableConcept[] */
    protected array $ingredient = [];
    protected ?FHIRReference $device = null;

    /** @var FHIRAdministrableProductDefinitionProperty[] */
    protected array $property = [];

    /** @var FHIRAdministrableProductDefinitionRouteOfAdministration[] */
    protected array $routeOfAdministration = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getFormOf(): array
    {
        return $this->formOf;
    }

    public function setFormOf(?FHIRReference ...$value): self
    {
        $this->formOf = array_filter($value);

        return $this;
    }

    public function addFormOf(?FHIRReference ...$value): self
    {
        $this->formOf = array_filter(array_merge($this->formOf, $value));

        return $this;
    }

    public function getAdministrableDoseForm(): ?FHIRCodeableConcept
    {
        return $this->administrableDoseForm;
    }

    public function setAdministrableDoseForm(?FHIRCodeableConcept $value): self
    {
        $this->administrableDoseForm = $value;

        return $this;
    }

    public function getUnitOfPresentation(): ?FHIRCodeableConcept
    {
        return $this->unitOfPresentation;
    }

    public function setUnitOfPresentation(?FHIRCodeableConcept $value): self
    {
        $this->unitOfPresentation = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getProducedFrom(): array
    {
        return $this->producedFrom;
    }

    public function setProducedFrom(?FHIRReference ...$value): self
    {
        $this->producedFrom = array_filter($value);

        return $this;
    }

    public function addProducedFrom(?FHIRReference ...$value): self
    {
        $this->producedFrom = array_filter(array_merge($this->producedFrom, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getIngredient(): array
    {
        return $this->ingredient;
    }

    public function setIngredient(?FHIRCodeableConcept ...$value): self
    {
        $this->ingredient = array_filter($value);

        return $this;
    }

    public function addIngredient(?FHIRCodeableConcept ...$value): self
    {
        $this->ingredient = array_filter(array_merge($this->ingredient, $value));

        return $this;
    }

    public function getDevice(): ?FHIRReference
    {
        return $this->device;
    }

    public function setDevice(?FHIRReference $value): self
    {
        $this->device = $value;

        return $this;
    }

    /**
     * @return FHIRAdministrableProductDefinitionProperty[]
     */
    public function getProperty(): array
    {
        return $this->property;
    }

    public function setProperty(?FHIRAdministrableProductDefinitionProperty ...$value): self
    {
        $this->property = array_filter($value);

        return $this;
    }

    public function addProperty(?FHIRAdministrableProductDefinitionProperty ...$value): self
    {
        $this->property = array_filter(array_merge($this->property, $value));

        return $this;
    }

    /**
     * @return FHIRAdministrableProductDefinitionRouteOfAdministration[]
     */
    public function getRouteOfAdministration(): array
    {
        return $this->routeOfAdministration;
    }

    public function setRouteOfAdministration(?FHIRAdministrableProductDefinitionRouteOfAdministration ...$value): self
    {
        $this->routeOfAdministration = array_filter($value);

        return $this;
    }

    public function addRouteOfAdministration(?FHIRAdministrableProductDefinitionRouteOfAdministration ...$value): self
    {
        $this->routeOfAdministration = array_filter(array_merge($this->routeOfAdministration, $value));

        return $this;
    }
}
