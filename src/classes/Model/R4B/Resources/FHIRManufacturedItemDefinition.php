<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ManufacturedItemDefinition Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRManufacturedItemDefinitionInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRManufacturedItemDefinitionProperty;

class FHIRManufacturedItemDefinition extends FHIRDomainResource implements FHIRManufacturedItemDefinitionInterface
{
    public const RESOURCE_NAME = 'ManufacturedItemDefinition';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $manufacturedDoseForm = null;
    protected ?FHIRCodeableConcept $unitOfPresentation = null;

    /** @var FHIRReference[] */
    protected array $manufacturer = [];

    /** @var FHIRCodeableConcept[] */
    protected array $ingredient = [];

    /** @var FHIRManufacturedItemDefinitionProperty[] */
    protected array $property = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getManufacturedDoseForm(): ?FHIRCodeableConcept
    {
        return $this->manufacturedDoseForm;
    }

    public function setManufacturedDoseForm(?FHIRCodeableConcept $value): self
    {
        $this->manufacturedDoseForm = $value;

        return $this;
    }

    public function getUnitOfPresentation(): ?FHIRCodeableConcept
    {
        return $this->unitOfPresentation;
    }

    public function setUnitOfPresentation(?FHIRCodeableConcept $value): self
    {
        $this->unitOfPresentation = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getManufacturer(): array
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?FHIRReference ...$value): self
    {
        $this->manufacturer = array_filter($value);

        return $this;
    }

    public function addManufacturer(?FHIRReference ...$value): self
    {
        $this->manufacturer = array_filter(array_merge($this->manufacturer, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getIngredient(): array
    {
        return $this->ingredient;
    }

    public function setIngredient(?FHIRCodeableConcept ...$value): self
    {
        $this->ingredient = array_filter($value);

        return $this;
    }

    public function addIngredient(?FHIRCodeableConcept ...$value): self
    {
        $this->ingredient = array_filter(array_merge($this->ingredient, $value));

        return $this;
    }

    /**
     * @return FHIRManufacturedItemDefinitionProperty[]
     */
    public function getProperty(): array
    {
        return $this->property;
    }

    public function setProperty(?FHIRManufacturedItemDefinitionProperty ...$value): self
    {
        $this->property = array_filter($value);

        return $this;
    }

    public function addProperty(?FHIRManufacturedItemDefinitionProperty ...$value): self
    {
        $this->property = array_filter(array_merge($this->property, $value));

        return $this;
    }
}
