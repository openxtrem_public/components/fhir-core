<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceDefinition Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRDeviceDefinitionInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRProdCharacteristic;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRProductShelfLife;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRDeviceDefinitionCapability;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRDeviceDefinitionDeviceName;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRDeviceDefinitionMaterial;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRDeviceDefinitionProperty;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRDeviceDefinitionSpecialization;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRDeviceDefinitionUdiDeviceIdentifier;

class FHIRDeviceDefinition extends FHIRDomainResource implements FHIRDeviceDefinitionInterface
{
    public const RESOURCE_NAME = 'DeviceDefinition';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRDeviceDefinitionUdiDeviceIdentifier[] */
    protected array $udiDeviceIdentifier = [];
    protected FHIRString|FHIRReference|null $manufacturer = null;

    /** @var FHIRDeviceDefinitionDeviceName[] */
    protected array $deviceName = [];
    protected ?FHIRString $modelNumber = null;
    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRDeviceDefinitionSpecialization[] */
    protected array $specialization = [];

    /** @var FHIRString[] */
    protected array $version = [];

    /** @var FHIRCodeableConcept[] */
    protected array $safety = [];

    /** @var FHIRProductShelfLife[] */
    protected array $shelfLifeStorage = [];
    protected ?FHIRProdCharacteristic $physicalCharacteristics = null;

    /** @var FHIRCodeableConcept[] */
    protected array $languageCode = [];

    /** @var FHIRDeviceDefinitionCapability[] */
    protected array $capability = [];

    /** @var FHIRDeviceDefinitionProperty[] */
    protected array $property = [];
    protected ?FHIRReference $owner = null;

    /** @var FHIRContactPoint[] */
    protected array $contact = [];
    protected ?FHIRUri $url = null;
    protected ?FHIRUri $onlineInformation = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];
    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRReference $parentDevice = null;

    /** @var FHIRDeviceDefinitionMaterial[] */
    protected array $material = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRDeviceDefinitionUdiDeviceIdentifier[]
     */
    public function getUdiDeviceIdentifier(): array
    {
        return $this->udiDeviceIdentifier;
    }

    public function setUdiDeviceIdentifier(?FHIRDeviceDefinitionUdiDeviceIdentifier ...$value): self
    {
        $this->udiDeviceIdentifier = array_filter($value);

        return $this;
    }

    public function addUdiDeviceIdentifier(?FHIRDeviceDefinitionUdiDeviceIdentifier ...$value): self
    {
        $this->udiDeviceIdentifier = array_filter(array_merge($this->udiDeviceIdentifier, $value));

        return $this;
    }

    public function getManufacturer(): FHIRString|FHIRReference|null
    {
        return $this->manufacturer;
    }

    public function setManufacturer(FHIRString|FHIRReference|null $value): self
    {
        $this->manufacturer = $value;

        return $this;
    }

    /**
     * @return FHIRDeviceDefinitionDeviceName[]
     */
    public function getDeviceName(): array
    {
        return $this->deviceName;
    }

    public function setDeviceName(?FHIRDeviceDefinitionDeviceName ...$value): self
    {
        $this->deviceName = array_filter($value);

        return $this;
    }

    public function addDeviceName(?FHIRDeviceDefinitionDeviceName ...$value): self
    {
        $this->deviceName = array_filter(array_merge($this->deviceName, $value));

        return $this;
    }

    public function getModelNumber(): ?FHIRString
    {
        return $this->modelNumber;
    }

    public function setModelNumber(string|FHIRString|null $value): self
    {
        $this->modelNumber = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRDeviceDefinitionSpecialization[]
     */
    public function getSpecialization(): array
    {
        return $this->specialization;
    }

    public function setSpecialization(?FHIRDeviceDefinitionSpecialization ...$value): self
    {
        $this->specialization = array_filter($value);

        return $this;
    }

    public function addSpecialization(?FHIRDeviceDefinitionSpecialization ...$value): self
    {
        $this->specialization = array_filter(array_merge($this->specialization, $value));

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getVersion(): array
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null ...$value): self
    {
        $this->version = [];
        $this->addVersion(...$value);

        return $this;
    }

    public function addVersion(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->version = array_filter(array_merge($this->version, $values));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSafety(): array
    {
        return $this->safety;
    }

    public function setSafety(?FHIRCodeableConcept ...$value): self
    {
        $this->safety = array_filter($value);

        return $this;
    }

    public function addSafety(?FHIRCodeableConcept ...$value): self
    {
        $this->safety = array_filter(array_merge($this->safety, $value));

        return $this;
    }

    /**
     * @return FHIRProductShelfLife[]
     */
    public function getShelfLifeStorage(): array
    {
        return $this->shelfLifeStorage;
    }

    public function setShelfLifeStorage(?FHIRProductShelfLife ...$value): self
    {
        $this->shelfLifeStorage = array_filter($value);

        return $this;
    }

    public function addShelfLifeStorage(?FHIRProductShelfLife ...$value): self
    {
        $this->shelfLifeStorage = array_filter(array_merge($this->shelfLifeStorage, $value));

        return $this;
    }

    public function getPhysicalCharacteristics(): ?FHIRProdCharacteristic
    {
        return $this->physicalCharacteristics;
    }

    public function setPhysicalCharacteristics(?FHIRProdCharacteristic $value): self
    {
        $this->physicalCharacteristics = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getLanguageCode(): array
    {
        return $this->languageCode;
    }

    public function setLanguageCode(?FHIRCodeableConcept ...$value): self
    {
        $this->languageCode = array_filter($value);

        return $this;
    }

    public function addLanguageCode(?FHIRCodeableConcept ...$value): self
    {
        $this->languageCode = array_filter(array_merge($this->languageCode, $value));

        return $this;
    }

    /**
     * @return FHIRDeviceDefinitionCapability[]
     */
    public function getCapability(): array
    {
        return $this->capability;
    }

    public function setCapability(?FHIRDeviceDefinitionCapability ...$value): self
    {
        $this->capability = array_filter($value);

        return $this;
    }

    public function addCapability(?FHIRDeviceDefinitionCapability ...$value): self
    {
        $this->capability = array_filter(array_merge($this->capability, $value));

        return $this;
    }

    /**
     * @return FHIRDeviceDefinitionProperty[]
     */
    public function getProperty(): array
    {
        return $this->property;
    }

    public function setProperty(?FHIRDeviceDefinitionProperty ...$value): self
    {
        $this->property = array_filter($value);

        return $this;
    }

    public function addProperty(?FHIRDeviceDefinitionProperty ...$value): self
    {
        $this->property = array_filter(array_merge($this->property, $value));

        return $this;
    }

    public function getOwner(): ?FHIRReference
    {
        return $this->owner;
    }

    public function setOwner(?FHIRReference $value): self
    {
        $this->owner = $value;

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactPoint ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactPoint ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getOnlineInformation(): ?FHIRUri
    {
        return $this->onlineInformation;
    }

    public function setOnlineInformation(string|FHIRUri|null $value): self
    {
        $this->onlineInformation = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getParentDevice(): ?FHIRReference
    {
        return $this->parentDevice;
    }

    public function setParentDevice(?FHIRReference $value): self
    {
        $this->parentDevice = $value;

        return $this;
    }

    /**
     * @return FHIRDeviceDefinitionMaterial[]
     */
    public function getMaterial(): array
    {
        return $this->material;
    }

    public function setMaterial(?FHIRDeviceDefinitionMaterial ...$value): self
    {
        $this->material = array_filter($value);

        return $this;
    }

    public function addMaterial(?FHIRDeviceDefinitionMaterial ...$value): self
    {
        $this->material = array_filter(array_merge($this->material, $value));

        return $this;
    }
}
