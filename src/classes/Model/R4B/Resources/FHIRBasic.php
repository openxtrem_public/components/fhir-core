<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Basic Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRBasicInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDate;

class FHIRBasic extends FHIRDomainResource implements FHIRBasicInterface
{
    public const RESOURCE_NAME = 'Basic';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRDate $created = null;
    protected ?FHIRReference $author = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getCreated(): ?FHIRDate
    {
        return $this->created;
    }

    public function setCreated(string|FHIRDate|null $value): self
    {
        $this->created = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getAuthor(): ?FHIRReference
    {
        return $this->author;
    }

    public function setAuthor(?FHIRReference $value): self
    {
        $this->author = $value;

        return $this;
    }
}
