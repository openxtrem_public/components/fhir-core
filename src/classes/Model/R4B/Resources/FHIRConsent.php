<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Consent Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRConsentInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRConsentPolicy;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRConsentProvision;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRConsentVerification;

class FHIRConsent extends FHIRDomainResource implements FHIRConsentInterface
{
    public const RESOURCE_NAME = 'Consent';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $scope = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRReference $patient = null;
    protected ?FHIRDateTime $dateTime = null;

    /** @var FHIRReference[] */
    protected array $performer = [];

    /** @var FHIRReference[] */
    protected array $organization = [];
    protected FHIRAttachment|FHIRReference|null $source = null;

    /** @var FHIRConsentPolicy[] */
    protected array $policy = [];
    protected ?FHIRCodeableConcept $policyRule = null;

    /** @var FHIRConsentVerification[] */
    protected array $verification = [];
    protected ?FHIRConsentProvision $provision = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getScope(): ?FHIRCodeableConcept
    {
        return $this->scope;
    }

    public function setScope(?FHIRCodeableConcept $value): self
    {
        $this->scope = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getDateTime(): ?FHIRDateTime
    {
        return $this->dateTime;
    }

    public function setDateTime(string|FHIRDateTime|null $value): self
    {
        $this->dateTime = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPerformer(): array
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRReference ...$value): self
    {
        $this->performer = array_filter($value);

        return $this;
    }

    public function addPerformer(?FHIRReference ...$value): self
    {
        $this->performer = array_filter(array_merge($this->performer, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getOrganization(): array
    {
        return $this->organization;
    }

    public function setOrganization(?FHIRReference ...$value): self
    {
        $this->organization = array_filter($value);

        return $this;
    }

    public function addOrganization(?FHIRReference ...$value): self
    {
        $this->organization = array_filter(array_merge($this->organization, $value));

        return $this;
    }

    public function getSource(): FHIRAttachment|FHIRReference|null
    {
        return $this->source;
    }

    public function setSource(FHIRAttachment|FHIRReference|null $value): self
    {
        $this->source = $value;

        return $this;
    }

    /**
     * @return FHIRConsentPolicy[]
     */
    public function getPolicy(): array
    {
        return $this->policy;
    }

    public function setPolicy(?FHIRConsentPolicy ...$value): self
    {
        $this->policy = array_filter($value);

        return $this;
    }

    public function addPolicy(?FHIRConsentPolicy ...$value): self
    {
        $this->policy = array_filter(array_merge($this->policy, $value));

        return $this;
    }

    public function getPolicyRule(): ?FHIRCodeableConcept
    {
        return $this->policyRule;
    }

    public function setPolicyRule(?FHIRCodeableConcept $value): self
    {
        $this->policyRule = $value;

        return $this;
    }

    /**
     * @return FHIRConsentVerification[]
     */
    public function getVerification(): array
    {
        return $this->verification;
    }

    public function setVerification(?FHIRConsentVerification ...$value): self
    {
        $this->verification = array_filter($value);

        return $this;
    }

    public function addVerification(?FHIRConsentVerification ...$value): self
    {
        $this->verification = array_filter(array_merge($this->verification, $value));

        return $this;
    }

    public function getProvision(): ?FHIRConsentProvision
    {
        return $this->provision;
    }

    public function setProvision(?FHIRConsentProvision $value): self
    {
        $this->provision = $value;

        return $this;
    }
}
