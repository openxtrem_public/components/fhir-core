<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Patient Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRPatientInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRAddress;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRHumanName;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRPatientCommunication;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRPatientContact;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRPatientLink;

class FHIRPatient extends FHIRDomainResource implements FHIRPatientInterface
{
    public const RESOURCE_NAME = 'Patient';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRBoolean $active = null;

    /** @var FHIRHumanName[] */
    protected array $name = [];

    /** @var FHIRContactPoint[] */
    protected array $telecom = [];
    protected ?FHIRCode $gender = null;
    protected ?FHIRDate $birthDate = null;
    protected FHIRBoolean|FHIRDateTime|null $deceased = null;

    /** @var FHIRAddress[] */
    protected array $address = [];
    protected ?FHIRCodeableConcept $maritalStatus = null;
    protected FHIRBoolean|FHIRInteger|null $multipleBirth = null;

    /** @var FHIRAttachment[] */
    protected array $photo = [];

    /** @var FHIRPatientContact[] */
    protected array $contact = [];

    /** @var FHIRPatientCommunication[] */
    protected array $communication = [];

    /** @var FHIRReference[] */
    protected array $generalPractitioner = [];
    protected ?FHIRReference $managingOrganization = null;

    /** @var FHIRPatientLink[] */
    protected array $link = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getActive(): ?FHIRBoolean
    {
        return $this->active;
    }

    public function setActive(bool|FHIRBoolean|null $value): self
    {
        $this->active = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRHumanName[]
     */
    public function getName(): array
    {
        return $this->name;
    }

    public function setName(?FHIRHumanName ...$value): self
    {
        $this->name = array_filter($value);

        return $this;
    }

    public function addName(?FHIRHumanName ...$value): self
    {
        $this->name = array_filter(array_merge($this->name, $value));

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getTelecom(): array
    {
        return $this->telecom;
    }

    public function setTelecom(?FHIRContactPoint ...$value): self
    {
        $this->telecom = array_filter($value);

        return $this;
    }

    public function addTelecom(?FHIRContactPoint ...$value): self
    {
        $this->telecom = array_filter(array_merge($this->telecom, $value));

        return $this;
    }

    public function getGender(): ?FHIRCode
    {
        return $this->gender;
    }

    public function setGender(string|FHIRCode|null $value): self
    {
        $this->gender = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getBirthDate(): ?FHIRDate
    {
        return $this->birthDate;
    }

    public function setBirthDate(string|FHIRDate|null $value): self
    {
        $this->birthDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getDeceased(): FHIRBoolean|FHIRDateTime|null
    {
        return $this->deceased;
    }

    public function setDeceased(FHIRBoolean|FHIRDateTime|null $value): self
    {
        $this->deceased = $value;

        return $this;
    }

    /**
     * @return FHIRAddress[]
     */
    public function getAddress(): array
    {
        return $this->address;
    }

    public function setAddress(?FHIRAddress ...$value): self
    {
        $this->address = array_filter($value);

        return $this;
    }

    public function addAddress(?FHIRAddress ...$value): self
    {
        $this->address = array_filter(array_merge($this->address, $value));

        return $this;
    }

    public function getMaritalStatus(): ?FHIRCodeableConcept
    {
        return $this->maritalStatus;
    }

    public function setMaritalStatus(?FHIRCodeableConcept $value): self
    {
        $this->maritalStatus = $value;

        return $this;
    }

    public function getMultipleBirth(): FHIRBoolean|FHIRInteger|null
    {
        return $this->multipleBirth;
    }

    public function setMultipleBirth(FHIRBoolean|FHIRInteger|null $value): self
    {
        $this->multipleBirth = $value;

        return $this;
    }

    /**
     * @return FHIRAttachment[]
     */
    public function getPhoto(): array
    {
        return $this->photo;
    }

    public function setPhoto(?FHIRAttachment ...$value): self
    {
        $this->photo = array_filter($value);

        return $this;
    }

    public function addPhoto(?FHIRAttachment ...$value): self
    {
        $this->photo = array_filter(array_merge($this->photo, $value));

        return $this;
    }

    /**
     * @return FHIRPatientContact[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRPatientContact ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRPatientContact ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    /**
     * @return FHIRPatientCommunication[]
     */
    public function getCommunication(): array
    {
        return $this->communication;
    }

    public function setCommunication(?FHIRPatientCommunication ...$value): self
    {
        $this->communication = array_filter($value);

        return $this;
    }

    public function addCommunication(?FHIRPatientCommunication ...$value): self
    {
        $this->communication = array_filter(array_merge($this->communication, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getGeneralPractitioner(): array
    {
        return $this->generalPractitioner;
    }

    public function setGeneralPractitioner(?FHIRReference ...$value): self
    {
        $this->generalPractitioner = array_filter($value);

        return $this;
    }

    public function addGeneralPractitioner(?FHIRReference ...$value): self
    {
        $this->generalPractitioner = array_filter(array_merge($this->generalPractitioner, $value));

        return $this;
    }

    public function getManagingOrganization(): ?FHIRReference
    {
        return $this->managingOrganization;
    }

    public function setManagingOrganization(?FHIRReference $value): self
    {
        $this->managingOrganization = $value;

        return $this;
    }

    /**
     * @return FHIRPatientLink[]
     */
    public function getLink(): array
    {
        return $this->link;
    }

    public function setLink(?FHIRPatientLink ...$value): self
    {
        $this->link = array_filter($value);

        return $this;
    }

    public function addLink(?FHIRPatientLink ...$value): self
    {
        $this->link = array_filter(array_merge($this->link, $value));

        return $this;
    }
}
