<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ResearchSubject Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRResearchSubjectInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRResearchSubject extends FHIRDomainResource implements FHIRResearchSubjectInterface
{
    public const RESOURCE_NAME = 'ResearchSubject';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRPeriod $period = null;
    protected ?FHIRReference $study = null;
    protected ?FHIRReference $individual = null;
    protected ?FHIRString $assignedArm = null;
    protected ?FHIRString $actualArm = null;
    protected ?FHIRReference $consent = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    public function getStudy(): ?FHIRReference
    {
        return $this->study;
    }

    public function setStudy(?FHIRReference $value): self
    {
        $this->study = $value;

        return $this;
    }

    public function getIndividual(): ?FHIRReference
    {
        return $this->individual;
    }

    public function setIndividual(?FHIRReference $value): self
    {
        $this->individual = $value;

        return $this;
    }

    public function getAssignedArm(): ?FHIRString
    {
        return $this->assignedArm;
    }

    public function setAssignedArm(string|FHIRString|null $value): self
    {
        $this->assignedArm = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getActualArm(): ?FHIRString
    {
        return $this->actualArm;
    }

    public function setActualArm(string|FHIRString|null $value): self
    {
        $this->actualArm = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getConsent(): ?FHIRReference
    {
        return $this->consent;
    }

    public function setConsent(?FHIRReference $value): self
    {
        $this->consent = $value;

        return $this;
    }
}
