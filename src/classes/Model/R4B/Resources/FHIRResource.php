<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Resource Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRResourceInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRMeta;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R4B\StructureDefinition;
use Ox\Components\FHIRCore\Definition\ResourceDefinition;

abstract class FHIRResource implements FHIRResourceInterface
{
    public const RESOURCE_NAME = 'Resource';

    protected ?FHIRString $id = null;
    protected ?FHIRMeta $meta = null;
    protected ?FHIRUri $implicitRules = null;
    protected ?FHIRCode $language = null;
    protected ResourceDefinition $fhir_definition;

    public function getId(): ?FHIRString
    {
        return $this->id;
    }

    public function setId(string|FHIRString|null $value): self
    {
        $this->id = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getMeta(): ?FHIRMeta
    {
        return $this->meta;
    }

    public function setMeta(?FHIRMeta $value): self
    {
        $this->meta = $value;

        return $this;
    }

    public function getImplicitRules(): ?FHIRUri
    {
        return $this->implicitRules;
    }

    public function setImplicitRules(string|FHIRUri|null $value): self
    {
        $this->implicitRules = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getLanguage(): ?FHIRCode
    {
        return $this->language;
    }

    public function setLanguage(string|FHIRCode|null $value): self
    {
        $this->language = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public static function isArrayNull(array $array): bool
    {
        foreach ($array as $item) {
            if (!$item->isNull()) {
                return false;
            }
        }

        return true;
    }

    public function isNull(): bool
    {
        foreach ($this->fhir_definition->getFields() as $field) {
            $gotValue = $this->{"get" . ucfirst($field->getLabel())}();
            if ($field->isUnique()) {
                if (!is_null($gotValue) && (!is_object($gotValue) || !$gotValue->isNull())) {
                    return false;
                }
            } elseif (!self::isArrayNull($gotValue)) {
                return false;
            }
        }

        return true;
    }

    public function __construct()
    {
        $this->fhir_definition = StructureDefinition::getResourceDefinition($this);
    }

    public function getFhirResourceDefinition(): ResourceDefinition
    {
        return $this->fhir_definition;
    }

    public static function getFhirStructureDefinition(): string
    {
        return StructureDefinition::class;
    }
}
