<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR RelatedPerson Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRRelatedPersonInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRAddress;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRHumanName;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRRelatedPersonCommunication;

class FHIRRelatedPerson extends FHIRDomainResource implements FHIRRelatedPersonInterface
{
    public const RESOURCE_NAME = 'RelatedPerson';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRBoolean $active = null;
    protected ?FHIRReference $patient = null;

    /** @var FHIRCodeableConcept[] */
    protected array $relationship = [];

    /** @var FHIRHumanName[] */
    protected array $name = [];

    /** @var FHIRContactPoint[] */
    protected array $telecom = [];
    protected ?FHIRCode $gender = null;
    protected ?FHIRDate $birthDate = null;

    /** @var FHIRAddress[] */
    protected array $address = [];

    /** @var FHIRAttachment[] */
    protected array $photo = [];
    protected ?FHIRPeriod $period = null;

    /** @var FHIRRelatedPersonCommunication[] */
    protected array $communication = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getActive(): ?FHIRBoolean
    {
        return $this->active;
    }

    public function setActive(bool|FHIRBoolean|null $value): self
    {
        $this->active = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getRelationship(): array
    {
        return $this->relationship;
    }

    public function setRelationship(?FHIRCodeableConcept ...$value): self
    {
        $this->relationship = array_filter($value);

        return $this;
    }

    public function addRelationship(?FHIRCodeableConcept ...$value): self
    {
        $this->relationship = array_filter(array_merge($this->relationship, $value));

        return $this;
    }

    /**
     * @return FHIRHumanName[]
     */
    public function getName(): array
    {
        return $this->name;
    }

    public function setName(?FHIRHumanName ...$value): self
    {
        $this->name = array_filter($value);

        return $this;
    }

    public function addName(?FHIRHumanName ...$value): self
    {
        $this->name = array_filter(array_merge($this->name, $value));

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getTelecom(): array
    {
        return $this->telecom;
    }

    public function setTelecom(?FHIRContactPoint ...$value): self
    {
        $this->telecom = array_filter($value);

        return $this;
    }

    public function addTelecom(?FHIRContactPoint ...$value): self
    {
        $this->telecom = array_filter(array_merge($this->telecom, $value));

        return $this;
    }

    public function getGender(): ?FHIRCode
    {
        return $this->gender;
    }

    public function setGender(string|FHIRCode|null $value): self
    {
        $this->gender = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getBirthDate(): ?FHIRDate
    {
        return $this->birthDate;
    }

    public function setBirthDate(string|FHIRDate|null $value): self
    {
        $this->birthDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAddress[]
     */
    public function getAddress(): array
    {
        return $this->address;
    }

    public function setAddress(?FHIRAddress ...$value): self
    {
        $this->address = array_filter($value);

        return $this;
    }

    public function addAddress(?FHIRAddress ...$value): self
    {
        $this->address = array_filter(array_merge($this->address, $value));

        return $this;
    }

    /**
     * @return FHIRAttachment[]
     */
    public function getPhoto(): array
    {
        return $this->photo;
    }

    public function setPhoto(?FHIRAttachment ...$value): self
    {
        $this->photo = array_filter($value);

        return $this;
    }

    public function addPhoto(?FHIRAttachment ...$value): self
    {
        $this->photo = array_filter(array_merge($this->photo, $value));

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    /**
     * @return FHIRRelatedPersonCommunication[]
     */
    public function getCommunication(): array
    {
        return $this->communication;
    }

    public function setCommunication(?FHIRRelatedPersonCommunication ...$value): self
    {
        $this->communication = array_filter($value);

        return $this;
    }

    public function addCommunication(?FHIRRelatedPersonCommunication ...$value): self
    {
        $this->communication = array_filter(array_merge($this->communication, $value));

        return $this;
    }
}
