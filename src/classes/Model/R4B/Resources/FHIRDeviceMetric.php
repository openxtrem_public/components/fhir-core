<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceMetric Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRDeviceMetricInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRDeviceMetricCalibration;

class FHIRDeviceMetric extends FHIRDomainResource implements FHIRDeviceMetricInterface
{
    public const RESOURCE_NAME = 'DeviceMetric';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableConcept $unit = null;
    protected ?FHIRReference $source = null;
    protected ?FHIRReference $parent = null;
    protected ?FHIRCode $operationalStatus = null;
    protected ?FHIRCode $color = null;
    protected ?FHIRCode $category = null;
    protected ?FHIRTiming $measurementPeriod = null;

    /** @var FHIRDeviceMetricCalibration[] */
    protected array $calibration = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getUnit(): ?FHIRCodeableConcept
    {
        return $this->unit;
    }

    public function setUnit(?FHIRCodeableConcept $value): self
    {
        $this->unit = $value;

        return $this;
    }

    public function getSource(): ?FHIRReference
    {
        return $this->source;
    }

    public function setSource(?FHIRReference $value): self
    {
        $this->source = $value;

        return $this;
    }

    public function getParent(): ?FHIRReference
    {
        return $this->parent;
    }

    public function setParent(?FHIRReference $value): self
    {
        $this->parent = $value;

        return $this;
    }

    public function getOperationalStatus(): ?FHIRCode
    {
        return $this->operationalStatus;
    }

    public function setOperationalStatus(string|FHIRCode|null $value): self
    {
        $this->operationalStatus = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getColor(): ?FHIRCode
    {
        return $this->color;
    }

    public function setColor(string|FHIRCode|null $value): self
    {
        $this->color = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCategory(): ?FHIRCode
    {
        return $this->category;
    }

    public function setCategory(string|FHIRCode|null $value): self
    {
        $this->category = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getMeasurementPeriod(): ?FHIRTiming
    {
        return $this->measurementPeriod;
    }

    public function setMeasurementPeriod(?FHIRTiming $value): self
    {
        $this->measurementPeriod = $value;

        return $this;
    }

    /**
     * @return FHIRDeviceMetricCalibration[]
     */
    public function getCalibration(): array
    {
        return $this->calibration;
    }

    public function setCalibration(?FHIRDeviceMetricCalibration ...$value): self
    {
        $this->calibration = array_filter($value);

        return $this;
    }

    public function addCalibration(?FHIRDeviceMetricCalibration ...$value): self
    {
        $this->calibration = array_filter(array_merge($this->calibration, $value));

        return $this;
    }
}
