<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MessageDefinition Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMessageDefinitionInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRMessageDefinitionAllowedResponse;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRMessageDefinitionFocus;

class FHIRMessageDefinition extends FHIRDomainResource implements FHIRMessageDefinitionInterface
{
    public const RESOURCE_NAME = 'MessageDefinition';

    protected ?FHIRUri $url = null;

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRString $version = null;
    protected ?FHIRString $name = null;
    protected ?FHIRString $title = null;

    /** @var FHIRCanonical[] */
    protected array $replaces = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRBoolean $experimental = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRString $publisher = null;

    /** @var FHIRContactDetail[] */
    protected array $contact = [];
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRUsageContext[] */
    protected array $useContext = [];

    /** @var FHIRCodeableConcept[] */
    protected array $jurisdiction = [];
    protected ?FHIRMarkdown $purpose = null;
    protected ?FHIRMarkdown $copyright = null;
    protected ?FHIRCanonical $base = null;

    /** @var FHIRCanonical[] */
    protected array $parent = [];
    protected FHIRCoding|FHIRUri|null $event = null;
    protected ?FHIRCode $category = null;

    /** @var FHIRMessageDefinitionFocus[] */
    protected array $focus = [];
    protected ?FHIRCode $responseRequired = null;

    /** @var FHIRMessageDefinitionAllowedResponse[] */
    protected array $allowedResponse = [];

    /** @var FHIRCanonical[] */
    protected array $graph = [];

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getReplaces(): array
    {
        return $this->replaces;
    }

    public function setReplaces(string|FHIRCanonical|null ...$value): self
    {
        $this->replaces = [];
        $this->addReplaces(...$value);

        return $this;
    }

    public function addReplaces(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->replaces = array_filter(array_merge($this->replaces, $values));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getExperimental(): ?FHIRBoolean
    {
        return $this->experimental;
    }

    public function setExperimental(bool|FHIRBoolean|null $value): self
    {
        $this->experimental = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPublisher(): ?FHIRString
    {
        return $this->publisher;
    }

    public function setPublisher(string|FHIRString|null $value): self
    {
        $this->publisher = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUseContext(): array
    {
        return $this->useContext;
    }

    public function setUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter($value);

        return $this;
    }

    public function addUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter(array_merge($this->useContext, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getJurisdiction(): array
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter($value);

        return $this;
    }

    public function addJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter(array_merge($this->jurisdiction, $value));

        return $this;
    }

    public function getPurpose(): ?FHIRMarkdown
    {
        return $this->purpose;
    }

    public function setPurpose(string|FHIRMarkdown|null $value): self
    {
        $this->purpose = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getCopyright(): ?FHIRMarkdown
    {
        return $this->copyright;
    }

    public function setCopyright(string|FHIRMarkdown|null $value): self
    {
        $this->copyright = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getBase(): ?FHIRCanonical
    {
        return $this->base;
    }

    public function setBase(string|FHIRCanonical|null $value): self
    {
        $this->base = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getParent(): array
    {
        return $this->parent;
    }

    public function setParent(string|FHIRCanonical|null ...$value): self
    {
        $this->parent = [];
        $this->addParent(...$value);

        return $this;
    }

    public function addParent(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->parent = array_filter(array_merge($this->parent, $values));

        return $this;
    }

    public function getEvent(): FHIRCoding|FHIRUri|null
    {
        return $this->event;
    }

    public function setEvent(FHIRCoding|FHIRUri|null $value): self
    {
        $this->event = $value;

        return $this;
    }

    public function getCategory(): ?FHIRCode
    {
        return $this->category;
    }

    public function setCategory(string|FHIRCode|null $value): self
    {
        $this->category = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRMessageDefinitionFocus[]
     */
    public function getFocus(): array
    {
        return $this->focus;
    }

    public function setFocus(?FHIRMessageDefinitionFocus ...$value): self
    {
        $this->focus = array_filter($value);

        return $this;
    }

    public function addFocus(?FHIRMessageDefinitionFocus ...$value): self
    {
        $this->focus = array_filter(array_merge($this->focus, $value));

        return $this;
    }

    public function getResponseRequired(): ?FHIRCode
    {
        return $this->responseRequired;
    }

    public function setResponseRequired(string|FHIRCode|null $value): self
    {
        $this->responseRequired = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRMessageDefinitionAllowedResponse[]
     */
    public function getAllowedResponse(): array
    {
        return $this->allowedResponse;
    }

    public function setAllowedResponse(?FHIRMessageDefinitionAllowedResponse ...$value): self
    {
        $this->allowedResponse = array_filter($value);

        return $this;
    }

    public function addAllowedResponse(?FHIRMessageDefinitionAllowedResponse ...$value): self
    {
        $this->allowedResponse = array_filter(array_merge($this->allowedResponse, $value));

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getGraph(): array
    {
        return $this->graph;
    }

    public function setGraph(string|FHIRCanonical|null ...$value): self
    {
        $this->graph = [];
        $this->addGraph(...$value);

        return $this;
    }

    public function addGraph(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->graph = array_filter(array_merge($this->graph, $values));

        return $this;
    }
}
