<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClinicalUseDefinition Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRClinicalUseDefinitionInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRClinicalUseDefinitionContraindication;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRClinicalUseDefinitionIndication;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRClinicalUseDefinitionInteraction;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRClinicalUseDefinitionUndesirableEffect;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRClinicalUseDefinitionWarning;

class FHIRClinicalUseDefinition extends FHIRDomainResource implements FHIRClinicalUseDefinitionInterface
{
    public const RESOURCE_NAME = 'ClinicalUseDefinition';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $type = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];

    /** @var FHIRReference[] */
    protected array $subject = [];
    protected ?FHIRCodeableConcept $status = null;
    protected ?FHIRClinicalUseDefinitionContraindication $contraindication = null;
    protected ?FHIRClinicalUseDefinitionIndication $indication = null;
    protected ?FHIRClinicalUseDefinitionInteraction $interaction = null;

    /** @var FHIRReference[] */
    protected array $population = [];
    protected ?FHIRClinicalUseDefinitionUndesirableEffect $undesirableEffect = null;
    protected ?FHIRClinicalUseDefinitionWarning $warning = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSubject(): array
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference ...$value): self
    {
        $this->subject = array_filter($value);

        return $this;
    }

    public function addSubject(?FHIRReference ...$value): self
    {
        $this->subject = array_filter(array_merge($this->subject, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCodeableConcept
    {
        return $this->status;
    }

    public function setStatus(?FHIRCodeableConcept $value): self
    {
        $this->status = $value;

        return $this;
    }

    public function getContraindication(): ?FHIRClinicalUseDefinitionContraindication
    {
        return $this->contraindication;
    }

    public function setContraindication(?FHIRClinicalUseDefinitionContraindication $value): self
    {
        $this->contraindication = $value;

        return $this;
    }

    public function getIndication(): ?FHIRClinicalUseDefinitionIndication
    {
        return $this->indication;
    }

    public function setIndication(?FHIRClinicalUseDefinitionIndication $value): self
    {
        $this->indication = $value;

        return $this;
    }

    public function getInteraction(): ?FHIRClinicalUseDefinitionInteraction
    {
        return $this->interaction;
    }

    public function setInteraction(?FHIRClinicalUseDefinitionInteraction $value): self
    {
        $this->interaction = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPopulation(): array
    {
        return $this->population;
    }

    public function setPopulation(?FHIRReference ...$value): self
    {
        $this->population = array_filter($value);

        return $this;
    }

    public function addPopulation(?FHIRReference ...$value): self
    {
        $this->population = array_filter(array_merge($this->population, $value));

        return $this;
    }

    public function getUndesirableEffect(): ?FHIRClinicalUseDefinitionUndesirableEffect
    {
        return $this->undesirableEffect;
    }

    public function setUndesirableEffect(?FHIRClinicalUseDefinitionUndesirableEffect $value): self
    {
        $this->undesirableEffect = $value;

        return $this;
    }

    public function getWarning(): ?FHIRClinicalUseDefinitionWarning
    {
        return $this->warning;
    }

    public function setWarning(?FHIRClinicalUseDefinitionWarning $value): self
    {
        $this->warning = $value;

        return $this;
    }
}
