<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DocumentManifest Resource
 */

namespace Ox\Components\FHIRCore\Model\R4B\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRDocumentManifestInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R4B\Resources\Backbone\FHIRDocumentManifestRelated;

class FHIRDocumentManifest extends FHIRDomainResource implements FHIRDocumentManifestInterface
{
    public const RESOURCE_NAME = 'DocumentManifest';

    protected ?FHIRIdentifier $masterIdentifier = null;

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRDateTime $created = null;

    /** @var FHIRReference[] */
    protected array $author = [];

    /** @var FHIRReference[] */
    protected array $recipient = [];
    protected ?FHIRUri $source = null;
    protected ?FHIRString $description = null;

    /** @var FHIRReference[] */
    protected array $content = [];

    /** @var FHIRDocumentManifestRelated[] */
    protected array $related = [];

    public function getMasterIdentifier(): ?FHIRIdentifier
    {
        return $this->masterIdentifier;
    }

    public function setMasterIdentifier(?FHIRIdentifier $value): self
    {
        $this->masterIdentifier = $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getCreated(): ?FHIRDateTime
    {
        return $this->created;
    }

    public function setCreated(string|FHIRDateTime|null $value): self
    {
        $this->created = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAuthor(): array
    {
        return $this->author;
    }

    public function setAuthor(?FHIRReference ...$value): self
    {
        $this->author = array_filter($value);

        return $this;
    }

    public function addAuthor(?FHIRReference ...$value): self
    {
        $this->author = array_filter(array_merge($this->author, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getRecipient(): array
    {
        return $this->recipient;
    }

    public function setRecipient(?FHIRReference ...$value): self
    {
        $this->recipient = array_filter($value);

        return $this;
    }

    public function addRecipient(?FHIRReference ...$value): self
    {
        $this->recipient = array_filter(array_merge($this->recipient, $value));

        return $this;
    }

    public function getSource(): ?FHIRUri
    {
        return $this->source;
    }

    public function setSource(string|FHIRUri|null $value): self
    {
        $this->source = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getContent(): array
    {
        return $this->content;
    }

    public function setContent(?FHIRReference ...$value): self
    {
        $this->content = array_filter($value);

        return $this;
    }

    public function addContent(?FHIRReference ...$value): self
    {
        $this->content = array_filter(array_merge($this->content, $value));

        return $this;
    }

    /**
     * @return FHIRDocumentManifestRelated[]
     */
    public function getRelated(): array
    {
        return $this->related;
    }

    public function setRelated(?FHIRDocumentManifestRelated ...$value): self
    {
        $this->related = array_filter($value);

        return $this;
    }

    public function addRelated(?FHIRDocumentManifestRelated ...$value): self
    {
        $this->related = array_filter(array_merge($this->related, $value));

        return $this;
    }
}
