<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR string Primitive
 */

namespace Ox\Components\FHIRCore\Model\R4B\Datatypes;

use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRStringInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRElement;

class FHIRString extends FHIRElement implements FHIRStringInterface
{
    public const RESOURCE_NAME = 'string';

    protected ?string $value = null;

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
