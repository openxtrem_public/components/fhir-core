<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR RelatedArtifact Complex
 */

namespace Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRRelatedArtifactInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUrl;

class FHIRRelatedArtifact extends FHIRElement implements FHIRRelatedArtifactInterface
{
    public const RESOURCE_NAME = 'RelatedArtifact';

    protected ?FHIRCode $type = null;
    protected ?FHIRString $label = null;
    protected ?FHIRString $display = null;
    protected ?FHIRMarkdown $citation = null;
    protected ?FHIRUrl $url = null;
    protected ?FHIRAttachment $document = null;
    protected ?FHIRCanonical $resource = null;

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getLabel(): ?FHIRString
    {
        return $this->label;
    }

    public function setLabel(string|FHIRString|null $value): self
    {
        $this->label = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDisplay(): ?FHIRString
    {
        return $this->display;
    }

    public function setDisplay(string|FHIRString|null $value): self
    {
        $this->display = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCitation(): ?FHIRMarkdown
    {
        return $this->citation;
    }

    public function setCitation(string|FHIRMarkdown|null $value): self
    {
        $this->citation = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getUrl(): ?FHIRUrl
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUrl|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUrl())->setValue($value) : $value;

        return $this;
    }

    public function getDocument(): ?FHIRAttachment
    {
        return $this->document;
    }

    public function setDocument(?FHIRAttachment $value): self
    {
        $this->document = $value;

        return $this;
    }

    public function getResource(): ?FHIRCanonical
    {
        return $this->resource;
    }

    public function setResource(string|FHIRCanonical|null $value): self
    {
        $this->resource = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }
}
