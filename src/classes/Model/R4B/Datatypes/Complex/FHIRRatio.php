<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Ratio Complex
 */

namespace Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRRatioInterface;

class FHIRRatio extends FHIRElement implements FHIRRatioInterface
{
    public const RESOURCE_NAME = 'Ratio';

    protected ?FHIRQuantity $numerator = null;
    protected ?FHIRQuantity $denominator = null;

    public function getNumerator(): ?FHIRQuantity
    {
        return $this->numerator;
    }

    public function setNumerator(?FHIRQuantity $value): self
    {
        $this->numerator = $value;

        return $this;
    }

    public function getDenominator(): ?FHIRQuantity
    {
        return $this->denominator;
    }

    public function setDenominator(?FHIRQuantity $value): self
    {
        $this->denominator = $value;

        return $this;
    }
}
