<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ProdCharacteristic Complex
 */

namespace Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRProdCharacteristicInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRProdCharacteristic extends FHIRBackboneElement implements FHIRProdCharacteristicInterface
{
    public const RESOURCE_NAME = 'ProdCharacteristic';

    protected ?FHIRQuantity $height = null;
    protected ?FHIRQuantity $width = null;
    protected ?FHIRQuantity $depth = null;
    protected ?FHIRQuantity $weight = null;
    protected ?FHIRQuantity $nominalVolume = null;
    protected ?FHIRQuantity $externalDiameter = null;
    protected ?FHIRString $shape = null;

    /** @var FHIRString[] */
    protected array $color = [];

    /** @var FHIRString[] */
    protected array $imprint = [];

    /** @var FHIRAttachment[] */
    protected array $image = [];
    protected ?FHIRCodeableConcept $scoring = null;

    public function getHeight(): ?FHIRQuantity
    {
        return $this->height;
    }

    public function setHeight(?FHIRQuantity $value): self
    {
        $this->height = $value;

        return $this;
    }

    public function getWidth(): ?FHIRQuantity
    {
        return $this->width;
    }

    public function setWidth(?FHIRQuantity $value): self
    {
        $this->width = $value;

        return $this;
    }

    public function getDepth(): ?FHIRQuantity
    {
        return $this->depth;
    }

    public function setDepth(?FHIRQuantity $value): self
    {
        $this->depth = $value;

        return $this;
    }

    public function getWeight(): ?FHIRQuantity
    {
        return $this->weight;
    }

    public function setWeight(?FHIRQuantity $value): self
    {
        $this->weight = $value;

        return $this;
    }

    public function getNominalVolume(): ?FHIRQuantity
    {
        return $this->nominalVolume;
    }

    public function setNominalVolume(?FHIRQuantity $value): self
    {
        $this->nominalVolume = $value;

        return $this;
    }

    public function getExternalDiameter(): ?FHIRQuantity
    {
        return $this->externalDiameter;
    }

    public function setExternalDiameter(?FHIRQuantity $value): self
    {
        $this->externalDiameter = $value;

        return $this;
    }

    public function getShape(): ?FHIRString
    {
        return $this->shape;
    }

    public function setShape(string|FHIRString|null $value): self
    {
        $this->shape = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getColor(): array
    {
        return $this->color;
    }

    public function setColor(string|FHIRString|null ...$value): self
    {
        $this->color = [];
        $this->addColor(...$value);

        return $this;
    }

    public function addColor(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->color = array_filter(array_merge($this->color, $values));

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getImprint(): array
    {
        return $this->imprint;
    }

    public function setImprint(string|FHIRString|null ...$value): self
    {
        $this->imprint = [];
        $this->addImprint(...$value);

        return $this;
    }

    public function addImprint(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->imprint = array_filter(array_merge($this->imprint, $values));

        return $this;
    }

    /**
     * @return FHIRAttachment[]
     */
    public function getImage(): array
    {
        return $this->image;
    }

    public function setImage(?FHIRAttachment ...$value): self
    {
        $this->image = array_filter($value);

        return $this;
    }

    public function addImage(?FHIRAttachment ...$value): self
    {
        $this->image = array_filter(array_merge($this->image, $value));

        return $this;
    }

    public function getScoring(): ?FHIRCodeableConcept
    {
        return $this->scoring;
    }

    public function setScoring(?FHIRCodeableConcept $value): self
    {
        $this->scoring = $value;

        return $this;
    }
}
