<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Dosage Complex
 */

namespace Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRDosageInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\Element\FHIRDosageDoseAndRate;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRDosage extends FHIRBackboneElement implements FHIRDosageInterface
{
    public const RESOURCE_NAME = 'Dosage';

    protected ?FHIRInteger $sequence = null;
    protected ?FHIRString $text = null;

    /** @var FHIRCodeableConcept[] */
    protected array $additionalInstruction = [];
    protected ?FHIRString $patientInstruction = null;
    protected ?FHIRTiming $timing = null;
    protected FHIRBoolean|FHIRCodeableConcept|null $asNeeded = null;
    protected ?FHIRCodeableConcept $site = null;
    protected ?FHIRCodeableConcept $route = null;
    protected ?FHIRCodeableConcept $method = null;

    /** @var FHIRDosageDoseAndRate[] */
    protected array $doseAndRate = [];
    protected ?FHIRRatio $maxDosePerPeriod = null;
    protected ?FHIRQuantity $maxDosePerAdministration = null;
    protected ?FHIRQuantity $maxDosePerLifetime = null;

    public function getSequence(): ?FHIRInteger
    {
        return $this->sequence;
    }

    public function setSequence(int|FHIRInteger|null $value): self
    {
        $this->sequence = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getText(): ?FHIRString
    {
        return $this->text;
    }

    public function setText(string|FHIRString|null $value): self
    {
        $this->text = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getAdditionalInstruction(): array
    {
        return $this->additionalInstruction;
    }

    public function setAdditionalInstruction(?FHIRCodeableConcept ...$value): self
    {
        $this->additionalInstruction = array_filter($value);

        return $this;
    }

    public function addAdditionalInstruction(?FHIRCodeableConcept ...$value): self
    {
        $this->additionalInstruction = array_filter(array_merge($this->additionalInstruction, $value));

        return $this;
    }

    public function getPatientInstruction(): ?FHIRString
    {
        return $this->patientInstruction;
    }

    public function setPatientInstruction(string|FHIRString|null $value): self
    {
        $this->patientInstruction = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTiming(): ?FHIRTiming
    {
        return $this->timing;
    }

    public function setTiming(?FHIRTiming $value): self
    {
        $this->timing = $value;

        return $this;
    }

    public function getAsNeeded(): FHIRBoolean|FHIRCodeableConcept|null
    {
        return $this->asNeeded;
    }

    public function setAsNeeded(FHIRBoolean|FHIRCodeableConcept|null $value): self
    {
        $this->asNeeded = $value;

        return $this;
    }

    public function getSite(): ?FHIRCodeableConcept
    {
        return $this->site;
    }

    public function setSite(?FHIRCodeableConcept $value): self
    {
        $this->site = $value;

        return $this;
    }

    public function getRoute(): ?FHIRCodeableConcept
    {
        return $this->route;
    }

    public function setRoute(?FHIRCodeableConcept $value): self
    {
        $this->route = $value;

        return $this;
    }

    public function getMethod(): ?FHIRCodeableConcept
    {
        return $this->method;
    }

    public function setMethod(?FHIRCodeableConcept $value): self
    {
        $this->method = $value;

        return $this;
    }

    /**
     * @return FHIRDosageDoseAndRate[]
     */
    public function getDoseAndRate(): array
    {
        return $this->doseAndRate;
    }

    public function setDoseAndRate(?FHIRDosageDoseAndRate ...$value): self
    {
        $this->doseAndRate = array_filter($value);

        return $this;
    }

    public function addDoseAndRate(?FHIRDosageDoseAndRate ...$value): self
    {
        $this->doseAndRate = array_filter(array_merge($this->doseAndRate, $value));

        return $this;
    }

    public function getMaxDosePerPeriod(): ?FHIRRatio
    {
        return $this->maxDosePerPeriod;
    }

    public function setMaxDosePerPeriod(?FHIRRatio $value): self
    {
        $this->maxDosePerPeriod = $value;

        return $this;
    }

    public function getMaxDosePerAdministration(): ?FHIRQuantity
    {
        return $this->maxDosePerAdministration;
    }

    public function setMaxDosePerAdministration(?FHIRQuantity $value): self
    {
        $this->maxDosePerAdministration = $value;

        return $this;
    }

    public function getMaxDosePerLifetime(): ?FHIRQuantity
    {
        return $this->maxDosePerLifetime;
    }

    public function setMaxDosePerLifetime(?FHIRQuantity $value): self
    {
        $this->maxDosePerLifetime = $value;

        return $this;
    }
}
