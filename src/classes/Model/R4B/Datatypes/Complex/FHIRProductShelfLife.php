<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ProductShelfLife Complex
 */

namespace Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRProductShelfLifeInterface;

class FHIRProductShelfLife extends FHIRBackboneElement implements FHIRProductShelfLifeInterface
{
    public const RESOURCE_NAME = 'ProductShelfLife';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRQuantity $period = null;

    /** @var FHIRCodeableConcept[] */
    protected array $specialPrecautionsForStorage = [];

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getPeriod(): ?FHIRQuantity
    {
        return $this->period;
    }

    public function setPeriod(?FHIRQuantity $value): self
    {
        $this->period = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSpecialPrecautionsForStorage(): array
    {
        return $this->specialPrecautionsForStorage;
    }

    public function setSpecialPrecautionsForStorage(?FHIRCodeableConcept ...$value): self
    {
        $this->specialPrecautionsForStorage = array_filter($value);

        return $this;
    }

    public function addSpecialPrecautionsForStorage(?FHIRCodeableConcept ...$value): self
    {
        $this->specialPrecautionsForStorage = array_filter(array_merge($this->specialPrecautionsForStorage, $value));

        return $this;
    }
}
