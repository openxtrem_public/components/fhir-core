<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DosageDoseAndRate Element
 */

namespace Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\Element;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\Element\FHIRDosageDoseAndRateInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRRatio;

class FHIRDosageDoseAndRate extends FHIRElement implements FHIRDosageDoseAndRateInterface
{
    public const RESOURCE_NAME = 'Dosage.doseAndRate';

    protected ?FHIRCodeableConcept $type = null;
    protected FHIRRange|FHIRQuantity|null $dose = null;
    protected FHIRRatio|FHIRRange|FHIRQuantity|null $rate = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getDose(): FHIRRange|FHIRQuantity|null
    {
        return $this->dose;
    }

    public function setDose(FHIRRange|FHIRQuantity|null $value): self
    {
        $this->dose = $value;

        return $this;
    }

    public function getRate(): FHIRRatio|FHIRRange|FHIRQuantity|null
    {
        return $this->rate;
    }

    public function setRate(FHIRRatio|FHIRRange|FHIRQuantity|null $value): self
    {
        $this->rate = $value;

        return $this;
    }
}
