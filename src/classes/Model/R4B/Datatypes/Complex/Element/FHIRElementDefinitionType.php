<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ElementDefinitionType Element
 */

namespace Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\Element;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\Element\FHIRElementDefinitionTypeInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUri;

class FHIRElementDefinitionType extends FHIRElement implements FHIRElementDefinitionTypeInterface
{
    public const RESOURCE_NAME = 'ElementDefinition.type';

    protected ?FHIRUri $code = null;

    /** @var FHIRCanonical[] */
    protected array $profile = [];

    /** @var FHIRCanonical[] */
    protected array $targetProfile = [];

    /** @var FHIRCode[] */
    protected array $aggregation = [];
    protected ?FHIRCode $versioning = null;

    public function getCode(): ?FHIRUri
    {
        return $this->code;
    }

    public function setCode(string|FHIRUri|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getProfile(): array
    {
        return $this->profile;
    }

    public function setProfile(string|FHIRCanonical|null ...$value): self
    {
        $this->profile = [];
        $this->addProfile(...$value);

        return $this;
    }

    public function addProfile(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->profile = array_filter(array_merge($this->profile, $values));

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getTargetProfile(): array
    {
        return $this->targetProfile;
    }

    public function setTargetProfile(string|FHIRCanonical|null ...$value): self
    {
        $this->targetProfile = [];
        $this->addTargetProfile(...$value);

        return $this;
    }

    public function addTargetProfile(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->targetProfile = array_filter(array_merge($this->targetProfile, $values));

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getAggregation(): array
    {
        return $this->aggregation;
    }

    public function setAggregation(string|FHIRCode|null ...$value): self
    {
        $this->aggregation = [];
        $this->addAggregation(...$value);

        return $this;
    }

    public function addAggregation(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->aggregation = array_filter(array_merge($this->aggregation, $values));

        return $this;
    }

    public function getVersioning(): ?FHIRCode
    {
        return $this->versioning;
    }

    public function setVersioning(string|FHIRCode|null $value): self
    {
        $this->versioning = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }
}
