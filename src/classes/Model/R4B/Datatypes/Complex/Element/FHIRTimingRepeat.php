<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TimingRepeat Element
 */

namespace Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\Element;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\Element\FHIRTimingRepeatInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRElement;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRTime;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRUnsignedInt;

class FHIRTimingRepeat extends FHIRElement implements FHIRTimingRepeatInterface
{
    public const RESOURCE_NAME = 'Timing.repeat';

    protected FHIRDuration|FHIRRange|FHIRPeriod|null $bounds = null;
    protected ?FHIRPositiveInt $count = null;
    protected ?FHIRPositiveInt $countMax = null;
    protected ?FHIRDecimal $duration = null;
    protected ?FHIRDecimal $durationMax = null;
    protected ?FHIRCode $durationUnit = null;
    protected ?FHIRPositiveInt $frequency = null;
    protected ?FHIRPositiveInt $frequencyMax = null;
    protected ?FHIRDecimal $period = null;
    protected ?FHIRDecimal $periodMax = null;
    protected ?FHIRCode $periodUnit = null;

    /** @var FHIRCode[] */
    protected array $dayOfWeek = [];

    /** @var FHIRTime[] */
    protected array $timeOfDay = [];

    /** @var FHIRCode[] */
    protected array $when = [];
    protected ?FHIRUnsignedInt $offset = null;

    public function getBounds(): FHIRDuration|FHIRRange|FHIRPeriod|null
    {
        return $this->bounds;
    }

    public function setBounds(FHIRDuration|FHIRRange|FHIRPeriod|null $value): self
    {
        $this->bounds = $value;

        return $this;
    }

    public function getCount(): ?FHIRPositiveInt
    {
        return $this->count;
    }

    public function setCount(int|FHIRPositiveInt|null $value): self
    {
        $this->count = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getCountMax(): ?FHIRPositiveInt
    {
        return $this->countMax;
    }

    public function setCountMax(int|FHIRPositiveInt|null $value): self
    {
        $this->countMax = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getDuration(): ?FHIRDecimal
    {
        return $this->duration;
    }

    public function setDuration(float|FHIRDecimal|null $value): self
    {
        $this->duration = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getDurationMax(): ?FHIRDecimal
    {
        return $this->durationMax;
    }

    public function setDurationMax(float|FHIRDecimal|null $value): self
    {
        $this->durationMax = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getDurationUnit(): ?FHIRCode
    {
        return $this->durationUnit;
    }

    public function setDurationUnit(string|FHIRCode|null $value): self
    {
        $this->durationUnit = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getFrequency(): ?FHIRPositiveInt
    {
        return $this->frequency;
    }

    public function setFrequency(int|FHIRPositiveInt|null $value): self
    {
        $this->frequency = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getFrequencyMax(): ?FHIRPositiveInt
    {
        return $this->frequencyMax;
    }

    public function setFrequencyMax(int|FHIRPositiveInt|null $value): self
    {
        $this->frequencyMax = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getPeriod(): ?FHIRDecimal
    {
        return $this->period;
    }

    public function setPeriod(float|FHIRDecimal|null $value): self
    {
        $this->period = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getPeriodMax(): ?FHIRDecimal
    {
        return $this->periodMax;
    }

    public function setPeriodMax(float|FHIRDecimal|null $value): self
    {
        $this->periodMax = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getPeriodUnit(): ?FHIRCode
    {
        return $this->periodUnit;
    }

    public function setPeriodUnit(string|FHIRCode|null $value): self
    {
        $this->periodUnit = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getDayOfWeek(): array
    {
        return $this->dayOfWeek;
    }

    public function setDayOfWeek(string|FHIRCode|null ...$value): self
    {
        $this->dayOfWeek = [];
        $this->addDayOfWeek(...$value);

        return $this;
    }

    public function addDayOfWeek(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->dayOfWeek = array_filter(array_merge($this->dayOfWeek, $values));

        return $this;
    }

    /**
     * @return FHIRTime[]
     */
    public function getTimeOfDay(): array
    {
        return $this->timeOfDay;
    }

    public function setTimeOfDay(string|FHIRTime|null ...$value): self
    {
        $this->timeOfDay = [];
        $this->addTimeOfDay(...$value);

        return $this;
    }

    public function addTimeOfDay(string|FHIRTime|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRTime())->setValue($v) : $v, $value);

        $this->timeOfDay = array_filter(array_merge($this->timeOfDay, $values));

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getWhen(): array
    {
        return $this->when;
    }

    public function setWhen(string|FHIRCode|null ...$value): self
    {
        $this->when = [];
        $this->addWhen(...$value);

        return $this;
    }

    public function addWhen(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->when = array_filter(array_merge($this->when, $values));

        return $this;
    }

    public function getOffset(): ?FHIRUnsignedInt
    {
        return $this->offset;
    }

    public function setOffset(int|FHIRUnsignedInt|null $value): self
    {
        $this->offset = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }
}
