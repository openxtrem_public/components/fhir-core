<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ContactPoint Complex
 */

namespace Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRContactPointInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRContactPoint extends FHIRElement implements FHIRContactPointInterface
{
    public const RESOURCE_NAME = 'ContactPoint';

    protected ?FHIRCode $system = null;
    protected ?FHIRString $value = null;
    protected ?FHIRCode $use = null;
    protected ?FHIRPositiveInt $rank = null;
    protected ?FHIRPeriod $period = null;

    public function getSystem(): ?FHIRCode
    {
        return $this->system;
    }

    public function setSystem(string|FHIRCode|null $value): self
    {
        $this->system = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getValue(): ?FHIRString
    {
        return $this->value;
    }

    public function setValue(string|FHIRString|null $value): self
    {
        $this->value = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getUse(): ?FHIRCode
    {
        return $this->use;
    }

    public function setUse(string|FHIRCode|null $value): self
    {
        $this->use = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getRank(): ?FHIRPositiveInt
    {
        return $this->rank;
    }

    public function setRank(int|FHIRPositiveInt|null $value): self
    {
        $this->rank = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }
}
