<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TriggerDefinition Complex
 */

namespace Ox\Components\FHIRCore\Model\R4B\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRTriggerDefinitionInterface;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4B\Datatypes\FHIRString;

class FHIRTriggerDefinition extends FHIRElement implements FHIRTriggerDefinitionInterface
{
    public const RESOURCE_NAME = 'TriggerDefinition';

    protected ?FHIRCode $type = null;
    protected ?FHIRString $name = null;
    protected FHIRTiming|FHIRReference|FHIRDate|FHIRDateTime|null $timing = null;

    /** @var FHIRDataRequirement[] */
    protected array $data = [];
    protected ?FHIRExpression $condition = null;

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTiming(): FHIRTiming|FHIRReference|FHIRDate|FHIRDateTime|null
    {
        return $this->timing;
    }

    public function setTiming(FHIRTiming|FHIRReference|FHIRDate|FHIRDateTime|null $value): self
    {
        $this->timing = $value;

        return $this;
    }

    /**
     * @return FHIRDataRequirement[]
     */
    public function getData(): array
    {
        return $this->data;
    }

    public function setData(?FHIRDataRequirement ...$value): self
    {
        $this->data = array_filter($value);

        return $this;
    }

    public function addData(?FHIRDataRequirement ...$value): self
    {
        $this->data = array_filter(array_merge($this->data, $value));

        return $this;
    }

    public function getCondition(): ?FHIRExpression
    {
        return $this->condition;
    }

    public function setCondition(?FHIRExpression $value): self
    {
        $this->condition = $value;

        return $this;
    }
}
