<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR url Primitive
 */

namespace Ox\Components\FHIRCore\Model\R4B\Datatypes;

use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRUrlInterface;

class FHIRUrl extends FHIRUri implements FHIRUrlInterface
{
    public const RESOURCE_NAME = 'url';
}
