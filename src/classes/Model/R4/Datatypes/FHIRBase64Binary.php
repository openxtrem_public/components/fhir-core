<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR base64Binary Primitive
 */

namespace Ox\Components\FHIRCore\Model\R4\Datatypes;

use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRBase64BinaryInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRElement;

class FHIRBase64Binary extends FHIRElement implements FHIRBase64BinaryInterface
{
    public const RESOURCE_NAME = 'base64Binary';

    protected ?string $value = null;

    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param bool        $alreadyEncoded
     */
    public function setValue(?string $value, bool $alreadyEncoded = true): self
    {
        if ($value === null) {
            $this->value = $value;
            return $this;
        }

        $this->value = $alreadyEncoded ? $value : base64_encode($value);

        return $this;
    }

    public function getDecodedData(): ?string
    {
        if ($this->value) {
            return base64_decode($this->value);
        }

        return null;
    }
}
