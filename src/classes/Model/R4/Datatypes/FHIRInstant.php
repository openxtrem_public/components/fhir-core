<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR instant Primitive
 */

namespace Ox\Components\FHIRCore\Model\R4\Datatypes;

use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRInstantInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRElement;

class FHIRInstant extends FHIRElement implements FHIRInstantInterface
{
    public const RESOURCE_NAME = 'instant';

    protected ?string $value = null;

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
