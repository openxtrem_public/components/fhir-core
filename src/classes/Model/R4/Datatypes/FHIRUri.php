<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR uri Primitive
 */

namespace Ox\Components\FHIRCore\Model\R4\Datatypes;

use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRUriInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRElement;

class FHIRUri extends FHIRElement implements FHIRUriInterface
{
    public const RESOURCE_NAME = 'uri';

    protected ?string $value = null;

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function isMatch(string $uri): bool
    {
        return preg_match("~^(?:urn:(?:uuid|oid):)?$uri$~", $this->value ?? '');
    }
}
