<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Money Complex
 */

namespace Ox\Components\FHIRCore\Model\R4\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRMoneyInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDecimal;

class FHIRMoney extends FHIRElement implements FHIRMoneyInterface
{
    public const RESOURCE_NAME = 'Money';

    protected ?FHIRDecimal $value = null;
    protected ?FHIRCode $currency = null;

    public function getValue(): ?FHIRDecimal
    {
        return $this->value;
    }

    public function setValue(float|FHIRDecimal|null $value): self
    {
        $this->value = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getCurrency(): ?FHIRCode
    {
        return $this->currency;
    }

    public function setCurrency(string|FHIRCode|null $value): self
    {
        $this->currency = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }
}
