<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Address Complex
 */

namespace Ox\Components\FHIRCore\Model\R4\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRAddressInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRAddress extends FHIRElement implements FHIRAddressInterface
{
    public const RESOURCE_NAME = 'Address';

    protected ?FHIRCode $use = null;
    protected ?FHIRCode $type = null;
    protected ?FHIRString $text = null;

    /** @var FHIRString[] */
    protected array $line = [];
    protected ?FHIRString $city = null;
    protected ?FHIRString $district = null;
    protected ?FHIRString $state = null;
    protected ?FHIRString $postalCode = null;
    protected ?FHIRString $country = null;
    protected ?FHIRPeriod $period = null;

    public function getUse(): ?FHIRCode
    {
        return $this->use;
    }

    public function setUse(string|FHIRCode|null $value): self
    {
        $this->use = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getText(): ?FHIRString
    {
        return $this->text;
    }

    public function setText(string|FHIRString|null $value): self
    {
        $this->text = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getLine(): array
    {
        return $this->line;
    }

    public function setLine(string|FHIRString|null ...$value): self
    {
        $this->line = [];
        $this->addLine(...$value);

        return $this;
    }

    public function addLine(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->line = array_filter(array_merge($this->line, $values));

        return $this;
    }

    public function getCity(): ?FHIRString
    {
        return $this->city;
    }

    public function setCity(string|FHIRString|null $value): self
    {
        $this->city = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDistrict(): ?FHIRString
    {
        return $this->district;
    }

    public function setDistrict(string|FHIRString|null $value): self
    {
        $this->district = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getState(): ?FHIRString
    {
        return $this->state;
    }

    public function setState(string|FHIRString|null $value): self
    {
        $this->state = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPostalCode(): ?FHIRString
    {
        return $this->postalCode;
    }

    public function setPostalCode(string|FHIRString|null $value): self
    {
        $this->postalCode = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCountry(): ?FHIRString
    {
        return $this->country;
    }

    public function setCountry(string|FHIRString|null $value): self
    {
        $this->country = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }
}
