<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DataRequirement Complex
 */

namespace Ox\Components\FHIRCore\Model\R4\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRDataRequirementInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\Element\FHIRDataRequirementCodeFilter;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\Element\FHIRDataRequirementDateFilter;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\Element\FHIRDataRequirementSort;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRDataRequirement extends FHIRElement implements FHIRDataRequirementInterface
{
    public const RESOURCE_NAME = 'DataRequirement';

    protected ?FHIRCode $type = null;

    /** @var FHIRCanonical[] */
    protected array $profile = [];
    protected FHIRCodeableConcept|FHIRReference|null $subject = null;

    /** @var FHIRString[] */
    protected array $mustSupport = [];

    /** @var FHIRDataRequirementCodeFilter[] */
    protected array $codeFilter = [];

    /** @var FHIRDataRequirementDateFilter[] */
    protected array $dateFilter = [];
    protected ?FHIRPositiveInt $limit = null;

    /** @var FHIRDataRequirementSort[] */
    protected array $sort = [];

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getProfile(): array
    {
        return $this->profile;
    }

    public function setProfile(string|FHIRCanonical|null ...$value): self
    {
        $this->profile = [];
        $this->addProfile(...$value);

        return $this;
    }

    public function addProfile(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->profile = array_filter(array_merge($this->profile, $values));

        return $this;
    }

    public function getSubject(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->subject;
    }

    public function setSubject(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->subject = $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getMustSupport(): array
    {
        return $this->mustSupport;
    }

    public function setMustSupport(string|FHIRString|null ...$value): self
    {
        $this->mustSupport = [];
        $this->addMustSupport(...$value);

        return $this;
    }

    public function addMustSupport(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->mustSupport = array_filter(array_merge($this->mustSupport, $values));

        return $this;
    }

    /**
     * @return FHIRDataRequirementCodeFilter[]
     */
    public function getCodeFilter(): array
    {
        return $this->codeFilter;
    }

    public function setCodeFilter(?FHIRDataRequirementCodeFilter ...$value): self
    {
        $this->codeFilter = array_filter($value);

        return $this;
    }

    public function addCodeFilter(?FHIRDataRequirementCodeFilter ...$value): self
    {
        $this->codeFilter = array_filter(array_merge($this->codeFilter, $value));

        return $this;
    }

    /**
     * @return FHIRDataRequirementDateFilter[]
     */
    public function getDateFilter(): array
    {
        return $this->dateFilter;
    }

    public function setDateFilter(?FHIRDataRequirementDateFilter ...$value): self
    {
        $this->dateFilter = array_filter($value);

        return $this;
    }

    public function addDateFilter(?FHIRDataRequirementDateFilter ...$value): self
    {
        $this->dateFilter = array_filter(array_merge($this->dateFilter, $value));

        return $this;
    }

    public function getLimit(): ?FHIRPositiveInt
    {
        return $this->limit;
    }

    public function setLimit(int|FHIRPositiveInt|null $value): self
    {
        $this->limit = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRDataRequirementSort[]
     */
    public function getSort(): array
    {
        return $this->sort;
    }

    public function setSort(?FHIRDataRequirementSort ...$value): self
    {
        $this->sort = array_filter($value);

        return $this;
    }

    public function addSort(?FHIRDataRequirementSort ...$value): self
    {
        $this->sort = array_filter(array_merge($this->sort, $value));

        return $this;
    }
}
