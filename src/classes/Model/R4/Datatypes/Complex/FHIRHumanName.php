<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR HumanName Complex
 */

namespace Ox\Components\FHIRCore\Model\R4\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRHumanNameInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRHumanName extends FHIRElement implements FHIRHumanNameInterface
{
    public const RESOURCE_NAME = 'HumanName';

    protected ?FHIRCode $use = null;
    protected ?FHIRString $text = null;
    protected ?FHIRString $family = null;

    /** @var FHIRString[] */
    protected array $given = [];

    /** @var FHIRString[] */
    protected array $prefix = [];

    /** @var FHIRString[] */
    protected array $suffix = [];
    protected ?FHIRPeriod $period = null;

    public function getUse(): ?FHIRCode
    {
        return $this->use;
    }

    public function setUse(string|FHIRCode|null $value): self
    {
        $this->use = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getText(): ?FHIRString
    {
        return $this->text;
    }

    public function setText(string|FHIRString|null $value): self
    {
        $this->text = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getFamily(): ?FHIRString
    {
        return $this->family;
    }

    public function setFamily(string|FHIRString|null $value): self
    {
        $this->family = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getGiven(): array
    {
        return $this->given;
    }

    public function setGiven(string|FHIRString|null ...$value): self
    {
        $this->given = [];
        $this->addGiven(...$value);

        return $this;
    }

    public function addGiven(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->given = array_filter(array_merge($this->given, $values));

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getPrefix(): array
    {
        return $this->prefix;
    }

    public function setPrefix(string|FHIRString|null ...$value): self
    {
        $this->prefix = [];
        $this->addPrefix(...$value);

        return $this;
    }

    public function addPrefix(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->prefix = array_filter(array_merge($this->prefix, $values));

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getSuffix(): array
    {
        return $this->suffix;
    }

    public function setSuffix(string|FHIRString|null ...$value): self
    {
        $this->suffix = [];
        $this->addSuffix(...$value);

        return $this;
    }

    public function addSuffix(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->suffix = array_filter(array_merge($this->suffix, $values));

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }
}
