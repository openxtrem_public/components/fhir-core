<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceAmount Complex
 */

namespace Ox\Components\FHIRCore\Model\R4\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRSubstanceAmountInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\Element\FHIRSubstanceAmountReferenceRange;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRSubstanceAmount extends FHIRBackboneElement implements FHIRSubstanceAmountInterface
{
    public const RESOURCE_NAME = 'SubstanceAmount';

    protected FHIRQuantity|FHIRRange|FHIRString|null $amount = null;
    protected ?FHIRCodeableConcept $amountType = null;
    protected ?FHIRString $amountText = null;
    protected ?FHIRSubstanceAmountReferenceRange $referenceRange = null;

    public function getAmount(): FHIRQuantity|FHIRRange|FHIRString|null
    {
        return $this->amount;
    }

    public function setAmount(FHIRQuantity|FHIRRange|FHIRString|null $value): self
    {
        $this->amount = $value;

        return $this;
    }

    public function getAmountType(): ?FHIRCodeableConcept
    {
        return $this->amountType;
    }

    public function setAmountType(?FHIRCodeableConcept $value): self
    {
        $this->amountType = $value;

        return $this;
    }

    public function getAmountText(): ?FHIRString
    {
        return $this->amountText;
    }

    public function setAmountText(string|FHIRString|null $value): self
    {
        $this->amountText = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getReferenceRange(): ?FHIRSubstanceAmountReferenceRange
    {
        return $this->referenceRange;
    }

    public function setReferenceRange(?FHIRSubstanceAmountReferenceRange $value): self
    {
        $this->referenceRange = $value;

        return $this;
    }
}
