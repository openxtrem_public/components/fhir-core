<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ElementDefinitionBinding Element
 */

namespace Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\Element;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\Element\FHIRElementDefinitionBindingInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRElementDefinitionBinding extends FHIRElement implements FHIRElementDefinitionBindingInterface
{
    public const RESOURCE_NAME = 'ElementDefinition.binding';

    protected ?FHIRCode $strength = null;
    protected ?FHIRString $description = null;
    protected ?FHIRCanonical $valueSet = null;

    public function getStrength(): ?FHIRCode
    {
        return $this->strength;
    }

    public function setStrength(string|FHIRCode|null $value): self
    {
        $this->strength = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getValueSet(): ?FHIRCanonical
    {
        return $this->valueSet;
    }

    public function setValueSet(string|FHIRCanonical|null $value): self
    {
        $this->valueSet = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }
}
