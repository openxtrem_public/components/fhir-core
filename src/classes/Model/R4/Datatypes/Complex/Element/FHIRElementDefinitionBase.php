<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ElementDefinitionBase Element
 */

namespace Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\Element;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\Element\FHIRElementDefinitionBaseInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUnsignedInt;

class FHIRElementDefinitionBase extends FHIRElement implements FHIRElementDefinitionBaseInterface
{
    public const RESOURCE_NAME = 'ElementDefinition.base';

    protected ?FHIRString $path = null;
    protected ?FHIRUnsignedInt $min = null;
    protected ?FHIRString $max = null;

    public function getPath(): ?FHIRString
    {
        return $this->path;
    }

    public function setPath(string|FHIRString|null $value): self
    {
        $this->path = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getMin(): ?FHIRUnsignedInt
    {
        return $this->min;
    }

    public function setMin(int|FHIRUnsignedInt|null $value): self
    {
        $this->min = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getMax(): ?FHIRString
    {
        return $this->max;
    }

    public function setMax(string|FHIRString|null $value): self
    {
        $this->max = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
