<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ElementDefinitionMapping Element
 */

namespace Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\Element;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\Element\FHIRElementDefinitionMappingInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRElementDefinitionMapping extends FHIRElement implements FHIRElementDefinitionMappingInterface
{
    public const RESOURCE_NAME = 'ElementDefinition.mapping';

    protected ?FHIRId $identity = null;
    protected ?FHIRCode $language = null;
    protected ?FHIRString $map = null;
    protected ?FHIRString $comment = null;

    public function getIdentity(): ?FHIRId
    {
        return $this->identity;
    }

    public function setIdentity(string|FHIRId|null $value): self
    {
        $this->identity = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getLanguage(): ?FHIRCode
    {
        return $this->language;
    }

    public function setLanguage(string|FHIRCode|null $value): self
    {
        $this->language = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getMap(): ?FHIRString
    {
        return $this->map;
    }

    public function setMap(string|FHIRString|null $value): self
    {
        $this->map = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getComment(): ?FHIRString
    {
        return $this->comment;
    }

    public function setComment(string|FHIRString|null $value): self
    {
        $this->comment = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
