<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceAmountReferenceRange Element
 */

namespace Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\Element;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\Element\FHIRSubstanceAmountReferenceRangeInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;

class FHIRSubstanceAmountReferenceRange extends FHIRElement implements FHIRSubstanceAmountReferenceRangeInterface
{
    public const RESOURCE_NAME = 'SubstanceAmount.referenceRange';

    protected ?FHIRQuantity $lowLimit = null;
    protected ?FHIRQuantity $highLimit = null;

    public function getLowLimit(): ?FHIRQuantity
    {
        return $this->lowLimit;
    }

    public function setLowLimit(?FHIRQuantity $value): self
    {
        $this->lowLimit = $value;

        return $this;
    }

    public function getHighLimit(): ?FHIRQuantity
    {
        return $this->highLimit;
    }

    public function setHighLimit(?FHIRQuantity $value): self
    {
        $this->highLimit = $value;

        return $this;
    }
}
