<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Quantity Complex
 */

namespace Ox\Components\FHIRCore\Model\R4\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRQuantityInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUri;

class FHIRQuantity extends FHIRElement implements FHIRQuantityInterface
{
    public const RESOURCE_NAME = 'Quantity';

    protected ?FHIRDecimal $value = null;
    protected ?FHIRCode $comparator = null;
    protected ?FHIRString $unit = null;
    protected ?FHIRUri $system = null;
    protected ?FHIRCode $code = null;

    public function getValue(): ?FHIRDecimal
    {
        return $this->value;
    }

    public function setValue(float|FHIRDecimal|null $value): self
    {
        $this->value = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getComparator(): ?FHIRCode
    {
        return $this->comparator;
    }

    public function setComparator(string|FHIRCode|null $value): self
    {
        $this->comparator = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getUnit(): ?FHIRString
    {
        return $this->unit;
    }

    public function setUnit(string|FHIRString|null $value): self
    {
        $this->unit = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSystem(): ?FHIRUri
    {
        return $this->system;
    }

    public function setSystem(string|FHIRUri|null $value): self
    {
        $this->system = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }
}
