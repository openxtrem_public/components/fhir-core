<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Period Complex
 */

namespace Ox\Components\FHIRCore\Model\R4\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRPeriodInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;

class FHIRPeriod extends FHIRElement implements FHIRPeriodInterface
{
    public const RESOURCE_NAME = 'Period';

    protected ?FHIRDateTime $start = null;
    protected ?FHIRDateTime $end = null;

    public function getStart(): ?FHIRDateTime
    {
        return $this->start;
    }

    public function setStart(string|FHIRDateTime|null $value): self
    {
        $this->start = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getEnd(): ?FHIRDateTime
    {
        return $this->end;
    }

    public function setEnd(string|FHIRDateTime|null $value): self
    {
        $this->end = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }
}
