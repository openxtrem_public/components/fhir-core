<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Meta Complex
 */

namespace Ox\Components\FHIRCore\Model\R4\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRMetaInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUri;

class FHIRMeta extends FHIRElement implements FHIRMetaInterface
{
    public const RESOURCE_NAME = 'Meta';

    protected ?FHIRId $versionId = null;
    protected ?FHIRInstant $lastUpdated = null;
    protected ?FHIRUri $source = null;

    /** @var FHIRCanonical[] */
    protected array $profile = [];

    /** @var FHIRCoding[] */
    protected array $security = [];

    /** @var FHIRCoding[] */
    protected array $tag = [];

    public function getVersionId(): ?FHIRId
    {
        return $this->versionId;
    }

    public function setVersionId(string|FHIRId|null $value): self
    {
        $this->versionId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getLastUpdated(): ?FHIRInstant
    {
        return $this->lastUpdated;
    }

    public function setLastUpdated(string|FHIRInstant|null $value): self
    {
        $this->lastUpdated = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    public function getSource(): ?FHIRUri
    {
        return $this->source;
    }

    public function setSource(string|FHIRUri|null $value): self
    {
        $this->source = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getProfile(): array
    {
        return $this->profile;
    }

    public function setProfile(string|FHIRCanonical|null ...$value): self
    {
        $this->profile = [];
        $this->addProfile(...$value);

        return $this;
    }

    public function addProfile(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->profile = array_filter(array_merge($this->profile, $values));

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getSecurity(): array
    {
        return $this->security;
    }

    public function setSecurity(?FHIRCoding ...$value): self
    {
        $this->security = array_filter($value);

        return $this;
    }

    public function addSecurity(?FHIRCoding ...$value): self
    {
        $this->security = array_filter(array_merge($this->security, $value));

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getTag(): array
    {
        return $this->tag;
    }

    public function setTag(?FHIRCoding ...$value): self
    {
        $this->tag = array_filter($value);

        return $this;
    }

    public function addTag(?FHIRCoding ...$value): self
    {
        $this->tag = array_filter(array_merge($this->tag, $value));

        return $this;
    }
}
