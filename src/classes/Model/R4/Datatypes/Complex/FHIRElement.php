<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Element Complex
 */

namespace Ox\Components\FHIRCore\Model\R4\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRElementInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\StructureDefinition;
use Ox\Components\FHIRCore\Definition\ResourceDefinition;

abstract class FHIRElement implements FHIRElementInterface
{
    public const RESOURCE_NAME = 'Element';

    protected ?FHIRString $id = null;

    /** @var FHIRExtension[] */
    protected array $extension = [];
    protected ResourceDefinition $fhir_definition;

    public function getId(): ?FHIRString
    {
        return $this->id;
    }

    public function setId(string|FHIRString|null $value): self
    {
        $this->id = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRExtension[]
     */
    public function getExtension(): array
    {
        return $this->extension;
    }

    public function setExtension(?FHIRExtension ...$value): self
    {
        $this->extension = array_filter($value);

        return $this;
    }

    public function addExtension(?FHIRExtension ...$value): self
    {
        $this->extension = array_filter(array_merge($this->extension, $value));

        return $this;
    }

    public static function isArrayNull(array $array): bool
    {
        foreach ($array as $item) {
            if (!$item->isNull()) {
                return false;
            }
        }

        return true;
    }

    public function isNull(): bool
    {
        foreach ($this->fhir_definition->getFields() as $field) {
            $gotValue = $this->{"get" . ucfirst($field->getLabel())}();
            if ($field->isUnique()) {
                if (!is_null($gotValue) && (!is_object($gotValue) || !$gotValue->isNull())) {
                    return false;
                }
            } elseif (!self::isArrayNull($gotValue)) {
                return false;
            }
        }

        return true;
    }

    public function __construct()
    {
        $this->fhir_definition = StructureDefinition::getResourceDefinition($this);
    }

    public function getFhirResourceDefinition(): ResourceDefinition
    {
        return $this->fhir_definition;
    }

    public static function getFhirStructureDefinition(): string
    {
        return StructureDefinition::class;
    }

    public function getExtensionFromUrls(string ...$urls): ?FHIRExtension
    {
        foreach ($this->extension ?? [] as $extension) {
            if ($extension = $extension->getFromUrls(...$urls)) {
                return $extension;
            }
        }

        return null;
    }
}
