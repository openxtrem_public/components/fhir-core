<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Narrative Complex
 */

namespace Ox\Components\FHIRCore\Model\R4\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRNarrativeInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRXhtml;

class FHIRNarrative extends FHIRElement implements FHIRNarrativeInterface
{
    public const RESOURCE_NAME = 'Narrative';

    protected ?FHIRCode $status = null;
    protected ?FHIRXhtml $div = null;

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDiv(): ?FHIRXhtml
    {
        return $this->div;
    }

    public function setDiv(string|FHIRXhtml|null $value): self
    {
        $this->div = is_string($value) ? (new FHIRXhtml())->setValue($value) : $value;

        return $this;
    }
}
