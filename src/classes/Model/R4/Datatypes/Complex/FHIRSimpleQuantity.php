<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SimpleQuantity Complex
 */

namespace Ox\Components\FHIRCore\Model\R4\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRSimpleQuantityInterface;

class FHIRSimpleQuantity extends FHIRQuantity implements FHIRSimpleQuantityInterface
{
    public const RESOURCE_NAME = 'SimpleQuantity';
}
