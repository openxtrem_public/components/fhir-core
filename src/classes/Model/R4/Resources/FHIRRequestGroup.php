<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR RequestGroup Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRRequestGroupInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRRequestGroupAction;

class FHIRRequestGroup extends FHIRDomainResource implements FHIRRequestGroupInterface
{
    public const RESOURCE_NAME = 'RequestGroup';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRCanonical[] */
    protected array $instantiatesCanonical = [];

    /** @var FHIRUri[] */
    protected array $instantiatesUri = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];

    /** @var FHIRReference[] */
    protected array $replaces = [];
    protected ?FHIRIdentifier $groupIdentifier = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRCode $intent = null;
    protected ?FHIRCode $priority = null;
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $encounter = null;
    protected ?FHIRDateTime $authoredOn = null;
    protected ?FHIRReference $author = null;

    /** @var FHIRCodeableConcept[] */
    protected array $reasonCode = [];

    /** @var FHIRReference[] */
    protected array $reasonReference = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRRequestGroupAction[] */
    protected array $action = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getInstantiatesCanonical(): array
    {
        return $this->instantiatesCanonical;
    }

    public function setInstantiatesCanonical(string|FHIRCanonical|null ...$value): self
    {
        $this->instantiatesCanonical = [];
        $this->addInstantiatesCanonical(...$value);

        return $this;
    }

    public function addInstantiatesCanonical(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->instantiatesCanonical = array_filter(array_merge($this->instantiatesCanonical, $values));

        return $this;
    }

    /**
     * @return FHIRUri[]
     */
    public function getInstantiatesUri(): array
    {
        return $this->instantiatesUri;
    }

    public function setInstantiatesUri(string|FHIRUri|null ...$value): self
    {
        $this->instantiatesUri = [];
        $this->addInstantiatesUri(...$value);

        return $this;
    }

    public function addInstantiatesUri(string|FHIRUri|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUri())->setValue($v) : $v, $value);

        $this->instantiatesUri = array_filter(array_merge($this->instantiatesUri, $values));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReplaces(): array
    {
        return $this->replaces;
    }

    public function setReplaces(?FHIRReference ...$value): self
    {
        $this->replaces = array_filter($value);

        return $this;
    }

    public function addReplaces(?FHIRReference ...$value): self
    {
        $this->replaces = array_filter(array_merge($this->replaces, $value));

        return $this;
    }

    public function getGroupIdentifier(): ?FHIRIdentifier
    {
        return $this->groupIdentifier;
    }

    public function setGroupIdentifier(?FHIRIdentifier $value): self
    {
        $this->groupIdentifier = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getIntent(): ?FHIRCode
    {
        return $this->intent;
    }

    public function setIntent(string|FHIRCode|null $value): self
    {
        $this->intent = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getPriority(): ?FHIRCode
    {
        return $this->priority;
    }

    public function setPriority(string|FHIRCode|null $value): self
    {
        $this->priority = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getAuthoredOn(): ?FHIRDateTime
    {
        return $this->authoredOn;
    }

    public function setAuthoredOn(string|FHIRDateTime|null $value): self
    {
        $this->authoredOn = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getAuthor(): ?FHIRReference
    {
        return $this->author;
    }

    public function setAuthor(?FHIRReference $value): self
    {
        $this->author = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReasonCode(): array
    {
        return $this->reasonCode;
    }

    public function setReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter($value);

        return $this;
    }

    public function addReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter(array_merge($this->reasonCode, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReasonReference(): array
    {
        return $this->reasonReference;
    }

    public function setReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter($value);

        return $this;
    }

    public function addReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter(array_merge($this->reasonReference, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRRequestGroupAction[]
     */
    public function getAction(): array
    {
        return $this->action;
    }

    public function setAction(?FHIRRequestGroupAction ...$value): self
    {
        $this->action = array_filter($value);

        return $this;
    }

    public function addAction(?FHIRRequestGroupAction ...$value): self
    {
        $this->action = array_filter(array_merge($this->action, $value));

        return $this;
    }
}
