<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductManufactured Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMedicinalProductManufacturedInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRProdCharacteristic;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;

class FHIRMedicinalProductManufactured extends FHIRDomainResource implements FHIRMedicinalProductManufacturedInterface
{
    public const RESOURCE_NAME = 'MedicinalProductManufactured';

    protected ?FHIRCodeableConcept $manufacturedDoseForm = null;
    protected ?FHIRCodeableConcept $unitOfPresentation = null;
    protected ?FHIRQuantity $quantity = null;

    /** @var FHIRReference[] */
    protected array $manufacturer = [];

    /** @var FHIRReference[] */
    protected array $ingredient = [];
    protected ?FHIRProdCharacteristic $physicalCharacteristics = null;

    /** @var FHIRCodeableConcept[] */
    protected array $otherCharacteristics = [];

    public function getManufacturedDoseForm(): ?FHIRCodeableConcept
    {
        return $this->manufacturedDoseForm;
    }

    public function setManufacturedDoseForm(?FHIRCodeableConcept $value): self
    {
        $this->manufacturedDoseForm = $value;

        return $this;
    }

    public function getUnitOfPresentation(): ?FHIRCodeableConcept
    {
        return $this->unitOfPresentation;
    }

    public function setUnitOfPresentation(?FHIRCodeableConcept $value): self
    {
        $this->unitOfPresentation = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getManufacturer(): array
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?FHIRReference ...$value): self
    {
        $this->manufacturer = array_filter($value);

        return $this;
    }

    public function addManufacturer(?FHIRReference ...$value): self
    {
        $this->manufacturer = array_filter(array_merge($this->manufacturer, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getIngredient(): array
    {
        return $this->ingredient;
    }

    public function setIngredient(?FHIRReference ...$value): self
    {
        $this->ingredient = array_filter($value);

        return $this;
    }

    public function addIngredient(?FHIRReference ...$value): self
    {
        $this->ingredient = array_filter(array_merge($this->ingredient, $value));

        return $this;
    }

    public function getPhysicalCharacteristics(): ?FHIRProdCharacteristic
    {
        return $this->physicalCharacteristics;
    }

    public function setPhysicalCharacteristics(?FHIRProdCharacteristic $value): self
    {
        $this->physicalCharacteristics = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getOtherCharacteristics(): array
    {
        return $this->otherCharacteristics;
    }

    public function setOtherCharacteristics(?FHIRCodeableConcept ...$value): self
    {
        $this->otherCharacteristics = array_filter($value);

        return $this;
    }

    public function addOtherCharacteristics(?FHIRCodeableConcept ...$value): self
    {
        $this->otherCharacteristics = array_filter(array_merge($this->otherCharacteristics, $value));

        return $this;
    }
}
