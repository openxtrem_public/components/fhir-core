<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Specimen Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRSpecimenInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRSpecimenCollection;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRSpecimenContainer;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRSpecimenProcessing;

class FHIRSpecimen extends FHIRDomainResource implements FHIRSpecimenInterface
{
    public const RESOURCE_NAME = 'Specimen';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRIdentifier $accessionIdentifier = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRDateTime $receivedTime = null;

    /** @var FHIRReference[] */
    protected array $parent = [];

    /** @var FHIRReference[] */
    protected array $request = [];
    protected ?FHIRSpecimenCollection $collection = null;

    /** @var FHIRSpecimenProcessing[] */
    protected array $processing = [];

    /** @var FHIRSpecimenContainer[] */
    protected array $container = [];

    /** @var FHIRCodeableConcept[] */
    protected array $condition = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getAccessionIdentifier(): ?FHIRIdentifier
    {
        return $this->accessionIdentifier;
    }

    public function setAccessionIdentifier(?FHIRIdentifier $value): self
    {
        $this->accessionIdentifier = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getReceivedTime(): ?FHIRDateTime
    {
        return $this->receivedTime;
    }

    public function setReceivedTime(string|FHIRDateTime|null $value): self
    {
        $this->receivedTime = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getParent(): array
    {
        return $this->parent;
    }

    public function setParent(?FHIRReference ...$value): self
    {
        $this->parent = array_filter($value);

        return $this;
    }

    public function addParent(?FHIRReference ...$value): self
    {
        $this->parent = array_filter(array_merge($this->parent, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getRequest(): array
    {
        return $this->request;
    }

    public function setRequest(?FHIRReference ...$value): self
    {
        $this->request = array_filter($value);

        return $this;
    }

    public function addRequest(?FHIRReference ...$value): self
    {
        $this->request = array_filter(array_merge($this->request, $value));

        return $this;
    }

    public function getCollection(): ?FHIRSpecimenCollection
    {
        return $this->collection;
    }

    public function setCollection(?FHIRSpecimenCollection $value): self
    {
        $this->collection = $value;

        return $this;
    }

    /**
     * @return FHIRSpecimenProcessing[]
     */
    public function getProcessing(): array
    {
        return $this->processing;
    }

    public function setProcessing(?FHIRSpecimenProcessing ...$value): self
    {
        $this->processing = array_filter($value);

        return $this;
    }

    public function addProcessing(?FHIRSpecimenProcessing ...$value): self
    {
        $this->processing = array_filter(array_merge($this->processing, $value));

        return $this;
    }

    /**
     * @return FHIRSpecimenContainer[]
     */
    public function getContainer(): array
    {
        return $this->container;
    }

    public function setContainer(?FHIRSpecimenContainer ...$value): self
    {
        $this->container = array_filter($value);

        return $this;
    }

    public function addContainer(?FHIRSpecimenContainer ...$value): self
    {
        $this->container = array_filter(array_merge($this->container, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCondition(): array
    {
        return $this->condition;
    }

    public function setCondition(?FHIRCodeableConcept ...$value): self
    {
        $this->condition = array_filter($value);

        return $this;
    }

    public function addCondition(?FHIRCodeableConcept ...$value): self
    {
        $this->condition = array_filter(array_merge($this->condition, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }
}
