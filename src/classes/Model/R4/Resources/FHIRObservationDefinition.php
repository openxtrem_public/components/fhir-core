<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ObservationDefinition Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRObservationDefinitionInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRObservationDefinitionQualifiedInterval;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRObservationDefinitionQuantitativeDetails;

class FHIRObservationDefinition extends FHIRDomainResource implements FHIRObservationDefinitionInterface
{
    public const RESOURCE_NAME = 'ObservationDefinition';

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRCodeableConcept $code = null;

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRCode[] */
    protected array $permittedDataType = [];
    protected ?FHIRBoolean $multipleResultsAllowed = null;
    protected ?FHIRCodeableConcept $method = null;
    protected ?FHIRString $preferredReportName = null;
    protected ?FHIRObservationDefinitionQuantitativeDetails $quantitativeDetails = null;

    /** @var FHIRObservationDefinitionQualifiedInterval[] */
    protected array $qualifiedInterval = [];
    protected ?FHIRReference $validCodedValueSet = null;
    protected ?FHIRReference $normalCodedValueSet = null;
    protected ?FHIRReference $abnormalCodedValueSet = null;
    protected ?FHIRReference $criticalCodedValueSet = null;

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getPermittedDataType(): array
    {
        return $this->permittedDataType;
    }

    public function setPermittedDataType(string|FHIRCode|null ...$value): self
    {
        $this->permittedDataType = [];
        $this->addPermittedDataType(...$value);

        return $this;
    }

    public function addPermittedDataType(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->permittedDataType = array_filter(array_merge($this->permittedDataType, $values));

        return $this;
    }

    public function getMultipleResultsAllowed(): ?FHIRBoolean
    {
        return $this->multipleResultsAllowed;
    }

    public function setMultipleResultsAllowed(bool|FHIRBoolean|null $value): self
    {
        $this->multipleResultsAllowed = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getMethod(): ?FHIRCodeableConcept
    {
        return $this->method;
    }

    public function setMethod(?FHIRCodeableConcept $value): self
    {
        $this->method = $value;

        return $this;
    }

    public function getPreferredReportName(): ?FHIRString
    {
        return $this->preferredReportName;
    }

    public function setPreferredReportName(string|FHIRString|null $value): self
    {
        $this->preferredReportName = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getQuantitativeDetails(): ?FHIRObservationDefinitionQuantitativeDetails
    {
        return $this->quantitativeDetails;
    }

    public function setQuantitativeDetails(?FHIRObservationDefinitionQuantitativeDetails $value): self
    {
        $this->quantitativeDetails = $value;

        return $this;
    }

    /**
     * @return FHIRObservationDefinitionQualifiedInterval[]
     */
    public function getQualifiedInterval(): array
    {
        return $this->qualifiedInterval;
    }

    public function setQualifiedInterval(?FHIRObservationDefinitionQualifiedInterval ...$value): self
    {
        $this->qualifiedInterval = array_filter($value);

        return $this;
    }

    public function addQualifiedInterval(?FHIRObservationDefinitionQualifiedInterval ...$value): self
    {
        $this->qualifiedInterval = array_filter(array_merge($this->qualifiedInterval, $value));

        return $this;
    }

    public function getValidCodedValueSet(): ?FHIRReference
    {
        return $this->validCodedValueSet;
    }

    public function setValidCodedValueSet(?FHIRReference $value): self
    {
        $this->validCodedValueSet = $value;

        return $this;
    }

    public function getNormalCodedValueSet(): ?FHIRReference
    {
        return $this->normalCodedValueSet;
    }

    public function setNormalCodedValueSet(?FHIRReference $value): self
    {
        $this->normalCodedValueSet = $value;

        return $this;
    }

    public function getAbnormalCodedValueSet(): ?FHIRReference
    {
        return $this->abnormalCodedValueSet;
    }

    public function setAbnormalCodedValueSet(?FHIRReference $value): self
    {
        $this->abnormalCodedValueSet = $value;

        return $this;
    }

    public function getCriticalCodedValueSet(): ?FHIRReference
    {
        return $this->criticalCodedValueSet;
    }

    public function setCriticalCodedValueSet(?FHIRReference $value): self
    {
        $this->criticalCodedValueSet = $value;

        return $this;
    }
}
