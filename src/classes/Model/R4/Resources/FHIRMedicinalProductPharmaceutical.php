<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductPharmaceutical Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMedicinalProductPharmaceuticalInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicinalProductPharmaceuticalCharacteristics;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicinalProductPharmaceuticalRouteOfAdministration;

class FHIRMedicinalProductPharmaceutical extends FHIRDomainResource implements FHIRMedicinalProductPharmaceuticalInterface
{
    public const RESOURCE_NAME = 'MedicinalProductPharmaceutical';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCodeableConcept $administrableDoseForm = null;
    protected ?FHIRCodeableConcept $unitOfPresentation = null;

    /** @var FHIRReference[] */
    protected array $ingredient = [];

    /** @var FHIRReference[] */
    protected array $device = [];

    /** @var FHIRMedicinalProductPharmaceuticalCharacteristics[] */
    protected array $characteristics = [];

    /** @var FHIRMedicinalProductPharmaceuticalRouteOfAdministration[] */
    protected array $routeOfAdministration = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getAdministrableDoseForm(): ?FHIRCodeableConcept
    {
        return $this->administrableDoseForm;
    }

    public function setAdministrableDoseForm(?FHIRCodeableConcept $value): self
    {
        $this->administrableDoseForm = $value;

        return $this;
    }

    public function getUnitOfPresentation(): ?FHIRCodeableConcept
    {
        return $this->unitOfPresentation;
    }

    public function setUnitOfPresentation(?FHIRCodeableConcept $value): self
    {
        $this->unitOfPresentation = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getIngredient(): array
    {
        return $this->ingredient;
    }

    public function setIngredient(?FHIRReference ...$value): self
    {
        $this->ingredient = array_filter($value);

        return $this;
    }

    public function addIngredient(?FHIRReference ...$value): self
    {
        $this->ingredient = array_filter(array_merge($this->ingredient, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getDevice(): array
    {
        return $this->device;
    }

    public function setDevice(?FHIRReference ...$value): self
    {
        $this->device = array_filter($value);

        return $this;
    }

    public function addDevice(?FHIRReference ...$value): self
    {
        $this->device = array_filter(array_merge($this->device, $value));

        return $this;
    }

    /**
     * @return FHIRMedicinalProductPharmaceuticalCharacteristics[]
     */
    public function getCharacteristics(): array
    {
        return $this->characteristics;
    }

    public function setCharacteristics(?FHIRMedicinalProductPharmaceuticalCharacteristics ...$value): self
    {
        $this->characteristics = array_filter($value);

        return $this;
    }

    public function addCharacteristics(?FHIRMedicinalProductPharmaceuticalCharacteristics ...$value): self
    {
        $this->characteristics = array_filter(array_merge($this->characteristics, $value));

        return $this;
    }

    /**
     * @return FHIRMedicinalProductPharmaceuticalRouteOfAdministration[]
     */
    public function getRouteOfAdministration(): array
    {
        return $this->routeOfAdministration;
    }

    public function setRouteOfAdministration(?FHIRMedicinalProductPharmaceuticalRouteOfAdministration ...$value): self
    {
        $this->routeOfAdministration = array_filter($value);

        return $this;
    }

    public function addRouteOfAdministration(?FHIRMedicinalProductPharmaceuticalRouteOfAdministration ...$value): self
    {
        $this->routeOfAdministration = array_filter(array_merge($this->routeOfAdministration, $value));

        return $this;
    }
}
