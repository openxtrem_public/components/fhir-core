<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Device Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRDeviceInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRDeviceDeviceName;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRDeviceProperty;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRDeviceSpecialization;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRDeviceUdiCarrier;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRDeviceVersion;

class FHIRDevice extends FHIRDomainResource implements FHIRDeviceInterface
{
    public const RESOURCE_NAME = 'Device';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRReference $definition = null;

    /** @var FHIRDeviceUdiCarrier[] */
    protected array $udiCarrier = [];
    protected ?FHIRCode $status = null;

    /** @var FHIRCodeableConcept[] */
    protected array $statusReason = [];
    protected ?FHIRString $distinctIdentifier = null;
    protected ?FHIRString $manufacturer = null;
    protected ?FHIRDateTime $manufactureDate = null;
    protected ?FHIRDateTime $expirationDate = null;
    protected ?FHIRString $lotNumber = null;
    protected ?FHIRString $serialNumber = null;

    /** @var FHIRDeviceDeviceName[] */
    protected array $deviceName = [];
    protected ?FHIRString $modelNumber = null;
    protected ?FHIRString $partNumber = null;
    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRDeviceSpecialization[] */
    protected array $specialization = [];

    /** @var FHIRDeviceVersion[] */
    protected array $version = [];

    /** @var FHIRDeviceProperty[] */
    protected array $property = [];
    protected ?FHIRReference $patient = null;
    protected ?FHIRReference $owner = null;

    /** @var FHIRContactPoint[] */
    protected array $contact = [];
    protected ?FHIRReference $location = null;
    protected ?FHIRUri $url = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRCodeableConcept[] */
    protected array $safety = [];
    protected ?FHIRReference $parent = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getDefinition(): ?FHIRReference
    {
        return $this->definition;
    }

    public function setDefinition(?FHIRReference $value): self
    {
        $this->definition = $value;

        return $this;
    }

    /**
     * @return FHIRDeviceUdiCarrier[]
     */
    public function getUdiCarrier(): array
    {
        return $this->udiCarrier;
    }

    public function setUdiCarrier(?FHIRDeviceUdiCarrier ...$value): self
    {
        $this->udiCarrier = array_filter($value);

        return $this;
    }

    public function addUdiCarrier(?FHIRDeviceUdiCarrier ...$value): self
    {
        $this->udiCarrier = array_filter(array_merge($this->udiCarrier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getStatusReason(): array
    {
        return $this->statusReason;
    }

    public function setStatusReason(?FHIRCodeableConcept ...$value): self
    {
        $this->statusReason = array_filter($value);

        return $this;
    }

    public function addStatusReason(?FHIRCodeableConcept ...$value): self
    {
        $this->statusReason = array_filter(array_merge($this->statusReason, $value));

        return $this;
    }

    public function getDistinctIdentifier(): ?FHIRString
    {
        return $this->distinctIdentifier;
    }

    public function setDistinctIdentifier(string|FHIRString|null $value): self
    {
        $this->distinctIdentifier = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getManufacturer(): ?FHIRString
    {
        return $this->manufacturer;
    }

    public function setManufacturer(string|FHIRString|null $value): self
    {
        $this->manufacturer = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getManufactureDate(): ?FHIRDateTime
    {
        return $this->manufactureDate;
    }

    public function setManufactureDate(string|FHIRDateTime|null $value): self
    {
        $this->manufactureDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getExpirationDate(): ?FHIRDateTime
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(string|FHIRDateTime|null $value): self
    {
        $this->expirationDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getLotNumber(): ?FHIRString
    {
        return $this->lotNumber;
    }

    public function setLotNumber(string|FHIRString|null $value): self
    {
        $this->lotNumber = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSerialNumber(): ?FHIRString
    {
        return $this->serialNumber;
    }

    public function setSerialNumber(string|FHIRString|null $value): self
    {
        $this->serialNumber = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRDeviceDeviceName[]
     */
    public function getDeviceName(): array
    {
        return $this->deviceName;
    }

    public function setDeviceName(?FHIRDeviceDeviceName ...$value): self
    {
        $this->deviceName = array_filter($value);

        return $this;
    }

    public function addDeviceName(?FHIRDeviceDeviceName ...$value): self
    {
        $this->deviceName = array_filter(array_merge($this->deviceName, $value));

        return $this;
    }

    public function getModelNumber(): ?FHIRString
    {
        return $this->modelNumber;
    }

    public function setModelNumber(string|FHIRString|null $value): self
    {
        $this->modelNumber = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPartNumber(): ?FHIRString
    {
        return $this->partNumber;
    }

    public function setPartNumber(string|FHIRString|null $value): self
    {
        $this->partNumber = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRDeviceSpecialization[]
     */
    public function getSpecialization(): array
    {
        return $this->specialization;
    }

    public function setSpecialization(?FHIRDeviceSpecialization ...$value): self
    {
        $this->specialization = array_filter($value);

        return $this;
    }

    public function addSpecialization(?FHIRDeviceSpecialization ...$value): self
    {
        $this->specialization = array_filter(array_merge($this->specialization, $value));

        return $this;
    }

    /**
     * @return FHIRDeviceVersion[]
     */
    public function getVersion(): array
    {
        return $this->version;
    }

    public function setVersion(?FHIRDeviceVersion ...$value): self
    {
        $this->version = array_filter($value);

        return $this;
    }

    public function addVersion(?FHIRDeviceVersion ...$value): self
    {
        $this->version = array_filter(array_merge($this->version, $value));

        return $this;
    }

    /**
     * @return FHIRDeviceProperty[]
     */
    public function getProperty(): array
    {
        return $this->property;
    }

    public function setProperty(?FHIRDeviceProperty ...$value): self
    {
        $this->property = array_filter($value);

        return $this;
    }

    public function addProperty(?FHIRDeviceProperty ...$value): self
    {
        $this->property = array_filter(array_merge($this->property, $value));

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getOwner(): ?FHIRReference
    {
        return $this->owner;
    }

    public function setOwner(?FHIRReference $value): self
    {
        $this->owner = $value;

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactPoint ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactPoint ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSafety(): array
    {
        return $this->safety;
    }

    public function setSafety(?FHIRCodeableConcept ...$value): self
    {
        $this->safety = array_filter($value);

        return $this;
    }

    public function addSafety(?FHIRCodeableConcept ...$value): self
    {
        $this->safety = array_filter(array_merge($this->safety, $value));

        return $this;
    }

    public function getParent(): ?FHIRReference
    {
        return $this->parent;
    }

    public function setParent(?FHIRReference $value): self
    {
        $this->parent = $value;

        return $this;
    }
}
