<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExplanationOfBenefit Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRExplanationOfBenefitInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRExplanationOfBenefitAccident;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRExplanationOfBenefitAddItem;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRExplanationOfBenefitBenefitBalance;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRExplanationOfBenefitCareTeam;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRExplanationOfBenefitDiagnosis;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRExplanationOfBenefitInsurance;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRExplanationOfBenefitItem;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRExplanationOfBenefitItemAdjudication;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRExplanationOfBenefitPayee;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRExplanationOfBenefitPayment;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRExplanationOfBenefitProcedure;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRExplanationOfBenefitProcessNote;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRExplanationOfBenefitRelated;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRExplanationOfBenefitSupportingInfo;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRExplanationOfBenefitTotal;

class FHIRExplanationOfBenefit extends FHIRDomainResource implements FHIRExplanationOfBenefitInterface
{
    public const RESOURCE_NAME = 'ExplanationOfBenefit';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableConcept $subType = null;
    protected ?FHIRCode $use = null;
    protected ?FHIRReference $patient = null;
    protected ?FHIRPeriod $billablePeriod = null;
    protected ?FHIRDateTime $created = null;
    protected ?FHIRReference $enterer = null;
    protected ?FHIRReference $insurer = null;
    protected ?FHIRReference $provider = null;
    protected ?FHIRCodeableConcept $priority = null;
    protected ?FHIRCodeableConcept $fundsReserveRequested = null;
    protected ?FHIRCodeableConcept $fundsReserve = null;

    /** @var FHIRExplanationOfBenefitRelated[] */
    protected array $related = [];
    protected ?FHIRReference $prescription = null;
    protected ?FHIRReference $originalPrescription = null;
    protected ?FHIRExplanationOfBenefitPayee $payee = null;
    protected ?FHIRReference $referral = null;
    protected ?FHIRReference $facility = null;
    protected ?FHIRReference $claim = null;
    protected ?FHIRReference $claimResponse = null;
    protected ?FHIRCode $outcome = null;
    protected ?FHIRString $disposition = null;

    /** @var FHIRString[] */
    protected array $preAuthRef = [];

    /** @var FHIRPeriod[] */
    protected array $preAuthRefPeriod = [];

    /** @var FHIRExplanationOfBenefitCareTeam[] */
    protected array $careTeam = [];

    /** @var FHIRExplanationOfBenefitSupportingInfo[] */
    protected array $supportingInfo = [];

    /** @var FHIRExplanationOfBenefitDiagnosis[] */
    protected array $diagnosis = [];

    /** @var FHIRExplanationOfBenefitProcedure[] */
    protected array $procedure = [];
    protected ?FHIRPositiveInt $precedence = null;

    /** @var FHIRExplanationOfBenefitInsurance[] */
    protected array $insurance = [];
    protected ?FHIRExplanationOfBenefitAccident $accident = null;

    /** @var FHIRExplanationOfBenefitItem[] */
    protected array $item = [];

    /** @var FHIRExplanationOfBenefitAddItem[] */
    protected array $addItem = [];

    /** @var FHIRExplanationOfBenefitItemAdjudication[] */
    protected array $adjudication = [];

    /** @var FHIRExplanationOfBenefitTotal[] */
    protected array $total = [];
    protected ?FHIRExplanationOfBenefitPayment $payment = null;
    protected ?FHIRCodeableConcept $formCode = null;
    protected ?FHIRAttachment $form = null;

    /** @var FHIRExplanationOfBenefitProcessNote[] */
    protected array $processNote = [];
    protected ?FHIRPeriod $benefitPeriod = null;

    /** @var FHIRExplanationOfBenefitBenefitBalance[] */
    protected array $benefitBalance = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getSubType(): ?FHIRCodeableConcept
    {
        return $this->subType;
    }

    public function setSubType(?FHIRCodeableConcept $value): self
    {
        $this->subType = $value;

        return $this;
    }

    public function getUse(): ?FHIRCode
    {
        return $this->use;
    }

    public function setUse(string|FHIRCode|null $value): self
    {
        $this->use = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getBillablePeriod(): ?FHIRPeriod
    {
        return $this->billablePeriod;
    }

    public function setBillablePeriod(?FHIRPeriod $value): self
    {
        $this->billablePeriod = $value;

        return $this;
    }

    public function getCreated(): ?FHIRDateTime
    {
        return $this->created;
    }

    public function setCreated(string|FHIRDateTime|null $value): self
    {
        $this->created = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getEnterer(): ?FHIRReference
    {
        return $this->enterer;
    }

    public function setEnterer(?FHIRReference $value): self
    {
        $this->enterer = $value;

        return $this;
    }

    public function getInsurer(): ?FHIRReference
    {
        return $this->insurer;
    }

    public function setInsurer(?FHIRReference $value): self
    {
        $this->insurer = $value;

        return $this;
    }

    public function getProvider(): ?FHIRReference
    {
        return $this->provider;
    }

    public function setProvider(?FHIRReference $value): self
    {
        $this->provider = $value;

        return $this;
    }

    public function getPriority(): ?FHIRCodeableConcept
    {
        return $this->priority;
    }

    public function setPriority(?FHIRCodeableConcept $value): self
    {
        $this->priority = $value;

        return $this;
    }

    public function getFundsReserveRequested(): ?FHIRCodeableConcept
    {
        return $this->fundsReserveRequested;
    }

    public function setFundsReserveRequested(?FHIRCodeableConcept $value): self
    {
        $this->fundsReserveRequested = $value;

        return $this;
    }

    public function getFundsReserve(): ?FHIRCodeableConcept
    {
        return $this->fundsReserve;
    }

    public function setFundsReserve(?FHIRCodeableConcept $value): self
    {
        $this->fundsReserve = $value;

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitRelated[]
     */
    public function getRelated(): array
    {
        return $this->related;
    }

    public function setRelated(?FHIRExplanationOfBenefitRelated ...$value): self
    {
        $this->related = array_filter($value);

        return $this;
    }

    public function addRelated(?FHIRExplanationOfBenefitRelated ...$value): self
    {
        $this->related = array_filter(array_merge($this->related, $value));

        return $this;
    }

    public function getPrescription(): ?FHIRReference
    {
        return $this->prescription;
    }

    public function setPrescription(?FHIRReference $value): self
    {
        $this->prescription = $value;

        return $this;
    }

    public function getOriginalPrescription(): ?FHIRReference
    {
        return $this->originalPrescription;
    }

    public function setOriginalPrescription(?FHIRReference $value): self
    {
        $this->originalPrescription = $value;

        return $this;
    }

    public function getPayee(): ?FHIRExplanationOfBenefitPayee
    {
        return $this->payee;
    }

    public function setPayee(?FHIRExplanationOfBenefitPayee $value): self
    {
        $this->payee = $value;

        return $this;
    }

    public function getReferral(): ?FHIRReference
    {
        return $this->referral;
    }

    public function setReferral(?FHIRReference $value): self
    {
        $this->referral = $value;

        return $this;
    }

    public function getFacility(): ?FHIRReference
    {
        return $this->facility;
    }

    public function setFacility(?FHIRReference $value): self
    {
        $this->facility = $value;

        return $this;
    }

    public function getClaim(): ?FHIRReference
    {
        return $this->claim;
    }

    public function setClaim(?FHIRReference $value): self
    {
        $this->claim = $value;

        return $this;
    }

    public function getClaimResponse(): ?FHIRReference
    {
        return $this->claimResponse;
    }

    public function setClaimResponse(?FHIRReference $value): self
    {
        $this->claimResponse = $value;

        return $this;
    }

    public function getOutcome(): ?FHIRCode
    {
        return $this->outcome;
    }

    public function setOutcome(string|FHIRCode|null $value): self
    {
        $this->outcome = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDisposition(): ?FHIRString
    {
        return $this->disposition;
    }

    public function setDisposition(string|FHIRString|null $value): self
    {
        $this->disposition = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getPreAuthRef(): array
    {
        return $this->preAuthRef;
    }

    public function setPreAuthRef(string|FHIRString|null ...$value): self
    {
        $this->preAuthRef = [];
        $this->addPreAuthRef(...$value);

        return $this;
    }

    public function addPreAuthRef(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->preAuthRef = array_filter(array_merge($this->preAuthRef, $values));

        return $this;
    }

    /**
     * @return FHIRPeriod[]
     */
    public function getPreAuthRefPeriod(): array
    {
        return $this->preAuthRefPeriod;
    }

    public function setPreAuthRefPeriod(?FHIRPeriod ...$value): self
    {
        $this->preAuthRefPeriod = array_filter($value);

        return $this;
    }

    public function addPreAuthRefPeriod(?FHIRPeriod ...$value): self
    {
        $this->preAuthRefPeriod = array_filter(array_merge($this->preAuthRefPeriod, $value));

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitCareTeam[]
     */
    public function getCareTeam(): array
    {
        return $this->careTeam;
    }

    public function setCareTeam(?FHIRExplanationOfBenefitCareTeam ...$value): self
    {
        $this->careTeam = array_filter($value);

        return $this;
    }

    public function addCareTeam(?FHIRExplanationOfBenefitCareTeam ...$value): self
    {
        $this->careTeam = array_filter(array_merge($this->careTeam, $value));

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitSupportingInfo[]
     */
    public function getSupportingInfo(): array
    {
        return $this->supportingInfo;
    }

    public function setSupportingInfo(?FHIRExplanationOfBenefitSupportingInfo ...$value): self
    {
        $this->supportingInfo = array_filter($value);

        return $this;
    }

    public function addSupportingInfo(?FHIRExplanationOfBenefitSupportingInfo ...$value): self
    {
        $this->supportingInfo = array_filter(array_merge($this->supportingInfo, $value));

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitDiagnosis[]
     */
    public function getDiagnosis(): array
    {
        return $this->diagnosis;
    }

    public function setDiagnosis(?FHIRExplanationOfBenefitDiagnosis ...$value): self
    {
        $this->diagnosis = array_filter($value);

        return $this;
    }

    public function addDiagnosis(?FHIRExplanationOfBenefitDiagnosis ...$value): self
    {
        $this->diagnosis = array_filter(array_merge($this->diagnosis, $value));

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitProcedure[]
     */
    public function getProcedure(): array
    {
        return $this->procedure;
    }

    public function setProcedure(?FHIRExplanationOfBenefitProcedure ...$value): self
    {
        $this->procedure = array_filter($value);

        return $this;
    }

    public function addProcedure(?FHIRExplanationOfBenefitProcedure ...$value): self
    {
        $this->procedure = array_filter(array_merge($this->procedure, $value));

        return $this;
    }

    public function getPrecedence(): ?FHIRPositiveInt
    {
        return $this->precedence;
    }

    public function setPrecedence(int|FHIRPositiveInt|null $value): self
    {
        $this->precedence = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitInsurance[]
     */
    public function getInsurance(): array
    {
        return $this->insurance;
    }

    public function setInsurance(?FHIRExplanationOfBenefitInsurance ...$value): self
    {
        $this->insurance = array_filter($value);

        return $this;
    }

    public function addInsurance(?FHIRExplanationOfBenefitInsurance ...$value): self
    {
        $this->insurance = array_filter(array_merge($this->insurance, $value));

        return $this;
    }

    public function getAccident(): ?FHIRExplanationOfBenefitAccident
    {
        return $this->accident;
    }

    public function setAccident(?FHIRExplanationOfBenefitAccident $value): self
    {
        $this->accident = $value;

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitItem[]
     */
    public function getItem(): array
    {
        return $this->item;
    }

    public function setItem(?FHIRExplanationOfBenefitItem ...$value): self
    {
        $this->item = array_filter($value);

        return $this;
    }

    public function addItem(?FHIRExplanationOfBenefitItem ...$value): self
    {
        $this->item = array_filter(array_merge($this->item, $value));

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitAddItem[]
     */
    public function getAddItem(): array
    {
        return $this->addItem;
    }

    public function setAddItem(?FHIRExplanationOfBenefitAddItem ...$value): self
    {
        $this->addItem = array_filter($value);

        return $this;
    }

    public function addAddItem(?FHIRExplanationOfBenefitAddItem ...$value): self
    {
        $this->addItem = array_filter(array_merge($this->addItem, $value));

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitItemAdjudication[]
     */
    public function getAdjudication(): array
    {
        return $this->adjudication;
    }

    public function setAdjudication(?FHIRExplanationOfBenefitItemAdjudication ...$value): self
    {
        $this->adjudication = array_filter($value);

        return $this;
    }

    public function addAdjudication(?FHIRExplanationOfBenefitItemAdjudication ...$value): self
    {
        $this->adjudication = array_filter(array_merge($this->adjudication, $value));

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitTotal[]
     */
    public function getTotal(): array
    {
        return $this->total;
    }

    public function setTotal(?FHIRExplanationOfBenefitTotal ...$value): self
    {
        $this->total = array_filter($value);

        return $this;
    }

    public function addTotal(?FHIRExplanationOfBenefitTotal ...$value): self
    {
        $this->total = array_filter(array_merge($this->total, $value));

        return $this;
    }

    public function getPayment(): ?FHIRExplanationOfBenefitPayment
    {
        return $this->payment;
    }

    public function setPayment(?FHIRExplanationOfBenefitPayment $value): self
    {
        $this->payment = $value;

        return $this;
    }

    public function getFormCode(): ?FHIRCodeableConcept
    {
        return $this->formCode;
    }

    public function setFormCode(?FHIRCodeableConcept $value): self
    {
        $this->formCode = $value;

        return $this;
    }

    public function getForm(): ?FHIRAttachment
    {
        return $this->form;
    }

    public function setForm(?FHIRAttachment $value): self
    {
        $this->form = $value;

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitProcessNote[]
     */
    public function getProcessNote(): array
    {
        return $this->processNote;
    }

    public function setProcessNote(?FHIRExplanationOfBenefitProcessNote ...$value): self
    {
        $this->processNote = array_filter($value);

        return $this;
    }

    public function addProcessNote(?FHIRExplanationOfBenefitProcessNote ...$value): self
    {
        $this->processNote = array_filter(array_merge($this->processNote, $value));

        return $this;
    }

    public function getBenefitPeriod(): ?FHIRPeriod
    {
        return $this->benefitPeriod;
    }

    public function setBenefitPeriod(?FHIRPeriod $value): self
    {
        $this->benefitPeriod = $value;

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitBenefitBalance[]
     */
    public function getBenefitBalance(): array
    {
        return $this->benefitBalance;
    }

    public function setBenefitBalance(?FHIRExplanationOfBenefitBenefitBalance ...$value): self
    {
        $this->benefitBalance = array_filter($value);

        return $this;
    }

    public function addBenefitBalance(?FHIRExplanationOfBenefitBenefitBalance ...$value): self
    {
        $this->benefitBalance = array_filter(array_merge($this->benefitBalance, $value));

        return $this;
    }
}
