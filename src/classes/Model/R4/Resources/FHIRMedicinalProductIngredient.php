<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductIngredient Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMedicinalProductIngredientInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicinalProductIngredientSpecifiedSubstance;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicinalProductIngredientSubstance;

class FHIRMedicinalProductIngredient extends FHIRDomainResource implements FHIRMedicinalProductIngredientInterface
{
    public const RESOURCE_NAME = 'MedicinalProductIngredient';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRCodeableConcept $role = null;
    protected ?FHIRBoolean $allergenicIndicator = null;

    /** @var FHIRReference[] */
    protected array $manufacturer = [];

    /** @var FHIRMedicinalProductIngredientSpecifiedSubstance[] */
    protected array $specifiedSubstance = [];
    protected ?FHIRMedicinalProductIngredientSubstance $substance = null;

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getRole(): ?FHIRCodeableConcept
    {
        return $this->role;
    }

    public function setRole(?FHIRCodeableConcept $value): self
    {
        $this->role = $value;

        return $this;
    }

    public function getAllergenicIndicator(): ?FHIRBoolean
    {
        return $this->allergenicIndicator;
    }

    public function setAllergenicIndicator(bool|FHIRBoolean|null $value): self
    {
        $this->allergenicIndicator = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getManufacturer(): array
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?FHIRReference ...$value): self
    {
        $this->manufacturer = array_filter($value);

        return $this;
    }

    public function addManufacturer(?FHIRReference ...$value): self
    {
        $this->manufacturer = array_filter(array_merge($this->manufacturer, $value));

        return $this;
    }

    /**
     * @return FHIRMedicinalProductIngredientSpecifiedSubstance[]
     */
    public function getSpecifiedSubstance(): array
    {
        return $this->specifiedSubstance;
    }

    public function setSpecifiedSubstance(?FHIRMedicinalProductIngredientSpecifiedSubstance ...$value): self
    {
        $this->specifiedSubstance = array_filter($value);

        return $this;
    }

    public function addSpecifiedSubstance(?FHIRMedicinalProductIngredientSpecifiedSubstance ...$value): self
    {
        $this->specifiedSubstance = array_filter(array_merge($this->specifiedSubstance, $value));

        return $this;
    }

    public function getSubstance(): ?FHIRMedicinalProductIngredientSubstance
    {
        return $this->substance;
    }

    public function setSubstance(?FHIRMedicinalProductIngredientSubstance $value): self
    {
        $this->substance = $value;

        return $this;
    }
}
