<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductAuthorization Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMedicinalProductAuthorizationInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicinalProductAuthorizationJurisdictionalAuthorization;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicinalProductAuthorizationProcedure;

class FHIRMedicinalProductAuthorization extends FHIRDomainResource implements FHIRMedicinalProductAuthorizationInterface
{
    public const RESOURCE_NAME = 'MedicinalProductAuthorization';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRReference $subject = null;

    /** @var FHIRCodeableConcept[] */
    protected array $country = [];

    /** @var FHIRCodeableConcept[] */
    protected array $jurisdiction = [];
    protected ?FHIRCodeableConcept $status = null;
    protected ?FHIRDateTime $statusDate = null;
    protected ?FHIRDateTime $restoreDate = null;
    protected ?FHIRPeriod $validityPeriod = null;
    protected ?FHIRPeriod $dataExclusivityPeriod = null;
    protected ?FHIRDateTime $dateOfFirstAuthorization = null;
    protected ?FHIRDateTime $internationalBirthDate = null;
    protected ?FHIRCodeableConcept $legalBasis = null;

    /** @var FHIRMedicinalProductAuthorizationJurisdictionalAuthorization[] */
    protected array $jurisdictionalAuthorization = [];
    protected ?FHIRReference $holder = null;
    protected ?FHIRReference $regulator = null;
    protected ?FHIRMedicinalProductAuthorizationProcedure $procedure = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCountry(): array
    {
        return $this->country;
    }

    public function setCountry(?FHIRCodeableConcept ...$value): self
    {
        $this->country = array_filter($value);

        return $this;
    }

    public function addCountry(?FHIRCodeableConcept ...$value): self
    {
        $this->country = array_filter(array_merge($this->country, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getJurisdiction(): array
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter($value);

        return $this;
    }

    public function addJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter(array_merge($this->jurisdiction, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCodeableConcept
    {
        return $this->status;
    }

    public function setStatus(?FHIRCodeableConcept $value): self
    {
        $this->status = $value;

        return $this;
    }

    public function getStatusDate(): ?FHIRDateTime
    {
        return $this->statusDate;
    }

    public function setStatusDate(string|FHIRDateTime|null $value): self
    {
        $this->statusDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getRestoreDate(): ?FHIRDateTime
    {
        return $this->restoreDate;
    }

    public function setRestoreDate(string|FHIRDateTime|null $value): self
    {
        $this->restoreDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getValidityPeriod(): ?FHIRPeriod
    {
        return $this->validityPeriod;
    }

    public function setValidityPeriod(?FHIRPeriod $value): self
    {
        $this->validityPeriod = $value;

        return $this;
    }

    public function getDataExclusivityPeriod(): ?FHIRPeriod
    {
        return $this->dataExclusivityPeriod;
    }

    public function setDataExclusivityPeriod(?FHIRPeriod $value): self
    {
        $this->dataExclusivityPeriod = $value;

        return $this;
    }

    public function getDateOfFirstAuthorization(): ?FHIRDateTime
    {
        return $this->dateOfFirstAuthorization;
    }

    public function setDateOfFirstAuthorization(string|FHIRDateTime|null $value): self
    {
        $this->dateOfFirstAuthorization = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getInternationalBirthDate(): ?FHIRDateTime
    {
        return $this->internationalBirthDate;
    }

    public function setInternationalBirthDate(string|FHIRDateTime|null $value): self
    {
        $this->internationalBirthDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getLegalBasis(): ?FHIRCodeableConcept
    {
        return $this->legalBasis;
    }

    public function setLegalBasis(?FHIRCodeableConcept $value): self
    {
        $this->legalBasis = $value;

        return $this;
    }

    /**
     * @return FHIRMedicinalProductAuthorizationJurisdictionalAuthorization[]
     */
    public function getJurisdictionalAuthorization(): array
    {
        return $this->jurisdictionalAuthorization;
    }

    public function setJurisdictionalAuthorization(
        ?FHIRMedicinalProductAuthorizationJurisdictionalAuthorization ...$value,
    ): self
    {
        $this->jurisdictionalAuthorization = array_filter($value);

        return $this;
    }

    public function addJurisdictionalAuthorization(
        ?FHIRMedicinalProductAuthorizationJurisdictionalAuthorization ...$value,
    ): self
    {
        $this->jurisdictionalAuthorization = array_filter(array_merge($this->jurisdictionalAuthorization, $value));

        return $this;
    }

    public function getHolder(): ?FHIRReference
    {
        return $this->holder;
    }

    public function setHolder(?FHIRReference $value): self
    {
        $this->holder = $value;

        return $this;
    }

    public function getRegulator(): ?FHIRReference
    {
        return $this->regulator;
    }

    public function setRegulator(?FHIRReference $value): self
    {
        $this->regulator = $value;

        return $this;
    }

    public function getProcedure(): ?FHIRMedicinalProductAuthorizationProcedure
    {
        return $this->procedure;
    }

    public function setProcedure(?FHIRMedicinalProductAuthorizationProcedure $value): self
    {
        $this->procedure = $value;

        return $this;
    }
}
