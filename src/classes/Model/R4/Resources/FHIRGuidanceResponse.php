<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR GuidanceResponse Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRGuidanceResponseInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRDataRequirement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUri;

class FHIRGuidanceResponse extends FHIRDomainResource implements FHIRGuidanceResponseInterface
{
    public const RESOURCE_NAME = 'GuidanceResponse';

    protected ?FHIRIdentifier $requestIdentifier = null;

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected FHIRUri|FHIRCanonical|FHIRCodeableConcept|null $module = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $encounter = null;
    protected ?FHIRDateTime $occurrenceDateTime = null;
    protected ?FHIRReference $performer = null;

    /** @var FHIRCodeableConcept[] */
    protected array $reasonCode = [];

    /** @var FHIRReference[] */
    protected array $reasonReference = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRReference[] */
    protected array $evaluationMessage = [];
    protected ?FHIRReference $outputParameters = null;
    protected ?FHIRReference $result = null;

    /** @var FHIRDataRequirement[] */
    protected array $dataRequirement = [];

    public function getRequestIdentifier(): ?FHIRIdentifier
    {
        return $this->requestIdentifier;
    }

    public function setRequestIdentifier(?FHIRIdentifier $value): self
    {
        $this->requestIdentifier = $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getModule(): FHIRUri|FHIRCanonical|FHIRCodeableConcept|null
    {
        return $this->module;
    }

    public function setModule(FHIRUri|FHIRCanonical|FHIRCodeableConcept|null $value): self
    {
        $this->module = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getOccurrenceDateTime(): ?FHIRDateTime
    {
        return $this->occurrenceDateTime;
    }

    public function setOccurrenceDateTime(string|FHIRDateTime|null $value): self
    {
        $this->occurrenceDateTime = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPerformer(): ?FHIRReference
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRReference $value): self
    {
        $this->performer = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReasonCode(): array
    {
        return $this->reasonCode;
    }

    public function setReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter($value);

        return $this;
    }

    public function addReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter(array_merge($this->reasonCode, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReasonReference(): array
    {
        return $this->reasonReference;
    }

    public function setReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter($value);

        return $this;
    }

    public function addReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter(array_merge($this->reasonReference, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEvaluationMessage(): array
    {
        return $this->evaluationMessage;
    }

    public function setEvaluationMessage(?FHIRReference ...$value): self
    {
        $this->evaluationMessage = array_filter($value);

        return $this;
    }

    public function addEvaluationMessage(?FHIRReference ...$value): self
    {
        $this->evaluationMessage = array_filter(array_merge($this->evaluationMessage, $value));

        return $this;
    }

    public function getOutputParameters(): ?FHIRReference
    {
        return $this->outputParameters;
    }

    public function setOutputParameters(?FHIRReference $value): self
    {
        $this->outputParameters = $value;

        return $this;
    }

    public function getResult(): ?FHIRReference
    {
        return $this->result;
    }

    public function setResult(?FHIRReference $value): self
    {
        $this->result = $value;

        return $this;
    }

    /**
     * @return FHIRDataRequirement[]
     */
    public function getDataRequirement(): array
    {
        return $this->dataRequirement;
    }

    public function setDataRequirement(?FHIRDataRequirement ...$value): self
    {
        $this->dataRequirement = array_filter($value);

        return $this;
    }

    public function addDataRequirement(?FHIRDataRequirement ...$value): self
    {
        $this->dataRequirement = array_filter(array_merge($this->dataRequirement, $value));

        return $this;
    }
}
