<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImmunizationEvaluation Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRImmunizationEvaluationInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRImmunizationEvaluation extends FHIRDomainResource implements FHIRImmunizationEvaluationInterface
{
    public const RESOURCE_NAME = 'ImmunizationEvaluation';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRReference $patient = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRReference $authority = null;
    protected ?FHIRCodeableConcept $targetDisease = null;
    protected ?FHIRReference $immunizationEvent = null;
    protected ?FHIRCodeableConcept $doseStatus = null;

    /** @var FHIRCodeableConcept[] */
    protected array $doseStatusReason = [];
    protected ?FHIRString $description = null;
    protected ?FHIRString $series = null;
    protected FHIRPositiveInt|FHIRString|null $doseNumber = null;
    protected FHIRPositiveInt|FHIRString|null $seriesDoses = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getAuthority(): ?FHIRReference
    {
        return $this->authority;
    }

    public function setAuthority(?FHIRReference $value): self
    {
        $this->authority = $value;

        return $this;
    }

    public function getTargetDisease(): ?FHIRCodeableConcept
    {
        return $this->targetDisease;
    }

    public function setTargetDisease(?FHIRCodeableConcept $value): self
    {
        $this->targetDisease = $value;

        return $this;
    }

    public function getImmunizationEvent(): ?FHIRReference
    {
        return $this->immunizationEvent;
    }

    public function setImmunizationEvent(?FHIRReference $value): self
    {
        $this->immunizationEvent = $value;

        return $this;
    }

    public function getDoseStatus(): ?FHIRCodeableConcept
    {
        return $this->doseStatus;
    }

    public function setDoseStatus(?FHIRCodeableConcept $value): self
    {
        $this->doseStatus = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getDoseStatusReason(): array
    {
        return $this->doseStatusReason;
    }

    public function setDoseStatusReason(?FHIRCodeableConcept ...$value): self
    {
        $this->doseStatusReason = array_filter($value);

        return $this;
    }

    public function addDoseStatusReason(?FHIRCodeableConcept ...$value): self
    {
        $this->doseStatusReason = array_filter(array_merge($this->doseStatusReason, $value));

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSeries(): ?FHIRString
    {
        return $this->series;
    }

    public function setSeries(string|FHIRString|null $value): self
    {
        $this->series = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDoseNumber(): FHIRPositiveInt|FHIRString|null
    {
        return $this->doseNumber;
    }

    public function setDoseNumber(FHIRPositiveInt|FHIRString|null $value): self
    {
        $this->doseNumber = $value;

        return $this;
    }

    public function getSeriesDoses(): FHIRPositiveInt|FHIRString|null
    {
        return $this->seriesDoses;
    }

    public function setSeriesDoses(FHIRPositiveInt|FHIRString|null $value): self
    {
        $this->seriesDoses = $value;

        return $this;
    }
}
