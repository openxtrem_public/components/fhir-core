<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR OperationOutcome Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIROperationOutcomeInterface;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIROperationOutcomeIssue;

class FHIROperationOutcome extends FHIRDomainResource implements FHIROperationOutcomeInterface
{
    public const RESOURCE_NAME = 'OperationOutcome';

    /** @var FHIROperationOutcomeIssue[] */
    protected array $issue = [];

    /**
     * @return FHIROperationOutcomeIssue[]
     */
    public function getIssue(): array
    {
        return $this->issue;
    }

    public function setIssue(?FHIROperationOutcomeIssue ...$value): self
    {
        $this->issue = array_filter($value);

        return $this;
    }

    public function addIssue(?FHIROperationOutcomeIssue ...$value): self
    {
        $this->issue = array_filter(array_merge($this->issue, $value));

        return $this;
    }
}
