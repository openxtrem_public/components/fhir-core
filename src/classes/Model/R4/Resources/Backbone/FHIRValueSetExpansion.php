<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ValueSetExpansion Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRValueSetExpansionInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUri;

class FHIRValueSetExpansion extends FHIRBackboneElement implements FHIRValueSetExpansionInterface
{
    public const RESOURCE_NAME = 'ValueSet.expansion';

    protected ?FHIRUri $identifier = null;
    protected ?FHIRDateTime $timestamp = null;
    protected ?FHIRInteger $total = null;
    protected ?FHIRInteger $offset = null;

    /** @var FHIRValueSetExpansionParameter[] */
    protected array $parameter = [];

    /** @var FHIRValueSetExpansionContains[] */
    protected array $contains = [];

    public function getIdentifier(): ?FHIRUri
    {
        return $this->identifier;
    }

    public function setIdentifier(string|FHIRUri|null $value): self
    {
        $this->identifier = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getTimestamp(): ?FHIRDateTime
    {
        return $this->timestamp;
    }

    public function setTimestamp(string|FHIRDateTime|null $value): self
    {
        $this->timestamp = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getTotal(): ?FHIRInteger
    {
        return $this->total;
    }

    public function setTotal(int|FHIRInteger|null $value): self
    {
        $this->total = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getOffset(): ?FHIRInteger
    {
        return $this->offset;
    }

    public function setOffset(int|FHIRInteger|null $value): self
    {
        $this->offset = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRValueSetExpansionParameter[]
     */
    public function getParameter(): array
    {
        return $this->parameter;
    }

    public function setParameter(?FHIRValueSetExpansionParameter ...$value): self
    {
        $this->parameter = array_filter($value);

        return $this;
    }

    public function addParameter(?FHIRValueSetExpansionParameter ...$value): self
    {
        $this->parameter = array_filter(array_merge($this->parameter, $value));

        return $this;
    }

    /**
     * @return FHIRValueSetExpansionContains[]
     */
    public function getContains(): array
    {
        return $this->contains;
    }

    public function setContains(?FHIRValueSetExpansionContains ...$value): self
    {
        $this->contains = array_filter($value);

        return $this;
    }

    public function addContains(?FHIRValueSetExpansionContains ...$value): self
    {
        $this->contains = array_filter(array_merge($this->contains, $value));

        return $this;
    }
}
