<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR BundleEntrySearch Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRBundleEntrySearchInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDecimal;

class FHIRBundleEntrySearch extends FHIRBackboneElement implements FHIRBundleEntrySearchInterface
{
    public const RESOURCE_NAME = 'Bundle.entry.search';

    protected ?FHIRCode $mode = null;
    protected ?FHIRDecimal $score = null;

    public function getMode(): ?FHIRCode
    {
        return $this->mode;
    }

    public function setMode(string|FHIRCode|null $value): self
    {
        $this->mode = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getScore(): ?FHIRDecimal
    {
        return $this->score;
    }

    public function setScore(float|FHIRDecimal|null $value): self
    {
        $this->score = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }
}
