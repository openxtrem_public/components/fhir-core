<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductPharmaceuticalRouteOfAdministrationTargetSpeciesWithdrawalPeriod Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductPharmaceuticalRouteOfAdministrationTargetSpeciesWithdrawalPeriodInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRMedicinalProductPharmaceuticalRouteOfAdministrationTargetSpeciesWithdrawalPeriod extends FHIRBackboneElement implements FHIRMedicinalProductPharmaceuticalRouteOfAdministrationTargetSpeciesWithdrawalPeriodInterface
{
    public const RESOURCE_NAME = 'MedicinalProductPharmaceutical.routeOfAdministration.targetSpecies.withdrawalPeriod';

    protected ?FHIRCodeableConcept $tissue = null;
    protected ?FHIRQuantity $value = null;
    protected ?FHIRString $supportingInformation = null;

    public function getTissue(): ?FHIRCodeableConcept
    {
        return $this->tissue;
    }

    public function setTissue(?FHIRCodeableConcept $value): self
    {
        $this->tissue = $value;

        return $this;
    }

    public function getValue(): ?FHIRQuantity
    {
        return $this->value;
    }

    public function setValue(?FHIRQuantity $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getSupportingInformation(): ?FHIRString
    {
        return $this->supportingInformation;
    }

    public function setSupportingInformation(string|FHIRString|null $value): self
    {
        $this->supportingInformation = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
