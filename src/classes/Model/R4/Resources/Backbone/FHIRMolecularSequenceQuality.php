<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MolecularSequenceQuality Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMolecularSequenceQualityInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRInteger;

class FHIRMolecularSequenceQuality extends FHIRBackboneElement implements FHIRMolecularSequenceQualityInterface
{
    public const RESOURCE_NAME = 'MolecularSequence.quality';

    protected ?FHIRCode $type = null;
    protected ?FHIRCodeableConcept $standardSequence = null;
    protected ?FHIRInteger $start = null;
    protected ?FHIRInteger $end = null;
    protected ?FHIRQuantity $score = null;
    protected ?FHIRCodeableConcept $method = null;
    protected ?FHIRDecimal $truthTP = null;
    protected ?FHIRDecimal $queryTP = null;
    protected ?FHIRDecimal $truthFN = null;
    protected ?FHIRDecimal $queryFP = null;
    protected ?FHIRDecimal $gtFP = null;
    protected ?FHIRDecimal $precision = null;
    protected ?FHIRDecimal $recall = null;
    protected ?FHIRDecimal $fScore = null;
    protected ?FHIRMolecularSequenceQualityRoc $roc = null;

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getStandardSequence(): ?FHIRCodeableConcept
    {
        return $this->standardSequence;
    }

    public function setStandardSequence(?FHIRCodeableConcept $value): self
    {
        $this->standardSequence = $value;

        return $this;
    }

    public function getStart(): ?FHIRInteger
    {
        return $this->start;
    }

    public function setStart(int|FHIRInteger|null $value): self
    {
        $this->start = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getEnd(): ?FHIRInteger
    {
        return $this->end;
    }

    public function setEnd(int|FHIRInteger|null $value): self
    {
        $this->end = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getScore(): ?FHIRQuantity
    {
        return $this->score;
    }

    public function setScore(?FHIRQuantity $value): self
    {
        $this->score = $value;

        return $this;
    }

    public function getMethod(): ?FHIRCodeableConcept
    {
        return $this->method;
    }

    public function setMethod(?FHIRCodeableConcept $value): self
    {
        $this->method = $value;

        return $this;
    }

    public function getTruthTP(): ?FHIRDecimal
    {
        return $this->truthTP;
    }

    public function setTruthTP(float|FHIRDecimal|null $value): self
    {
        $this->truthTP = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getQueryTP(): ?FHIRDecimal
    {
        return $this->queryTP;
    }

    public function setQueryTP(float|FHIRDecimal|null $value): self
    {
        $this->queryTP = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getTruthFN(): ?FHIRDecimal
    {
        return $this->truthFN;
    }

    public function setTruthFN(float|FHIRDecimal|null $value): self
    {
        $this->truthFN = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getQueryFP(): ?FHIRDecimal
    {
        return $this->queryFP;
    }

    public function setQueryFP(float|FHIRDecimal|null $value): self
    {
        $this->queryFP = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getGtFP(): ?FHIRDecimal
    {
        return $this->gtFP;
    }

    public function setGtFP(float|FHIRDecimal|null $value): self
    {
        $this->gtFP = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getPrecision(): ?FHIRDecimal
    {
        return $this->precision;
    }

    public function setPrecision(float|FHIRDecimal|null $value): self
    {
        $this->precision = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getRecall(): ?FHIRDecimal
    {
        return $this->recall;
    }

    public function setRecall(float|FHIRDecimal|null $value): self
    {
        $this->recall = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getFScore(): ?FHIRDecimal
    {
        return $this->fScore;
    }

    public function setFScore(float|FHIRDecimal|null $value): self
    {
        $this->fScore = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getRoc(): ?FHIRMolecularSequenceQualityRoc
    {
        return $this->roc;
    }

    public function setRoc(?FHIRMolecularSequenceQualityRoc $value): self
    {
        $this->roc = $value;

        return $this;
    }
}
