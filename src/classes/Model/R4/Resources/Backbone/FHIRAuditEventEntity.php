<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AuditEventEntity Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAuditEventEntityInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBase64Binary;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRAuditEventEntity extends FHIRBackboneElement implements FHIRAuditEventEntityInterface
{
    public const RESOURCE_NAME = 'AuditEvent.entity';

    protected ?FHIRReference $what = null;
    protected ?FHIRCoding $type = null;
    protected ?FHIRCoding $role = null;
    protected ?FHIRCoding $lifecycle = null;

    /** @var FHIRCoding[] */
    protected array $securityLabel = [];
    protected ?FHIRString $name = null;
    protected ?FHIRString $description = null;
    protected ?FHIRBase64Binary $query = null;

    /** @var FHIRAuditEventEntityDetail[] */
    protected array $detail = [];

    public function getWhat(): ?FHIRReference
    {
        return $this->what;
    }

    public function setWhat(?FHIRReference $value): self
    {
        $this->what = $value;

        return $this;
    }

    public function getType(): ?FHIRCoding
    {
        return $this->type;
    }

    public function setType(?FHIRCoding $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getRole(): ?FHIRCoding
    {
        return $this->role;
    }

    public function setRole(?FHIRCoding $value): self
    {
        $this->role = $value;

        return $this;
    }

    public function getLifecycle(): ?FHIRCoding
    {
        return $this->lifecycle;
    }

    public function setLifecycle(?FHIRCoding $value): self
    {
        $this->lifecycle = $value;

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getSecurityLabel(): array
    {
        return $this->securityLabel;
    }

    public function setSecurityLabel(?FHIRCoding ...$value): self
    {
        $this->securityLabel = array_filter($value);

        return $this;
    }

    public function addSecurityLabel(?FHIRCoding ...$value): self
    {
        $this->securityLabel = array_filter(array_merge($this->securityLabel, $value));

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getQuery(): ?FHIRBase64Binary
    {
        return $this->query;
    }

    public function setQuery(string|FHIRBase64Binary|null $value): self
    {
        $this->query = is_string($value) ? (new FHIRBase64Binary())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAuditEventEntityDetail[]
     */
    public function getDetail(): array
    {
        return $this->detail;
    }

    public function setDetail(?FHIRAuditEventEntityDetail ...$value): self
    {
        $this->detail = array_filter($value);

        return $this;
    }

    public function addDetail(?FHIRAuditEventEntityDetail ...$value): self
    {
        $this->detail = array_filter(array_merge($this->detail, $value));

        return $this;
    }
}
