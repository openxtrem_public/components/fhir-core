<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestReportSetup Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestReportSetupInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;

class FHIRTestReportSetup extends FHIRBackboneElement implements FHIRTestReportSetupInterface
{
    public const RESOURCE_NAME = 'TestReport.setup';

    /** @var FHIRTestReportSetupAction[] */
    protected array $action = [];

    /**
     * @return FHIRTestReportSetupAction[]
     */
    public function getAction(): array
    {
        return $this->action;
    }

    public function setAction(?FHIRTestReportSetupAction ...$value): self
    {
        $this->action = array_filter($value);

        return $this;
    }

    public function addAction(?FHIRTestReportSetupAction ...$value): self
    {
        $this->action = array_filter(array_merge($this->action, $value));

        return $this;
    }
}
