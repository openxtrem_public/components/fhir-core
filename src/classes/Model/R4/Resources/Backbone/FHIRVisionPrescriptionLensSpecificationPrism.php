<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR VisionPrescriptionLensSpecificationPrism Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRVisionPrescriptionLensSpecificationPrismInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDecimal;

class FHIRVisionPrescriptionLensSpecificationPrism extends FHIRBackboneElement implements FHIRVisionPrescriptionLensSpecificationPrismInterface
{
    public const RESOURCE_NAME = 'VisionPrescription.lensSpecification.prism';

    protected ?FHIRDecimal $amount = null;
    protected ?FHIRCode $base = null;

    public function getAmount(): ?FHIRDecimal
    {
        return $this->amount;
    }

    public function setAmount(float|FHIRDecimal|null $value): self
    {
        $this->amount = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getBase(): ?FHIRCode
    {
        return $this->base;
    }

    public function setBase(string|FHIRCode|null $value): self
    {
        $this->base = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }
}
