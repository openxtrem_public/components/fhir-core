<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstancePolymerRepeatRepeatUnitStructuralRepresentation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstancePolymerRepeatRepeatUnitStructuralRepresentationInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRSubstancePolymerRepeatRepeatUnitStructuralRepresentation extends FHIRBackboneElement implements FHIRSubstancePolymerRepeatRepeatUnitStructuralRepresentationInterface
{
    public const RESOURCE_NAME = 'SubstancePolymer.repeat.repeatUnit.structuralRepresentation';

    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRString $representation = null;
    protected ?FHIRAttachment $attachment = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getRepresentation(): ?FHIRString
    {
        return $this->representation;
    }

    public function setRepresentation(string|FHIRString|null $value): self
    {
        $this->representation = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getAttachment(): ?FHIRAttachment
    {
        return $this->attachment;
    }

    public function setAttachment(?FHIRAttachment $value): self
    {
        $this->attachment = $value;

        return $this;
    }
}
