<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MessageHeaderDestination Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMessageHeaderDestinationInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUrl;

class FHIRMessageHeaderDestination extends FHIRBackboneElement implements FHIRMessageHeaderDestinationInterface
{
    public const RESOURCE_NAME = 'MessageHeader.destination';

    protected ?FHIRString $name = null;
    protected ?FHIRReference $target = null;
    protected ?FHIRUrl $endpoint = null;
    protected ?FHIRReference $receiver = null;

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTarget(): ?FHIRReference
    {
        return $this->target;
    }

    public function setTarget(?FHIRReference $value): self
    {
        $this->target = $value;

        return $this;
    }

    public function getEndpoint(): ?FHIRUrl
    {
        return $this->endpoint;
    }

    public function setEndpoint(string|FHIRUrl|null $value): self
    {
        $this->endpoint = is_string($value) ? (new FHIRUrl())->setValue($value) : $value;

        return $this;
    }

    public function getReceiver(): ?FHIRReference
    {
        return $this->receiver;
    }

    public function setReceiver(?FHIRReference $value): self
    {
        $this->receiver = $value;

        return $this;
    }
}
