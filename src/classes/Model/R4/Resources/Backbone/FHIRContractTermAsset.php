<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ContractTermAsset Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRContractTermAssetInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUnsignedInt;

class FHIRContractTermAsset extends FHIRBackboneElement implements FHIRContractTermAssetInterface
{
    public const RESOURCE_NAME = 'Contract.term.asset';

    protected ?FHIRCodeableConcept $scope = null;

    /** @var FHIRCodeableConcept[] */
    protected array $type = [];

    /** @var FHIRReference[] */
    protected array $typeReference = [];

    /** @var FHIRCodeableConcept[] */
    protected array $subtype = [];
    protected ?FHIRCoding $relationship = null;

    /** @var FHIRContractTermAssetContext[] */
    protected array $context = [];
    protected ?FHIRString $condition = null;

    /** @var FHIRCodeableConcept[] */
    protected array $periodType = [];

    /** @var FHIRPeriod[] */
    protected array $period = [];

    /** @var FHIRPeriod[] */
    protected array $usePeriod = [];
    protected ?FHIRString $text = null;

    /** @var FHIRString[] */
    protected array $linkId = [];

    /** @var FHIRContractTermOfferAnswer[] */
    protected array $answer = [];

    /** @var FHIRUnsignedInt[] */
    protected array $securityLabelNumber = [];

    /** @var FHIRContractTermAssetValuedItem[] */
    protected array $valuedItem = [];

    public function getScope(): ?FHIRCodeableConcept
    {
        return $this->scope;
    }

    public function setScope(?FHIRCodeableConcept $value): self
    {
        $this->scope = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getTypeReference(): array
    {
        return $this->typeReference;
    }

    public function setTypeReference(?FHIRReference ...$value): self
    {
        $this->typeReference = array_filter($value);

        return $this;
    }

    public function addTypeReference(?FHIRReference ...$value): self
    {
        $this->typeReference = array_filter(array_merge($this->typeReference, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSubtype(): array
    {
        return $this->subtype;
    }

    public function setSubtype(?FHIRCodeableConcept ...$value): self
    {
        $this->subtype = array_filter($value);

        return $this;
    }

    public function addSubtype(?FHIRCodeableConcept ...$value): self
    {
        $this->subtype = array_filter(array_merge($this->subtype, $value));

        return $this;
    }

    public function getRelationship(): ?FHIRCoding
    {
        return $this->relationship;
    }

    public function setRelationship(?FHIRCoding $value): self
    {
        $this->relationship = $value;

        return $this;
    }

    /**
     * @return FHIRContractTermAssetContext[]
     */
    public function getContext(): array
    {
        return $this->context;
    }

    public function setContext(?FHIRContractTermAssetContext ...$value): self
    {
        $this->context = array_filter($value);

        return $this;
    }

    public function addContext(?FHIRContractTermAssetContext ...$value): self
    {
        $this->context = array_filter(array_merge($this->context, $value));

        return $this;
    }

    public function getCondition(): ?FHIRString
    {
        return $this->condition;
    }

    public function setCondition(string|FHIRString|null $value): self
    {
        $this->condition = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getPeriodType(): array
    {
        return $this->periodType;
    }

    public function setPeriodType(?FHIRCodeableConcept ...$value): self
    {
        $this->periodType = array_filter($value);

        return $this;
    }

    public function addPeriodType(?FHIRCodeableConcept ...$value): self
    {
        $this->periodType = array_filter(array_merge($this->periodType, $value));

        return $this;
    }

    /**
     * @return FHIRPeriod[]
     */
    public function getPeriod(): array
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod ...$value): self
    {
        $this->period = array_filter($value);

        return $this;
    }

    public function addPeriod(?FHIRPeriod ...$value): self
    {
        $this->period = array_filter(array_merge($this->period, $value));

        return $this;
    }

    /**
     * @return FHIRPeriod[]
     */
    public function getUsePeriod(): array
    {
        return $this->usePeriod;
    }

    public function setUsePeriod(?FHIRPeriod ...$value): self
    {
        $this->usePeriod = array_filter($value);

        return $this;
    }

    public function addUsePeriod(?FHIRPeriod ...$value): self
    {
        $this->usePeriod = array_filter(array_merge($this->usePeriod, $value));

        return $this;
    }

    public function getText(): ?FHIRString
    {
        return $this->text;
    }

    public function setText(string|FHIRString|null $value): self
    {
        $this->text = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getLinkId(): array
    {
        return $this->linkId;
    }

    public function setLinkId(string|FHIRString|null ...$value): self
    {
        $this->linkId = [];
        $this->addLinkId(...$value);

        return $this;
    }

    public function addLinkId(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->linkId = array_filter(array_merge($this->linkId, $values));

        return $this;
    }

    /**
     * @return FHIRContractTermOfferAnswer[]
     */
    public function getAnswer(): array
    {
        return $this->answer;
    }

    public function setAnswer(?FHIRContractTermOfferAnswer ...$value): self
    {
        $this->answer = array_filter($value);

        return $this;
    }

    public function addAnswer(?FHIRContractTermOfferAnswer ...$value): self
    {
        $this->answer = array_filter(array_merge($this->answer, $value));

        return $this;
    }

    /**
     * @return FHIRUnsignedInt[]
     */
    public function getSecurityLabelNumber(): array
    {
        return $this->securityLabelNumber;
    }

    public function setSecurityLabelNumber(int|FHIRUnsignedInt|null ...$value): self
    {
        $this->securityLabelNumber = [];
        $this->addSecurityLabelNumber(...$value);

        return $this;
    }

    public function addSecurityLabelNumber(int|FHIRUnsignedInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRUnsignedInt())->setValue($v) : $v, $value);

        $this->securityLabelNumber = array_filter(array_merge($this->securityLabelNumber, $values));

        return $this;
    }

    /**
     * @return FHIRContractTermAssetValuedItem[]
     */
    public function getValuedItem(): array
    {
        return $this->valuedItem;
    }

    public function setValuedItem(?FHIRContractTermAssetValuedItem ...$value): self
    {
        $this->valuedItem = array_filter($value);

        return $this;
    }

    public function addValuedItem(?FHIRContractTermAssetValuedItem ...$value): self
    {
        $this->valuedItem = array_filter(array_merge($this->valuedItem, $value));

        return $this;
    }
}
