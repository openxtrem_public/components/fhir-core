<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EncounterDiagnosis Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREncounterDiagnosisInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRPositiveInt;

class FHIREncounterDiagnosis extends FHIRBackboneElement implements FHIREncounterDiagnosisInterface
{
    public const RESOURCE_NAME = 'Encounter.diagnosis';

    protected ?FHIRReference $condition = null;
    protected ?FHIRCodeableConcept $use = null;
    protected ?FHIRPositiveInt $rank = null;

    public function getCondition(): ?FHIRReference
    {
        return $this->condition;
    }

    public function setCondition(?FHIRReference $value): self
    {
        $this->condition = $value;

        return $this;
    }

    public function getUse(): ?FHIRCodeableConcept
    {
        return $this->use;
    }

    public function setUse(?FHIRCodeableConcept $value): self
    {
        $this->use = $value;

        return $this;
    }

    public function getRank(): ?FHIRPositiveInt
    {
        return $this->rank;
    }

    public function setRank(int|FHIRPositiveInt|null $value): self
    {
        $this->rank = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }
}
