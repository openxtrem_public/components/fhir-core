<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EffectEvidenceSynthesisResultsByExposure Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREffectEvidenceSynthesisResultsByExposureInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIREffectEvidenceSynthesisResultsByExposure extends FHIRBackboneElement implements FHIREffectEvidenceSynthesisResultsByExposureInterface
{
    public const RESOURCE_NAME = 'EffectEvidenceSynthesis.resultsByExposure';

    protected ?FHIRString $description = null;
    protected ?FHIRCode $exposureState = null;
    protected ?FHIRCodeableConcept $variantState = null;
    protected ?FHIRReference $riskEvidenceSynthesis = null;

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getExposureState(): ?FHIRCode
    {
        return $this->exposureState;
    }

    public function setExposureState(string|FHIRCode|null $value): self
    {
        $this->exposureState = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getVariantState(): ?FHIRCodeableConcept
    {
        return $this->variantState;
    }

    public function setVariantState(?FHIRCodeableConcept $value): self
    {
        $this->variantState = $value;

        return $this;
    }

    public function getRiskEvidenceSynthesis(): ?FHIRReference
    {
        return $this->riskEvidenceSynthesis;
    }

    public function setRiskEvidenceSynthesis(?FHIRReference $value): self
    {
        $this->riskEvidenceSynthesis = $value;

        return $this;
    }
}
