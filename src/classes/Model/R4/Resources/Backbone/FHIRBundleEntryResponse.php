<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR BundleEntryResponse Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRBundleEntryResponseInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R4\Resources\FHIRResource;

class FHIRBundleEntryResponse extends FHIRBackboneElement implements FHIRBundleEntryResponseInterface
{
    public const RESOURCE_NAME = 'Bundle.entry.response';

    protected ?FHIRString $status = null;
    protected ?FHIRUri $location = null;
    protected ?FHIRString $etag = null;
    protected ?FHIRInstant $lastModified = null;
    protected ?FHIRResource $outcome = null;

    public function getStatus(): ?FHIRString
    {
        return $this->status;
    }

    public function setStatus(string|FHIRString|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getLocation(): ?FHIRUri
    {
        return $this->location;
    }

    public function setLocation(string|FHIRUri|null $value): self
    {
        $this->location = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getEtag(): ?FHIRString
    {
        return $this->etag;
    }

    public function setEtag(string|FHIRString|null $value): self
    {
        $this->etag = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getLastModified(): ?FHIRInstant
    {
        return $this->lastModified;
    }

    public function setLastModified(string|FHIRInstant|null $value): self
    {
        $this->lastModified = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    public function getOutcome(): ?FHIRResource
    {
        return $this->outcome;
    }

    public function setOutcome(?FHIRResource $value): self
    {
        $this->outcome = $value;

        return $this;
    }
}
