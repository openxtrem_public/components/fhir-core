<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ContractTermAssetValuedItem Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRContractTermAssetValuedItemInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUnsignedInt;

class FHIRContractTermAssetValuedItem extends FHIRBackboneElement implements FHIRContractTermAssetValuedItemInterface
{
    public const RESOURCE_NAME = 'Contract.term.asset.valuedItem';

    protected FHIRCodeableConcept|FHIRReference|null $entity = null;
    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRDateTime $effectiveTime = null;
    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRMoney $unitPrice = null;
    protected ?FHIRDecimal $factor = null;
    protected ?FHIRDecimal $points = null;
    protected ?FHIRMoney $net = null;
    protected ?FHIRString $payment = null;
    protected ?FHIRDateTime $paymentDate = null;
    protected ?FHIRReference $responsible = null;
    protected ?FHIRReference $recipient = null;

    /** @var FHIRString[] */
    protected array $linkId = [];

    /** @var FHIRUnsignedInt[] */
    protected array $securityLabelNumber = [];

    public function getEntity(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->entity;
    }

    public function setEntity(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->entity = $value;

        return $this;
    }

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getEffectiveTime(): ?FHIRDateTime
    {
        return $this->effectiveTime;
    }

    public function setEffectiveTime(string|FHIRDateTime|null $value): self
    {
        $this->effectiveTime = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getUnitPrice(): ?FHIRMoney
    {
        return $this->unitPrice;
    }

    public function setUnitPrice(?FHIRMoney $value): self
    {
        $this->unitPrice = $value;

        return $this;
    }

    public function getFactor(): ?FHIRDecimal
    {
        return $this->factor;
    }

    public function setFactor(float|FHIRDecimal|null $value): self
    {
        $this->factor = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getPoints(): ?FHIRDecimal
    {
        return $this->points;
    }

    public function setPoints(float|FHIRDecimal|null $value): self
    {
        $this->points = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getNet(): ?FHIRMoney
    {
        return $this->net;
    }

    public function setNet(?FHIRMoney $value): self
    {
        $this->net = $value;

        return $this;
    }

    public function getPayment(): ?FHIRString
    {
        return $this->payment;
    }

    public function setPayment(string|FHIRString|null $value): self
    {
        $this->payment = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPaymentDate(): ?FHIRDateTime
    {
        return $this->paymentDate;
    }

    public function setPaymentDate(string|FHIRDateTime|null $value): self
    {
        $this->paymentDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getResponsible(): ?FHIRReference
    {
        return $this->responsible;
    }

    public function setResponsible(?FHIRReference $value): self
    {
        $this->responsible = $value;

        return $this;
    }

    public function getRecipient(): ?FHIRReference
    {
        return $this->recipient;
    }

    public function setRecipient(?FHIRReference $value): self
    {
        $this->recipient = $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getLinkId(): array
    {
        return $this->linkId;
    }

    public function setLinkId(string|FHIRString|null ...$value): self
    {
        $this->linkId = [];
        $this->addLinkId(...$value);

        return $this;
    }

    public function addLinkId(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->linkId = array_filter(array_merge($this->linkId, $values));

        return $this;
    }

    /**
     * @return FHIRUnsignedInt[]
     */
    public function getSecurityLabelNumber(): array
    {
        return $this->securityLabelNumber;
    }

    public function setSecurityLabelNumber(int|FHIRUnsignedInt|null ...$value): self
    {
        $this->securityLabelNumber = [];
        $this->addSecurityLabelNumber(...$value);

        return $this;
    }

    public function addSecurityLabelNumber(int|FHIRUnsignedInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRUnsignedInt())->setValue($v) : $v, $value);

        $this->securityLabelNumber = array_filter(array_merge($this->securityLabelNumber, $values));

        return $this;
    }
}
