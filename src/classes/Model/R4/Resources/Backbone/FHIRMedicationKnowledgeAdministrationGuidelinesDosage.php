<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledgeAdministrationGuidelinesDosage Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationKnowledgeAdministrationGuidelinesDosageInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRDosage;

class FHIRMedicationKnowledgeAdministrationGuidelinesDosage extends FHIRBackboneElement implements FHIRMedicationKnowledgeAdministrationGuidelinesDosageInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge.administrationGuidelines.dosage';

    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRDosage[] */
    protected array $dosage = [];

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRDosage[]
     */
    public function getDosage(): array
    {
        return $this->dosage;
    }

    public function setDosage(?FHIRDosage ...$value): self
    {
        $this->dosage = array_filter($value);

        return $this;
    }

    public function addDosage(?FHIRDosage ...$value): self
    {
        $this->dosage = array_filter(array_merge($this->dosage, $value));

        return $this;
    }
}
