<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AuditEventAgent Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAuditEventAgentInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUri;

class FHIRAuditEventAgent extends FHIRBackboneElement implements FHIRAuditEventAgentInterface
{
    public const RESOURCE_NAME = 'AuditEvent.agent';

    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRCodeableConcept[] */
    protected array $role = [];
    protected ?FHIRReference $who = null;
    protected ?FHIRString $altId = null;
    protected ?FHIRString $name = null;
    protected ?FHIRBoolean $requestor = null;
    protected ?FHIRReference $location = null;

    /** @var FHIRUri[] */
    protected array $policy = [];
    protected ?FHIRCoding $media = null;
    protected ?FHIRAuditEventAgentNetwork $network = null;

    /** @var FHIRCodeableConcept[] */
    protected array $purposeOfUse = [];

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getRole(): array
    {
        return $this->role;
    }

    public function setRole(?FHIRCodeableConcept ...$value): self
    {
        $this->role = array_filter($value);

        return $this;
    }

    public function addRole(?FHIRCodeableConcept ...$value): self
    {
        $this->role = array_filter(array_merge($this->role, $value));

        return $this;
    }

    public function getWho(): ?FHIRReference
    {
        return $this->who;
    }

    public function setWho(?FHIRReference $value): self
    {
        $this->who = $value;

        return $this;
    }

    public function getAltId(): ?FHIRString
    {
        return $this->altId;
    }

    public function setAltId(string|FHIRString|null $value): self
    {
        $this->altId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getRequestor(): ?FHIRBoolean
    {
        return $this->requestor;
    }

    public function setRequestor(bool|FHIRBoolean|null $value): self
    {
        $this->requestor = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    /**
     * @return FHIRUri[]
     */
    public function getPolicy(): array
    {
        return $this->policy;
    }

    public function setPolicy(string|FHIRUri|null ...$value): self
    {
        $this->policy = [];
        $this->addPolicy(...$value);

        return $this;
    }

    public function addPolicy(string|FHIRUri|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUri())->setValue($v) : $v, $value);

        $this->policy = array_filter(array_merge($this->policy, $values));

        return $this;
    }

    public function getMedia(): ?FHIRCoding
    {
        return $this->media;
    }

    public function setMedia(?FHIRCoding $value): self
    {
        $this->media = $value;

        return $this;
    }

    public function getNetwork(): ?FHIRAuditEventAgentNetwork
    {
        return $this->network;
    }

    public function setNetwork(?FHIRAuditEventAgentNetwork $value): self
    {
        $this->network = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getPurposeOfUse(): array
    {
        return $this->purposeOfUse;
    }

    public function setPurposeOfUse(?FHIRCodeableConcept ...$value): self
    {
        $this->purposeOfUse = array_filter($value);

        return $this;
    }

    public function addPurposeOfUse(?FHIRCodeableConcept ...$value): self
    {
        $this->purposeOfUse = array_filter(array_merge($this->purposeOfUse, $value));

        return $this;
    }
}
