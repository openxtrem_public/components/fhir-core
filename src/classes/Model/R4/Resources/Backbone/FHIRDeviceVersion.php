<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceVersion Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceVersionInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRDeviceVersion extends FHIRBackboneElement implements FHIRDeviceVersionInterface
{
    public const RESOURCE_NAME = 'Device.version';

    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRIdentifier $component = null;
    protected ?FHIRString $value = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getComponent(): ?FHIRIdentifier
    {
        return $this->component;
    }

    public function setComponent(?FHIRIdentifier $value): self
    {
        $this->component = $value;

        return $this;
    }

    public function getValue(): ?FHIRString
    {
        return $this->value;
    }

    public function setValue(string|FHIRString|null $value): self
    {
        $this->value = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
