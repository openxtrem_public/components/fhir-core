<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR GraphDefinitionLinkTargetCompartment Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRGraphDefinitionLinkTargetCompartmentInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRGraphDefinitionLinkTargetCompartment extends FHIRBackboneElement implements FHIRGraphDefinitionLinkTargetCompartmentInterface
{
    public const RESOURCE_NAME = 'GraphDefinition.link.target.compartment';

    protected ?FHIRCode $use = null;
    protected ?FHIRCode $code = null;
    protected ?FHIRCode $rule = null;
    protected ?FHIRString $expression = null;
    protected ?FHIRString $description = null;

    public function getUse(): ?FHIRCode
    {
        return $this->use;
    }

    public function setUse(string|FHIRCode|null $value): self
    {
        $this->use = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getRule(): ?FHIRCode
    {
        return $this->rule;
    }

    public function setRule(string|FHIRCode|null $value): self
    {
        $this->rule = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getExpression(): ?FHIRString
    {
        return $this->expression;
    }

    public function setExpression(string|FHIRString|null $value): self
    {
        $this->expression = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
