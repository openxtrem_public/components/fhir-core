<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR OperationDefinitionParameterBinding Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIROperationDefinitionParameterBindingInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;

class FHIROperationDefinitionParameterBinding extends FHIRBackboneElement implements FHIROperationDefinitionParameterBindingInterface
{
    public const RESOURCE_NAME = 'OperationDefinition.parameter.binding';

    protected ?FHIRCode $strength = null;
    protected ?FHIRCanonical $valueSet = null;

    public function getStrength(): ?FHIRCode
    {
        return $this->strength;
    }

    public function setStrength(string|FHIRCode|null $value): self
    {
        $this->strength = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getValueSet(): ?FHIRCanonical
    {
        return $this->valueSet;
    }

    public function setValueSet(string|FHIRCanonical|null $value): self
    {
        $this->valueSet = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }
}
