<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductManufacturingBusinessOperation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductManufacturingBusinessOperationInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;

class FHIRMedicinalProductManufacturingBusinessOperation extends FHIRBackboneElement implements FHIRMedicinalProductManufacturingBusinessOperationInterface
{
    public const RESOURCE_NAME = 'MedicinalProduct.manufacturingBusinessOperation';

    protected ?FHIRCodeableConcept $operationType = null;
    protected ?FHIRIdentifier $authorisationReferenceNumber = null;
    protected ?FHIRDateTime $effectiveDate = null;
    protected ?FHIRCodeableConcept $confidentialityIndicator = null;

    /** @var FHIRReference[] */
    protected array $manufacturer = [];
    protected ?FHIRReference $regulator = null;

    public function getOperationType(): ?FHIRCodeableConcept
    {
        return $this->operationType;
    }

    public function setOperationType(?FHIRCodeableConcept $value): self
    {
        $this->operationType = $value;

        return $this;
    }

    public function getAuthorisationReferenceNumber(): ?FHIRIdentifier
    {
        return $this->authorisationReferenceNumber;
    }

    public function setAuthorisationReferenceNumber(?FHIRIdentifier $value): self
    {
        $this->authorisationReferenceNumber = $value;

        return $this;
    }

    public function getEffectiveDate(): ?FHIRDateTime
    {
        return $this->effectiveDate;
    }

    public function setEffectiveDate(string|FHIRDateTime|null $value): self
    {
        $this->effectiveDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getConfidentialityIndicator(): ?FHIRCodeableConcept
    {
        return $this->confidentialityIndicator;
    }

    public function setConfidentialityIndicator(?FHIRCodeableConcept $value): self
    {
        $this->confidentialityIndicator = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getManufacturer(): array
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?FHIRReference ...$value): self
    {
        $this->manufacturer = array_filter($value);

        return $this;
    }

    public function addManufacturer(?FHIRReference ...$value): self
    {
        $this->manufacturer = array_filter(array_merge($this->manufacturer, $value));

        return $this;
    }

    public function getRegulator(): ?FHIRReference
    {
        return $this->regulator;
    }

    public function setRegulator(?FHIRReference $value): self
    {
        $this->regulator = $value;

        return $this;
    }
}
