<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImplementationGuideDefinitionPage Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImplementationGuideDefinitionPageInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUrl;

class FHIRImplementationGuideDefinitionPage extends FHIRBackboneElement implements FHIRImplementationGuideDefinitionPageInterface
{
    public const RESOURCE_NAME = 'ImplementationGuide.definition.page';

    protected FHIRUrl|FHIRReference|null $name = null;
    protected ?FHIRString $title = null;
    protected ?FHIRCode $generation = null;

    /** @var FHIRImplementationGuideDefinitionPage[] */
    protected array $page = [];

    public function getName(): FHIRUrl|FHIRReference|null
    {
        return $this->name;
    }

    public function setName(FHIRUrl|FHIRReference|null $value): self
    {
        $this->name = $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getGeneration(): ?FHIRCode
    {
        return $this->generation;
    }

    public function setGeneration(string|FHIRCode|null $value): self
    {
        $this->generation = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRImplementationGuideDefinitionPage[]
     */
    public function getPage(): array
    {
        return $this->page;
    }

    public function setPage(?FHIRImplementationGuideDefinitionPage ...$value): self
    {
        $this->page = array_filter($value);

        return $this;
    }

    public function addPage(?FHIRImplementationGuideDefinitionPage ...$value): self
    {
        $this->page = array_filter(array_merge($this->page, $value));

        return $this;
    }
}
