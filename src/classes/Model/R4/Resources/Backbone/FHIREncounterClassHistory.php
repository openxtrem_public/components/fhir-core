<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EncounterClassHistory Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREncounterClassHistoryInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;

class FHIREncounterClassHistory extends FHIRBackboneElement implements FHIREncounterClassHistoryInterface
{
    public const RESOURCE_NAME = 'Encounter.classHistory';

    protected ?FHIRCoding $class = null;
    protected ?FHIRPeriod $period = null;

    public function getClass(): ?FHIRCoding
    {
        return $this->class;
    }

    public function setClass(?FHIRCoding $value): self
    {
        $this->class = $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }
}
