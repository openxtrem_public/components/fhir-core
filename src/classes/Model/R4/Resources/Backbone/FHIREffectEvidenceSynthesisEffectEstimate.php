<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EffectEvidenceSynthesisEffectEstimate Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREffectEvidenceSynthesisEffectEstimateInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIREffectEvidenceSynthesisEffectEstimate extends FHIRBackboneElement implements FHIREffectEvidenceSynthesisEffectEstimateInterface
{
    public const RESOURCE_NAME = 'EffectEvidenceSynthesis.effectEstimate';

    protected ?FHIRString $description = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableConcept $variantState = null;
    protected ?FHIRDecimal $value = null;
    protected ?FHIRCodeableConcept $unitOfMeasure = null;

    /** @var FHIREffectEvidenceSynthesisEffectEstimatePrecisionEstimate[] */
    protected array $precisionEstimate = [];

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getVariantState(): ?FHIRCodeableConcept
    {
        return $this->variantState;
    }

    public function setVariantState(?FHIRCodeableConcept $value): self
    {
        $this->variantState = $value;

        return $this;
    }

    public function getValue(): ?FHIRDecimal
    {
        return $this->value;
    }

    public function setValue(float|FHIRDecimal|null $value): self
    {
        $this->value = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getUnitOfMeasure(): ?FHIRCodeableConcept
    {
        return $this->unitOfMeasure;
    }

    public function setUnitOfMeasure(?FHIRCodeableConcept $value): self
    {
        $this->unitOfMeasure = $value;

        return $this;
    }

    /**
     * @return FHIREffectEvidenceSynthesisEffectEstimatePrecisionEstimate[]
     */
    public function getPrecisionEstimate(): array
    {
        return $this->precisionEstimate;
    }

    public function setPrecisionEstimate(?FHIREffectEvidenceSynthesisEffectEstimatePrecisionEstimate ...$value): self
    {
        $this->precisionEstimate = array_filter($value);

        return $this;
    }

    public function addPrecisionEstimate(?FHIREffectEvidenceSynthesisEffectEstimatePrecisionEstimate ...$value): self
    {
        $this->precisionEstimate = array_filter(array_merge($this->precisionEstimate, $value));

        return $this;
    }
}
