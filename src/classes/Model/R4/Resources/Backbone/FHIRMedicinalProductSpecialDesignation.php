<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductSpecialDesignation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductSpecialDesignationInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;

class FHIRMedicinalProductSpecialDesignation extends FHIRBackboneElement implements FHIRMedicinalProductSpecialDesignationInterface
{
    public const RESOURCE_NAME = 'MedicinalProduct.specialDesignation';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableConcept $intendedUse = null;
    protected FHIRCodeableConcept|FHIRReference|null $indication = null;
    protected ?FHIRCodeableConcept $status = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRCodeableConcept $species = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getIntendedUse(): ?FHIRCodeableConcept
    {
        return $this->intendedUse;
    }

    public function setIntendedUse(?FHIRCodeableConcept $value): self
    {
        $this->intendedUse = $value;

        return $this;
    }

    public function getIndication(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->indication;
    }

    public function setIndication(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->indication = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCodeableConcept
    {
        return $this->status;
    }

    public function setStatus(?FHIRCodeableConcept $value): self
    {
        $this->status = $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getSpecies(): ?FHIRCodeableConcept
    {
        return $this->species;
    }

    public function setSpecies(?FHIRCodeableConcept $value): self
    {
        $this->species = $value;

        return $this;
    }
}
