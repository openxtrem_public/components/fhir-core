<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR RequestGroupAction Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRRequestGroupActionInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAge;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRRelatedArtifact;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRRequestGroupAction extends FHIRBackboneElement implements FHIRRequestGroupActionInterface
{
    public const RESOURCE_NAME = 'RequestGroup.action';

    protected ?FHIRString $prefix = null;
    protected ?FHIRString $title = null;
    protected ?FHIRString $description = null;
    protected ?FHIRString $textEquivalent = null;
    protected ?FHIRCode $priority = null;

    /** @var FHIRCodeableConcept[] */
    protected array $code = [];

    /** @var FHIRRelatedArtifact[] */
    protected array $documentation = [];

    /** @var FHIRRequestGroupActionCondition[] */
    protected array $condition = [];

    /** @var FHIRRequestGroupActionRelatedAction[] */
    protected array $relatedAction = [];
    protected FHIRDateTime|FHIRAge|FHIRPeriod|FHIRDuration|FHIRRange|FHIRTiming|null $timing = null;

    /** @var FHIRReference[] */
    protected array $participant = [];
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCode $groupingBehavior = null;
    protected ?FHIRCode $selectionBehavior = null;
    protected ?FHIRCode $requiredBehavior = null;
    protected ?FHIRCode $precheckBehavior = null;
    protected ?FHIRCode $cardinalityBehavior = null;
    protected ?FHIRReference $resource = null;

    /** @var FHIRRequestGroupAction[] */
    protected array $action = [];

    public function getPrefix(): ?FHIRString
    {
        return $this->prefix;
    }

    public function setPrefix(string|FHIRString|null $value): self
    {
        $this->prefix = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTextEquivalent(): ?FHIRString
    {
        return $this->textEquivalent;
    }

    public function setTextEquivalent(string|FHIRString|null $value): self
    {
        $this->textEquivalent = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPriority(): ?FHIRCode
    {
        return $this->priority;
    }

    public function setPriority(string|FHIRCode|null $value): self
    {
        $this->priority = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCode(): array
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept ...$value): self
    {
        $this->code = array_filter($value);

        return $this;
    }

    public function addCode(?FHIRCodeableConcept ...$value): self
    {
        $this->code = array_filter(array_merge($this->code, $value));

        return $this;
    }

    /**
     * @return FHIRRelatedArtifact[]
     */
    public function getDocumentation(): array
    {
        return $this->documentation;
    }

    public function setDocumentation(?FHIRRelatedArtifact ...$value): self
    {
        $this->documentation = array_filter($value);

        return $this;
    }

    public function addDocumentation(?FHIRRelatedArtifact ...$value): self
    {
        $this->documentation = array_filter(array_merge($this->documentation, $value));

        return $this;
    }

    /**
     * @return FHIRRequestGroupActionCondition[]
     */
    public function getCondition(): array
    {
        return $this->condition;
    }

    public function setCondition(?FHIRRequestGroupActionCondition ...$value): self
    {
        $this->condition = array_filter($value);

        return $this;
    }

    public function addCondition(?FHIRRequestGroupActionCondition ...$value): self
    {
        $this->condition = array_filter(array_merge($this->condition, $value));

        return $this;
    }

    /**
     * @return FHIRRequestGroupActionRelatedAction[]
     */
    public function getRelatedAction(): array
    {
        return $this->relatedAction;
    }

    public function setRelatedAction(?FHIRRequestGroupActionRelatedAction ...$value): self
    {
        $this->relatedAction = array_filter($value);

        return $this;
    }

    public function addRelatedAction(?FHIRRequestGroupActionRelatedAction ...$value): self
    {
        $this->relatedAction = array_filter(array_merge($this->relatedAction, $value));

        return $this;
    }

    public function getTiming(): FHIRDateTime|FHIRAge|FHIRPeriod|FHIRDuration|FHIRRange|FHIRTiming|null
    {
        return $this->timing;
    }

    public function setTiming(FHIRDateTime|FHIRAge|FHIRPeriod|FHIRDuration|FHIRRange|FHIRTiming|null $value): self
    {
        $this->timing = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getParticipant(): array
    {
        return $this->participant;
    }

    public function setParticipant(?FHIRReference ...$value): self
    {
        $this->participant = array_filter($value);

        return $this;
    }

    public function addParticipant(?FHIRReference ...$value): self
    {
        $this->participant = array_filter(array_merge($this->participant, $value));

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getGroupingBehavior(): ?FHIRCode
    {
        return $this->groupingBehavior;
    }

    public function setGroupingBehavior(string|FHIRCode|null $value): self
    {
        $this->groupingBehavior = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getSelectionBehavior(): ?FHIRCode
    {
        return $this->selectionBehavior;
    }

    public function setSelectionBehavior(string|FHIRCode|null $value): self
    {
        $this->selectionBehavior = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getRequiredBehavior(): ?FHIRCode
    {
        return $this->requiredBehavior;
    }

    public function setRequiredBehavior(string|FHIRCode|null $value): self
    {
        $this->requiredBehavior = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getPrecheckBehavior(): ?FHIRCode
    {
        return $this->precheckBehavior;
    }

    public function setPrecheckBehavior(string|FHIRCode|null $value): self
    {
        $this->precheckBehavior = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCardinalityBehavior(): ?FHIRCode
    {
        return $this->cardinalityBehavior;
    }

    public function setCardinalityBehavior(string|FHIRCode|null $value): self
    {
        $this->cardinalityBehavior = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getResource(): ?FHIRReference
    {
        return $this->resource;
    }

    public function setResource(?FHIRReference $value): self
    {
        $this->resource = $value;

        return $this;
    }

    /**
     * @return FHIRRequestGroupAction[]
     */
    public function getAction(): array
    {
        return $this->action;
    }

    public function setAction(?FHIRRequestGroupAction ...$value): self
    {
        $this->action = array_filter($value);

        return $this;
    }

    public function addAction(?FHIRRequestGroupAction ...$value): self
    {
        $this->action = array_filter(array_merge($this->action, $value));

        return $this;
    }
}
