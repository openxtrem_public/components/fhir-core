<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EffectEvidenceSynthesisCertainty Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREffectEvidenceSynthesisCertaintyInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;

class FHIREffectEvidenceSynthesisCertainty extends FHIRBackboneElement implements FHIREffectEvidenceSynthesisCertaintyInterface
{
    public const RESOURCE_NAME = 'EffectEvidenceSynthesis.certainty';

    /** @var FHIRCodeableConcept[] */
    protected array $rating = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIREffectEvidenceSynthesisCertaintyCertaintySubcomponent[] */
    protected array $certaintySubcomponent = [];

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getRating(): array
    {
        return $this->rating;
    }

    public function setRating(?FHIRCodeableConcept ...$value): self
    {
        $this->rating = array_filter($value);

        return $this;
    }

    public function addRating(?FHIRCodeableConcept ...$value): self
    {
        $this->rating = array_filter(array_merge($this->rating, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIREffectEvidenceSynthesisCertaintyCertaintySubcomponent[]
     */
    public function getCertaintySubcomponent(): array
    {
        return $this->certaintySubcomponent;
    }

    public function setCertaintySubcomponent(
        ?FHIREffectEvidenceSynthesisCertaintyCertaintySubcomponent ...$value,
    ): self
    {
        $this->certaintySubcomponent = array_filter($value);

        return $this;
    }

    public function addCertaintySubcomponent(
        ?FHIREffectEvidenceSynthesisCertaintyCertaintySubcomponent ...$value,
    ): self
    {
        $this->certaintySubcomponent = array_filter(array_merge($this->certaintySubcomponent, $value));

        return $this;
    }
}
