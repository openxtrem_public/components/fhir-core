<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CatalogEntryRelatedEntry Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCatalogEntryRelatedEntryInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;

class FHIRCatalogEntryRelatedEntry extends FHIRBackboneElement implements FHIRCatalogEntryRelatedEntryInterface
{
    public const RESOURCE_NAME = 'CatalogEntry.relatedEntry';

    protected ?FHIRCode $relationtype = null;
    protected ?FHIRReference $item = null;

    public function getRelationtype(): ?FHIRCode
    {
        return $this->relationtype;
    }

    public function setRelationtype(string|FHIRCode|null $value): self
    {
        $this->relationtype = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getItem(): ?FHIRReference
    {
        return $this->item;
    }

    public function setItem(?FHIRReference $value): self
    {
        $this->item = $value;

        return $this;
    }
}
