<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SpecimenDefinitionTypeTested Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSpecimenDefinitionTypeTestedInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRSpecimenDefinitionTypeTested extends FHIRBackboneElement implements FHIRSpecimenDefinitionTypeTestedInterface
{
    public const RESOURCE_NAME = 'SpecimenDefinition.typeTested';

    protected ?FHIRBoolean $isDerived = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCode $preference = null;
    protected ?FHIRSpecimenDefinitionTypeTestedContainer $container = null;
    protected ?FHIRString $requirement = null;
    protected ?FHIRDuration $retentionTime = null;

    /** @var FHIRCodeableConcept[] */
    protected array $rejectionCriterion = [];

    /** @var FHIRSpecimenDefinitionTypeTestedHandling[] */
    protected array $handling = [];

    public function getIsDerived(): ?FHIRBoolean
    {
        return $this->isDerived;
    }

    public function setIsDerived(bool|FHIRBoolean|null $value): self
    {
        $this->isDerived = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getPreference(): ?FHIRCode
    {
        return $this->preference;
    }

    public function setPreference(string|FHIRCode|null $value): self
    {
        $this->preference = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getContainer(): ?FHIRSpecimenDefinitionTypeTestedContainer
    {
        return $this->container;
    }

    public function setContainer(?FHIRSpecimenDefinitionTypeTestedContainer $value): self
    {
        $this->container = $value;

        return $this;
    }

    public function getRequirement(): ?FHIRString
    {
        return $this->requirement;
    }

    public function setRequirement(string|FHIRString|null $value): self
    {
        $this->requirement = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getRetentionTime(): ?FHIRDuration
    {
        return $this->retentionTime;
    }

    public function setRetentionTime(?FHIRDuration $value): self
    {
        $this->retentionTime = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getRejectionCriterion(): array
    {
        return $this->rejectionCriterion;
    }

    public function setRejectionCriterion(?FHIRCodeableConcept ...$value): self
    {
        $this->rejectionCriterion = array_filter($value);

        return $this;
    }

    public function addRejectionCriterion(?FHIRCodeableConcept ...$value): self
    {
        $this->rejectionCriterion = array_filter(array_merge($this->rejectionCriterion, $value));

        return $this;
    }

    /**
     * @return FHIRSpecimenDefinitionTypeTestedHandling[]
     */
    public function getHandling(): array
    {
        return $this->handling;
    }

    public function setHandling(?FHIRSpecimenDefinitionTypeTestedHandling ...$value): self
    {
        $this->handling = array_filter($value);

        return $this;
    }

    public function addHandling(?FHIRSpecimenDefinitionTypeTestedHandling ...$value): self
    {
        $this->handling = array_filter(array_merge($this->handling, $value));

        return $this;
    }
}
