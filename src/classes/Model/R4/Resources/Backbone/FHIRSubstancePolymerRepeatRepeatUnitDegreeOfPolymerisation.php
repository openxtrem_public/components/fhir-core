<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstancePolymerRepeatRepeatUnitDegreeOfPolymerisation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstancePolymerRepeatRepeatUnitDegreeOfPolymerisationInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRSubstanceAmount;

class FHIRSubstancePolymerRepeatRepeatUnitDegreeOfPolymerisation extends FHIRBackboneElement implements FHIRSubstancePolymerRepeatRepeatUnitDegreeOfPolymerisationInterface
{
    public const RESOURCE_NAME = 'SubstancePolymer.repeat.repeatUnit.degreeOfPolymerisation';

    protected ?FHIRCodeableConcept $degree = null;
    protected ?FHIRSubstanceAmount $amount = null;

    public function getDegree(): ?FHIRCodeableConcept
    {
        return $this->degree;
    }

    public function setDegree(?FHIRCodeableConcept $value): self
    {
        $this->degree = $value;

        return $this;
    }

    public function getAmount(): ?FHIRSubstanceAmount
    {
        return $this->amount;
    }

    public function setAmount(?FHIRSubstanceAmount $value): self
    {
        $this->amount = $value;

        return $this;
    }
}
