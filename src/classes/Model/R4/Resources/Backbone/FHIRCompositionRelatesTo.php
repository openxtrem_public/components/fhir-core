<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CompositionRelatesTo Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCompositionRelatesToInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;

class FHIRCompositionRelatesTo extends FHIRBackboneElement implements FHIRCompositionRelatesToInterface
{
    public const RESOURCE_NAME = 'Composition.relatesTo';

    protected ?FHIRCode $code = null;
    protected FHIRIdentifier|FHIRReference|null $target = null;

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getTarget(): FHIRIdentifier|FHIRReference|null
    {
        return $this->target;
    }

    public function setTarget(FHIRIdentifier|FHIRReference|null $value): self
    {
        $this->target = $value;

        return $this;
    }
}
