<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR StructureMapGroupRuleDependent Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRStructureMapGroupRuleDependentInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRStructureMapGroupRuleDependent extends FHIRBackboneElement implements FHIRStructureMapGroupRuleDependentInterface
{
    public const RESOURCE_NAME = 'StructureMap.group.rule.dependent';

    protected ?FHIRId $name = null;

    /** @var FHIRString[] */
    protected array $variable = [];

    public function getName(): ?FHIRId
    {
        return $this->name;
    }

    public function setName(string|FHIRId|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getVariable(): array
    {
        return $this->variable;
    }

    public function setVariable(string|FHIRString|null ...$value): self
    {
        $this->variable = [];
        $this->addVariable(...$value);

        return $this;
    }

    public function addVariable(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->variable = array_filter(array_merge($this->variable, $values));

        return $this;
    }
}
