<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductIngredientSpecifiedSubstanceStrength Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductIngredientSpecifiedSubstanceStrengthInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRRatio;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRMedicinalProductIngredientSpecifiedSubstanceStrength extends FHIRBackboneElement implements FHIRMedicinalProductIngredientSpecifiedSubstanceStrengthInterface
{
    public const RESOURCE_NAME = 'MedicinalProductIngredient.specifiedSubstance.strength';

    protected ?FHIRRatio $presentation = null;
    protected ?FHIRRatio $presentationLowLimit = null;
    protected ?FHIRRatio $concentration = null;
    protected ?FHIRRatio $concentrationLowLimit = null;
    protected ?FHIRString $measurementPoint = null;

    /** @var FHIRCodeableConcept[] */
    protected array $country = [];

    /** @var FHIRMedicinalProductIngredientSpecifiedSubstanceStrengthReferenceStrength[] */
    protected array $referenceStrength = [];

    public function getPresentation(): ?FHIRRatio
    {
        return $this->presentation;
    }

    public function setPresentation(?FHIRRatio $value): self
    {
        $this->presentation = $value;

        return $this;
    }

    public function getPresentationLowLimit(): ?FHIRRatio
    {
        return $this->presentationLowLimit;
    }

    public function setPresentationLowLimit(?FHIRRatio $value): self
    {
        $this->presentationLowLimit = $value;

        return $this;
    }

    public function getConcentration(): ?FHIRRatio
    {
        return $this->concentration;
    }

    public function setConcentration(?FHIRRatio $value): self
    {
        $this->concentration = $value;

        return $this;
    }

    public function getConcentrationLowLimit(): ?FHIRRatio
    {
        return $this->concentrationLowLimit;
    }

    public function setConcentrationLowLimit(?FHIRRatio $value): self
    {
        $this->concentrationLowLimit = $value;

        return $this;
    }

    public function getMeasurementPoint(): ?FHIRString
    {
        return $this->measurementPoint;
    }

    public function setMeasurementPoint(string|FHIRString|null $value): self
    {
        $this->measurementPoint = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCountry(): array
    {
        return $this->country;
    }

    public function setCountry(?FHIRCodeableConcept ...$value): self
    {
        $this->country = array_filter($value);

        return $this;
    }

    public function addCountry(?FHIRCodeableConcept ...$value): self
    {
        $this->country = array_filter(array_merge($this->country, $value));

        return $this;
    }

    /**
     * @return FHIRMedicinalProductIngredientSpecifiedSubstanceStrengthReferenceStrength[]
     */
    public function getReferenceStrength(): array
    {
        return $this->referenceStrength;
    }

    public function setReferenceStrength(
        ?FHIRMedicinalProductIngredientSpecifiedSubstanceStrengthReferenceStrength ...$value,
    ): self
    {
        $this->referenceStrength = array_filter($value);

        return $this;
    }

    public function addReferenceStrength(
        ?FHIRMedicinalProductIngredientSpecifiedSubstanceStrengthReferenceStrength ...$value,
    ): self
    {
        $this->referenceStrength = array_filter(array_merge($this->referenceStrength, $value));

        return $this;
    }
}
