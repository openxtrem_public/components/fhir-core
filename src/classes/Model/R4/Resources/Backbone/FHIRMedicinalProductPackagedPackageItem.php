<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductPackagedPackageItem Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductPackagedPackageItemInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRProdCharacteristic;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRProductShelfLife;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;

class FHIRMedicinalProductPackagedPackageItem extends FHIRBackboneElement implements FHIRMedicinalProductPackagedPackageItemInterface
{
    public const RESOURCE_NAME = 'MedicinalProductPackaged.packageItem';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRQuantity $quantity = null;

    /** @var FHIRCodeableConcept[] */
    protected array $material = [];

    /** @var FHIRCodeableConcept[] */
    protected array $alternateMaterial = [];

    /** @var FHIRReference[] */
    protected array $device = [];

    /** @var FHIRReference[] */
    protected array $manufacturedItem = [];

    /** @var FHIRMedicinalProductPackagedPackageItem[] */
    protected array $packageItem = [];
    protected ?FHIRProdCharacteristic $physicalCharacteristics = null;

    /** @var FHIRCodeableConcept[] */
    protected array $otherCharacteristics = [];

    /** @var FHIRProductShelfLife[] */
    protected array $shelfLifeStorage = [];

    /** @var FHIRReference[] */
    protected array $manufacturer = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getMaterial(): array
    {
        return $this->material;
    }

    public function setMaterial(?FHIRCodeableConcept ...$value): self
    {
        $this->material = array_filter($value);

        return $this;
    }

    public function addMaterial(?FHIRCodeableConcept ...$value): self
    {
        $this->material = array_filter(array_merge($this->material, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getAlternateMaterial(): array
    {
        return $this->alternateMaterial;
    }

    public function setAlternateMaterial(?FHIRCodeableConcept ...$value): self
    {
        $this->alternateMaterial = array_filter($value);

        return $this;
    }

    public function addAlternateMaterial(?FHIRCodeableConcept ...$value): self
    {
        $this->alternateMaterial = array_filter(array_merge($this->alternateMaterial, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getDevice(): array
    {
        return $this->device;
    }

    public function setDevice(?FHIRReference ...$value): self
    {
        $this->device = array_filter($value);

        return $this;
    }

    public function addDevice(?FHIRReference ...$value): self
    {
        $this->device = array_filter(array_merge($this->device, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getManufacturedItem(): array
    {
        return $this->manufacturedItem;
    }

    public function setManufacturedItem(?FHIRReference ...$value): self
    {
        $this->manufacturedItem = array_filter($value);

        return $this;
    }

    public function addManufacturedItem(?FHIRReference ...$value): self
    {
        $this->manufacturedItem = array_filter(array_merge($this->manufacturedItem, $value));

        return $this;
    }

    /**
     * @return FHIRMedicinalProductPackagedPackageItem[]
     */
    public function getPackageItem(): array
    {
        return $this->packageItem;
    }

    public function setPackageItem(?FHIRMedicinalProductPackagedPackageItem ...$value): self
    {
        $this->packageItem = array_filter($value);

        return $this;
    }

    public function addPackageItem(?FHIRMedicinalProductPackagedPackageItem ...$value): self
    {
        $this->packageItem = array_filter(array_merge($this->packageItem, $value));

        return $this;
    }

    public function getPhysicalCharacteristics(): ?FHIRProdCharacteristic
    {
        return $this->physicalCharacteristics;
    }

    public function setPhysicalCharacteristics(?FHIRProdCharacteristic $value): self
    {
        $this->physicalCharacteristics = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getOtherCharacteristics(): array
    {
        return $this->otherCharacteristics;
    }

    public function setOtherCharacteristics(?FHIRCodeableConcept ...$value): self
    {
        $this->otherCharacteristics = array_filter($value);

        return $this;
    }

    public function addOtherCharacteristics(?FHIRCodeableConcept ...$value): self
    {
        $this->otherCharacteristics = array_filter(array_merge($this->otherCharacteristics, $value));

        return $this;
    }

    /**
     * @return FHIRProductShelfLife[]
     */
    public function getShelfLifeStorage(): array
    {
        return $this->shelfLifeStorage;
    }

    public function setShelfLifeStorage(?FHIRProductShelfLife ...$value): self
    {
        $this->shelfLifeStorage = array_filter($value);

        return $this;
    }

    public function addShelfLifeStorage(?FHIRProductShelfLife ...$value): self
    {
        $this->shelfLifeStorage = array_filter(array_merge($this->shelfLifeStorage, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getManufacturer(): array
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?FHIRReference ...$value): self
    {
        $this->manufacturer = array_filter($value);

        return $this;
    }

    public function addManufacturer(?FHIRReference ...$value): self
    {
        $this->manufacturer = array_filter(array_merge($this->manufacturer, $value));

        return $this;
    }
}
