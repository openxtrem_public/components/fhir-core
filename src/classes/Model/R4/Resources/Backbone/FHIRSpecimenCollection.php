<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SpecimenCollection Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSpecimenCollectionInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;

class FHIRSpecimenCollection extends FHIRBackboneElement implements FHIRSpecimenCollectionInterface
{
    public const RESOURCE_NAME = 'Specimen.collection';

    protected ?FHIRReference $collector = null;
    protected FHIRDateTime|FHIRPeriod|null $collected = null;
    protected ?FHIRDuration $duration = null;
    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRCodeableConcept $method = null;
    protected ?FHIRCodeableConcept $bodySite = null;
    protected FHIRCodeableConcept|FHIRDuration|null $fastingStatus = null;

    public function getCollector(): ?FHIRReference
    {
        return $this->collector;
    }

    public function setCollector(?FHIRReference $value): self
    {
        $this->collector = $value;

        return $this;
    }

    public function getCollected(): FHIRDateTime|FHIRPeriod|null
    {
        return $this->collected;
    }

    public function setCollected(FHIRDateTime|FHIRPeriod|null $value): self
    {
        $this->collected = $value;

        return $this;
    }

    public function getDuration(): ?FHIRDuration
    {
        return $this->duration;
    }

    public function setDuration(?FHIRDuration $value): self
    {
        $this->duration = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getMethod(): ?FHIRCodeableConcept
    {
        return $this->method;
    }

    public function setMethod(?FHIRCodeableConcept $value): self
    {
        $this->method = $value;

        return $this;
    }

    public function getBodySite(): ?FHIRCodeableConcept
    {
        return $this->bodySite;
    }

    public function setBodySite(?FHIRCodeableConcept $value): self
    {
        $this->bodySite = $value;

        return $this;
    }

    public function getFastingStatus(): FHIRCodeableConcept|FHIRDuration|null
    {
        return $this->fastingStatus;
    }

    public function setFastingStatus(FHIRCodeableConcept|FHIRDuration|null $value): self
    {
        $this->fastingStatus = $value;

        return $this;
    }
}
