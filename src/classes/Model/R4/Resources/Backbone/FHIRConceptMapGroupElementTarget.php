<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ConceptMapGroupElementTarget Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRConceptMapGroupElementTargetInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRConceptMapGroupElementTarget extends FHIRBackboneElement implements FHIRConceptMapGroupElementTargetInterface
{
    public const RESOURCE_NAME = 'ConceptMap.group.element.target';

    protected ?FHIRCode $code = null;
    protected ?FHIRString $display = null;
    protected ?FHIRCode $equivalence = null;
    protected ?FHIRString $comment = null;

    /** @var FHIRConceptMapGroupElementTargetDependsOn[] */
    protected array $dependsOn = [];

    /** @var FHIRConceptMapGroupElementTargetDependsOn[] */
    protected array $product = [];

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDisplay(): ?FHIRString
    {
        return $this->display;
    }

    public function setDisplay(string|FHIRString|null $value): self
    {
        $this->display = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getEquivalence(): ?FHIRCode
    {
        return $this->equivalence;
    }

    public function setEquivalence(string|FHIRCode|null $value): self
    {
        $this->equivalence = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getComment(): ?FHIRString
    {
        return $this->comment;
    }

    public function setComment(string|FHIRString|null $value): self
    {
        $this->comment = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRConceptMapGroupElementTargetDependsOn[]
     */
    public function getDependsOn(): array
    {
        return $this->dependsOn;
    }

    public function setDependsOn(?FHIRConceptMapGroupElementTargetDependsOn ...$value): self
    {
        $this->dependsOn = array_filter($value);

        return $this;
    }

    public function addDependsOn(?FHIRConceptMapGroupElementTargetDependsOn ...$value): self
    {
        $this->dependsOn = array_filter(array_merge($this->dependsOn, $value));

        return $this;
    }

    /**
     * @return FHIRConceptMapGroupElementTargetDependsOn[]
     */
    public function getProduct(): array
    {
        return $this->product;
    }

    public function setProduct(?FHIRConceptMapGroupElementTargetDependsOn ...$value): self
    {
        $this->product = array_filter($value);

        return $this;
    }

    public function addProduct(?FHIRConceptMapGroupElementTargetDependsOn ...$value): self
    {
        $this->product = array_filter(array_merge($this->product, $value));

        return $this;
    }
}
