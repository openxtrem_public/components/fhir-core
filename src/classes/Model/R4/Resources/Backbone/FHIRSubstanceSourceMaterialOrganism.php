<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceSourceMaterialOrganism Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceSourceMaterialOrganismInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRSubstanceSourceMaterialOrganism extends FHIRBackboneElement implements FHIRSubstanceSourceMaterialOrganismInterface
{
    public const RESOURCE_NAME = 'SubstanceSourceMaterial.organism';

    protected ?FHIRCodeableConcept $family = null;
    protected ?FHIRCodeableConcept $genus = null;
    protected ?FHIRCodeableConcept $species = null;
    protected ?FHIRCodeableConcept $intraspecificType = null;
    protected ?FHIRString $intraspecificDescription = null;

    /** @var FHIRSubstanceSourceMaterialOrganismAuthor[] */
    protected array $author = [];
    protected ?FHIRSubstanceSourceMaterialOrganismHybrid $hybrid = null;
    protected ?FHIRSubstanceSourceMaterialOrganismOrganismGeneral $organismGeneral = null;

    public function getFamily(): ?FHIRCodeableConcept
    {
        return $this->family;
    }

    public function setFamily(?FHIRCodeableConcept $value): self
    {
        $this->family = $value;

        return $this;
    }

    public function getGenus(): ?FHIRCodeableConcept
    {
        return $this->genus;
    }

    public function setGenus(?FHIRCodeableConcept $value): self
    {
        $this->genus = $value;

        return $this;
    }

    public function getSpecies(): ?FHIRCodeableConcept
    {
        return $this->species;
    }

    public function setSpecies(?FHIRCodeableConcept $value): self
    {
        $this->species = $value;

        return $this;
    }

    public function getIntraspecificType(): ?FHIRCodeableConcept
    {
        return $this->intraspecificType;
    }

    public function setIntraspecificType(?FHIRCodeableConcept $value): self
    {
        $this->intraspecificType = $value;

        return $this;
    }

    public function getIntraspecificDescription(): ?FHIRString
    {
        return $this->intraspecificDescription;
    }

    public function setIntraspecificDescription(string|FHIRString|null $value): self
    {
        $this->intraspecificDescription = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRSubstanceSourceMaterialOrganismAuthor[]
     */
    public function getAuthor(): array
    {
        return $this->author;
    }

    public function setAuthor(?FHIRSubstanceSourceMaterialOrganismAuthor ...$value): self
    {
        $this->author = array_filter($value);

        return $this;
    }

    public function addAuthor(?FHIRSubstanceSourceMaterialOrganismAuthor ...$value): self
    {
        $this->author = array_filter(array_merge($this->author, $value));

        return $this;
    }

    public function getHybrid(): ?FHIRSubstanceSourceMaterialOrganismHybrid
    {
        return $this->hybrid;
    }

    public function setHybrid(?FHIRSubstanceSourceMaterialOrganismHybrid $value): self
    {
        $this->hybrid = $value;

        return $this;
    }

    public function getOrganismGeneral(): ?FHIRSubstanceSourceMaterialOrganismOrganismGeneral
    {
        return $this->organismGeneral;
    }

    public function setOrganismGeneral(?FHIRSubstanceSourceMaterialOrganismOrganismGeneral $value): self
    {
        $this->organismGeneral = $value;

        return $this;
    }
}
