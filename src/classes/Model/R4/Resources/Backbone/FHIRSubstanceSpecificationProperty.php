<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceSpecificationProperty Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceSpecificationPropertyInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRSubstanceSpecificationProperty extends FHIRBackboneElement implements FHIRSubstanceSpecificationPropertyInterface
{
    public const RESOURCE_NAME = 'SubstanceSpecification.property';

    protected ?FHIRCodeableConcept $category = null;
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRString $parameters = null;
    protected FHIRReference|FHIRCodeableConcept|null $definingSubstance = null;
    protected FHIRQuantity|FHIRString|null $amount = null;

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getParameters(): ?FHIRString
    {
        return $this->parameters;
    }

    public function setParameters(string|FHIRString|null $value): self
    {
        $this->parameters = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDefiningSubstance(): FHIRReference|FHIRCodeableConcept|null
    {
        return $this->definingSubstance;
    }

    public function setDefiningSubstance(FHIRReference|FHIRCodeableConcept|null $value): self
    {
        $this->definingSubstance = $value;

        return $this;
    }

    public function getAmount(): FHIRQuantity|FHIRString|null
    {
        return $this->amount;
    }

    public function setAmount(FHIRQuantity|FHIRString|null $value): self
    {
        $this->amount = $value;

        return $this;
    }
}
