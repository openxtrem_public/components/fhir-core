<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PersonLink Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRPersonLinkInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;

class FHIRPersonLink extends FHIRBackboneElement implements FHIRPersonLinkInterface
{
    public const RESOURCE_NAME = 'Person.link';

    protected ?FHIRReference $target = null;
    protected ?FHIRCode $assurance = null;

    public function getTarget(): ?FHIRReference
    {
        return $this->target;
    }

    public function setTarget(?FHIRReference $value): self
    {
        $this->target = $value;

        return $this;
    }

    public function getAssurance(): ?FHIRCode
    {
        return $this->assurance;
    }

    public function setAssurance(string|FHIRCode|null $value): self
    {
        $this->assurance = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }
}
