<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TerminologyCapabilitiesClosure Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTerminologyCapabilitiesClosureInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;

class FHIRTerminologyCapabilitiesClosure extends FHIRBackboneElement implements FHIRTerminologyCapabilitiesClosureInterface
{
    public const RESOURCE_NAME = 'TerminologyCapabilities.closure';

    protected ?FHIRBoolean $translation = null;

    public function getTranslation(): ?FHIRBoolean
    {
        return $this->translation;
    }

    public function setTranslation(bool|FHIRBoolean|null $value): self
    {
        $this->translation = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }
}
