<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TerminologyCapabilitiesExpansion Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTerminologyCapabilitiesExpansionInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRMarkdown;

class FHIRTerminologyCapabilitiesExpansion extends FHIRBackboneElement implements FHIRTerminologyCapabilitiesExpansionInterface
{
    public const RESOURCE_NAME = 'TerminologyCapabilities.expansion';

    protected ?FHIRBoolean $hierarchical = null;
    protected ?FHIRBoolean $paging = null;
    protected ?FHIRBoolean $incomplete = null;

    /** @var FHIRTerminologyCapabilitiesExpansionParameter[] */
    protected array $parameter = [];
    protected ?FHIRMarkdown $textFilter = null;

    public function getHierarchical(): ?FHIRBoolean
    {
        return $this->hierarchical;
    }

    public function setHierarchical(bool|FHIRBoolean|null $value): self
    {
        $this->hierarchical = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getPaging(): ?FHIRBoolean
    {
        return $this->paging;
    }

    public function setPaging(bool|FHIRBoolean|null $value): self
    {
        $this->paging = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getIncomplete(): ?FHIRBoolean
    {
        return $this->incomplete;
    }

    public function setIncomplete(bool|FHIRBoolean|null $value): self
    {
        $this->incomplete = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRTerminologyCapabilitiesExpansionParameter[]
     */
    public function getParameter(): array
    {
        return $this->parameter;
    }

    public function setParameter(?FHIRTerminologyCapabilitiesExpansionParameter ...$value): self
    {
        $this->parameter = array_filter($value);

        return $this;
    }

    public function addParameter(?FHIRTerminologyCapabilitiesExpansionParameter ...$value): self
    {
        $this->parameter = array_filter(array_merge($this->parameter, $value));

        return $this;
    }

    public function getTextFilter(): ?FHIRMarkdown
    {
        return $this->textFilter;
    }

    public function setTextFilter(string|FHIRMarkdown|null $value): self
    {
        $this->textFilter = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }
}
