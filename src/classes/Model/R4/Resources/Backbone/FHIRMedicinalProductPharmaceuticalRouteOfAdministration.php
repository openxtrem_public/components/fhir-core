<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductPharmaceuticalRouteOfAdministration Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductPharmaceuticalRouteOfAdministrationInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRRatio;

class FHIRMedicinalProductPharmaceuticalRouteOfAdministration extends FHIRBackboneElement implements FHIRMedicinalProductPharmaceuticalRouteOfAdministrationInterface
{
    public const RESOURCE_NAME = 'MedicinalProductPharmaceutical.routeOfAdministration';

    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRQuantity $firstDose = null;
    protected ?FHIRQuantity $maxSingleDose = null;
    protected ?FHIRQuantity $maxDosePerDay = null;
    protected ?FHIRRatio $maxDosePerTreatmentPeriod = null;
    protected ?FHIRDuration $maxTreatmentPeriod = null;

    /** @var FHIRMedicinalProductPharmaceuticalRouteOfAdministrationTargetSpecies[] */
    protected array $targetSpecies = [];

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getFirstDose(): ?FHIRQuantity
    {
        return $this->firstDose;
    }

    public function setFirstDose(?FHIRQuantity $value): self
    {
        $this->firstDose = $value;

        return $this;
    }

    public function getMaxSingleDose(): ?FHIRQuantity
    {
        return $this->maxSingleDose;
    }

    public function setMaxSingleDose(?FHIRQuantity $value): self
    {
        $this->maxSingleDose = $value;

        return $this;
    }

    public function getMaxDosePerDay(): ?FHIRQuantity
    {
        return $this->maxDosePerDay;
    }

    public function setMaxDosePerDay(?FHIRQuantity $value): self
    {
        $this->maxDosePerDay = $value;

        return $this;
    }

    public function getMaxDosePerTreatmentPeriod(): ?FHIRRatio
    {
        return $this->maxDosePerTreatmentPeriod;
    }

    public function setMaxDosePerTreatmentPeriod(?FHIRRatio $value): self
    {
        $this->maxDosePerTreatmentPeriod = $value;

        return $this;
    }

    public function getMaxTreatmentPeriod(): ?FHIRDuration
    {
        return $this->maxTreatmentPeriod;
    }

    public function setMaxTreatmentPeriod(?FHIRDuration $value): self
    {
        $this->maxTreatmentPeriod = $value;

        return $this;
    }

    /**
     * @return FHIRMedicinalProductPharmaceuticalRouteOfAdministrationTargetSpecies[]
     */
    public function getTargetSpecies(): array
    {
        return $this->targetSpecies;
    }

    public function setTargetSpecies(
        ?FHIRMedicinalProductPharmaceuticalRouteOfAdministrationTargetSpecies ...$value,
    ): self
    {
        $this->targetSpecies = array_filter($value);

        return $this;
    }

    public function addTargetSpecies(
        ?FHIRMedicinalProductPharmaceuticalRouteOfAdministrationTargetSpecies ...$value,
    ): self
    {
        $this->targetSpecies = array_filter(array_merge($this->targetSpecies, $value));

        return $this;
    }
}
