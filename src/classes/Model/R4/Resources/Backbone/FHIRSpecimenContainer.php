<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SpecimenContainer Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSpecimenContainerInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRSpecimenContainer extends FHIRBackboneElement implements FHIRSpecimenContainerInterface
{
    public const RESOURCE_NAME = 'Specimen.container';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRString $description = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRQuantity $capacity = null;
    protected ?FHIRQuantity $specimenQuantity = null;
    protected FHIRCodeableConcept|FHIRReference|null $additive = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getCapacity(): ?FHIRQuantity
    {
        return $this->capacity;
    }

    public function setCapacity(?FHIRQuantity $value): self
    {
        $this->capacity = $value;

        return $this;
    }

    public function getSpecimenQuantity(): ?FHIRQuantity
    {
        return $this->specimenQuantity;
    }

    public function setSpecimenQuantity(?FHIRQuantity $value): self
    {
        $this->specimenQuantity = $value;

        return $this;
    }

    public function getAdditive(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->additive;
    }

    public function setAdditive(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->additive = $value;

        return $this;
    }
}
