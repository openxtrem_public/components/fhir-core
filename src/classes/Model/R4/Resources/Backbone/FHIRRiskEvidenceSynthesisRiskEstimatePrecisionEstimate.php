<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR RiskEvidenceSynthesisRiskEstimatePrecisionEstimate Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRRiskEvidenceSynthesisRiskEstimatePrecisionEstimateInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDecimal;

class FHIRRiskEvidenceSynthesisRiskEstimatePrecisionEstimate extends FHIRBackboneElement implements FHIRRiskEvidenceSynthesisRiskEstimatePrecisionEstimateInterface
{
    public const RESOURCE_NAME = 'RiskEvidenceSynthesis.riskEstimate.precisionEstimate';

    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRDecimal $level = null;
    protected ?FHIRDecimal $from = null;
    protected ?FHIRDecimal $to = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getLevel(): ?FHIRDecimal
    {
        return $this->level;
    }

    public function setLevel(float|FHIRDecimal|null $value): self
    {
        $this->level = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getFrom(): ?FHIRDecimal
    {
        return $this->from;
    }

    public function setFrom(float|FHIRDecimal|null $value): self
    {
        $this->from = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getTo(): ?FHIRDecimal
    {
        return $this->to;
    }

    public function setTo(float|FHIRDecimal|null $value): self
    {
        $this->to = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }
}
