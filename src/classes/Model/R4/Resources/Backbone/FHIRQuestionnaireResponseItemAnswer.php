<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR QuestionnaireResponseItemAnswer Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRQuestionnaireResponseItemAnswerInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRElement;

class FHIRQuestionnaireResponseItemAnswer extends FHIRBackboneElement implements FHIRQuestionnaireResponseItemAnswerInterface
{
    public const RESOURCE_NAME = 'QuestionnaireResponse.item.answer';

    protected ?FHIRElement $value = null;

    /** @var FHIRQuestionnaireResponseItem[] */
    protected array $item = [];

    public function getValue(): ?FHIRElement
    {
        return $this->value;
    }

    public function setValue(?FHIRElement $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return FHIRQuestionnaireResponseItem[]
     */
    public function getItem(): array
    {
        return $this->item;
    }

    public function setItem(?FHIRQuestionnaireResponseItem ...$value): self
    {
        $this->item = array_filter($value);

        return $this;
    }

    public function addItem(?FHIRQuestionnaireResponseItem ...$value): self
    {
        $this->item = array_filter(array_merge($this->item, $value));

        return $this;
    }
}
