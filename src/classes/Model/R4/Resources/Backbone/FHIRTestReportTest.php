<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestReportTest Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestReportTestInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRTestReportTest extends FHIRBackboneElement implements FHIRTestReportTestInterface
{
    public const RESOURCE_NAME = 'TestReport.test';

    protected ?FHIRString $name = null;
    protected ?FHIRString $description = null;

    /** @var FHIRTestReportTestAction[] */
    protected array $action = [];

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRTestReportTestAction[]
     */
    public function getAction(): array
    {
        return $this->action;
    }

    public function setAction(?FHIRTestReportTestAction ...$value): self
    {
        $this->action = array_filter($value);

        return $this;
    }

    public function addAction(?FHIRTestReportTestAction ...$value): self
    {
        $this->action = array_filter(array_merge($this->action, $value));

        return $this;
    }
}
