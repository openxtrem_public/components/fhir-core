<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceSpecificationStructureIsotope Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceSpecificationStructureIsotopeInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;

class FHIRSubstanceSpecificationStructureIsotope extends FHIRBackboneElement implements FHIRSubstanceSpecificationStructureIsotopeInterface
{
    public const RESOURCE_NAME = 'SubstanceSpecification.structure.isotope';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRCodeableConcept $name = null;
    protected ?FHIRCodeableConcept $substitution = null;
    protected ?FHIRQuantity $halfLife = null;
    protected ?FHIRSubstanceSpecificationStructureIsotopeMolecularWeight $molecularWeight = null;

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getName(): ?FHIRCodeableConcept
    {
        return $this->name;
    }

    public function setName(?FHIRCodeableConcept $value): self
    {
        $this->name = $value;

        return $this;
    }

    public function getSubstitution(): ?FHIRCodeableConcept
    {
        return $this->substitution;
    }

    public function setSubstitution(?FHIRCodeableConcept $value): self
    {
        $this->substitution = $value;

        return $this;
    }

    public function getHalfLife(): ?FHIRQuantity
    {
        return $this->halfLife;
    }

    public function setHalfLife(?FHIRQuantity $value): self
    {
        $this->halfLife = $value;

        return $this;
    }

    public function getMolecularWeight(): ?FHIRSubstanceSpecificationStructureIsotopeMolecularWeight
    {
        return $this->molecularWeight;
    }

    public function setMolecularWeight(?FHIRSubstanceSpecificationStructureIsotopeMolecularWeight $value): self
    {
        $this->molecularWeight = $value;

        return $this;
    }
}
