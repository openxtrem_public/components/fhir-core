<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceProteinSubunit Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceProteinSubunitInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRSubstanceProteinSubunit extends FHIRBackboneElement implements FHIRSubstanceProteinSubunitInterface
{
    public const RESOURCE_NAME = 'SubstanceProtein.subunit';

    protected ?FHIRInteger $subunit = null;
    protected ?FHIRString $sequence = null;
    protected ?FHIRInteger $length = null;
    protected ?FHIRAttachment $sequenceAttachment = null;
    protected ?FHIRIdentifier $nTerminalModificationId = null;
    protected ?FHIRString $nTerminalModification = null;
    protected ?FHIRIdentifier $cTerminalModificationId = null;
    protected ?FHIRString $cTerminalModification = null;

    public function getSubunit(): ?FHIRInteger
    {
        return $this->subunit;
    }

    public function setSubunit(int|FHIRInteger|null $value): self
    {
        $this->subunit = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getSequence(): ?FHIRString
    {
        return $this->sequence;
    }

    public function setSequence(string|FHIRString|null $value): self
    {
        $this->sequence = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getLength(): ?FHIRInteger
    {
        return $this->length;
    }

    public function setLength(int|FHIRInteger|null $value): self
    {
        $this->length = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getSequenceAttachment(): ?FHIRAttachment
    {
        return $this->sequenceAttachment;
    }

    public function setSequenceAttachment(?FHIRAttachment $value): self
    {
        $this->sequenceAttachment = $value;

        return $this;
    }

    public function getNTerminalModificationId(): ?FHIRIdentifier
    {
        return $this->nTerminalModificationId;
    }

    public function setNTerminalModificationId(?FHIRIdentifier $value): self
    {
        $this->nTerminalModificationId = $value;

        return $this;
    }

    public function getNTerminalModification(): ?FHIRString
    {
        return $this->nTerminalModification;
    }

    public function setNTerminalModification(string|FHIRString|null $value): self
    {
        $this->nTerminalModification = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCTerminalModificationId(): ?FHIRIdentifier
    {
        return $this->cTerminalModificationId;
    }

    public function setCTerminalModificationId(?FHIRIdentifier $value): self
    {
        $this->cTerminalModificationId = $value;

        return $this;
    }

    public function getCTerminalModification(): ?FHIRString
    {
        return $this->cTerminalModification;
    }

    public function setCTerminalModification(string|FHIRString|null $value): self
    {
        $this->cTerminalModification = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
