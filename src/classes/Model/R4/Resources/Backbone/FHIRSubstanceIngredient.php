<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceIngredient Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceIngredientInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRRatio;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;

class FHIRSubstanceIngredient extends FHIRBackboneElement implements FHIRSubstanceIngredientInterface
{
    public const RESOURCE_NAME = 'Substance.ingredient';

    protected ?FHIRRatio $quantity = null;
    protected FHIRCodeableConcept|FHIRReference|null $substance = null;

    public function getQuantity(): ?FHIRRatio
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRRatio $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getSubstance(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->substance;
    }

    public function setSubstance(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->substance = $value;

        return $this;
    }
}
