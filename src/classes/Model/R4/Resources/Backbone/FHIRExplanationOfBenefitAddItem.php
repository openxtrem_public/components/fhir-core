<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExplanationOfBenefitAddItem Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExplanationOfBenefitAddItemInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAddress;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRPositiveInt;

class FHIRExplanationOfBenefitAddItem extends FHIRBackboneElement implements FHIRExplanationOfBenefitAddItemInterface
{
    public const RESOURCE_NAME = 'ExplanationOfBenefit.addItem';

    /** @var FHIRPositiveInt[] */
    protected array $itemSequence = [];

    /** @var FHIRPositiveInt[] */
    protected array $detailSequence = [];

    /** @var FHIRPositiveInt[] */
    protected array $subDetailSequence = [];

    /** @var FHIRReference[] */
    protected array $provider = [];
    protected ?FHIRCodeableConcept $productOrService = null;

    /** @var FHIRCodeableConcept[] */
    protected array $modifier = [];

    /** @var FHIRCodeableConcept[] */
    protected array $programCode = [];
    protected FHIRDate|FHIRPeriod|null $serviced = null;
    protected FHIRCodeableConcept|FHIRAddress|FHIRReference|null $location = null;
    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRMoney $unitPrice = null;
    protected ?FHIRDecimal $factor = null;
    protected ?FHIRMoney $net = null;
    protected ?FHIRCodeableConcept $bodySite = null;

    /** @var FHIRCodeableConcept[] */
    protected array $subSite = [];

    /** @var FHIRPositiveInt[] */
    protected array $noteNumber = [];

    /** @var FHIRExplanationOfBenefitItemAdjudication[] */
    protected array $adjudication = [];

    /** @var FHIRExplanationOfBenefitAddItemDetail[] */
    protected array $detail = [];

    /**
     * @return FHIRPositiveInt[]
     */
    public function getItemSequence(): array
    {
        return $this->itemSequence;
    }

    public function setItemSequence(int|FHIRPositiveInt|null ...$value): self
    {
        $this->itemSequence = [];
        $this->addItemSequence(...$value);

        return $this;
    }

    public function addItemSequence(int|FHIRPositiveInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRPositiveInt())->setValue($v) : $v, $value);

        $this->itemSequence = array_filter(array_merge($this->itemSequence, $values));

        return $this;
    }

    /**
     * @return FHIRPositiveInt[]
     */
    public function getDetailSequence(): array
    {
        return $this->detailSequence;
    }

    public function setDetailSequence(int|FHIRPositiveInt|null ...$value): self
    {
        $this->detailSequence = [];
        $this->addDetailSequence(...$value);

        return $this;
    }

    public function addDetailSequence(int|FHIRPositiveInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRPositiveInt())->setValue($v) : $v, $value);

        $this->detailSequence = array_filter(array_merge($this->detailSequence, $values));

        return $this;
    }

    /**
     * @return FHIRPositiveInt[]
     */
    public function getSubDetailSequence(): array
    {
        return $this->subDetailSequence;
    }

    public function setSubDetailSequence(int|FHIRPositiveInt|null ...$value): self
    {
        $this->subDetailSequence = [];
        $this->addSubDetailSequence(...$value);

        return $this;
    }

    public function addSubDetailSequence(int|FHIRPositiveInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRPositiveInt())->setValue($v) : $v, $value);

        $this->subDetailSequence = array_filter(array_merge($this->subDetailSequence, $values));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getProvider(): array
    {
        return $this->provider;
    }

    public function setProvider(?FHIRReference ...$value): self
    {
        $this->provider = array_filter($value);

        return $this;
    }

    public function addProvider(?FHIRReference ...$value): self
    {
        $this->provider = array_filter(array_merge($this->provider, $value));

        return $this;
    }

    public function getProductOrService(): ?FHIRCodeableConcept
    {
        return $this->productOrService;
    }

    public function setProductOrService(?FHIRCodeableConcept $value): self
    {
        $this->productOrService = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getModifier(): array
    {
        return $this->modifier;
    }

    public function setModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->modifier = array_filter($value);

        return $this;
    }

    public function addModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->modifier = array_filter(array_merge($this->modifier, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getProgramCode(): array
    {
        return $this->programCode;
    }

    public function setProgramCode(?FHIRCodeableConcept ...$value): self
    {
        $this->programCode = array_filter($value);

        return $this;
    }

    public function addProgramCode(?FHIRCodeableConcept ...$value): self
    {
        $this->programCode = array_filter(array_merge($this->programCode, $value));

        return $this;
    }

    public function getServiced(): FHIRDate|FHIRPeriod|null
    {
        return $this->serviced;
    }

    public function setServiced(FHIRDate|FHIRPeriod|null $value): self
    {
        $this->serviced = $value;

        return $this;
    }

    public function getLocation(): FHIRCodeableConcept|FHIRAddress|FHIRReference|null
    {
        return $this->location;
    }

    public function setLocation(FHIRCodeableConcept|FHIRAddress|FHIRReference|null $value): self
    {
        $this->location = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getUnitPrice(): ?FHIRMoney
    {
        return $this->unitPrice;
    }

    public function setUnitPrice(?FHIRMoney $value): self
    {
        $this->unitPrice = $value;

        return $this;
    }

    public function getFactor(): ?FHIRDecimal
    {
        return $this->factor;
    }

    public function setFactor(float|FHIRDecimal|null $value): self
    {
        $this->factor = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getNet(): ?FHIRMoney
    {
        return $this->net;
    }

    public function setNet(?FHIRMoney $value): self
    {
        $this->net = $value;

        return $this;
    }

    public function getBodySite(): ?FHIRCodeableConcept
    {
        return $this->bodySite;
    }

    public function setBodySite(?FHIRCodeableConcept $value): self
    {
        $this->bodySite = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSubSite(): array
    {
        return $this->subSite;
    }

    public function setSubSite(?FHIRCodeableConcept ...$value): self
    {
        $this->subSite = array_filter($value);

        return $this;
    }

    public function addSubSite(?FHIRCodeableConcept ...$value): self
    {
        $this->subSite = array_filter(array_merge($this->subSite, $value));

        return $this;
    }

    /**
     * @return FHIRPositiveInt[]
     */
    public function getNoteNumber(): array
    {
        return $this->noteNumber;
    }

    public function setNoteNumber(int|FHIRPositiveInt|null ...$value): self
    {
        $this->noteNumber = [];
        $this->addNoteNumber(...$value);

        return $this;
    }

    public function addNoteNumber(int|FHIRPositiveInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRPositiveInt())->setValue($v) : $v, $value);

        $this->noteNumber = array_filter(array_merge($this->noteNumber, $values));

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitItemAdjudication[]
     */
    public function getAdjudication(): array
    {
        return $this->adjudication;
    }

    public function setAdjudication(?FHIRExplanationOfBenefitItemAdjudication ...$value): self
    {
        $this->adjudication = array_filter($value);

        return $this;
    }

    public function addAdjudication(?FHIRExplanationOfBenefitItemAdjudication ...$value): self
    {
        $this->adjudication = array_filter(array_merge($this->adjudication, $value));

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitAddItemDetail[]
     */
    public function getDetail(): array
    {
        return $this->detail;
    }

    public function setDetail(?FHIRExplanationOfBenefitAddItemDetail ...$value): self
    {
        $this->detail = array_filter($value);

        return $this;
    }

    public function addDetail(?FHIRExplanationOfBenefitAddItemDetail ...$value): self
    {
        $this->detail = array_filter(array_merge($this->detail, $value));

        return $this;
    }
}
