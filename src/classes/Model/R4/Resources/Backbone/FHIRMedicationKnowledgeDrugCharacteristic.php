<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledgeDrugCharacteristic Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationKnowledgeDrugCharacteristicInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBase64Binary;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRMedicationKnowledgeDrugCharacteristic extends FHIRBackboneElement implements FHIRMedicationKnowledgeDrugCharacteristicInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge.drugCharacteristic';

    protected ?FHIRCodeableConcept $type = null;
    protected FHIRCodeableConcept|FHIRString|FHIRQuantity|FHIRBase64Binary|null $value = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getValue(): FHIRCodeableConcept|FHIRString|FHIRQuantity|FHIRBase64Binary|null
    {
        return $this->value;
    }

    public function setValue(FHIRCodeableConcept|FHIRString|FHIRQuantity|FHIRBase64Binary|null $value): self
    {
        $this->value = $value;

        return $this;
    }
}
