<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceSourceMaterialFractionDescription Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceSourceMaterialFractionDescriptionInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRSubstanceSourceMaterialFractionDescription extends FHIRBackboneElement implements FHIRSubstanceSourceMaterialFractionDescriptionInterface
{
    public const RESOURCE_NAME = 'SubstanceSourceMaterial.fractionDescription';

    protected ?FHIRString $fraction = null;
    protected ?FHIRCodeableConcept $materialType = null;

    public function getFraction(): ?FHIRString
    {
        return $this->fraction;
    }

    public function setFraction(string|FHIRString|null $value): self
    {
        $this->fraction = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getMaterialType(): ?FHIRCodeableConcept
    {
        return $this->materialType;
    }

    public function setMaterialType(?FHIRCodeableConcept $value): self
    {
        $this->materialType = $value;

        return $this;
    }
}
