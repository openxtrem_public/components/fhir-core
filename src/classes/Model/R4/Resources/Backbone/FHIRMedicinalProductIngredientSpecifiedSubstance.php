<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductIngredientSpecifiedSubstance Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductIngredientSpecifiedSubstanceInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;

class FHIRMedicinalProductIngredientSpecifiedSubstance extends FHIRBackboneElement implements FHIRMedicinalProductIngredientSpecifiedSubstanceInterface
{
    public const RESOURCE_NAME = 'MedicinalProductIngredient.specifiedSubstance';

    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRCodeableConcept $group = null;
    protected ?FHIRCodeableConcept $confidentiality = null;

    /** @var FHIRMedicinalProductIngredientSpecifiedSubstanceStrength[] */
    protected array $strength = [];

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getGroup(): ?FHIRCodeableConcept
    {
        return $this->group;
    }

    public function setGroup(?FHIRCodeableConcept $value): self
    {
        $this->group = $value;

        return $this;
    }

    public function getConfidentiality(): ?FHIRCodeableConcept
    {
        return $this->confidentiality;
    }

    public function setConfidentiality(?FHIRCodeableConcept $value): self
    {
        $this->confidentiality = $value;

        return $this;
    }

    /**
     * @return FHIRMedicinalProductIngredientSpecifiedSubstanceStrength[]
     */
    public function getStrength(): array
    {
        return $this->strength;
    }

    public function setStrength(?FHIRMedicinalProductIngredientSpecifiedSubstanceStrength ...$value): self
    {
        $this->strength = array_filter($value);

        return $this;
    }

    public function addStrength(?FHIRMedicinalProductIngredientSpecifiedSubstanceStrength ...$value): self
    {
        $this->strength = array_filter(array_merge($this->strength, $value));

        return $this;
    }
}
