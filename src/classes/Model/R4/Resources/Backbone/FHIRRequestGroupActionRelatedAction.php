<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR RequestGroupActionRelatedAction Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRRequestGroupActionRelatedActionInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRId;

class FHIRRequestGroupActionRelatedAction extends FHIRBackboneElement implements FHIRRequestGroupActionRelatedActionInterface
{
    public const RESOURCE_NAME = 'RequestGroup.action.relatedAction';

    protected ?FHIRId $actionId = null;
    protected ?FHIRCode $relationship = null;
    protected FHIRDuration|FHIRRange|null $offset = null;

    public function getActionId(): ?FHIRId
    {
        return $this->actionId;
    }

    public function setActionId(string|FHIRId|null $value): self
    {
        $this->actionId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getRelationship(): ?FHIRCode
    {
        return $this->relationship;
    }

    public function setRelationship(string|FHIRCode|null $value): self
    {
        $this->relationship = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getOffset(): FHIRDuration|FHIRRange|null
    {
        return $this->offset;
    }

    public function setOffset(FHIRDuration|FHIRRange|null $value): self
    {
        $this->offset = $value;

        return $this;
    }
}
