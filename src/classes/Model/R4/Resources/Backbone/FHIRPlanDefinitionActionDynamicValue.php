<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PlanDefinitionActionDynamicValue Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRPlanDefinitionActionDynamicValueInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRExpression;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRPlanDefinitionActionDynamicValue extends FHIRBackboneElement implements FHIRPlanDefinitionActionDynamicValueInterface
{
    public const RESOURCE_NAME = 'PlanDefinition.action.dynamicValue';

    protected ?FHIRString $path = null;
    protected ?FHIRExpression $expression = null;

    public function getPath(): ?FHIRString
    {
        return $this->path;
    }

    public function setPath(string|FHIRString|null $value): self
    {
        $this->path = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getExpression(): ?FHIRExpression
    {
        return $this->expression;
    }

    public function setExpression(?FHIRExpression $value): self
    {
        $this->expression = $value;

        return $this;
    }
}
