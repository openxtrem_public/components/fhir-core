<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CommunicationPayload Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCommunicationPayloadInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRCommunicationPayload extends FHIRBackboneElement implements FHIRCommunicationPayloadInterface
{
    public const RESOURCE_NAME = 'Communication.payload';

    protected FHIRString|FHIRAttachment|FHIRReference|null $content = null;

    public function getContent(): FHIRString|FHIRAttachment|FHIRReference|null
    {
        return $this->content;
    }

    public function setContent(FHIRString|FHIRAttachment|FHIRReference|null $value): self
    {
        $this->content = $value;

        return $this;
    }
}
