<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR NutritionOrderEnteralFormula Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRNutritionOrderEnteralFormulaInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRNutritionOrderEnteralFormula extends FHIRBackboneElement implements FHIRNutritionOrderEnteralFormulaInterface
{
    public const RESOURCE_NAME = 'NutritionOrder.enteralFormula';

    protected ?FHIRCodeableConcept $baseFormulaType = null;
    protected ?FHIRString $baseFormulaProductName = null;
    protected ?FHIRCodeableConcept $additiveType = null;
    protected ?FHIRString $additiveProductName = null;
    protected ?FHIRQuantity $caloricDensity = null;
    protected ?FHIRCodeableConcept $routeofAdministration = null;

    /** @var FHIRNutritionOrderEnteralFormulaAdministration[] */
    protected array $administration = [];
    protected ?FHIRQuantity $maxVolumeToDeliver = null;
    protected ?FHIRString $administrationInstruction = null;

    public function getBaseFormulaType(): ?FHIRCodeableConcept
    {
        return $this->baseFormulaType;
    }

    public function setBaseFormulaType(?FHIRCodeableConcept $value): self
    {
        $this->baseFormulaType = $value;

        return $this;
    }

    public function getBaseFormulaProductName(): ?FHIRString
    {
        return $this->baseFormulaProductName;
    }

    public function setBaseFormulaProductName(string|FHIRString|null $value): self
    {
        $this->baseFormulaProductName = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getAdditiveType(): ?FHIRCodeableConcept
    {
        return $this->additiveType;
    }

    public function setAdditiveType(?FHIRCodeableConcept $value): self
    {
        $this->additiveType = $value;

        return $this;
    }

    public function getAdditiveProductName(): ?FHIRString
    {
        return $this->additiveProductName;
    }

    public function setAdditiveProductName(string|FHIRString|null $value): self
    {
        $this->additiveProductName = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCaloricDensity(): ?FHIRQuantity
    {
        return $this->caloricDensity;
    }

    public function setCaloricDensity(?FHIRQuantity $value): self
    {
        $this->caloricDensity = $value;

        return $this;
    }

    public function getRouteofAdministration(): ?FHIRCodeableConcept
    {
        return $this->routeofAdministration;
    }

    public function setRouteofAdministration(?FHIRCodeableConcept $value): self
    {
        $this->routeofAdministration = $value;

        return $this;
    }

    /**
     * @return FHIRNutritionOrderEnteralFormulaAdministration[]
     */
    public function getAdministration(): array
    {
        return $this->administration;
    }

    public function setAdministration(?FHIRNutritionOrderEnteralFormulaAdministration ...$value): self
    {
        $this->administration = array_filter($value);

        return $this;
    }

    public function addAdministration(?FHIRNutritionOrderEnteralFormulaAdministration ...$value): self
    {
        $this->administration = array_filter(array_merge($this->administration, $value));

        return $this;
    }

    public function getMaxVolumeToDeliver(): ?FHIRQuantity
    {
        return $this->maxVolumeToDeliver;
    }

    public function setMaxVolumeToDeliver(?FHIRQuantity $value): self
    {
        $this->maxVolumeToDeliver = $value;

        return $this;
    }

    public function getAdministrationInstruction(): ?FHIRString
    {
        return $this->administrationInstruction;
    }

    public function setAdministrationInstruction(string|FHIRString|null $value): self
    {
        $this->administrationInstruction = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
