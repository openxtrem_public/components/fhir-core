<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceDefinitionMaterial Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceDefinitionMaterialInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;

class FHIRDeviceDefinitionMaterial extends FHIRBackboneElement implements FHIRDeviceDefinitionMaterialInterface
{
    public const RESOURCE_NAME = 'DeviceDefinition.material';

    protected ?FHIRCodeableConcept $substance = null;
    protected ?FHIRBoolean $alternate = null;
    protected ?FHIRBoolean $allergenicIndicator = null;

    public function getSubstance(): ?FHIRCodeableConcept
    {
        return $this->substance;
    }

    public function setSubstance(?FHIRCodeableConcept $value): self
    {
        $this->substance = $value;

        return $this;
    }

    public function getAlternate(): ?FHIRBoolean
    {
        return $this->alternate;
    }

    public function setAlternate(bool|FHIRBoolean|null $value): self
    {
        $this->alternate = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getAllergenicIndicator(): ?FHIRBoolean
    {
        return $this->allergenicIndicator;
    }

    public function setAllergenicIndicator(bool|FHIRBoolean|null $value): self
    {
        $this->allergenicIndicator = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }
}
