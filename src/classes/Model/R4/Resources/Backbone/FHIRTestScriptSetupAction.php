<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestScriptSetupAction Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestScriptSetupActionInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;

class FHIRTestScriptSetupAction extends FHIRBackboneElement implements FHIRTestScriptSetupActionInterface
{
    public const RESOURCE_NAME = 'TestScript.setup.action';

    protected ?FHIRTestScriptSetupActionOperation $operation = null;
    protected ?FHIRTestScriptSetupActionAssert $assert = null;

    public function getOperation(): ?FHIRTestScriptSetupActionOperation
    {
        return $this->operation;
    }

    public function setOperation(?FHIRTestScriptSetupActionOperation $value): self
    {
        $this->operation = $value;

        return $this;
    }

    public function getAssert(): ?FHIRTestScriptSetupActionAssert
    {
        return $this->assert;
    }

    public function setAssert(?FHIRTestScriptSetupActionAssert $value): self
    {
        $this->assert = $value;

        return $this;
    }
}
