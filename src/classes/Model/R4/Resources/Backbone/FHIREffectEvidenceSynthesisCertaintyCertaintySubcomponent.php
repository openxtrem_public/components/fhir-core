<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EffectEvidenceSynthesisCertaintyCertaintySubcomponent Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREffectEvidenceSynthesisCertaintyCertaintySubcomponentInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;

class FHIREffectEvidenceSynthesisCertaintyCertaintySubcomponent extends FHIRBackboneElement implements FHIREffectEvidenceSynthesisCertaintyCertaintySubcomponentInterface
{
    public const RESOURCE_NAME = 'EffectEvidenceSynthesis.certainty.certaintySubcomponent';

    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRCodeableConcept[] */
    protected array $rating = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getRating(): array
    {
        return $this->rating;
    }

    public function setRating(?FHIRCodeableConcept ...$value): self
    {
        $this->rating = array_filter($value);

        return $this;
    }

    public function addRating(?FHIRCodeableConcept ...$value): self
    {
        $this->rating = array_filter(array_merge($this->rating, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }
}
