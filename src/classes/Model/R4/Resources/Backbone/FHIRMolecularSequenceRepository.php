<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MolecularSequenceRepository Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMolecularSequenceRepositoryInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUri;

class FHIRMolecularSequenceRepository extends FHIRBackboneElement implements FHIRMolecularSequenceRepositoryInterface
{
    public const RESOURCE_NAME = 'MolecularSequence.repository';

    protected ?FHIRCode $type = null;
    protected ?FHIRUri $url = null;
    protected ?FHIRString $name = null;
    protected ?FHIRString $datasetId = null;
    protected ?FHIRString $variantsetId = null;
    protected ?FHIRString $readsetId = null;

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDatasetId(): ?FHIRString
    {
        return $this->datasetId;
    }

    public function setDatasetId(string|FHIRString|null $value): self
    {
        $this->datasetId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getVariantsetId(): ?FHIRString
    {
        return $this->variantsetId;
    }

    public function setVariantsetId(string|FHIRString|null $value): self
    {
        $this->variantsetId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getReadsetId(): ?FHIRString
    {
        return $this->readsetId;
    }

    public function setReadsetId(string|FHIRString|null $value): self
    {
        $this->readsetId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
