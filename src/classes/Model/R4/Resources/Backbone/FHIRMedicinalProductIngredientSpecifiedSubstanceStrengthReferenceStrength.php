<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductIngredientSpecifiedSubstanceStrengthReferenceStrength Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductIngredientSpecifiedSubstanceStrengthReferenceStrengthInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRRatio;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRMedicinalProductIngredientSpecifiedSubstanceStrengthReferenceStrength extends FHIRBackboneElement implements FHIRMedicinalProductIngredientSpecifiedSubstanceStrengthReferenceStrengthInterface
{
    public const RESOURCE_NAME = 'MedicinalProductIngredient.specifiedSubstance.strength.referenceStrength';

    protected ?FHIRCodeableConcept $substance = null;
    protected ?FHIRRatio $strength = null;
    protected ?FHIRRatio $strengthLowLimit = null;
    protected ?FHIRString $measurementPoint = null;

    /** @var FHIRCodeableConcept[] */
    protected array $country = [];

    public function getSubstance(): ?FHIRCodeableConcept
    {
        return $this->substance;
    }

    public function setSubstance(?FHIRCodeableConcept $value): self
    {
        $this->substance = $value;

        return $this;
    }

    public function getStrength(): ?FHIRRatio
    {
        return $this->strength;
    }

    public function setStrength(?FHIRRatio $value): self
    {
        $this->strength = $value;

        return $this;
    }

    public function getStrengthLowLimit(): ?FHIRRatio
    {
        return $this->strengthLowLimit;
    }

    public function setStrengthLowLimit(?FHIRRatio $value): self
    {
        $this->strengthLowLimit = $value;

        return $this;
    }

    public function getMeasurementPoint(): ?FHIRString
    {
        return $this->measurementPoint;
    }

    public function setMeasurementPoint(string|FHIRString|null $value): self
    {
        $this->measurementPoint = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCountry(): array
    {
        return $this->country;
    }

    public function setCountry(?FHIRCodeableConcept ...$value): self
    {
        $this->country = array_filter($value);

        return $this;
    }

    public function addCountry(?FHIRCodeableConcept ...$value): self
    {
        $this->country = array_filter(array_merge($this->country, $value));

        return $this;
    }
}
