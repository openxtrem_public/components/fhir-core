<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstancePolymerRepeatRepeatUnit Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstancePolymerRepeatRepeatUnitInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRSubstanceAmount;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRSubstancePolymerRepeatRepeatUnit extends FHIRBackboneElement implements FHIRSubstancePolymerRepeatRepeatUnitInterface
{
    public const RESOURCE_NAME = 'SubstancePolymer.repeat.repeatUnit';

    protected ?FHIRCodeableConcept $orientationOfPolymerisation = null;
    protected ?FHIRString $repeatUnit = null;
    protected ?FHIRSubstanceAmount $amount = null;

    /** @var FHIRSubstancePolymerRepeatRepeatUnitDegreeOfPolymerisation[] */
    protected array $degreeOfPolymerisation = [];

    /** @var FHIRSubstancePolymerRepeatRepeatUnitStructuralRepresentation[] */
    protected array $structuralRepresentation = [];

    public function getOrientationOfPolymerisation(): ?FHIRCodeableConcept
    {
        return $this->orientationOfPolymerisation;
    }

    public function setOrientationOfPolymerisation(?FHIRCodeableConcept $value): self
    {
        $this->orientationOfPolymerisation = $value;

        return $this;
    }

    public function getRepeatUnit(): ?FHIRString
    {
        return $this->repeatUnit;
    }

    public function setRepeatUnit(string|FHIRString|null $value): self
    {
        $this->repeatUnit = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getAmount(): ?FHIRSubstanceAmount
    {
        return $this->amount;
    }

    public function setAmount(?FHIRSubstanceAmount $value): self
    {
        $this->amount = $value;

        return $this;
    }

    /**
     * @return FHIRSubstancePolymerRepeatRepeatUnitDegreeOfPolymerisation[]
     */
    public function getDegreeOfPolymerisation(): array
    {
        return $this->degreeOfPolymerisation;
    }

    public function setDegreeOfPolymerisation(
        ?FHIRSubstancePolymerRepeatRepeatUnitDegreeOfPolymerisation ...$value,
    ): self
    {
        $this->degreeOfPolymerisation = array_filter($value);

        return $this;
    }

    public function addDegreeOfPolymerisation(
        ?FHIRSubstancePolymerRepeatRepeatUnitDegreeOfPolymerisation ...$value,
    ): self
    {
        $this->degreeOfPolymerisation = array_filter(array_merge($this->degreeOfPolymerisation, $value));

        return $this;
    }

    /**
     * @return FHIRSubstancePolymerRepeatRepeatUnitStructuralRepresentation[]
     */
    public function getStructuralRepresentation(): array
    {
        return $this->structuralRepresentation;
    }

    public function setStructuralRepresentation(
        ?FHIRSubstancePolymerRepeatRepeatUnitStructuralRepresentation ...$value,
    ): self
    {
        $this->structuralRepresentation = array_filter($value);

        return $this;
    }

    public function addStructuralRepresentation(
        ?FHIRSubstancePolymerRepeatRepeatUnitStructuralRepresentation ...$value,
    ): self
    {
        $this->structuralRepresentation = array_filter(array_merge($this->structuralRepresentation, $value));

        return $this;
    }
}
