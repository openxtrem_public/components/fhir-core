<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationRequestDispenseRequest Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationRequestDispenseRequestInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUnsignedInt;

class FHIRMedicationRequestDispenseRequest extends FHIRBackboneElement implements FHIRMedicationRequestDispenseRequestInterface
{
    public const RESOURCE_NAME = 'MedicationRequest.dispenseRequest';

    protected ?FHIRMedicationRequestDispenseRequestInitialFill $initialFill = null;
    protected ?FHIRDuration $dispenseInterval = null;
    protected ?FHIRPeriod $validityPeriod = null;
    protected ?FHIRUnsignedInt $numberOfRepeatsAllowed = null;
    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRDuration $expectedSupplyDuration = null;
    protected ?FHIRReference $performer = null;

    public function getInitialFill(): ?FHIRMedicationRequestDispenseRequestInitialFill
    {
        return $this->initialFill;
    }

    public function setInitialFill(?FHIRMedicationRequestDispenseRequestInitialFill $value): self
    {
        $this->initialFill = $value;

        return $this;
    }

    public function getDispenseInterval(): ?FHIRDuration
    {
        return $this->dispenseInterval;
    }

    public function setDispenseInterval(?FHIRDuration $value): self
    {
        $this->dispenseInterval = $value;

        return $this;
    }

    public function getValidityPeriod(): ?FHIRPeriod
    {
        return $this->validityPeriod;
    }

    public function setValidityPeriod(?FHIRPeriod $value): self
    {
        $this->validityPeriod = $value;

        return $this;
    }

    public function getNumberOfRepeatsAllowed(): ?FHIRUnsignedInt
    {
        return $this->numberOfRepeatsAllowed;
    }

    public function setNumberOfRepeatsAllowed(int|FHIRUnsignedInt|null $value): self
    {
        $this->numberOfRepeatsAllowed = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getExpectedSupplyDuration(): ?FHIRDuration
    {
        return $this->expectedSupplyDuration;
    }

    public function setExpectedSupplyDuration(?FHIRDuration $value): self
    {
        $this->expectedSupplyDuration = $value;

        return $this;
    }

    public function getPerformer(): ?FHIRReference
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRReference $value): self
    {
        $this->performer = $value;

        return $this;
    }
}
