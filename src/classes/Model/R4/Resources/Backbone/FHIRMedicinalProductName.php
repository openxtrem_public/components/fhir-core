<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductName Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductNameInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRMedicinalProductName extends FHIRBackboneElement implements FHIRMedicinalProductNameInterface
{
    public const RESOURCE_NAME = 'MedicinalProduct.name';

    protected ?FHIRString $productName = null;

    /** @var FHIRMedicinalProductNameNamePart[] */
    protected array $namePart = [];

    /** @var FHIRMedicinalProductNameCountryLanguage[] */
    protected array $countryLanguage = [];

    public function getProductName(): ?FHIRString
    {
        return $this->productName;
    }

    public function setProductName(string|FHIRString|null $value): self
    {
        $this->productName = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRMedicinalProductNameNamePart[]
     */
    public function getNamePart(): array
    {
        return $this->namePart;
    }

    public function setNamePart(?FHIRMedicinalProductNameNamePart ...$value): self
    {
        $this->namePart = array_filter($value);

        return $this;
    }

    public function addNamePart(?FHIRMedicinalProductNameNamePart ...$value): self
    {
        $this->namePart = array_filter(array_merge($this->namePart, $value));

        return $this;
    }

    /**
     * @return FHIRMedicinalProductNameCountryLanguage[]
     */
    public function getCountryLanguage(): array
    {
        return $this->countryLanguage;
    }

    public function setCountryLanguage(?FHIRMedicinalProductNameCountryLanguage ...$value): self
    {
        $this->countryLanguage = array_filter($value);

        return $this;
    }

    public function addCountryLanguage(?FHIRMedicinalProductNameCountryLanguage ...$value): self
    {
        $this->countryLanguage = array_filter(array_merge($this->countryLanguage, $value));

        return $this;
    }
}
