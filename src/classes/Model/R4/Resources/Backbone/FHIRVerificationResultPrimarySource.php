<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR VerificationResultPrimarySource Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRVerificationResultPrimarySourceInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;

class FHIRVerificationResultPrimarySource extends FHIRBackboneElement implements FHIRVerificationResultPrimarySourceInterface
{
    public const RESOURCE_NAME = 'VerificationResult.primarySource';

    protected ?FHIRReference $who = null;

    /** @var FHIRCodeableConcept[] */
    protected array $type = [];

    /** @var FHIRCodeableConcept[] */
    protected array $communicationMethod = [];
    protected ?FHIRCodeableConcept $validationStatus = null;
    protected ?FHIRDateTime $validationDate = null;
    protected ?FHIRCodeableConcept $canPushUpdates = null;

    /** @var FHIRCodeableConcept[] */
    protected array $pushTypeAvailable = [];

    public function getWho(): ?FHIRReference
    {
        return $this->who;
    }

    public function setWho(?FHIRReference $value): self
    {
        $this->who = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCommunicationMethod(): array
    {
        return $this->communicationMethod;
    }

    public function setCommunicationMethod(?FHIRCodeableConcept ...$value): self
    {
        $this->communicationMethod = array_filter($value);

        return $this;
    }

    public function addCommunicationMethod(?FHIRCodeableConcept ...$value): self
    {
        $this->communicationMethod = array_filter(array_merge($this->communicationMethod, $value));

        return $this;
    }

    public function getValidationStatus(): ?FHIRCodeableConcept
    {
        return $this->validationStatus;
    }

    public function setValidationStatus(?FHIRCodeableConcept $value): self
    {
        $this->validationStatus = $value;

        return $this;
    }

    public function getValidationDate(): ?FHIRDateTime
    {
        return $this->validationDate;
    }

    public function setValidationDate(string|FHIRDateTime|null $value): self
    {
        $this->validationDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getCanPushUpdates(): ?FHIRCodeableConcept
    {
        return $this->canPushUpdates;
    }

    public function setCanPushUpdates(?FHIRCodeableConcept $value): self
    {
        $this->canPushUpdates = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getPushTypeAvailable(): array
    {
        return $this->pushTypeAvailable;
    }

    public function setPushTypeAvailable(?FHIRCodeableConcept ...$value): self
    {
        $this->pushTypeAvailable = array_filter($value);

        return $this;
    }

    public function addPushTypeAvailable(?FHIRCodeableConcept ...$value): self
    {
        $this->pushTypeAvailable = array_filter(array_merge($this->pushTypeAvailable, $value));

        return $this;
    }
}
