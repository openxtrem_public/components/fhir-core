<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR BiologicallyDerivedProductProcessing Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRBiologicallyDerivedProductProcessingInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRBiologicallyDerivedProductProcessing extends FHIRBackboneElement implements FHIRBiologicallyDerivedProductProcessingInterface
{
    public const RESOURCE_NAME = 'BiologicallyDerivedProduct.processing';

    protected ?FHIRString $description = null;
    protected ?FHIRCodeableConcept $procedure = null;
    protected ?FHIRReference $additive = null;
    protected FHIRDateTime|FHIRPeriod|null $time = null;

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getProcedure(): ?FHIRCodeableConcept
    {
        return $this->procedure;
    }

    public function setProcedure(?FHIRCodeableConcept $value): self
    {
        $this->procedure = $value;

        return $this;
    }

    public function getAdditive(): ?FHIRReference
    {
        return $this->additive;
    }

    public function setAdditive(?FHIRReference $value): self
    {
        $this->additive = $value;

        return $this;
    }

    public function getTime(): FHIRDateTime|FHIRPeriod|null
    {
        return $this->time;
    }

    public function setTime(FHIRDateTime|FHIRPeriod|null $value): self
    {
        $this->time = $value;

        return $this;
    }
}
