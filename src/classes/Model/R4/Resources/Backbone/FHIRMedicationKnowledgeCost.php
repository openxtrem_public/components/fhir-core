<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledgeCost Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationKnowledgeCostInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRMedicationKnowledgeCost extends FHIRBackboneElement implements FHIRMedicationKnowledgeCostInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge.cost';

    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRString $source = null;
    protected ?FHIRMoney $cost = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getSource(): ?FHIRString
    {
        return $this->source;
    }

    public function setSource(string|FHIRString|null $value): self
    {
        $this->source = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCost(): ?FHIRMoney
    {
        return $this->cost;
    }

    public function setCost(?FHIRMoney $value): self
    {
        $this->cost = $value;

        return $this;
    }
}
