<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestScriptSetupActionOperation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestScriptSetupActionOperationInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRTestScriptSetupActionOperation extends FHIRBackboneElement implements FHIRTestScriptSetupActionOperationInterface
{
    public const RESOURCE_NAME = 'TestScript.setup.action.operation';

    protected ?FHIRCoding $type = null;
    protected ?FHIRCode $resource = null;
    protected ?FHIRString $label = null;
    protected ?FHIRString $description = null;
    protected ?FHIRCode $accept = null;
    protected ?FHIRCode $contentType = null;
    protected ?FHIRInteger $destination = null;
    protected ?FHIRBoolean $encodeRequestUrl = null;
    protected ?FHIRCode $method = null;
    protected ?FHIRInteger $origin = null;
    protected ?FHIRString $params = null;

    /** @var FHIRTestScriptSetupActionOperationRequestHeader[] */
    protected array $requestHeader = [];
    protected ?FHIRId $requestId = null;
    protected ?FHIRId $responseId = null;
    protected ?FHIRId $sourceId = null;
    protected ?FHIRId $targetId = null;
    protected ?FHIRString $url = null;

    public function getType(): ?FHIRCoding
    {
        return $this->type;
    }

    public function setType(?FHIRCoding $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getResource(): ?FHIRCode
    {
        return $this->resource;
    }

    public function setResource(string|FHIRCode|null $value): self
    {
        $this->resource = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getLabel(): ?FHIRString
    {
        return $this->label;
    }

    public function setLabel(string|FHIRString|null $value): self
    {
        $this->label = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getAccept(): ?FHIRCode
    {
        return $this->accept;
    }

    public function setAccept(string|FHIRCode|null $value): self
    {
        $this->accept = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getContentType(): ?FHIRCode
    {
        return $this->contentType;
    }

    public function setContentType(string|FHIRCode|null $value): self
    {
        $this->contentType = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDestination(): ?FHIRInteger
    {
        return $this->destination;
    }

    public function setDestination(int|FHIRInteger|null $value): self
    {
        $this->destination = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getEncodeRequestUrl(): ?FHIRBoolean
    {
        return $this->encodeRequestUrl;
    }

    public function setEncodeRequestUrl(bool|FHIRBoolean|null $value): self
    {
        $this->encodeRequestUrl = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getMethod(): ?FHIRCode
    {
        return $this->method;
    }

    public function setMethod(string|FHIRCode|null $value): self
    {
        $this->method = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getOrigin(): ?FHIRInteger
    {
        return $this->origin;
    }

    public function setOrigin(int|FHIRInteger|null $value): self
    {
        $this->origin = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getParams(): ?FHIRString
    {
        return $this->params;
    }

    public function setParams(string|FHIRString|null $value): self
    {
        $this->params = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRTestScriptSetupActionOperationRequestHeader[]
     */
    public function getRequestHeader(): array
    {
        return $this->requestHeader;
    }

    public function setRequestHeader(?FHIRTestScriptSetupActionOperationRequestHeader ...$value): self
    {
        $this->requestHeader = array_filter($value);

        return $this;
    }

    public function addRequestHeader(?FHIRTestScriptSetupActionOperationRequestHeader ...$value): self
    {
        $this->requestHeader = array_filter(array_merge($this->requestHeader, $value));

        return $this;
    }

    public function getRequestId(): ?FHIRId
    {
        return $this->requestId;
    }

    public function setRequestId(string|FHIRId|null $value): self
    {
        $this->requestId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getResponseId(): ?FHIRId
    {
        return $this->responseId;
    }

    public function setResponseId(string|FHIRId|null $value): self
    {
        $this->responseId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getSourceId(): ?FHIRId
    {
        return $this->sourceId;
    }

    public function setSourceId(string|FHIRId|null $value): self
    {
        $this->sourceId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getTargetId(): ?FHIRId
    {
        return $this->targetId;
    }

    public function setTargetId(string|FHIRId|null $value): self
    {
        $this->targetId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getUrl(): ?FHIRString
    {
        return $this->url;
    }

    public function setUrl(string|FHIRString|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
