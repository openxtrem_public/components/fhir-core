<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR RiskEvidenceSynthesisRiskEstimate Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRRiskEvidenceSynthesisRiskEstimateInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRRiskEvidenceSynthesisRiskEstimate extends FHIRBackboneElement implements FHIRRiskEvidenceSynthesisRiskEstimateInterface
{
    public const RESOURCE_NAME = 'RiskEvidenceSynthesis.riskEstimate';

    protected ?FHIRString $description = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRDecimal $value = null;
    protected ?FHIRCodeableConcept $unitOfMeasure = null;
    protected ?FHIRInteger $denominatorCount = null;
    protected ?FHIRInteger $numeratorCount = null;

    /** @var FHIRRiskEvidenceSynthesisRiskEstimatePrecisionEstimate[] */
    protected array $precisionEstimate = [];

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getValue(): ?FHIRDecimal
    {
        return $this->value;
    }

    public function setValue(float|FHIRDecimal|null $value): self
    {
        $this->value = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getUnitOfMeasure(): ?FHIRCodeableConcept
    {
        return $this->unitOfMeasure;
    }

    public function setUnitOfMeasure(?FHIRCodeableConcept $value): self
    {
        $this->unitOfMeasure = $value;

        return $this;
    }

    public function getDenominatorCount(): ?FHIRInteger
    {
        return $this->denominatorCount;
    }

    public function setDenominatorCount(int|FHIRInteger|null $value): self
    {
        $this->denominatorCount = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getNumeratorCount(): ?FHIRInteger
    {
        return $this->numeratorCount;
    }

    public function setNumeratorCount(int|FHIRInteger|null $value): self
    {
        $this->numeratorCount = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRRiskEvidenceSynthesisRiskEstimatePrecisionEstimate[]
     */
    public function getPrecisionEstimate(): array
    {
        return $this->precisionEstimate;
    }

    public function setPrecisionEstimate(?FHIRRiskEvidenceSynthesisRiskEstimatePrecisionEstimate ...$value): self
    {
        $this->precisionEstimate = array_filter($value);

        return $this;
    }

    public function addPrecisionEstimate(?FHIRRiskEvidenceSynthesisRiskEstimatePrecisionEstimate ...$value): self
    {
        $this->precisionEstimate = array_filter(array_merge($this->precisionEstimate, $value));

        return $this;
    }
}
