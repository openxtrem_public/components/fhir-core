<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceReferenceInformationClassification Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceReferenceInformationClassificationInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;

class FHIRSubstanceReferenceInformationClassification extends FHIRBackboneElement implements FHIRSubstanceReferenceInformationClassificationInterface
{
    public const RESOURCE_NAME = 'SubstanceReferenceInformation.classification';

    protected ?FHIRCodeableConcept $domain = null;
    protected ?FHIRCodeableConcept $classification = null;

    /** @var FHIRCodeableConcept[] */
    protected array $subtype = [];

    /** @var FHIRReference[] */
    protected array $source = [];

    public function getDomain(): ?FHIRCodeableConcept
    {
        return $this->domain;
    }

    public function setDomain(?FHIRCodeableConcept $value): self
    {
        $this->domain = $value;

        return $this;
    }

    public function getClassification(): ?FHIRCodeableConcept
    {
        return $this->classification;
    }

    public function setClassification(?FHIRCodeableConcept $value): self
    {
        $this->classification = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSubtype(): array
    {
        return $this->subtype;
    }

    public function setSubtype(?FHIRCodeableConcept ...$value): self
    {
        $this->subtype = array_filter($value);

        return $this;
    }

    public function addSubtype(?FHIRCodeableConcept ...$value): self
    {
        $this->subtype = array_filter(array_merge($this->subtype, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSource(): array
    {
        return $this->source;
    }

    public function setSource(?FHIRReference ...$value): self
    {
        $this->source = array_filter($value);

        return $this;
    }

    public function addSource(?FHIRReference ...$value): self
    {
        $this->source = array_filter(array_merge($this->source, $value));

        return $this;
    }
}
