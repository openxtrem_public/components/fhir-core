<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR LocationHoursOfOperation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRLocationHoursOfOperationInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRTime;

class FHIRLocationHoursOfOperation extends FHIRBackboneElement implements FHIRLocationHoursOfOperationInterface
{
    public const RESOURCE_NAME = 'Location.hoursOfOperation';

    /** @var FHIRCode[] */
    protected array $daysOfWeek = [];
    protected ?FHIRBoolean $allDay = null;
    protected ?FHIRTime $openingTime = null;
    protected ?FHIRTime $closingTime = null;

    /**
     * @return FHIRCode[]
     */
    public function getDaysOfWeek(): array
    {
        return $this->daysOfWeek;
    }

    public function setDaysOfWeek(string|FHIRCode|null ...$value): self
    {
        $this->daysOfWeek = [];
        $this->addDaysOfWeek(...$value);

        return $this;
    }

    public function addDaysOfWeek(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->daysOfWeek = array_filter(array_merge($this->daysOfWeek, $values));

        return $this;
    }

    public function getAllDay(): ?FHIRBoolean
    {
        return $this->allDay;
    }

    public function setAllDay(bool|FHIRBoolean|null $value): self
    {
        $this->allDay = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getOpeningTime(): ?FHIRTime
    {
        return $this->openingTime;
    }

    public function setOpeningTime(string|FHIRTime|null $value): self
    {
        $this->openingTime = is_string($value) ? (new FHIRTime())->setValue($value) : $value;

        return $this;
    }

    public function getClosingTime(): ?FHIRTime
    {
        return $this->closingTime;
    }

    public function setClosingTime(string|FHIRTime|null $value): self
    {
        $this->closingTime = is_string($value) ? (new FHIRTime())->setValue($value) : $value;

        return $this;
    }
}
