<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AuditEventSource Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAuditEventSourceInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRAuditEventSource extends FHIRBackboneElement implements FHIRAuditEventSourceInterface
{
    public const RESOURCE_NAME = 'AuditEvent.source';

    protected ?FHIRString $site = null;
    protected ?FHIRReference $observer = null;

    /** @var FHIRCoding[] */
    protected array $type = [];

    public function getSite(): ?FHIRString
    {
        return $this->site;
    }

    public function setSite(string|FHIRString|null $value): self
    {
        $this->site = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getObserver(): ?FHIRReference
    {
        return $this->observer;
    }

    public function setObserver(?FHIRReference $value): self
    {
        $this->observer = $value;

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRCoding ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRCoding ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }
}
