<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductInteractionInteractant Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductInteractionInteractantInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;

class FHIRMedicinalProductInteractionInteractant extends FHIRBackboneElement implements FHIRMedicinalProductInteractionInteractantInterface
{
    public const RESOURCE_NAME = 'MedicinalProductInteraction.interactant';

    protected FHIRReference|FHIRCodeableConcept|null $item = null;

    public function getItem(): FHIRReference|FHIRCodeableConcept|null
    {
        return $this->item;
    }

    public function setItem(FHIRReference|FHIRCodeableConcept|null $value): self
    {
        $this->item = $value;

        return $this;
    }
}
