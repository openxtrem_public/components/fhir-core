<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MeasureReportGroupStratifier Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMeasureReportGroupStratifierInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;

class FHIRMeasureReportGroupStratifier extends FHIRBackboneElement implements FHIRMeasureReportGroupStratifierInterface
{
    public const RESOURCE_NAME = 'MeasureReport.group.stratifier';

    /** @var FHIRCodeableConcept[] */
    protected array $code = [];

    /** @var FHIRMeasureReportGroupStratifierStratum[] */
    protected array $stratum = [];

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCode(): array
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept ...$value): self
    {
        $this->code = array_filter($value);

        return $this;
    }

    public function addCode(?FHIRCodeableConcept ...$value): self
    {
        $this->code = array_filter(array_merge($this->code, $value));

        return $this;
    }

    /**
     * @return FHIRMeasureReportGroupStratifierStratum[]
     */
    public function getStratum(): array
    {
        return $this->stratum;
    }

    public function setStratum(?FHIRMeasureReportGroupStratifierStratum ...$value): self
    {
        $this->stratum = array_filter($value);

        return $this;
    }

    public function addStratum(?FHIRMeasureReportGroupStratifierStratum ...$value): self
    {
        $this->stratum = array_filter(array_merge($this->stratum, $value));

        return $this;
    }
}
