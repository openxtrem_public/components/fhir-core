<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ValueSetCompose Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRValueSetComposeInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDate;

class FHIRValueSetCompose extends FHIRBackboneElement implements FHIRValueSetComposeInterface
{
    public const RESOURCE_NAME = 'ValueSet.compose';

    protected ?FHIRDate $lockedDate = null;
    protected ?FHIRBoolean $inactive = null;

    /** @var FHIRValueSetComposeInclude[] */
    protected array $include = [];

    /** @var FHIRValueSetComposeInclude[] */
    protected array $exclude = [];

    public function getLockedDate(): ?FHIRDate
    {
        return $this->lockedDate;
    }

    public function setLockedDate(string|FHIRDate|null $value): self
    {
        $this->lockedDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getInactive(): ?FHIRBoolean
    {
        return $this->inactive;
    }

    public function setInactive(bool|FHIRBoolean|null $value): self
    {
        $this->inactive = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRValueSetComposeInclude[]
     */
    public function getInclude(): array
    {
        return $this->include;
    }

    public function setInclude(?FHIRValueSetComposeInclude ...$value): self
    {
        $this->include = array_filter($value);

        return $this;
    }

    public function addInclude(?FHIRValueSetComposeInclude ...$value): self
    {
        $this->include = array_filter(array_merge($this->include, $value));

        return $this;
    }

    /**
     * @return FHIRValueSetComposeInclude[]
     */
    public function getExclude(): array
    {
        return $this->exclude;
    }

    public function setExclude(?FHIRValueSetComposeInclude ...$value): self
    {
        $this->exclude = array_filter($value);

        return $this;
    }

    public function addExclude(?FHIRValueSetComposeInclude ...$value): self
    {
        $this->exclude = array_filter(array_merge($this->exclude, $value));

        return $this;
    }
}
