<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PlanDefinitionGoalTarget Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRPlanDefinitionGoalTargetInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRRange;

class FHIRPlanDefinitionGoalTarget extends FHIRBackboneElement implements FHIRPlanDefinitionGoalTargetInterface
{
    public const RESOURCE_NAME = 'PlanDefinition.goal.target';

    protected ?FHIRCodeableConcept $measure = null;
    protected FHIRQuantity|FHIRRange|FHIRCodeableConcept|null $detail = null;
    protected ?FHIRDuration $due = null;

    public function getMeasure(): ?FHIRCodeableConcept
    {
        return $this->measure;
    }

    public function setMeasure(?FHIRCodeableConcept $value): self
    {
        $this->measure = $value;

        return $this;
    }

    public function getDetail(): FHIRQuantity|FHIRRange|FHIRCodeableConcept|null
    {
        return $this->detail;
    }

    public function setDetail(FHIRQuantity|FHIRRange|FHIRCodeableConcept|null $value): self
    {
        $this->detail = $value;

        return $this;
    }

    public function getDue(): ?FHIRDuration
    {
        return $this->due;
    }

    public function setDue(?FHIRDuration $value): self
    {
        $this->due = $value;

        return $this;
    }
}
