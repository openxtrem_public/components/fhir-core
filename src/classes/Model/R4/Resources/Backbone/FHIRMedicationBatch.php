<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationBatch Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationBatchInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRMedicationBatch extends FHIRBackboneElement implements FHIRMedicationBatchInterface
{
    public const RESOURCE_NAME = 'Medication.batch';

    protected ?FHIRString $lotNumber = null;
    protected ?FHIRDateTime $expirationDate = null;

    public function getLotNumber(): ?FHIRString
    {
        return $this->lotNumber;
    }

    public function setLotNumber(string|FHIRString|null $value): self
    {
        $this->lotNumber = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getExpirationDate(): ?FHIRDateTime
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(string|FHIRDateTime|null $value): self
    {
        $this->expirationDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }
}
