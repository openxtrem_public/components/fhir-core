<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductNameNamePart Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductNameNamePartInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRMedicinalProductNameNamePart extends FHIRBackboneElement implements FHIRMedicinalProductNameNamePartInterface
{
    public const RESOURCE_NAME = 'MedicinalProduct.name.namePart';

    protected ?FHIRString $part = null;
    protected ?FHIRCoding $type = null;

    public function getPart(): ?FHIRString
    {
        return $this->part;
    }

    public function setPart(string|FHIRString|null $value): self
    {
        $this->part = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCoding
    {
        return $this->type;
    }

    public function setType(?FHIRCoding $value): self
    {
        $this->type = $value;

        return $this;
    }
}
