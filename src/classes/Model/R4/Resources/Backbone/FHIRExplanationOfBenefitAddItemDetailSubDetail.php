<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExplanationOfBenefitAddItemDetailSubDetail Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExplanationOfBenefitAddItemDetailSubDetailInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRPositiveInt;

class FHIRExplanationOfBenefitAddItemDetailSubDetail extends FHIRBackboneElement implements FHIRExplanationOfBenefitAddItemDetailSubDetailInterface
{
    public const RESOURCE_NAME = 'ExplanationOfBenefit.addItem.detail.subDetail';

    protected ?FHIRCodeableConcept $productOrService = null;

    /** @var FHIRCodeableConcept[] */
    protected array $modifier = [];
    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRMoney $unitPrice = null;
    protected ?FHIRDecimal $factor = null;
    protected ?FHIRMoney $net = null;

    /** @var FHIRPositiveInt[] */
    protected array $noteNumber = [];

    /** @var FHIRExplanationOfBenefitItemAdjudication[] */
    protected array $adjudication = [];

    public function getProductOrService(): ?FHIRCodeableConcept
    {
        return $this->productOrService;
    }

    public function setProductOrService(?FHIRCodeableConcept $value): self
    {
        $this->productOrService = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getModifier(): array
    {
        return $this->modifier;
    }

    public function setModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->modifier = array_filter($value);

        return $this;
    }

    public function addModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->modifier = array_filter(array_merge($this->modifier, $value));

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getUnitPrice(): ?FHIRMoney
    {
        return $this->unitPrice;
    }

    public function setUnitPrice(?FHIRMoney $value): self
    {
        $this->unitPrice = $value;

        return $this;
    }

    public function getFactor(): ?FHIRDecimal
    {
        return $this->factor;
    }

    public function setFactor(float|FHIRDecimal|null $value): self
    {
        $this->factor = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getNet(): ?FHIRMoney
    {
        return $this->net;
    }

    public function setNet(?FHIRMoney $value): self
    {
        $this->net = $value;

        return $this;
    }

    /**
     * @return FHIRPositiveInt[]
     */
    public function getNoteNumber(): array
    {
        return $this->noteNumber;
    }

    public function setNoteNumber(int|FHIRPositiveInt|null ...$value): self
    {
        $this->noteNumber = [];
        $this->addNoteNumber(...$value);

        return $this;
    }

    public function addNoteNumber(int|FHIRPositiveInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRPositiveInt())->setValue($v) : $v, $value);

        $this->noteNumber = array_filter(array_merge($this->noteNumber, $values));

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitItemAdjudication[]
     */
    public function getAdjudication(): array
    {
        return $this->adjudication;
    }

    public function setAdjudication(?FHIRExplanationOfBenefitItemAdjudication ...$value): self
    {
        $this->adjudication = array_filter($value);

        return $this;
    }

    public function addAdjudication(?FHIRExplanationOfBenefitItemAdjudication ...$value): self
    {
        $this->adjudication = array_filter(array_merge($this->adjudication, $value));

        return $this;
    }
}
