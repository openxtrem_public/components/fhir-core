<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExampleScenarioProcessStepOperation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExampleScenarioProcessStepOperationInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRExampleScenarioProcessStepOperation extends FHIRBackboneElement implements FHIRExampleScenarioProcessStepOperationInterface
{
    public const RESOURCE_NAME = 'ExampleScenario.process.step.operation';

    protected ?FHIRString $number = null;
    protected ?FHIRString $type = null;
    protected ?FHIRString $name = null;
    protected ?FHIRString $initiator = null;
    protected ?FHIRString $receiver = null;
    protected ?FHIRMarkdown $description = null;
    protected ?FHIRBoolean $initiatorActive = null;
    protected ?FHIRBoolean $receiverActive = null;
    protected ?FHIRExampleScenarioInstanceContainedInstance $request = null;
    protected ?FHIRExampleScenarioInstanceContainedInstance $response = null;

    public function getNumber(): ?FHIRString
    {
        return $this->number;
    }

    public function setNumber(string|FHIRString|null $value): self
    {
        $this->number = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRString
    {
        return $this->type;
    }

    public function setType(string|FHIRString|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getInitiator(): ?FHIRString
    {
        return $this->initiator;
    }

    public function setInitiator(string|FHIRString|null $value): self
    {
        $this->initiator = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getReceiver(): ?FHIRString
    {
        return $this->receiver;
    }

    public function setReceiver(string|FHIRString|null $value): self
    {
        $this->receiver = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getInitiatorActive(): ?FHIRBoolean
    {
        return $this->initiatorActive;
    }

    public function setInitiatorActive(bool|FHIRBoolean|null $value): self
    {
        $this->initiatorActive = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getReceiverActive(): ?FHIRBoolean
    {
        return $this->receiverActive;
    }

    public function setReceiverActive(bool|FHIRBoolean|null $value): self
    {
        $this->receiverActive = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getRequest(): ?FHIRExampleScenarioInstanceContainedInstance
    {
        return $this->request;
    }

    public function setRequest(?FHIRExampleScenarioInstanceContainedInstance $value): self
    {
        $this->request = $value;

        return $this;
    }

    public function getResponse(): ?FHIRExampleScenarioInstanceContainedInstance
    {
        return $this->response;
    }

    public function setResponse(?FHIRExampleScenarioInstanceContainedInstance $value): self
    {
        $this->response = $value;

        return $this;
    }
}
