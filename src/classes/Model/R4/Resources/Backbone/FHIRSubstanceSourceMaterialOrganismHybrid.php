<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceSourceMaterialOrganismHybrid Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceSourceMaterialOrganismHybridInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRSubstanceSourceMaterialOrganismHybrid extends FHIRBackboneElement implements FHIRSubstanceSourceMaterialOrganismHybridInterface
{
    public const RESOURCE_NAME = 'SubstanceSourceMaterial.organism.hybrid';

    protected ?FHIRString $maternalOrganismId = null;
    protected ?FHIRString $maternalOrganismName = null;
    protected ?FHIRString $paternalOrganismId = null;
    protected ?FHIRString $paternalOrganismName = null;
    protected ?FHIRCodeableConcept $hybridType = null;

    public function getMaternalOrganismId(): ?FHIRString
    {
        return $this->maternalOrganismId;
    }

    public function setMaternalOrganismId(string|FHIRString|null $value): self
    {
        $this->maternalOrganismId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getMaternalOrganismName(): ?FHIRString
    {
        return $this->maternalOrganismName;
    }

    public function setMaternalOrganismName(string|FHIRString|null $value): self
    {
        $this->maternalOrganismName = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPaternalOrganismId(): ?FHIRString
    {
        return $this->paternalOrganismId;
    }

    public function setPaternalOrganismId(string|FHIRString|null $value): self
    {
        $this->paternalOrganismId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPaternalOrganismName(): ?FHIRString
    {
        return $this->paternalOrganismName;
    }

    public function setPaternalOrganismName(string|FHIRString|null $value): self
    {
        $this->paternalOrganismName = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getHybridType(): ?FHIRCodeableConcept
    {
        return $this->hybridType;
    }

    public function setHybridType(?FHIRCodeableConcept $value): self
    {
        $this->hybridType = $value;

        return $this;
    }
}
