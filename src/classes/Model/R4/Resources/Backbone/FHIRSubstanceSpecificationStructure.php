<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceSpecificationStructure Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceSpecificationStructureInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRSubstanceSpecificationStructure extends FHIRBackboneElement implements FHIRSubstanceSpecificationStructureInterface
{
    public const RESOURCE_NAME = 'SubstanceSpecification.structure';

    protected ?FHIRCodeableConcept $stereochemistry = null;
    protected ?FHIRCodeableConcept $opticalActivity = null;
    protected ?FHIRString $molecularFormula = null;
    protected ?FHIRString $molecularFormulaByMoiety = null;

    /** @var FHIRSubstanceSpecificationStructureIsotope[] */
    protected array $isotope = [];
    protected ?FHIRSubstanceSpecificationStructureIsotopeMolecularWeight $molecularWeight = null;

    /** @var FHIRReference[] */
    protected array $source = [];

    /** @var FHIRSubstanceSpecificationStructureRepresentation[] */
    protected array $representation = [];

    public function getStereochemistry(): ?FHIRCodeableConcept
    {
        return $this->stereochemistry;
    }

    public function setStereochemistry(?FHIRCodeableConcept $value): self
    {
        $this->stereochemistry = $value;

        return $this;
    }

    public function getOpticalActivity(): ?FHIRCodeableConcept
    {
        return $this->opticalActivity;
    }

    public function setOpticalActivity(?FHIRCodeableConcept $value): self
    {
        $this->opticalActivity = $value;

        return $this;
    }

    public function getMolecularFormula(): ?FHIRString
    {
        return $this->molecularFormula;
    }

    public function setMolecularFormula(string|FHIRString|null $value): self
    {
        $this->molecularFormula = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getMolecularFormulaByMoiety(): ?FHIRString
    {
        return $this->molecularFormulaByMoiety;
    }

    public function setMolecularFormulaByMoiety(string|FHIRString|null $value): self
    {
        $this->molecularFormulaByMoiety = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRSubstanceSpecificationStructureIsotope[]
     */
    public function getIsotope(): array
    {
        return $this->isotope;
    }

    public function setIsotope(?FHIRSubstanceSpecificationStructureIsotope ...$value): self
    {
        $this->isotope = array_filter($value);

        return $this;
    }

    public function addIsotope(?FHIRSubstanceSpecificationStructureIsotope ...$value): self
    {
        $this->isotope = array_filter(array_merge($this->isotope, $value));

        return $this;
    }

    public function getMolecularWeight(): ?FHIRSubstanceSpecificationStructureIsotopeMolecularWeight
    {
        return $this->molecularWeight;
    }

    public function setMolecularWeight(?FHIRSubstanceSpecificationStructureIsotopeMolecularWeight $value): self
    {
        $this->molecularWeight = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSource(): array
    {
        return $this->source;
    }

    public function setSource(?FHIRReference ...$value): self
    {
        $this->source = array_filter($value);

        return $this;
    }

    public function addSource(?FHIRReference ...$value): self
    {
        $this->source = array_filter(array_merge($this->source, $value));

        return $this;
    }

    /**
     * @return FHIRSubstanceSpecificationStructureRepresentation[]
     */
    public function getRepresentation(): array
    {
        return $this->representation;
    }

    public function setRepresentation(?FHIRSubstanceSpecificationStructureRepresentation ...$value): self
    {
        $this->representation = array_filter($value);

        return $this;
    }

    public function addRepresentation(?FHIRSubstanceSpecificationStructureRepresentation ...$value): self
    {
        $this->representation = array_filter(array_merge($this->representation, $value));

        return $this;
    }
}
