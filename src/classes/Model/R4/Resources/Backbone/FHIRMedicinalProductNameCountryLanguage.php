<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductNameCountryLanguage Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductNameCountryLanguageInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;

class FHIRMedicinalProductNameCountryLanguage extends FHIRBackboneElement implements FHIRMedicinalProductNameCountryLanguageInterface
{
    public const RESOURCE_NAME = 'MedicinalProduct.name.countryLanguage';

    protected ?FHIRCodeableConcept $country = null;
    protected ?FHIRCodeableConcept $jurisdiction = null;
    protected ?FHIRCodeableConcept $language = null;

    public function getCountry(): ?FHIRCodeableConcept
    {
        return $this->country;
    }

    public function setCountry(?FHIRCodeableConcept $value): self
    {
        $this->country = $value;

        return $this;
    }

    public function getJurisdiction(): ?FHIRCodeableConcept
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(?FHIRCodeableConcept $value): self
    {
        $this->jurisdiction = $value;

        return $this;
    }

    public function getLanguage(): ?FHIRCodeableConcept
    {
        return $this->language;
    }

    public function setLanguage(?FHIRCodeableConcept $value): self
    {
        $this->language = $value;

        return $this;
    }
}
