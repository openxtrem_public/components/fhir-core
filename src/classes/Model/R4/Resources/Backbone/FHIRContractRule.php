<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ContractRule Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRContractRuleInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;

class FHIRContractRule extends FHIRBackboneElement implements FHIRContractRuleInterface
{
    public const RESOURCE_NAME = 'Contract.rule';

    protected FHIRAttachment|FHIRReference|null $content = null;

    public function getContent(): FHIRAttachment|FHIRReference|null
    {
        return $this->content;
    }

    public function setContent(FHIRAttachment|FHIRReference|null $value): self
    {
        $this->content = $value;

        return $this;
    }
}
