<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExampleScenarioProcess Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExampleScenarioProcessInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRExampleScenarioProcess extends FHIRBackboneElement implements FHIRExampleScenarioProcessInterface
{
    public const RESOURCE_NAME = 'ExampleScenario.process';

    protected ?FHIRString $title = null;
    protected ?FHIRMarkdown $description = null;
    protected ?FHIRMarkdown $preConditions = null;
    protected ?FHIRMarkdown $postConditions = null;

    /** @var FHIRExampleScenarioProcessStep[] */
    protected array $step = [];

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getPreConditions(): ?FHIRMarkdown
    {
        return $this->preConditions;
    }

    public function setPreConditions(string|FHIRMarkdown|null $value): self
    {
        $this->preConditions = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getPostConditions(): ?FHIRMarkdown
    {
        return $this->postConditions;
    }

    public function setPostConditions(string|FHIRMarkdown|null $value): self
    {
        $this->postConditions = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRExampleScenarioProcessStep[]
     */
    public function getStep(): array
    {
        return $this->step;
    }

    public function setStep(?FHIRExampleScenarioProcessStep ...$value): self
    {
        $this->step = array_filter($value);

        return $this;
    }

    public function addStep(?FHIRExampleScenarioProcessStep ...$value): self
    {
        $this->step = array_filter(array_merge($this->step, $value));

        return $this;
    }
}
