<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EvidenceVariableCharacteristic Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREvidenceVariableCharacteristicInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRDataRequirement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRExpression;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRTriggerDefinition;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIREvidenceVariableCharacteristic extends FHIRBackboneElement implements FHIREvidenceVariableCharacteristicInterface
{
    public const RESOURCE_NAME = 'EvidenceVariable.characteristic';

    protected ?FHIRString $description = null;
    protected FHIRReference|FHIRCanonical|FHIRCodeableConcept|FHIRExpression|FHIRDataRequirement|FHIRTriggerDefinition|null $definition = null;

    /** @var FHIRUsageContext[] */
    protected array $usageContext = [];
    protected ?FHIRBoolean $exclude = null;
    protected FHIRDateTime|FHIRPeriod|FHIRDuration|FHIRTiming|null $participantEffective = null;
    protected ?FHIRDuration $timeFromStart = null;
    protected ?FHIRCode $groupMeasure = null;

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDefinition(
    ): FHIRReference|FHIRCanonical|FHIRCodeableConcept|FHIRExpression|FHIRDataRequirement|FHIRTriggerDefinition|null
    {
        return $this->definition;
    }

    public function setDefinition(
        FHIRReference|FHIRCanonical|FHIRCodeableConcept|FHIRExpression|FHIRDataRequirement|FHIRTriggerDefinition|null $value,
    ): self
    {
        $this->definition = $value;

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUsageContext(): array
    {
        return $this->usageContext;
    }

    public function setUsageContext(?FHIRUsageContext ...$value): self
    {
        $this->usageContext = array_filter($value);

        return $this;
    }

    public function addUsageContext(?FHIRUsageContext ...$value): self
    {
        $this->usageContext = array_filter(array_merge($this->usageContext, $value));

        return $this;
    }

    public function getExclude(): ?FHIRBoolean
    {
        return $this->exclude;
    }

    public function setExclude(bool|FHIRBoolean|null $value): self
    {
        $this->exclude = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getParticipantEffective(): FHIRDateTime|FHIRPeriod|FHIRDuration|FHIRTiming|null
    {
        return $this->participantEffective;
    }

    public function setParticipantEffective(FHIRDateTime|FHIRPeriod|FHIRDuration|FHIRTiming|null $value): self
    {
        $this->participantEffective = $value;

        return $this;
    }

    public function getTimeFromStart(): ?FHIRDuration
    {
        return $this->timeFromStart;
    }

    public function setTimeFromStart(?FHIRDuration $value): self
    {
        $this->timeFromStart = $value;

        return $this;
    }

    public function getGroupMeasure(): ?FHIRCode
    {
        return $this->groupMeasure;
    }

    public function setGroupMeasure(string|FHIRCode|null $value): self
    {
        $this->groupMeasure = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }
}
