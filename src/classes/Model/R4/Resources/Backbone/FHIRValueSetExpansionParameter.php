<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ValueSetExpansionParameter Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRValueSetExpansionParameterInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUri;

class FHIRValueSetExpansionParameter extends FHIRBackboneElement implements FHIRValueSetExpansionParameterInterface
{
    public const RESOURCE_NAME = 'ValueSet.expansion.parameter';

    protected ?FHIRString $name = null;
    protected FHIRString|FHIRBoolean|FHIRInteger|FHIRDecimal|FHIRUri|FHIRCode|FHIRDateTime|null $value = null;

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getValue(): FHIRString|FHIRBoolean|FHIRInteger|FHIRDecimal|FHIRUri|FHIRCode|FHIRDateTime|null
    {
        return $this->value;
    }

    public function setValue(
        FHIRString|FHIRBoolean|FHIRInteger|FHIRDecimal|FHIRUri|FHIRCode|FHIRDateTime|null $value,
    ): self
    {
        $this->value = $value;

        return $this;
    }
}
