<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR GraphDefinitionLinkTarget Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRGraphDefinitionLinkTargetInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRGraphDefinitionLinkTarget extends FHIRBackboneElement implements FHIRGraphDefinitionLinkTargetInterface
{
    public const RESOURCE_NAME = 'GraphDefinition.link.target';

    protected ?FHIRCode $type = null;
    protected ?FHIRString $params = null;
    protected ?FHIRCanonical $profile = null;

    /** @var FHIRGraphDefinitionLinkTargetCompartment[] */
    protected array $compartment = [];

    /** @var FHIRGraphDefinitionLink[] */
    protected array $link = [];

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getParams(): ?FHIRString
    {
        return $this->params;
    }

    public function setParams(string|FHIRString|null $value): self
    {
        $this->params = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getProfile(): ?FHIRCanonical
    {
        return $this->profile;
    }

    public function setProfile(string|FHIRCanonical|null $value): self
    {
        $this->profile = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRGraphDefinitionLinkTargetCompartment[]
     */
    public function getCompartment(): array
    {
        return $this->compartment;
    }

    public function setCompartment(?FHIRGraphDefinitionLinkTargetCompartment ...$value): self
    {
        $this->compartment = array_filter($value);

        return $this;
    }

    public function addCompartment(?FHIRGraphDefinitionLinkTargetCompartment ...$value): self
    {
        $this->compartment = array_filter(array_merge($this->compartment, $value));

        return $this;
    }

    /**
     * @return FHIRGraphDefinitionLink[]
     */
    public function getLink(): array
    {
        return $this->link;
    }

    public function setLink(?FHIRGraphDefinitionLink ...$value): self
    {
        $this->link = array_filter($value);

        return $this;
    }

    public function addLink(?FHIRGraphDefinitionLink ...$value): self
    {
        $this->link = array_filter(array_merge($this->link, $value));

        return $this;
    }
}
