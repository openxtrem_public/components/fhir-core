<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MeasureGroupStratifierComponent Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMeasureGroupStratifierComponentInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRExpression;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRMeasureGroupStratifierComponent extends FHIRBackboneElement implements FHIRMeasureGroupStratifierComponentInterface
{
    public const RESOURCE_NAME = 'Measure.group.stratifier.component';

    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRString $description = null;
    protected ?FHIRExpression $criteria = null;

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCriteria(): ?FHIRExpression
    {
        return $this->criteria;
    }

    public function setCriteria(?FHIRExpression $value): self
    {
        $this->criteria = $value;

        return $this;
    }
}
