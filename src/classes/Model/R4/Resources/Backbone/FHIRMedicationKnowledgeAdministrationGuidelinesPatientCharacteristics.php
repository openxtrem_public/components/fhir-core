<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledgeAdministrationGuidelinesPatientCharacteristics Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationKnowledgeAdministrationGuidelinesPatientCharacteristicsInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRMedicationKnowledgeAdministrationGuidelinesPatientCharacteristics extends FHIRBackboneElement implements FHIRMedicationKnowledgeAdministrationGuidelinesPatientCharacteristicsInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge.administrationGuidelines.patientCharacteristics';

    protected FHIRCodeableConcept|FHIRQuantity|null $characteristic = null;

    /** @var FHIRString[] */
    protected array $value = [];

    public function getCharacteristic(): FHIRCodeableConcept|FHIRQuantity|null
    {
        return $this->characteristic;
    }

    public function setCharacteristic(FHIRCodeableConcept|FHIRQuantity|null $value): self
    {
        $this->characteristic = $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getValue(): array
    {
        return $this->value;
    }

    public function setValue(string|FHIRString|null ...$value): self
    {
        $this->value = [];
        $this->addValue(...$value);

        return $this;
    }

    public function addValue(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->value = array_filter(array_merge($this->value, $values));

        return $this;
    }
}
