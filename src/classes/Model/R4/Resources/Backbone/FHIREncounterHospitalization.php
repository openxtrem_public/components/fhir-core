<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EncounterHospitalization Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREncounterHospitalizationInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;

class FHIREncounterHospitalization extends FHIRBackboneElement implements FHIREncounterHospitalizationInterface
{
    public const RESOURCE_NAME = 'Encounter.hospitalization';

    protected ?FHIRIdentifier $preAdmissionIdentifier = null;
    protected ?FHIRReference $origin = null;
    protected ?FHIRCodeableConcept $admitSource = null;
    protected ?FHIRCodeableConcept $reAdmission = null;

    /** @var FHIRCodeableConcept[] */
    protected array $dietPreference = [];

    /** @var FHIRCodeableConcept[] */
    protected array $specialCourtesy = [];

    /** @var FHIRCodeableConcept[] */
    protected array $specialArrangement = [];
    protected ?FHIRReference $destination = null;
    protected ?FHIRCodeableConcept $dischargeDisposition = null;

    public function getPreAdmissionIdentifier(): ?FHIRIdentifier
    {
        return $this->preAdmissionIdentifier;
    }

    public function setPreAdmissionIdentifier(?FHIRIdentifier $value): self
    {
        $this->preAdmissionIdentifier = $value;

        return $this;
    }

    public function getOrigin(): ?FHIRReference
    {
        return $this->origin;
    }

    public function setOrigin(?FHIRReference $value): self
    {
        $this->origin = $value;

        return $this;
    }

    public function getAdmitSource(): ?FHIRCodeableConcept
    {
        return $this->admitSource;
    }

    public function setAdmitSource(?FHIRCodeableConcept $value): self
    {
        $this->admitSource = $value;

        return $this;
    }

    public function getReAdmission(): ?FHIRCodeableConcept
    {
        return $this->reAdmission;
    }

    public function setReAdmission(?FHIRCodeableConcept $value): self
    {
        $this->reAdmission = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getDietPreference(): array
    {
        return $this->dietPreference;
    }

    public function setDietPreference(?FHIRCodeableConcept ...$value): self
    {
        $this->dietPreference = array_filter($value);

        return $this;
    }

    public function addDietPreference(?FHIRCodeableConcept ...$value): self
    {
        $this->dietPreference = array_filter(array_merge($this->dietPreference, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSpecialCourtesy(): array
    {
        return $this->specialCourtesy;
    }

    public function setSpecialCourtesy(?FHIRCodeableConcept ...$value): self
    {
        $this->specialCourtesy = array_filter($value);

        return $this;
    }

    public function addSpecialCourtesy(?FHIRCodeableConcept ...$value): self
    {
        $this->specialCourtesy = array_filter(array_merge($this->specialCourtesy, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSpecialArrangement(): array
    {
        return $this->specialArrangement;
    }

    public function setSpecialArrangement(?FHIRCodeableConcept ...$value): self
    {
        $this->specialArrangement = array_filter($value);

        return $this;
    }

    public function addSpecialArrangement(?FHIRCodeableConcept ...$value): self
    {
        $this->specialArrangement = array_filter(array_merge($this->specialArrangement, $value));

        return $this;
    }

    public function getDestination(): ?FHIRReference
    {
        return $this->destination;
    }

    public function setDestination(?FHIRReference $value): self
    {
        $this->destination = $value;

        return $this;
    }

    public function getDischargeDisposition(): ?FHIRCodeableConcept
    {
        return $this->dischargeDisposition;
    }

    public function setDischargeDisposition(?FHIRCodeableConcept $value): self
    {
        $this->dischargeDisposition = $value;

        return $this;
    }
}
