<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceNucleicAcidSubunitSugar Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceNucleicAcidSubunitSugarInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRSubstanceNucleicAcidSubunitSugar extends FHIRBackboneElement implements FHIRSubstanceNucleicAcidSubunitSugarInterface
{
    public const RESOURCE_NAME = 'SubstanceNucleicAcid.subunit.sugar';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRString $name = null;
    protected ?FHIRString $residueSite = null;

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getResidueSite(): ?FHIRString
    {
        return $this->residueSite;
    }

    public function setResidueSite(string|FHIRString|null $value): self
    {
        $this->residueSite = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
