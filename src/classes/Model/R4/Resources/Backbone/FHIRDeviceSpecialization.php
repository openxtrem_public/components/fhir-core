<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceSpecialization Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceSpecializationInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRDeviceSpecialization extends FHIRBackboneElement implements FHIRDeviceSpecializationInterface
{
    public const RESOURCE_NAME = 'Device.specialization';

    protected ?FHIRCodeableConcept $systemType = null;
    protected ?FHIRString $version = null;

    public function getSystemType(): ?FHIRCodeableConcept
    {
        return $this->systemType;
    }

    public function setSystemType(?FHIRCodeableConcept $value): self
    {
        $this->systemType = $value;

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
