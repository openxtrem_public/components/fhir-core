<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CoverageEligibilityResponseInsuranceItemBenefit Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCoverageEligibilityResponseInsuranceItemBenefitInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUnsignedInt;

class FHIRCoverageEligibilityResponseInsuranceItemBenefit extends FHIRBackboneElement implements FHIRCoverageEligibilityResponseInsuranceItemBenefitInterface
{
    public const RESOURCE_NAME = 'CoverageEligibilityResponse.insurance.item.benefit';

    protected ?FHIRCodeableConcept $type = null;
    protected FHIRUnsignedInt|FHIRString|FHIRMoney|null $allowed = null;
    protected FHIRUnsignedInt|FHIRString|FHIRMoney|null $used = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getAllowed(): FHIRUnsignedInt|FHIRString|FHIRMoney|null
    {
        return $this->allowed;
    }

    public function setAllowed(FHIRUnsignedInt|FHIRString|FHIRMoney|null $value): self
    {
        $this->allowed = $value;

        return $this;
    }

    public function getUsed(): FHIRUnsignedInt|FHIRString|FHIRMoney|null
    {
        return $this->used;
    }

    public function setUsed(FHIRUnsignedInt|FHIRString|FHIRMoney|null $value): self
    {
        $this->used = $value;

        return $this;
    }
}
