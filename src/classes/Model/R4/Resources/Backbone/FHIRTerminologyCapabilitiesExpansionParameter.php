<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TerminologyCapabilitiesExpansionParameter Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTerminologyCapabilitiesExpansionParameterInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRTerminologyCapabilitiesExpansionParameter extends FHIRBackboneElement implements FHIRTerminologyCapabilitiesExpansionParameterInterface
{
    public const RESOURCE_NAME = 'TerminologyCapabilities.expansion.parameter';

    protected ?FHIRCode $name = null;
    protected ?FHIRString $documentation = null;

    public function getName(): ?FHIRCode
    {
        return $this->name;
    }

    public function setName(string|FHIRCode|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDocumentation(): ?FHIRString
    {
        return $this->documentation;
    }

    public function setDocumentation(string|FHIRString|null $value): self
    {
        $this->documentation = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
