<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceDefinitionUdiDeviceIdentifier Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceDefinitionUdiDeviceIdentifierInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUri;

class FHIRDeviceDefinitionUdiDeviceIdentifier extends FHIRBackboneElement implements FHIRDeviceDefinitionUdiDeviceIdentifierInterface
{
    public const RESOURCE_NAME = 'DeviceDefinition.udiDeviceIdentifier';

    protected ?FHIRString $deviceIdentifier = null;
    protected ?FHIRUri $issuer = null;
    protected ?FHIRUri $jurisdiction = null;

    public function getDeviceIdentifier(): ?FHIRString
    {
        return $this->deviceIdentifier;
    }

    public function setDeviceIdentifier(string|FHIRString|null $value): self
    {
        $this->deviceIdentifier = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getIssuer(): ?FHIRUri
    {
        return $this->issuer;
    }

    public function setIssuer(string|FHIRUri|null $value): self
    {
        $this->issuer = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getJurisdiction(): ?FHIRUri
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(string|FHIRUri|null $value): self
    {
        $this->jurisdiction = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }
}
