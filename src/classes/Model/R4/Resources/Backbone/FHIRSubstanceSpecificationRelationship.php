<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceSpecificationRelationship Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceSpecificationRelationshipInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRRatio;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRSubstanceSpecificationRelationship extends FHIRBackboneElement implements FHIRSubstanceSpecificationRelationshipInterface
{
    public const RESOURCE_NAME = 'SubstanceSpecification.relationship';

    protected FHIRReference|FHIRCodeableConcept|null $substance = null;
    protected ?FHIRCodeableConcept $relationship = null;
    protected ?FHIRBoolean $isDefining = null;
    protected FHIRQuantity|FHIRRange|FHIRRatio|FHIRString|null $amount = null;
    protected ?FHIRRatio $amountRatioLowLimit = null;
    protected ?FHIRCodeableConcept $amountType = null;

    /** @var FHIRReference[] */
    protected array $source = [];

    public function getSubstance(): FHIRReference|FHIRCodeableConcept|null
    {
        return $this->substance;
    }

    public function setSubstance(FHIRReference|FHIRCodeableConcept|null $value): self
    {
        $this->substance = $value;

        return $this;
    }

    public function getRelationship(): ?FHIRCodeableConcept
    {
        return $this->relationship;
    }

    public function setRelationship(?FHIRCodeableConcept $value): self
    {
        $this->relationship = $value;

        return $this;
    }

    public function getIsDefining(): ?FHIRBoolean
    {
        return $this->isDefining;
    }

    public function setIsDefining(bool|FHIRBoolean|null $value): self
    {
        $this->isDefining = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getAmount(): FHIRQuantity|FHIRRange|FHIRRatio|FHIRString|null
    {
        return $this->amount;
    }

    public function setAmount(FHIRQuantity|FHIRRange|FHIRRatio|FHIRString|null $value): self
    {
        $this->amount = $value;

        return $this;
    }

    public function getAmountRatioLowLimit(): ?FHIRRatio
    {
        return $this->amountRatioLowLimit;
    }

    public function setAmountRatioLowLimit(?FHIRRatio $value): self
    {
        $this->amountRatioLowLimit = $value;

        return $this;
    }

    public function getAmountType(): ?FHIRCodeableConcept
    {
        return $this->amountType;
    }

    public function setAmountType(?FHIRCodeableConcept $value): self
    {
        $this->amountType = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSource(): array
    {
        return $this->source;
    }

    public function setSource(?FHIRReference ...$value): self
    {
        $this->source = array_filter($value);

        return $this;
    }

    public function addSource(?FHIRReference ...$value): self
    {
        $this->source = array_filter(array_merge($this->source, $value));

        return $this;
    }
}
