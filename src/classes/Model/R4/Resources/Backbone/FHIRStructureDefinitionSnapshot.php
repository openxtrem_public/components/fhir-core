<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR StructureDefinitionSnapshot Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRStructureDefinitionSnapshotInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRElementDefinition;

class FHIRStructureDefinitionSnapshot extends FHIRBackboneElement implements FHIRStructureDefinitionSnapshotInterface
{
    public const RESOURCE_NAME = 'StructureDefinition.snapshot';

    /** @var FHIRElementDefinition[] */
    protected array $element = [];

    /**
     * @return FHIRElementDefinition[]
     */
    public function getElement(): array
    {
        return $this->element;
    }

    public function setElement(?FHIRElementDefinition ...$value): self
    {
        $this->element = array_filter($value);

        return $this;
    }

    public function addElement(?FHIRElementDefinition ...$value): self
    {
        $this->element = array_filter(array_merge($this->element, $value));

        return $this;
    }
}
