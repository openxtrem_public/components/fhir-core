<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MeasureReportGroupPopulation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMeasureReportGroupPopulationInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRInteger;

class FHIRMeasureReportGroupPopulation extends FHIRBackboneElement implements FHIRMeasureReportGroupPopulationInterface
{
    public const RESOURCE_NAME = 'MeasureReport.group.population';

    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRInteger $count = null;
    protected ?FHIRReference $subjectResults = null;

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getCount(): ?FHIRInteger
    {
        return $this->count;
    }

    public function setCount(int|FHIRInteger|null $value): self
    {
        $this->count = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getSubjectResults(): ?FHIRReference
    {
        return $this->subjectResults;
    }

    public function setSubjectResults(?FHIRReference $value): self
    {
        $this->subjectResults = $value;

        return $this;
    }
}
