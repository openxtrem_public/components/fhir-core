<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceReferenceInformationGene Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceReferenceInformationGeneInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;

class FHIRSubstanceReferenceInformationGene extends FHIRBackboneElement implements FHIRSubstanceReferenceInformationGeneInterface
{
    public const RESOURCE_NAME = 'SubstanceReferenceInformation.gene';

    protected ?FHIRCodeableConcept $geneSequenceOrigin = null;
    protected ?FHIRCodeableConcept $gene = null;

    /** @var FHIRReference[] */
    protected array $source = [];

    public function getGeneSequenceOrigin(): ?FHIRCodeableConcept
    {
        return $this->geneSequenceOrigin;
    }

    public function setGeneSequenceOrigin(?FHIRCodeableConcept $value): self
    {
        $this->geneSequenceOrigin = $value;

        return $this;
    }

    public function getGene(): ?FHIRCodeableConcept
    {
        return $this->gene;
    }

    public function setGene(?FHIRCodeableConcept $value): self
    {
        $this->gene = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSource(): array
    {
        return $this->source;
    }

    public function setSource(?FHIRReference ...$value): self
    {
        $this->source = array_filter($value);

        return $this;
    }

    public function addSource(?FHIRReference ...$value): self
    {
        $this->source = array_filter(array_merge($this->source, $value));

        return $this;
    }
}
