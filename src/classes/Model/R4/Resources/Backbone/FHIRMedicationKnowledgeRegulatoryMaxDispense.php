<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledgeRegulatoryMaxDispense Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationKnowledgeRegulatoryMaxDispenseInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;

class FHIRMedicationKnowledgeRegulatoryMaxDispense extends FHIRBackboneElement implements FHIRMedicationKnowledgeRegulatoryMaxDispenseInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge.regulatory.maxDispense';

    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRDuration $period = null;

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getPeriod(): ?FHIRDuration
    {
        return $this->period;
    }

    public function setPeriod(?FHIRDuration $value): self
    {
        $this->period = $value;

        return $this;
    }
}
