<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductAuthorizationProcedure Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductAuthorizationProcedureInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;

class FHIRMedicinalProductAuthorizationProcedure extends FHIRBackboneElement implements FHIRMedicinalProductAuthorizationProcedureInterface
{
    public const RESOURCE_NAME = 'MedicinalProductAuthorization.procedure';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRCodeableConcept $type = null;
    protected FHIRPeriod|FHIRDateTime|null $date = null;

    /** @var FHIRMedicinalProductAuthorizationProcedure[] */
    protected array $application = [];

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getDate(): FHIRPeriod|FHIRDateTime|null
    {
        return $this->date;
    }

    public function setDate(FHIRPeriod|FHIRDateTime|null $value): self
    {
        $this->date = $value;

        return $this;
    }

    /**
     * @return FHIRMedicinalProductAuthorizationProcedure[]
     */
    public function getApplication(): array
    {
        return $this->application;
    }

    public function setApplication(?FHIRMedicinalProductAuthorizationProcedure ...$value): self
    {
        $this->application = array_filter($value);

        return $this;
    }

    public function addApplication(?FHIRMedicinalProductAuthorizationProcedure ...$value): self
    {
        $this->application = array_filter(array_merge($this->application, $value));

        return $this;
    }
}
