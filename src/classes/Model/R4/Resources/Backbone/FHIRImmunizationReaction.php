<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImmunizationReaction Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImmunizationReactionInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;

class FHIRImmunizationReaction extends FHIRBackboneElement implements FHIRImmunizationReactionInterface
{
    public const RESOURCE_NAME = 'Immunization.reaction';

    protected ?FHIRDateTime $date = null;
    protected ?FHIRReference $detail = null;
    protected ?FHIRBoolean $reported = null;

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getDetail(): ?FHIRReference
    {
        return $this->detail;
    }

    public function setDetail(?FHIRReference $value): self
    {
        $this->detail = $value;

        return $this;
    }

    public function getReported(): ?FHIRBoolean
    {
        return $this->reported;
    }

    public function setReported(bool|FHIRBoolean|null $value): self
    {
        $this->reported = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }
}
