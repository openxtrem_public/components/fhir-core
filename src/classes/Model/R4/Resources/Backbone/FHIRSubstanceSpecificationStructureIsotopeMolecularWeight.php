<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceSpecificationStructureIsotopeMolecularWeight Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceSpecificationStructureIsotopeMolecularWeightInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;

class FHIRSubstanceSpecificationStructureIsotopeMolecularWeight extends FHIRBackboneElement implements FHIRSubstanceSpecificationStructureIsotopeMolecularWeightInterface
{
    public const RESOURCE_NAME = 'SubstanceSpecification.structure.isotope.molecularWeight';

    protected ?FHIRCodeableConcept $method = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRQuantity $amount = null;

    public function getMethod(): ?FHIRCodeableConcept
    {
        return $this->method;
    }

    public function setMethod(?FHIRCodeableConcept $value): self
    {
        $this->method = $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getAmount(): ?FHIRQuantity
    {
        return $this->amount;
    }

    public function setAmount(?FHIRQuantity $value): self
    {
        $this->amount = $value;

        return $this;
    }
}
