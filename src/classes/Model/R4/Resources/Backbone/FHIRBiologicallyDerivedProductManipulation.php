<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR BiologicallyDerivedProductManipulation Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRBiologicallyDerivedProductManipulationInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRBiologicallyDerivedProductManipulation extends FHIRBackboneElement implements FHIRBiologicallyDerivedProductManipulationInterface
{
    public const RESOURCE_NAME = 'BiologicallyDerivedProduct.manipulation';

    protected ?FHIRString $description = null;
    protected FHIRDateTime|FHIRPeriod|null $time = null;

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTime(): FHIRDateTime|FHIRPeriod|null
    {
        return $this->time;
    }

    public function setTime(FHIRDateTime|FHIRPeriod|null $value): self
    {
        $this->time = $value;

        return $this;
    }
}
