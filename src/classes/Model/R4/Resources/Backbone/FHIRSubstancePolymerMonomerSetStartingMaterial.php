<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstancePolymerMonomerSetStartingMaterial Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstancePolymerMonomerSetStartingMaterialInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRSubstanceAmount;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;

class FHIRSubstancePolymerMonomerSetStartingMaterial extends FHIRBackboneElement implements FHIRSubstancePolymerMonomerSetStartingMaterialInterface
{
    public const RESOURCE_NAME = 'SubstancePolymer.monomerSet.startingMaterial';

    protected ?FHIRCodeableConcept $material = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRBoolean $isDefining = null;
    protected ?FHIRSubstanceAmount $amount = null;

    public function getMaterial(): ?FHIRCodeableConcept
    {
        return $this->material;
    }

    public function setMaterial(?FHIRCodeableConcept $value): self
    {
        $this->material = $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getIsDefining(): ?FHIRBoolean
    {
        return $this->isDefining;
    }

    public function setIsDefining(bool|FHIRBoolean|null $value): self
    {
        $this->isDefining = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getAmount(): ?FHIRSubstanceAmount
    {
        return $this->amount;
    }

    public function setAmount(?FHIRSubstanceAmount $value): self
    {
        $this->amount = $value;

        return $this;
    }
}
