<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationRequestDispenseRequestInitialFill Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationRequestDispenseRequestInitialFillInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;

class FHIRMedicationRequestDispenseRequestInitialFill extends FHIRBackboneElement implements FHIRMedicationRequestDispenseRequestInitialFillInterface
{
    public const RESOURCE_NAME = 'MedicationRequest.dispenseRequest.initialFill';

    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRDuration $duration = null;

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getDuration(): ?FHIRDuration
    {
        return $this->duration;
    }

    public function setDuration(?FHIRDuration $value): self
    {
        $this->duration = $value;

        return $this;
    }
}
