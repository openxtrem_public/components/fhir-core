<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR BundleLink Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRBundleLinkInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUri;

class FHIRBundleLink extends FHIRBackboneElement implements FHIRBundleLinkInterface
{
    public const RESOURCE_NAME = 'Bundle.link';

    protected ?FHIRString $relation = null;
    protected ?FHIRUri $url = null;

    public function getRelation(): ?FHIRString
    {
        return $this->relation;
    }

    public function setRelation(string|FHIRString|null $value): self
    {
        $this->relation = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }
}
