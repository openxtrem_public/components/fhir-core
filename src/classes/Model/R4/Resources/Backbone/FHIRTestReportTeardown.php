<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestReportTeardown Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestReportTeardownInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;

class FHIRTestReportTeardown extends FHIRBackboneElement implements FHIRTestReportTeardownInterface
{
    public const RESOURCE_NAME = 'TestReport.teardown';

    /** @var FHIRTestReportTeardownAction[] */
    protected array $action = [];

    /**
     * @return FHIRTestReportTeardownAction[]
     */
    public function getAction(): array
    {
        return $this->action;
    }

    public function setAction(?FHIRTestReportTeardownAction ...$value): self
    {
        $this->action = array_filter($value);

        return $this;
    }

    public function addAction(?FHIRTestReportTeardownAction ...$value): self
    {
        $this->action = array_filter(array_merge($this->action, $value));

        return $this;
    }
}
