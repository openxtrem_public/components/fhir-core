<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImplementationGuideDefinitionResource Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImplementationGuideDefinitionResourceInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRImplementationGuideDefinitionResource extends FHIRBackboneElement implements FHIRImplementationGuideDefinitionResourceInterface
{
    public const RESOURCE_NAME = 'ImplementationGuide.definition.resource';

    protected ?FHIRReference $reference = null;

    /** @var FHIRCode[] */
    protected array $fhirVersion = [];
    protected ?FHIRString $name = null;
    protected ?FHIRString $description = null;
    protected FHIRBoolean|FHIRCanonical|null $example = null;
    protected ?FHIRId $groupingId = null;

    public function getReference(): ?FHIRReference
    {
        return $this->reference;
    }

    public function setReference(?FHIRReference $value): self
    {
        $this->reference = $value;

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getFhirVersion(): array
    {
        return $this->fhirVersion;
    }

    public function setFhirVersion(string|FHIRCode|null ...$value): self
    {
        $this->fhirVersion = [];
        $this->addFhirVersion(...$value);

        return $this;
    }

    public function addFhirVersion(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->fhirVersion = array_filter(array_merge($this->fhirVersion, $values));

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getExample(): FHIRBoolean|FHIRCanonical|null
    {
        return $this->example;
    }

    public function setExample(FHIRBoolean|FHIRCanonical|null $value): self
    {
        $this->example = $value;

        return $this;
    }

    public function getGroupingId(): ?FHIRId
    {
        return $this->groupingId;
    }

    public function setGroupingId(string|FHIRId|null $value): self
    {
        $this->groupingId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }
}
