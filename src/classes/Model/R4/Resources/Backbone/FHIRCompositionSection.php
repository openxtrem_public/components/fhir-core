<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CompositionSection Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCompositionSectionInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRNarrative;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRCompositionSection extends FHIRBackboneElement implements FHIRCompositionSectionInterface
{
    public const RESOURCE_NAME = 'Composition.section';

    protected ?FHIRString $title = null;
    protected ?FHIRCodeableConcept $code = null;

    /** @var FHIRReference[] */
    protected array $author = [];
    protected ?FHIRReference $focus = null;
    protected ?FHIRNarrative $text = null;
    protected ?FHIRCode $mode = null;
    protected ?FHIRCodeableConcept $orderedBy = null;

    /** @var FHIRReference[] */
    protected array $entry = [];
    protected ?FHIRCodeableConcept $emptyReason = null;

    /** @var FHIRCompositionSection[] */
    protected array $section = [];

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAuthor(): array
    {
        return $this->author;
    }

    public function setAuthor(?FHIRReference ...$value): self
    {
        $this->author = array_filter($value);

        return $this;
    }

    public function addAuthor(?FHIRReference ...$value): self
    {
        $this->author = array_filter(array_merge($this->author, $value));

        return $this;
    }

    public function getFocus(): ?FHIRReference
    {
        return $this->focus;
    }

    public function setFocus(?FHIRReference $value): self
    {
        $this->focus = $value;

        return $this;
    }

    public function getText(): ?FHIRNarrative
    {
        return $this->text;
    }

    public function setText(?FHIRNarrative $value): self
    {
        $this->text = $value;

        return $this;
    }

    public function getMode(): ?FHIRCode
    {
        return $this->mode;
    }

    public function setMode(string|FHIRCode|null $value): self
    {
        $this->mode = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getOrderedBy(): ?FHIRCodeableConcept
    {
        return $this->orderedBy;
    }

    public function setOrderedBy(?FHIRCodeableConcept $value): self
    {
        $this->orderedBy = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEntry(): array
    {
        return $this->entry;
    }

    public function setEntry(?FHIRReference ...$value): self
    {
        $this->entry = array_filter($value);

        return $this;
    }

    public function addEntry(?FHIRReference ...$value): self
    {
        $this->entry = array_filter(array_merge($this->entry, $value));

        return $this;
    }

    public function getEmptyReason(): ?FHIRCodeableConcept
    {
        return $this->emptyReason;
    }

    public function setEmptyReason(?FHIRCodeableConcept $value): self
    {
        $this->emptyReason = $value;

        return $this;
    }

    /**
     * @return FHIRCompositionSection[]
     */
    public function getSection(): array
    {
        return $this->section;
    }

    public function setSection(?FHIRCompositionSection ...$value): self
    {
        $this->section = array_filter($value);

        return $this;
    }

    public function addSection(?FHIRCompositionSection ...$value): self
    {
        $this->section = array_filter(array_merge($this->section, $value));

        return $this;
    }
}
