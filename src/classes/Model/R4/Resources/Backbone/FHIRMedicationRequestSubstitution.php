<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationRequestSubstitution Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationRequestSubstitutionInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;

class FHIRMedicationRequestSubstitution extends FHIRBackboneElement implements FHIRMedicationRequestSubstitutionInterface
{
    public const RESOURCE_NAME = 'MedicationRequest.substitution';

    protected FHIRBoolean|FHIRCodeableConcept|null $allowed = null;
    protected ?FHIRCodeableConcept $reason = null;

    public function getAllowed(): FHIRBoolean|FHIRCodeableConcept|null
    {
        return $this->allowed;
    }

    public function setAllowed(FHIRBoolean|FHIRCodeableConcept|null $value): self
    {
        $this->allowed = $value;

        return $this;
    }

    public function getReason(): ?FHIRCodeableConcept
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableConcept $value): self
    {
        $this->reason = $value;

        return $this;
    }
}
