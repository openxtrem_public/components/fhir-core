<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceSpecificationNameOfficial Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceSpecificationNameOfficialInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;

class FHIRSubstanceSpecificationNameOfficial extends FHIRBackboneElement implements FHIRSubstanceSpecificationNameOfficialInterface
{
    public const RESOURCE_NAME = 'SubstanceSpecification.name.official';

    protected ?FHIRCodeableConcept $authority = null;
    protected ?FHIRCodeableConcept $status = null;
    protected ?FHIRDateTime $date = null;

    public function getAuthority(): ?FHIRCodeableConcept
    {
        return $this->authority;
    }

    public function setAuthority(?FHIRCodeableConcept $value): self
    {
        $this->authority = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCodeableConcept
    {
        return $this->status;
    }

    public function setStatus(?FHIRCodeableConcept $value): self
    {
        $this->status = $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }
}
