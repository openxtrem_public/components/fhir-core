<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AuditEventAgentNetwork Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAuditEventAgentNetworkInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRAuditEventAgentNetwork extends FHIRBackboneElement implements FHIRAuditEventAgentNetworkInterface
{
    public const RESOURCE_NAME = 'AuditEvent.agent.network';

    protected ?FHIRString $address = null;
    protected ?FHIRCode $type = null;

    public function getAddress(): ?FHIRString
    {
        return $this->address;
    }

    public function setAddress(string|FHIRString|null $value): self
    {
        $this->address = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }
}
