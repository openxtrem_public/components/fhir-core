<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceSpecificationName Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubstanceSpecificationNameInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRSubstanceSpecificationName extends FHIRBackboneElement implements FHIRSubstanceSpecificationNameInterface
{
    public const RESOURCE_NAME = 'SubstanceSpecification.name';

    protected ?FHIRString $name = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableConcept $status = null;
    protected ?FHIRBoolean $preferred = null;

    /** @var FHIRCodeableConcept[] */
    protected array $language = [];

    /** @var FHIRCodeableConcept[] */
    protected array $domain = [];

    /** @var FHIRCodeableConcept[] */
    protected array $jurisdiction = [];

    /** @var FHIRSubstanceSpecificationName[] */
    protected array $synonym = [];

    /** @var FHIRSubstanceSpecificationName[] */
    protected array $translation = [];

    /** @var FHIRSubstanceSpecificationNameOfficial[] */
    protected array $official = [];

    /** @var FHIRReference[] */
    protected array $source = [];

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCodeableConcept
    {
        return $this->status;
    }

    public function setStatus(?FHIRCodeableConcept $value): self
    {
        $this->status = $value;

        return $this;
    }

    public function getPreferred(): ?FHIRBoolean
    {
        return $this->preferred;
    }

    public function setPreferred(bool|FHIRBoolean|null $value): self
    {
        $this->preferred = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getLanguage(): array
    {
        return $this->language;
    }

    public function setLanguage(?FHIRCodeableConcept ...$value): self
    {
        $this->language = array_filter($value);

        return $this;
    }

    public function addLanguage(?FHIRCodeableConcept ...$value): self
    {
        $this->language = array_filter(array_merge($this->language, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getDomain(): array
    {
        return $this->domain;
    }

    public function setDomain(?FHIRCodeableConcept ...$value): self
    {
        $this->domain = array_filter($value);

        return $this;
    }

    public function addDomain(?FHIRCodeableConcept ...$value): self
    {
        $this->domain = array_filter(array_merge($this->domain, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getJurisdiction(): array
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter($value);

        return $this;
    }

    public function addJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter(array_merge($this->jurisdiction, $value));

        return $this;
    }

    /**
     * @return FHIRSubstanceSpecificationName[]
     */
    public function getSynonym(): array
    {
        return $this->synonym;
    }

    public function setSynonym(?FHIRSubstanceSpecificationName ...$value): self
    {
        $this->synonym = array_filter($value);

        return $this;
    }

    public function addSynonym(?FHIRSubstanceSpecificationName ...$value): self
    {
        $this->synonym = array_filter(array_merge($this->synonym, $value));

        return $this;
    }

    /**
     * @return FHIRSubstanceSpecificationName[]
     */
    public function getTranslation(): array
    {
        return $this->translation;
    }

    public function setTranslation(?FHIRSubstanceSpecificationName ...$value): self
    {
        $this->translation = array_filter($value);

        return $this;
    }

    public function addTranslation(?FHIRSubstanceSpecificationName ...$value): self
    {
        $this->translation = array_filter(array_merge($this->translation, $value));

        return $this;
    }

    /**
     * @return FHIRSubstanceSpecificationNameOfficial[]
     */
    public function getOfficial(): array
    {
        return $this->official;
    }

    public function setOfficial(?FHIRSubstanceSpecificationNameOfficial ...$value): self
    {
        $this->official = array_filter($value);

        return $this;
    }

    public function addOfficial(?FHIRSubstanceSpecificationNameOfficial ...$value): self
    {
        $this->official = array_filter(array_merge($this->official, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSource(): array
    {
        return $this->source;
    }

    public function setSource(?FHIRReference ...$value): self
    {
        $this->source = array_filter($value);

        return $this;
    }

    public function addSource(?FHIRReference ...$value): self
    {
        $this->source = array_filter(array_merge($this->source, $value));

        return $this;
    }
}
