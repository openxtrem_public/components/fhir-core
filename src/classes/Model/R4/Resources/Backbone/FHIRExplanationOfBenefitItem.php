<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExplanationOfBenefitItem Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExplanationOfBenefitItemInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAddress;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRPositiveInt;

class FHIRExplanationOfBenefitItem extends FHIRBackboneElement implements FHIRExplanationOfBenefitItemInterface
{
    public const RESOURCE_NAME = 'ExplanationOfBenefit.item';

    protected ?FHIRPositiveInt $sequence = null;

    /** @var FHIRPositiveInt[] */
    protected array $careTeamSequence = [];

    /** @var FHIRPositiveInt[] */
    protected array $diagnosisSequence = [];

    /** @var FHIRPositiveInt[] */
    protected array $procedureSequence = [];

    /** @var FHIRPositiveInt[] */
    protected array $informationSequence = [];
    protected ?FHIRCodeableConcept $revenue = null;
    protected ?FHIRCodeableConcept $category = null;
    protected ?FHIRCodeableConcept $productOrService = null;

    /** @var FHIRCodeableConcept[] */
    protected array $modifier = [];

    /** @var FHIRCodeableConcept[] */
    protected array $programCode = [];
    protected FHIRDate|FHIRPeriod|null $serviced = null;
    protected FHIRCodeableConcept|FHIRAddress|FHIRReference|null $location = null;
    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRMoney $unitPrice = null;
    protected ?FHIRDecimal $factor = null;
    protected ?FHIRMoney $net = null;

    /** @var FHIRReference[] */
    protected array $udi = [];
    protected ?FHIRCodeableConcept $bodySite = null;

    /** @var FHIRCodeableConcept[] */
    protected array $subSite = [];

    /** @var FHIRReference[] */
    protected array $encounter = [];

    /** @var FHIRPositiveInt[] */
    protected array $noteNumber = [];

    /** @var FHIRExplanationOfBenefitItemAdjudication[] */
    protected array $adjudication = [];

    /** @var FHIRExplanationOfBenefitItemDetail[] */
    protected array $detail = [];

    public function getSequence(): ?FHIRPositiveInt
    {
        return $this->sequence;
    }

    public function setSequence(int|FHIRPositiveInt|null $value): self
    {
        $this->sequence = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRPositiveInt[]
     */
    public function getCareTeamSequence(): array
    {
        return $this->careTeamSequence;
    }

    public function setCareTeamSequence(int|FHIRPositiveInt|null ...$value): self
    {
        $this->careTeamSequence = [];
        $this->addCareTeamSequence(...$value);

        return $this;
    }

    public function addCareTeamSequence(int|FHIRPositiveInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRPositiveInt())->setValue($v) : $v, $value);

        $this->careTeamSequence = array_filter(array_merge($this->careTeamSequence, $values));

        return $this;
    }

    /**
     * @return FHIRPositiveInt[]
     */
    public function getDiagnosisSequence(): array
    {
        return $this->diagnosisSequence;
    }

    public function setDiagnosisSequence(int|FHIRPositiveInt|null ...$value): self
    {
        $this->diagnosisSequence = [];
        $this->addDiagnosisSequence(...$value);

        return $this;
    }

    public function addDiagnosisSequence(int|FHIRPositiveInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRPositiveInt())->setValue($v) : $v, $value);

        $this->diagnosisSequence = array_filter(array_merge($this->diagnosisSequence, $values));

        return $this;
    }

    /**
     * @return FHIRPositiveInt[]
     */
    public function getProcedureSequence(): array
    {
        return $this->procedureSequence;
    }

    public function setProcedureSequence(int|FHIRPositiveInt|null ...$value): self
    {
        $this->procedureSequence = [];
        $this->addProcedureSequence(...$value);

        return $this;
    }

    public function addProcedureSequence(int|FHIRPositiveInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRPositiveInt())->setValue($v) : $v, $value);

        $this->procedureSequence = array_filter(array_merge($this->procedureSequence, $values));

        return $this;
    }

    /**
     * @return FHIRPositiveInt[]
     */
    public function getInformationSequence(): array
    {
        return $this->informationSequence;
    }

    public function setInformationSequence(int|FHIRPositiveInt|null ...$value): self
    {
        $this->informationSequence = [];
        $this->addInformationSequence(...$value);

        return $this;
    }

    public function addInformationSequence(int|FHIRPositiveInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRPositiveInt())->setValue($v) : $v, $value);

        $this->informationSequence = array_filter(array_merge($this->informationSequence, $values));

        return $this;
    }

    public function getRevenue(): ?FHIRCodeableConcept
    {
        return $this->revenue;
    }

    public function setRevenue(?FHIRCodeableConcept $value): self
    {
        $this->revenue = $value;

        return $this;
    }

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getProductOrService(): ?FHIRCodeableConcept
    {
        return $this->productOrService;
    }

    public function setProductOrService(?FHIRCodeableConcept $value): self
    {
        $this->productOrService = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getModifier(): array
    {
        return $this->modifier;
    }

    public function setModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->modifier = array_filter($value);

        return $this;
    }

    public function addModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->modifier = array_filter(array_merge($this->modifier, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getProgramCode(): array
    {
        return $this->programCode;
    }

    public function setProgramCode(?FHIRCodeableConcept ...$value): self
    {
        $this->programCode = array_filter($value);

        return $this;
    }

    public function addProgramCode(?FHIRCodeableConcept ...$value): self
    {
        $this->programCode = array_filter(array_merge($this->programCode, $value));

        return $this;
    }

    public function getServiced(): FHIRDate|FHIRPeriod|null
    {
        return $this->serviced;
    }

    public function setServiced(FHIRDate|FHIRPeriod|null $value): self
    {
        $this->serviced = $value;

        return $this;
    }

    public function getLocation(): FHIRCodeableConcept|FHIRAddress|FHIRReference|null
    {
        return $this->location;
    }

    public function setLocation(FHIRCodeableConcept|FHIRAddress|FHIRReference|null $value): self
    {
        $this->location = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getUnitPrice(): ?FHIRMoney
    {
        return $this->unitPrice;
    }

    public function setUnitPrice(?FHIRMoney $value): self
    {
        $this->unitPrice = $value;

        return $this;
    }

    public function getFactor(): ?FHIRDecimal
    {
        return $this->factor;
    }

    public function setFactor(float|FHIRDecimal|null $value): self
    {
        $this->factor = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getNet(): ?FHIRMoney
    {
        return $this->net;
    }

    public function setNet(?FHIRMoney $value): self
    {
        $this->net = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getUdi(): array
    {
        return $this->udi;
    }

    public function setUdi(?FHIRReference ...$value): self
    {
        $this->udi = array_filter($value);

        return $this;
    }

    public function addUdi(?FHIRReference ...$value): self
    {
        $this->udi = array_filter(array_merge($this->udi, $value));

        return $this;
    }

    public function getBodySite(): ?FHIRCodeableConcept
    {
        return $this->bodySite;
    }

    public function setBodySite(?FHIRCodeableConcept $value): self
    {
        $this->bodySite = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSubSite(): array
    {
        return $this->subSite;
    }

    public function setSubSite(?FHIRCodeableConcept ...$value): self
    {
        $this->subSite = array_filter($value);

        return $this;
    }

    public function addSubSite(?FHIRCodeableConcept ...$value): self
    {
        $this->subSite = array_filter(array_merge($this->subSite, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEncounter(): array
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference ...$value): self
    {
        $this->encounter = array_filter($value);

        return $this;
    }

    public function addEncounter(?FHIRReference ...$value): self
    {
        $this->encounter = array_filter(array_merge($this->encounter, $value));

        return $this;
    }

    /**
     * @return FHIRPositiveInt[]
     */
    public function getNoteNumber(): array
    {
        return $this->noteNumber;
    }

    public function setNoteNumber(int|FHIRPositiveInt|null ...$value): self
    {
        $this->noteNumber = [];
        $this->addNoteNumber(...$value);

        return $this;
    }

    public function addNoteNumber(int|FHIRPositiveInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRPositiveInt())->setValue($v) : $v, $value);

        $this->noteNumber = array_filter(array_merge($this->noteNumber, $values));

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitItemAdjudication[]
     */
    public function getAdjudication(): array
    {
        return $this->adjudication;
    }

    public function setAdjudication(?FHIRExplanationOfBenefitItemAdjudication ...$value): self
    {
        $this->adjudication = array_filter($value);

        return $this;
    }

    public function addAdjudication(?FHIRExplanationOfBenefitItemAdjudication ...$value): self
    {
        $this->adjudication = array_filter(array_merge($this->adjudication, $value));

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitItemDetail[]
     */
    public function getDetail(): array
    {
        return $this->detail;
    }

    public function setDetail(?FHIRExplanationOfBenefitItemDetail ...$value): self
    {
        $this->detail = array_filter($value);

        return $this;
    }

    public function addDetail(?FHIRExplanationOfBenefitItemDetail ...$value): self
    {
        $this->detail = array_filter(array_merge($this->detail, $value));

        return $this;
    }
}
