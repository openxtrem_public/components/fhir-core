<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EffectEvidenceSynthesisSampleSize Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREffectEvidenceSynthesisSampleSizeInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIREffectEvidenceSynthesisSampleSize extends FHIRBackboneElement implements FHIREffectEvidenceSynthesisSampleSizeInterface
{
    public const RESOURCE_NAME = 'EffectEvidenceSynthesis.sampleSize';

    protected ?FHIRString $description = null;
    protected ?FHIRInteger $numberOfStudies = null;
    protected ?FHIRInteger $numberOfParticipants = null;

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getNumberOfStudies(): ?FHIRInteger
    {
        return $this->numberOfStudies;
    }

    public function setNumberOfStudies(int|FHIRInteger|null $value): self
    {
        $this->numberOfStudies = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getNumberOfParticipants(): ?FHIRInteger
    {
        return $this->numberOfParticipants;
    }

    public function setNumberOfParticipants(int|FHIRInteger|null $value): self
    {
        $this->numberOfParticipants = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }
}
