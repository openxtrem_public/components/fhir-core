<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductPackagedBatchIdentifier Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductPackagedBatchIdentifierInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;

class FHIRMedicinalProductPackagedBatchIdentifier extends FHIRBackboneElement implements FHIRMedicinalProductPackagedBatchIdentifierInterface
{
    public const RESOURCE_NAME = 'MedicinalProductPackaged.batchIdentifier';

    protected ?FHIRIdentifier $outerPackaging = null;
    protected ?FHIRIdentifier $immediatePackaging = null;

    public function getOuterPackaging(): ?FHIRIdentifier
    {
        return $this->outerPackaging;
    }

    public function setOuterPackaging(?FHIRIdentifier $value): self
    {
        $this->outerPackaging = $value;

        return $this;
    }

    public function getImmediatePackaging(): ?FHIRIdentifier
    {
        return $this->immediatePackaging;
    }

    public function setImmediatePackaging(?FHIRIdentifier $value): self
    {
        $this->immediatePackaging = $value;

        return $this;
    }
}
