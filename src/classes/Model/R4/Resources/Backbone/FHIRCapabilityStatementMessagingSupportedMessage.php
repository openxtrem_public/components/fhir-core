<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CapabilityStatementMessagingSupportedMessage Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCapabilityStatementMessagingSupportedMessageInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;

class FHIRCapabilityStatementMessagingSupportedMessage extends FHIRBackboneElement implements FHIRCapabilityStatementMessagingSupportedMessageInterface
{
    public const RESOURCE_NAME = 'CapabilityStatement.messaging.supportedMessage';

    protected ?FHIRCode $mode = null;
    protected ?FHIRCanonical $definition = null;

    public function getMode(): ?FHIRCode
    {
        return $this->mode;
    }

    public function setMode(string|FHIRCode|null $value): self
    {
        $this->mode = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDefinition(): ?FHIRCanonical
    {
        return $this->definition;
    }

    public function setDefinition(string|FHIRCanonical|null $value): self
    {
        $this->definition = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }
}
