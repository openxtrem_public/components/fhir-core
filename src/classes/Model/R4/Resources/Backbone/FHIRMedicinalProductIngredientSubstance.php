<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductIngredientSubstance Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductIngredientSubstanceInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;

class FHIRMedicinalProductIngredientSubstance extends FHIRBackboneElement implements FHIRMedicinalProductIngredientSubstanceInterface
{
    public const RESOURCE_NAME = 'MedicinalProductIngredient.substance';

    protected ?FHIRCodeableConcept $code = null;

    /** @var FHIRMedicinalProductIngredientSpecifiedSubstanceStrength[] */
    protected array $strength = [];

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * @return FHIRMedicinalProductIngredientSpecifiedSubstanceStrength[]
     */
    public function getStrength(): array
    {
        return $this->strength;
    }

    public function setStrength(?FHIRMedicinalProductIngredientSpecifiedSubstanceStrength ...$value): self
    {
        $this->strength = array_filter($value);

        return $this;
    }

    public function addStrength(?FHIRMedicinalProductIngredientSpecifiedSubstanceStrength ...$value): self
    {
        $this->strength = array_filter(array_merge($this->strength, $value));

        return $this;
    }
}
