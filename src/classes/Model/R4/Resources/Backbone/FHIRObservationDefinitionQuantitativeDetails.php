<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ObservationDefinitionQuantitativeDetails Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRObservationDefinitionQuantitativeDetailsInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRInteger;

class FHIRObservationDefinitionQuantitativeDetails extends FHIRBackboneElement implements FHIRObservationDefinitionQuantitativeDetailsInterface
{
    public const RESOURCE_NAME = 'ObservationDefinition.quantitativeDetails';

    protected ?FHIRCodeableConcept $customaryUnit = null;
    protected ?FHIRCodeableConcept $unit = null;
    protected ?FHIRDecimal $conversionFactor = null;
    protected ?FHIRInteger $decimalPrecision = null;

    public function getCustomaryUnit(): ?FHIRCodeableConcept
    {
        return $this->customaryUnit;
    }

    public function setCustomaryUnit(?FHIRCodeableConcept $value): self
    {
        $this->customaryUnit = $value;

        return $this;
    }

    public function getUnit(): ?FHIRCodeableConcept
    {
        return $this->unit;
    }

    public function setUnit(?FHIRCodeableConcept $value): self
    {
        $this->unit = $value;

        return $this;
    }

    public function getConversionFactor(): ?FHIRDecimal
    {
        return $this->conversionFactor;
    }

    public function setConversionFactor(float|FHIRDecimal|null $value): self
    {
        $this->conversionFactor = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getDecimalPrecision(): ?FHIRInteger
    {
        return $this->decimalPrecision;
    }

    public function setDecimalPrecision(int|FHIRInteger|null $value): self
    {
        $this->decimalPrecision = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }
}
