<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductPharmaceuticalRouteOfAdministrationTargetSpecies Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductPharmaceuticalRouteOfAdministrationTargetSpeciesInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;

class FHIRMedicinalProductPharmaceuticalRouteOfAdministrationTargetSpecies extends FHIRBackboneElement implements FHIRMedicinalProductPharmaceuticalRouteOfAdministrationTargetSpeciesInterface
{
    public const RESOURCE_NAME = 'MedicinalProductPharmaceutical.routeOfAdministration.targetSpecies';

    protected ?FHIRCodeableConcept $code = null;

    /** @var FHIRMedicinalProductPharmaceuticalRouteOfAdministrationTargetSpeciesWithdrawalPeriod[] */
    protected array $withdrawalPeriod = [];

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * @return FHIRMedicinalProductPharmaceuticalRouteOfAdministrationTargetSpeciesWithdrawalPeriod[]
     */
    public function getWithdrawalPeriod(): array
    {
        return $this->withdrawalPeriod;
    }

    public function setWithdrawalPeriod(
        ?FHIRMedicinalProductPharmaceuticalRouteOfAdministrationTargetSpeciesWithdrawalPeriod ...$value,
    ): self
    {
        $this->withdrawalPeriod = array_filter($value);

        return $this;
    }

    public function addWithdrawalPeriod(
        ?FHIRMedicinalProductPharmaceuticalRouteOfAdministrationTargetSpeciesWithdrawalPeriod ...$value,
    ): self
    {
        $this->withdrawalPeriod = array_filter(array_merge($this->withdrawalPeriod, $value));

        return $this;
    }
}
