<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImplementationGuideManifestResource Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImplementationGuideManifestResourceInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUrl;

class FHIRImplementationGuideManifestResource extends FHIRBackboneElement implements FHIRImplementationGuideManifestResourceInterface
{
    public const RESOURCE_NAME = 'ImplementationGuide.manifest.resource';

    protected ?FHIRReference $reference = null;
    protected FHIRBoolean|FHIRCanonical|null $example = null;
    protected ?FHIRUrl $relativePath = null;

    public function getReference(): ?FHIRReference
    {
        return $this->reference;
    }

    public function setReference(?FHIRReference $value): self
    {
        $this->reference = $value;

        return $this;
    }

    public function getExample(): FHIRBoolean|FHIRCanonical|null
    {
        return $this->example;
    }

    public function setExample(FHIRBoolean|FHIRCanonical|null $value): self
    {
        $this->example = $value;

        return $this;
    }

    public function getRelativePath(): ?FHIRUrl
    {
        return $this->relativePath;
    }

    public function setRelativePath(string|FHIRUrl|null $value): self
    {
        $this->relativePath = is_string($value) ? (new FHIRUrl())->setValue($value) : $value;

        return $this;
    }
}
