<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceDefinitionSpecialization Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceDefinitionSpecializationInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRDeviceDefinitionSpecialization extends FHIRBackboneElement implements FHIRDeviceDefinitionSpecializationInterface
{
    public const RESOURCE_NAME = 'DeviceDefinition.specialization';

    protected ?FHIRString $systemType = null;
    protected ?FHIRString $version = null;

    public function getSystemType(): ?FHIRString
    {
        return $this->systemType;
    }

    public function setSystemType(string|FHIRString|null $value): self
    {
        $this->systemType = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
