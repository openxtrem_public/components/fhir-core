<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR StructureDefinitionDifferential Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRStructureDefinitionDifferentialInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRElementDefinition;

class FHIRStructureDefinitionDifferential extends FHIRBackboneElement implements FHIRStructureDefinitionDifferentialInterface
{
    public const RESOURCE_NAME = 'StructureDefinition.differential';

    /** @var FHIRElementDefinition[] */
    protected array $element = [];

    /**
     * @return FHIRElementDefinition[]
     */
    public function getElement(): array
    {
        return $this->element;
    }

    public function setElement(?FHIRElementDefinition ...$value): self
    {
        $this->element = array_filter($value);

        return $this;
    }

    public function addElement(?FHIRElementDefinition ...$value): self
    {
        $this->element = array_filter(array_merge($this->element, $value));

        return $this;
    }
}
