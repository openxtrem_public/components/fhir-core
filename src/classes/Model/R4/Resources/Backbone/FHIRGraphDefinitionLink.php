<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR GraphDefinitionLink Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRGraphDefinitionLinkInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRGraphDefinitionLink extends FHIRBackboneElement implements FHIRGraphDefinitionLinkInterface
{
    public const RESOURCE_NAME = 'GraphDefinition.link';

    protected ?FHIRString $path = null;
    protected ?FHIRString $sliceName = null;
    protected ?FHIRInteger $min = null;
    protected ?FHIRString $max = null;
    protected ?FHIRString $description = null;

    /** @var FHIRGraphDefinitionLinkTarget[] */
    protected array $target = [];

    public function getPath(): ?FHIRString
    {
        return $this->path;
    }

    public function setPath(string|FHIRString|null $value): self
    {
        $this->path = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSliceName(): ?FHIRString
    {
        return $this->sliceName;
    }

    public function setSliceName(string|FHIRString|null $value): self
    {
        $this->sliceName = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getMin(): ?FHIRInteger
    {
        return $this->min;
    }

    public function setMin(int|FHIRInteger|null $value): self
    {
        $this->min = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getMax(): ?FHIRString
    {
        return $this->max;
    }

    public function setMax(string|FHIRString|null $value): self
    {
        $this->max = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRGraphDefinitionLinkTarget[]
     */
    public function getTarget(): array
    {
        return $this->target;
    }

    public function setTarget(?FHIRGraphDefinitionLinkTarget ...$value): self
    {
        $this->target = array_filter($value);

        return $this;
    }

    public function addTarget(?FHIRGraphDefinitionLinkTarget ...$value): self
    {
        $this->target = array_filter(array_merge($this->target, $value));

        return $this;
    }
}
