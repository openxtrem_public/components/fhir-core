<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AuditEventEntityDetail Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAuditEventEntityDetailInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBase64Binary;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRAuditEventEntityDetail extends FHIRBackboneElement implements FHIRAuditEventEntityDetailInterface
{
    public const RESOURCE_NAME = 'AuditEvent.entity.detail';

    protected ?FHIRString $type = null;
    protected FHIRString|FHIRBase64Binary|null $value = null;

    public function getType(): ?FHIRString
    {
        return $this->type;
    }

    public function setType(string|FHIRString|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getValue(): FHIRString|FHIRBase64Binary|null
    {
        return $this->value;
    }

    public function setValue(FHIRString|FHIRBase64Binary|null $value): self
    {
        $this->value = $value;

        return $this;
    }
}
