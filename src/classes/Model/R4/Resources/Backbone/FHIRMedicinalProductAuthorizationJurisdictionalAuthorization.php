<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductAuthorizationJurisdictionalAuthorization Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductAuthorizationJurisdictionalAuthorizationInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;

class FHIRMedicinalProductAuthorizationJurisdictionalAuthorization extends FHIRBackboneElement implements FHIRMedicinalProductAuthorizationJurisdictionalAuthorizationInterface
{
    public const RESOURCE_NAME = 'MedicinalProductAuthorization.jurisdictionalAuthorization';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCodeableConcept $country = null;

    /** @var FHIRCodeableConcept[] */
    protected array $jurisdiction = [];
    protected ?FHIRCodeableConcept $legalStatusOfSupply = null;
    protected ?FHIRPeriod $validityPeriod = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getCountry(): ?FHIRCodeableConcept
    {
        return $this->country;
    }

    public function setCountry(?FHIRCodeableConcept $value): self
    {
        $this->country = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getJurisdiction(): array
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter($value);

        return $this;
    }

    public function addJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter(array_merge($this->jurisdiction, $value));

        return $this;
    }

    public function getLegalStatusOfSupply(): ?FHIRCodeableConcept
    {
        return $this->legalStatusOfSupply;
    }

    public function setLegalStatusOfSupply(?FHIRCodeableConcept $value): self
    {
        $this->legalStatusOfSupply = $value;

        return $this;
    }

    public function getValidityPeriod(): ?FHIRPeriod
    {
        return $this->validityPeriod;
    }

    public function setValidityPeriod(?FHIRPeriod $value): self
    {
        $this->validityPeriod = $value;

        return $this;
    }
}
