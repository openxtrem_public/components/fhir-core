<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR GroupMember Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRGroupMemberInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;

class FHIRGroupMember extends FHIRBackboneElement implements FHIRGroupMemberInterface
{
    public const RESOURCE_NAME = 'Group.member';

    protected ?FHIRReference $entity = null;
    protected ?FHIRPeriod $period = null;
    protected ?FHIRBoolean $inactive = null;

    public function getEntity(): ?FHIRReference
    {
        return $this->entity;
    }

    public function setEntity(?FHIRReference $value): self
    {
        $this->entity = $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    public function getInactive(): ?FHIRBoolean
    {
        return $this->inactive;
    }

    public function setInactive(bool|FHIRBoolean|null $value): self
    {
        $this->inactive = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }
}
