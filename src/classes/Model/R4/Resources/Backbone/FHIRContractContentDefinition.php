<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ContractContentDefinition Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRContractContentDefinitionInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRMarkdown;

class FHIRContractContentDefinition extends FHIRBackboneElement implements FHIRContractContentDefinitionInterface
{
    public const RESOURCE_NAME = 'Contract.contentDefinition';

    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableConcept $subType = null;
    protected ?FHIRReference $publisher = null;
    protected ?FHIRDateTime $publicationDate = null;
    protected ?FHIRCode $publicationStatus = null;
    protected ?FHIRMarkdown $copyright = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getSubType(): ?FHIRCodeableConcept
    {
        return $this->subType;
    }

    public function setSubType(?FHIRCodeableConcept $value): self
    {
        $this->subType = $value;

        return $this;
    }

    public function getPublisher(): ?FHIRReference
    {
        return $this->publisher;
    }

    public function setPublisher(?FHIRReference $value): self
    {
        $this->publisher = $value;

        return $this;
    }

    public function getPublicationDate(): ?FHIRDateTime
    {
        return $this->publicationDate;
    }

    public function setPublicationDate(string|FHIRDateTime|null $value): self
    {
        $this->publicationDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPublicationStatus(): ?FHIRCode
    {
        return $this->publicationStatus;
    }

    public function setPublicationStatus(string|FHIRCode|null $value): self
    {
        $this->publicationStatus = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCopyright(): ?FHIRMarkdown
    {
        return $this->copyright;
    }

    public function setCopyright(string|FHIRMarkdown|null $value): self
    {
        $this->copyright = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }
}
