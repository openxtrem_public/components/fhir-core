<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestScriptVariable Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestScriptVariableInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRTestScriptVariable extends FHIRBackboneElement implements FHIRTestScriptVariableInterface
{
    public const RESOURCE_NAME = 'TestScript.variable';

    protected ?FHIRString $name = null;
    protected ?FHIRString $defaultValue = null;
    protected ?FHIRString $description = null;
    protected ?FHIRString $expression = null;
    protected ?FHIRString $headerField = null;
    protected ?FHIRString $hint = null;
    protected ?FHIRString $path = null;
    protected ?FHIRId $sourceId = null;

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDefaultValue(): ?FHIRString
    {
        return $this->defaultValue;
    }

    public function setDefaultValue(string|FHIRString|null $value): self
    {
        $this->defaultValue = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getExpression(): ?FHIRString
    {
        return $this->expression;
    }

    public function setExpression(string|FHIRString|null $value): self
    {
        $this->expression = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getHeaderField(): ?FHIRString
    {
        return $this->headerField;
    }

    public function setHeaderField(string|FHIRString|null $value): self
    {
        $this->headerField = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getHint(): ?FHIRString
    {
        return $this->hint;
    }

    public function setHint(string|FHIRString|null $value): self
    {
        $this->hint = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPath(): ?FHIRString
    {
        return $this->path;
    }

    public function setPath(string|FHIRString|null $value): self
    {
        $this->path = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSourceId(): ?FHIRId
    {
        return $this->sourceId;
    }

    public function setSourceId(string|FHIRId|null $value): self
    {
        $this->sourceId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }
}
