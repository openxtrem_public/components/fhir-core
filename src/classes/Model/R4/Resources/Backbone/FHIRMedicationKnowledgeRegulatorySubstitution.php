<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledgeRegulatorySubstitution Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationKnowledgeRegulatorySubstitutionInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;

class FHIRMedicationKnowledgeRegulatorySubstitution extends FHIRBackboneElement implements FHIRMedicationKnowledgeRegulatorySubstitutionInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge.regulatory.substitution';

    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRBoolean $allowed = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getAllowed(): ?FHIRBoolean
    {
        return $this->allowed;
    }

    public function setAllowed(bool|FHIRBoolean|null $value): self
    {
        $this->allowed = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }
}
