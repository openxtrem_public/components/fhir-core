<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductContraindicationOtherTherapy Backbone
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicinalProductContraindicationOtherTherapyInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;

class FHIRMedicinalProductContraindicationOtherTherapy extends FHIRBackboneElement implements FHIRMedicinalProductContraindicationOtherTherapyInterface
{
    public const RESOURCE_NAME = 'MedicinalProductContraindication.otherTherapy';

    protected ?FHIRCodeableConcept $therapyRelationshipType = null;
    protected FHIRCodeableConcept|FHIRReference|null $medication = null;

    public function getTherapyRelationshipType(): ?FHIRCodeableConcept
    {
        return $this->therapyRelationshipType;
    }

    public function setTherapyRelationshipType(?FHIRCodeableConcept $value): self
    {
        $this->therapyRelationshipType = $value;

        return $this;
    }

    public function getMedication(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->medication;
    }

    public function setMedication(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->medication = $value;

        return $this;
    }
}
