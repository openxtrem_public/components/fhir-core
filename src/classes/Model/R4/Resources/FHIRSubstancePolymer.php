<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstancePolymer Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRSubstancePolymerInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRSubstancePolymerMonomerSet;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRSubstancePolymerRepeat;

class FHIRSubstancePolymer extends FHIRDomainResource implements FHIRSubstancePolymerInterface
{
    public const RESOURCE_NAME = 'SubstancePolymer';

    protected ?FHIRCodeableConcept $class = null;
    protected ?FHIRCodeableConcept $geometry = null;

    /** @var FHIRCodeableConcept[] */
    protected array $copolymerConnectivity = [];

    /** @var FHIRString[] */
    protected array $modification = [];

    /** @var FHIRSubstancePolymerMonomerSet[] */
    protected array $monomerSet = [];

    /** @var FHIRSubstancePolymerRepeat[] */
    protected array $repeat = [];

    public function getClass(): ?FHIRCodeableConcept
    {
        return $this->class;
    }

    public function setClass(?FHIRCodeableConcept $value): self
    {
        $this->class = $value;

        return $this;
    }

    public function getGeometry(): ?FHIRCodeableConcept
    {
        return $this->geometry;
    }

    public function setGeometry(?FHIRCodeableConcept $value): self
    {
        $this->geometry = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCopolymerConnectivity(): array
    {
        return $this->copolymerConnectivity;
    }

    public function setCopolymerConnectivity(?FHIRCodeableConcept ...$value): self
    {
        $this->copolymerConnectivity = array_filter($value);

        return $this;
    }

    public function addCopolymerConnectivity(?FHIRCodeableConcept ...$value): self
    {
        $this->copolymerConnectivity = array_filter(array_merge($this->copolymerConnectivity, $value));

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getModification(): array
    {
        return $this->modification;
    }

    public function setModification(string|FHIRString|null ...$value): self
    {
        $this->modification = [];
        $this->addModification(...$value);

        return $this;
    }

    public function addModification(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->modification = array_filter(array_merge($this->modification, $values));

        return $this;
    }

    /**
     * @return FHIRSubstancePolymerMonomerSet[]
     */
    public function getMonomerSet(): array
    {
        return $this->monomerSet;
    }

    public function setMonomerSet(?FHIRSubstancePolymerMonomerSet ...$value): self
    {
        $this->monomerSet = array_filter($value);

        return $this;
    }

    public function addMonomerSet(?FHIRSubstancePolymerMonomerSet ...$value): self
    {
        $this->monomerSet = array_filter(array_merge($this->monomerSet, $value));

        return $this;
    }

    /**
     * @return FHIRSubstancePolymerRepeat[]
     */
    public function getRepeat(): array
    {
        return $this->repeat;
    }

    public function setRepeat(?FHIRSubstancePolymerRepeat ...$value): self
    {
        $this->repeat = array_filter($value);

        return $this;
    }

    public function addRepeat(?FHIRSubstancePolymerRepeat ...$value): self
    {
        $this->repeat = array_filter(array_merge($this->repeat, $value));

        return $this;
    }
}
