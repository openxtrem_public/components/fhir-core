<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR BodyStructure Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRBodyStructureInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRBodyStructure extends FHIRDomainResource implements FHIRBodyStructureInterface
{
    public const RESOURCE_NAME = 'BodyStructure';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRBoolean $active = null;
    protected ?FHIRCodeableConcept $morphology = null;
    protected ?FHIRCodeableConcept $location = null;

    /** @var FHIRCodeableConcept[] */
    protected array $locationQualifier = [];
    protected ?FHIRString $description = null;

    /** @var FHIRAttachment[] */
    protected array $image = [];
    protected ?FHIRReference $patient = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getActive(): ?FHIRBoolean
    {
        return $this->active;
    }

    public function setActive(bool|FHIRBoolean|null $value): self
    {
        $this->active = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getMorphology(): ?FHIRCodeableConcept
    {
        return $this->morphology;
    }

    public function setMorphology(?FHIRCodeableConcept $value): self
    {
        $this->morphology = $value;

        return $this;
    }

    public function getLocation(): ?FHIRCodeableConcept
    {
        return $this->location;
    }

    public function setLocation(?FHIRCodeableConcept $value): self
    {
        $this->location = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getLocationQualifier(): array
    {
        return $this->locationQualifier;
    }

    public function setLocationQualifier(?FHIRCodeableConcept ...$value): self
    {
        $this->locationQualifier = array_filter($value);

        return $this;
    }

    public function addLocationQualifier(?FHIRCodeableConcept ...$value): self
    {
        $this->locationQualifier = array_filter(array_merge($this->locationQualifier, $value));

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAttachment[]
     */
    public function getImage(): array
    {
        return $this->image;
    }

    public function setImage(?FHIRAttachment ...$value): self
    {
        $this->image = array_filter($value);

        return $this;
    }

    public function addImage(?FHIRAttachment ...$value): self
    {
        $this->image = array_filter(array_merge($this->image, $value));

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }
}
