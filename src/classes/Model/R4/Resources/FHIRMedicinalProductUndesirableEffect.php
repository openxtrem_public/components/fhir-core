<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductUndesirableEffect Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMedicinalProductUndesirableEffectInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPopulation;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;

class FHIRMedicinalProductUndesirableEffect extends FHIRDomainResource implements FHIRMedicinalProductUndesirableEffectInterface
{
    public const RESOURCE_NAME = 'MedicinalProductUndesirableEffect';

    /** @var FHIRReference[] */
    protected array $subject = [];
    protected ?FHIRCodeableConcept $symptomConditionEffect = null;
    protected ?FHIRCodeableConcept $classification = null;
    protected ?FHIRCodeableConcept $frequencyOfOccurrence = null;

    /** @var FHIRPopulation[] */
    protected array $population = [];

    /**
     * @return FHIRReference[]
     */
    public function getSubject(): array
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference ...$value): self
    {
        $this->subject = array_filter($value);

        return $this;
    }

    public function addSubject(?FHIRReference ...$value): self
    {
        $this->subject = array_filter(array_merge($this->subject, $value));

        return $this;
    }

    public function getSymptomConditionEffect(): ?FHIRCodeableConcept
    {
        return $this->symptomConditionEffect;
    }

    public function setSymptomConditionEffect(?FHIRCodeableConcept $value): self
    {
        $this->symptomConditionEffect = $value;

        return $this;
    }

    public function getClassification(): ?FHIRCodeableConcept
    {
        return $this->classification;
    }

    public function setClassification(?FHIRCodeableConcept $value): self
    {
        $this->classification = $value;

        return $this;
    }

    public function getFrequencyOfOccurrence(): ?FHIRCodeableConcept
    {
        return $this->frequencyOfOccurrence;
    }

    public function setFrequencyOfOccurrence(?FHIRCodeableConcept $value): self
    {
        $this->frequencyOfOccurrence = $value;

        return $this;
    }

    /**
     * @return FHIRPopulation[]
     */
    public function getPopulation(): array
    {
        return $this->population;
    }

    public function setPopulation(?FHIRPopulation ...$value): self
    {
        $this->population = array_filter($value);

        return $this;
    }

    public function addPopulation(?FHIRPopulation ...$value): self
    {
        $this->population = array_filter(array_merge($this->population, $value));

        return $this;
    }
}
