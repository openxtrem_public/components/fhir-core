<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EvidenceVariable Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIREvidenceVariableInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRRelatedArtifact;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIREvidenceVariableCharacteristic;

class FHIREvidenceVariable extends FHIRDomainResource implements FHIREvidenceVariableInterface
{
    public const RESOURCE_NAME = 'EvidenceVariable';

    protected ?FHIRUri $url = null;

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRString $version = null;
    protected ?FHIRString $name = null;
    protected ?FHIRString $title = null;
    protected ?FHIRString $shortTitle = null;
    protected ?FHIRString $subtitle = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRString $publisher = null;

    /** @var FHIRContactDetail[] */
    protected array $contact = [];
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRUsageContext[] */
    protected array $useContext = [];

    /** @var FHIRCodeableConcept[] */
    protected array $jurisdiction = [];
    protected ?FHIRMarkdown $copyright = null;
    protected ?FHIRDate $approvalDate = null;
    protected ?FHIRDate $lastReviewDate = null;
    protected ?FHIRPeriod $effectivePeriod = null;

    /** @var FHIRCodeableConcept[] */
    protected array $topic = [];

    /** @var FHIRContactDetail[] */
    protected array $author = [];

    /** @var FHIRContactDetail[] */
    protected array $editor = [];

    /** @var FHIRContactDetail[] */
    protected array $reviewer = [];

    /** @var FHIRContactDetail[] */
    protected array $endorser = [];

    /** @var FHIRRelatedArtifact[] */
    protected array $relatedArtifact = [];
    protected ?FHIRCode $type = null;

    /** @var FHIREvidenceVariableCharacteristic[] */
    protected array $characteristic = [];

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getShortTitle(): ?FHIRString
    {
        return $this->shortTitle;
    }

    public function setShortTitle(string|FHIRString|null $value): self
    {
        $this->shortTitle = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSubtitle(): ?FHIRString
    {
        return $this->subtitle;
    }

    public function setSubtitle(string|FHIRString|null $value): self
    {
        $this->subtitle = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPublisher(): ?FHIRString
    {
        return $this->publisher;
    }

    public function setPublisher(string|FHIRString|null $value): self
    {
        $this->publisher = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUseContext(): array
    {
        return $this->useContext;
    }

    public function setUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter($value);

        return $this;
    }

    public function addUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter(array_merge($this->useContext, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getJurisdiction(): array
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter($value);

        return $this;
    }

    public function addJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter(array_merge($this->jurisdiction, $value));

        return $this;
    }

    public function getCopyright(): ?FHIRMarkdown
    {
        return $this->copyright;
    }

    public function setCopyright(string|FHIRMarkdown|null $value): self
    {
        $this->copyright = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getApprovalDate(): ?FHIRDate
    {
        return $this->approvalDate;
    }

    public function setApprovalDate(string|FHIRDate|null $value): self
    {
        $this->approvalDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getLastReviewDate(): ?FHIRDate
    {
        return $this->lastReviewDate;
    }

    public function setLastReviewDate(string|FHIRDate|null $value): self
    {
        $this->lastReviewDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getEffectivePeriod(): ?FHIRPeriod
    {
        return $this->effectivePeriod;
    }

    public function setEffectivePeriod(?FHIRPeriod $value): self
    {
        $this->effectivePeriod = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getTopic(): array
    {
        return $this->topic;
    }

    public function setTopic(?FHIRCodeableConcept ...$value): self
    {
        $this->topic = array_filter($value);

        return $this;
    }

    public function addTopic(?FHIRCodeableConcept ...$value): self
    {
        $this->topic = array_filter(array_merge($this->topic, $value));

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getAuthor(): array
    {
        return $this->author;
    }

    public function setAuthor(?FHIRContactDetail ...$value): self
    {
        $this->author = array_filter($value);

        return $this;
    }

    public function addAuthor(?FHIRContactDetail ...$value): self
    {
        $this->author = array_filter(array_merge($this->author, $value));

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getEditor(): array
    {
        return $this->editor;
    }

    public function setEditor(?FHIRContactDetail ...$value): self
    {
        $this->editor = array_filter($value);

        return $this;
    }

    public function addEditor(?FHIRContactDetail ...$value): self
    {
        $this->editor = array_filter(array_merge($this->editor, $value));

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getReviewer(): array
    {
        return $this->reviewer;
    }

    public function setReviewer(?FHIRContactDetail ...$value): self
    {
        $this->reviewer = array_filter($value);

        return $this;
    }

    public function addReviewer(?FHIRContactDetail ...$value): self
    {
        $this->reviewer = array_filter(array_merge($this->reviewer, $value));

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getEndorser(): array
    {
        return $this->endorser;
    }

    public function setEndorser(?FHIRContactDetail ...$value): self
    {
        $this->endorser = array_filter($value);

        return $this;
    }

    public function addEndorser(?FHIRContactDetail ...$value): self
    {
        $this->endorser = array_filter(array_merge($this->endorser, $value));

        return $this;
    }

    /**
     * @return FHIRRelatedArtifact[]
     */
    public function getRelatedArtifact(): array
    {
        return $this->relatedArtifact;
    }

    public function setRelatedArtifact(?FHIRRelatedArtifact ...$value): self
    {
        $this->relatedArtifact = array_filter($value);

        return $this;
    }

    public function addRelatedArtifact(?FHIRRelatedArtifact ...$value): self
    {
        $this->relatedArtifact = array_filter(array_merge($this->relatedArtifact, $value));

        return $this;
    }

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIREvidenceVariableCharacteristic[]
     */
    public function getCharacteristic(): array
    {
        return $this->characteristic;
    }

    public function setCharacteristic(?FHIREvidenceVariableCharacteristic ...$value): self
    {
        $this->characteristic = array_filter($value);

        return $this;
    }

    public function addCharacteristic(?FHIREvidenceVariableCharacteristic ...$value): self
    {
        $this->characteristic = array_filter(array_merge($this->characteristic, $value));

        return $this;
    }
}
