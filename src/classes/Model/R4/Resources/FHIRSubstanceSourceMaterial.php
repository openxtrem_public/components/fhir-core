<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceSourceMaterial Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRSubstanceSourceMaterialInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRSubstanceSourceMaterialFractionDescription;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRSubstanceSourceMaterialOrganism;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRSubstanceSourceMaterialPartDescription;

class FHIRSubstanceSourceMaterial extends FHIRDomainResource implements FHIRSubstanceSourceMaterialInterface
{
    public const RESOURCE_NAME = 'SubstanceSourceMaterial';

    protected ?FHIRCodeableConcept $sourceMaterialClass = null;
    protected ?FHIRCodeableConcept $sourceMaterialType = null;
    protected ?FHIRCodeableConcept $sourceMaterialState = null;
    protected ?FHIRIdentifier $organismId = null;
    protected ?FHIRString $organismName = null;

    /** @var FHIRIdentifier[] */
    protected array $parentSubstanceId = [];

    /** @var FHIRString[] */
    protected array $parentSubstanceName = [];

    /** @var FHIRCodeableConcept[] */
    protected array $countryOfOrigin = [];

    /** @var FHIRString[] */
    protected array $geographicalLocation = [];
    protected ?FHIRCodeableConcept $developmentStage = null;

    /** @var FHIRSubstanceSourceMaterialFractionDescription[] */
    protected array $fractionDescription = [];
    protected ?FHIRSubstanceSourceMaterialOrganism $organism = null;

    /** @var FHIRSubstanceSourceMaterialPartDescription[] */
    protected array $partDescription = [];

    public function getSourceMaterialClass(): ?FHIRCodeableConcept
    {
        return $this->sourceMaterialClass;
    }

    public function setSourceMaterialClass(?FHIRCodeableConcept $value): self
    {
        $this->sourceMaterialClass = $value;

        return $this;
    }

    public function getSourceMaterialType(): ?FHIRCodeableConcept
    {
        return $this->sourceMaterialType;
    }

    public function setSourceMaterialType(?FHIRCodeableConcept $value): self
    {
        $this->sourceMaterialType = $value;

        return $this;
    }

    public function getSourceMaterialState(): ?FHIRCodeableConcept
    {
        return $this->sourceMaterialState;
    }

    public function setSourceMaterialState(?FHIRCodeableConcept $value): self
    {
        $this->sourceMaterialState = $value;

        return $this;
    }

    public function getOrganismId(): ?FHIRIdentifier
    {
        return $this->organismId;
    }

    public function setOrganismId(?FHIRIdentifier $value): self
    {
        $this->organismId = $value;

        return $this;
    }

    public function getOrganismName(): ?FHIRString
    {
        return $this->organismName;
    }

    public function setOrganismName(string|FHIRString|null $value): self
    {
        $this->organismName = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getParentSubstanceId(): array
    {
        return $this->parentSubstanceId;
    }

    public function setParentSubstanceId(?FHIRIdentifier ...$value): self
    {
        $this->parentSubstanceId = array_filter($value);

        return $this;
    }

    public function addParentSubstanceId(?FHIRIdentifier ...$value): self
    {
        $this->parentSubstanceId = array_filter(array_merge($this->parentSubstanceId, $value));

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getParentSubstanceName(): array
    {
        return $this->parentSubstanceName;
    }

    public function setParentSubstanceName(string|FHIRString|null ...$value): self
    {
        $this->parentSubstanceName = [];
        $this->addParentSubstanceName(...$value);

        return $this;
    }

    public function addParentSubstanceName(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->parentSubstanceName = array_filter(array_merge($this->parentSubstanceName, $values));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCountryOfOrigin(): array
    {
        return $this->countryOfOrigin;
    }

    public function setCountryOfOrigin(?FHIRCodeableConcept ...$value): self
    {
        $this->countryOfOrigin = array_filter($value);

        return $this;
    }

    public function addCountryOfOrigin(?FHIRCodeableConcept ...$value): self
    {
        $this->countryOfOrigin = array_filter(array_merge($this->countryOfOrigin, $value));

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getGeographicalLocation(): array
    {
        return $this->geographicalLocation;
    }

    public function setGeographicalLocation(string|FHIRString|null ...$value): self
    {
        $this->geographicalLocation = [];
        $this->addGeographicalLocation(...$value);

        return $this;
    }

    public function addGeographicalLocation(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->geographicalLocation = array_filter(array_merge($this->geographicalLocation, $values));

        return $this;
    }

    public function getDevelopmentStage(): ?FHIRCodeableConcept
    {
        return $this->developmentStage;
    }

    public function setDevelopmentStage(?FHIRCodeableConcept $value): self
    {
        $this->developmentStage = $value;

        return $this;
    }

    /**
     * @return FHIRSubstanceSourceMaterialFractionDescription[]
     */
    public function getFractionDescription(): array
    {
        return $this->fractionDescription;
    }

    public function setFractionDescription(?FHIRSubstanceSourceMaterialFractionDescription ...$value): self
    {
        $this->fractionDescription = array_filter($value);

        return $this;
    }

    public function addFractionDescription(?FHIRSubstanceSourceMaterialFractionDescription ...$value): self
    {
        $this->fractionDescription = array_filter(array_merge($this->fractionDescription, $value));

        return $this;
    }

    public function getOrganism(): ?FHIRSubstanceSourceMaterialOrganism
    {
        return $this->organism;
    }

    public function setOrganism(?FHIRSubstanceSourceMaterialOrganism $value): self
    {
        $this->organism = $value;

        return $this;
    }

    /**
     * @return FHIRSubstanceSourceMaterialPartDescription[]
     */
    public function getPartDescription(): array
    {
        return $this->partDescription;
    }

    public function setPartDescription(?FHIRSubstanceSourceMaterialPartDescription ...$value): self
    {
        $this->partDescription = array_filter($value);

        return $this;
    }

    public function addPartDescription(?FHIRSubstanceSourceMaterialPartDescription ...$value): self
    {
        $this->partDescription = array_filter(array_merge($this->partDescription, $value));

        return $this;
    }
}
