<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductContraindication Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMedicinalProductContraindicationInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPopulation;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicinalProductContraindicationOtherTherapy;

class FHIRMedicinalProductContraindication extends FHIRDomainResource implements FHIRMedicinalProductContraindicationInterface
{
    public const RESOURCE_NAME = 'MedicinalProductContraindication';

    /** @var FHIRReference[] */
    protected array $subject = [];
    protected ?FHIRCodeableConcept $disease = null;
    protected ?FHIRCodeableConcept $diseaseStatus = null;

    /** @var FHIRCodeableConcept[] */
    protected array $comorbidity = [];

    /** @var FHIRReference[] */
    protected array $therapeuticIndication = [];

    /** @var FHIRMedicinalProductContraindicationOtherTherapy[] */
    protected array $otherTherapy = [];

    /** @var FHIRPopulation[] */
    protected array $population = [];

    /**
     * @return FHIRReference[]
     */
    public function getSubject(): array
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference ...$value): self
    {
        $this->subject = array_filter($value);

        return $this;
    }

    public function addSubject(?FHIRReference ...$value): self
    {
        $this->subject = array_filter(array_merge($this->subject, $value));

        return $this;
    }

    public function getDisease(): ?FHIRCodeableConcept
    {
        return $this->disease;
    }

    public function setDisease(?FHIRCodeableConcept $value): self
    {
        $this->disease = $value;

        return $this;
    }

    public function getDiseaseStatus(): ?FHIRCodeableConcept
    {
        return $this->diseaseStatus;
    }

    public function setDiseaseStatus(?FHIRCodeableConcept $value): self
    {
        $this->diseaseStatus = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getComorbidity(): array
    {
        return $this->comorbidity;
    }

    public function setComorbidity(?FHIRCodeableConcept ...$value): self
    {
        $this->comorbidity = array_filter($value);

        return $this;
    }

    public function addComorbidity(?FHIRCodeableConcept ...$value): self
    {
        $this->comorbidity = array_filter(array_merge($this->comorbidity, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getTherapeuticIndication(): array
    {
        return $this->therapeuticIndication;
    }

    public function setTherapeuticIndication(?FHIRReference ...$value): self
    {
        $this->therapeuticIndication = array_filter($value);

        return $this;
    }

    public function addTherapeuticIndication(?FHIRReference ...$value): self
    {
        $this->therapeuticIndication = array_filter(array_merge($this->therapeuticIndication, $value));

        return $this;
    }

    /**
     * @return FHIRMedicinalProductContraindicationOtherTherapy[]
     */
    public function getOtherTherapy(): array
    {
        return $this->otherTherapy;
    }

    public function setOtherTherapy(?FHIRMedicinalProductContraindicationOtherTherapy ...$value): self
    {
        $this->otherTherapy = array_filter($value);

        return $this;
    }

    public function addOtherTherapy(?FHIRMedicinalProductContraindicationOtherTherapy ...$value): self
    {
        $this->otherTherapy = array_filter(array_merge($this->otherTherapy, $value));

        return $this;
    }

    /**
     * @return FHIRPopulation[]
     */
    public function getPopulation(): array
    {
        return $this->population;
    }

    public function setPopulation(?FHIRPopulation ...$value): self
    {
        $this->population = array_filter($value);

        return $this;
    }

    public function addPopulation(?FHIRPopulation ...$value): self
    {
        $this->population = array_filter(array_merge($this->population, $value));

        return $this;
    }
}
