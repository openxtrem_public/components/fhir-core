<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SpecimenDefinition Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRSpecimenDefinitionInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRSpecimenDefinitionTypeTested;

class FHIRSpecimenDefinition extends FHIRDomainResource implements FHIRSpecimenDefinitionInterface
{
    public const RESOURCE_NAME = 'SpecimenDefinition';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRCodeableConcept $typeCollected = null;

    /** @var FHIRCodeableConcept[] */
    protected array $patientPreparation = [];
    protected ?FHIRString $timeAspect = null;

    /** @var FHIRCodeableConcept[] */
    protected array $collection = [];

    /** @var FHIRSpecimenDefinitionTypeTested[] */
    protected array $typeTested = [];

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getTypeCollected(): ?FHIRCodeableConcept
    {
        return $this->typeCollected;
    }

    public function setTypeCollected(?FHIRCodeableConcept $value): self
    {
        $this->typeCollected = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getPatientPreparation(): array
    {
        return $this->patientPreparation;
    }

    public function setPatientPreparation(?FHIRCodeableConcept ...$value): self
    {
        $this->patientPreparation = array_filter($value);

        return $this;
    }

    public function addPatientPreparation(?FHIRCodeableConcept ...$value): self
    {
        $this->patientPreparation = array_filter(array_merge($this->patientPreparation, $value));

        return $this;
    }

    public function getTimeAspect(): ?FHIRString
    {
        return $this->timeAspect;
    }

    public function setTimeAspect(string|FHIRString|null $value): self
    {
        $this->timeAspect = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCollection(): array
    {
        return $this->collection;
    }

    public function setCollection(?FHIRCodeableConcept ...$value): self
    {
        $this->collection = array_filter($value);

        return $this;
    }

    public function addCollection(?FHIRCodeableConcept ...$value): self
    {
        $this->collection = array_filter(array_merge($this->collection, $value));

        return $this;
    }

    /**
     * @return FHIRSpecimenDefinitionTypeTested[]
     */
    public function getTypeTested(): array
    {
        return $this->typeTested;
    }

    public function setTypeTested(?FHIRSpecimenDefinitionTypeTested ...$value): self
    {
        $this->typeTested = array_filter($value);

        return $this;
    }

    public function addTypeTested(?FHIRSpecimenDefinitionTypeTested ...$value): self
    {
        $this->typeTested = array_filter(array_merge($this->typeTested, $value));

        return $this;
    }
}
