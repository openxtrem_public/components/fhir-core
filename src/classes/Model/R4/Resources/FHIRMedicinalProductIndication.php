<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductIndication Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMedicinalProductIndicationInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPopulation;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicinalProductIndicationOtherTherapy;

class FHIRMedicinalProductIndication extends FHIRDomainResource implements FHIRMedicinalProductIndicationInterface
{
    public const RESOURCE_NAME = 'MedicinalProductIndication';

    /** @var FHIRReference[] */
    protected array $subject = [];
    protected ?FHIRCodeableConcept $diseaseSymptomProcedure = null;
    protected ?FHIRCodeableConcept $diseaseStatus = null;

    /** @var FHIRCodeableConcept[] */
    protected array $comorbidity = [];
    protected ?FHIRCodeableConcept $intendedEffect = null;
    protected ?FHIRQuantity $duration = null;

    /** @var FHIRMedicinalProductIndicationOtherTherapy[] */
    protected array $otherTherapy = [];

    /** @var FHIRReference[] */
    protected array $undesirableEffect = [];

    /** @var FHIRPopulation[] */
    protected array $population = [];

    /**
     * @return FHIRReference[]
     */
    public function getSubject(): array
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference ...$value): self
    {
        $this->subject = array_filter($value);

        return $this;
    }

    public function addSubject(?FHIRReference ...$value): self
    {
        $this->subject = array_filter(array_merge($this->subject, $value));

        return $this;
    }

    public function getDiseaseSymptomProcedure(): ?FHIRCodeableConcept
    {
        return $this->diseaseSymptomProcedure;
    }

    public function setDiseaseSymptomProcedure(?FHIRCodeableConcept $value): self
    {
        $this->diseaseSymptomProcedure = $value;

        return $this;
    }

    public function getDiseaseStatus(): ?FHIRCodeableConcept
    {
        return $this->diseaseStatus;
    }

    public function setDiseaseStatus(?FHIRCodeableConcept $value): self
    {
        $this->diseaseStatus = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getComorbidity(): array
    {
        return $this->comorbidity;
    }

    public function setComorbidity(?FHIRCodeableConcept ...$value): self
    {
        $this->comorbidity = array_filter($value);

        return $this;
    }

    public function addComorbidity(?FHIRCodeableConcept ...$value): self
    {
        $this->comorbidity = array_filter(array_merge($this->comorbidity, $value));

        return $this;
    }

    public function getIntendedEffect(): ?FHIRCodeableConcept
    {
        return $this->intendedEffect;
    }

    public function setIntendedEffect(?FHIRCodeableConcept $value): self
    {
        $this->intendedEffect = $value;

        return $this;
    }

    public function getDuration(): ?FHIRQuantity
    {
        return $this->duration;
    }

    public function setDuration(?FHIRQuantity $value): self
    {
        $this->duration = $value;

        return $this;
    }

    /**
     * @return FHIRMedicinalProductIndicationOtherTherapy[]
     */
    public function getOtherTherapy(): array
    {
        return $this->otherTherapy;
    }

    public function setOtherTherapy(?FHIRMedicinalProductIndicationOtherTherapy ...$value): self
    {
        $this->otherTherapy = array_filter($value);

        return $this;
    }

    public function addOtherTherapy(?FHIRMedicinalProductIndicationOtherTherapy ...$value): self
    {
        $this->otherTherapy = array_filter(array_merge($this->otherTherapy, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getUndesirableEffect(): array
    {
        return $this->undesirableEffect;
    }

    public function setUndesirableEffect(?FHIRReference ...$value): self
    {
        $this->undesirableEffect = array_filter($value);

        return $this;
    }

    public function addUndesirableEffect(?FHIRReference ...$value): self
    {
        $this->undesirableEffect = array_filter(array_merge($this->undesirableEffect, $value));

        return $this;
    }

    /**
     * @return FHIRPopulation[]
     */
    public function getPopulation(): array
    {
        return $this->population;
    }

    public function setPopulation(?FHIRPopulation ...$value): self
    {
        $this->population = array_filter($value);

        return $this;
    }

    public function addPopulation(?FHIRPopulation ...$value): self
    {
        $this->population = array_filter(array_merge($this->population, $value));

        return $this;
    }
}
