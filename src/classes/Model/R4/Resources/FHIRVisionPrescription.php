<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR VisionPrescription Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRVisionPrescriptionInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRVisionPrescriptionLensSpecification;

class FHIRVisionPrescription extends FHIRDomainResource implements FHIRVisionPrescriptionInterface
{
    public const RESOURCE_NAME = 'VisionPrescription';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRDateTime $created = null;
    protected ?FHIRReference $patient = null;
    protected ?FHIRReference $encounter = null;
    protected ?FHIRDateTime $dateWritten = null;
    protected ?FHIRReference $prescriber = null;

    /** @var FHIRVisionPrescriptionLensSpecification[] */
    protected array $lensSpecification = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCreated(): ?FHIRDateTime
    {
        return $this->created;
    }

    public function setCreated(string|FHIRDateTime|null $value): self
    {
        $this->created = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getDateWritten(): ?FHIRDateTime
    {
        return $this->dateWritten;
    }

    public function setDateWritten(string|FHIRDateTime|null $value): self
    {
        $this->dateWritten = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPrescriber(): ?FHIRReference
    {
        return $this->prescriber;
    }

    public function setPrescriber(?FHIRReference $value): self
    {
        $this->prescriber = $value;

        return $this;
    }

    /**
     * @return FHIRVisionPrescriptionLensSpecification[]
     */
    public function getLensSpecification(): array
    {
        return $this->lensSpecification;
    }

    public function setLensSpecification(?FHIRVisionPrescriptionLensSpecification ...$value): self
    {
        $this->lensSpecification = array_filter($value);

        return $this;
    }

    public function addLensSpecification(?FHIRVisionPrescriptionLensSpecification ...$value): self
    {
        $this->lensSpecification = array_filter(array_merge($this->lensSpecification, $value));

        return $this;
    }
}
