<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubstanceSpecification Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRSubstanceSpecificationInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRSubstanceSpecificationCode;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRSubstanceSpecificationMoiety;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRSubstanceSpecificationName;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRSubstanceSpecificationProperty;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRSubstanceSpecificationRelationship;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRSubstanceSpecificationStructure;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRSubstanceSpecificationStructureIsotopeMolecularWeight;

class FHIRSubstanceSpecification extends FHIRDomainResource implements FHIRSubstanceSpecificationInterface
{
    public const RESOURCE_NAME = 'SubstanceSpecification';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableConcept $status = null;
    protected ?FHIRCodeableConcept $domain = null;
    protected ?FHIRString $description = null;

    /** @var FHIRReference[] */
    protected array $source = [];
    protected ?FHIRString $comment = null;

    /** @var FHIRSubstanceSpecificationMoiety[] */
    protected array $moiety = [];

    /** @var FHIRSubstanceSpecificationProperty[] */
    protected array $property = [];
    protected ?FHIRReference $referenceInformation = null;
    protected ?FHIRSubstanceSpecificationStructure $structure = null;

    /** @var FHIRSubstanceSpecificationCode[] */
    protected array $code = [];

    /** @var FHIRSubstanceSpecificationName[] */
    protected array $name = [];

    /** @var FHIRSubstanceSpecificationStructureIsotopeMolecularWeight[] */
    protected array $molecularWeight = [];

    /** @var FHIRSubstanceSpecificationRelationship[] */
    protected array $relationship = [];
    protected ?FHIRReference $nucleicAcid = null;
    protected ?FHIRReference $polymer = null;
    protected ?FHIRReference $protein = null;
    protected ?FHIRReference $sourceMaterial = null;

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCodeableConcept
    {
        return $this->status;
    }

    public function setStatus(?FHIRCodeableConcept $value): self
    {
        $this->status = $value;

        return $this;
    }

    public function getDomain(): ?FHIRCodeableConcept
    {
        return $this->domain;
    }

    public function setDomain(?FHIRCodeableConcept $value): self
    {
        $this->domain = $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSource(): array
    {
        return $this->source;
    }

    public function setSource(?FHIRReference ...$value): self
    {
        $this->source = array_filter($value);

        return $this;
    }

    public function addSource(?FHIRReference ...$value): self
    {
        $this->source = array_filter(array_merge($this->source, $value));

        return $this;
    }

    public function getComment(): ?FHIRString
    {
        return $this->comment;
    }

    public function setComment(string|FHIRString|null $value): self
    {
        $this->comment = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRSubstanceSpecificationMoiety[]
     */
    public function getMoiety(): array
    {
        return $this->moiety;
    }

    public function setMoiety(?FHIRSubstanceSpecificationMoiety ...$value): self
    {
        $this->moiety = array_filter($value);

        return $this;
    }

    public function addMoiety(?FHIRSubstanceSpecificationMoiety ...$value): self
    {
        $this->moiety = array_filter(array_merge($this->moiety, $value));

        return $this;
    }

    /**
     * @return FHIRSubstanceSpecificationProperty[]
     */
    public function getProperty(): array
    {
        return $this->property;
    }

    public function setProperty(?FHIRSubstanceSpecificationProperty ...$value): self
    {
        $this->property = array_filter($value);

        return $this;
    }

    public function addProperty(?FHIRSubstanceSpecificationProperty ...$value): self
    {
        $this->property = array_filter(array_merge($this->property, $value));

        return $this;
    }

    public function getReferenceInformation(): ?FHIRReference
    {
        return $this->referenceInformation;
    }

    public function setReferenceInformation(?FHIRReference $value): self
    {
        $this->referenceInformation = $value;

        return $this;
    }

    public function getStructure(): ?FHIRSubstanceSpecificationStructure
    {
        return $this->structure;
    }

    public function setStructure(?FHIRSubstanceSpecificationStructure $value): self
    {
        $this->structure = $value;

        return $this;
    }

    /**
     * @return FHIRSubstanceSpecificationCode[]
     */
    public function getCode(): array
    {
        return $this->code;
    }

    public function setCode(?FHIRSubstanceSpecificationCode ...$value): self
    {
        $this->code = array_filter($value);

        return $this;
    }

    public function addCode(?FHIRSubstanceSpecificationCode ...$value): self
    {
        $this->code = array_filter(array_merge($this->code, $value));

        return $this;
    }

    /**
     * @return FHIRSubstanceSpecificationName[]
     */
    public function getName(): array
    {
        return $this->name;
    }

    public function setName(?FHIRSubstanceSpecificationName ...$value): self
    {
        $this->name = array_filter($value);

        return $this;
    }

    public function addName(?FHIRSubstanceSpecificationName ...$value): self
    {
        $this->name = array_filter(array_merge($this->name, $value));

        return $this;
    }

    /**
     * @return FHIRSubstanceSpecificationStructureIsotopeMolecularWeight[]
     */
    public function getMolecularWeight(): array
    {
        return $this->molecularWeight;
    }

    public function setMolecularWeight(?FHIRSubstanceSpecificationStructureIsotopeMolecularWeight ...$value): self
    {
        $this->molecularWeight = array_filter($value);

        return $this;
    }

    public function addMolecularWeight(?FHIRSubstanceSpecificationStructureIsotopeMolecularWeight ...$value): self
    {
        $this->molecularWeight = array_filter(array_merge($this->molecularWeight, $value));

        return $this;
    }

    /**
     * @return FHIRSubstanceSpecificationRelationship[]
     */
    public function getRelationship(): array
    {
        return $this->relationship;
    }

    public function setRelationship(?FHIRSubstanceSpecificationRelationship ...$value): self
    {
        $this->relationship = array_filter($value);

        return $this;
    }

    public function addRelationship(?FHIRSubstanceSpecificationRelationship ...$value): self
    {
        $this->relationship = array_filter(array_merge($this->relationship, $value));

        return $this;
    }

    public function getNucleicAcid(): ?FHIRReference
    {
        return $this->nucleicAcid;
    }

    public function setNucleicAcid(?FHIRReference $value): self
    {
        $this->nucleicAcid = $value;

        return $this;
    }

    public function getPolymer(): ?FHIRReference
    {
        return $this->polymer;
    }

    public function setPolymer(?FHIRReference $value): self
    {
        $this->polymer = $value;

        return $this;
    }

    public function getProtein(): ?FHIRReference
    {
        return $this->protein;
    }

    public function setProtein(?FHIRReference $value): self
    {
        $this->protein = $value;

        return $this;
    }

    public function getSourceMaterial(): ?FHIRReference
    {
        return $this->sourceMaterial;
    }

    public function setSourceMaterial(?FHIRReference $value): self
    {
        $this->sourceMaterial = $value;

        return $this;
    }
}
