<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImagingStudy Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRImagingStudyInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUnsignedInt;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRImagingStudySeries;

class FHIRImagingStudy extends FHIRDomainResource implements FHIRImagingStudyInterface
{
    public const RESOURCE_NAME = 'ImagingStudy';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;

    /** @var FHIRCoding[] */
    protected array $modality = [];
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $encounter = null;
    protected ?FHIRDateTime $started = null;

    /** @var FHIRReference[] */
    protected array $basedOn = [];
    protected ?FHIRReference $referrer = null;

    /** @var FHIRReference[] */
    protected array $interpreter = [];

    /** @var FHIRReference[] */
    protected array $endpoint = [];
    protected ?FHIRUnsignedInt $numberOfSeries = null;
    protected ?FHIRUnsignedInt $numberOfInstances = null;
    protected ?FHIRReference $procedureReference = null;

    /** @var FHIRCodeableConcept[] */
    protected array $procedureCode = [];
    protected ?FHIRReference $location = null;

    /** @var FHIRCodeableConcept[] */
    protected array $reasonCode = [];

    /** @var FHIRReference[] */
    protected array $reasonReference = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];
    protected ?FHIRString $description = null;

    /** @var FHIRImagingStudySeries[] */
    protected array $series = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getModality(): array
    {
        return $this->modality;
    }

    public function setModality(?FHIRCoding ...$value): self
    {
        $this->modality = array_filter($value);

        return $this;
    }

    public function addModality(?FHIRCoding ...$value): self
    {
        $this->modality = array_filter(array_merge($this->modality, $value));

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getStarted(): ?FHIRDateTime
    {
        return $this->started;
    }

    public function setStarted(string|FHIRDateTime|null $value): self
    {
        $this->started = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    public function getReferrer(): ?FHIRReference
    {
        return $this->referrer;
    }

    public function setReferrer(?FHIRReference $value): self
    {
        $this->referrer = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getInterpreter(): array
    {
        return $this->interpreter;
    }

    public function setInterpreter(?FHIRReference ...$value): self
    {
        $this->interpreter = array_filter($value);

        return $this;
    }

    public function addInterpreter(?FHIRReference ...$value): self
    {
        $this->interpreter = array_filter(array_merge($this->interpreter, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEndpoint(): array
    {
        return $this->endpoint;
    }

    public function setEndpoint(?FHIRReference ...$value): self
    {
        $this->endpoint = array_filter($value);

        return $this;
    }

    public function addEndpoint(?FHIRReference ...$value): self
    {
        $this->endpoint = array_filter(array_merge($this->endpoint, $value));

        return $this;
    }

    public function getNumberOfSeries(): ?FHIRUnsignedInt
    {
        return $this->numberOfSeries;
    }

    public function setNumberOfSeries(int|FHIRUnsignedInt|null $value): self
    {
        $this->numberOfSeries = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getNumberOfInstances(): ?FHIRUnsignedInt
    {
        return $this->numberOfInstances;
    }

    public function setNumberOfInstances(int|FHIRUnsignedInt|null $value): self
    {
        $this->numberOfInstances = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getProcedureReference(): ?FHIRReference
    {
        return $this->procedureReference;
    }

    public function setProcedureReference(?FHIRReference $value): self
    {
        $this->procedureReference = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getProcedureCode(): array
    {
        return $this->procedureCode;
    }

    public function setProcedureCode(?FHIRCodeableConcept ...$value): self
    {
        $this->procedureCode = array_filter($value);

        return $this;
    }

    public function addProcedureCode(?FHIRCodeableConcept ...$value): self
    {
        $this->procedureCode = array_filter(array_merge($this->procedureCode, $value));

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReasonCode(): array
    {
        return $this->reasonCode;
    }

    public function setReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter($value);

        return $this;
    }

    public function addReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter(array_merge($this->reasonCode, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReasonReference(): array
    {
        return $this->reasonReference;
    }

    public function setReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter($value);

        return $this;
    }

    public function addReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter(array_merge($this->reasonReference, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRImagingStudySeries[]
     */
    public function getSeries(): array
    {
        return $this->series;
    }

    public function setSeries(?FHIRImagingStudySeries ...$value): self
    {
        $this->series = array_filter($value);

        return $this;
    }

    public function addSeries(?FHIRImagingStudySeries ...$value): self
    {
        $this->series = array_filter(array_merge($this->series, $value));

        return $this;
    }
}
