<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CareTeam Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRCareTeamInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRCareTeamParticipant;

class FHIRCareTeam extends FHIRDomainResource implements FHIRCareTeamInterface
{
    public const RESOURCE_NAME = 'CareTeam';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRString $name = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $encounter = null;
    protected ?FHIRPeriod $period = null;

    /** @var FHIRCareTeamParticipant[] */
    protected array $participant = [];

    /** @var FHIRCodeableConcept[] */
    protected array $reasonCode = [];

    /** @var FHIRReference[] */
    protected array $reasonReference = [];

    /** @var FHIRReference[] */
    protected array $managingOrganization = [];

    /** @var FHIRContactPoint[] */
    protected array $telecom = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    /**
     * @return FHIRCareTeamParticipant[]
     */
    public function getParticipant(): array
    {
        return $this->participant;
    }

    public function setParticipant(?FHIRCareTeamParticipant ...$value): self
    {
        $this->participant = array_filter($value);

        return $this;
    }

    public function addParticipant(?FHIRCareTeamParticipant ...$value): self
    {
        $this->participant = array_filter(array_merge($this->participant, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReasonCode(): array
    {
        return $this->reasonCode;
    }

    public function setReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter($value);

        return $this;
    }

    public function addReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter(array_merge($this->reasonCode, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReasonReference(): array
    {
        return $this->reasonReference;
    }

    public function setReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter($value);

        return $this;
    }

    public function addReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter(array_merge($this->reasonReference, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getManagingOrganization(): array
    {
        return $this->managingOrganization;
    }

    public function setManagingOrganization(?FHIRReference ...$value): self
    {
        $this->managingOrganization = array_filter($value);

        return $this;
    }

    public function addManagingOrganization(?FHIRReference ...$value): self
    {
        $this->managingOrganization = array_filter(array_merge($this->managingOrganization, $value));

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getTelecom(): array
    {
        return $this->telecom;
    }

    public function setTelecom(?FHIRContactPoint ...$value): self
    {
        $this->telecom = array_filter($value);

        return $this;
    }

    public function addTelecom(?FHIRContactPoint ...$value): self
    {
        $this->telecom = array_filter(array_merge($this->telecom, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }
}
