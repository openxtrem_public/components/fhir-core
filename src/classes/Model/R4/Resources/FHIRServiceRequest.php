<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ServiceRequest Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRServiceRequestInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRRatio;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUri;

class FHIRServiceRequest extends FHIRDomainResource implements FHIRServiceRequestInterface
{
    public const RESOURCE_NAME = 'ServiceRequest';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRCanonical[] */
    protected array $instantiatesCanonical = [];

    /** @var FHIRUri[] */
    protected array $instantiatesUri = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];

    /** @var FHIRReference[] */
    protected array $replaces = [];
    protected ?FHIRIdentifier $requisition = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRCode $intent = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRCode $priority = null;
    protected ?FHIRBoolean $doNotPerform = null;
    protected ?FHIRCodeableConcept $code = null;

    /** @var FHIRCodeableConcept[] */
    protected array $orderDetail = [];
    protected FHIRQuantity|FHIRRatio|FHIRRange|null $quantity = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $encounter = null;
    protected FHIRDateTime|FHIRPeriod|FHIRTiming|null $occurrence = null;
    protected FHIRBoolean|FHIRCodeableConcept|null $asNeeded = null;
    protected ?FHIRDateTime $authoredOn = null;
    protected ?FHIRReference $requester = null;
    protected ?FHIRCodeableConcept $performerType = null;

    /** @var FHIRReference[] */
    protected array $performer = [];

    /** @var FHIRCodeableConcept[] */
    protected array $locationCode = [];

    /** @var FHIRReference[] */
    protected array $locationReference = [];

    /** @var FHIRCodeableConcept[] */
    protected array $reasonCode = [];

    /** @var FHIRReference[] */
    protected array $reasonReference = [];

    /** @var FHIRReference[] */
    protected array $insurance = [];

    /** @var FHIRReference[] */
    protected array $supportingInfo = [];

    /** @var FHIRReference[] */
    protected array $specimen = [];

    /** @var FHIRCodeableConcept[] */
    protected array $bodySite = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];
    protected ?FHIRString $patientInstruction = null;

    /** @var FHIRReference[] */
    protected array $relevantHistory = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getInstantiatesCanonical(): array
    {
        return $this->instantiatesCanonical;
    }

    public function setInstantiatesCanonical(string|FHIRCanonical|null ...$value): self
    {
        $this->instantiatesCanonical = [];
        $this->addInstantiatesCanonical(...$value);

        return $this;
    }

    public function addInstantiatesCanonical(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->instantiatesCanonical = array_filter(array_merge($this->instantiatesCanonical, $values));

        return $this;
    }

    /**
     * @return FHIRUri[]
     */
    public function getInstantiatesUri(): array
    {
        return $this->instantiatesUri;
    }

    public function setInstantiatesUri(string|FHIRUri|null ...$value): self
    {
        $this->instantiatesUri = [];
        $this->addInstantiatesUri(...$value);

        return $this;
    }

    public function addInstantiatesUri(string|FHIRUri|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUri())->setValue($v) : $v, $value);

        $this->instantiatesUri = array_filter(array_merge($this->instantiatesUri, $values));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReplaces(): array
    {
        return $this->replaces;
    }

    public function setReplaces(?FHIRReference ...$value): self
    {
        $this->replaces = array_filter($value);

        return $this;
    }

    public function addReplaces(?FHIRReference ...$value): self
    {
        $this->replaces = array_filter(array_merge($this->replaces, $value));

        return $this;
    }

    public function getRequisition(): ?FHIRIdentifier
    {
        return $this->requisition;
    }

    public function setRequisition(?FHIRIdentifier $value): self
    {
        $this->requisition = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getIntent(): ?FHIRCode
    {
        return $this->intent;
    }

    public function setIntent(string|FHIRCode|null $value): self
    {
        $this->intent = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getPriority(): ?FHIRCode
    {
        return $this->priority;
    }

    public function setPriority(string|FHIRCode|null $value): self
    {
        $this->priority = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDoNotPerform(): ?FHIRBoolean
    {
        return $this->doNotPerform;
    }

    public function setDoNotPerform(bool|FHIRBoolean|null $value): self
    {
        $this->doNotPerform = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getOrderDetail(): array
    {
        return $this->orderDetail;
    }

    public function setOrderDetail(?FHIRCodeableConcept ...$value): self
    {
        $this->orderDetail = array_filter($value);

        return $this;
    }

    public function addOrderDetail(?FHIRCodeableConcept ...$value): self
    {
        $this->orderDetail = array_filter(array_merge($this->orderDetail, $value));

        return $this;
    }

    public function getQuantity(): FHIRQuantity|FHIRRatio|FHIRRange|null
    {
        return $this->quantity;
    }

    public function setQuantity(FHIRQuantity|FHIRRatio|FHIRRange|null $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getOccurrence(): FHIRDateTime|FHIRPeriod|FHIRTiming|null
    {
        return $this->occurrence;
    }

    public function setOccurrence(FHIRDateTime|FHIRPeriod|FHIRTiming|null $value): self
    {
        $this->occurrence = $value;

        return $this;
    }

    public function getAsNeeded(): FHIRBoolean|FHIRCodeableConcept|null
    {
        return $this->asNeeded;
    }

    public function setAsNeeded(FHIRBoolean|FHIRCodeableConcept|null $value): self
    {
        $this->asNeeded = $value;

        return $this;
    }

    public function getAuthoredOn(): ?FHIRDateTime
    {
        return $this->authoredOn;
    }

    public function setAuthoredOn(string|FHIRDateTime|null $value): self
    {
        $this->authoredOn = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getRequester(): ?FHIRReference
    {
        return $this->requester;
    }

    public function setRequester(?FHIRReference $value): self
    {
        $this->requester = $value;

        return $this;
    }

    public function getPerformerType(): ?FHIRCodeableConcept
    {
        return $this->performerType;
    }

    public function setPerformerType(?FHIRCodeableConcept $value): self
    {
        $this->performerType = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPerformer(): array
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRReference ...$value): self
    {
        $this->performer = array_filter($value);

        return $this;
    }

    public function addPerformer(?FHIRReference ...$value): self
    {
        $this->performer = array_filter(array_merge($this->performer, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getLocationCode(): array
    {
        return $this->locationCode;
    }

    public function setLocationCode(?FHIRCodeableConcept ...$value): self
    {
        $this->locationCode = array_filter($value);

        return $this;
    }

    public function addLocationCode(?FHIRCodeableConcept ...$value): self
    {
        $this->locationCode = array_filter(array_merge($this->locationCode, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getLocationReference(): array
    {
        return $this->locationReference;
    }

    public function setLocationReference(?FHIRReference ...$value): self
    {
        $this->locationReference = array_filter($value);

        return $this;
    }

    public function addLocationReference(?FHIRReference ...$value): self
    {
        $this->locationReference = array_filter(array_merge($this->locationReference, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReasonCode(): array
    {
        return $this->reasonCode;
    }

    public function setReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter($value);

        return $this;
    }

    public function addReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter(array_merge($this->reasonCode, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReasonReference(): array
    {
        return $this->reasonReference;
    }

    public function setReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter($value);

        return $this;
    }

    public function addReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter(array_merge($this->reasonReference, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getInsurance(): array
    {
        return $this->insurance;
    }

    public function setInsurance(?FHIRReference ...$value): self
    {
        $this->insurance = array_filter($value);

        return $this;
    }

    public function addInsurance(?FHIRReference ...$value): self
    {
        $this->insurance = array_filter(array_merge($this->insurance, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupportingInfo(): array
    {
        return $this->supportingInfo;
    }

    public function setSupportingInfo(?FHIRReference ...$value): self
    {
        $this->supportingInfo = array_filter($value);

        return $this;
    }

    public function addSupportingInfo(?FHIRReference ...$value): self
    {
        $this->supportingInfo = array_filter(array_merge($this->supportingInfo, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSpecimen(): array
    {
        return $this->specimen;
    }

    public function setSpecimen(?FHIRReference ...$value): self
    {
        $this->specimen = array_filter($value);

        return $this;
    }

    public function addSpecimen(?FHIRReference ...$value): self
    {
        $this->specimen = array_filter(array_merge($this->specimen, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getBodySite(): array
    {
        return $this->bodySite;
    }

    public function setBodySite(?FHIRCodeableConcept ...$value): self
    {
        $this->bodySite = array_filter($value);

        return $this;
    }

    public function addBodySite(?FHIRCodeableConcept ...$value): self
    {
        $this->bodySite = array_filter(array_merge($this->bodySite, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    public function getPatientInstruction(): ?FHIRString
    {
        return $this->patientInstruction;
    }

    public function setPatientInstruction(string|FHIRString|null $value): self
    {
        $this->patientInstruction = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getRelevantHistory(): array
    {
        return $this->relevantHistory;
    }

    public function setRelevantHistory(?FHIRReference ...$value): self
    {
        $this->relevantHistory = array_filter($value);

        return $this;
    }

    public function addRelevantHistory(?FHIRReference ...$value): self
    {
        $this->relevantHistory = array_filter(array_merge($this->relevantHistory, $value));

        return $this;
    }
}
