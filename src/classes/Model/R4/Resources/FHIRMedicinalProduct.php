<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProduct Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMedicinalProductInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRMarketingStatus;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicinalProductManufacturingBusinessOperation;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicinalProductName;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicinalProductSpecialDesignation;

class FHIRMedicinalProduct extends FHIRDomainResource implements FHIRMedicinalProductInterface
{
    public const RESOURCE_NAME = 'MedicinalProduct';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCoding $domain = null;
    protected ?FHIRCodeableConcept $combinedPharmaceuticalDoseForm = null;
    protected ?FHIRCodeableConcept $legalStatusOfSupply = null;
    protected ?FHIRCodeableConcept $additionalMonitoringIndicator = null;

    /** @var FHIRString[] */
    protected array $specialMeasures = [];
    protected ?FHIRCodeableConcept $paediatricUseIndicator = null;

    /** @var FHIRCodeableConcept[] */
    protected array $productClassification = [];

    /** @var FHIRMarketingStatus[] */
    protected array $marketingStatus = [];

    /** @var FHIRReference[] */
    protected array $pharmaceuticalProduct = [];

    /** @var FHIRReference[] */
    protected array $packagedMedicinalProduct = [];

    /** @var FHIRReference[] */
    protected array $attachedDocument = [];

    /** @var FHIRReference[] */
    protected array $masterFile = [];

    /** @var FHIRReference[] */
    protected array $contact = [];

    /** @var FHIRReference[] */
    protected array $clinicalTrial = [];

    /** @var FHIRMedicinalProductName[] */
    protected array $name = [];

    /** @var FHIRIdentifier[] */
    protected array $crossReference = [];

    /** @var FHIRMedicinalProductManufacturingBusinessOperation[] */
    protected array $manufacturingBusinessOperation = [];

    /** @var FHIRMedicinalProductSpecialDesignation[] */
    protected array $specialDesignation = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getDomain(): ?FHIRCoding
    {
        return $this->domain;
    }

    public function setDomain(?FHIRCoding $value): self
    {
        $this->domain = $value;

        return $this;
    }

    public function getCombinedPharmaceuticalDoseForm(): ?FHIRCodeableConcept
    {
        return $this->combinedPharmaceuticalDoseForm;
    }

    public function setCombinedPharmaceuticalDoseForm(?FHIRCodeableConcept $value): self
    {
        $this->combinedPharmaceuticalDoseForm = $value;

        return $this;
    }

    public function getLegalStatusOfSupply(): ?FHIRCodeableConcept
    {
        return $this->legalStatusOfSupply;
    }

    public function setLegalStatusOfSupply(?FHIRCodeableConcept $value): self
    {
        $this->legalStatusOfSupply = $value;

        return $this;
    }

    public function getAdditionalMonitoringIndicator(): ?FHIRCodeableConcept
    {
        return $this->additionalMonitoringIndicator;
    }

    public function setAdditionalMonitoringIndicator(?FHIRCodeableConcept $value): self
    {
        $this->additionalMonitoringIndicator = $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getSpecialMeasures(): array
    {
        return $this->specialMeasures;
    }

    public function setSpecialMeasures(string|FHIRString|null ...$value): self
    {
        $this->specialMeasures = [];
        $this->addSpecialMeasures(...$value);

        return $this;
    }

    public function addSpecialMeasures(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->specialMeasures = array_filter(array_merge($this->specialMeasures, $values));

        return $this;
    }

    public function getPaediatricUseIndicator(): ?FHIRCodeableConcept
    {
        return $this->paediatricUseIndicator;
    }

    public function setPaediatricUseIndicator(?FHIRCodeableConcept $value): self
    {
        $this->paediatricUseIndicator = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getProductClassification(): array
    {
        return $this->productClassification;
    }

    public function setProductClassification(?FHIRCodeableConcept ...$value): self
    {
        $this->productClassification = array_filter($value);

        return $this;
    }

    public function addProductClassification(?FHIRCodeableConcept ...$value): self
    {
        $this->productClassification = array_filter(array_merge($this->productClassification, $value));

        return $this;
    }

    /**
     * @return FHIRMarketingStatus[]
     */
    public function getMarketingStatus(): array
    {
        return $this->marketingStatus;
    }

    public function setMarketingStatus(?FHIRMarketingStatus ...$value): self
    {
        $this->marketingStatus = array_filter($value);

        return $this;
    }

    public function addMarketingStatus(?FHIRMarketingStatus ...$value): self
    {
        $this->marketingStatus = array_filter(array_merge($this->marketingStatus, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPharmaceuticalProduct(): array
    {
        return $this->pharmaceuticalProduct;
    }

    public function setPharmaceuticalProduct(?FHIRReference ...$value): self
    {
        $this->pharmaceuticalProduct = array_filter($value);

        return $this;
    }

    public function addPharmaceuticalProduct(?FHIRReference ...$value): self
    {
        $this->pharmaceuticalProduct = array_filter(array_merge($this->pharmaceuticalProduct, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPackagedMedicinalProduct(): array
    {
        return $this->packagedMedicinalProduct;
    }

    public function setPackagedMedicinalProduct(?FHIRReference ...$value): self
    {
        $this->packagedMedicinalProduct = array_filter($value);

        return $this;
    }

    public function addPackagedMedicinalProduct(?FHIRReference ...$value): self
    {
        $this->packagedMedicinalProduct = array_filter(array_merge($this->packagedMedicinalProduct, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAttachedDocument(): array
    {
        return $this->attachedDocument;
    }

    public function setAttachedDocument(?FHIRReference ...$value): self
    {
        $this->attachedDocument = array_filter($value);

        return $this;
    }

    public function addAttachedDocument(?FHIRReference ...$value): self
    {
        $this->attachedDocument = array_filter(array_merge($this->attachedDocument, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getMasterFile(): array
    {
        return $this->masterFile;
    }

    public function setMasterFile(?FHIRReference ...$value): self
    {
        $this->masterFile = array_filter($value);

        return $this;
    }

    public function addMasterFile(?FHIRReference ...$value): self
    {
        $this->masterFile = array_filter(array_merge($this->masterFile, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRReference ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRReference ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getClinicalTrial(): array
    {
        return $this->clinicalTrial;
    }

    public function setClinicalTrial(?FHIRReference ...$value): self
    {
        $this->clinicalTrial = array_filter($value);

        return $this;
    }

    public function addClinicalTrial(?FHIRReference ...$value): self
    {
        $this->clinicalTrial = array_filter(array_merge($this->clinicalTrial, $value));

        return $this;
    }

    /**
     * @return FHIRMedicinalProductName[]
     */
    public function getName(): array
    {
        return $this->name;
    }

    public function setName(?FHIRMedicinalProductName ...$value): self
    {
        $this->name = array_filter($value);

        return $this;
    }

    public function addName(?FHIRMedicinalProductName ...$value): self
    {
        $this->name = array_filter(array_merge($this->name, $value));

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getCrossReference(): array
    {
        return $this->crossReference;
    }

    public function setCrossReference(?FHIRIdentifier ...$value): self
    {
        $this->crossReference = array_filter($value);

        return $this;
    }

    public function addCrossReference(?FHIRIdentifier ...$value): self
    {
        $this->crossReference = array_filter(array_merge($this->crossReference, $value));

        return $this;
    }

    /**
     * @return FHIRMedicinalProductManufacturingBusinessOperation[]
     */
    public function getManufacturingBusinessOperation(): array
    {
        return $this->manufacturingBusinessOperation;
    }

    public function setManufacturingBusinessOperation(
        ?FHIRMedicinalProductManufacturingBusinessOperation ...$value,
    ): self
    {
        $this->manufacturingBusinessOperation = array_filter($value);

        return $this;
    }

    public function addManufacturingBusinessOperation(
        ?FHIRMedicinalProductManufacturingBusinessOperation ...$value,
    ): self
    {
        $this->manufacturingBusinessOperation = array_filter(array_merge($this->manufacturingBusinessOperation, $value));

        return $this;
    }

    /**
     * @return FHIRMedicinalProductSpecialDesignation[]
     */
    public function getSpecialDesignation(): array
    {
        return $this->specialDesignation;
    }

    public function setSpecialDesignation(?FHIRMedicinalProductSpecialDesignation ...$value): self
    {
        $this->specialDesignation = array_filter($value);

        return $this;
    }

    public function addSpecialDesignation(?FHIRMedicinalProductSpecialDesignation ...$value): self
    {
        $this->specialDesignation = array_filter(array_merge($this->specialDesignation, $value));

        return $this;
    }
}
