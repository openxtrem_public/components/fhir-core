<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Invoice Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRInvoiceInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRInvoiceLineItem;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRInvoiceLineItemPriceComponent;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRInvoiceParticipant;

class FHIRInvoice extends FHIRDomainResource implements FHIRInvoiceInterface
{
    public const RESOURCE_NAME = 'Invoice';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRString $cancelledReason = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $recipient = null;
    protected ?FHIRDateTime $date = null;

    /** @var FHIRInvoiceParticipant[] */
    protected array $participant = [];
    protected ?FHIRReference $issuer = null;
    protected ?FHIRReference $account = null;

    /** @var FHIRInvoiceLineItem[] */
    protected array $lineItem = [];

    /** @var FHIRInvoiceLineItemPriceComponent[] */
    protected array $totalPriceComponent = [];
    protected ?FHIRMoney $totalNet = null;
    protected ?FHIRMoney $totalGross = null;
    protected ?FHIRMarkdown $paymentTerms = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCancelledReason(): ?FHIRString
    {
        return $this->cancelledReason;
    }

    public function setCancelledReason(string|FHIRString|null $value): self
    {
        $this->cancelledReason = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getRecipient(): ?FHIRReference
    {
        return $this->recipient;
    }

    public function setRecipient(?FHIRReference $value): self
    {
        $this->recipient = $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRInvoiceParticipant[]
     */
    public function getParticipant(): array
    {
        return $this->participant;
    }

    public function setParticipant(?FHIRInvoiceParticipant ...$value): self
    {
        $this->participant = array_filter($value);

        return $this;
    }

    public function addParticipant(?FHIRInvoiceParticipant ...$value): self
    {
        $this->participant = array_filter(array_merge($this->participant, $value));

        return $this;
    }

    public function getIssuer(): ?FHIRReference
    {
        return $this->issuer;
    }

    public function setIssuer(?FHIRReference $value): self
    {
        $this->issuer = $value;

        return $this;
    }

    public function getAccount(): ?FHIRReference
    {
        return $this->account;
    }

    public function setAccount(?FHIRReference $value): self
    {
        $this->account = $value;

        return $this;
    }

    /**
     * @return FHIRInvoiceLineItem[]
     */
    public function getLineItem(): array
    {
        return $this->lineItem;
    }

    public function setLineItem(?FHIRInvoiceLineItem ...$value): self
    {
        $this->lineItem = array_filter($value);

        return $this;
    }

    public function addLineItem(?FHIRInvoiceLineItem ...$value): self
    {
        $this->lineItem = array_filter(array_merge($this->lineItem, $value));

        return $this;
    }

    /**
     * @return FHIRInvoiceLineItemPriceComponent[]
     */
    public function getTotalPriceComponent(): array
    {
        return $this->totalPriceComponent;
    }

    public function setTotalPriceComponent(?FHIRInvoiceLineItemPriceComponent ...$value): self
    {
        $this->totalPriceComponent = array_filter($value);

        return $this;
    }

    public function addTotalPriceComponent(?FHIRInvoiceLineItemPriceComponent ...$value): self
    {
        $this->totalPriceComponent = array_filter(array_merge($this->totalPriceComponent, $value));

        return $this;
    }

    public function getTotalNet(): ?FHIRMoney
    {
        return $this->totalNet;
    }

    public function setTotalNet(?FHIRMoney $value): self
    {
        $this->totalNet = $value;

        return $this;
    }

    public function getTotalGross(): ?FHIRMoney
    {
        return $this->totalGross;
    }

    public function setTotalGross(?FHIRMoney $value): self
    {
        $this->totalGross = $value;

        return $this;
    }

    public function getPaymentTerms(): ?FHIRMarkdown
    {
        return $this->paymentTerms;
    }

    public function setPaymentTerms(string|FHIRMarkdown|null $value): self
    {
        $this->paymentTerms = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }
}
