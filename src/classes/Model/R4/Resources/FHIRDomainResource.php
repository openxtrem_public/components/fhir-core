<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DomainResource Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRDomainResourceInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRExtension;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRNarrative;

abstract class FHIRDomainResource extends FHIRResource implements FHIRDomainResourceInterface
{
    public const RESOURCE_NAME = 'DomainResource';

    protected ?FHIRNarrative $text = null;

    /** @var FHIRResource[] */
    protected array $contained = [];

    /** @var FHIRExtension[] */
    protected array $extension = [];

    /** @var FHIRExtension[] */
    protected array $modifierExtension = [];

    public function getText(): ?FHIRNarrative
    {
        return $this->text;
    }

    public function setText(?FHIRNarrative $value): self
    {
        $this->text = $value;

        return $this;
    }

    /**
     * @return FHIRResource[]
     */
    public function getContained(): array
    {
        return $this->contained;
    }

    public function setContained(?FHIRResource ...$value): self
    {
        $this->contained = array_filter($value);

        return $this;
    }

    public function addContained(?FHIRResource ...$value): self
    {
        $this->contained = array_filter(array_merge($this->contained, $value));

        return $this;
    }

    /**
     * @return FHIRExtension[]
     */
    public function getExtension(): array
    {
        return $this->extension;
    }

    public function setExtension(?FHIRExtension ...$value): self
    {
        $this->extension = array_filter($value);

        return $this;
    }

    public function addExtension(?FHIRExtension ...$value): self
    {
        $this->extension = array_filter(array_merge($this->extension, $value));

        return $this;
    }

    /**
     * @return FHIRExtension[]
     */
    public function getModifierExtension(): array
    {
        return $this->modifierExtension;
    }

    public function setModifierExtension(?FHIRExtension ...$value): self
    {
        $this->modifierExtension = array_filter($value);

        return $this;
    }

    public function addModifierExtension(?FHIRExtension ...$value): self
    {
        $this->modifierExtension = array_filter(array_merge($this->modifierExtension, $value));

        return $this;
    }
}
