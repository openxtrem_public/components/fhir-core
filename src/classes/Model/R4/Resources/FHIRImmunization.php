<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Immunization Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRImmunizationInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRImmunizationEducation;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRImmunizationPerformer;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRImmunizationProtocolApplied;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRImmunizationReaction;

class FHIRImmunization extends FHIRDomainResource implements FHIRImmunizationInterface
{
    public const RESOURCE_NAME = 'Immunization';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $statusReason = null;
    protected ?FHIRCodeableConcept $vaccineCode = null;
    protected ?FHIRReference $patient = null;
    protected ?FHIRReference $encounter = null;
    protected FHIRDateTime|FHIRString|null $occurrence = null;
    protected ?FHIRDateTime $recorded = null;
    protected ?FHIRBoolean $primarySource = null;
    protected ?FHIRCodeableConcept $reportOrigin = null;
    protected ?FHIRReference $location = null;
    protected ?FHIRReference $manufacturer = null;
    protected ?FHIRString $lotNumber = null;
    protected ?FHIRDate $expirationDate = null;
    protected ?FHIRCodeableConcept $site = null;
    protected ?FHIRCodeableConcept $route = null;
    protected ?FHIRQuantity $doseQuantity = null;

    /** @var FHIRImmunizationPerformer[] */
    protected array $performer = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRCodeableConcept[] */
    protected array $reasonCode = [];

    /** @var FHIRReference[] */
    protected array $reasonReference = [];
    protected ?FHIRBoolean $isSubpotent = null;

    /** @var FHIRCodeableConcept[] */
    protected array $subpotentReason = [];

    /** @var FHIRImmunizationEducation[] */
    protected array $education = [];

    /** @var FHIRCodeableConcept[] */
    protected array $programEligibility = [];
    protected ?FHIRCodeableConcept $fundingSource = null;

    /** @var FHIRImmunizationReaction[] */
    protected array $reaction = [];

    /** @var FHIRImmunizationProtocolApplied[] */
    protected array $protocolApplied = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getStatusReason(): ?FHIRCodeableConcept
    {
        return $this->statusReason;
    }

    public function setStatusReason(?FHIRCodeableConcept $value): self
    {
        $this->statusReason = $value;

        return $this;
    }

    public function getVaccineCode(): ?FHIRCodeableConcept
    {
        return $this->vaccineCode;
    }

    public function setVaccineCode(?FHIRCodeableConcept $value): self
    {
        $this->vaccineCode = $value;

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getOccurrence(): FHIRDateTime|FHIRString|null
    {
        return $this->occurrence;
    }

    public function setOccurrence(FHIRDateTime|FHIRString|null $value): self
    {
        $this->occurrence = $value;

        return $this;
    }

    public function getRecorded(): ?FHIRDateTime
    {
        return $this->recorded;
    }

    public function setRecorded(string|FHIRDateTime|null $value): self
    {
        $this->recorded = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPrimarySource(): ?FHIRBoolean
    {
        return $this->primarySource;
    }

    public function setPrimarySource(bool|FHIRBoolean|null $value): self
    {
        $this->primarySource = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getReportOrigin(): ?FHIRCodeableConcept
    {
        return $this->reportOrigin;
    }

    public function setReportOrigin(?FHIRCodeableConcept $value): self
    {
        $this->reportOrigin = $value;

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    public function getManufacturer(): ?FHIRReference
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?FHIRReference $value): self
    {
        $this->manufacturer = $value;

        return $this;
    }

    public function getLotNumber(): ?FHIRString
    {
        return $this->lotNumber;
    }

    public function setLotNumber(string|FHIRString|null $value): self
    {
        $this->lotNumber = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getExpirationDate(): ?FHIRDate
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(string|FHIRDate|null $value): self
    {
        $this->expirationDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getSite(): ?FHIRCodeableConcept
    {
        return $this->site;
    }

    public function setSite(?FHIRCodeableConcept $value): self
    {
        $this->site = $value;

        return $this;
    }

    public function getRoute(): ?FHIRCodeableConcept
    {
        return $this->route;
    }

    public function setRoute(?FHIRCodeableConcept $value): self
    {
        $this->route = $value;

        return $this;
    }

    public function getDoseQuantity(): ?FHIRQuantity
    {
        return $this->doseQuantity;
    }

    public function setDoseQuantity(?FHIRQuantity $value): self
    {
        $this->doseQuantity = $value;

        return $this;
    }

    /**
     * @return FHIRImmunizationPerformer[]
     */
    public function getPerformer(): array
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRImmunizationPerformer ...$value): self
    {
        $this->performer = array_filter($value);

        return $this;
    }

    public function addPerformer(?FHIRImmunizationPerformer ...$value): self
    {
        $this->performer = array_filter(array_merge($this->performer, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReasonCode(): array
    {
        return $this->reasonCode;
    }

    public function setReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter($value);

        return $this;
    }

    public function addReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter(array_merge($this->reasonCode, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReasonReference(): array
    {
        return $this->reasonReference;
    }

    public function setReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter($value);

        return $this;
    }

    public function addReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter(array_merge($this->reasonReference, $value));

        return $this;
    }

    public function getIsSubpotent(): ?FHIRBoolean
    {
        return $this->isSubpotent;
    }

    public function setIsSubpotent(bool|FHIRBoolean|null $value): self
    {
        $this->isSubpotent = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSubpotentReason(): array
    {
        return $this->subpotentReason;
    }

    public function setSubpotentReason(?FHIRCodeableConcept ...$value): self
    {
        $this->subpotentReason = array_filter($value);

        return $this;
    }

    public function addSubpotentReason(?FHIRCodeableConcept ...$value): self
    {
        $this->subpotentReason = array_filter(array_merge($this->subpotentReason, $value));

        return $this;
    }

    /**
     * @return FHIRImmunizationEducation[]
     */
    public function getEducation(): array
    {
        return $this->education;
    }

    public function setEducation(?FHIRImmunizationEducation ...$value): self
    {
        $this->education = array_filter($value);

        return $this;
    }

    public function addEducation(?FHIRImmunizationEducation ...$value): self
    {
        $this->education = array_filter(array_merge($this->education, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getProgramEligibility(): array
    {
        return $this->programEligibility;
    }

    public function setProgramEligibility(?FHIRCodeableConcept ...$value): self
    {
        $this->programEligibility = array_filter($value);

        return $this;
    }

    public function addProgramEligibility(?FHIRCodeableConcept ...$value): self
    {
        $this->programEligibility = array_filter(array_merge($this->programEligibility, $value));

        return $this;
    }

    public function getFundingSource(): ?FHIRCodeableConcept
    {
        return $this->fundingSource;
    }

    public function setFundingSource(?FHIRCodeableConcept $value): self
    {
        $this->fundingSource = $value;

        return $this;
    }

    /**
     * @return FHIRImmunizationReaction[]
     */
    public function getReaction(): array
    {
        return $this->reaction;
    }

    public function setReaction(?FHIRImmunizationReaction ...$value): self
    {
        $this->reaction = array_filter($value);

        return $this;
    }

    public function addReaction(?FHIRImmunizationReaction ...$value): self
    {
        $this->reaction = array_filter(array_merge($this->reaction, $value));

        return $this;
    }

    /**
     * @return FHIRImmunizationProtocolApplied[]
     */
    public function getProtocolApplied(): array
    {
        return $this->protocolApplied;
    }

    public function setProtocolApplied(?FHIRImmunizationProtocolApplied ...$value): self
    {
        $this->protocolApplied = array_filter($value);

        return $this;
    }

    public function addProtocolApplied(?FHIRImmunizationProtocolApplied ...$value): self
    {
        $this->protocolApplied = array_filter(array_merge($this->protocolApplied, $value));

        return $this;
    }
}
