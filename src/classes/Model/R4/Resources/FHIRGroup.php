<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Group Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRGroupInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUnsignedInt;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRGroupCharacteristic;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRGroupMember;

class FHIRGroup extends FHIRDomainResource implements FHIRGroupInterface
{
    public const RESOURCE_NAME = 'Group';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRBoolean $active = null;
    protected ?FHIRCode $type = null;
    protected ?FHIRBoolean $actual = null;
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRString $name = null;
    protected ?FHIRUnsignedInt $quantity = null;
    protected ?FHIRReference $managingEntity = null;

    /** @var FHIRGroupCharacteristic[] */
    protected array $characteristic = [];

    /** @var FHIRGroupMember[] */
    protected array $member = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getActive(): ?FHIRBoolean
    {
        return $this->active;
    }

    public function setActive(bool|FHIRBoolean|null $value): self
    {
        $this->active = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getActual(): ?FHIRBoolean
    {
        return $this->actual;
    }

    public function setActual(bool|FHIRBoolean|null $value): self
    {
        $this->actual = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getQuantity(): ?FHIRUnsignedInt
    {
        return $this->quantity;
    }

    public function setQuantity(int|FHIRUnsignedInt|null $value): self
    {
        $this->quantity = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getManagingEntity(): ?FHIRReference
    {
        return $this->managingEntity;
    }

    public function setManagingEntity(?FHIRReference $value): self
    {
        $this->managingEntity = $value;

        return $this;
    }

    /**
     * @return FHIRGroupCharacteristic[]
     */
    public function getCharacteristic(): array
    {
        return $this->characteristic;
    }

    public function setCharacteristic(?FHIRGroupCharacteristic ...$value): self
    {
        $this->characteristic = array_filter($value);

        return $this;
    }

    public function addCharacteristic(?FHIRGroupCharacteristic ...$value): self
    {
        $this->characteristic = array_filter(array_merge($this->characteristic, $value));

        return $this;
    }

    /**
     * @return FHIRGroupMember[]
     */
    public function getMember(): array
    {
        return $this->member;
    }

    public function setMember(?FHIRGroupMember ...$value): self
    {
        $this->member = array_filter($value);

        return $this;
    }

    public function addMember(?FHIRGroupMember ...$value): self
    {
        $this->member = array_filter(array_merge($this->member, $value));

        return $this;
    }
}
