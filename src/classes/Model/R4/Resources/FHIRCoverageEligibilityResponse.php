<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CoverageEligibilityResponse Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRCoverageEligibilityResponseInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRCoverageEligibilityResponseError;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRCoverageEligibilityResponseInsurance;

class FHIRCoverageEligibilityResponse extends FHIRDomainResource implements FHIRCoverageEligibilityResponseInterface
{
    public const RESOURCE_NAME = 'CoverageEligibilityResponse';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;

    /** @var FHIRCode[] */
    protected array $purpose = [];
    protected ?FHIRReference $patient = null;
    protected FHIRDate|FHIRPeriod|null $serviced = null;
    protected ?FHIRDateTime $created = null;
    protected ?FHIRReference $requestor = null;
    protected ?FHIRReference $request = null;
    protected ?FHIRCode $outcome = null;
    protected ?FHIRString $disposition = null;
    protected ?FHIRReference $insurer = null;

    /** @var FHIRCoverageEligibilityResponseInsurance[] */
    protected array $insurance = [];
    protected ?FHIRString $preAuthRef = null;
    protected ?FHIRCodeableConcept $form = null;

    /** @var FHIRCoverageEligibilityResponseError[] */
    protected array $error = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getPurpose(): array
    {
        return $this->purpose;
    }

    public function setPurpose(string|FHIRCode|null ...$value): self
    {
        $this->purpose = [];
        $this->addPurpose(...$value);

        return $this;
    }

    public function addPurpose(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->purpose = array_filter(array_merge($this->purpose, $values));

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getServiced(): FHIRDate|FHIRPeriod|null
    {
        return $this->serviced;
    }

    public function setServiced(FHIRDate|FHIRPeriod|null $value): self
    {
        $this->serviced = $value;

        return $this;
    }

    public function getCreated(): ?FHIRDateTime
    {
        return $this->created;
    }

    public function setCreated(string|FHIRDateTime|null $value): self
    {
        $this->created = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getRequestor(): ?FHIRReference
    {
        return $this->requestor;
    }

    public function setRequestor(?FHIRReference $value): self
    {
        $this->requestor = $value;

        return $this;
    }

    public function getRequest(): ?FHIRReference
    {
        return $this->request;
    }

    public function setRequest(?FHIRReference $value): self
    {
        $this->request = $value;

        return $this;
    }

    public function getOutcome(): ?FHIRCode
    {
        return $this->outcome;
    }

    public function setOutcome(string|FHIRCode|null $value): self
    {
        $this->outcome = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDisposition(): ?FHIRString
    {
        return $this->disposition;
    }

    public function setDisposition(string|FHIRString|null $value): self
    {
        $this->disposition = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getInsurer(): ?FHIRReference
    {
        return $this->insurer;
    }

    public function setInsurer(?FHIRReference $value): self
    {
        $this->insurer = $value;

        return $this;
    }

    /**
     * @return FHIRCoverageEligibilityResponseInsurance[]
     */
    public function getInsurance(): array
    {
        return $this->insurance;
    }

    public function setInsurance(?FHIRCoverageEligibilityResponseInsurance ...$value): self
    {
        $this->insurance = array_filter($value);

        return $this;
    }

    public function addInsurance(?FHIRCoverageEligibilityResponseInsurance ...$value): self
    {
        $this->insurance = array_filter(array_merge($this->insurance, $value));

        return $this;
    }

    public function getPreAuthRef(): ?FHIRString
    {
        return $this->preAuthRef;
    }

    public function setPreAuthRef(string|FHIRString|null $value): self
    {
        $this->preAuthRef = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getForm(): ?FHIRCodeableConcept
    {
        return $this->form;
    }

    public function setForm(?FHIRCodeableConcept $value): self
    {
        $this->form = $value;

        return $this;
    }

    /**
     * @return FHIRCoverageEligibilityResponseError[]
     */
    public function getError(): array
    {
        return $this->error;
    }

    public function setError(?FHIRCoverageEligibilityResponseError ...$value): self
    {
        $this->error = array_filter($value);

        return $this;
    }

    public function addError(?FHIRCoverageEligibilityResponseError ...$value): self
    {
        $this->error = array_filter(array_merge($this->error, $value));

        return $this;
    }
}
