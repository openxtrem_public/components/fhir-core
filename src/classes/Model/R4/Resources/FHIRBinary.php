<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Binary Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRBinaryInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBase64Binary;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;

class FHIRBinary extends FHIRResource implements FHIRBinaryInterface
{
    public const RESOURCE_NAME = 'Binary';

    protected ?FHIRCode $contentType = null;
    protected ?FHIRReference $securityContext = null;
    protected ?FHIRBase64Binary $data = null;

    public function getContentType(): ?FHIRCode
    {
        return $this->contentType;
    }

    public function setContentType(string|FHIRCode|null $value): self
    {
        $this->contentType = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getSecurityContext(): ?FHIRReference
    {
        return $this->securityContext;
    }

    public function setSecurityContext(?FHIRReference $value): self
    {
        $this->securityContext = $value;

        return $this;
    }

    public function getData(): ?FHIRBase64Binary
    {
        return $this->data;
    }

    public function setData(string|FHIRBase64Binary|null $value): self
    {
        $this->data = is_string($value) ? (new FHIRBase64Binary())->setValue($value) : $value;

        return $this;
    }
}
