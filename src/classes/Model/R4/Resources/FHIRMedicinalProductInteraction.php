<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductInteraction Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMedicinalProductInteractionInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicinalProductInteractionInteractant;

class FHIRMedicinalProductInteraction extends FHIRDomainResource implements FHIRMedicinalProductInteractionInterface
{
    public const RESOURCE_NAME = 'MedicinalProductInteraction';

    /** @var FHIRReference[] */
    protected array $subject = [];
    protected ?FHIRString $description = null;

    /** @var FHIRMedicinalProductInteractionInteractant[] */
    protected array $interactant = [];
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableConcept $effect = null;
    protected ?FHIRCodeableConcept $incidence = null;
    protected ?FHIRCodeableConcept $management = null;

    /**
     * @return FHIRReference[]
     */
    public function getSubject(): array
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference ...$value): self
    {
        $this->subject = array_filter($value);

        return $this;
    }

    public function addSubject(?FHIRReference ...$value): self
    {
        $this->subject = array_filter(array_merge($this->subject, $value));

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRMedicinalProductInteractionInteractant[]
     */
    public function getInteractant(): array
    {
        return $this->interactant;
    }

    public function setInteractant(?FHIRMedicinalProductInteractionInteractant ...$value): self
    {
        $this->interactant = array_filter($value);

        return $this;
    }

    public function addInteractant(?FHIRMedicinalProductInteractionInteractant ...$value): self
    {
        $this->interactant = array_filter(array_merge($this->interactant, $value));

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getEffect(): ?FHIRCodeableConcept
    {
        return $this->effect;
    }

    public function setEffect(?FHIRCodeableConcept $value): self
    {
        $this->effect = $value;

        return $this;
    }

    public function getIncidence(): ?FHIRCodeableConcept
    {
        return $this->incidence;
    }

    public function setIncidence(?FHIRCodeableConcept $value): self
    {
        $this->incidence = $value;

        return $this;
    }

    public function getManagement(): ?FHIRCodeableConcept
    {
        return $this->management;
    }

    public function setManagement(?FHIRCodeableConcept $value): self
    {
        $this->management = $value;

        return $this;
    }
}
