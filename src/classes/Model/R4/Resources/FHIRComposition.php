<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Composition Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRCompositionInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRCompositionAttester;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRCompositionEvent;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRCompositionRelatesTo;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRCompositionSection;

class FHIRComposition extends FHIRDomainResource implements FHIRCompositionInterface
{
    public const RESOURCE_NAME = 'Composition';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $encounter = null;
    protected ?FHIRDateTime $date = null;

    /** @var FHIRReference[] */
    protected array $author = [];
    protected ?FHIRString $title = null;
    protected ?FHIRCode $confidentiality = null;

    /** @var FHIRCompositionAttester[] */
    protected array $attester = [];
    protected ?FHIRReference $custodian = null;

    /** @var FHIRCompositionRelatesTo[] */
    protected array $relatesTo = [];

    /** @var FHIRCompositionEvent[] */
    protected array $event = [];

    /** @var FHIRCompositionSection[] */
    protected array $section = [];

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAuthor(): array
    {
        return $this->author;
    }

    public function setAuthor(?FHIRReference ...$value): self
    {
        $this->author = array_filter($value);

        return $this;
    }

    public function addAuthor(?FHIRReference ...$value): self
    {
        $this->author = array_filter(array_merge($this->author, $value));

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getConfidentiality(): ?FHIRCode
    {
        return $this->confidentiality;
    }

    public function setConfidentiality(string|FHIRCode|null $value): self
    {
        $this->confidentiality = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCompositionAttester[]
     */
    public function getAttester(): array
    {
        return $this->attester;
    }

    public function setAttester(?FHIRCompositionAttester ...$value): self
    {
        $this->attester = array_filter($value);

        return $this;
    }

    public function addAttester(?FHIRCompositionAttester ...$value): self
    {
        $this->attester = array_filter(array_merge($this->attester, $value));

        return $this;
    }

    public function getCustodian(): ?FHIRReference
    {
        return $this->custodian;
    }

    public function setCustodian(?FHIRReference $value): self
    {
        $this->custodian = $value;

        return $this;
    }

    /**
     * @return FHIRCompositionRelatesTo[]
     */
    public function getRelatesTo(): array
    {
        return $this->relatesTo;
    }

    public function setRelatesTo(?FHIRCompositionRelatesTo ...$value): self
    {
        $this->relatesTo = array_filter($value);

        return $this;
    }

    public function addRelatesTo(?FHIRCompositionRelatesTo ...$value): self
    {
        $this->relatesTo = array_filter(array_merge($this->relatesTo, $value));

        return $this;
    }

    /**
     * @return FHIRCompositionEvent[]
     */
    public function getEvent(): array
    {
        return $this->event;
    }

    public function setEvent(?FHIRCompositionEvent ...$value): self
    {
        $this->event = array_filter($value);

        return $this;
    }

    public function addEvent(?FHIRCompositionEvent ...$value): self
    {
        $this->event = array_filter(array_merge($this->event, $value));

        return $this;
    }

    /**
     * @return FHIRCompositionSection[]
     */
    public function getSection(): array
    {
        return $this->section;
    }

    public function setSection(?FHIRCompositionSection ...$value): self
    {
        $this->section = array_filter($value);

        return $this;
    }

    public function addSection(?FHIRCompositionSection ...$value): self
    {
        $this->section = array_filter(array_merge($this->section, $value));

        return $this;
    }
}
