<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MessageHeader Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMessageHeaderInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMessageHeaderDestination;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMessageHeaderResponse;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMessageHeaderSource;

class FHIRMessageHeader extends FHIRDomainResource implements FHIRMessageHeaderInterface
{
    public const RESOURCE_NAME = 'MessageHeader';

    protected FHIRCoding|FHIRUri|null $event = null;

    /** @var FHIRMessageHeaderDestination[] */
    protected array $destination = [];
    protected ?FHIRReference $sender = null;
    protected ?FHIRReference $enterer = null;
    protected ?FHIRReference $author = null;
    protected ?FHIRMessageHeaderSource $source = null;
    protected ?FHIRReference $responsible = null;
    protected ?FHIRCodeableConcept $reason = null;
    protected ?FHIRMessageHeaderResponse $response = null;

    /** @var FHIRReference[] */
    protected array $focus = [];
    protected ?FHIRCanonical $definition = null;

    public function getEvent(): FHIRCoding|FHIRUri|null
    {
        return $this->event;
    }

    public function setEvent(FHIRCoding|FHIRUri|null $value): self
    {
        $this->event = $value;

        return $this;
    }

    /**
     * @return FHIRMessageHeaderDestination[]
     */
    public function getDestination(): array
    {
        return $this->destination;
    }

    public function setDestination(?FHIRMessageHeaderDestination ...$value): self
    {
        $this->destination = array_filter($value);

        return $this;
    }

    public function addDestination(?FHIRMessageHeaderDestination ...$value): self
    {
        $this->destination = array_filter(array_merge($this->destination, $value));

        return $this;
    }

    public function getSender(): ?FHIRReference
    {
        return $this->sender;
    }

    public function setSender(?FHIRReference $value): self
    {
        $this->sender = $value;

        return $this;
    }

    public function getEnterer(): ?FHIRReference
    {
        return $this->enterer;
    }

    public function setEnterer(?FHIRReference $value): self
    {
        $this->enterer = $value;

        return $this;
    }

    public function getAuthor(): ?FHIRReference
    {
        return $this->author;
    }

    public function setAuthor(?FHIRReference $value): self
    {
        $this->author = $value;

        return $this;
    }

    public function getSource(): ?FHIRMessageHeaderSource
    {
        return $this->source;
    }

    public function setSource(?FHIRMessageHeaderSource $value): self
    {
        $this->source = $value;

        return $this;
    }

    public function getResponsible(): ?FHIRReference
    {
        return $this->responsible;
    }

    public function setResponsible(?FHIRReference $value): self
    {
        $this->responsible = $value;

        return $this;
    }

    public function getReason(): ?FHIRCodeableConcept
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableConcept $value): self
    {
        $this->reason = $value;

        return $this;
    }

    public function getResponse(): ?FHIRMessageHeaderResponse
    {
        return $this->response;
    }

    public function setResponse(?FHIRMessageHeaderResponse $value): self
    {
        $this->response = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getFocus(): array
    {
        return $this->focus;
    }

    public function setFocus(?FHIRReference ...$value): self
    {
        $this->focus = array_filter($value);

        return $this;
    }

    public function addFocus(?FHIRReference ...$value): self
    {
        $this->focus = array_filter(array_merge($this->focus, $value));

        return $this;
    }

    public function getDefinition(): ?FHIRCanonical
    {
        return $this->definition;
    }

    public function setDefinition(string|FHIRCanonical|null $value): self
    {
        $this->definition = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }
}
