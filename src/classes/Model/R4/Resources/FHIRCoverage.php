<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Coverage Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRCoverageInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRCoverageClass;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRCoverageCostToBeneficiary;

class FHIRCoverage extends FHIRDomainResource implements FHIRCoverageInterface
{
    public const RESOURCE_NAME = 'Coverage';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRReference $policyHolder = null;
    protected ?FHIRReference $subscriber = null;
    protected ?FHIRString $subscriberId = null;
    protected ?FHIRReference $beneficiary = null;
    protected ?FHIRString $dependent = null;
    protected ?FHIRCodeableConcept $relationship = null;
    protected ?FHIRPeriod $period = null;

    /** @var FHIRReference[] */
    protected array $payor = [];

    /** @var FHIRCoverageClass[] */
    protected array $class = [];
    protected ?FHIRPositiveInt $order = null;
    protected ?FHIRString $network = null;

    /** @var FHIRCoverageCostToBeneficiary[] */
    protected array $costToBeneficiary = [];
    protected ?FHIRBoolean $subrogation = null;

    /** @var FHIRReference[] */
    protected array $contract = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getPolicyHolder(): ?FHIRReference
    {
        return $this->policyHolder;
    }

    public function setPolicyHolder(?FHIRReference $value): self
    {
        $this->policyHolder = $value;

        return $this;
    }

    public function getSubscriber(): ?FHIRReference
    {
        return $this->subscriber;
    }

    public function setSubscriber(?FHIRReference $value): self
    {
        $this->subscriber = $value;

        return $this;
    }

    public function getSubscriberId(): ?FHIRString
    {
        return $this->subscriberId;
    }

    public function setSubscriberId(string|FHIRString|null $value): self
    {
        $this->subscriberId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getBeneficiary(): ?FHIRReference
    {
        return $this->beneficiary;
    }

    public function setBeneficiary(?FHIRReference $value): self
    {
        $this->beneficiary = $value;

        return $this;
    }

    public function getDependent(): ?FHIRString
    {
        return $this->dependent;
    }

    public function setDependent(string|FHIRString|null $value): self
    {
        $this->dependent = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getRelationship(): ?FHIRCodeableConcept
    {
        return $this->relationship;
    }

    public function setRelationship(?FHIRCodeableConcept $value): self
    {
        $this->relationship = $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPayor(): array
    {
        return $this->payor;
    }

    public function setPayor(?FHIRReference ...$value): self
    {
        $this->payor = array_filter($value);

        return $this;
    }

    public function addPayor(?FHIRReference ...$value): self
    {
        $this->payor = array_filter(array_merge($this->payor, $value));

        return $this;
    }

    /**
     * @return FHIRCoverageClass[]
     */
    public function getClass(): array
    {
        return $this->class;
    }

    public function setClass(?FHIRCoverageClass ...$value): self
    {
        $this->class = array_filter($value);

        return $this;
    }

    public function addClass(?FHIRCoverageClass ...$value): self
    {
        $this->class = array_filter(array_merge($this->class, $value));

        return $this;
    }

    public function getOrder(): ?FHIRPositiveInt
    {
        return $this->order;
    }

    public function setOrder(int|FHIRPositiveInt|null $value): self
    {
        $this->order = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getNetwork(): ?FHIRString
    {
        return $this->network;
    }

    public function setNetwork(string|FHIRString|null $value): self
    {
        $this->network = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCoverageCostToBeneficiary[]
     */
    public function getCostToBeneficiary(): array
    {
        return $this->costToBeneficiary;
    }

    public function setCostToBeneficiary(?FHIRCoverageCostToBeneficiary ...$value): self
    {
        $this->costToBeneficiary = array_filter($value);

        return $this;
    }

    public function addCostToBeneficiary(?FHIRCoverageCostToBeneficiary ...$value): self
    {
        $this->costToBeneficiary = array_filter(array_merge($this->costToBeneficiary, $value));

        return $this;
    }

    public function getSubrogation(): ?FHIRBoolean
    {
        return $this->subrogation;
    }

    public function setSubrogation(bool|FHIRBoolean|null $value): self
    {
        $this->subrogation = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getContract(): array
    {
        return $this->contract;
    }

    public function setContract(?FHIRReference ...$value): self
    {
        $this->contract = array_filter($value);

        return $this;
    }

    public function addContract(?FHIRReference ...$value): self
    {
        $this->contract = array_filter(array_merge($this->contract, $value));

        return $this;
    }
}
