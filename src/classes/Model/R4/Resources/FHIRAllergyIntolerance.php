<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AllergyIntolerance Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRAllergyIntoleranceInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAge;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRAllergyIntoleranceReaction;

class FHIRAllergyIntolerance extends FHIRDomainResource implements FHIRAllergyIntoleranceInterface
{
    public const RESOURCE_NAME = 'AllergyIntolerance';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCodeableConcept $clinicalStatus = null;
    protected ?FHIRCodeableConcept $verificationStatus = null;
    protected ?FHIRCode $type = null;

    /** @var FHIRCode[] */
    protected array $category = [];
    protected ?FHIRCode $criticality = null;
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRReference $patient = null;
    protected ?FHIRReference $encounter = null;
    protected FHIRDateTime|FHIRAge|FHIRPeriod|FHIRRange|FHIRString|null $onset = null;
    protected ?FHIRDateTime $recordedDate = null;
    protected ?FHIRReference $recorder = null;
    protected ?FHIRReference $asserter = null;
    protected ?FHIRDateTime $lastOccurrence = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRAllergyIntoleranceReaction[] */
    protected array $reaction = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getClinicalStatus(): ?FHIRCodeableConcept
    {
        return $this->clinicalStatus;
    }

    public function setClinicalStatus(?FHIRCodeableConcept $value): self
    {
        $this->clinicalStatus = $value;

        return $this;
    }

    public function getVerificationStatus(): ?FHIRCodeableConcept
    {
        return $this->verificationStatus;
    }

    public function setVerificationStatus(?FHIRCodeableConcept $value): self
    {
        $this->verificationStatus = $value;

        return $this;
    }

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(string|FHIRCode|null ...$value): self
    {
        $this->category = [];
        $this->addCategory(...$value);

        return $this;
    }

    public function addCategory(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->category = array_filter(array_merge($this->category, $values));

        return $this;
    }

    public function getCriticality(): ?FHIRCode
    {
        return $this->criticality;
    }

    public function setCriticality(string|FHIRCode|null $value): self
    {
        $this->criticality = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getOnset(): FHIRDateTime|FHIRAge|FHIRPeriod|FHIRRange|FHIRString|null
    {
        return $this->onset;
    }

    public function setOnset(FHIRDateTime|FHIRAge|FHIRPeriod|FHIRRange|FHIRString|null $value): self
    {
        $this->onset = $value;

        return $this;
    }

    public function getRecordedDate(): ?FHIRDateTime
    {
        return $this->recordedDate;
    }

    public function setRecordedDate(string|FHIRDateTime|null $value): self
    {
        $this->recordedDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getRecorder(): ?FHIRReference
    {
        return $this->recorder;
    }

    public function setRecorder(?FHIRReference $value): self
    {
        $this->recorder = $value;

        return $this;
    }

    public function getAsserter(): ?FHIRReference
    {
        return $this->asserter;
    }

    public function setAsserter(?FHIRReference $value): self
    {
        $this->asserter = $value;

        return $this;
    }

    public function getLastOccurrence(): ?FHIRDateTime
    {
        return $this->lastOccurrence;
    }

    public function setLastOccurrence(string|FHIRDateTime|null $value): self
    {
        $this->lastOccurrence = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRAllergyIntoleranceReaction[]
     */
    public function getReaction(): array
    {
        return $this->reaction;
    }

    public function setReaction(?FHIRAllergyIntoleranceReaction ...$value): self
    {
        $this->reaction = array_filter($value);

        return $this;
    }

    public function addReaction(?FHIRAllergyIntoleranceReaction ...$value): self
    {
        $this->reaction = array_filter(array_merge($this->reaction, $value));

        return $this;
    }
}
