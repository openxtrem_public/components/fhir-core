<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Location Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRLocationInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAddress;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRLocationHoursOfOperation;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRLocationPosition;

class FHIRLocation extends FHIRDomainResource implements FHIRLocationInterface
{
    public const RESOURCE_NAME = 'Location';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCoding $operationalStatus = null;
    protected ?FHIRString $name = null;

    /** @var FHIRString[] */
    protected array $alias = [];
    protected ?FHIRString $description = null;
    protected ?FHIRCode $mode = null;

    /** @var FHIRCodeableConcept[] */
    protected array $type = [];

    /** @var FHIRContactPoint[] */
    protected array $telecom = [];
    protected ?FHIRAddress $address = null;
    protected ?FHIRCodeableConcept $physicalType = null;
    protected ?FHIRLocationPosition $position = null;
    protected ?FHIRReference $managingOrganization = null;
    protected ?FHIRReference $partOf = null;

    /** @var FHIRLocationHoursOfOperation[] */
    protected array $hoursOfOperation = [];
    protected ?FHIRString $availabilityExceptions = null;

    /** @var FHIRReference[] */
    protected array $endpoint = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getOperationalStatus(): ?FHIRCoding
    {
        return $this->operationalStatus;
    }

    public function setOperationalStatus(?FHIRCoding $value): self
    {
        $this->operationalStatus = $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getAlias(): array
    {
        return $this->alias;
    }

    public function setAlias(string|FHIRString|null ...$value): self
    {
        $this->alias = [];
        $this->addAlias(...$value);

        return $this;
    }

    public function addAlias(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->alias = array_filter(array_merge($this->alias, $values));

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getMode(): ?FHIRCode
    {
        return $this->mode;
    }

    public function setMode(string|FHIRCode|null $value): self
    {
        $this->mode = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getTelecom(): array
    {
        return $this->telecom;
    }

    public function setTelecom(?FHIRContactPoint ...$value): self
    {
        $this->telecom = array_filter($value);

        return $this;
    }

    public function addTelecom(?FHIRContactPoint ...$value): self
    {
        $this->telecom = array_filter(array_merge($this->telecom, $value));

        return $this;
    }

    public function getAddress(): ?FHIRAddress
    {
        return $this->address;
    }

    public function setAddress(?FHIRAddress $value): self
    {
        $this->address = $value;

        return $this;
    }

    public function getPhysicalType(): ?FHIRCodeableConcept
    {
        return $this->physicalType;
    }

    public function setPhysicalType(?FHIRCodeableConcept $value): self
    {
        $this->physicalType = $value;

        return $this;
    }

    public function getPosition(): ?FHIRLocationPosition
    {
        return $this->position;
    }

    public function setPosition(?FHIRLocationPosition $value): self
    {
        $this->position = $value;

        return $this;
    }

    public function getManagingOrganization(): ?FHIRReference
    {
        return $this->managingOrganization;
    }

    public function setManagingOrganization(?FHIRReference $value): self
    {
        $this->managingOrganization = $value;

        return $this;
    }

    public function getPartOf(): ?FHIRReference
    {
        return $this->partOf;
    }

    public function setPartOf(?FHIRReference $value): self
    {
        $this->partOf = $value;

        return $this;
    }

    /**
     * @return FHIRLocationHoursOfOperation[]
     */
    public function getHoursOfOperation(): array
    {
        return $this->hoursOfOperation;
    }

    public function setHoursOfOperation(?FHIRLocationHoursOfOperation ...$value): self
    {
        $this->hoursOfOperation = array_filter($value);

        return $this;
    }

    public function addHoursOfOperation(?FHIRLocationHoursOfOperation ...$value): self
    {
        $this->hoursOfOperation = array_filter(array_merge($this->hoursOfOperation, $value));

        return $this;
    }

    public function getAvailabilityExceptions(): ?FHIRString
    {
        return $this->availabilityExceptions;
    }

    public function setAvailabilityExceptions(string|FHIRString|null $value): self
    {
        $this->availabilityExceptions = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEndpoint(): array
    {
        return $this->endpoint;
    }

    public function setEndpoint(?FHIRReference ...$value): self
    {
        $this->endpoint = array_filter($value);

        return $this;
    }

    public function addEndpoint(?FHIRReference ...$value): self
    {
        $this->endpoint = array_filter(array_merge($this->endpoint, $value));

        return $this;
    }
}
