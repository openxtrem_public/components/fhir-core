<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Medication Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMedicationInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRRatio;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicationBatch;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicationIngredient;

class FHIRMedication extends FHIRDomainResource implements FHIRMedicationInterface
{
    public const RESOURCE_NAME = 'Medication';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRReference $manufacturer = null;
    protected ?FHIRCodeableConcept $form = null;
    protected ?FHIRRatio $amount = null;

    /** @var FHIRMedicationIngredient[] */
    protected array $ingredient = [];
    protected ?FHIRMedicationBatch $batch = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getManufacturer(): ?FHIRReference
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?FHIRReference $value): self
    {
        $this->manufacturer = $value;

        return $this;
    }

    public function getForm(): ?FHIRCodeableConcept
    {
        return $this->form;
    }

    public function setForm(?FHIRCodeableConcept $value): self
    {
        $this->form = $value;

        return $this;
    }

    public function getAmount(): ?FHIRRatio
    {
        return $this->amount;
    }

    public function setAmount(?FHIRRatio $value): self
    {
        $this->amount = $value;

        return $this;
    }

    /**
     * @return FHIRMedicationIngredient[]
     */
    public function getIngredient(): array
    {
        return $this->ingredient;
    }

    public function setIngredient(?FHIRMedicationIngredient ...$value): self
    {
        $this->ingredient = array_filter($value);

        return $this;
    }

    public function addIngredient(?FHIRMedicationIngredient ...$value): self
    {
        $this->ingredient = array_filter(array_merge($this->ingredient, $value));

        return $this;
    }

    public function getBatch(): ?FHIRMedicationBatch
    {
        return $this->batch;
    }

    public function setBatch(?FHIRMedicationBatch $value): self
    {
        $this->batch = $value;

        return $this;
    }
}
