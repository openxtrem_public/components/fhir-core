<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClaimResponse Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRClaimResponseInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRClaimResponseAddItem;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRClaimResponseError;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRClaimResponseInsurance;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRClaimResponseItem;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRClaimResponseItemAdjudication;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRClaimResponsePayment;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRClaimResponseProcessNote;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRClaimResponseTotal;

class FHIRClaimResponse extends FHIRDomainResource implements FHIRClaimResponseInterface
{
    public const RESOURCE_NAME = 'ClaimResponse';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableConcept $subType = null;
    protected ?FHIRCode $use = null;
    protected ?FHIRReference $patient = null;
    protected ?FHIRDateTime $created = null;
    protected ?FHIRReference $insurer = null;
    protected ?FHIRReference $requestor = null;
    protected ?FHIRReference $request = null;
    protected ?FHIRCode $outcome = null;
    protected ?FHIRString $disposition = null;
    protected ?FHIRString $preAuthRef = null;
    protected ?FHIRPeriod $preAuthPeriod = null;
    protected ?FHIRCodeableConcept $payeeType = null;

    /** @var FHIRClaimResponseItem[] */
    protected array $item = [];

    /** @var FHIRClaimResponseAddItem[] */
    protected array $addItem = [];

    /** @var FHIRClaimResponseItemAdjudication[] */
    protected array $adjudication = [];

    /** @var FHIRClaimResponseTotal[] */
    protected array $total = [];
    protected ?FHIRClaimResponsePayment $payment = null;
    protected ?FHIRCodeableConcept $fundsReserve = null;
    protected ?FHIRCodeableConcept $formCode = null;
    protected ?FHIRAttachment $form = null;

    /** @var FHIRClaimResponseProcessNote[] */
    protected array $processNote = [];

    /** @var FHIRReference[] */
    protected array $communicationRequest = [];

    /** @var FHIRClaimResponseInsurance[] */
    protected array $insurance = [];

    /** @var FHIRClaimResponseError[] */
    protected array $error = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getSubType(): ?FHIRCodeableConcept
    {
        return $this->subType;
    }

    public function setSubType(?FHIRCodeableConcept $value): self
    {
        $this->subType = $value;

        return $this;
    }

    public function getUse(): ?FHIRCode
    {
        return $this->use;
    }

    public function setUse(string|FHIRCode|null $value): self
    {
        $this->use = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getCreated(): ?FHIRDateTime
    {
        return $this->created;
    }

    public function setCreated(string|FHIRDateTime|null $value): self
    {
        $this->created = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getInsurer(): ?FHIRReference
    {
        return $this->insurer;
    }

    public function setInsurer(?FHIRReference $value): self
    {
        $this->insurer = $value;

        return $this;
    }

    public function getRequestor(): ?FHIRReference
    {
        return $this->requestor;
    }

    public function setRequestor(?FHIRReference $value): self
    {
        $this->requestor = $value;

        return $this;
    }

    public function getRequest(): ?FHIRReference
    {
        return $this->request;
    }

    public function setRequest(?FHIRReference $value): self
    {
        $this->request = $value;

        return $this;
    }

    public function getOutcome(): ?FHIRCode
    {
        return $this->outcome;
    }

    public function setOutcome(string|FHIRCode|null $value): self
    {
        $this->outcome = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDisposition(): ?FHIRString
    {
        return $this->disposition;
    }

    public function setDisposition(string|FHIRString|null $value): self
    {
        $this->disposition = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPreAuthRef(): ?FHIRString
    {
        return $this->preAuthRef;
    }

    public function setPreAuthRef(string|FHIRString|null $value): self
    {
        $this->preAuthRef = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPreAuthPeriod(): ?FHIRPeriod
    {
        return $this->preAuthPeriod;
    }

    public function setPreAuthPeriod(?FHIRPeriod $value): self
    {
        $this->preAuthPeriod = $value;

        return $this;
    }

    public function getPayeeType(): ?FHIRCodeableConcept
    {
        return $this->payeeType;
    }

    public function setPayeeType(?FHIRCodeableConcept $value): self
    {
        $this->payeeType = $value;

        return $this;
    }

    /**
     * @return FHIRClaimResponseItem[]
     */
    public function getItem(): array
    {
        return $this->item;
    }

    public function setItem(?FHIRClaimResponseItem ...$value): self
    {
        $this->item = array_filter($value);

        return $this;
    }

    public function addItem(?FHIRClaimResponseItem ...$value): self
    {
        $this->item = array_filter(array_merge($this->item, $value));

        return $this;
    }

    /**
     * @return FHIRClaimResponseAddItem[]
     */
    public function getAddItem(): array
    {
        return $this->addItem;
    }

    public function setAddItem(?FHIRClaimResponseAddItem ...$value): self
    {
        $this->addItem = array_filter($value);

        return $this;
    }

    public function addAddItem(?FHIRClaimResponseAddItem ...$value): self
    {
        $this->addItem = array_filter(array_merge($this->addItem, $value));

        return $this;
    }

    /**
     * @return FHIRClaimResponseItemAdjudication[]
     */
    public function getAdjudication(): array
    {
        return $this->adjudication;
    }

    public function setAdjudication(?FHIRClaimResponseItemAdjudication ...$value): self
    {
        $this->adjudication = array_filter($value);

        return $this;
    }

    public function addAdjudication(?FHIRClaimResponseItemAdjudication ...$value): self
    {
        $this->adjudication = array_filter(array_merge($this->adjudication, $value));

        return $this;
    }

    /**
     * @return FHIRClaimResponseTotal[]
     */
    public function getTotal(): array
    {
        return $this->total;
    }

    public function setTotal(?FHIRClaimResponseTotal ...$value): self
    {
        $this->total = array_filter($value);

        return $this;
    }

    public function addTotal(?FHIRClaimResponseTotal ...$value): self
    {
        $this->total = array_filter(array_merge($this->total, $value));

        return $this;
    }

    public function getPayment(): ?FHIRClaimResponsePayment
    {
        return $this->payment;
    }

    public function setPayment(?FHIRClaimResponsePayment $value): self
    {
        $this->payment = $value;

        return $this;
    }

    public function getFundsReserve(): ?FHIRCodeableConcept
    {
        return $this->fundsReserve;
    }

    public function setFundsReserve(?FHIRCodeableConcept $value): self
    {
        $this->fundsReserve = $value;

        return $this;
    }

    public function getFormCode(): ?FHIRCodeableConcept
    {
        return $this->formCode;
    }

    public function setFormCode(?FHIRCodeableConcept $value): self
    {
        $this->formCode = $value;

        return $this;
    }

    public function getForm(): ?FHIRAttachment
    {
        return $this->form;
    }

    public function setForm(?FHIRAttachment $value): self
    {
        $this->form = $value;

        return $this;
    }

    /**
     * @return FHIRClaimResponseProcessNote[]
     */
    public function getProcessNote(): array
    {
        return $this->processNote;
    }

    public function setProcessNote(?FHIRClaimResponseProcessNote ...$value): self
    {
        $this->processNote = array_filter($value);

        return $this;
    }

    public function addProcessNote(?FHIRClaimResponseProcessNote ...$value): self
    {
        $this->processNote = array_filter(array_merge($this->processNote, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getCommunicationRequest(): array
    {
        return $this->communicationRequest;
    }

    public function setCommunicationRequest(?FHIRReference ...$value): self
    {
        $this->communicationRequest = array_filter($value);

        return $this;
    }

    public function addCommunicationRequest(?FHIRReference ...$value): self
    {
        $this->communicationRequest = array_filter(array_merge($this->communicationRequest, $value));

        return $this;
    }

    /**
     * @return FHIRClaimResponseInsurance[]
     */
    public function getInsurance(): array
    {
        return $this->insurance;
    }

    public function setInsurance(?FHIRClaimResponseInsurance ...$value): self
    {
        $this->insurance = array_filter($value);

        return $this;
    }

    public function addInsurance(?FHIRClaimResponseInsurance ...$value): self
    {
        $this->insurance = array_filter(array_merge($this->insurance, $value));

        return $this;
    }

    /**
     * @return FHIRClaimResponseError[]
     */
    public function getError(): array
    {
        return $this->error;
    }

    public function setError(?FHIRClaimResponseError ...$value): self
    {
        $this->error = array_filter($value);

        return $this;
    }

    public function addError(?FHIRClaimResponseError ...$value): self
    {
        $this->error = array_filter(array_merge($this->error, $value));

        return $this;
    }
}
