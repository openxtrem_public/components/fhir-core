<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Appointment Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRAppointmentInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUnsignedInt;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRAppointmentParticipant;

class FHIRAppointment extends FHIRDomainResource implements FHIRAppointmentInterface
{
    public const RESOURCE_NAME = 'Appointment';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $cancelationReason = null;

    /** @var FHIRCodeableConcept[] */
    protected array $serviceCategory = [];

    /** @var FHIRCodeableConcept[] */
    protected array $serviceType = [];

    /** @var FHIRCodeableConcept[] */
    protected array $specialty = [];
    protected ?FHIRCodeableConcept $appointmentType = null;

    /** @var FHIRCodeableConcept[] */
    protected array $reasonCode = [];

    /** @var FHIRReference[] */
    protected array $reasonReference = [];
    protected ?FHIRUnsignedInt $priority = null;
    protected ?FHIRString $description = null;

    /** @var FHIRReference[] */
    protected array $supportingInformation = [];
    protected ?FHIRInstant $start = null;
    protected ?FHIRInstant $end = null;
    protected ?FHIRPositiveInt $minutesDuration = null;

    /** @var FHIRReference[] */
    protected array $slot = [];
    protected ?FHIRDateTime $created = null;
    protected ?FHIRString $comment = null;
    protected ?FHIRString $patientInstruction = null;

    /** @var FHIRReference[] */
    protected array $basedOn = [];

    /** @var FHIRAppointmentParticipant[] */
    protected array $participant = [];

    /** @var FHIRPeriod[] */
    protected array $requestedPeriod = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCancelationReason(): ?FHIRCodeableConcept
    {
        return $this->cancelationReason;
    }

    public function setCancelationReason(?FHIRCodeableConcept $value): self
    {
        $this->cancelationReason = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getServiceCategory(): array
    {
        return $this->serviceCategory;
    }

    public function setServiceCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->serviceCategory = array_filter($value);

        return $this;
    }

    public function addServiceCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->serviceCategory = array_filter(array_merge($this->serviceCategory, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getServiceType(): array
    {
        return $this->serviceType;
    }

    public function setServiceType(?FHIRCodeableConcept ...$value): self
    {
        $this->serviceType = array_filter($value);

        return $this;
    }

    public function addServiceType(?FHIRCodeableConcept ...$value): self
    {
        $this->serviceType = array_filter(array_merge($this->serviceType, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSpecialty(): array
    {
        return $this->specialty;
    }

    public function setSpecialty(?FHIRCodeableConcept ...$value): self
    {
        $this->specialty = array_filter($value);

        return $this;
    }

    public function addSpecialty(?FHIRCodeableConcept ...$value): self
    {
        $this->specialty = array_filter(array_merge($this->specialty, $value));

        return $this;
    }

    public function getAppointmentType(): ?FHIRCodeableConcept
    {
        return $this->appointmentType;
    }

    public function setAppointmentType(?FHIRCodeableConcept $value): self
    {
        $this->appointmentType = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReasonCode(): array
    {
        return $this->reasonCode;
    }

    public function setReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter($value);

        return $this;
    }

    public function addReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter(array_merge($this->reasonCode, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReasonReference(): array
    {
        return $this->reasonReference;
    }

    public function setReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter($value);

        return $this;
    }

    public function addReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter(array_merge($this->reasonReference, $value));

        return $this;
    }

    public function getPriority(): ?FHIRUnsignedInt
    {
        return $this->priority;
    }

    public function setPriority(int|FHIRUnsignedInt|null $value): self
    {
        $this->priority = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupportingInformation(): array
    {
        return $this->supportingInformation;
    }

    public function setSupportingInformation(?FHIRReference ...$value): self
    {
        $this->supportingInformation = array_filter($value);

        return $this;
    }

    public function addSupportingInformation(?FHIRReference ...$value): self
    {
        $this->supportingInformation = array_filter(array_merge($this->supportingInformation, $value));

        return $this;
    }

    public function getStart(): ?FHIRInstant
    {
        return $this->start;
    }

    public function setStart(string|FHIRInstant|null $value): self
    {
        $this->start = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    public function getEnd(): ?FHIRInstant
    {
        return $this->end;
    }

    public function setEnd(string|FHIRInstant|null $value): self
    {
        $this->end = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    public function getMinutesDuration(): ?FHIRPositiveInt
    {
        return $this->minutesDuration;
    }

    public function setMinutesDuration(int|FHIRPositiveInt|null $value): self
    {
        $this->minutesDuration = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSlot(): array
    {
        return $this->slot;
    }

    public function setSlot(?FHIRReference ...$value): self
    {
        $this->slot = array_filter($value);

        return $this;
    }

    public function addSlot(?FHIRReference ...$value): self
    {
        $this->slot = array_filter(array_merge($this->slot, $value));

        return $this;
    }

    public function getCreated(): ?FHIRDateTime
    {
        return $this->created;
    }

    public function setCreated(string|FHIRDateTime|null $value): self
    {
        $this->created = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getComment(): ?FHIRString
    {
        return $this->comment;
    }

    public function setComment(string|FHIRString|null $value): self
    {
        $this->comment = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPatientInstruction(): ?FHIRString
    {
        return $this->patientInstruction;
    }

    public function setPatientInstruction(string|FHIRString|null $value): self
    {
        $this->patientInstruction = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    /**
     * @return FHIRAppointmentParticipant[]
     */
    public function getParticipant(): array
    {
        return $this->participant;
    }

    public function setParticipant(?FHIRAppointmentParticipant ...$value): self
    {
        $this->participant = array_filter($value);

        return $this;
    }

    public function addParticipant(?FHIRAppointmentParticipant ...$value): self
    {
        $this->participant = array_filter(array_merge($this->participant, $value));

        return $this;
    }

    /**
     * @return FHIRPeriod[]
     */
    public function getRequestedPeriod(): array
    {
        return $this->requestedPeriod;
    }

    public function setRequestedPeriod(?FHIRPeriod ...$value): self
    {
        $this->requestedPeriod = array_filter($value);

        return $this;
    }

    public function addRequestedPeriod(?FHIRPeriod ...$value): self
    {
        $this->requestedPeriod = array_filter(array_merge($this->requestedPeriod, $value));

        return $this;
    }
}
