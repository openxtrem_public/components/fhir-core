<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Condition Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRConditionInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAge;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRConditionEvidence;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRConditionStage;

class FHIRCondition extends FHIRDomainResource implements FHIRConditionInterface
{
    public const RESOURCE_NAME = 'Condition';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCodeableConcept $clinicalStatus = null;
    protected ?FHIRCodeableConcept $verificationStatus = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRCodeableConcept $severity = null;
    protected ?FHIRCodeableConcept $code = null;

    /** @var FHIRCodeableConcept[] */
    protected array $bodySite = [];
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $encounter = null;
    protected FHIRDateTime|FHIRAge|FHIRPeriod|FHIRRange|FHIRString|null $onset = null;
    protected FHIRDateTime|FHIRAge|FHIRPeriod|FHIRRange|FHIRString|null $abatement = null;
    protected ?FHIRDateTime $recordedDate = null;
    protected ?FHIRReference $recorder = null;
    protected ?FHIRReference $asserter = null;

    /** @var FHIRConditionStage[] */
    protected array $stage = [];

    /** @var FHIRConditionEvidence[] */
    protected array $evidence = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getClinicalStatus(): ?FHIRCodeableConcept
    {
        return $this->clinicalStatus;
    }

    public function setClinicalStatus(?FHIRCodeableConcept $value): self
    {
        $this->clinicalStatus = $value;

        return $this;
    }

    public function getVerificationStatus(): ?FHIRCodeableConcept
    {
        return $this->verificationStatus;
    }

    public function setVerificationStatus(?FHIRCodeableConcept $value): self
    {
        $this->verificationStatus = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getSeverity(): ?FHIRCodeableConcept
    {
        return $this->severity;
    }

    public function setSeverity(?FHIRCodeableConcept $value): self
    {
        $this->severity = $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getBodySite(): array
    {
        return $this->bodySite;
    }

    public function setBodySite(?FHIRCodeableConcept ...$value): self
    {
        $this->bodySite = array_filter($value);

        return $this;
    }

    public function addBodySite(?FHIRCodeableConcept ...$value): self
    {
        $this->bodySite = array_filter(array_merge($this->bodySite, $value));

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getOnset(): FHIRDateTime|FHIRAge|FHIRPeriod|FHIRRange|FHIRString|null
    {
        return $this->onset;
    }

    public function setOnset(FHIRDateTime|FHIRAge|FHIRPeriod|FHIRRange|FHIRString|null $value): self
    {
        $this->onset = $value;

        return $this;
    }

    public function getAbatement(): FHIRDateTime|FHIRAge|FHIRPeriod|FHIRRange|FHIRString|null
    {
        return $this->abatement;
    }

    public function setAbatement(FHIRDateTime|FHIRAge|FHIRPeriod|FHIRRange|FHIRString|null $value): self
    {
        $this->abatement = $value;

        return $this;
    }

    public function getRecordedDate(): ?FHIRDateTime
    {
        return $this->recordedDate;
    }

    public function setRecordedDate(string|FHIRDateTime|null $value): self
    {
        $this->recordedDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getRecorder(): ?FHIRReference
    {
        return $this->recorder;
    }

    public function setRecorder(?FHIRReference $value): self
    {
        $this->recorder = $value;

        return $this;
    }

    public function getAsserter(): ?FHIRReference
    {
        return $this->asserter;
    }

    public function setAsserter(?FHIRReference $value): self
    {
        $this->asserter = $value;

        return $this;
    }

    /**
     * @return FHIRConditionStage[]
     */
    public function getStage(): array
    {
        return $this->stage;
    }

    public function setStage(?FHIRConditionStage ...$value): self
    {
        $this->stage = array_filter($value);

        return $this;
    }

    public function addStage(?FHIRConditionStage ...$value): self
    {
        $this->stage = array_filter(array_merge($this->stage, $value));

        return $this;
    }

    /**
     * @return FHIRConditionEvidence[]
     */
    public function getEvidence(): array
    {
        return $this->evidence;
    }

    public function setEvidence(?FHIRConditionEvidence ...$value): self
    {
        $this->evidence = array_filter($value);

        return $this;
    }

    public function addEvidence(?FHIRConditionEvidence ...$value): self
    {
        $this->evidence = array_filter(array_merge($this->evidence, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }
}
