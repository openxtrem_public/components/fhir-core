<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Bundle Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRBundleInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRSignature;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUnsignedInt;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRBundleEntry;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRBundleLink;

class FHIRBundle extends FHIRResource implements FHIRBundleInterface
{
    public const RESOURCE_NAME = 'Bundle';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRCode $type = null;
    protected ?FHIRInstant $timestamp = null;
    protected ?FHIRUnsignedInt $total = null;

    /** @var FHIRBundleLink[] */
    protected array $link = [];

    /** @var FHIRBundleEntry[] */
    protected array $entry = [];
    protected ?FHIRSignature $signature = null;

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getTimestamp(): ?FHIRInstant
    {
        return $this->timestamp;
    }

    public function setTimestamp(string|FHIRInstant|null $value): self
    {
        $this->timestamp = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    public function getTotal(): ?FHIRUnsignedInt
    {
        return $this->total;
    }

    public function setTotal(int|FHIRUnsignedInt|null $value): self
    {
        $this->total = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRBundleLink[]
     */
    public function getLink(): array
    {
        return $this->link;
    }

    public function setLink(?FHIRBundleLink ...$value): self
    {
        $this->link = array_filter($value);

        return $this;
    }

    public function addLink(?FHIRBundleLink ...$value): self
    {
        $this->link = array_filter(array_merge($this->link, $value));

        return $this;
    }

    /**
     * @return FHIRBundleEntry[]
     */
    public function getEntry(): array
    {
        return $this->entry;
    }

    public function setEntry(?FHIRBundleEntry ...$value): self
    {
        $this->entry = array_filter($value);

        return $this;
    }

    public function addEntry(?FHIRBundleEntry ...$value): self
    {
        $this->entry = array_filter(array_merge($this->entry, $value));

        return $this;
    }

    public function getSignature(): ?FHIRSignature
    {
        return $this->signature;
    }

    public function setSignature(?FHIRSignature $value): self
    {
        $this->signature = $value;

        return $this;
    }
}
