<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR BiologicallyDerivedProduct Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRBiologicallyDerivedProductInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRBiologicallyDerivedProductCollection;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRBiologicallyDerivedProductManipulation;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRBiologicallyDerivedProductProcessing;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRBiologicallyDerivedProductStorage;

class FHIRBiologicallyDerivedProduct extends FHIRDomainResource implements FHIRBiologicallyDerivedProductInterface
{
    public const RESOURCE_NAME = 'BiologicallyDerivedProduct';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $productCategory = null;
    protected ?FHIRCodeableConcept $productCode = null;
    protected ?FHIRCode $status = null;

    /** @var FHIRReference[] */
    protected array $request = [];
    protected ?FHIRInteger $quantity = null;

    /** @var FHIRReference[] */
    protected array $parent = [];
    protected ?FHIRBiologicallyDerivedProductCollection $collection = null;

    /** @var FHIRBiologicallyDerivedProductProcessing[] */
    protected array $processing = [];
    protected ?FHIRBiologicallyDerivedProductManipulation $manipulation = null;

    /** @var FHIRBiologicallyDerivedProductStorage[] */
    protected array $storage = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getProductCategory(): ?FHIRCode
    {
        return $this->productCategory;
    }

    public function setProductCategory(string|FHIRCode|null $value): self
    {
        $this->productCategory = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getProductCode(): ?FHIRCodeableConcept
    {
        return $this->productCode;
    }

    public function setProductCode(?FHIRCodeableConcept $value): self
    {
        $this->productCode = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getRequest(): array
    {
        return $this->request;
    }

    public function setRequest(?FHIRReference ...$value): self
    {
        $this->request = array_filter($value);

        return $this;
    }

    public function addRequest(?FHIRReference ...$value): self
    {
        $this->request = array_filter(array_merge($this->request, $value));

        return $this;
    }

    public function getQuantity(): ?FHIRInteger
    {
        return $this->quantity;
    }

    public function setQuantity(int|FHIRInteger|null $value): self
    {
        $this->quantity = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getParent(): array
    {
        return $this->parent;
    }

    public function setParent(?FHIRReference ...$value): self
    {
        $this->parent = array_filter($value);

        return $this;
    }

    public function addParent(?FHIRReference ...$value): self
    {
        $this->parent = array_filter(array_merge($this->parent, $value));

        return $this;
    }

    public function getCollection(): ?FHIRBiologicallyDerivedProductCollection
    {
        return $this->collection;
    }

    public function setCollection(?FHIRBiologicallyDerivedProductCollection $value): self
    {
        $this->collection = $value;

        return $this;
    }

    /**
     * @return FHIRBiologicallyDerivedProductProcessing[]
     */
    public function getProcessing(): array
    {
        return $this->processing;
    }

    public function setProcessing(?FHIRBiologicallyDerivedProductProcessing ...$value): self
    {
        $this->processing = array_filter($value);

        return $this;
    }

    public function addProcessing(?FHIRBiologicallyDerivedProductProcessing ...$value): self
    {
        $this->processing = array_filter(array_merge($this->processing, $value));

        return $this;
    }

    public function getManipulation(): ?FHIRBiologicallyDerivedProductManipulation
    {
        return $this->manipulation;
    }

    public function setManipulation(?FHIRBiologicallyDerivedProductManipulation $value): self
    {
        $this->manipulation = $value;

        return $this;
    }

    /**
     * @return FHIRBiologicallyDerivedProductStorage[]
     */
    public function getStorage(): array
    {
        return $this->storage;
    }

    public function setStorage(?FHIRBiologicallyDerivedProductStorage ...$value): self
    {
        $this->storage = array_filter($value);

        return $this;
    }

    public function addStorage(?FHIRBiologicallyDerivedProductStorage ...$value): self
    {
        $this->storage = array_filter(array_merge($this->storage, $value));

        return $this;
    }
}
