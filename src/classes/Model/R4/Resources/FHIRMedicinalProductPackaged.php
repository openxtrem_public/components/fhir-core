<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicinalProductPackaged Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMedicinalProductPackagedInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRMarketingStatus;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicinalProductPackagedBatchIdentifier;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicinalProductPackagedPackageItem;

class FHIRMedicinalProductPackaged extends FHIRDomainResource implements FHIRMedicinalProductPackagedInterface
{
    public const RESOURCE_NAME = 'MedicinalProductPackaged';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRReference[] */
    protected array $subject = [];
    protected ?FHIRString $description = null;
    protected ?FHIRCodeableConcept $legalStatusOfSupply = null;

    /** @var FHIRMarketingStatus[] */
    protected array $marketingStatus = [];
    protected ?FHIRReference $marketingAuthorization = null;

    /** @var FHIRReference[] */
    protected array $manufacturer = [];

    /** @var FHIRMedicinalProductPackagedBatchIdentifier[] */
    protected array $batchIdentifier = [];

    /** @var FHIRMedicinalProductPackagedPackageItem[] */
    protected array $packageItem = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSubject(): array
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference ...$value): self
    {
        $this->subject = array_filter($value);

        return $this;
    }

    public function addSubject(?FHIRReference ...$value): self
    {
        $this->subject = array_filter(array_merge($this->subject, $value));

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getLegalStatusOfSupply(): ?FHIRCodeableConcept
    {
        return $this->legalStatusOfSupply;
    }

    public function setLegalStatusOfSupply(?FHIRCodeableConcept $value): self
    {
        $this->legalStatusOfSupply = $value;

        return $this;
    }

    /**
     * @return FHIRMarketingStatus[]
     */
    public function getMarketingStatus(): array
    {
        return $this->marketingStatus;
    }

    public function setMarketingStatus(?FHIRMarketingStatus ...$value): self
    {
        $this->marketingStatus = array_filter($value);

        return $this;
    }

    public function addMarketingStatus(?FHIRMarketingStatus ...$value): self
    {
        $this->marketingStatus = array_filter(array_merge($this->marketingStatus, $value));

        return $this;
    }

    public function getMarketingAuthorization(): ?FHIRReference
    {
        return $this->marketingAuthorization;
    }

    public function setMarketingAuthorization(?FHIRReference $value): self
    {
        $this->marketingAuthorization = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getManufacturer(): array
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?FHIRReference ...$value): self
    {
        $this->manufacturer = array_filter($value);

        return $this;
    }

    public function addManufacturer(?FHIRReference ...$value): self
    {
        $this->manufacturer = array_filter(array_merge($this->manufacturer, $value));

        return $this;
    }

    /**
     * @return FHIRMedicinalProductPackagedBatchIdentifier[]
     */
    public function getBatchIdentifier(): array
    {
        return $this->batchIdentifier;
    }

    public function setBatchIdentifier(?FHIRMedicinalProductPackagedBatchIdentifier ...$value): self
    {
        $this->batchIdentifier = array_filter($value);

        return $this;
    }

    public function addBatchIdentifier(?FHIRMedicinalProductPackagedBatchIdentifier ...$value): self
    {
        $this->batchIdentifier = array_filter(array_merge($this->batchIdentifier, $value));

        return $this;
    }

    /**
     * @return FHIRMedicinalProductPackagedPackageItem[]
     */
    public function getPackageItem(): array
    {
        return $this->packageItem;
    }

    public function setPackageItem(?FHIRMedicinalProductPackagedPackageItem ...$value): self
    {
        $this->packageItem = array_filter($value);

        return $this;
    }

    public function addPackageItem(?FHIRMedicinalProductPackagedPackageItem ...$value): self
    {
        $this->packageItem = array_filter(array_merge($this->packageItem, $value));

        return $this;
    }
}
