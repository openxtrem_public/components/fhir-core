<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SupplyRequest Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRSupplyRequestInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRSupplyRequestParameter;

class FHIRSupplyRequest extends FHIRDomainResource implements FHIRSupplyRequestInterface
{
    public const RESOURCE_NAME = 'SupplyRequest';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $category = null;
    protected ?FHIRCode $priority = null;
    protected FHIRCodeableConcept|FHIRReference|null $item = null;
    protected ?FHIRQuantity $quantity = null;

    /** @var FHIRSupplyRequestParameter[] */
    protected array $parameter = [];
    protected FHIRDateTime|FHIRPeriod|FHIRTiming|null $occurrence = null;
    protected ?FHIRDateTime $authoredOn = null;
    protected ?FHIRReference $requester = null;

    /** @var FHIRReference[] */
    protected array $supplier = [];

    /** @var FHIRCodeableConcept[] */
    protected array $reasonCode = [];

    /** @var FHIRReference[] */
    protected array $reasonReference = [];
    protected ?FHIRReference $deliverFrom = null;
    protected ?FHIRReference $deliverTo = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getPriority(): ?FHIRCode
    {
        return $this->priority;
    }

    public function setPriority(string|FHIRCode|null $value): self
    {
        $this->priority = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getItem(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->item;
    }

    public function setItem(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->item = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    /**
     * @return FHIRSupplyRequestParameter[]
     */
    public function getParameter(): array
    {
        return $this->parameter;
    }

    public function setParameter(?FHIRSupplyRequestParameter ...$value): self
    {
        $this->parameter = array_filter($value);

        return $this;
    }

    public function addParameter(?FHIRSupplyRequestParameter ...$value): self
    {
        $this->parameter = array_filter(array_merge($this->parameter, $value));

        return $this;
    }

    public function getOccurrence(): FHIRDateTime|FHIRPeriod|FHIRTiming|null
    {
        return $this->occurrence;
    }

    public function setOccurrence(FHIRDateTime|FHIRPeriod|FHIRTiming|null $value): self
    {
        $this->occurrence = $value;

        return $this;
    }

    public function getAuthoredOn(): ?FHIRDateTime
    {
        return $this->authoredOn;
    }

    public function setAuthoredOn(string|FHIRDateTime|null $value): self
    {
        $this->authoredOn = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getRequester(): ?FHIRReference
    {
        return $this->requester;
    }

    public function setRequester(?FHIRReference $value): self
    {
        $this->requester = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupplier(): array
    {
        return $this->supplier;
    }

    public function setSupplier(?FHIRReference ...$value): self
    {
        $this->supplier = array_filter($value);

        return $this;
    }

    public function addSupplier(?FHIRReference ...$value): self
    {
        $this->supplier = array_filter(array_merge($this->supplier, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReasonCode(): array
    {
        return $this->reasonCode;
    }

    public function setReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter($value);

        return $this;
    }

    public function addReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter(array_merge($this->reasonCode, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReasonReference(): array
    {
        return $this->reasonReference;
    }

    public function setReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter($value);

        return $this;
    }

    public function addReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter(array_merge($this->reasonReference, $value));

        return $this;
    }

    public function getDeliverFrom(): ?FHIRReference
    {
        return $this->deliverFrom;
    }

    public function setDeliverFrom(?FHIRReference $value): self
    {
        $this->deliverFrom = $value;

        return $this;
    }

    public function getDeliverTo(): ?FHIRReference
    {
        return $this->deliverTo;
    }

    public function setDeliverTo(?FHIRReference $value): self
    {
        $this->deliverTo = $value;

        return $this;
    }
}
