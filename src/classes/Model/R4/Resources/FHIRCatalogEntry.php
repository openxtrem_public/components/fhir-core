<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CatalogEntry Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRCatalogEntryInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRCatalogEntryRelatedEntry;

class FHIRCatalogEntry extends FHIRDomainResource implements FHIRCatalogEntryInterface
{
    public const RESOURCE_NAME = 'CatalogEntry';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRBoolean $orderable = null;
    protected ?FHIRReference $referencedItem = null;

    /** @var FHIRIdentifier[] */
    protected array $additionalIdentifier = [];

    /** @var FHIRCodeableConcept[] */
    protected array $classification = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRPeriod $validityPeriod = null;
    protected ?FHIRDateTime $validTo = null;
    protected ?FHIRDateTime $lastUpdated = null;

    /** @var FHIRCodeableConcept[] */
    protected array $additionalCharacteristic = [];

    /** @var FHIRCodeableConcept[] */
    protected array $additionalClassification = [];

    /** @var FHIRCatalogEntryRelatedEntry[] */
    protected array $relatedEntry = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getOrderable(): ?FHIRBoolean
    {
        return $this->orderable;
    }

    public function setOrderable(bool|FHIRBoolean|null $value): self
    {
        $this->orderable = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getReferencedItem(): ?FHIRReference
    {
        return $this->referencedItem;
    }

    public function setReferencedItem(?FHIRReference $value): self
    {
        $this->referencedItem = $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getAdditionalIdentifier(): array
    {
        return $this->additionalIdentifier;
    }

    public function setAdditionalIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->additionalIdentifier = array_filter($value);

        return $this;
    }

    public function addAdditionalIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->additionalIdentifier = array_filter(array_merge($this->additionalIdentifier, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getClassification(): array
    {
        return $this->classification;
    }

    public function setClassification(?FHIRCodeableConcept ...$value): self
    {
        $this->classification = array_filter($value);

        return $this;
    }

    public function addClassification(?FHIRCodeableConcept ...$value): self
    {
        $this->classification = array_filter(array_merge($this->classification, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getValidityPeriod(): ?FHIRPeriod
    {
        return $this->validityPeriod;
    }

    public function setValidityPeriod(?FHIRPeriod $value): self
    {
        $this->validityPeriod = $value;

        return $this;
    }

    public function getValidTo(): ?FHIRDateTime
    {
        return $this->validTo;
    }

    public function setValidTo(string|FHIRDateTime|null $value): self
    {
        $this->validTo = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getLastUpdated(): ?FHIRDateTime
    {
        return $this->lastUpdated;
    }

    public function setLastUpdated(string|FHIRDateTime|null $value): self
    {
        $this->lastUpdated = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getAdditionalCharacteristic(): array
    {
        return $this->additionalCharacteristic;
    }

    public function setAdditionalCharacteristic(?FHIRCodeableConcept ...$value): self
    {
        $this->additionalCharacteristic = array_filter($value);

        return $this;
    }

    public function addAdditionalCharacteristic(?FHIRCodeableConcept ...$value): self
    {
        $this->additionalCharacteristic = array_filter(array_merge($this->additionalCharacteristic, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getAdditionalClassification(): array
    {
        return $this->additionalClassification;
    }

    public function setAdditionalClassification(?FHIRCodeableConcept ...$value): self
    {
        $this->additionalClassification = array_filter($value);

        return $this;
    }

    public function addAdditionalClassification(?FHIRCodeableConcept ...$value): self
    {
        $this->additionalClassification = array_filter(array_merge($this->additionalClassification, $value));

        return $this;
    }

    /**
     * @return FHIRCatalogEntryRelatedEntry[]
     */
    public function getRelatedEntry(): array
    {
        return $this->relatedEntry;
    }

    public function setRelatedEntry(?FHIRCatalogEntryRelatedEntry ...$value): self
    {
        $this->relatedEntry = array_filter($value);

        return $this;
    }

    public function addRelatedEntry(?FHIRCatalogEntryRelatedEntry ...$value): self
    {
        $this->relatedEntry = array_filter(array_merge($this->relatedEntry, $value));

        return $this;
    }
}
