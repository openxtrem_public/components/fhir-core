<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Procedure Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRProcedureInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAge;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRProcedureFocalDevice;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRProcedurePerformer;

class FHIRProcedure extends FHIRDomainResource implements FHIRProcedureInterface
{
    public const RESOURCE_NAME = 'Procedure';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRCanonical[] */
    protected array $instantiatesCanonical = [];

    /** @var FHIRUri[] */
    protected array $instantiatesUri = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];

    /** @var FHIRReference[] */
    protected array $partOf = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $statusReason = null;
    protected ?FHIRCodeableConcept $category = null;
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $encounter = null;
    protected FHIRDateTime|FHIRPeriod|FHIRString|FHIRAge|FHIRRange|null $performed = null;
    protected ?FHIRReference $recorder = null;
    protected ?FHIRReference $asserter = null;

    /** @var FHIRProcedurePerformer[] */
    protected array $performer = [];
    protected ?FHIRReference $location = null;

    /** @var FHIRCodeableConcept[] */
    protected array $reasonCode = [];

    /** @var FHIRReference[] */
    protected array $reasonReference = [];

    /** @var FHIRCodeableConcept[] */
    protected array $bodySite = [];
    protected ?FHIRCodeableConcept $outcome = null;

    /** @var FHIRReference[] */
    protected array $report = [];

    /** @var FHIRCodeableConcept[] */
    protected array $complication = [];

    /** @var FHIRReference[] */
    protected array $complicationDetail = [];

    /** @var FHIRCodeableConcept[] */
    protected array $followUp = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRProcedureFocalDevice[] */
    protected array $focalDevice = [];

    /** @var FHIRReference[] */
    protected array $usedReference = [];

    /** @var FHIRCodeableConcept[] */
    protected array $usedCode = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getInstantiatesCanonical(): array
    {
        return $this->instantiatesCanonical;
    }

    public function setInstantiatesCanonical(string|FHIRCanonical|null ...$value): self
    {
        $this->instantiatesCanonical = [];
        $this->addInstantiatesCanonical(...$value);

        return $this;
    }

    public function addInstantiatesCanonical(string|FHIRCanonical|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCanonical())->setValue($v) : $v, $value);

        $this->instantiatesCanonical = array_filter(array_merge($this->instantiatesCanonical, $values));

        return $this;
    }

    /**
     * @return FHIRUri[]
     */
    public function getInstantiatesUri(): array
    {
        return $this->instantiatesUri;
    }

    public function setInstantiatesUri(string|FHIRUri|null ...$value): self
    {
        $this->instantiatesUri = [];
        $this->addInstantiatesUri(...$value);

        return $this;
    }

    public function addInstantiatesUri(string|FHIRUri|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUri())->setValue($v) : $v, $value);

        $this->instantiatesUri = array_filter(array_merge($this->instantiatesUri, $values));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPartOf(): array
    {
        return $this->partOf;
    }

    public function setPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter($value);

        return $this;
    }

    public function addPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter(array_merge($this->partOf, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getStatusReason(): ?FHIRCodeableConcept
    {
        return $this->statusReason;
    }

    public function setStatusReason(?FHIRCodeableConcept $value): self
    {
        $this->statusReason = $value;

        return $this;
    }

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getPerformed(): FHIRDateTime|FHIRPeriod|FHIRString|FHIRAge|FHIRRange|null
    {
        return $this->performed;
    }

    public function setPerformed(FHIRDateTime|FHIRPeriod|FHIRString|FHIRAge|FHIRRange|null $value): self
    {
        $this->performed = $value;

        return $this;
    }

    public function getRecorder(): ?FHIRReference
    {
        return $this->recorder;
    }

    public function setRecorder(?FHIRReference $value): self
    {
        $this->recorder = $value;

        return $this;
    }

    public function getAsserter(): ?FHIRReference
    {
        return $this->asserter;
    }

    public function setAsserter(?FHIRReference $value): self
    {
        $this->asserter = $value;

        return $this;
    }

    /**
     * @return FHIRProcedurePerformer[]
     */
    public function getPerformer(): array
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRProcedurePerformer ...$value): self
    {
        $this->performer = array_filter($value);

        return $this;
    }

    public function addPerformer(?FHIRProcedurePerformer ...$value): self
    {
        $this->performer = array_filter(array_merge($this->performer, $value));

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReasonCode(): array
    {
        return $this->reasonCode;
    }

    public function setReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter($value);

        return $this;
    }

    public function addReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter(array_merge($this->reasonCode, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReasonReference(): array
    {
        return $this->reasonReference;
    }

    public function setReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter($value);

        return $this;
    }

    public function addReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter(array_merge($this->reasonReference, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getBodySite(): array
    {
        return $this->bodySite;
    }

    public function setBodySite(?FHIRCodeableConcept ...$value): self
    {
        $this->bodySite = array_filter($value);

        return $this;
    }

    public function addBodySite(?FHIRCodeableConcept ...$value): self
    {
        $this->bodySite = array_filter(array_merge($this->bodySite, $value));

        return $this;
    }

    public function getOutcome(): ?FHIRCodeableConcept
    {
        return $this->outcome;
    }

    public function setOutcome(?FHIRCodeableConcept $value): self
    {
        $this->outcome = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReport(): array
    {
        return $this->report;
    }

    public function setReport(?FHIRReference ...$value): self
    {
        $this->report = array_filter($value);

        return $this;
    }

    public function addReport(?FHIRReference ...$value): self
    {
        $this->report = array_filter(array_merge($this->report, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getComplication(): array
    {
        return $this->complication;
    }

    public function setComplication(?FHIRCodeableConcept ...$value): self
    {
        $this->complication = array_filter($value);

        return $this;
    }

    public function addComplication(?FHIRCodeableConcept ...$value): self
    {
        $this->complication = array_filter(array_merge($this->complication, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getComplicationDetail(): array
    {
        return $this->complicationDetail;
    }

    public function setComplicationDetail(?FHIRReference ...$value): self
    {
        $this->complicationDetail = array_filter($value);

        return $this;
    }

    public function addComplicationDetail(?FHIRReference ...$value): self
    {
        $this->complicationDetail = array_filter(array_merge($this->complicationDetail, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getFollowUp(): array
    {
        return $this->followUp;
    }

    public function setFollowUp(?FHIRCodeableConcept ...$value): self
    {
        $this->followUp = array_filter($value);

        return $this;
    }

    public function addFollowUp(?FHIRCodeableConcept ...$value): self
    {
        $this->followUp = array_filter(array_merge($this->followUp, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRProcedureFocalDevice[]
     */
    public function getFocalDevice(): array
    {
        return $this->focalDevice;
    }

    public function setFocalDevice(?FHIRProcedureFocalDevice ...$value): self
    {
        $this->focalDevice = array_filter($value);

        return $this;
    }

    public function addFocalDevice(?FHIRProcedureFocalDevice ...$value): self
    {
        $this->focalDevice = array_filter(array_merge($this->focalDevice, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getUsedReference(): array
    {
        return $this->usedReference;
    }

    public function setUsedReference(?FHIRReference ...$value): self
    {
        $this->usedReference = array_filter($value);

        return $this;
    }

    public function addUsedReference(?FHIRReference ...$value): self
    {
        $this->usedReference = array_filter(array_merge($this->usedReference, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getUsedCode(): array
    {
        return $this->usedCode;
    }

    public function setUsedCode(?FHIRCodeableConcept ...$value): self
    {
        $this->usedCode = array_filter($value);

        return $this;
    }

    public function addUsedCode(?FHIRCodeableConcept ...$value): self
    {
        $this->usedCode = array_filter(array_merge($this->usedCode, $value));

        return $this;
    }
}
