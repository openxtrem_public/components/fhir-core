<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationKnowledge Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMedicationKnowledgeInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicationKnowledgeAdministrationGuidelines;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicationKnowledgeCost;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicationKnowledgeDrugCharacteristic;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicationKnowledgeIngredient;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicationKnowledgeKinetics;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicationKnowledgeMedicineClassification;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicationKnowledgeMonitoringProgram;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicationKnowledgeMonograph;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicationKnowledgePackaging;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicationKnowledgeRegulatory;
use Ox\Components\FHIRCore\Model\R4\Resources\Backbone\FHIRMedicationKnowledgeRelatedMedicationKnowledge;

class FHIRMedicationKnowledge extends FHIRDomainResource implements FHIRMedicationKnowledgeInterface
{
    public const RESOURCE_NAME = 'MedicationKnowledge';

    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRReference $manufacturer = null;
    protected ?FHIRCodeableConcept $doseForm = null;
    protected ?FHIRQuantity $amount = null;

    /** @var FHIRString[] */
    protected array $synonym = [];

    /** @var FHIRMedicationKnowledgeRelatedMedicationKnowledge[] */
    protected array $relatedMedicationKnowledge = [];

    /** @var FHIRReference[] */
    protected array $associatedMedication = [];

    /** @var FHIRCodeableConcept[] */
    protected array $productType = [];

    /** @var FHIRMedicationKnowledgeMonograph[] */
    protected array $monograph = [];

    /** @var FHIRMedicationKnowledgeIngredient[] */
    protected array $ingredient = [];
    protected ?FHIRMarkdown $preparationInstruction = null;

    /** @var FHIRCodeableConcept[] */
    protected array $intendedRoute = [];

    /** @var FHIRMedicationKnowledgeCost[] */
    protected array $cost = [];

    /** @var FHIRMedicationKnowledgeMonitoringProgram[] */
    protected array $monitoringProgram = [];

    /** @var FHIRMedicationKnowledgeAdministrationGuidelines[] */
    protected array $administrationGuidelines = [];

    /** @var FHIRMedicationKnowledgeMedicineClassification[] */
    protected array $medicineClassification = [];
    protected ?FHIRMedicationKnowledgePackaging $packaging = null;

    /** @var FHIRMedicationKnowledgeDrugCharacteristic[] */
    protected array $drugCharacteristic = [];

    /** @var FHIRReference[] */
    protected array $contraindication = [];

    /** @var FHIRMedicationKnowledgeRegulatory[] */
    protected array $regulatory = [];

    /** @var FHIRMedicationKnowledgeKinetics[] */
    protected array $kinetics = [];

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getManufacturer(): ?FHIRReference
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?FHIRReference $value): self
    {
        $this->manufacturer = $value;

        return $this;
    }

    public function getDoseForm(): ?FHIRCodeableConcept
    {
        return $this->doseForm;
    }

    public function setDoseForm(?FHIRCodeableConcept $value): self
    {
        $this->doseForm = $value;

        return $this;
    }

    public function getAmount(): ?FHIRQuantity
    {
        return $this->amount;
    }

    public function setAmount(?FHIRQuantity $value): self
    {
        $this->amount = $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getSynonym(): array
    {
        return $this->synonym;
    }

    public function setSynonym(string|FHIRString|null ...$value): self
    {
        $this->synonym = [];
        $this->addSynonym(...$value);

        return $this;
    }

    public function addSynonym(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->synonym = array_filter(array_merge($this->synonym, $values));

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeRelatedMedicationKnowledge[]
     */
    public function getRelatedMedicationKnowledge(): array
    {
        return $this->relatedMedicationKnowledge;
    }

    public function setRelatedMedicationKnowledge(?FHIRMedicationKnowledgeRelatedMedicationKnowledge ...$value): self
    {
        $this->relatedMedicationKnowledge = array_filter($value);

        return $this;
    }

    public function addRelatedMedicationKnowledge(?FHIRMedicationKnowledgeRelatedMedicationKnowledge ...$value): self
    {
        $this->relatedMedicationKnowledge = array_filter(array_merge($this->relatedMedicationKnowledge, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAssociatedMedication(): array
    {
        return $this->associatedMedication;
    }

    public function setAssociatedMedication(?FHIRReference ...$value): self
    {
        $this->associatedMedication = array_filter($value);

        return $this;
    }

    public function addAssociatedMedication(?FHIRReference ...$value): self
    {
        $this->associatedMedication = array_filter(array_merge($this->associatedMedication, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getProductType(): array
    {
        return $this->productType;
    }

    public function setProductType(?FHIRCodeableConcept ...$value): self
    {
        $this->productType = array_filter($value);

        return $this;
    }

    public function addProductType(?FHIRCodeableConcept ...$value): self
    {
        $this->productType = array_filter(array_merge($this->productType, $value));

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeMonograph[]
     */
    public function getMonograph(): array
    {
        return $this->monograph;
    }

    public function setMonograph(?FHIRMedicationKnowledgeMonograph ...$value): self
    {
        $this->monograph = array_filter($value);

        return $this;
    }

    public function addMonograph(?FHIRMedicationKnowledgeMonograph ...$value): self
    {
        $this->monograph = array_filter(array_merge($this->monograph, $value));

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeIngredient[]
     */
    public function getIngredient(): array
    {
        return $this->ingredient;
    }

    public function setIngredient(?FHIRMedicationKnowledgeIngredient ...$value): self
    {
        $this->ingredient = array_filter($value);

        return $this;
    }

    public function addIngredient(?FHIRMedicationKnowledgeIngredient ...$value): self
    {
        $this->ingredient = array_filter(array_merge($this->ingredient, $value));

        return $this;
    }

    public function getPreparationInstruction(): ?FHIRMarkdown
    {
        return $this->preparationInstruction;
    }

    public function setPreparationInstruction(string|FHIRMarkdown|null $value): self
    {
        $this->preparationInstruction = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getIntendedRoute(): array
    {
        return $this->intendedRoute;
    }

    public function setIntendedRoute(?FHIRCodeableConcept ...$value): self
    {
        $this->intendedRoute = array_filter($value);

        return $this;
    }

    public function addIntendedRoute(?FHIRCodeableConcept ...$value): self
    {
        $this->intendedRoute = array_filter(array_merge($this->intendedRoute, $value));

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeCost[]
     */
    public function getCost(): array
    {
        return $this->cost;
    }

    public function setCost(?FHIRMedicationKnowledgeCost ...$value): self
    {
        $this->cost = array_filter($value);

        return $this;
    }

    public function addCost(?FHIRMedicationKnowledgeCost ...$value): self
    {
        $this->cost = array_filter(array_merge($this->cost, $value));

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeMonitoringProgram[]
     */
    public function getMonitoringProgram(): array
    {
        return $this->monitoringProgram;
    }

    public function setMonitoringProgram(?FHIRMedicationKnowledgeMonitoringProgram ...$value): self
    {
        $this->monitoringProgram = array_filter($value);

        return $this;
    }

    public function addMonitoringProgram(?FHIRMedicationKnowledgeMonitoringProgram ...$value): self
    {
        $this->monitoringProgram = array_filter(array_merge($this->monitoringProgram, $value));

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeAdministrationGuidelines[]
     */
    public function getAdministrationGuidelines(): array
    {
        return $this->administrationGuidelines;
    }

    public function setAdministrationGuidelines(?FHIRMedicationKnowledgeAdministrationGuidelines ...$value): self
    {
        $this->administrationGuidelines = array_filter($value);

        return $this;
    }

    public function addAdministrationGuidelines(?FHIRMedicationKnowledgeAdministrationGuidelines ...$value): self
    {
        $this->administrationGuidelines = array_filter(array_merge($this->administrationGuidelines, $value));

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeMedicineClassification[]
     */
    public function getMedicineClassification(): array
    {
        return $this->medicineClassification;
    }

    public function setMedicineClassification(?FHIRMedicationKnowledgeMedicineClassification ...$value): self
    {
        $this->medicineClassification = array_filter($value);

        return $this;
    }

    public function addMedicineClassification(?FHIRMedicationKnowledgeMedicineClassification ...$value): self
    {
        $this->medicineClassification = array_filter(array_merge($this->medicineClassification, $value));

        return $this;
    }

    public function getPackaging(): ?FHIRMedicationKnowledgePackaging
    {
        return $this->packaging;
    }

    public function setPackaging(?FHIRMedicationKnowledgePackaging $value): self
    {
        $this->packaging = $value;

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeDrugCharacteristic[]
     */
    public function getDrugCharacteristic(): array
    {
        return $this->drugCharacteristic;
    }

    public function setDrugCharacteristic(?FHIRMedicationKnowledgeDrugCharacteristic ...$value): self
    {
        $this->drugCharacteristic = array_filter($value);

        return $this;
    }

    public function addDrugCharacteristic(?FHIRMedicationKnowledgeDrugCharacteristic ...$value): self
    {
        $this->drugCharacteristic = array_filter(array_merge($this->drugCharacteristic, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getContraindication(): array
    {
        return $this->contraindication;
    }

    public function setContraindication(?FHIRReference ...$value): self
    {
        $this->contraindication = array_filter($value);

        return $this;
    }

    public function addContraindication(?FHIRReference ...$value): self
    {
        $this->contraindication = array_filter(array_merge($this->contraindication, $value));

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeRegulatory[]
     */
    public function getRegulatory(): array
    {
        return $this->regulatory;
    }

    public function setRegulatory(?FHIRMedicationKnowledgeRegulatory ...$value): self
    {
        $this->regulatory = array_filter($value);

        return $this;
    }

    public function addRegulatory(?FHIRMedicationKnowledgeRegulatory ...$value): self
    {
        $this->regulatory = array_filter(array_merge($this->regulatory, $value));

        return $this;
    }

    /**
     * @return FHIRMedicationKnowledgeKinetics[]
     */
    public function getKinetics(): array
    {
        return $this->kinetics;
    }

    public function setKinetics(?FHIRMedicationKnowledgeKinetics ...$value): self
    {
        $this->kinetics = array_filter($value);

        return $this;
    }

    public function addKinetics(?FHIRMedicationKnowledgeKinetics ...$value): self
    {
        $this->kinetics = array_filter(array_merge($this->kinetics, $value));

        return $this;
    }
}
