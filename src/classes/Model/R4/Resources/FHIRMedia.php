<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Media Resource
 */

namespace Ox\Components\FHIRCore\Model\R4\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMediaInterface;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\R4\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\R4\Datatypes\FHIRString;

class FHIRMedia extends FHIRDomainResource implements FHIRMediaInterface
{
    public const RESOURCE_NAME = 'Media';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];

    /** @var FHIRReference[] */
    protected array $partOf = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableConcept $modality = null;
    protected ?FHIRCodeableConcept $view = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $encounter = null;
    protected FHIRDateTime|FHIRPeriod|null $created = null;
    protected ?FHIRInstant $issued = null;
    protected ?FHIRReference $operator = null;

    /** @var FHIRCodeableConcept[] */
    protected array $reasonCode = [];
    protected ?FHIRCodeableConcept $bodySite = null;
    protected ?FHIRString $deviceName = null;
    protected ?FHIRReference $device = null;
    protected ?FHIRPositiveInt $height = null;
    protected ?FHIRPositiveInt $width = null;
    protected ?FHIRPositiveInt $frames = null;
    protected ?FHIRDecimal $duration = null;
    protected ?FHIRAttachment $content = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPartOf(): array
    {
        return $this->partOf;
    }

    public function setPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter($value);

        return $this;
    }

    public function addPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter(array_merge($this->partOf, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getModality(): ?FHIRCodeableConcept
    {
        return $this->modality;
    }

    public function setModality(?FHIRCodeableConcept $value): self
    {
        $this->modality = $value;

        return $this;
    }

    public function getView(): ?FHIRCodeableConcept
    {
        return $this->view;
    }

    public function setView(?FHIRCodeableConcept $value): self
    {
        $this->view = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getCreated(): FHIRDateTime|FHIRPeriod|null
    {
        return $this->created;
    }

    public function setCreated(FHIRDateTime|FHIRPeriod|null $value): self
    {
        $this->created = $value;

        return $this;
    }

    public function getIssued(): ?FHIRInstant
    {
        return $this->issued;
    }

    public function setIssued(string|FHIRInstant|null $value): self
    {
        $this->issued = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    public function getOperator(): ?FHIRReference
    {
        return $this->operator;
    }

    public function setOperator(?FHIRReference $value): self
    {
        $this->operator = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReasonCode(): array
    {
        return $this->reasonCode;
    }

    public function setReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter($value);

        return $this;
    }

    public function addReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter(array_merge($this->reasonCode, $value));

        return $this;
    }

    public function getBodySite(): ?FHIRCodeableConcept
    {
        return $this->bodySite;
    }

    public function setBodySite(?FHIRCodeableConcept $value): self
    {
        $this->bodySite = $value;

        return $this;
    }

    public function getDeviceName(): ?FHIRString
    {
        return $this->deviceName;
    }

    public function setDeviceName(string|FHIRString|null $value): self
    {
        $this->deviceName = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDevice(): ?FHIRReference
    {
        return $this->device;
    }

    public function setDevice(?FHIRReference $value): self
    {
        $this->device = $value;

        return $this;
    }

    public function getHeight(): ?FHIRPositiveInt
    {
        return $this->height;
    }

    public function setHeight(int|FHIRPositiveInt|null $value): self
    {
        $this->height = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getWidth(): ?FHIRPositiveInt
    {
        return $this->width;
    }

    public function setWidth(int|FHIRPositiveInt|null $value): self
    {
        $this->width = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getFrames(): ?FHIRPositiveInt
    {
        return $this->frames;
    }

    public function setFrames(int|FHIRPositiveInt|null $value): self
    {
        $this->frames = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getDuration(): ?FHIRDecimal
    {
        return $this->duration;
    }

    public function setDuration(float|FHIRDecimal|null $value): self
    {
        $this->duration = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getContent(): ?FHIRAttachment
    {
        return $this->content;
    }

    public function setContent(?FHIRAttachment $value): self
    {
        $this->content = $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }
}
