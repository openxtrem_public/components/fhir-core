<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Account Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRAccountInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRAccountCoverage;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRAccountGuarantor;

class FHIRAccount extends FHIRDomainResource implements FHIRAccountInterface
{
    public const RESOURCE_NAME = 'Account';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRString $name = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRPeriod $period = null;
    protected ?FHIRPeriod $active = null;
    protected ?FHIRMoney $balance = null;

    /** @var FHIRAccountCoverage[] */
    protected array $coverage = [];
    protected ?FHIRReference $owner = null;
    protected ?FHIRString $description = null;

    /** @var FHIRAccountGuarantor[] */
    protected array $guarantor = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    public function getActive(): ?FHIRPeriod
    {
        return $this->active;
    }

    public function setActive(?FHIRPeriod $value): self
    {
        $this->active = $value;

        return $this;
    }

    public function getBalance(): ?FHIRMoney
    {
        return $this->balance;
    }

    public function setBalance(?FHIRMoney $value): self
    {
        $this->balance = $value;

        return $this;
    }

    /**
     * @return FHIRAccountCoverage[]
     */
    public function getCoverage(): array
    {
        return $this->coverage;
    }

    public function setCoverage(?FHIRAccountCoverage ...$value): self
    {
        $this->coverage = array_filter($value);

        return $this;
    }

    public function addCoverage(?FHIRAccountCoverage ...$value): self
    {
        $this->coverage = array_filter(array_merge($this->coverage, $value));

        return $this;
    }

    public function getOwner(): ?FHIRReference
    {
        return $this->owner;
    }

    public function setOwner(?FHIRReference $value): self
    {
        $this->owner = $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAccountGuarantor[]
     */
    public function getGuarantor(): array
    {
        return $this->guarantor;
    }

    public function setGuarantor(?FHIRAccountGuarantor ...$value): self
    {
        $this->guarantor = array_filter($value);

        return $this;
    }

    public function addGuarantor(?FHIRAccountGuarantor ...$value): self
    {
        $this->guarantor = array_filter(array_merge($this->guarantor, $value));

        return $this;
    }
}
