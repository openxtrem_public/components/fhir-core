<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EligibilityResponse Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIREligibilityResponseInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIREligibilityResponseError;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIREligibilityResponseInsurance;

class FHIREligibilityResponse extends FHIRDomainResource implements FHIREligibilityResponseInterface
{
    public const RESOURCE_NAME = 'EligibilityResponse';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRDateTime $created = null;
    protected ?FHIRReference $requestProvider = null;
    protected ?FHIRReference $requestOrganization = null;
    protected ?FHIRReference $request = null;
    protected ?FHIRCodeableConcept $outcome = null;
    protected ?FHIRString $disposition = null;
    protected ?FHIRReference $insurer = null;
    protected ?FHIRBoolean $inforce = null;

    /** @var FHIREligibilityResponseInsurance[] */
    protected array $insurance = [];
    protected ?FHIRCodeableConcept $form = null;

    /** @var FHIREligibilityResponseError[] */
    protected array $error = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCreated(): ?FHIRDateTime
    {
        return $this->created;
    }

    public function setCreated(string|FHIRDateTime|null $value): self
    {
        $this->created = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getRequestProvider(): ?FHIRReference
    {
        return $this->requestProvider;
    }

    public function setRequestProvider(?FHIRReference $value): self
    {
        $this->requestProvider = $value;

        return $this;
    }

    public function getRequestOrganization(): ?FHIRReference
    {
        return $this->requestOrganization;
    }

    public function setRequestOrganization(?FHIRReference $value): self
    {
        $this->requestOrganization = $value;

        return $this;
    }

    public function getRequest(): ?FHIRReference
    {
        return $this->request;
    }

    public function setRequest(?FHIRReference $value): self
    {
        $this->request = $value;

        return $this;
    }

    public function getOutcome(): ?FHIRCodeableConcept
    {
        return $this->outcome;
    }

    public function setOutcome(?FHIRCodeableConcept $value): self
    {
        $this->outcome = $value;

        return $this;
    }

    public function getDisposition(): ?FHIRString
    {
        return $this->disposition;
    }

    public function setDisposition(string|FHIRString|null $value): self
    {
        $this->disposition = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getInsurer(): ?FHIRReference
    {
        return $this->insurer;
    }

    public function setInsurer(?FHIRReference $value): self
    {
        $this->insurer = $value;

        return $this;
    }

    public function getInforce(): ?FHIRBoolean
    {
        return $this->inforce;
    }

    public function setInforce(bool|FHIRBoolean|null $value): self
    {
        $this->inforce = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIREligibilityResponseInsurance[]
     */
    public function getInsurance(): array
    {
        return $this->insurance;
    }

    public function setInsurance(?FHIREligibilityResponseInsurance ...$value): self
    {
        $this->insurance = array_filter($value);

        return $this;
    }

    public function addInsurance(?FHIREligibilityResponseInsurance ...$value): self
    {
        $this->insurance = array_filter(array_merge($this->insurance, $value));

        return $this;
    }

    public function getForm(): ?FHIRCodeableConcept
    {
        return $this->form;
    }

    public function setForm(?FHIRCodeableConcept $value): self
    {
        $this->form = $value;

        return $this;
    }

    /**
     * @return FHIREligibilityResponseError[]
     */
    public function getError(): array
    {
        return $this->error;
    }

    public function setError(?FHIREligibilityResponseError ...$value): self
    {
        $this->error = array_filter($value);

        return $this;
    }

    public function addError(?FHIREligibilityResponseError ...$value): self
    {
        $this->error = array_filter(array_merge($this->error, $value));

        return $this;
    }
}
