<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestReport Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRTestReportInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRTestReportParticipant;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRTestReportSetup;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRTestReportTeardown;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRTestReportTest;

class FHIRTestReport extends FHIRDomainResource implements FHIRTestReportInterface
{
    public const RESOURCE_NAME = 'TestReport';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRString $name = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRReference $testScript = null;
    protected ?FHIRCode $result = null;
    protected ?FHIRDecimal $score = null;
    protected ?FHIRString $tester = null;
    protected ?FHIRDateTime $issued = null;

    /** @var FHIRTestReportParticipant[] */
    protected array $participant = [];
    protected ?FHIRTestReportSetup $setup = null;

    /** @var FHIRTestReportTest[] */
    protected array $test = [];
    protected ?FHIRTestReportTeardown $teardown = null;

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getTestScript(): ?FHIRReference
    {
        return $this->testScript;
    }

    public function setTestScript(?FHIRReference $value): self
    {
        $this->testScript = $value;

        return $this;
    }

    public function getResult(): ?FHIRCode
    {
        return $this->result;
    }

    public function setResult(string|FHIRCode|null $value): self
    {
        $this->result = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getScore(): ?FHIRDecimal
    {
        return $this->score;
    }

    public function setScore(float|FHIRDecimal|null $value): self
    {
        $this->score = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getTester(): ?FHIRString
    {
        return $this->tester;
    }

    public function setTester(string|FHIRString|null $value): self
    {
        $this->tester = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getIssued(): ?FHIRDateTime
    {
        return $this->issued;
    }

    public function setIssued(string|FHIRDateTime|null $value): self
    {
        $this->issued = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRTestReportParticipant[]
     */
    public function getParticipant(): array
    {
        return $this->participant;
    }

    public function setParticipant(?FHIRTestReportParticipant ...$value): self
    {
        $this->participant = array_filter($value);

        return $this;
    }

    public function addParticipant(?FHIRTestReportParticipant ...$value): self
    {
        $this->participant = array_filter(array_merge($this->participant, $value));

        return $this;
    }

    public function getSetup(): ?FHIRTestReportSetup
    {
        return $this->setup;
    }

    public function setSetup(?FHIRTestReportSetup $value): self
    {
        $this->setup = $value;

        return $this;
    }

    /**
     * @return FHIRTestReportTest[]
     */
    public function getTest(): array
    {
        return $this->test;
    }

    public function setTest(?FHIRTestReportTest ...$value): self
    {
        $this->test = array_filter($value);

        return $this;
    }

    public function addTest(?FHIRTestReportTest ...$value): self
    {
        $this->test = array_filter(array_merge($this->test, $value));

        return $this;
    }

    public function getTeardown(): ?FHIRTestReportTeardown
    {
        return $this->teardown;
    }

    public function setTeardown(?FHIRTestReportTeardown $value): self
    {
        $this->teardown = $value;

        return $this;
    }
}
