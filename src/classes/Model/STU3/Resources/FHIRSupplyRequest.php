<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SupplyRequest Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRSupplyRequestInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRSupplyRequestOrderedItem;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRSupplyRequestRequester;

class FHIRSupplyRequest extends FHIRDomainResource implements FHIRSupplyRequestInterface
{
    public const RESOURCE_NAME = 'SupplyRequest';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $category = null;
    protected ?FHIRCode $priority = null;
    protected ?FHIRSupplyRequestOrderedItem $orderedItem = null;
    protected FHIRDateTime|FHIRPeriod|FHIRTiming|null $occurrence = null;
    protected ?FHIRDateTime $authoredOn = null;
    protected ?FHIRSupplyRequestRequester $requester = null;

    /** @var FHIRReference[] */
    protected array $supplier = [];
    protected FHIRCodeableConcept|FHIRReference|null $reason = null;
    protected ?FHIRReference $deliverFrom = null;
    protected ?FHIRReference $deliverTo = null;

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getPriority(): ?FHIRCode
    {
        return $this->priority;
    }

    public function setPriority(string|FHIRCode|null $value): self
    {
        $this->priority = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getOrderedItem(): ?FHIRSupplyRequestOrderedItem
    {
        return $this->orderedItem;
    }

    public function setOrderedItem(?FHIRSupplyRequestOrderedItem $value): self
    {
        $this->orderedItem = $value;

        return $this;
    }

    public function getOccurrence(): FHIRDateTime|FHIRPeriod|FHIRTiming|null
    {
        return $this->occurrence;
    }

    public function setOccurrence(FHIRDateTime|FHIRPeriod|FHIRTiming|null $value): self
    {
        $this->occurrence = $value;

        return $this;
    }

    public function getAuthoredOn(): ?FHIRDateTime
    {
        return $this->authoredOn;
    }

    public function setAuthoredOn(string|FHIRDateTime|null $value): self
    {
        $this->authoredOn = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getRequester(): ?FHIRSupplyRequestRequester
    {
        return $this->requester;
    }

    public function setRequester(?FHIRSupplyRequestRequester $value): self
    {
        $this->requester = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupplier(): array
    {
        return $this->supplier;
    }

    public function setSupplier(?FHIRReference ...$value): self
    {
        $this->supplier = array_filter($value);

        return $this;
    }

    public function addSupplier(?FHIRReference ...$value): self
    {
        $this->supplier = array_filter(array_merge($this->supplier, $value));

        return $this;
    }

    public function getReason(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->reason;
    }

    public function setReason(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->reason = $value;

        return $this;
    }

    public function getDeliverFrom(): ?FHIRReference
    {
        return $this->deliverFrom;
    }

    public function setDeliverFrom(?FHIRReference $value): self
    {
        $this->deliverFrom = $value;

        return $this;
    }

    public function getDeliverTo(): ?FHIRReference
    {
        return $this->deliverTo;
    }

    public function setDeliverTo(?FHIRReference $value): self
    {
        $this->deliverTo = $value;

        return $this;
    }
}
