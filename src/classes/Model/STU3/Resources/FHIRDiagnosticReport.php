<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DiagnosticReport Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRDiagnosticReportInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRDiagnosticReportImage;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRDiagnosticReportPerformer;

class FHIRDiagnosticReport extends FHIRDomainResource implements FHIRDiagnosticReportInterface
{
    public const RESOURCE_NAME = 'DiagnosticReport';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $category = null;
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $context = null;
    protected FHIRDateTime|FHIRPeriod|null $effective = null;
    protected ?FHIRInstant $issued = null;

    /** @var FHIRDiagnosticReportPerformer[] */
    protected array $performer = [];

    /** @var FHIRReference[] */
    protected array $specimen = [];

    /** @var FHIRReference[] */
    protected array $result = [];

    /** @var FHIRReference[] */
    protected array $imagingStudy = [];

    /** @var FHIRDiagnosticReportImage[] */
    protected array $image = [];
    protected ?FHIRString $conclusion = null;

    /** @var FHIRCodeableConcept[] */
    protected array $codedDiagnosis = [];

    /** @var FHIRAttachment[] */
    protected array $presentedForm = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getContext(): ?FHIRReference
    {
        return $this->context;
    }

    public function setContext(?FHIRReference $value): self
    {
        $this->context = $value;

        return $this;
    }

    public function getEffective(): FHIRDateTime|FHIRPeriod|null
    {
        return $this->effective;
    }

    public function setEffective(FHIRDateTime|FHIRPeriod|null $value): self
    {
        $this->effective = $value;

        return $this;
    }

    public function getIssued(): ?FHIRInstant
    {
        return $this->issued;
    }

    public function setIssued(string|FHIRInstant|null $value): self
    {
        $this->issued = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRDiagnosticReportPerformer[]
     */
    public function getPerformer(): array
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRDiagnosticReportPerformer ...$value): self
    {
        $this->performer = array_filter($value);

        return $this;
    }

    public function addPerformer(?FHIRDiagnosticReportPerformer ...$value): self
    {
        $this->performer = array_filter(array_merge($this->performer, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSpecimen(): array
    {
        return $this->specimen;
    }

    public function setSpecimen(?FHIRReference ...$value): self
    {
        $this->specimen = array_filter($value);

        return $this;
    }

    public function addSpecimen(?FHIRReference ...$value): self
    {
        $this->specimen = array_filter(array_merge($this->specimen, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getResult(): array
    {
        return $this->result;
    }

    public function setResult(?FHIRReference ...$value): self
    {
        $this->result = array_filter($value);

        return $this;
    }

    public function addResult(?FHIRReference ...$value): self
    {
        $this->result = array_filter(array_merge($this->result, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getImagingStudy(): array
    {
        return $this->imagingStudy;
    }

    public function setImagingStudy(?FHIRReference ...$value): self
    {
        $this->imagingStudy = array_filter($value);

        return $this;
    }

    public function addImagingStudy(?FHIRReference ...$value): self
    {
        $this->imagingStudy = array_filter(array_merge($this->imagingStudy, $value));

        return $this;
    }

    /**
     * @return FHIRDiagnosticReportImage[]
     */
    public function getImage(): array
    {
        return $this->image;
    }

    public function setImage(?FHIRDiagnosticReportImage ...$value): self
    {
        $this->image = array_filter($value);

        return $this;
    }

    public function addImage(?FHIRDiagnosticReportImage ...$value): self
    {
        $this->image = array_filter(array_merge($this->image, $value));

        return $this;
    }

    public function getConclusion(): ?FHIRString
    {
        return $this->conclusion;
    }

    public function setConclusion(string|FHIRString|null $value): self
    {
        $this->conclusion = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCodedDiagnosis(): array
    {
        return $this->codedDiagnosis;
    }

    public function setCodedDiagnosis(?FHIRCodeableConcept ...$value): self
    {
        $this->codedDiagnosis = array_filter($value);

        return $this;
    }

    public function addCodedDiagnosis(?FHIRCodeableConcept ...$value): self
    {
        $this->codedDiagnosis = array_filter(array_merge($this->codedDiagnosis, $value));

        return $this;
    }

    /**
     * @return FHIRAttachment[]
     */
    public function getPresentedForm(): array
    {
        return $this->presentedForm;
    }

    public function setPresentedForm(?FHIRAttachment ...$value): self
    {
        $this->presentedForm = array_filter($value);

        return $this;
    }

    public function addPresentedForm(?FHIRAttachment ...$value): self
    {
        $this->presentedForm = array_filter(array_merge($this->presentedForm, $value));

        return $this;
    }
}
