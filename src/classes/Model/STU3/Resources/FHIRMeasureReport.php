<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MeasureReport Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMeasureReportInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRMeasureReportGroup;

class FHIRMeasureReport extends FHIRDomainResource implements FHIRMeasureReportInterface
{
    public const RESOURCE_NAME = 'MeasureReport';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRCode $type = null;
    protected ?FHIRReference $measure = null;
    protected ?FHIRReference $patient = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRReference $reportingOrganization = null;
    protected ?FHIRPeriod $period = null;

    /** @var FHIRMeasureReportGroup[] */
    protected array $group = [];
    protected ?FHIRReference $evaluatedResources = null;

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getMeasure(): ?FHIRReference
    {
        return $this->measure;
    }

    public function setMeasure(?FHIRReference $value): self
    {
        $this->measure = $value;

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getReportingOrganization(): ?FHIRReference
    {
        return $this->reportingOrganization;
    }

    public function setReportingOrganization(?FHIRReference $value): self
    {
        $this->reportingOrganization = $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    /**
     * @return FHIRMeasureReportGroup[]
     */
    public function getGroup(): array
    {
        return $this->group;
    }

    public function setGroup(?FHIRMeasureReportGroup ...$value): self
    {
        $this->group = array_filter($value);

        return $this;
    }

    public function addGroup(?FHIRMeasureReportGroup ...$value): self
    {
        $this->group = array_filter(array_merge($this->group, $value));

        return $this;
    }

    public function getEvaluatedResources(): ?FHIRReference
    {
        return $this->evaluatedResources;
    }

    public function setEvaluatedResources(?FHIRReference $value): self
    {
        $this->evaluatedResources = $value;

        return $this;
    }
}
