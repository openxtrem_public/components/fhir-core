<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImmunizationRecommendation Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRImmunizationRecommendationInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRImmunizationRecommendationRecommendation;

class FHIRImmunizationRecommendation extends FHIRDomainResource implements FHIRImmunizationRecommendationInterface
{
    public const RESOURCE_NAME = 'ImmunizationRecommendation';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRReference $patient = null;

    /** @var FHIRImmunizationRecommendationRecommendation[] */
    protected array $recommendation = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    /**
     * @return FHIRImmunizationRecommendationRecommendation[]
     */
    public function getRecommendation(): array
    {
        return $this->recommendation;
    }

    public function setRecommendation(?FHIRImmunizationRecommendationRecommendation ...$value): self
    {
        $this->recommendation = array_filter($value);

        return $this;
    }

    public function addRecommendation(?FHIRImmunizationRecommendationRecommendation ...$value): self
    {
        $this->recommendation = array_filter(array_merge($this->recommendation, $value));

        return $this;
    }
}
