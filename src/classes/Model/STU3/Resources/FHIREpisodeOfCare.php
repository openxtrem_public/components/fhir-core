<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EpisodeOfCare Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIREpisodeOfCareInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIREpisodeOfCareDiagnosis;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIREpisodeOfCareStatusHistory;

class FHIREpisodeOfCare extends FHIRDomainResource implements FHIREpisodeOfCareInterface
{
    public const RESOURCE_NAME = 'EpisodeOfCare';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;

    /** @var FHIREpisodeOfCareStatusHistory[] */
    protected array $statusHistory = [];

    /** @var FHIRCodeableConcept[] */
    protected array $type = [];

    /** @var FHIREpisodeOfCareDiagnosis[] */
    protected array $diagnosis = [];
    protected ?FHIRReference $patient = null;
    protected ?FHIRReference $managingOrganization = null;
    protected ?FHIRPeriod $period = null;

    /** @var FHIRReference[] */
    protected array $referralRequest = [];
    protected ?FHIRReference $careManager = null;

    /** @var FHIRReference[] */
    protected array $team = [];

    /** @var FHIRReference[] */
    protected array $account = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIREpisodeOfCareStatusHistory[]
     */
    public function getStatusHistory(): array
    {
        return $this->statusHistory;
    }

    public function setStatusHistory(?FHIREpisodeOfCareStatusHistory ...$value): self
    {
        $this->statusHistory = array_filter($value);

        return $this;
    }

    public function addStatusHistory(?FHIREpisodeOfCareStatusHistory ...$value): self
    {
        $this->statusHistory = array_filter(array_merge($this->statusHistory, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }

    /**
     * @return FHIREpisodeOfCareDiagnosis[]
     */
    public function getDiagnosis(): array
    {
        return $this->diagnosis;
    }

    public function setDiagnosis(?FHIREpisodeOfCareDiagnosis ...$value): self
    {
        $this->diagnosis = array_filter($value);

        return $this;
    }

    public function addDiagnosis(?FHIREpisodeOfCareDiagnosis ...$value): self
    {
        $this->diagnosis = array_filter(array_merge($this->diagnosis, $value));

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getManagingOrganization(): ?FHIRReference
    {
        return $this->managingOrganization;
    }

    public function setManagingOrganization(?FHIRReference $value): self
    {
        $this->managingOrganization = $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReferralRequest(): array
    {
        return $this->referralRequest;
    }

    public function setReferralRequest(?FHIRReference ...$value): self
    {
        $this->referralRequest = array_filter($value);

        return $this;
    }

    public function addReferralRequest(?FHIRReference ...$value): self
    {
        $this->referralRequest = array_filter(array_merge($this->referralRequest, $value));

        return $this;
    }

    public function getCareManager(): ?FHIRReference
    {
        return $this->careManager;
    }

    public function setCareManager(?FHIRReference $value): self
    {
        $this->careManager = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getTeam(): array
    {
        return $this->team;
    }

    public function setTeam(?FHIRReference ...$value): self
    {
        $this->team = array_filter($value);

        return $this;
    }

    public function addTeam(?FHIRReference ...$value): self
    {
        $this->team = array_filter(array_merge($this->team, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAccount(): array
    {
        return $this->account;
    }

    public function setAccount(?FHIRReference ...$value): self
    {
        $this->account = array_filter($value);

        return $this;
    }

    public function addAccount(?FHIRReference ...$value): self
    {
        $this->account = array_filter(array_merge($this->account, $value));

        return $this;
    }
}
