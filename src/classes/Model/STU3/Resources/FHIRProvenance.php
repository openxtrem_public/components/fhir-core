<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Provenance Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRProvenanceInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRSignature;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRProvenanceAgent;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRProvenanceEntity;

class FHIRProvenance extends FHIRDomainResource implements FHIRProvenanceInterface
{
    public const RESOURCE_NAME = 'Provenance';

    /** @var FHIRReference[] */
    protected array $target = [];
    protected ?FHIRPeriod $period = null;
    protected ?FHIRInstant $recorded = null;

    /** @var FHIRUri[] */
    protected array $policy = [];
    protected ?FHIRReference $location = null;

    /** @var FHIRCoding[] */
    protected array $reason = [];
    protected ?FHIRCoding $activity = null;

    /** @var FHIRProvenanceAgent[] */
    protected array $agent = [];

    /** @var FHIRProvenanceEntity[] */
    protected array $entity = [];

    /** @var FHIRSignature[] */
    protected array $signature = [];

    /**
     * @return FHIRReference[]
     */
    public function getTarget(): array
    {
        return $this->target;
    }

    public function setTarget(?FHIRReference ...$value): self
    {
        $this->target = array_filter($value);

        return $this;
    }

    public function addTarget(?FHIRReference ...$value): self
    {
        $this->target = array_filter(array_merge($this->target, $value));

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    public function getRecorded(): ?FHIRInstant
    {
        return $this->recorded;
    }

    public function setRecorded(string|FHIRInstant|null $value): self
    {
        $this->recorded = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRUri[]
     */
    public function getPolicy(): array
    {
        return $this->policy;
    }

    public function setPolicy(string|FHIRUri|null ...$value): self
    {
        $this->policy = [];
        $this->addPolicy(...$value);

        return $this;
    }

    public function addPolicy(string|FHIRUri|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUri())->setValue($v) : $v, $value);

        $this->policy = array_filter(array_merge($this->policy, $values));

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getReason(): array
    {
        return $this->reason;
    }

    public function setReason(?FHIRCoding ...$value): self
    {
        $this->reason = array_filter($value);

        return $this;
    }

    public function addReason(?FHIRCoding ...$value): self
    {
        $this->reason = array_filter(array_merge($this->reason, $value));

        return $this;
    }

    public function getActivity(): ?FHIRCoding
    {
        return $this->activity;
    }

    public function setActivity(?FHIRCoding $value): self
    {
        $this->activity = $value;

        return $this;
    }

    /**
     * @return FHIRProvenanceAgent[]
     */
    public function getAgent(): array
    {
        return $this->agent;
    }

    public function setAgent(?FHIRProvenanceAgent ...$value): self
    {
        $this->agent = array_filter($value);

        return $this;
    }

    public function addAgent(?FHIRProvenanceAgent ...$value): self
    {
        $this->agent = array_filter(array_merge($this->agent, $value));

        return $this;
    }

    /**
     * @return FHIRProvenanceEntity[]
     */
    public function getEntity(): array
    {
        return $this->entity;
    }

    public function setEntity(?FHIRProvenanceEntity ...$value): self
    {
        $this->entity = array_filter($value);

        return $this;
    }

    public function addEntity(?FHIRProvenanceEntity ...$value): self
    {
        $this->entity = array_filter(array_merge($this->entity, $value));

        return $this;
    }

    /**
     * @return FHIRSignature[]
     */
    public function getSignature(): array
    {
        return $this->signature;
    }

    public function setSignature(?FHIRSignature ...$value): self
    {
        $this->signature = array_filter($value);

        return $this;
    }

    public function addSignature(?FHIRSignature ...$value): self
    {
        $this->signature = array_filter(array_merge($this->signature, $value));

        return $this;
    }
}
