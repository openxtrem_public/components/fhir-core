<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Medication Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMedicationInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRMedicationIngredient;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRMedicationPackage;

class FHIRMedication extends FHIRDomainResource implements FHIRMedicationInterface
{
    public const RESOURCE_NAME = 'Medication';

    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRBoolean $isBrand = null;
    protected ?FHIRBoolean $isOverTheCounter = null;
    protected ?FHIRReference $manufacturer = null;
    protected ?FHIRCodeableConcept $form = null;

    /** @var FHIRMedicationIngredient[] */
    protected array $ingredient = [];
    protected ?FHIRMedicationPackage $package = null;

    /** @var FHIRAttachment[] */
    protected array $image = [];

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getIsBrand(): ?FHIRBoolean
    {
        return $this->isBrand;
    }

    public function setIsBrand(bool|FHIRBoolean|null $value): self
    {
        $this->isBrand = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getIsOverTheCounter(): ?FHIRBoolean
    {
        return $this->isOverTheCounter;
    }

    public function setIsOverTheCounter(bool|FHIRBoolean|null $value): self
    {
        $this->isOverTheCounter = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getManufacturer(): ?FHIRReference
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?FHIRReference $value): self
    {
        $this->manufacturer = $value;

        return $this;
    }

    public function getForm(): ?FHIRCodeableConcept
    {
        return $this->form;
    }

    public function setForm(?FHIRCodeableConcept $value): self
    {
        $this->form = $value;

        return $this;
    }

    /**
     * @return FHIRMedicationIngredient[]
     */
    public function getIngredient(): array
    {
        return $this->ingredient;
    }

    public function setIngredient(?FHIRMedicationIngredient ...$value): self
    {
        $this->ingredient = array_filter($value);

        return $this;
    }

    public function addIngredient(?FHIRMedicationIngredient ...$value): self
    {
        $this->ingredient = array_filter(array_merge($this->ingredient, $value));

        return $this;
    }

    public function getPackage(): ?FHIRMedicationPackage
    {
        return $this->package;
    }

    public function setPackage(?FHIRMedicationPackage $value): self
    {
        $this->package = $value;

        return $this;
    }

    /**
     * @return FHIRAttachment[]
     */
    public function getImage(): array
    {
        return $this->image;
    }

    public function setImage(?FHIRAttachment ...$value): self
    {
        $this->image = array_filter($value);

        return $this;
    }

    public function addImage(?FHIRAttachment ...$value): self
    {
        $this->image = array_filter(array_merge($this->image, $value));

        return $this;
    }
}
