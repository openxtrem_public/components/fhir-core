<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Task Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRTaskInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRTaskInput;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRTaskOutput;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRTaskRequester;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRTaskRestriction;

class FHIRTask extends FHIRDomainResource implements FHIRTaskInterface
{
    public const RESOURCE_NAME = 'Task';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected FHIRUri|FHIRReference|null $definition = null;

    /** @var FHIRReference[] */
    protected array $basedOn = [];
    protected ?FHIRIdentifier $groupIdentifier = null;

    /** @var FHIRReference[] */
    protected array $partOf = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $statusReason = null;
    protected ?FHIRCodeableConcept $businessStatus = null;
    protected ?FHIRCode $intent = null;
    protected ?FHIRCode $priority = null;
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRString $description = null;
    protected ?FHIRReference $focus = null;
    protected ?FHIRReference $for = null;
    protected ?FHIRReference $context = null;
    protected ?FHIRPeriod $executionPeriod = null;
    protected ?FHIRDateTime $authoredOn = null;
    protected ?FHIRDateTime $lastModified = null;
    protected ?FHIRTaskRequester $requester = null;

    /** @var FHIRCodeableConcept[] */
    protected array $performerType = [];
    protected ?FHIRReference $owner = null;
    protected ?FHIRCodeableConcept $reason = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRReference[] */
    protected array $relevantHistory = [];
    protected ?FHIRTaskRestriction $restriction = null;

    /** @var FHIRTaskInput[] */
    protected array $input = [];

    /** @var FHIRTaskOutput[] */
    protected array $output = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getDefinition(): FHIRUri|FHIRReference|null
    {
        return $this->definition;
    }

    public function setDefinition(FHIRUri|FHIRReference|null $value): self
    {
        $this->definition = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    public function getGroupIdentifier(): ?FHIRIdentifier
    {
        return $this->groupIdentifier;
    }

    public function setGroupIdentifier(?FHIRIdentifier $value): self
    {
        $this->groupIdentifier = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPartOf(): array
    {
        return $this->partOf;
    }

    public function setPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter($value);

        return $this;
    }

    public function addPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter(array_merge($this->partOf, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getStatusReason(): ?FHIRCodeableConcept
    {
        return $this->statusReason;
    }

    public function setStatusReason(?FHIRCodeableConcept $value): self
    {
        $this->statusReason = $value;

        return $this;
    }

    public function getBusinessStatus(): ?FHIRCodeableConcept
    {
        return $this->businessStatus;
    }

    public function setBusinessStatus(?FHIRCodeableConcept $value): self
    {
        $this->businessStatus = $value;

        return $this;
    }

    public function getIntent(): ?FHIRCode
    {
        return $this->intent;
    }

    public function setIntent(string|FHIRCode|null $value): self
    {
        $this->intent = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getPriority(): ?FHIRCode
    {
        return $this->priority;
    }

    public function setPriority(string|FHIRCode|null $value): self
    {
        $this->priority = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getFocus(): ?FHIRReference
    {
        return $this->focus;
    }

    public function setFocus(?FHIRReference $value): self
    {
        $this->focus = $value;

        return $this;
    }

    public function getFor(): ?FHIRReference
    {
        return $this->for;
    }

    public function setFor(?FHIRReference $value): self
    {
        $this->for = $value;

        return $this;
    }

    public function getContext(): ?FHIRReference
    {
        return $this->context;
    }

    public function setContext(?FHIRReference $value): self
    {
        $this->context = $value;

        return $this;
    }

    public function getExecutionPeriod(): ?FHIRPeriod
    {
        return $this->executionPeriod;
    }

    public function setExecutionPeriod(?FHIRPeriod $value): self
    {
        $this->executionPeriod = $value;

        return $this;
    }

    public function getAuthoredOn(): ?FHIRDateTime
    {
        return $this->authoredOn;
    }

    public function setAuthoredOn(string|FHIRDateTime|null $value): self
    {
        $this->authoredOn = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getLastModified(): ?FHIRDateTime
    {
        return $this->lastModified;
    }

    public function setLastModified(string|FHIRDateTime|null $value): self
    {
        $this->lastModified = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getRequester(): ?FHIRTaskRequester
    {
        return $this->requester;
    }

    public function setRequester(?FHIRTaskRequester $value): self
    {
        $this->requester = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getPerformerType(): array
    {
        return $this->performerType;
    }

    public function setPerformerType(?FHIRCodeableConcept ...$value): self
    {
        $this->performerType = array_filter($value);

        return $this;
    }

    public function addPerformerType(?FHIRCodeableConcept ...$value): self
    {
        $this->performerType = array_filter(array_merge($this->performerType, $value));

        return $this;
    }

    public function getOwner(): ?FHIRReference
    {
        return $this->owner;
    }

    public function setOwner(?FHIRReference $value): self
    {
        $this->owner = $value;

        return $this;
    }

    public function getReason(): ?FHIRCodeableConcept
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableConcept $value): self
    {
        $this->reason = $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getRelevantHistory(): array
    {
        return $this->relevantHistory;
    }

    public function setRelevantHistory(?FHIRReference ...$value): self
    {
        $this->relevantHistory = array_filter($value);

        return $this;
    }

    public function addRelevantHistory(?FHIRReference ...$value): self
    {
        $this->relevantHistory = array_filter(array_merge($this->relevantHistory, $value));

        return $this;
    }

    public function getRestriction(): ?FHIRTaskRestriction
    {
        return $this->restriction;
    }

    public function setRestriction(?FHIRTaskRestriction $value): self
    {
        $this->restriction = $value;

        return $this;
    }

    /**
     * @return FHIRTaskInput[]
     */
    public function getInput(): array
    {
        return $this->input;
    }

    public function setInput(?FHIRTaskInput ...$value): self
    {
        $this->input = array_filter($value);

        return $this;
    }

    public function addInput(?FHIRTaskInput ...$value): self
    {
        $this->input = array_filter(array_merge($this->input, $value));

        return $this;
    }

    /**
     * @return FHIRTaskOutput[]
     */
    public function getOutput(): array
    {
        return $this->output;
    }

    public function setOutput(?FHIRTaskOutput ...$value): self
    {
        $this->output = array_filter($value);

        return $this;
    }

    public function addOutput(?FHIRTaskOutput ...$value): self
    {
        $this->output = array_filter(array_merge($this->output, $value));

        return $this;
    }
}
