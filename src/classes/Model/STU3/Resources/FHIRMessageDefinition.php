<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MessageDefinition Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMessageDefinitionInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRMessageDefinitionAllowedResponse;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRMessageDefinitionFocus;

class FHIRMessageDefinition extends FHIRDomainResource implements FHIRMessageDefinitionInterface
{
    public const RESOURCE_NAME = 'MessageDefinition';

    protected ?FHIRUri $url = null;
    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRString $version = null;
    protected ?FHIRString $name = null;
    protected ?FHIRString $title = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRBoolean $experimental = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRString $publisher = null;

    /** @var FHIRContactDetail[] */
    protected array $contact = [];
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRUsageContext[] */
    protected array $useContext = [];

    /** @var FHIRCodeableConcept[] */
    protected array $jurisdiction = [];
    protected ?FHIRMarkdown $purpose = null;
    protected ?FHIRMarkdown $copyright = null;
    protected ?FHIRReference $base = null;

    /** @var FHIRReference[] */
    protected array $parent = [];

    /** @var FHIRReference[] */
    protected array $replaces = [];
    protected ?FHIRCoding $event = null;
    protected ?FHIRCode $category = null;

    /** @var FHIRMessageDefinitionFocus[] */
    protected array $focus = [];
    protected ?FHIRBoolean $responseRequired = null;

    /** @var FHIRMessageDefinitionAllowedResponse[] */
    protected array $allowedResponse = [];

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getExperimental(): ?FHIRBoolean
    {
        return $this->experimental;
    }

    public function setExperimental(bool|FHIRBoolean|null $value): self
    {
        $this->experimental = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPublisher(): ?FHIRString
    {
        return $this->publisher;
    }

    public function setPublisher(string|FHIRString|null $value): self
    {
        $this->publisher = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUseContext(): array
    {
        return $this->useContext;
    }

    public function setUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter($value);

        return $this;
    }

    public function addUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter(array_merge($this->useContext, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getJurisdiction(): array
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter($value);

        return $this;
    }

    public function addJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter(array_merge($this->jurisdiction, $value));

        return $this;
    }

    public function getPurpose(): ?FHIRMarkdown
    {
        return $this->purpose;
    }

    public function setPurpose(string|FHIRMarkdown|null $value): self
    {
        $this->purpose = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getCopyright(): ?FHIRMarkdown
    {
        return $this->copyright;
    }

    public function setCopyright(string|FHIRMarkdown|null $value): self
    {
        $this->copyright = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getBase(): ?FHIRReference
    {
        return $this->base;
    }

    public function setBase(?FHIRReference $value): self
    {
        $this->base = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getParent(): array
    {
        return $this->parent;
    }

    public function setParent(?FHIRReference ...$value): self
    {
        $this->parent = array_filter($value);

        return $this;
    }

    public function addParent(?FHIRReference ...$value): self
    {
        $this->parent = array_filter(array_merge($this->parent, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReplaces(): array
    {
        return $this->replaces;
    }

    public function setReplaces(?FHIRReference ...$value): self
    {
        $this->replaces = array_filter($value);

        return $this;
    }

    public function addReplaces(?FHIRReference ...$value): self
    {
        $this->replaces = array_filter(array_merge($this->replaces, $value));

        return $this;
    }

    public function getEvent(): ?FHIRCoding
    {
        return $this->event;
    }

    public function setEvent(?FHIRCoding $value): self
    {
        $this->event = $value;

        return $this;
    }

    public function getCategory(): ?FHIRCode
    {
        return $this->category;
    }

    public function setCategory(string|FHIRCode|null $value): self
    {
        $this->category = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRMessageDefinitionFocus[]
     */
    public function getFocus(): array
    {
        return $this->focus;
    }

    public function setFocus(?FHIRMessageDefinitionFocus ...$value): self
    {
        $this->focus = array_filter($value);

        return $this;
    }

    public function addFocus(?FHIRMessageDefinitionFocus ...$value): self
    {
        $this->focus = array_filter(array_merge($this->focus, $value));

        return $this;
    }

    public function getResponseRequired(): ?FHIRBoolean
    {
        return $this->responseRequired;
    }

    public function setResponseRequired(bool|FHIRBoolean|null $value): self
    {
        $this->responseRequired = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRMessageDefinitionAllowedResponse[]
     */
    public function getAllowedResponse(): array
    {
        return $this->allowedResponse;
    }

    public function setAllowedResponse(?FHIRMessageDefinitionAllowedResponse ...$value): self
    {
        $this->allowedResponse = array_filter($value);

        return $this;
    }

    public function addAllowedResponse(?FHIRMessageDefinitionAllowedResponse ...$value): self
    {
        $this->allowedResponse = array_filter(array_merge($this->allowedResponse, $value));

        return $this;
    }
}
