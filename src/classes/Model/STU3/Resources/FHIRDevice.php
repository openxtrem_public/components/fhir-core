<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Device Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRDeviceInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRDeviceUdi;

class FHIRDevice extends FHIRDomainResource implements FHIRDeviceInterface
{
    public const RESOURCE_NAME = 'Device';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRDeviceUdi $udi = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRString $lotNumber = null;
    protected ?FHIRString $manufacturer = null;
    protected ?FHIRDateTime $manufactureDate = null;
    protected ?FHIRDateTime $expirationDate = null;
    protected ?FHIRString $model = null;
    protected ?FHIRString $version = null;
    protected ?FHIRReference $patient = null;
    protected ?FHIRReference $owner = null;

    /** @var FHIRContactPoint[] */
    protected array $contact = [];
    protected ?FHIRReference $location = null;
    protected ?FHIRUri $url = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRCodeableConcept[] */
    protected array $safety = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getUdi(): ?FHIRDeviceUdi
    {
        return $this->udi;
    }

    public function setUdi(?FHIRDeviceUdi $value): self
    {
        $this->udi = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getLotNumber(): ?FHIRString
    {
        return $this->lotNumber;
    }

    public function setLotNumber(string|FHIRString|null $value): self
    {
        $this->lotNumber = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getManufacturer(): ?FHIRString
    {
        return $this->manufacturer;
    }

    public function setManufacturer(string|FHIRString|null $value): self
    {
        $this->manufacturer = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getManufactureDate(): ?FHIRDateTime
    {
        return $this->manufactureDate;
    }

    public function setManufactureDate(string|FHIRDateTime|null $value): self
    {
        $this->manufactureDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getExpirationDate(): ?FHIRDateTime
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(string|FHIRDateTime|null $value): self
    {
        $this->expirationDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getModel(): ?FHIRString
    {
        return $this->model;
    }

    public function setModel(string|FHIRString|null $value): self
    {
        $this->model = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getOwner(): ?FHIRReference
    {
        return $this->owner;
    }

    public function setOwner(?FHIRReference $value): self
    {
        $this->owner = $value;

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactPoint ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactPoint ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSafety(): array
    {
        return $this->safety;
    }

    public function setSafety(?FHIRCodeableConcept ...$value): self
    {
        $this->safety = array_filter($value);

        return $this;
    }

    public function addSafety(?FHIRCodeableConcept ...$value): self
    {
        $this->safety = array_filter(array_merge($this->safety, $value));

        return $this;
    }
}
