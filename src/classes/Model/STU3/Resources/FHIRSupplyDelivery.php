<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SupplyDelivery Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRSupplyDeliveryInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRSupplyDeliverySuppliedItem;

class FHIRSupplyDelivery extends FHIRDomainResource implements FHIRSupplyDeliveryInterface
{
    public const RESOURCE_NAME = 'SupplyDelivery';

    protected ?FHIRIdentifier $identifier = null;

    /** @var FHIRReference[] */
    protected array $basedOn = [];

    /** @var FHIRReference[] */
    protected array $partOf = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRReference $patient = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRSupplyDeliverySuppliedItem $suppliedItem = null;
    protected FHIRDateTime|FHIRPeriod|FHIRTiming|null $occurrence = null;
    protected ?FHIRReference $supplier = null;
    protected ?FHIRReference $destination = null;

    /** @var FHIRReference[] */
    protected array $receiver = [];

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPartOf(): array
    {
        return $this->partOf;
    }

    public function setPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter($value);

        return $this;
    }

    public function addPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter(array_merge($this->partOf, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getSuppliedItem(): ?FHIRSupplyDeliverySuppliedItem
    {
        return $this->suppliedItem;
    }

    public function setSuppliedItem(?FHIRSupplyDeliverySuppliedItem $value): self
    {
        $this->suppliedItem = $value;

        return $this;
    }

    public function getOccurrence(): FHIRDateTime|FHIRPeriod|FHIRTiming|null
    {
        return $this->occurrence;
    }

    public function setOccurrence(FHIRDateTime|FHIRPeriod|FHIRTiming|null $value): self
    {
        $this->occurrence = $value;

        return $this;
    }

    public function getSupplier(): ?FHIRReference
    {
        return $this->supplier;
    }

    public function setSupplier(?FHIRReference $value): self
    {
        $this->supplier = $value;

        return $this;
    }

    public function getDestination(): ?FHIRReference
    {
        return $this->destination;
    }

    public function setDestination(?FHIRReference $value): self
    {
        $this->destination = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReceiver(): array
    {
        return $this->receiver;
    }

    public function setReceiver(?FHIRReference ...$value): self
    {
        $this->receiver = array_filter($value);

        return $this;
    }

    public function addReceiver(?FHIRReference ...$value): self
    {
        $this->receiver = array_filter(array_merge($this->receiver, $value));

        return $this;
    }
}
