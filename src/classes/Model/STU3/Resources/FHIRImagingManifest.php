<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImagingManifest Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRImagingManifestInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRImagingManifestStudy;

class FHIRImagingManifest extends FHIRDomainResource implements FHIRImagingManifestInterface
{
    public const RESOURCE_NAME = 'ImagingManifest';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRReference $patient = null;
    protected ?FHIRDateTime $authoringTime = null;
    protected ?FHIRReference $author = null;
    protected ?FHIRString $description = null;

    /** @var FHIRImagingManifestStudy[] */
    protected array $study = [];

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getAuthoringTime(): ?FHIRDateTime
    {
        return $this->authoringTime;
    }

    public function setAuthoringTime(string|FHIRDateTime|null $value): self
    {
        $this->authoringTime = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getAuthor(): ?FHIRReference
    {
        return $this->author;
    }

    public function setAuthor(?FHIRReference $value): self
    {
        $this->author = $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRImagingManifestStudy[]
     */
    public function getStudy(): array
    {
        return $this->study;
    }

    public function setStudy(?FHIRImagingManifestStudy ...$value): self
    {
        $this->study = array_filter($value);

        return $this;
    }

    public function addStudy(?FHIRImagingManifestStudy ...$value): self
    {
        $this->study = array_filter(array_merge($this->study, $value));

        return $this;
    }
}
