<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClaimResponse Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRClaimResponseInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRClaimResponseAddItem;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRClaimResponseError;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRClaimResponseInsurance;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRClaimResponseItem;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRClaimResponsePayment;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRClaimResponseProcessNote;

class FHIRClaimResponse extends FHIRDomainResource implements FHIRClaimResponseInterface
{
    public const RESOURCE_NAME = 'ClaimResponse';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRReference $patient = null;
    protected ?FHIRDateTime $created = null;
    protected ?FHIRReference $insurer = null;
    protected ?FHIRReference $requestProvider = null;
    protected ?FHIRReference $requestOrganization = null;
    protected ?FHIRReference $request = null;
    protected ?FHIRCodeableConcept $outcome = null;
    protected ?FHIRString $disposition = null;
    protected ?FHIRCodeableConcept $payeeType = null;

    /** @var FHIRClaimResponseItem[] */
    protected array $item = [];

    /** @var FHIRClaimResponseAddItem[] */
    protected array $addItem = [];

    /** @var FHIRClaimResponseError[] */
    protected array $error = [];
    protected ?FHIRMoney $totalCost = null;
    protected ?FHIRMoney $unallocDeductable = null;
    protected ?FHIRMoney $totalBenefit = null;
    protected ?FHIRClaimResponsePayment $payment = null;
    protected ?FHIRCoding $reserved = null;
    protected ?FHIRCodeableConcept $form = null;

    /** @var FHIRClaimResponseProcessNote[] */
    protected array $processNote = [];

    /** @var FHIRReference[] */
    protected array $communicationRequest = [];

    /** @var FHIRClaimResponseInsurance[] */
    protected array $insurance = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getCreated(): ?FHIRDateTime
    {
        return $this->created;
    }

    public function setCreated(string|FHIRDateTime|null $value): self
    {
        $this->created = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getInsurer(): ?FHIRReference
    {
        return $this->insurer;
    }

    public function setInsurer(?FHIRReference $value): self
    {
        $this->insurer = $value;

        return $this;
    }

    public function getRequestProvider(): ?FHIRReference
    {
        return $this->requestProvider;
    }

    public function setRequestProvider(?FHIRReference $value): self
    {
        $this->requestProvider = $value;

        return $this;
    }

    public function getRequestOrganization(): ?FHIRReference
    {
        return $this->requestOrganization;
    }

    public function setRequestOrganization(?FHIRReference $value): self
    {
        $this->requestOrganization = $value;

        return $this;
    }

    public function getRequest(): ?FHIRReference
    {
        return $this->request;
    }

    public function setRequest(?FHIRReference $value): self
    {
        $this->request = $value;

        return $this;
    }

    public function getOutcome(): ?FHIRCodeableConcept
    {
        return $this->outcome;
    }

    public function setOutcome(?FHIRCodeableConcept $value): self
    {
        $this->outcome = $value;

        return $this;
    }

    public function getDisposition(): ?FHIRString
    {
        return $this->disposition;
    }

    public function setDisposition(string|FHIRString|null $value): self
    {
        $this->disposition = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPayeeType(): ?FHIRCodeableConcept
    {
        return $this->payeeType;
    }

    public function setPayeeType(?FHIRCodeableConcept $value): self
    {
        $this->payeeType = $value;

        return $this;
    }

    /**
     * @return FHIRClaimResponseItem[]
     */
    public function getItem(): array
    {
        return $this->item;
    }

    public function setItem(?FHIRClaimResponseItem ...$value): self
    {
        $this->item = array_filter($value);

        return $this;
    }

    public function addItem(?FHIRClaimResponseItem ...$value): self
    {
        $this->item = array_filter(array_merge($this->item, $value));

        return $this;
    }

    /**
     * @return FHIRClaimResponseAddItem[]
     */
    public function getAddItem(): array
    {
        return $this->addItem;
    }

    public function setAddItem(?FHIRClaimResponseAddItem ...$value): self
    {
        $this->addItem = array_filter($value);

        return $this;
    }

    public function addAddItem(?FHIRClaimResponseAddItem ...$value): self
    {
        $this->addItem = array_filter(array_merge($this->addItem, $value));

        return $this;
    }

    /**
     * @return FHIRClaimResponseError[]
     */
    public function getError(): array
    {
        return $this->error;
    }

    public function setError(?FHIRClaimResponseError ...$value): self
    {
        $this->error = array_filter($value);

        return $this;
    }

    public function addError(?FHIRClaimResponseError ...$value): self
    {
        $this->error = array_filter(array_merge($this->error, $value));

        return $this;
    }

    public function getTotalCost(): ?FHIRMoney
    {
        return $this->totalCost;
    }

    public function setTotalCost(?FHIRMoney $value): self
    {
        $this->totalCost = $value;

        return $this;
    }

    public function getUnallocDeductable(): ?FHIRMoney
    {
        return $this->unallocDeductable;
    }

    public function setUnallocDeductable(?FHIRMoney $value): self
    {
        $this->unallocDeductable = $value;

        return $this;
    }

    public function getTotalBenefit(): ?FHIRMoney
    {
        return $this->totalBenefit;
    }

    public function setTotalBenefit(?FHIRMoney $value): self
    {
        $this->totalBenefit = $value;

        return $this;
    }

    public function getPayment(): ?FHIRClaimResponsePayment
    {
        return $this->payment;
    }

    public function setPayment(?FHIRClaimResponsePayment $value): self
    {
        $this->payment = $value;

        return $this;
    }

    public function getReserved(): ?FHIRCoding
    {
        return $this->reserved;
    }

    public function setReserved(?FHIRCoding $value): self
    {
        $this->reserved = $value;

        return $this;
    }

    public function getForm(): ?FHIRCodeableConcept
    {
        return $this->form;
    }

    public function setForm(?FHIRCodeableConcept $value): self
    {
        $this->form = $value;

        return $this;
    }

    /**
     * @return FHIRClaimResponseProcessNote[]
     */
    public function getProcessNote(): array
    {
        return $this->processNote;
    }

    public function setProcessNote(?FHIRClaimResponseProcessNote ...$value): self
    {
        $this->processNote = array_filter($value);

        return $this;
    }

    public function addProcessNote(?FHIRClaimResponseProcessNote ...$value): self
    {
        $this->processNote = array_filter(array_merge($this->processNote, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getCommunicationRequest(): array
    {
        return $this->communicationRequest;
    }

    public function setCommunicationRequest(?FHIRReference ...$value): self
    {
        $this->communicationRequest = array_filter($value);

        return $this;
    }

    public function addCommunicationRequest(?FHIRReference ...$value): self
    {
        $this->communicationRequest = array_filter(array_merge($this->communicationRequest, $value));

        return $this;
    }

    /**
     * @return FHIRClaimResponseInsurance[]
     */
    public function getInsurance(): array
    {
        return $this->insurance;
    }

    public function setInsurance(?FHIRClaimResponseInsurance ...$value): self
    {
        $this->insurance = array_filter($value);

        return $this;
    }

    public function addInsurance(?FHIRClaimResponseInsurance ...$value): self
    {
        $this->insurance = array_filter(array_merge($this->insurance, $value));

        return $this;
    }
}
