<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR QuestionnaireResponse Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRQuestionnaireResponseInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRQuestionnaireResponseItem;

class FHIRQuestionnaireResponse extends FHIRDomainResource implements FHIRQuestionnaireResponseInterface
{
    public const RESOURCE_NAME = 'QuestionnaireResponse';

    protected ?FHIRIdentifier $identifier = null;

    /** @var FHIRReference[] */
    protected array $basedOn = [];

    /** @var FHIRReference[] */
    protected array $parent = [];
    protected ?FHIRReference $questionnaire = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $context = null;
    protected ?FHIRDateTime $authored = null;
    protected ?FHIRReference $author = null;
    protected ?FHIRReference $source = null;

    /** @var FHIRQuestionnaireResponseItem[] */
    protected array $item = [];

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getParent(): array
    {
        return $this->parent;
    }

    public function setParent(?FHIRReference ...$value): self
    {
        $this->parent = array_filter($value);

        return $this;
    }

    public function addParent(?FHIRReference ...$value): self
    {
        $this->parent = array_filter(array_merge($this->parent, $value));

        return $this;
    }

    public function getQuestionnaire(): ?FHIRReference
    {
        return $this->questionnaire;
    }

    public function setQuestionnaire(?FHIRReference $value): self
    {
        $this->questionnaire = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getContext(): ?FHIRReference
    {
        return $this->context;
    }

    public function setContext(?FHIRReference $value): self
    {
        $this->context = $value;

        return $this;
    }

    public function getAuthored(): ?FHIRDateTime
    {
        return $this->authored;
    }

    public function setAuthored(string|FHIRDateTime|null $value): self
    {
        $this->authored = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getAuthor(): ?FHIRReference
    {
        return $this->author;
    }

    public function setAuthor(?FHIRReference $value): self
    {
        $this->author = $value;

        return $this;
    }

    public function getSource(): ?FHIRReference
    {
        return $this->source;
    }

    public function setSource(?FHIRReference $value): self
    {
        $this->source = $value;

        return $this;
    }

    /**
     * @return FHIRQuestionnaireResponseItem[]
     */
    public function getItem(): array
    {
        return $this->item;
    }

    public function setItem(?FHIRQuestionnaireResponseItem ...$value): self
    {
        $this->item = array_filter($value);

        return $this;
    }

    public function addItem(?FHIRQuestionnaireResponseItem ...$value): self
    {
        $this->item = array_filter(array_merge($this->item, $value));

        return $this;
    }
}
