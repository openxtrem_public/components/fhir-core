<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceComponent Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRDeviceComponentInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRDeviceComponentProductionSpecification;

class FHIRDeviceComponent extends FHIRDomainResource implements FHIRDeviceComponentInterface
{
    public const RESOURCE_NAME = 'DeviceComponent';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRInstant $lastSystemChange = null;
    protected ?FHIRReference $source = null;
    protected ?FHIRReference $parent = null;

    /** @var FHIRCodeableConcept[] */
    protected array $operationalStatus = [];
    protected ?FHIRCodeableConcept $parameterGroup = null;
    protected ?FHIRCode $measurementPrinciple = null;

    /** @var FHIRDeviceComponentProductionSpecification[] */
    protected array $productionSpecification = [];
    protected ?FHIRCodeableConcept $languageCode = null;

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getLastSystemChange(): ?FHIRInstant
    {
        return $this->lastSystemChange;
    }

    public function setLastSystemChange(string|FHIRInstant|null $value): self
    {
        $this->lastSystemChange = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    public function getSource(): ?FHIRReference
    {
        return $this->source;
    }

    public function setSource(?FHIRReference $value): self
    {
        $this->source = $value;

        return $this;
    }

    public function getParent(): ?FHIRReference
    {
        return $this->parent;
    }

    public function setParent(?FHIRReference $value): self
    {
        $this->parent = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getOperationalStatus(): array
    {
        return $this->operationalStatus;
    }

    public function setOperationalStatus(?FHIRCodeableConcept ...$value): self
    {
        $this->operationalStatus = array_filter($value);

        return $this;
    }

    public function addOperationalStatus(?FHIRCodeableConcept ...$value): self
    {
        $this->operationalStatus = array_filter(array_merge($this->operationalStatus, $value));

        return $this;
    }

    public function getParameterGroup(): ?FHIRCodeableConcept
    {
        return $this->parameterGroup;
    }

    public function setParameterGroup(?FHIRCodeableConcept $value): self
    {
        $this->parameterGroup = $value;

        return $this;
    }

    public function getMeasurementPrinciple(): ?FHIRCode
    {
        return $this->measurementPrinciple;
    }

    public function setMeasurementPrinciple(string|FHIRCode|null $value): self
    {
        $this->measurementPrinciple = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRDeviceComponentProductionSpecification[]
     */
    public function getProductionSpecification(): array
    {
        return $this->productionSpecification;
    }

    public function setProductionSpecification(?FHIRDeviceComponentProductionSpecification ...$value): self
    {
        $this->productionSpecification = array_filter($value);

        return $this;
    }

    public function addProductionSpecification(?FHIRDeviceComponentProductionSpecification ...$value): self
    {
        $this->productionSpecification = array_filter(array_merge($this->productionSpecification, $value));

        return $this;
    }

    public function getLanguageCode(): ?FHIRCodeableConcept
    {
        return $this->languageCode;
    }

    public function setLanguageCode(?FHIRCodeableConcept $value): self
    {
        $this->languageCode = $value;

        return $this;
    }
}
