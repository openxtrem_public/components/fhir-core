<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExpansionProfile Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRExpansionProfileInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRExpansionProfileDesignation;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRExpansionProfileExcludedSystem;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRExpansionProfileFixedVersion;

class FHIRExpansionProfile extends FHIRDomainResource implements FHIRExpansionProfileInterface
{
    public const RESOURCE_NAME = 'ExpansionProfile';

    protected ?FHIRUri $url = null;
    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRString $version = null;
    protected ?FHIRString $name = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRBoolean $experimental = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRString $publisher = null;

    /** @var FHIRContactDetail[] */
    protected array $contact = [];
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRUsageContext[] */
    protected array $useContext = [];

    /** @var FHIRCodeableConcept[] */
    protected array $jurisdiction = [];

    /** @var FHIRExpansionProfileFixedVersion[] */
    protected array $fixedVersion = [];
    protected ?FHIRExpansionProfileExcludedSystem $excludedSystem = null;
    protected ?FHIRBoolean $includeDesignations = null;
    protected ?FHIRExpansionProfileDesignation $designation = null;
    protected ?FHIRBoolean $includeDefinition = null;
    protected ?FHIRBoolean $activeOnly = null;
    protected ?FHIRBoolean $excludeNested = null;
    protected ?FHIRBoolean $excludeNotForUI = null;
    protected ?FHIRBoolean $excludePostCoordinated = null;
    protected ?FHIRCode $displayLanguage = null;
    protected ?FHIRBoolean $limitedExpansion = null;

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getExperimental(): ?FHIRBoolean
    {
        return $this->experimental;
    }

    public function setExperimental(bool|FHIRBoolean|null $value): self
    {
        $this->experimental = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPublisher(): ?FHIRString
    {
        return $this->publisher;
    }

    public function setPublisher(string|FHIRString|null $value): self
    {
        $this->publisher = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUseContext(): array
    {
        return $this->useContext;
    }

    public function setUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter($value);

        return $this;
    }

    public function addUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter(array_merge($this->useContext, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getJurisdiction(): array
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter($value);

        return $this;
    }

    public function addJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter(array_merge($this->jurisdiction, $value));

        return $this;
    }

    /**
     * @return FHIRExpansionProfileFixedVersion[]
     */
    public function getFixedVersion(): array
    {
        return $this->fixedVersion;
    }

    public function setFixedVersion(?FHIRExpansionProfileFixedVersion ...$value): self
    {
        $this->fixedVersion = array_filter($value);

        return $this;
    }

    public function addFixedVersion(?FHIRExpansionProfileFixedVersion ...$value): self
    {
        $this->fixedVersion = array_filter(array_merge($this->fixedVersion, $value));

        return $this;
    }

    public function getExcludedSystem(): ?FHIRExpansionProfileExcludedSystem
    {
        return $this->excludedSystem;
    }

    public function setExcludedSystem(?FHIRExpansionProfileExcludedSystem $value): self
    {
        $this->excludedSystem = $value;

        return $this;
    }

    public function getIncludeDesignations(): ?FHIRBoolean
    {
        return $this->includeDesignations;
    }

    public function setIncludeDesignations(bool|FHIRBoolean|null $value): self
    {
        $this->includeDesignations = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getDesignation(): ?FHIRExpansionProfileDesignation
    {
        return $this->designation;
    }

    public function setDesignation(?FHIRExpansionProfileDesignation $value): self
    {
        $this->designation = $value;

        return $this;
    }

    public function getIncludeDefinition(): ?FHIRBoolean
    {
        return $this->includeDefinition;
    }

    public function setIncludeDefinition(bool|FHIRBoolean|null $value): self
    {
        $this->includeDefinition = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getActiveOnly(): ?FHIRBoolean
    {
        return $this->activeOnly;
    }

    public function setActiveOnly(bool|FHIRBoolean|null $value): self
    {
        $this->activeOnly = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getExcludeNested(): ?FHIRBoolean
    {
        return $this->excludeNested;
    }

    public function setExcludeNested(bool|FHIRBoolean|null $value): self
    {
        $this->excludeNested = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getExcludeNotForUI(): ?FHIRBoolean
    {
        return $this->excludeNotForUI;
    }

    public function setExcludeNotForUI(bool|FHIRBoolean|null $value): self
    {
        $this->excludeNotForUI = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getExcludePostCoordinated(): ?FHIRBoolean
    {
        return $this->excludePostCoordinated;
    }

    public function setExcludePostCoordinated(bool|FHIRBoolean|null $value): self
    {
        $this->excludePostCoordinated = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getDisplayLanguage(): ?FHIRCode
    {
        return $this->displayLanguage;
    }

    public function setDisplayLanguage(string|FHIRCode|null $value): self
    {
        $this->displayLanguage = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getLimitedExpansion(): ?FHIRBoolean
    {
        return $this->limitedExpansion;
    }

    public function setLimitedExpansion(bool|FHIRBoolean|null $value): self
    {
        $this->limitedExpansion = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }
}
