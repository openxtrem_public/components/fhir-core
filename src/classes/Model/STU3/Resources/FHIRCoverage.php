<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Coverage Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRCoverageInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRCoverageGrouping;

class FHIRCoverage extends FHIRDomainResource implements FHIRCoverageInterface
{
    public const RESOURCE_NAME = 'Coverage';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRReference $policyHolder = null;
    protected ?FHIRReference $subscriber = null;
    protected ?FHIRString $subscriberId = null;
    protected ?FHIRReference $beneficiary = null;
    protected ?FHIRCodeableConcept $relationship = null;
    protected ?FHIRPeriod $period = null;

    /** @var FHIRReference[] */
    protected array $payor = [];
    protected ?FHIRCoverageGrouping $grouping = null;
    protected ?FHIRString $dependent = null;
    protected ?FHIRString $sequence = null;
    protected ?FHIRPositiveInt $order = null;
    protected ?FHIRString $network = null;

    /** @var FHIRReference[] */
    protected array $contract = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getPolicyHolder(): ?FHIRReference
    {
        return $this->policyHolder;
    }

    public function setPolicyHolder(?FHIRReference $value): self
    {
        $this->policyHolder = $value;

        return $this;
    }

    public function getSubscriber(): ?FHIRReference
    {
        return $this->subscriber;
    }

    public function setSubscriber(?FHIRReference $value): self
    {
        $this->subscriber = $value;

        return $this;
    }

    public function getSubscriberId(): ?FHIRString
    {
        return $this->subscriberId;
    }

    public function setSubscriberId(string|FHIRString|null $value): self
    {
        $this->subscriberId = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getBeneficiary(): ?FHIRReference
    {
        return $this->beneficiary;
    }

    public function setBeneficiary(?FHIRReference $value): self
    {
        $this->beneficiary = $value;

        return $this;
    }

    public function getRelationship(): ?FHIRCodeableConcept
    {
        return $this->relationship;
    }

    public function setRelationship(?FHIRCodeableConcept $value): self
    {
        $this->relationship = $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPayor(): array
    {
        return $this->payor;
    }

    public function setPayor(?FHIRReference ...$value): self
    {
        $this->payor = array_filter($value);

        return $this;
    }

    public function addPayor(?FHIRReference ...$value): self
    {
        $this->payor = array_filter(array_merge($this->payor, $value));

        return $this;
    }

    public function getGrouping(): ?FHIRCoverageGrouping
    {
        return $this->grouping;
    }

    public function setGrouping(?FHIRCoverageGrouping $value): self
    {
        $this->grouping = $value;

        return $this;
    }

    public function getDependent(): ?FHIRString
    {
        return $this->dependent;
    }

    public function setDependent(string|FHIRString|null $value): self
    {
        $this->dependent = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSequence(): ?FHIRString
    {
        return $this->sequence;
    }

    public function setSequence(string|FHIRString|null $value): self
    {
        $this->sequence = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getOrder(): ?FHIRPositiveInt
    {
        return $this->order;
    }

    public function setOrder(int|FHIRPositiveInt|null $value): self
    {
        $this->order = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getNetwork(): ?FHIRString
    {
        return $this->network;
    }

    public function setNetwork(string|FHIRString|null $value): self
    {
        $this->network = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getContract(): array
    {
        return $this->contract;
    }

    public function setContract(?FHIRReference ...$value): self
    {
        $this->contract = array_filter($value);

        return $this;
    }

    public function addContract(?FHIRReference ...$value): self
    {
        $this->contract = array_filter(array_merge($this->contract, $value));

        return $this;
    }
}
