<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceUseStatement Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRDeviceUseStatementInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;

class FHIRDeviceUseStatement extends FHIRDomainResource implements FHIRDeviceUseStatementInterface
{
    public const RESOURCE_NAME = 'DeviceUseStatement';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRPeriod $whenUsed = null;
    protected FHIRTiming|FHIRPeriod|FHIRDateTime|null $timing = null;
    protected ?FHIRDateTime $recordedOn = null;
    protected ?FHIRReference $source = null;
    protected ?FHIRReference $device = null;

    /** @var FHIRCodeableConcept[] */
    protected array $indication = [];
    protected ?FHIRCodeableConcept $bodySite = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getWhenUsed(): ?FHIRPeriod
    {
        return $this->whenUsed;
    }

    public function setWhenUsed(?FHIRPeriod $value): self
    {
        $this->whenUsed = $value;

        return $this;
    }

    public function getTiming(): FHIRTiming|FHIRPeriod|FHIRDateTime|null
    {
        return $this->timing;
    }

    public function setTiming(FHIRTiming|FHIRPeriod|FHIRDateTime|null $value): self
    {
        $this->timing = $value;

        return $this;
    }

    public function getRecordedOn(): ?FHIRDateTime
    {
        return $this->recordedOn;
    }

    public function setRecordedOn(string|FHIRDateTime|null $value): self
    {
        $this->recordedOn = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getSource(): ?FHIRReference
    {
        return $this->source;
    }

    public function setSource(?FHIRReference $value): self
    {
        $this->source = $value;

        return $this;
    }

    public function getDevice(): ?FHIRReference
    {
        return $this->device;
    }

    public function setDevice(?FHIRReference $value): self
    {
        $this->device = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getIndication(): array
    {
        return $this->indication;
    }

    public function setIndication(?FHIRCodeableConcept ...$value): self
    {
        $this->indication = array_filter($value);

        return $this;
    }

    public function addIndication(?FHIRCodeableConcept ...$value): self
    {
        $this->indication = array_filter(array_merge($this->indication, $value));

        return $this;
    }

    public function getBodySite(): ?FHIRCodeableConcept
    {
        return $this->bodySite;
    }

    public function setBodySite(?FHIRCodeableConcept $value): self
    {
        $this->bodySite = $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }
}
