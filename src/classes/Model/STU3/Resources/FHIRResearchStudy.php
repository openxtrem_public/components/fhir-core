<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ResearchStudy Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRResearchStudyInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRRelatedArtifact;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRResearchStudyArm;

class FHIRResearchStudy extends FHIRDomainResource implements FHIRResearchStudyInterface
{
    public const RESOURCE_NAME = 'ResearchStudy';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRString $title = null;

    /** @var FHIRReference[] */
    protected array $protocol = [];

    /** @var FHIRReference[] */
    protected array $partOf = [];
    protected ?FHIRCode $status = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];

    /** @var FHIRCodeableConcept[] */
    protected array $focus = [];

    /** @var FHIRContactDetail[] */
    protected array $contact = [];

    /** @var FHIRRelatedArtifact[] */
    protected array $relatedArtifact = [];

    /** @var FHIRCodeableConcept[] */
    protected array $keyword = [];

    /** @var FHIRCodeableConcept[] */
    protected array $jurisdiction = [];
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRReference[] */
    protected array $enrollment = [];
    protected ?FHIRPeriod $period = null;
    protected ?FHIRReference $sponsor = null;
    protected ?FHIRReference $principalInvestigator = null;

    /** @var FHIRReference[] */
    protected array $site = [];
    protected ?FHIRCodeableConcept $reasonStopped = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRResearchStudyArm[] */
    protected array $arm = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getProtocol(): array
    {
        return $this->protocol;
    }

    public function setProtocol(?FHIRReference ...$value): self
    {
        $this->protocol = array_filter($value);

        return $this;
    }

    public function addProtocol(?FHIRReference ...$value): self
    {
        $this->protocol = array_filter(array_merge($this->protocol, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPartOf(): array
    {
        return $this->partOf;
    }

    public function setPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter($value);

        return $this;
    }

    public function addPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter(array_merge($this->partOf, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getFocus(): array
    {
        return $this->focus;
    }

    public function setFocus(?FHIRCodeableConcept ...$value): self
    {
        $this->focus = array_filter($value);

        return $this;
    }

    public function addFocus(?FHIRCodeableConcept ...$value): self
    {
        $this->focus = array_filter(array_merge($this->focus, $value));

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    /**
     * @return FHIRRelatedArtifact[]
     */
    public function getRelatedArtifact(): array
    {
        return $this->relatedArtifact;
    }

    public function setRelatedArtifact(?FHIRRelatedArtifact ...$value): self
    {
        $this->relatedArtifact = array_filter($value);

        return $this;
    }

    public function addRelatedArtifact(?FHIRRelatedArtifact ...$value): self
    {
        $this->relatedArtifact = array_filter(array_merge($this->relatedArtifact, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getKeyword(): array
    {
        return $this->keyword;
    }

    public function setKeyword(?FHIRCodeableConcept ...$value): self
    {
        $this->keyword = array_filter($value);

        return $this;
    }

    public function addKeyword(?FHIRCodeableConcept ...$value): self
    {
        $this->keyword = array_filter(array_merge($this->keyword, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getJurisdiction(): array
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter($value);

        return $this;
    }

    public function addJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter(array_merge($this->jurisdiction, $value));

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEnrollment(): array
    {
        return $this->enrollment;
    }

    public function setEnrollment(?FHIRReference ...$value): self
    {
        $this->enrollment = array_filter($value);

        return $this;
    }

    public function addEnrollment(?FHIRReference ...$value): self
    {
        $this->enrollment = array_filter(array_merge($this->enrollment, $value));

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    public function getSponsor(): ?FHIRReference
    {
        return $this->sponsor;
    }

    public function setSponsor(?FHIRReference $value): self
    {
        $this->sponsor = $value;

        return $this;
    }

    public function getPrincipalInvestigator(): ?FHIRReference
    {
        return $this->principalInvestigator;
    }

    public function setPrincipalInvestigator(?FHIRReference $value): self
    {
        $this->principalInvestigator = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSite(): array
    {
        return $this->site;
    }

    public function setSite(?FHIRReference ...$value): self
    {
        $this->site = array_filter($value);

        return $this;
    }

    public function addSite(?FHIRReference ...$value): self
    {
        $this->site = array_filter(array_merge($this->site, $value));

        return $this;
    }

    public function getReasonStopped(): ?FHIRCodeableConcept
    {
        return $this->reasonStopped;
    }

    public function setReasonStopped(?FHIRCodeableConcept $value): self
    {
        $this->reasonStopped = $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRResearchStudyArm[]
     */
    public function getArm(): array
    {
        return $this->arm;
    }

    public function setArm(?FHIRResearchStudyArm ...$value): self
    {
        $this->arm = array_filter($value);

        return $this;
    }

    public function addArm(?FHIRResearchStudyArm ...$value): self
    {
        $this->arm = array_filter(array_merge($this->arm, $value));

        return $this;
    }
}
