<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationPackageContent Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationPackageContentInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;

class FHIRMedicationPackageContent extends FHIRBackboneElement implements FHIRMedicationPackageContentInterface
{
    public const RESOURCE_NAME = 'Medication.package.content';

    protected FHIRCodeableConcept|FHIRReference|null $item = null;
    protected ?FHIRQuantity $amount = null;

    public function getItem(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->item;
    }

    public function setItem(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->item = $value;

        return $this;
    }

    public function getAmount(): ?FHIRQuantity
    {
        return $this->amount;
    }

    public function setAmount(?FHIRQuantity $value): self
    {
        $this->amount = $value;

        return $this;
    }
}
