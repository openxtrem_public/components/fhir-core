<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EligibilityResponseInsurance Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREligibilityResponseInsuranceInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;

class FHIREligibilityResponseInsurance extends FHIRBackboneElement implements FHIREligibilityResponseInsuranceInterface
{
    public const RESOURCE_NAME = 'EligibilityResponse.insurance';

    protected ?FHIRReference $coverage = null;
    protected ?FHIRReference $contract = null;

    /** @var FHIREligibilityResponseInsuranceBenefitBalance[] */
    protected array $benefitBalance = [];

    public function getCoverage(): ?FHIRReference
    {
        return $this->coverage;
    }

    public function setCoverage(?FHIRReference $value): self
    {
        $this->coverage = $value;

        return $this;
    }

    public function getContract(): ?FHIRReference
    {
        return $this->contract;
    }

    public function setContract(?FHIRReference $value): self
    {
        $this->contract = $value;

        return $this;
    }

    /**
     * @return FHIREligibilityResponseInsuranceBenefitBalance[]
     */
    public function getBenefitBalance(): array
    {
        return $this->benefitBalance;
    }

    public function setBenefitBalance(?FHIREligibilityResponseInsuranceBenefitBalance ...$value): self
    {
        $this->benefitBalance = array_filter($value);

        return $this;
    }

    public function addBenefitBalance(?FHIREligibilityResponseInsuranceBenefitBalance ...$value): self
    {
        $this->benefitBalance = array_filter(array_merge($this->benefitBalance, $value));

        return $this;
    }
}
