<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClaimInformation Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRClaimInformationInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRClaimInformation extends FHIRBackboneElement implements FHIRClaimInformationInterface
{
    public const RESOURCE_NAME = 'Claim.information';

    protected ?FHIRPositiveInt $sequence = null;
    protected ?FHIRCodeableConcept $category = null;
    protected ?FHIRCodeableConcept $code = null;
    protected FHIRDate|FHIRPeriod|null $timing = null;
    protected FHIRString|FHIRQuantity|FHIRAttachment|FHIRReference|null $value = null;
    protected ?FHIRCodeableConcept $reason = null;

    public function getSequence(): ?FHIRPositiveInt
    {
        return $this->sequence;
    }

    public function setSequence(int|FHIRPositiveInt|null $value): self
    {
        $this->sequence = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getTiming(): FHIRDate|FHIRPeriod|null
    {
        return $this->timing;
    }

    public function setTiming(FHIRDate|FHIRPeriod|null $value): self
    {
        $this->timing = $value;

        return $this;
    }

    public function getValue(): FHIRString|FHIRQuantity|FHIRAttachment|FHIRReference|null
    {
        return $this->value;
    }

    public function setValue(FHIRString|FHIRQuantity|FHIRAttachment|FHIRReference|null $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getReason(): ?FHIRCodeableConcept
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableConcept $value): self
    {
        $this->reason = $value;

        return $this;
    }
}
