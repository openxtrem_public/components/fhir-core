<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ContractTermAgent Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRContractTermAgentInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRElement;

class FHIRContractTermAgent extends FHIRBackboneElement implements FHIRContractTermAgentInterface
{
    public const RESOURCE_NAME = 'Contract.term.agent';

    protected ?FHIRElement $actor = null;

    /** @var FHIRCodeableConcept[] */
    protected array $role = [];

    public function getActor(): ?FHIRElement
    {
        return $this->actor;
    }

    public function setActor(?FHIRElement $value): self
    {
        $this->actor = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getRole(): array
    {
        return $this->role;
    }

    public function setRole(?FHIRCodeableConcept ...$value): self
    {
        $this->role = array_filter($value);

        return $this;
    }

    public function addRole(?FHIRCodeableConcept ...$value): self
    {
        $this->role = array_filter(array_merge($this->role, $value));

        return $this;
    }
}
