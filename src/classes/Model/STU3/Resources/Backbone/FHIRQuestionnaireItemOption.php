<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR QuestionnaireItemOption Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRQuestionnaireItemOptionInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRTime;

class FHIRQuestionnaireItemOption extends FHIRBackboneElement implements FHIRQuestionnaireItemOptionInterface
{
    public const RESOURCE_NAME = 'Questionnaire.item.option';

    protected FHIRInteger|FHIRDate|FHIRTime|FHIRString|FHIRCoding|null $value = null;

    public function getValue(): FHIRInteger|FHIRDate|FHIRTime|FHIRString|FHIRCoding|null
    {
        return $this->value;
    }

    public function setValue(FHIRInteger|FHIRDate|FHIRTime|FHIRString|FHIRCoding|null $value): self
    {
        $this->value = $value;

        return $this;
    }
}
