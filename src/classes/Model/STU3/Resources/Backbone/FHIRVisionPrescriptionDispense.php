<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR VisionPrescriptionDispense Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRVisionPrescriptionDispenseInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRVisionPrescriptionDispense extends FHIRBackboneElement implements FHIRVisionPrescriptionDispenseInterface
{
    public const RESOURCE_NAME = 'VisionPrescription.dispense';

    protected ?FHIRCodeableConcept $product = null;
    protected ?FHIRCode $eye = null;
    protected ?FHIRDecimal $sphere = null;
    protected ?FHIRDecimal $cylinder = null;
    protected ?FHIRInteger $axis = null;
    protected ?FHIRDecimal $prism = null;
    protected ?FHIRCode $base = null;
    protected ?FHIRDecimal $add = null;
    protected ?FHIRDecimal $power = null;
    protected ?FHIRDecimal $backCurve = null;
    protected ?FHIRDecimal $diameter = null;
    protected ?FHIRQuantity $duration = null;
    protected ?FHIRString $color = null;
    protected ?FHIRString $brand = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    public function getProduct(): ?FHIRCodeableConcept
    {
        return $this->product;
    }

    public function setProduct(?FHIRCodeableConcept $value): self
    {
        $this->product = $value;

        return $this;
    }

    public function getEye(): ?FHIRCode
    {
        return $this->eye;
    }

    public function setEye(string|FHIRCode|null $value): self
    {
        $this->eye = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getSphere(): ?FHIRDecimal
    {
        return $this->sphere;
    }

    public function setSphere(float|FHIRDecimal|null $value): self
    {
        $this->sphere = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getCylinder(): ?FHIRDecimal
    {
        return $this->cylinder;
    }

    public function setCylinder(float|FHIRDecimal|null $value): self
    {
        $this->cylinder = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getAxis(): ?FHIRInteger
    {
        return $this->axis;
    }

    public function setAxis(int|FHIRInteger|null $value): self
    {
        $this->axis = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getPrism(): ?FHIRDecimal
    {
        return $this->prism;
    }

    public function setPrism(float|FHIRDecimal|null $value): self
    {
        $this->prism = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getBase(): ?FHIRCode
    {
        return $this->base;
    }

    public function setBase(string|FHIRCode|null $value): self
    {
        $this->base = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getAdd(): ?FHIRDecimal
    {
        return $this->add;
    }

    public function setAdd(float|FHIRDecimal|null $value): self
    {
        $this->add = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getPower(): ?FHIRDecimal
    {
        return $this->power;
    }

    public function setPower(float|FHIRDecimal|null $value): self
    {
        $this->power = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getBackCurve(): ?FHIRDecimal
    {
        return $this->backCurve;
    }

    public function setBackCurve(float|FHIRDecimal|null $value): self
    {
        $this->backCurve = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getDiameter(): ?FHIRDecimal
    {
        return $this->diameter;
    }

    public function setDiameter(float|FHIRDecimal|null $value): self
    {
        $this->diameter = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getDuration(): ?FHIRQuantity
    {
        return $this->duration;
    }

    public function setDuration(?FHIRQuantity $value): self
    {
        $this->duration = $value;

        return $this;
    }

    public function getColor(): ?FHIRString
    {
        return $this->color;
    }

    public function setColor(string|FHIRString|null $value): self
    {
        $this->color = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getBrand(): ?FHIRString
    {
        return $this->brand;
    }

    public function setBrand(string|FHIRString|null $value): self
    {
        $this->brand = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }
}
