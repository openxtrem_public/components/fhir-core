<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR OperationDefinitionOverload Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIROperationDefinitionOverloadInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIROperationDefinitionOverload extends FHIRBackboneElement implements FHIROperationDefinitionOverloadInterface
{
    public const RESOURCE_NAME = 'OperationDefinition.overload';

    /** @var FHIRString[] */
    protected array $parameterName = [];
    protected ?FHIRString $comment = null;

    /**
     * @return FHIRString[]
     */
    public function getParameterName(): array
    {
        return $this->parameterName;
    }

    public function setParameterName(string|FHIRString|null ...$value): self
    {
        $this->parameterName = [];
        $this->addParameterName(...$value);

        return $this;
    }

    public function addParameterName(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->parameterName = array_filter(array_merge($this->parameterName, $values));

        return $this;
    }

    public function getComment(): ?FHIRString
    {
        return $this->comment;
    }

    public function setComment(string|FHIRString|null $value): self
    {
        $this->comment = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
