<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DiagnosticReportImage Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDiagnosticReportImageInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRDiagnosticReportImage extends FHIRBackboneElement implements FHIRDiagnosticReportImageInterface
{
    public const RESOURCE_NAME = 'DiagnosticReport.image';

    protected ?FHIRString $comment = null;
    protected ?FHIRReference $link = null;

    public function getComment(): ?FHIRString
    {
        return $this->comment;
    }

    public function setComment(string|FHIRString|null $value): self
    {
        $this->comment = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getLink(): ?FHIRReference
    {
        return $this->link;
    }

    public function setLink(?FHIRReference $value): self
    {
        $this->link = $value;

        return $this;
    }
}
