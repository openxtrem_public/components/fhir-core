<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR OperationDefinitionParameter Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIROperationDefinitionParameterInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIROperationDefinitionParameter extends FHIRBackboneElement implements FHIROperationDefinitionParameterInterface
{
    public const RESOURCE_NAME = 'OperationDefinition.parameter';

    protected ?FHIRCode $name = null;
    protected ?FHIRCode $use = null;
    protected ?FHIRInteger $min = null;
    protected ?FHIRString $max = null;
    protected ?FHIRString $documentation = null;
    protected ?FHIRCode $type = null;
    protected ?FHIRCode $searchType = null;
    protected ?FHIRReference $profile = null;
    protected ?FHIROperationDefinitionParameterBinding $binding = null;

    /** @var FHIROperationDefinitionParameter[] */
    protected array $part = [];

    public function getName(): ?FHIRCode
    {
        return $this->name;
    }

    public function setName(string|FHIRCode|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getUse(): ?FHIRCode
    {
        return $this->use;
    }

    public function setUse(string|FHIRCode|null $value): self
    {
        $this->use = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getMin(): ?FHIRInteger
    {
        return $this->min;
    }

    public function setMin(int|FHIRInteger|null $value): self
    {
        $this->min = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getMax(): ?FHIRString
    {
        return $this->max;
    }

    public function setMax(string|FHIRString|null $value): self
    {
        $this->max = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDocumentation(): ?FHIRString
    {
        return $this->documentation;
    }

    public function setDocumentation(string|FHIRString|null $value): self
    {
        $this->documentation = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getSearchType(): ?FHIRCode
    {
        return $this->searchType;
    }

    public function setSearchType(string|FHIRCode|null $value): self
    {
        $this->searchType = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getProfile(): ?FHIRReference
    {
        return $this->profile;
    }

    public function setProfile(?FHIRReference $value): self
    {
        $this->profile = $value;

        return $this;
    }

    public function getBinding(): ?FHIROperationDefinitionParameterBinding
    {
        return $this->binding;
    }

    public function setBinding(?FHIROperationDefinitionParameterBinding $value): self
    {
        $this->binding = $value;

        return $this;
    }

    /**
     * @return FHIROperationDefinitionParameter[]
     */
    public function getPart(): array
    {
        return $this->part;
    }

    public function setPart(?FHIROperationDefinitionParameter ...$value): self
    {
        $this->part = array_filter($value);

        return $this;
    }

    public function addPart(?FHIROperationDefinitionParameter ...$value): self
    {
        $this->part = array_filter(array_merge($this->part, $value));

        return $this;
    }
}
