<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR QuestionnaireItemEnableWhen Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRQuestionnaireItemEnableWhenInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRQuestionnaireItemEnableWhen extends FHIRBackboneElement implements FHIRQuestionnaireItemEnableWhenInterface
{
    public const RESOURCE_NAME = 'Questionnaire.item.enableWhen';

    protected ?FHIRString $question = null;
    protected ?FHIRBoolean $hasAnswer = null;
    protected ?FHIRElement $answer = null;

    public function getQuestion(): ?FHIRString
    {
        return $this->question;
    }

    public function setQuestion(string|FHIRString|null $value): self
    {
        $this->question = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getHasAnswer(): ?FHIRBoolean
    {
        return $this->hasAnswer;
    }

    public function setHasAnswer(bool|FHIRBoolean|null $value): self
    {
        $this->hasAnswer = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getAnswer(): ?FHIRElement
    {
        return $this->answer;
    }

    public function setAnswer(?FHIRElement $value): self
    {
        $this->answer = $value;

        return $this;
    }
}
