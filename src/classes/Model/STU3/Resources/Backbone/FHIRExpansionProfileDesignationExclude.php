<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExpansionProfileDesignationExclude Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExpansionProfileDesignationExcludeInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;

class FHIRExpansionProfileDesignationExclude extends FHIRBackboneElement implements FHIRExpansionProfileDesignationExcludeInterface
{
    public const RESOURCE_NAME = 'ExpansionProfile.designation.exclude';

    /** @var FHIRExpansionProfileDesignationExcludeDesignation[] */
    protected array $designation = [];

    /**
     * @return FHIRExpansionProfileDesignationExcludeDesignation[]
     */
    public function getDesignation(): array
    {
        return $this->designation;
    }

    public function setDesignation(?FHIRExpansionProfileDesignationExcludeDesignation ...$value): self
    {
        $this->designation = array_filter($value);

        return $this;
    }

    public function addDesignation(?FHIRExpansionProfileDesignationExcludeDesignation ...$value): self
    {
        $this->designation = array_filter(array_merge($this->designation, $value));

        return $this;
    }
}
