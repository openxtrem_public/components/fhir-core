<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExplanationOfBenefitItemAdjudication Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExplanationOfBenefitItemAdjudicationInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDecimal;

class FHIRExplanationOfBenefitItemAdjudication extends FHIRBackboneElement implements FHIRExplanationOfBenefitItemAdjudicationInterface
{
    public const RESOURCE_NAME = 'ExplanationOfBenefit.item.adjudication';

    protected ?FHIRCodeableConcept $category = null;
    protected ?FHIRCodeableConcept $reason = null;
    protected ?FHIRMoney $amount = null;
    protected ?FHIRDecimal $value = null;

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getReason(): ?FHIRCodeableConcept
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableConcept $value): self
    {
        $this->reason = $value;

        return $this;
    }

    public function getAmount(): ?FHIRMoney
    {
        return $this->amount;
    }

    public function setAmount(?FHIRMoney $value): self
    {
        $this->amount = $value;

        return $this;
    }

    public function getValue(): ?FHIRDecimal
    {
        return $this->value;
    }

    public function setValue(float|FHIRDecimal|null $value): self
    {
        $this->value = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }
}
