<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationAdministrationDosage Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationAdministrationDosageInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRRatio;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRMedicationAdministrationDosage extends FHIRBackboneElement implements FHIRMedicationAdministrationDosageInterface
{
    public const RESOURCE_NAME = 'MedicationAdministration.dosage';

    protected ?FHIRString $text = null;
    protected ?FHIRCodeableConcept $site = null;
    protected ?FHIRCodeableConcept $route = null;
    protected ?FHIRCodeableConcept $method = null;
    protected ?FHIRQuantity $dose = null;
    protected FHIRRatio|FHIRQuantity|null $rate = null;

    public function getText(): ?FHIRString
    {
        return $this->text;
    }

    public function setText(string|FHIRString|null $value): self
    {
        $this->text = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSite(): ?FHIRCodeableConcept
    {
        return $this->site;
    }

    public function setSite(?FHIRCodeableConcept $value): self
    {
        $this->site = $value;

        return $this;
    }

    public function getRoute(): ?FHIRCodeableConcept
    {
        return $this->route;
    }

    public function setRoute(?FHIRCodeableConcept $value): self
    {
        $this->route = $value;

        return $this;
    }

    public function getMethod(): ?FHIRCodeableConcept
    {
        return $this->method;
    }

    public function setMethod(?FHIRCodeableConcept $value): self
    {
        $this->method = $value;

        return $this;
    }

    public function getDose(): ?FHIRQuantity
    {
        return $this->dose;
    }

    public function setDose(?FHIRQuantity $value): self
    {
        $this->dose = $value;

        return $this;
    }

    public function getRate(): FHIRRatio|FHIRQuantity|null
    {
        return $this->rate;
    }

    public function setRate(FHIRRatio|FHIRQuantity|null $value): self
    {
        $this->rate = $value;

        return $this;
    }
}
