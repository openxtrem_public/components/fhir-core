<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImplementationGuidePage Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImplementationGuidePageInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;

class FHIRImplementationGuidePage extends FHIRBackboneElement implements FHIRImplementationGuidePageInterface
{
    public const RESOURCE_NAME = 'ImplementationGuide.page';

    protected ?FHIRUri $source = null;
    protected ?FHIRString $title = null;
    protected ?FHIRCode $kind = null;

    /** @var FHIRCode[] */
    protected array $type = [];

    /** @var FHIRString[] */
    protected array $package = [];
    protected ?FHIRCode $format = null;

    /** @var FHIRImplementationGuidePage[] */
    protected array $page = [];

    public function getSource(): ?FHIRUri
    {
        return $this->source;
    }

    public function setSource(string|FHIRUri|null $value): self
    {
        $this->source = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getKind(): ?FHIRCode
    {
        return $this->kind;
    }

    public function setKind(string|FHIRCode|null $value): self
    {
        $this->kind = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null ...$value): self
    {
        $this->type = [];
        $this->addType(...$value);

        return $this;
    }

    public function addType(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->type = array_filter(array_merge($this->type, $values));

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getPackage(): array
    {
        return $this->package;
    }

    public function setPackage(string|FHIRString|null ...$value): self
    {
        $this->package = [];
        $this->addPackage(...$value);

        return $this;
    }

    public function addPackage(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->package = array_filter(array_merge($this->package, $values));

        return $this;
    }

    public function getFormat(): ?FHIRCode
    {
        return $this->format;
    }

    public function setFormat(string|FHIRCode|null $value): self
    {
        $this->format = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRImplementationGuidePage[]
     */
    public function getPage(): array
    {
        return $this->page;
    }

    public function setPage(?FHIRImplementationGuidePage ...$value): self
    {
        $this->page = array_filter($value);

        return $this;
    }

    public function addPage(?FHIRImplementationGuidePage ...$value): self
    {
        $this->page = array_filter(array_merge($this->page, $value));

        return $this;
    }
}
