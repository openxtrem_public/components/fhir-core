<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SpecimenProcessing Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSpecimenProcessingInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRSpecimenProcessing extends FHIRBackboneElement implements FHIRSpecimenProcessingInterface
{
    public const RESOURCE_NAME = 'Specimen.processing';

    protected ?FHIRString $description = null;
    protected ?FHIRCodeableConcept $procedure = null;

    /** @var FHIRReference[] */
    protected array $additive = [];
    protected FHIRDateTime|FHIRPeriod|null $time = null;

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getProcedure(): ?FHIRCodeableConcept
    {
        return $this->procedure;
    }

    public function setProcedure(?FHIRCodeableConcept $value): self
    {
        $this->procedure = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAdditive(): array
    {
        return $this->additive;
    }

    public function setAdditive(?FHIRReference ...$value): self
    {
        $this->additive = array_filter($value);

        return $this;
    }

    public function addAdditive(?FHIRReference ...$value): self
    {
        $this->additive = array_filter(array_merge($this->additive, $value));

        return $this;
    }

    public function getTime(): FHIRDateTime|FHIRPeriod|null
    {
        return $this->time;
    }

    public function setTime(FHIRDateTime|FHIRPeriod|null $value): self
    {
        $this->time = $value;

        return $this;
    }
}
