<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CapabilityStatementMessagingEvent Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCapabilityStatementMessagingEventInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRCapabilityStatementMessagingEvent extends FHIRBackboneElement implements FHIRCapabilityStatementMessagingEventInterface
{
    public const RESOURCE_NAME = 'CapabilityStatement.messaging.event';

    protected ?FHIRCoding $code = null;
    protected ?FHIRCode $category = null;
    protected ?FHIRCode $mode = null;
    protected ?FHIRCode $focus = null;
    protected ?FHIRReference $request = null;
    protected ?FHIRReference $response = null;
    protected ?FHIRString $documentation = null;

    public function getCode(): ?FHIRCoding
    {
        return $this->code;
    }

    public function setCode(?FHIRCoding $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getCategory(): ?FHIRCode
    {
        return $this->category;
    }

    public function setCategory(string|FHIRCode|null $value): self
    {
        $this->category = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getMode(): ?FHIRCode
    {
        return $this->mode;
    }

    public function setMode(string|FHIRCode|null $value): self
    {
        $this->mode = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getFocus(): ?FHIRCode
    {
        return $this->focus;
    }

    public function setFocus(string|FHIRCode|null $value): self
    {
        $this->focus = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getRequest(): ?FHIRReference
    {
        return $this->request;
    }

    public function setRequest(?FHIRReference $value): self
    {
        $this->request = $value;

        return $this;
    }

    public function getResponse(): ?FHIRReference
    {
        return $this->response;
    }

    public function setResponse(?FHIRReference $value): self
    {
        $this->response = $value;

        return $this;
    }

    public function getDocumentation(): ?FHIRString
    {
        return $this->documentation;
    }

    public function setDocumentation(string|FHIRString|null $value): self
    {
        $this->documentation = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
