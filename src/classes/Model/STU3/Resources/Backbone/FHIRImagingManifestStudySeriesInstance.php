<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImagingManifestStudySeriesInstance Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImagingManifestStudySeriesInstanceInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIROid;

class FHIRImagingManifestStudySeriesInstance extends FHIRBackboneElement implements FHIRImagingManifestStudySeriesInstanceInterface
{
    public const RESOURCE_NAME = 'ImagingManifest.study.series.instance';

    protected ?FHIROid $sopClass = null;
    protected ?FHIROid $uid = null;

    public function getSopClass(): ?FHIROid
    {
        return $this->sopClass;
    }

    public function setSopClass(string|FHIROid|null $value): self
    {
        $this->sopClass = is_string($value) ? (new FHIROid())->setValue($value) : $value;

        return $this;
    }

    public function getUid(): ?FHIROid
    {
        return $this->uid;
    }

    public function setUid(string|FHIROid|null $value): self
    {
        $this->uid = is_string($value) ? (new FHIROid())->setValue($value) : $value;

        return $this;
    }
}
