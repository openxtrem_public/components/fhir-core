<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ConsentData Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRConsentDataInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;

class FHIRConsentData extends FHIRBackboneElement implements FHIRConsentDataInterface
{
    public const RESOURCE_NAME = 'Consent.data';

    protected ?FHIRCode $meaning = null;
    protected ?FHIRReference $reference = null;

    public function getMeaning(): ?FHIRCode
    {
        return $this->meaning;
    }

    public function setMeaning(string|FHIRCode|null $value): self
    {
        $this->meaning = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getReference(): ?FHIRReference
    {
        return $this->reference;
    }

    public function setReference(?FHIRReference $value): self
    {
        $this->reference = $value;

        return $this;
    }
}
