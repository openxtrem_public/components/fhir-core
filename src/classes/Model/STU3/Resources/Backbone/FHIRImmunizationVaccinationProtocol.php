<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImmunizationVaccinationProtocol Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImmunizationVaccinationProtocolInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRImmunizationVaccinationProtocol extends FHIRBackboneElement implements FHIRImmunizationVaccinationProtocolInterface
{
    public const RESOURCE_NAME = 'Immunization.vaccinationProtocol';

    protected ?FHIRPositiveInt $doseSequence = null;
    protected ?FHIRString $description = null;
    protected ?FHIRReference $authority = null;
    protected ?FHIRString $series = null;
    protected ?FHIRPositiveInt $seriesDoses = null;

    /** @var FHIRCodeableConcept[] */
    protected array $targetDisease = [];
    protected ?FHIRCodeableConcept $doseStatus = null;
    protected ?FHIRCodeableConcept $doseStatusReason = null;

    public function getDoseSequence(): ?FHIRPositiveInt
    {
        return $this->doseSequence;
    }

    public function setDoseSequence(int|FHIRPositiveInt|null $value): self
    {
        $this->doseSequence = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getAuthority(): ?FHIRReference
    {
        return $this->authority;
    }

    public function setAuthority(?FHIRReference $value): self
    {
        $this->authority = $value;

        return $this;
    }

    public function getSeries(): ?FHIRString
    {
        return $this->series;
    }

    public function setSeries(string|FHIRString|null $value): self
    {
        $this->series = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSeriesDoses(): ?FHIRPositiveInt
    {
        return $this->seriesDoses;
    }

    public function setSeriesDoses(int|FHIRPositiveInt|null $value): self
    {
        $this->seriesDoses = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getTargetDisease(): array
    {
        return $this->targetDisease;
    }

    public function setTargetDisease(?FHIRCodeableConcept ...$value): self
    {
        $this->targetDisease = array_filter($value);

        return $this;
    }

    public function addTargetDisease(?FHIRCodeableConcept ...$value): self
    {
        $this->targetDisease = array_filter(array_merge($this->targetDisease, $value));

        return $this;
    }

    public function getDoseStatus(): ?FHIRCodeableConcept
    {
        return $this->doseStatus;
    }

    public function setDoseStatus(?FHIRCodeableConcept $value): self
    {
        $this->doseStatus = $value;

        return $this;
    }

    public function getDoseStatusReason(): ?FHIRCodeableConcept
    {
        return $this->doseStatusReason;
    }

    public function setDoseStatusReason(?FHIRCodeableConcept $value): self
    {
        $this->doseStatusReason = $value;

        return $this;
    }
}
