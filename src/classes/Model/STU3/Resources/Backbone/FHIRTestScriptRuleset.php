<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestScriptRuleset Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestScriptRulesetInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;

class FHIRTestScriptRuleset extends FHIRBackboneElement implements FHIRTestScriptRulesetInterface
{
    public const RESOURCE_NAME = 'TestScript.ruleset';

    protected ?FHIRReference $resource = null;

    /** @var FHIRTestScriptRulesetRule[] */
    protected array $rule = [];

    public function getResource(): ?FHIRReference
    {
        return $this->resource;
    }

    public function setResource(?FHIRReference $value): self
    {
        $this->resource = $value;

        return $this;
    }

    /**
     * @return FHIRTestScriptRulesetRule[]
     */
    public function getRule(): array
    {
        return $this->rule;
    }

    public function setRule(?FHIRTestScriptRulesetRule ...$value): self
    {
        $this->rule = array_filter($value);

        return $this;
    }

    public function addRule(?FHIRTestScriptRulesetRule ...$value): self
    {
        $this->rule = array_filter(array_merge($this->rule, $value));

        return $this;
    }
}
