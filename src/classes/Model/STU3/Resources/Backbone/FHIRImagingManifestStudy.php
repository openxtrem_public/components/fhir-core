<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImagingManifestStudy Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImagingManifestStudyInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIROid;

class FHIRImagingManifestStudy extends FHIRBackboneElement implements FHIRImagingManifestStudyInterface
{
    public const RESOURCE_NAME = 'ImagingManifest.study';

    protected ?FHIROid $uid = null;
    protected ?FHIRReference $imagingStudy = null;

    /** @var FHIRReference[] */
    protected array $endpoint = [];

    /** @var FHIRImagingManifestStudySeries[] */
    protected array $series = [];

    public function getUid(): ?FHIROid
    {
        return $this->uid;
    }

    public function setUid(string|FHIROid|null $value): self
    {
        $this->uid = is_string($value) ? (new FHIROid())->setValue($value) : $value;

        return $this;
    }

    public function getImagingStudy(): ?FHIRReference
    {
        return $this->imagingStudy;
    }

    public function setImagingStudy(?FHIRReference $value): self
    {
        $this->imagingStudy = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEndpoint(): array
    {
        return $this->endpoint;
    }

    public function setEndpoint(?FHIRReference ...$value): self
    {
        $this->endpoint = array_filter($value);

        return $this;
    }

    public function addEndpoint(?FHIRReference ...$value): self
    {
        $this->endpoint = array_filter(array_merge($this->endpoint, $value));

        return $this;
    }

    /**
     * @return FHIRImagingManifestStudySeries[]
     */
    public function getSeries(): array
    {
        return $this->series;
    }

    public function setSeries(?FHIRImagingManifestStudySeries ...$value): self
    {
        $this->series = array_filter($value);

        return $this;
    }

    public function addSeries(?FHIRImagingManifestStudySeries ...$value): self
    {
        $this->series = array_filter(array_merge($this->series, $value));

        return $this;
    }
}
