<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MeasureGroup Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMeasureGroupInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRMeasureGroup extends FHIRBackboneElement implements FHIRMeasureGroupInterface
{
    public const RESOURCE_NAME = 'Measure.group';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRString $name = null;
    protected ?FHIRString $description = null;

    /** @var FHIRMeasureGroupPopulation[] */
    protected array $population = [];

    /** @var FHIRMeasureGroupStratifier[] */
    protected array $stratifier = [];

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRMeasureGroupPopulation[]
     */
    public function getPopulation(): array
    {
        return $this->population;
    }

    public function setPopulation(?FHIRMeasureGroupPopulation ...$value): self
    {
        $this->population = array_filter($value);

        return $this;
    }

    public function addPopulation(?FHIRMeasureGroupPopulation ...$value): self
    {
        $this->population = array_filter(array_merge($this->population, $value));

        return $this;
    }

    /**
     * @return FHIRMeasureGroupStratifier[]
     */
    public function getStratifier(): array
    {
        return $this->stratifier;
    }

    public function setStratifier(?FHIRMeasureGroupStratifier ...$value): self
    {
        $this->stratifier = array_filter($value);

        return $this;
    }

    public function addStratifier(?FHIRMeasureGroupStratifier ...$value): self
    {
        $this->stratifier = array_filter(array_merge($this->stratifier, $value));

        return $this;
    }
}
