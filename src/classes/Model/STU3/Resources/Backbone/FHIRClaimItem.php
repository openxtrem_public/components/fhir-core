<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClaimItem Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRClaimItemInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAddress;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRPositiveInt;

class FHIRClaimItem extends FHIRBackboneElement implements FHIRClaimItemInterface
{
    public const RESOURCE_NAME = 'Claim.item';

    protected ?FHIRPositiveInt $sequence = null;

    /** @var FHIRPositiveInt[] */
    protected array $careTeamLinkId = [];

    /** @var FHIRPositiveInt[] */
    protected array $diagnosisLinkId = [];

    /** @var FHIRPositiveInt[] */
    protected array $procedureLinkId = [];

    /** @var FHIRPositiveInt[] */
    protected array $informationLinkId = [];
    protected ?FHIRCodeableConcept $revenue = null;
    protected ?FHIRCodeableConcept $category = null;
    protected ?FHIRCodeableConcept $service = null;

    /** @var FHIRCodeableConcept[] */
    protected array $modifier = [];

    /** @var FHIRCodeableConcept[] */
    protected array $programCode = [];
    protected FHIRDate|FHIRPeriod|null $serviced = null;
    protected FHIRCodeableConcept|FHIRAddress|FHIRReference|null $location = null;
    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRMoney $unitPrice = null;
    protected ?FHIRDecimal $factor = null;
    protected ?FHIRMoney $net = null;

    /** @var FHIRReference[] */
    protected array $udi = [];
    protected ?FHIRCodeableConcept $bodySite = null;

    /** @var FHIRCodeableConcept[] */
    protected array $subSite = [];

    /** @var FHIRReference[] */
    protected array $encounter = [];

    /** @var FHIRClaimItemDetail[] */
    protected array $detail = [];

    public function getSequence(): ?FHIRPositiveInt
    {
        return $this->sequence;
    }

    public function setSequence(int|FHIRPositiveInt|null $value): self
    {
        $this->sequence = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRPositiveInt[]
     */
    public function getCareTeamLinkId(): array
    {
        return $this->careTeamLinkId;
    }

    public function setCareTeamLinkId(int|FHIRPositiveInt|null ...$value): self
    {
        $this->careTeamLinkId = [];
        $this->addCareTeamLinkId(...$value);

        return $this;
    }

    public function addCareTeamLinkId(int|FHIRPositiveInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRPositiveInt())->setValue($v) : $v, $value);

        $this->careTeamLinkId = array_filter(array_merge($this->careTeamLinkId, $values));

        return $this;
    }

    /**
     * @return FHIRPositiveInt[]
     */
    public function getDiagnosisLinkId(): array
    {
        return $this->diagnosisLinkId;
    }

    public function setDiagnosisLinkId(int|FHIRPositiveInt|null ...$value): self
    {
        $this->diagnosisLinkId = [];
        $this->addDiagnosisLinkId(...$value);

        return $this;
    }

    public function addDiagnosisLinkId(int|FHIRPositiveInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRPositiveInt())->setValue($v) : $v, $value);

        $this->diagnosisLinkId = array_filter(array_merge($this->diagnosisLinkId, $values));

        return $this;
    }

    /**
     * @return FHIRPositiveInt[]
     */
    public function getProcedureLinkId(): array
    {
        return $this->procedureLinkId;
    }

    public function setProcedureLinkId(int|FHIRPositiveInt|null ...$value): self
    {
        $this->procedureLinkId = [];
        $this->addProcedureLinkId(...$value);

        return $this;
    }

    public function addProcedureLinkId(int|FHIRPositiveInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRPositiveInt())->setValue($v) : $v, $value);

        $this->procedureLinkId = array_filter(array_merge($this->procedureLinkId, $values));

        return $this;
    }

    /**
     * @return FHIRPositiveInt[]
     */
    public function getInformationLinkId(): array
    {
        return $this->informationLinkId;
    }

    public function setInformationLinkId(int|FHIRPositiveInt|null ...$value): self
    {
        $this->informationLinkId = [];
        $this->addInformationLinkId(...$value);

        return $this;
    }

    public function addInformationLinkId(int|FHIRPositiveInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRPositiveInt())->setValue($v) : $v, $value);

        $this->informationLinkId = array_filter(array_merge($this->informationLinkId, $values));

        return $this;
    }

    public function getRevenue(): ?FHIRCodeableConcept
    {
        return $this->revenue;
    }

    public function setRevenue(?FHIRCodeableConcept $value): self
    {
        $this->revenue = $value;

        return $this;
    }

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getService(): ?FHIRCodeableConcept
    {
        return $this->service;
    }

    public function setService(?FHIRCodeableConcept $value): self
    {
        $this->service = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getModifier(): array
    {
        return $this->modifier;
    }

    public function setModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->modifier = array_filter($value);

        return $this;
    }

    public function addModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->modifier = array_filter(array_merge($this->modifier, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getProgramCode(): array
    {
        return $this->programCode;
    }

    public function setProgramCode(?FHIRCodeableConcept ...$value): self
    {
        $this->programCode = array_filter($value);

        return $this;
    }

    public function addProgramCode(?FHIRCodeableConcept ...$value): self
    {
        $this->programCode = array_filter(array_merge($this->programCode, $value));

        return $this;
    }

    public function getServiced(): FHIRDate|FHIRPeriod|null
    {
        return $this->serviced;
    }

    public function setServiced(FHIRDate|FHIRPeriod|null $value): self
    {
        $this->serviced = $value;

        return $this;
    }

    public function getLocation(): FHIRCodeableConcept|FHIRAddress|FHIRReference|null
    {
        return $this->location;
    }

    public function setLocation(FHIRCodeableConcept|FHIRAddress|FHIRReference|null $value): self
    {
        $this->location = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getUnitPrice(): ?FHIRMoney
    {
        return $this->unitPrice;
    }

    public function setUnitPrice(?FHIRMoney $value): self
    {
        $this->unitPrice = $value;

        return $this;
    }

    public function getFactor(): ?FHIRDecimal
    {
        return $this->factor;
    }

    public function setFactor(float|FHIRDecimal|null $value): self
    {
        $this->factor = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getNet(): ?FHIRMoney
    {
        return $this->net;
    }

    public function setNet(?FHIRMoney $value): self
    {
        $this->net = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getUdi(): array
    {
        return $this->udi;
    }

    public function setUdi(?FHIRReference ...$value): self
    {
        $this->udi = array_filter($value);

        return $this;
    }

    public function addUdi(?FHIRReference ...$value): self
    {
        $this->udi = array_filter(array_merge($this->udi, $value));

        return $this;
    }

    public function getBodySite(): ?FHIRCodeableConcept
    {
        return $this->bodySite;
    }

    public function setBodySite(?FHIRCodeableConcept $value): self
    {
        $this->bodySite = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSubSite(): array
    {
        return $this->subSite;
    }

    public function setSubSite(?FHIRCodeableConcept ...$value): self
    {
        $this->subSite = array_filter($value);

        return $this;
    }

    public function addSubSite(?FHIRCodeableConcept ...$value): self
    {
        $this->subSite = array_filter(array_merge($this->subSite, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEncounter(): array
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference ...$value): self
    {
        $this->encounter = array_filter($value);

        return $this;
    }

    public function addEncounter(?FHIRReference ...$value): self
    {
        $this->encounter = array_filter(array_merge($this->encounter, $value));

        return $this;
    }

    /**
     * @return FHIRClaimItemDetail[]
     */
    public function getDetail(): array
    {
        return $this->detail;
    }

    public function setDetail(?FHIRClaimItemDetail ...$value): self
    {
        $this->detail = array_filter($value);

        return $this;
    }

    public function addDetail(?FHIRClaimItemDetail ...$value): self
    {
        $this->detail = array_filter(array_merge($this->detail, $value));

        return $this;
    }
}
