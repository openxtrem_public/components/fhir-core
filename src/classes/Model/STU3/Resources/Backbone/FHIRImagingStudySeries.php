<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImagingStudySeries Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImagingStudySeriesInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIROid;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUnsignedInt;

class FHIRImagingStudySeries extends FHIRBackboneElement implements FHIRImagingStudySeriesInterface
{
    public const RESOURCE_NAME = 'ImagingStudy.series';

    protected ?FHIROid $uid = null;
    protected ?FHIRUnsignedInt $number = null;
    protected ?FHIRCoding $modality = null;
    protected ?FHIRString $description = null;
    protected ?FHIRUnsignedInt $numberOfInstances = null;
    protected ?FHIRCode $availability = null;

    /** @var FHIRReference[] */
    protected array $endpoint = [];
    protected ?FHIRCoding $bodySite = null;
    protected ?FHIRCoding $laterality = null;
    protected ?FHIRDateTime $started = null;

    /** @var FHIRReference[] */
    protected array $performer = [];

    /** @var FHIRImagingStudySeriesInstance[] */
    protected array $instance = [];

    public function getUid(): ?FHIROid
    {
        return $this->uid;
    }

    public function setUid(string|FHIROid|null $value): self
    {
        $this->uid = is_string($value) ? (new FHIROid())->setValue($value) : $value;

        return $this;
    }

    public function getNumber(): ?FHIRUnsignedInt
    {
        return $this->number;
    }

    public function setNumber(int|FHIRUnsignedInt|null $value): self
    {
        $this->number = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getModality(): ?FHIRCoding
    {
        return $this->modality;
    }

    public function setModality(?FHIRCoding $value): self
    {
        $this->modality = $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getNumberOfInstances(): ?FHIRUnsignedInt
    {
        return $this->numberOfInstances;
    }

    public function setNumberOfInstances(int|FHIRUnsignedInt|null $value): self
    {
        $this->numberOfInstances = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getAvailability(): ?FHIRCode
    {
        return $this->availability;
    }

    public function setAvailability(string|FHIRCode|null $value): self
    {
        $this->availability = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEndpoint(): array
    {
        return $this->endpoint;
    }

    public function setEndpoint(?FHIRReference ...$value): self
    {
        $this->endpoint = array_filter($value);

        return $this;
    }

    public function addEndpoint(?FHIRReference ...$value): self
    {
        $this->endpoint = array_filter(array_merge($this->endpoint, $value));

        return $this;
    }

    public function getBodySite(): ?FHIRCoding
    {
        return $this->bodySite;
    }

    public function setBodySite(?FHIRCoding $value): self
    {
        $this->bodySite = $value;

        return $this;
    }

    public function getLaterality(): ?FHIRCoding
    {
        return $this->laterality;
    }

    public function setLaterality(?FHIRCoding $value): self
    {
        $this->laterality = $value;

        return $this;
    }

    public function getStarted(): ?FHIRDateTime
    {
        return $this->started;
    }

    public function setStarted(string|FHIRDateTime|null $value): self
    {
        $this->started = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPerformer(): array
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRReference ...$value): self
    {
        $this->performer = array_filter($value);

        return $this;
    }

    public function addPerformer(?FHIRReference ...$value): self
    {
        $this->performer = array_filter(array_merge($this->performer, $value));

        return $this;
    }

    /**
     * @return FHIRImagingStudySeriesInstance[]
     */
    public function getInstance(): array
    {
        return $this->instance;
    }

    public function setInstance(?FHIRImagingStudySeriesInstance ...$value): self
    {
        $this->instance = array_filter($value);

        return $this;
    }

    public function addInstance(?FHIRImagingStudySeriesInstance ...$value): self
    {
        $this->instance = array_filter(array_merge($this->instance, $value));

        return $this;
    }
}
