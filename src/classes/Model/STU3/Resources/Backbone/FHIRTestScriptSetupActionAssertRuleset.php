<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestScriptSetupActionAssertRuleset Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestScriptSetupActionAssertRulesetInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRId;

class FHIRTestScriptSetupActionAssertRuleset extends FHIRBackboneElement implements FHIRTestScriptSetupActionAssertRulesetInterface
{
    public const RESOURCE_NAME = 'TestScript.setup.action.assert.ruleset';

    protected ?FHIRId $rulesetId = null;

    /** @var FHIRTestScriptSetupActionAssertRulesetRule[] */
    protected array $rule = [];

    public function getRulesetId(): ?FHIRId
    {
        return $this->rulesetId;
    }

    public function setRulesetId(string|FHIRId|null $value): self
    {
        $this->rulesetId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRTestScriptSetupActionAssertRulesetRule[]
     */
    public function getRule(): array
    {
        return $this->rule;
    }

    public function setRule(?FHIRTestScriptSetupActionAssertRulesetRule ...$value): self
    {
        $this->rule = array_filter($value);

        return $this;
    }

    public function addRule(?FHIRTestScriptSetupActionAssertRulesetRule ...$value): self
    {
        $this->rule = array_filter(array_merge($this->rule, $value));

        return $this;
    }
}
