<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CapabilityStatementRest Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCapabilityStatementRestInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;

class FHIRCapabilityStatementRest extends FHIRBackboneElement implements FHIRCapabilityStatementRestInterface
{
    public const RESOURCE_NAME = 'CapabilityStatement.rest';

    protected ?FHIRCode $mode = null;
    protected ?FHIRString $documentation = null;
    protected ?FHIRCapabilityStatementRestSecurity $security = null;

    /** @var FHIRCapabilityStatementRestResource[] */
    protected array $resource = [];

    /** @var FHIRCapabilityStatementRestInteraction[] */
    protected array $interaction = [];

    /** @var FHIRCapabilityStatementRestResourceSearchParam[] */
    protected array $searchParam = [];

    /** @var FHIRCapabilityStatementRestOperation[] */
    protected array $operation = [];

    /** @var FHIRUri[] */
    protected array $compartment = [];

    public function getMode(): ?FHIRCode
    {
        return $this->mode;
    }

    public function setMode(string|FHIRCode|null $value): self
    {
        $this->mode = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDocumentation(): ?FHIRString
    {
        return $this->documentation;
    }

    public function setDocumentation(string|FHIRString|null $value): self
    {
        $this->documentation = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSecurity(): ?FHIRCapabilityStatementRestSecurity
    {
        return $this->security;
    }

    public function setSecurity(?FHIRCapabilityStatementRestSecurity $value): self
    {
        $this->security = $value;

        return $this;
    }

    /**
     * @return FHIRCapabilityStatementRestResource[]
     */
    public function getResource(): array
    {
        return $this->resource;
    }

    public function setResource(?FHIRCapabilityStatementRestResource ...$value): self
    {
        $this->resource = array_filter($value);

        return $this;
    }

    public function addResource(?FHIRCapabilityStatementRestResource ...$value): self
    {
        $this->resource = array_filter(array_merge($this->resource, $value));

        return $this;
    }

    /**
     * @return FHIRCapabilityStatementRestInteraction[]
     */
    public function getInteraction(): array
    {
        return $this->interaction;
    }

    public function setInteraction(?FHIRCapabilityStatementRestInteraction ...$value): self
    {
        $this->interaction = array_filter($value);

        return $this;
    }

    public function addInteraction(?FHIRCapabilityStatementRestInteraction ...$value): self
    {
        $this->interaction = array_filter(array_merge($this->interaction, $value));

        return $this;
    }

    /**
     * @return FHIRCapabilityStatementRestResourceSearchParam[]
     */
    public function getSearchParam(): array
    {
        return $this->searchParam;
    }

    public function setSearchParam(?FHIRCapabilityStatementRestResourceSearchParam ...$value): self
    {
        $this->searchParam = array_filter($value);

        return $this;
    }

    public function addSearchParam(?FHIRCapabilityStatementRestResourceSearchParam ...$value): self
    {
        $this->searchParam = array_filter(array_merge($this->searchParam, $value));

        return $this;
    }

    /**
     * @return FHIRCapabilityStatementRestOperation[]
     */
    public function getOperation(): array
    {
        return $this->operation;
    }

    public function setOperation(?FHIRCapabilityStatementRestOperation ...$value): self
    {
        $this->operation = array_filter($value);

        return $this;
    }

    public function addOperation(?FHIRCapabilityStatementRestOperation ...$value): self
    {
        $this->operation = array_filter(array_merge($this->operation, $value));

        return $this;
    }

    /**
     * @return FHIRUri[]
     */
    public function getCompartment(): array
    {
        return $this->compartment;
    }

    public function setCompartment(string|FHIRUri|null ...$value): self
    {
        $this->compartment = [];
        $this->addCompartment(...$value);

        return $this;
    }

    public function addCompartment(string|FHIRUri|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUri())->setValue($v) : $v, $value);

        $this->compartment = array_filter(array_merge($this->compartment, $values));

        return $this;
    }
}
