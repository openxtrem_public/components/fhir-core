<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CapabilityStatementRestOperation Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCapabilityStatementRestOperationInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRCapabilityStatementRestOperation extends FHIRBackboneElement implements FHIRCapabilityStatementRestOperationInterface
{
    public const RESOURCE_NAME = 'CapabilityStatement.rest.operation';

    protected ?FHIRString $name = null;
    protected ?FHIRReference $definition = null;

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDefinition(): ?FHIRReference
    {
        return $this->definition;
    }

    public function setDefinition(?FHIRReference $value): self
    {
        $this->definition = $value;

        return $this;
    }
}
