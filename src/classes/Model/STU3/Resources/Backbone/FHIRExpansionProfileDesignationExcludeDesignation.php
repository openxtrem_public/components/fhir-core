<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExpansionProfileDesignationExcludeDesignation Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExpansionProfileDesignationExcludeDesignationInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;

class FHIRExpansionProfileDesignationExcludeDesignation extends FHIRBackboneElement implements FHIRExpansionProfileDesignationExcludeDesignationInterface
{
    public const RESOURCE_NAME = 'ExpansionProfile.designation.exclude.designation';

    protected ?FHIRCode $language = null;
    protected ?FHIRCoding $use = null;

    public function getLanguage(): ?FHIRCode
    {
        return $this->language;
    }

    public function setLanguage(string|FHIRCode|null $value): self
    {
        $this->language = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getUse(): ?FHIRCoding
    {
        return $this->use;
    }

    public function setUse(?FHIRCoding $value): self
    {
        $this->use = $value;

        return $this;
    }
}
