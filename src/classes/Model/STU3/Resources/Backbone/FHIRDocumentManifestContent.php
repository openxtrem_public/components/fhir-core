<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DocumentManifestContent Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDocumentManifestContentInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;

class FHIRDocumentManifestContent extends FHIRBackboneElement implements FHIRDocumentManifestContentInterface
{
    public const RESOURCE_NAME = 'DocumentManifest.content';

    protected FHIRAttachment|FHIRReference|null $p = null;

    public function getP(): FHIRAttachment|FHIRReference|null
    {
        return $this->p;
    }

    public function setP(FHIRAttachment|FHIRReference|null $value): self
    {
        $this->p = $value;

        return $this;
    }
}
