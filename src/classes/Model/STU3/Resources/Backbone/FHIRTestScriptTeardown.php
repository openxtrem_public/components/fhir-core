<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestScriptTeardown Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestScriptTeardownInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;

class FHIRTestScriptTeardown extends FHIRBackboneElement implements FHIRTestScriptTeardownInterface
{
    public const RESOURCE_NAME = 'TestScript.teardown';

    /** @var FHIRTestScriptTeardownAction[] */
    protected array $action = [];

    /**
     * @return FHIRTestScriptTeardownAction[]
     */
    public function getAction(): array
    {
        return $this->action;
    }

    public function setAction(?FHIRTestScriptTeardownAction ...$value): self
    {
        $this->action = array_filter($value);

        return $this;
    }

    public function addAction(?FHIRTestScriptTeardownAction ...$value): self
    {
        $this->action = array_filter(array_merge($this->action, $value));

        return $this;
    }
}
