<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR StructureMapGroupRuleSource Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRStructureMapGroupRuleSourceInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRStructureMapGroupRuleSource extends FHIRBackboneElement implements FHIRStructureMapGroupRuleSourceInterface
{
    public const RESOURCE_NAME = 'StructureMap.group.rule.source';

    protected ?FHIRId $context = null;
    protected ?FHIRInteger $min = null;
    protected ?FHIRString $max = null;
    protected ?FHIRString $type = null;
    protected ?FHIRElement $defaultValue = null;
    protected ?FHIRString $element = null;
    protected ?FHIRCode $listMode = null;
    protected ?FHIRId $variable = null;
    protected ?FHIRString $condition = null;
    protected ?FHIRString $check = null;

    public function getContext(): ?FHIRId
    {
        return $this->context;
    }

    public function setContext(string|FHIRId|null $value): self
    {
        $this->context = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getMin(): ?FHIRInteger
    {
        return $this->min;
    }

    public function setMin(int|FHIRInteger|null $value): self
    {
        $this->min = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getMax(): ?FHIRString
    {
        return $this->max;
    }

    public function setMax(string|FHIRString|null $value): self
    {
        $this->max = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRString
    {
        return $this->type;
    }

    public function setType(string|FHIRString|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDefaultValue(): ?FHIRElement
    {
        return $this->defaultValue;
    }

    public function setDefaultValue(?FHIRElement $value): self
    {
        $this->defaultValue = $value;

        return $this;
    }

    public function getElement(): ?FHIRString
    {
        return $this->element;
    }

    public function setElement(string|FHIRString|null $value): self
    {
        $this->element = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getListMode(): ?FHIRCode
    {
        return $this->listMode;
    }

    public function setListMode(string|FHIRCode|null $value): self
    {
        $this->listMode = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getVariable(): ?FHIRId
    {
        return $this->variable;
    }

    public function setVariable(string|FHIRId|null $value): self
    {
        $this->variable = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getCondition(): ?FHIRString
    {
        return $this->condition;
    }

    public function setCondition(string|FHIRString|null $value): self
    {
        $this->condition = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCheck(): ?FHIRString
    {
        return $this->check;
    }

    public function setCheck(string|FHIRString|null $value): self
    {
        $this->check = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
