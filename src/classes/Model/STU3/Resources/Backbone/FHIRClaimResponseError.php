<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClaimResponseError Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRClaimResponseErrorInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRPositiveInt;

class FHIRClaimResponseError extends FHIRBackboneElement implements FHIRClaimResponseErrorInterface
{
    public const RESOURCE_NAME = 'ClaimResponse.error';

    protected ?FHIRPositiveInt $sequenceLinkId = null;
    protected ?FHIRPositiveInt $detailSequenceLinkId = null;
    protected ?FHIRPositiveInt $subdetailSequenceLinkId = null;
    protected ?FHIRCodeableConcept $code = null;

    public function getSequenceLinkId(): ?FHIRPositiveInt
    {
        return $this->sequenceLinkId;
    }

    public function setSequenceLinkId(int|FHIRPositiveInt|null $value): self
    {
        $this->sequenceLinkId = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getDetailSequenceLinkId(): ?FHIRPositiveInt
    {
        return $this->detailSequenceLinkId;
    }

    public function setDetailSequenceLinkId(int|FHIRPositiveInt|null $value): self
    {
        $this->detailSequenceLinkId = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getSubdetailSequenceLinkId(): ?FHIRPositiveInt
    {
        return $this->subdetailSequenceLinkId;
    }

    public function setSubdetailSequenceLinkId(int|FHIRPositiveInt|null $value): self
    {
        $this->subdetailSequenceLinkId = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }
}
