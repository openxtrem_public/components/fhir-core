<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ProvenanceAgent Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRProvenanceAgentInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;

class FHIRProvenanceAgent extends FHIRBackboneElement implements FHIRProvenanceAgentInterface
{
    public const RESOURCE_NAME = 'Provenance.agent';

    /** @var FHIRCodeableConcept[] */
    protected array $role = [];
    protected FHIRUri|FHIRReference|null $who = null;
    protected FHIRUri|FHIRReference|null $onBehalfOf = null;
    protected ?FHIRCodeableConcept $relatedAgentType = null;

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getRole(): array
    {
        return $this->role;
    }

    public function setRole(?FHIRCodeableConcept ...$value): self
    {
        $this->role = array_filter($value);

        return $this;
    }

    public function addRole(?FHIRCodeableConcept ...$value): self
    {
        $this->role = array_filter(array_merge($this->role, $value));

        return $this;
    }

    public function getWho(): FHIRUri|FHIRReference|null
    {
        return $this->who;
    }

    public function setWho(FHIRUri|FHIRReference|null $value): self
    {
        $this->who = $value;

        return $this;
    }

    public function getOnBehalfOf(): FHIRUri|FHIRReference|null
    {
        return $this->onBehalfOf;
    }

    public function setOnBehalfOf(FHIRUri|FHIRReference|null $value): self
    {
        $this->onBehalfOf = $value;

        return $this;
    }

    public function getRelatedAgentType(): ?FHIRCodeableConcept
    {
        return $this->relatedAgentType;
    }

    public function setRelatedAgentType(?FHIRCodeableConcept $value): self
    {
        $this->relatedAgentType = $value;

        return $this;
    }
}
