<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CarePlanActivity Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCarePlanActivityInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;

class FHIRCarePlanActivity extends FHIRBackboneElement implements FHIRCarePlanActivityInterface
{
    public const RESOURCE_NAME = 'CarePlan.activity';

    /** @var FHIRCodeableConcept[] */
    protected array $outcomeCodeableConcept = [];

    /** @var FHIRReference[] */
    protected array $outcomeReference = [];

    /** @var FHIRAnnotation[] */
    protected array $progress = [];
    protected ?FHIRElement $reference = null;
    protected ?FHIRCarePlanActivityDetail $detail = null;

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getOutcomeCodeableConcept(): array
    {
        return $this->outcomeCodeableConcept;
    }

    public function setOutcomeCodeableConcept(?FHIRCodeableConcept ...$value): self
    {
        $this->outcomeCodeableConcept = array_filter($value);

        return $this;
    }

    public function addOutcomeCodeableConcept(?FHIRCodeableConcept ...$value): self
    {
        $this->outcomeCodeableConcept = array_filter(array_merge($this->outcomeCodeableConcept, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getOutcomeReference(): array
    {
        return $this->outcomeReference;
    }

    public function setOutcomeReference(?FHIRReference ...$value): self
    {
        $this->outcomeReference = array_filter($value);

        return $this;
    }

    public function addOutcomeReference(?FHIRReference ...$value): self
    {
        $this->outcomeReference = array_filter(array_merge($this->outcomeReference, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getProgress(): array
    {
        return $this->progress;
    }

    public function setProgress(?FHIRAnnotation ...$value): self
    {
        $this->progress = array_filter($value);

        return $this;
    }

    public function addProgress(?FHIRAnnotation ...$value): self
    {
        $this->progress = array_filter(array_merge($this->progress, $value));

        return $this;
    }

    public function getReference(): ?FHIRElement
    {
        return $this->reference;
    }

    public function setReference(?FHIRElement $value): self
    {
        $this->reference = $value;

        return $this;
    }

    public function getDetail(): ?FHIRCarePlanActivityDetail
    {
        return $this->detail;
    }

    public function setDetail(?FHIRCarePlanActivityDetail $value): self
    {
        $this->detail = $value;

        return $this;
    }
}
