<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR StructureMapGroupRuleTarget Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRStructureMapGroupRuleTargetInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRStructureMapGroupRuleTarget extends FHIRBackboneElement implements FHIRStructureMapGroupRuleTargetInterface
{
    public const RESOURCE_NAME = 'StructureMap.group.rule.target';

    protected ?FHIRId $context = null;
    protected ?FHIRCode $contextType = null;
    protected ?FHIRString $element = null;
    protected ?FHIRId $variable = null;

    /** @var FHIRCode[] */
    protected array $listMode = [];
    protected ?FHIRId $listRuleId = null;
    protected ?FHIRCode $transform = null;

    /** @var FHIRStructureMapGroupRuleTargetParameter[] */
    protected array $parameter = [];

    public function getContext(): ?FHIRId
    {
        return $this->context;
    }

    public function setContext(string|FHIRId|null $value): self
    {
        $this->context = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getContextType(): ?FHIRCode
    {
        return $this->contextType;
    }

    public function setContextType(string|FHIRCode|null $value): self
    {
        $this->contextType = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getElement(): ?FHIRString
    {
        return $this->element;
    }

    public function setElement(string|FHIRString|null $value): self
    {
        $this->element = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getVariable(): ?FHIRId
    {
        return $this->variable;
    }

    public function setVariable(string|FHIRId|null $value): self
    {
        $this->variable = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getListMode(): array
    {
        return $this->listMode;
    }

    public function setListMode(string|FHIRCode|null ...$value): self
    {
        $this->listMode = [];
        $this->addListMode(...$value);

        return $this;
    }

    public function addListMode(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->listMode = array_filter(array_merge($this->listMode, $values));

        return $this;
    }

    public function getListRuleId(): ?FHIRId
    {
        return $this->listRuleId;
    }

    public function setListRuleId(string|FHIRId|null $value): self
    {
        $this->listRuleId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getTransform(): ?FHIRCode
    {
        return $this->transform;
    }

    public function setTransform(string|FHIRCode|null $value): self
    {
        $this->transform = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRStructureMapGroupRuleTargetParameter[]
     */
    public function getParameter(): array
    {
        return $this->parameter;
    }

    public function setParameter(?FHIRStructureMapGroupRuleTargetParameter ...$value): self
    {
        $this->parameter = array_filter($value);

        return $this;
    }

    public function addParameter(?FHIRStructureMapGroupRuleTargetParameter ...$value): self
    {
        $this->parameter = array_filter(array_merge($this->parameter, $value));

        return $this;
    }
}
