<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClaimInsurance Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRClaimInsuranceInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRClaimInsurance extends FHIRBackboneElement implements FHIRClaimInsuranceInterface
{
    public const RESOURCE_NAME = 'Claim.insurance';

    protected ?FHIRPositiveInt $sequence = null;
    protected ?FHIRBoolean $focal = null;
    protected ?FHIRReference $coverage = null;
    protected ?FHIRString $businessArrangement = null;

    /** @var FHIRString[] */
    protected array $preAuthRef = [];
    protected ?FHIRReference $claimResponse = null;

    public function getSequence(): ?FHIRPositiveInt
    {
        return $this->sequence;
    }

    public function setSequence(int|FHIRPositiveInt|null $value): self
    {
        $this->sequence = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getFocal(): ?FHIRBoolean
    {
        return $this->focal;
    }

    public function setFocal(bool|FHIRBoolean|null $value): self
    {
        $this->focal = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getCoverage(): ?FHIRReference
    {
        return $this->coverage;
    }

    public function setCoverage(?FHIRReference $value): self
    {
        $this->coverage = $value;

        return $this;
    }

    public function getBusinessArrangement(): ?FHIRString
    {
        return $this->businessArrangement;
    }

    public function setBusinessArrangement(string|FHIRString|null $value): self
    {
        $this->businessArrangement = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getPreAuthRef(): array
    {
        return $this->preAuthRef;
    }

    public function setPreAuthRef(string|FHIRString|null ...$value): self
    {
        $this->preAuthRef = [];
        $this->addPreAuthRef(...$value);

        return $this;
    }

    public function addPreAuthRef(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->preAuthRef = array_filter(array_merge($this->preAuthRef, $values));

        return $this;
    }

    public function getClaimResponse(): ?FHIRReference
    {
        return $this->claimResponse;
    }

    public function setClaimResponse(?FHIRReference $value): self
    {
        $this->claimResponse = $value;

        return $this;
    }
}
