<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SubscriptionChannel Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRSubscriptionChannelInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;

class FHIRSubscriptionChannel extends FHIRBackboneElement implements FHIRSubscriptionChannelInterface
{
    public const RESOURCE_NAME = 'Subscription.channel';

    protected ?FHIRCode $type = null;
    protected ?FHIRUri $endpoint = null;
    protected ?FHIRString $payload = null;

    /** @var FHIRString[] */
    protected array $header = [];

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getEndpoint(): ?FHIRUri
    {
        return $this->endpoint;
    }

    public function setEndpoint(string|FHIRUri|null $value): self
    {
        $this->endpoint = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getPayload(): ?FHIRString
    {
        return $this->payload;
    }

    public function setPayload(string|FHIRString|null $value): self
    {
        $this->payload = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getHeader(): array
    {
        return $this->header;
    }

    public function setHeader(string|FHIRString|null ...$value): self
    {
        $this->header = [];
        $this->addHeader(...$value);

        return $this;
    }

    public function addHeader(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->header = array_filter(array_merge($this->header, $values));

        return $this;
    }
}
