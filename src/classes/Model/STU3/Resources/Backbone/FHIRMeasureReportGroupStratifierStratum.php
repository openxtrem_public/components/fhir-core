<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MeasureReportGroupStratifierStratum Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMeasureReportGroupStratifierStratumInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRMeasureReportGroupStratifierStratum extends FHIRBackboneElement implements FHIRMeasureReportGroupStratifierStratumInterface
{
    public const RESOURCE_NAME = 'MeasureReport.group.stratifier.stratum';

    protected ?FHIRString $value = null;

    /** @var FHIRMeasureReportGroupStratifierStratumPopulation[] */
    protected array $population = [];
    protected ?FHIRDecimal $measureScore = null;

    public function getValue(): ?FHIRString
    {
        return $this->value;
    }

    public function setValue(string|FHIRString|null $value): self
    {
        $this->value = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRMeasureReportGroupStratifierStratumPopulation[]
     */
    public function getPopulation(): array
    {
        return $this->population;
    }

    public function setPopulation(?FHIRMeasureReportGroupStratifierStratumPopulation ...$value): self
    {
        $this->population = array_filter($value);

        return $this;
    }

    public function addPopulation(?FHIRMeasureReportGroupStratifierStratumPopulation ...$value): self
    {
        $this->population = array_filter(array_merge($this->population, $value));

        return $this;
    }

    public function getMeasureScore(): ?FHIRDecimal
    {
        return $this->measureScore;
    }

    public function setMeasureScore(float|FHIRDecimal|null $value): self
    {
        $this->measureScore = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }
}
