<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceComponentProductionSpecification Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceComponentProductionSpecificationInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRDeviceComponentProductionSpecification extends FHIRBackboneElement implements FHIRDeviceComponentProductionSpecificationInterface
{
    public const RESOURCE_NAME = 'DeviceComponent.productionSpecification';

    protected ?FHIRCodeableConcept $specType = null;
    protected ?FHIRIdentifier $componentId = null;
    protected ?FHIRString $productionSpec = null;

    public function getSpecType(): ?FHIRCodeableConcept
    {
        return $this->specType;
    }

    public function setSpecType(?FHIRCodeableConcept $value): self
    {
        $this->specType = $value;

        return $this;
    }

    public function getComponentId(): ?FHIRIdentifier
    {
        return $this->componentId;
    }

    public function setComponentId(?FHIRIdentifier $value): self
    {
        $this->componentId = $value;

        return $this;
    }

    public function getProductionSpec(): ?FHIRString
    {
        return $this->productionSpec;
    }

    public function setProductionSpec(string|FHIRString|null $value): self
    {
        $this->productionSpec = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
