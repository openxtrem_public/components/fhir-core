<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CapabilityStatementRestSecurityCertificate Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCapabilityStatementRestSecurityCertificateInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBase64Binary;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;

class FHIRCapabilityStatementRestSecurityCertificate extends FHIRBackboneElement implements FHIRCapabilityStatementRestSecurityCertificateInterface
{
    public const RESOURCE_NAME = 'CapabilityStatement.rest.security.certificate';

    protected ?FHIRCode $type = null;
    protected ?FHIRBase64Binary $blob = null;

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getBlob(): ?FHIRBase64Binary
    {
        return $this->blob;
    }

    public function setBlob(string|FHIRBase64Binary|null $value): self
    {
        $this->blob = is_string($value) ? (new FHIRBase64Binary())->setValue($value) : $value;

        return $this;
    }
}
