<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DocumentReferenceContext Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDocumentReferenceContextInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;

class FHIRDocumentReferenceContext extends FHIRBackboneElement implements FHIRDocumentReferenceContextInterface
{
    public const RESOURCE_NAME = 'DocumentReference.context';

    protected ?FHIRReference $encounter = null;

    /** @var FHIRCodeableConcept[] */
    protected array $event = [];
    protected ?FHIRPeriod $period = null;
    protected ?FHIRCodeableConcept $facilityType = null;
    protected ?FHIRCodeableConcept $practiceSetting = null;
    protected ?FHIRReference $sourcePatientInfo = null;

    /** @var FHIRDocumentReferenceContextRelated[] */
    protected array $related = [];

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getEvent(): array
    {
        return $this->event;
    }

    public function setEvent(?FHIRCodeableConcept ...$value): self
    {
        $this->event = array_filter($value);

        return $this;
    }

    public function addEvent(?FHIRCodeableConcept ...$value): self
    {
        $this->event = array_filter(array_merge($this->event, $value));

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    public function getFacilityType(): ?FHIRCodeableConcept
    {
        return $this->facilityType;
    }

    public function setFacilityType(?FHIRCodeableConcept $value): self
    {
        $this->facilityType = $value;

        return $this;
    }

    public function getPracticeSetting(): ?FHIRCodeableConcept
    {
        return $this->practiceSetting;
    }

    public function setPracticeSetting(?FHIRCodeableConcept $value): self
    {
        $this->practiceSetting = $value;

        return $this;
    }

    public function getSourcePatientInfo(): ?FHIRReference
    {
        return $this->sourcePatientInfo;
    }

    public function setSourcePatientInfo(?FHIRReference $value): self
    {
        $this->sourcePatientInfo = $value;

        return $this;
    }

    /**
     * @return FHIRDocumentReferenceContextRelated[]
     */
    public function getRelated(): array
    {
        return $this->related;
    }

    public function setRelated(?FHIRDocumentReferenceContextRelated ...$value): self
    {
        $this->related = array_filter($value);

        return $this;
    }

    public function addRelated(?FHIRDocumentReferenceContextRelated ...$value): self
    {
        $this->related = array_filter(array_merge($this->related, $value));

        return $this;
    }
}
