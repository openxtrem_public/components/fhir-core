<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImmunizationExplanation Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImmunizationExplanationInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;

class FHIRImmunizationExplanation extends FHIRBackboneElement implements FHIRImmunizationExplanationInterface
{
    public const RESOURCE_NAME = 'Immunization.explanation';

    /** @var FHIRCodeableConcept[] */
    protected array $reason = [];

    /** @var FHIRCodeableConcept[] */
    protected array $reasonNotGiven = [];

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReason(): array
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableConcept ...$value): self
    {
        $this->reason = array_filter($value);

        return $this;
    }

    public function addReason(?FHIRCodeableConcept ...$value): self
    {
        $this->reason = array_filter(array_merge($this->reason, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReasonNotGiven(): array
    {
        return $this->reasonNotGiven;
    }

    public function setReasonNotGiven(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonNotGiven = array_filter($value);

        return $this;
    }

    public function addReasonNotGiven(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonNotGiven = array_filter(array_merge($this->reasonNotGiven, $value));

        return $this;
    }
}
