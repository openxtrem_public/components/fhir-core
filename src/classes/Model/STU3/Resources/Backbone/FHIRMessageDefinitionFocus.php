<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MessageDefinitionFocus Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMessageDefinitionFocusInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUnsignedInt;

class FHIRMessageDefinitionFocus extends FHIRBackboneElement implements FHIRMessageDefinitionFocusInterface
{
    public const RESOURCE_NAME = 'MessageDefinition.focus';

    protected ?FHIRCode $code = null;
    protected ?FHIRReference $profile = null;
    protected ?FHIRUnsignedInt $min = null;
    protected ?FHIRString $max = null;

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getProfile(): ?FHIRReference
    {
        return $this->profile;
    }

    public function setProfile(?FHIRReference $value): self
    {
        $this->profile = $value;

        return $this;
    }

    public function getMin(): ?FHIRUnsignedInt
    {
        return $this->min;
    }

    public function setMin(int|FHIRUnsignedInt|null $value): self
    {
        $this->min = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getMax(): ?FHIRString
    {
        return $this->max;
    }

    public function setMax(string|FHIRString|null $value): self
    {
        $this->max = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
