<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ObservationComponent Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRObservationComponentInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRElement;

class FHIRObservationComponent extends FHIRBackboneElement implements FHIRObservationComponentInterface
{
    public const RESOURCE_NAME = 'Observation.component';

    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRElement $value = null;
    protected ?FHIRCodeableConcept $dataAbsentReason = null;
    protected ?FHIRCodeableConcept $interpretation = null;

    /** @var FHIRObservationReferenceRange[] */
    protected array $referenceRange = [];

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getValue(): ?FHIRElement
    {
        return $this->value;
    }

    public function setValue(?FHIRElement $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getDataAbsentReason(): ?FHIRCodeableConcept
    {
        return $this->dataAbsentReason;
    }

    public function setDataAbsentReason(?FHIRCodeableConcept $value): self
    {
        $this->dataAbsentReason = $value;

        return $this;
    }

    public function getInterpretation(): ?FHIRCodeableConcept
    {
        return $this->interpretation;
    }

    public function setInterpretation(?FHIRCodeableConcept $value): self
    {
        $this->interpretation = $value;

        return $this;
    }

    /**
     * @return FHIRObservationReferenceRange[]
     */
    public function getReferenceRange(): array
    {
        return $this->referenceRange;
    }

    public function setReferenceRange(?FHIRObservationReferenceRange ...$value): self
    {
        $this->referenceRange = array_filter($value);

        return $this;
    }

    public function addReferenceRange(?FHIRObservationReferenceRange ...$value): self
    {
        $this->referenceRange = array_filter(array_merge($this->referenceRange, $value));

        return $this;
    }
}
