<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ProcessRequestItem Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRProcessRequestItemInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRInteger;

class FHIRProcessRequestItem extends FHIRBackboneElement implements FHIRProcessRequestItemInterface
{
    public const RESOURCE_NAME = 'ProcessRequest.item';

    protected ?FHIRInteger $sequenceLinkId = null;

    public function getSequenceLinkId(): ?FHIRInteger
    {
        return $this->sequenceLinkId;
    }

    public function setSequenceLinkId(int|FHIRInteger|null $value): self
    {
        $this->sequenceLinkId = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }
}
