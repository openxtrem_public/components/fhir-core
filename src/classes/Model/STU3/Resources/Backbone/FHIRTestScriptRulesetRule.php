<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestScriptRulesetRule Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestScriptRulesetRuleInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRId;

class FHIRTestScriptRulesetRule extends FHIRBackboneElement implements FHIRTestScriptRulesetRuleInterface
{
    public const RESOURCE_NAME = 'TestScript.ruleset.rule';

    protected ?FHIRId $ruleId = null;

    /** @var FHIRTestScriptRulesetRuleParam[] */
    protected array $param = [];

    public function getRuleId(): ?FHIRId
    {
        return $this->ruleId;
    }

    public function setRuleId(string|FHIRId|null $value): self
    {
        $this->ruleId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRTestScriptRulesetRuleParam[]
     */
    public function getParam(): array
    {
        return $this->param;
    }

    public function setParam(?FHIRTestScriptRulesetRuleParam ...$value): self
    {
        $this->param = array_filter($value);

        return $this;
    }

    public function addParam(?FHIRTestScriptRulesetRuleParam ...$value): self
    {
        $this->param = array_filter(array_merge($this->param, $value));

        return $this;
    }
}
