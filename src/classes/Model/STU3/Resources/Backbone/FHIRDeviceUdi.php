<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceUdi Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceUdiInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBase64Binary;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;

class FHIRDeviceUdi extends FHIRBackboneElement implements FHIRDeviceUdiInterface
{
    public const RESOURCE_NAME = 'Device.udi';

    protected ?FHIRString $deviceIdentifier = null;
    protected ?FHIRString $name = null;
    protected ?FHIRUri $jurisdiction = null;
    protected ?FHIRString $carrierHRF = null;
    protected ?FHIRBase64Binary $carrierAIDC = null;
    protected ?FHIRUri $issuer = null;
    protected ?FHIRCode $entryType = null;

    public function getDeviceIdentifier(): ?FHIRString
    {
        return $this->deviceIdentifier;
    }

    public function setDeviceIdentifier(string|FHIRString|null $value): self
    {
        $this->deviceIdentifier = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getJurisdiction(): ?FHIRUri
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(string|FHIRUri|null $value): self
    {
        $this->jurisdiction = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getCarrierHRF(): ?FHIRString
    {
        return $this->carrierHRF;
    }

    public function setCarrierHRF(string|FHIRString|null $value): self
    {
        $this->carrierHRF = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCarrierAIDC(): ?FHIRBase64Binary
    {
        return $this->carrierAIDC;
    }

    public function setCarrierAIDC(string|FHIRBase64Binary|null $value): self
    {
        $this->carrierAIDC = is_string($value) ? (new FHIRBase64Binary())->setValue($value) : $value;

        return $this;
    }

    public function getIssuer(): ?FHIRUri
    {
        return $this->issuer;
    }

    public function setIssuer(string|FHIRUri|null $value): self
    {
        $this->issuer = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getEntryType(): ?FHIRCode
    {
        return $this->entryType;
    }

    public function setEntryType(string|FHIRCode|null $value): self
    {
        $this->entryType = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }
}
