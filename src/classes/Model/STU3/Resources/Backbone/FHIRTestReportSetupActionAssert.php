<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestReportSetupActionAssert Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestReportSetupActionAssertInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRTestReportSetupActionAssert extends FHIRBackboneElement implements FHIRTestReportSetupActionAssertInterface
{
    public const RESOURCE_NAME = 'TestReport.setup.action.assert';

    protected ?FHIRCode $result = null;
    protected ?FHIRMarkdown $message = null;
    protected ?FHIRString $detail = null;

    public function getResult(): ?FHIRCode
    {
        return $this->result;
    }

    public function setResult(string|FHIRCode|null $value): self
    {
        $this->result = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getMessage(): ?FHIRMarkdown
    {
        return $this->message;
    }

    public function setMessage(string|FHIRMarkdown|null $value): self
    {
        $this->message = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getDetail(): ?FHIRString
    {
        return $this->detail;
    }

    public function setDetail(string|FHIRString|null $value): self
    {
        $this->detail = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
