<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CapabilityStatementMessaging Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCapabilityStatementMessagingInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUnsignedInt;

class FHIRCapabilityStatementMessaging extends FHIRBackboneElement implements FHIRCapabilityStatementMessagingInterface
{
    public const RESOURCE_NAME = 'CapabilityStatement.messaging';

    /** @var FHIRCapabilityStatementMessagingEndpoint[] */
    protected array $endpoint = [];
    protected ?FHIRUnsignedInt $reliableCache = null;
    protected ?FHIRString $documentation = null;

    /** @var FHIRCapabilityStatementMessagingSupportedMessage[] */
    protected array $supportedMessage = [];

    /** @var FHIRCapabilityStatementMessagingEvent[] */
    protected array $event = [];

    /**
     * @return FHIRCapabilityStatementMessagingEndpoint[]
     */
    public function getEndpoint(): array
    {
        return $this->endpoint;
    }

    public function setEndpoint(?FHIRCapabilityStatementMessagingEndpoint ...$value): self
    {
        $this->endpoint = array_filter($value);

        return $this;
    }

    public function addEndpoint(?FHIRCapabilityStatementMessagingEndpoint ...$value): self
    {
        $this->endpoint = array_filter(array_merge($this->endpoint, $value));

        return $this;
    }

    public function getReliableCache(): ?FHIRUnsignedInt
    {
        return $this->reliableCache;
    }

    public function setReliableCache(int|FHIRUnsignedInt|null $value): self
    {
        $this->reliableCache = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getDocumentation(): ?FHIRString
    {
        return $this->documentation;
    }

    public function setDocumentation(string|FHIRString|null $value): self
    {
        $this->documentation = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCapabilityStatementMessagingSupportedMessage[]
     */
    public function getSupportedMessage(): array
    {
        return $this->supportedMessage;
    }

    public function setSupportedMessage(?FHIRCapabilityStatementMessagingSupportedMessage ...$value): self
    {
        $this->supportedMessage = array_filter($value);

        return $this;
    }

    public function addSupportedMessage(?FHIRCapabilityStatementMessagingSupportedMessage ...$value): self
    {
        $this->supportedMessage = array_filter(array_merge($this->supportedMessage, $value));

        return $this;
    }

    /**
     * @return FHIRCapabilityStatementMessagingEvent[]
     */
    public function getEvent(): array
    {
        return $this->event;
    }

    public function setEvent(?FHIRCapabilityStatementMessagingEvent ...$value): self
    {
        $this->event = array_filter($value);

        return $this;
    }

    public function addEvent(?FHIRCapabilityStatementMessagingEvent ...$value): self
    {
        $this->event = array_filter(array_merge($this->event, $value));

        return $this;
    }
}
