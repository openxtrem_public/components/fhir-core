<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ReferralRequestRequester Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRReferralRequestRequesterInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;

class FHIRReferralRequestRequester extends FHIRBackboneElement implements FHIRReferralRequestRequesterInterface
{
    public const RESOURCE_NAME = 'ReferralRequest.requester';

    protected ?FHIRReference $agent = null;
    protected ?FHIRReference $onBehalfOf = null;

    public function getAgent(): ?FHIRReference
    {
        return $this->agent;
    }

    public function setAgent(?FHIRReference $value): self
    {
        $this->agent = $value;

        return $this;
    }

    public function getOnBehalfOf(): ?FHIRReference
    {
        return $this->onBehalfOf;
    }

    public function setOnBehalfOf(?FHIRReference $value): self
    {
        $this->onBehalfOf = $value;

        return $this;
    }
}
