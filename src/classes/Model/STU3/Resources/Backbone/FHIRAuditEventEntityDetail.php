<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AuditEventEntityDetail Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAuditEventEntityDetailInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBase64Binary;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRAuditEventEntityDetail extends FHIRBackboneElement implements FHIRAuditEventEntityDetailInterface
{
    public const RESOURCE_NAME = 'AuditEvent.entity.detail';

    protected ?FHIRString $type = null;
    protected ?FHIRBase64Binary $value = null;

    public function getType(): ?FHIRString
    {
        return $this->type;
    }

    public function setType(string|FHIRString|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getValue(): ?FHIRBase64Binary
    {
        return $this->value;
    }

    public function setValue(string|FHIRBase64Binary|null $value): self
    {
        $this->value = is_string($value) ? (new FHIRBase64Binary())->setValue($value) : $value;

        return $this;
    }
}
