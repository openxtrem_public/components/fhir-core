<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AdverseEventSuspectEntity Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAdverseEventSuspectEntityInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRAdverseEventSuspectEntity extends FHIRBackboneElement implements FHIRAdverseEventSuspectEntityInterface
{
    public const RESOURCE_NAME = 'AdverseEvent.suspectEntity';

    protected ?FHIRReference $instance = null;
    protected ?FHIRCode $causality = null;
    protected ?FHIRCodeableConcept $causalityAssessment = null;
    protected ?FHIRString $causalityProductRelatedness = null;
    protected ?FHIRCodeableConcept $causalityMethod = null;
    protected ?FHIRReference $causalityAuthor = null;
    protected ?FHIRCodeableConcept $causalityResult = null;

    public function getInstance(): ?FHIRReference
    {
        return $this->instance;
    }

    public function setInstance(?FHIRReference $value): self
    {
        $this->instance = $value;

        return $this;
    }

    public function getCausality(): ?FHIRCode
    {
        return $this->causality;
    }

    public function setCausality(string|FHIRCode|null $value): self
    {
        $this->causality = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCausalityAssessment(): ?FHIRCodeableConcept
    {
        return $this->causalityAssessment;
    }

    public function setCausalityAssessment(?FHIRCodeableConcept $value): self
    {
        $this->causalityAssessment = $value;

        return $this;
    }

    public function getCausalityProductRelatedness(): ?FHIRString
    {
        return $this->causalityProductRelatedness;
    }

    public function setCausalityProductRelatedness(string|FHIRString|null $value): self
    {
        $this->causalityProductRelatedness = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getCausalityMethod(): ?FHIRCodeableConcept
    {
        return $this->causalityMethod;
    }

    public function setCausalityMethod(?FHIRCodeableConcept $value): self
    {
        $this->causalityMethod = $value;

        return $this;
    }

    public function getCausalityAuthor(): ?FHIRReference
    {
        return $this->causalityAuthor;
    }

    public function setCausalityAuthor(?FHIRReference $value): self
    {
        $this->causalityAuthor = $value;

        return $this;
    }

    public function getCausalityResult(): ?FHIRCodeableConcept
    {
        return $this->causalityResult;
    }

    public function setCausalityResult(?FHIRCodeableConcept $value): self
    {
        $this->causalityResult = $value;

        return $this;
    }
}
