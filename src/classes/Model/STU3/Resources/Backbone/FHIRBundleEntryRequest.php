<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR BundleEntryRequest Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRBundleEntryRequestInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;

class FHIRBundleEntryRequest extends FHIRBackboneElement implements FHIRBundleEntryRequestInterface
{
    public const RESOURCE_NAME = 'Bundle.entry.request';

    protected ?FHIRCode $method = null;
    protected ?FHIRUri $url = null;
    protected ?FHIRString $ifNoneMatch = null;
    protected ?FHIRInstant $ifModifiedSince = null;
    protected ?FHIRString $ifMatch = null;
    protected ?FHIRString $ifNoneExist = null;

    public function getMethod(): ?FHIRCode
    {
        return $this->method;
    }

    public function setMethod(string|FHIRCode|null $value): self
    {
        $this->method = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getIfNoneMatch(): ?FHIRString
    {
        return $this->ifNoneMatch;
    }

    public function setIfNoneMatch(string|FHIRString|null $value): self
    {
        $this->ifNoneMatch = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getIfModifiedSince(): ?FHIRInstant
    {
        return $this->ifModifiedSince;
    }

    public function setIfModifiedSince(string|FHIRInstant|null $value): self
    {
        $this->ifModifiedSince = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    public function getIfMatch(): ?FHIRString
    {
        return $this->ifMatch;
    }

    public function setIfMatch(string|FHIRString|null $value): self
    {
        $this->ifMatch = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getIfNoneExist(): ?FHIRString
    {
        return $this->ifNoneExist;
    }

    public function setIfNoneExist(string|FHIRString|null $value): self
    {
        $this->ifNoneExist = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
