<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MeasureSupplementalData Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMeasureSupplementalDataInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRMeasureSupplementalData extends FHIRBackboneElement implements FHIRMeasureSupplementalDataInterface
{
    public const RESOURCE_NAME = 'Measure.supplementalData';

    protected ?FHIRIdentifier $identifier = null;

    /** @var FHIRCodeableConcept[] */
    protected array $usage = [];
    protected ?FHIRString $criteria = null;
    protected ?FHIRString $path = null;

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getUsage(): array
    {
        return $this->usage;
    }

    public function setUsage(?FHIRCodeableConcept ...$value): self
    {
        $this->usage = array_filter($value);

        return $this;
    }

    public function addUsage(?FHIRCodeableConcept ...$value): self
    {
        $this->usage = array_filter(array_merge($this->usage, $value));

        return $this;
    }

    public function getCriteria(): ?FHIRString
    {
        return $this->criteria;
    }

    public function setCriteria(string|FHIRString|null $value): self
    {
        $this->criteria = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPath(): ?FHIRString
    {
        return $this->path;
    }

    public function setPath(string|FHIRString|null $value): self
    {
        $this->path = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
