<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ListEntry Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRListEntryInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;

class FHIRListEntry extends FHIRBackboneElement implements FHIRListEntryInterface
{
    public const RESOURCE_NAME = 'List.entry';

    protected ?FHIRCodeableConcept $flag = null;
    protected ?FHIRBoolean $deleted = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRReference $item = null;

    public function getFlag(): ?FHIRCodeableConcept
    {
        return $this->flag;
    }

    public function setFlag(?FHIRCodeableConcept $value): self
    {
        $this->flag = $value;

        return $this;
    }

    public function getDeleted(): ?FHIRBoolean
    {
        return $this->deleted;
    }

    public function setDeleted(bool|FHIRBoolean|null $value): self
    {
        $this->deleted = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getItem(): ?FHIRReference
    {
        return $this->item;
    }

    public function setItem(?FHIRReference $value): self
    {
        $this->item = $value;

        return $this;
    }
}
