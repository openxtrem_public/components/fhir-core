<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CoverageGrouping Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCoverageGroupingInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRCoverageGrouping extends FHIRBackboneElement implements FHIRCoverageGroupingInterface
{
    public const RESOURCE_NAME = 'Coverage.grouping';

    protected ?FHIRString $group = null;
    protected ?FHIRString $groupDisplay = null;
    protected ?FHIRString $subGroup = null;
    protected ?FHIRString $subGroupDisplay = null;
    protected ?FHIRString $plan = null;
    protected ?FHIRString $planDisplay = null;
    protected ?FHIRString $subPlan = null;
    protected ?FHIRString $subPlanDisplay = null;
    protected ?FHIRString $class = null;
    protected ?FHIRString $classDisplay = null;
    protected ?FHIRString $subClass = null;
    protected ?FHIRString $subClassDisplay = null;

    public function getGroup(): ?FHIRString
    {
        return $this->group;
    }

    public function setGroup(string|FHIRString|null $value): self
    {
        $this->group = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getGroupDisplay(): ?FHIRString
    {
        return $this->groupDisplay;
    }

    public function setGroupDisplay(string|FHIRString|null $value): self
    {
        $this->groupDisplay = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSubGroup(): ?FHIRString
    {
        return $this->subGroup;
    }

    public function setSubGroup(string|FHIRString|null $value): self
    {
        $this->subGroup = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSubGroupDisplay(): ?FHIRString
    {
        return $this->subGroupDisplay;
    }

    public function setSubGroupDisplay(string|FHIRString|null $value): self
    {
        $this->subGroupDisplay = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPlan(): ?FHIRString
    {
        return $this->plan;
    }

    public function setPlan(string|FHIRString|null $value): self
    {
        $this->plan = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPlanDisplay(): ?FHIRString
    {
        return $this->planDisplay;
    }

    public function setPlanDisplay(string|FHIRString|null $value): self
    {
        $this->planDisplay = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSubPlan(): ?FHIRString
    {
        return $this->subPlan;
    }

    public function setSubPlan(string|FHIRString|null $value): self
    {
        $this->subPlan = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSubPlanDisplay(): ?FHIRString
    {
        return $this->subPlanDisplay;
    }

    public function setSubPlanDisplay(string|FHIRString|null $value): self
    {
        $this->subPlanDisplay = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getClass(): ?FHIRString
    {
        return $this->class;
    }

    public function setClass(string|FHIRString|null $value): self
    {
        $this->class = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getClassDisplay(): ?FHIRString
    {
        return $this->classDisplay;
    }

    public function setClassDisplay(string|FHIRString|null $value): self
    {
        $this->classDisplay = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSubClass(): ?FHIRString
    {
        return $this->subClass;
    }

    public function setSubClass(string|FHIRString|null $value): self
    {
        $this->subClass = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSubClassDisplay(): ?FHIRString
    {
        return $this->subClassDisplay;
    }

    public function setSubClassDisplay(string|FHIRString|null $value): self
    {
        $this->subClassDisplay = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
