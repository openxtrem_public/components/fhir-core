<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR OperationOutcomeIssue Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIROperationOutcomeIssueInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIROperationOutcomeIssue extends FHIRBackboneElement implements FHIROperationOutcomeIssueInterface
{
    public const RESOURCE_NAME = 'OperationOutcome.issue';

    protected ?FHIRCode $severity = null;
    protected ?FHIRCode $code = null;
    protected ?FHIRCodeableConcept $details = null;
    protected ?FHIRString $diagnostics = null;

    /** @var FHIRString[] */
    protected array $location = [];

    /** @var FHIRString[] */
    protected array $expression = [];

    public function getSeverity(): ?FHIRCode
    {
        return $this->severity;
    }

    public function setSeverity(string|FHIRCode|null $value): self
    {
        $this->severity = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDetails(): ?FHIRCodeableConcept
    {
        return $this->details;
    }

    public function setDetails(?FHIRCodeableConcept $value): self
    {
        $this->details = $value;

        return $this;
    }

    public function getDiagnostics(): ?FHIRString
    {
        return $this->diagnostics;
    }

    public function setDiagnostics(string|FHIRString|null $value): self
    {
        $this->diagnostics = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getLocation(): array
    {
        return $this->location;
    }

    public function setLocation(string|FHIRString|null ...$value): self
    {
        $this->location = [];
        $this->addLocation(...$value);

        return $this;
    }

    public function addLocation(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->location = array_filter(array_merge($this->location, $values));

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getExpression(): array
    {
        return $this->expression;
    }

    public function setExpression(string|FHIRString|null ...$value): self
    {
        $this->expression = [];
        $this->addExpression(...$value);

        return $this;
    }

    public function addExpression(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->expression = array_filter(array_merge($this->expression, $values));

        return $this;
    }
}
