<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExpansionProfileDesignation Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExpansionProfileDesignationInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;

class FHIRExpansionProfileDesignation extends FHIRBackboneElement implements FHIRExpansionProfileDesignationInterface
{
    public const RESOURCE_NAME = 'ExpansionProfile.designation';

    protected ?FHIRExpansionProfileDesignationInclude $include = null;
    protected ?FHIRExpansionProfileDesignationExclude $exclude = null;

    public function getInclude(): ?FHIRExpansionProfileDesignationInclude
    {
        return $this->include;
    }

    public function setInclude(?FHIRExpansionProfileDesignationInclude $value): self
    {
        $this->include = $value;

        return $this;
    }

    public function getExclude(): ?FHIRExpansionProfileDesignationExclude
    {
        return $this->exclude;
    }

    public function setExclude(?FHIRExpansionProfileDesignationExclude $value): self
    {
        $this->exclude = $value;

        return $this;
    }
}
