<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestScriptDestination Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestScriptDestinationInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRInteger;

class FHIRTestScriptDestination extends FHIRBackboneElement implements FHIRTestScriptDestinationInterface
{
    public const RESOURCE_NAME = 'TestScript.destination';

    protected ?FHIRInteger $index = null;
    protected ?FHIRCoding $profile = null;

    public function getIndex(): ?FHIRInteger
    {
        return $this->index;
    }

    public function setIndex(int|FHIRInteger|null $value): self
    {
        $this->index = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getProfile(): ?FHIRCoding
    {
        return $this->profile;
    }

    public function setProfile(?FHIRCoding $value): self
    {
        $this->profile = $value;

        return $this;
    }
}
