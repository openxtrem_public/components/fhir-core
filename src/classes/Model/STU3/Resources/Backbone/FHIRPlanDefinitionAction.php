<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PlanDefinitionAction Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRPlanDefinitionActionInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRDataRequirement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRRelatedArtifact;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRTriggerDefinition;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRPlanDefinitionAction extends FHIRBackboneElement implements FHIRPlanDefinitionActionInterface
{
    public const RESOURCE_NAME = 'PlanDefinition.action';

    protected ?FHIRString $label = null;
    protected ?FHIRString $title = null;
    protected ?FHIRString $description = null;
    protected ?FHIRString $textEquivalent = null;

    /** @var FHIRCodeableConcept[] */
    protected array $code = [];

    /** @var FHIRCodeableConcept[] */
    protected array $reason = [];

    /** @var FHIRRelatedArtifact[] */
    protected array $documentation = [];

    /** @var FHIRId[] */
    protected array $goalId = [];

    /** @var FHIRTriggerDefinition[] */
    protected array $triggerDefinition = [];

    /** @var FHIRPlanDefinitionActionCondition[] */
    protected array $condition = [];

    /** @var FHIRDataRequirement[] */
    protected array $input = [];

    /** @var FHIRDataRequirement[] */
    protected array $output = [];

    /** @var FHIRPlanDefinitionActionRelatedAction[] */
    protected array $relatedAction = [];
    protected FHIRDateTime|FHIRPeriod|FHIRDuration|FHIRRange|FHIRTiming|null $timing = null;

    /** @var FHIRPlanDefinitionActionParticipant[] */
    protected array $participant = [];
    protected ?FHIRCoding $type = null;
    protected ?FHIRCode $groupingBehavior = null;
    protected ?FHIRCode $selectionBehavior = null;
    protected ?FHIRCode $requiredBehavior = null;
    protected ?FHIRCode $precheckBehavior = null;
    protected ?FHIRCode $cardinalityBehavior = null;
    protected ?FHIRReference $definition = null;
    protected ?FHIRReference $transform = null;

    /** @var FHIRPlanDefinitionActionDynamicValue[] */
    protected array $dynamicValue = [];

    /** @var FHIRPlanDefinitionAction[] */
    protected array $action = [];

    public function getLabel(): ?FHIRString
    {
        return $this->label;
    }

    public function setLabel(string|FHIRString|null $value): self
    {
        $this->label = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTextEquivalent(): ?FHIRString
    {
        return $this->textEquivalent;
    }

    public function setTextEquivalent(string|FHIRString|null $value): self
    {
        $this->textEquivalent = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCode(): array
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept ...$value): self
    {
        $this->code = array_filter($value);

        return $this;
    }

    public function addCode(?FHIRCodeableConcept ...$value): self
    {
        $this->code = array_filter(array_merge($this->code, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReason(): array
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableConcept ...$value): self
    {
        $this->reason = array_filter($value);

        return $this;
    }

    public function addReason(?FHIRCodeableConcept ...$value): self
    {
        $this->reason = array_filter(array_merge($this->reason, $value));

        return $this;
    }

    /**
     * @return FHIRRelatedArtifact[]
     */
    public function getDocumentation(): array
    {
        return $this->documentation;
    }

    public function setDocumentation(?FHIRRelatedArtifact ...$value): self
    {
        $this->documentation = array_filter($value);

        return $this;
    }

    public function addDocumentation(?FHIRRelatedArtifact ...$value): self
    {
        $this->documentation = array_filter(array_merge($this->documentation, $value));

        return $this;
    }

    /**
     * @return FHIRId[]
     */
    public function getGoalId(): array
    {
        return $this->goalId;
    }

    public function setGoalId(string|FHIRId|null ...$value): self
    {
        $this->goalId = [];
        $this->addGoalId(...$value);

        return $this;
    }

    public function addGoalId(string|FHIRId|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRId())->setValue($v) : $v, $value);

        $this->goalId = array_filter(array_merge($this->goalId, $values));

        return $this;
    }

    /**
     * @return FHIRTriggerDefinition[]
     */
    public function getTriggerDefinition(): array
    {
        return $this->triggerDefinition;
    }

    public function setTriggerDefinition(?FHIRTriggerDefinition ...$value): self
    {
        $this->triggerDefinition = array_filter($value);

        return $this;
    }

    public function addTriggerDefinition(?FHIRTriggerDefinition ...$value): self
    {
        $this->triggerDefinition = array_filter(array_merge($this->triggerDefinition, $value));

        return $this;
    }

    /**
     * @return FHIRPlanDefinitionActionCondition[]
     */
    public function getCondition(): array
    {
        return $this->condition;
    }

    public function setCondition(?FHIRPlanDefinitionActionCondition ...$value): self
    {
        $this->condition = array_filter($value);

        return $this;
    }

    public function addCondition(?FHIRPlanDefinitionActionCondition ...$value): self
    {
        $this->condition = array_filter(array_merge($this->condition, $value));

        return $this;
    }

    /**
     * @return FHIRDataRequirement[]
     */
    public function getInput(): array
    {
        return $this->input;
    }

    public function setInput(?FHIRDataRequirement ...$value): self
    {
        $this->input = array_filter($value);

        return $this;
    }

    public function addInput(?FHIRDataRequirement ...$value): self
    {
        $this->input = array_filter(array_merge($this->input, $value));

        return $this;
    }

    /**
     * @return FHIRDataRequirement[]
     */
    public function getOutput(): array
    {
        return $this->output;
    }

    public function setOutput(?FHIRDataRequirement ...$value): self
    {
        $this->output = array_filter($value);

        return $this;
    }

    public function addOutput(?FHIRDataRequirement ...$value): self
    {
        $this->output = array_filter(array_merge($this->output, $value));

        return $this;
    }

    /**
     * @return FHIRPlanDefinitionActionRelatedAction[]
     */
    public function getRelatedAction(): array
    {
        return $this->relatedAction;
    }

    public function setRelatedAction(?FHIRPlanDefinitionActionRelatedAction ...$value): self
    {
        $this->relatedAction = array_filter($value);

        return $this;
    }

    public function addRelatedAction(?FHIRPlanDefinitionActionRelatedAction ...$value): self
    {
        $this->relatedAction = array_filter(array_merge($this->relatedAction, $value));

        return $this;
    }

    public function getTiming(): FHIRDateTime|FHIRPeriod|FHIRDuration|FHIRRange|FHIRTiming|null
    {
        return $this->timing;
    }

    public function setTiming(FHIRDateTime|FHIRPeriod|FHIRDuration|FHIRRange|FHIRTiming|null $value): self
    {
        $this->timing = $value;

        return $this;
    }

    /**
     * @return FHIRPlanDefinitionActionParticipant[]
     */
    public function getParticipant(): array
    {
        return $this->participant;
    }

    public function setParticipant(?FHIRPlanDefinitionActionParticipant ...$value): self
    {
        $this->participant = array_filter($value);

        return $this;
    }

    public function addParticipant(?FHIRPlanDefinitionActionParticipant ...$value): self
    {
        $this->participant = array_filter(array_merge($this->participant, $value));

        return $this;
    }

    public function getType(): ?FHIRCoding
    {
        return $this->type;
    }

    public function setType(?FHIRCoding $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getGroupingBehavior(): ?FHIRCode
    {
        return $this->groupingBehavior;
    }

    public function setGroupingBehavior(string|FHIRCode|null $value): self
    {
        $this->groupingBehavior = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getSelectionBehavior(): ?FHIRCode
    {
        return $this->selectionBehavior;
    }

    public function setSelectionBehavior(string|FHIRCode|null $value): self
    {
        $this->selectionBehavior = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getRequiredBehavior(): ?FHIRCode
    {
        return $this->requiredBehavior;
    }

    public function setRequiredBehavior(string|FHIRCode|null $value): self
    {
        $this->requiredBehavior = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getPrecheckBehavior(): ?FHIRCode
    {
        return $this->precheckBehavior;
    }

    public function setPrecheckBehavior(string|FHIRCode|null $value): self
    {
        $this->precheckBehavior = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCardinalityBehavior(): ?FHIRCode
    {
        return $this->cardinalityBehavior;
    }

    public function setCardinalityBehavior(string|FHIRCode|null $value): self
    {
        $this->cardinalityBehavior = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDefinition(): ?FHIRReference
    {
        return $this->definition;
    }

    public function setDefinition(?FHIRReference $value): self
    {
        $this->definition = $value;

        return $this;
    }

    public function getTransform(): ?FHIRReference
    {
        return $this->transform;
    }

    public function setTransform(?FHIRReference $value): self
    {
        $this->transform = $value;

        return $this;
    }

    /**
     * @return FHIRPlanDefinitionActionDynamicValue[]
     */
    public function getDynamicValue(): array
    {
        return $this->dynamicValue;
    }

    public function setDynamicValue(?FHIRPlanDefinitionActionDynamicValue ...$value): self
    {
        $this->dynamicValue = array_filter($value);

        return $this;
    }

    public function addDynamicValue(?FHIRPlanDefinitionActionDynamicValue ...$value): self
    {
        $this->dynamicValue = array_filter(array_merge($this->dynamicValue, $value));

        return $this;
    }

    /**
     * @return FHIRPlanDefinitionAction[]
     */
    public function getAction(): array
    {
        return $this->action;
    }

    public function setAction(?FHIRPlanDefinitionAction ...$value): self
    {
        $this->action = array_filter($value);

        return $this;
    }

    public function addAction(?FHIRPlanDefinitionAction ...$value): self
    {
        $this->action = array_filter(array_merge($this->action, $value));

        return $this;
    }
}
