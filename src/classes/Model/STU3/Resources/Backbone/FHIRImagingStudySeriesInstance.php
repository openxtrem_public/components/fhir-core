<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImagingStudySeriesInstance Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImagingStudySeriesInstanceInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIROid;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUnsignedInt;

class FHIRImagingStudySeriesInstance extends FHIRBackboneElement implements FHIRImagingStudySeriesInstanceInterface
{
    public const RESOURCE_NAME = 'ImagingStudy.series.instance';

    protected ?FHIROid $uid = null;
    protected ?FHIRUnsignedInt $number = null;
    protected ?FHIROid $sopClass = null;
    protected ?FHIRString $title = null;

    public function getUid(): ?FHIROid
    {
        return $this->uid;
    }

    public function setUid(string|FHIROid|null $value): self
    {
        $this->uid = is_string($value) ? (new FHIROid())->setValue($value) : $value;

        return $this;
    }

    public function getNumber(): ?FHIRUnsignedInt
    {
        return $this->number;
    }

    public function setNumber(int|FHIRUnsignedInt|null $value): self
    {
        $this->number = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getSopClass(): ?FHIROid
    {
        return $this->sopClass;
    }

    public function setSopClass(string|FHIROid|null $value): self
    {
        $this->sopClass = is_string($value) ? (new FHIROid())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
