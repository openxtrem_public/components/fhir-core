<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImplementationGuideDependency Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImplementationGuideDependencyInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;

class FHIRImplementationGuideDependency extends FHIRBackboneElement implements FHIRImplementationGuideDependencyInterface
{
    public const RESOURCE_NAME = 'ImplementationGuide.dependency';

    protected ?FHIRCode $type = null;
    protected ?FHIRUri $uri = null;

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getUri(): ?FHIRUri
    {
        return $this->uri;
    }

    public function setUri(string|FHIRUri|null $value): self
    {
        $this->uri = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }
}
