<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ConceptMapGroupElementTargetDependsOn Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRConceptMapGroupElementTargetDependsOnInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;

class FHIRConceptMapGroupElementTargetDependsOn extends FHIRBackboneElement implements FHIRConceptMapGroupElementTargetDependsOnInterface
{
    public const RESOURCE_NAME = 'ConceptMap.group.element.target.dependsOn';

    protected ?FHIRUri $property = null;
    protected ?FHIRUri $system = null;
    protected ?FHIRString $code = null;
    protected ?FHIRString $display = null;

    public function getProperty(): ?FHIRUri
    {
        return $this->property;
    }

    public function setProperty(string|FHIRUri|null $value): self
    {
        $this->property = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getSystem(): ?FHIRUri
    {
        return $this->system;
    }

    public function setSystem(string|FHIRUri|null $value): self
    {
        $this->system = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRString
    {
        return $this->code;
    }

    public function setCode(string|FHIRString|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDisplay(): ?FHIRString
    {
        return $this->display;
    }

    public function setDisplay(string|FHIRString|null $value): self
    {
        $this->display = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
