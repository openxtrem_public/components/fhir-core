<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExpansionProfileExcludedSystem Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExpansionProfileExcludedSystemInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;

class FHIRExpansionProfileExcludedSystem extends FHIRBackboneElement implements FHIRExpansionProfileExcludedSystemInterface
{
    public const RESOURCE_NAME = 'ExpansionProfile.excludedSystem';

    protected ?FHIRUri $system = null;
    protected ?FHIRString $version = null;

    public function getSystem(): ?FHIRUri
    {
        return $this->system;
    }

    public function setSystem(string|FHIRUri|null $value): self
    {
        $this->system = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
