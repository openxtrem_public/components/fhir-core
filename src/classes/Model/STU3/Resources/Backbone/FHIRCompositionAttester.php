<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CompositionAttester Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCompositionAttesterInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;

class FHIRCompositionAttester extends FHIRBackboneElement implements FHIRCompositionAttesterInterface
{
    public const RESOURCE_NAME = 'Composition.attester';

    /** @var FHIRCode[] */
    protected array $mode = [];
    protected ?FHIRDateTime $time = null;
    protected ?FHIRReference $party = null;

    /**
     * @return FHIRCode[]
     */
    public function getMode(): array
    {
        return $this->mode;
    }

    public function setMode(string|FHIRCode|null ...$value): self
    {
        $this->mode = [];
        $this->addMode(...$value);

        return $this;
    }

    public function addMode(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->mode = array_filter(array_merge($this->mode, $values));

        return $this;
    }

    public function getTime(): ?FHIRDateTime
    {
        return $this->time;
    }

    public function setTime(string|FHIRDateTime|null $value): self
    {
        $this->time = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getParty(): ?FHIRReference
    {
        return $this->party;
    }

    public function setParty(?FHIRReference $value): self
    {
        $this->party = $value;

        return $this;
    }
}
