<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExpansionProfileDesignationInclude Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRExpansionProfileDesignationIncludeInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;

class FHIRExpansionProfileDesignationInclude extends FHIRBackboneElement implements FHIRExpansionProfileDesignationIncludeInterface
{
    public const RESOURCE_NAME = 'ExpansionProfile.designation.include';

    /** @var FHIRExpansionProfileDesignationIncludeDesignation[] */
    protected array $designation = [];

    /**
     * @return FHIRExpansionProfileDesignationIncludeDesignation[]
     */
    public function getDesignation(): array
    {
        return $this->designation;
    }

    public function setDesignation(?FHIRExpansionProfileDesignationIncludeDesignation ...$value): self
    {
        $this->designation = array_filter($value);

        return $this;
    }

    public function addDesignation(?FHIRExpansionProfileDesignationIncludeDesignation ...$value): self
    {
        $this->designation = array_filter(array_merge($this->designation, $value));

        return $this;
    }
}
