<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PatientContact Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRPatientContactInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAddress;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRHumanName;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;

class FHIRPatientContact extends FHIRBackboneElement implements FHIRPatientContactInterface
{
    public const RESOURCE_NAME = 'Patient.contact';

    /** @var FHIRCodeableConcept[] */
    protected array $relationship = [];
    protected ?FHIRHumanName $name = null;

    /** @var FHIRContactPoint[] */
    protected array $telecom = [];
    protected ?FHIRAddress $address = null;
    protected ?FHIRCode $gender = null;
    protected ?FHIRReference $organization = null;
    protected ?FHIRPeriod $period = null;

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getRelationship(): array
    {
        return $this->relationship;
    }

    public function setRelationship(?FHIRCodeableConcept ...$value): self
    {
        $this->relationship = array_filter($value);

        return $this;
    }

    public function addRelationship(?FHIRCodeableConcept ...$value): self
    {
        $this->relationship = array_filter(array_merge($this->relationship, $value));

        return $this;
    }

    public function getName(): ?FHIRHumanName
    {
        return $this->name;
    }

    public function setName(?FHIRHumanName $value): self
    {
        $this->name = $value;

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getTelecom(): array
    {
        return $this->telecom;
    }

    public function setTelecom(?FHIRContactPoint ...$value): self
    {
        $this->telecom = array_filter($value);

        return $this;
    }

    public function addTelecom(?FHIRContactPoint ...$value): self
    {
        $this->telecom = array_filter(array_merge($this->telecom, $value));

        return $this;
    }

    public function getAddress(): ?FHIRAddress
    {
        return $this->address;
    }

    public function setAddress(?FHIRAddress $value): self
    {
        $this->address = $value;

        return $this;
    }

    public function getGender(): ?FHIRCode
    {
        return $this->gender;
    }

    public function setGender(string|FHIRCode|null $value): self
    {
        $this->gender = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getOrganization(): ?FHIRReference
    {
        return $this->organization;
    }

    public function setOrganization(?FHIRReference $value): self
    {
        $this->organization = $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }
}
