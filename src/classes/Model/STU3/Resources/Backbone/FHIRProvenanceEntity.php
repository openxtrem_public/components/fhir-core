<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ProvenanceEntity Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRProvenanceEntityInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;

class FHIRProvenanceEntity extends FHIRBackboneElement implements FHIRProvenanceEntityInterface
{
    public const RESOURCE_NAME = 'Provenance.entity';

    protected ?FHIRCode $role = null;
    protected FHIRUri|FHIRReference|FHIRIdentifier|null $what = null;

    /** @var FHIRProvenanceAgent[] */
    protected array $agent = [];

    public function getRole(): ?FHIRCode
    {
        return $this->role;
    }

    public function setRole(string|FHIRCode|null $value): self
    {
        $this->role = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getWhat(): FHIRUri|FHIRReference|FHIRIdentifier|null
    {
        return $this->what;
    }

    public function setWhat(FHIRUri|FHIRReference|FHIRIdentifier|null $value): self
    {
        $this->what = $value;

        return $this;
    }

    /**
     * @return FHIRProvenanceAgent[]
     */
    public function getAgent(): array
    {
        return $this->agent;
    }

    public function setAgent(?FHIRProvenanceAgent ...$value): self
    {
        $this->agent = array_filter($value);

        return $this;
    }

    public function addAgent(?FHIRProvenanceAgent ...$value): self
    {
        $this->agent = array_filter(array_merge($this->agent, $value));

        return $this;
    }
}
