<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImagingManifestStudySeries Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImagingManifestStudySeriesInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIROid;

class FHIRImagingManifestStudySeries extends FHIRBackboneElement implements FHIRImagingManifestStudySeriesInterface
{
    public const RESOURCE_NAME = 'ImagingManifest.study.series';

    protected ?FHIROid $uid = null;

    /** @var FHIRReference[] */
    protected array $endpoint = [];

    /** @var FHIRImagingManifestStudySeriesInstance[] */
    protected array $instance = [];

    public function getUid(): ?FHIROid
    {
        return $this->uid;
    }

    public function setUid(string|FHIROid|null $value): self
    {
        $this->uid = is_string($value) ? (new FHIROid())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEndpoint(): array
    {
        return $this->endpoint;
    }

    public function setEndpoint(?FHIRReference ...$value): self
    {
        $this->endpoint = array_filter($value);

        return $this;
    }

    public function addEndpoint(?FHIRReference ...$value): self
    {
        $this->endpoint = array_filter(array_merge($this->endpoint, $value));

        return $this;
    }

    /**
     * @return FHIRImagingManifestStudySeriesInstance[]
     */
    public function getInstance(): array
    {
        return $this->instance;
    }

    public function setInstance(?FHIRImagingManifestStudySeriesInstance ...$value): self
    {
        $this->instance = array_filter($value);

        return $this;
    }

    public function addInstance(?FHIRImagingManifestStudySeriesInstance ...$value): self
    {
        $this->instance = array_filter(array_merge($this->instance, $value));

        return $this;
    }
}
