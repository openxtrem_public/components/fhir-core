<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ParametersParameter Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRParametersParameterInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Resources\FHIRResource;

class FHIRParametersParameter extends FHIRBackboneElement implements FHIRParametersParameterInterface
{
    public const RESOURCE_NAME = 'Parameters.parameter';

    protected ?FHIRString $name = null;
    protected ?FHIRElement $value = null;
    protected ?FHIRResource $resource = null;

    /** @var FHIRParametersParameter[] */
    protected array $part = [];

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getValue(): ?FHIRElement
    {
        return $this->value;
    }

    public function setValue(?FHIRElement $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getResource(): ?FHIRResource
    {
        return $this->resource;
    }

    public function setResource(?FHIRResource $value): self
    {
        $this->resource = $value;

        return $this;
    }

    /**
     * @return FHIRParametersParameter[]
     */
    public function getPart(): array
    {
        return $this->part;
    }

    public function setPart(?FHIRParametersParameter ...$value): self
    {
        $this->part = array_filter($value);

        return $this;
    }

    public function addPart(?FHIRParametersParameter ...$value): self
    {
        $this->part = array_filter(array_merge($this->part, $value));

        return $this;
    }
}
