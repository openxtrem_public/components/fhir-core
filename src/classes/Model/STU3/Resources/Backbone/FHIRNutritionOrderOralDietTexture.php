<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR NutritionOrderOralDietTexture Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRNutritionOrderOralDietTextureInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;

class FHIRNutritionOrderOralDietTexture extends FHIRBackboneElement implements FHIRNutritionOrderOralDietTextureInterface
{
    public const RESOURCE_NAME = 'NutritionOrder.oralDiet.texture';

    protected ?FHIRCodeableConcept $modifier = null;
    protected ?FHIRCodeableConcept $foodType = null;

    public function getModifier(): ?FHIRCodeableConcept
    {
        return $this->modifier;
    }

    public function setModifier(?FHIRCodeableConcept $value): self
    {
        $this->modifier = $value;

        return $this;
    }

    public function getFoodType(): ?FHIRCodeableConcept
    {
        return $this->foodType;
    }

    public function setFoodType(?FHIRCodeableConcept $value): self
    {
        $this->foodType = $value;

        return $this;
    }
}
