<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PlanDefinitionGoal Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRPlanDefinitionGoalInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRRelatedArtifact;

class FHIRPlanDefinitionGoal extends FHIRBackboneElement implements FHIRPlanDefinitionGoalInterface
{
    public const RESOURCE_NAME = 'PlanDefinition.goal';

    protected ?FHIRCodeableConcept $category = null;
    protected ?FHIRCodeableConcept $description = null;
    protected ?FHIRCodeableConcept $priority = null;
    protected ?FHIRCodeableConcept $start = null;

    /** @var FHIRCodeableConcept[] */
    protected array $addresses = [];

    /** @var FHIRRelatedArtifact[] */
    protected array $documentation = [];

    /** @var FHIRPlanDefinitionGoalTarget[] */
    protected array $target = [];

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getDescription(): ?FHIRCodeableConcept
    {
        return $this->description;
    }

    public function setDescription(?FHIRCodeableConcept $value): self
    {
        $this->description = $value;

        return $this;
    }

    public function getPriority(): ?FHIRCodeableConcept
    {
        return $this->priority;
    }

    public function setPriority(?FHIRCodeableConcept $value): self
    {
        $this->priority = $value;

        return $this;
    }

    public function getStart(): ?FHIRCodeableConcept
    {
        return $this->start;
    }

    public function setStart(?FHIRCodeableConcept $value): self
    {
        $this->start = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getAddresses(): array
    {
        return $this->addresses;
    }

    public function setAddresses(?FHIRCodeableConcept ...$value): self
    {
        $this->addresses = array_filter($value);

        return $this;
    }

    public function addAddresses(?FHIRCodeableConcept ...$value): self
    {
        $this->addresses = array_filter(array_merge($this->addresses, $value));

        return $this;
    }

    /**
     * @return FHIRRelatedArtifact[]
     */
    public function getDocumentation(): array
    {
        return $this->documentation;
    }

    public function setDocumentation(?FHIRRelatedArtifact ...$value): self
    {
        $this->documentation = array_filter($value);

        return $this;
    }

    public function addDocumentation(?FHIRRelatedArtifact ...$value): self
    {
        $this->documentation = array_filter(array_merge($this->documentation, $value));

        return $this;
    }

    /**
     * @return FHIRPlanDefinitionGoalTarget[]
     */
    public function getTarget(): array
    {
        return $this->target;
    }

    public function setTarget(?FHIRPlanDefinitionGoalTarget ...$value): self
    {
        $this->target = array_filter($value);

        return $this;
    }

    public function addTarget(?FHIRPlanDefinitionGoalTarget ...$value): self
    {
        $this->target = array_filter(array_merge($this->target, $value));

        return $this;
    }
}
