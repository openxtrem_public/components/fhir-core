<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestScriptSetupActionAssertRule Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestScriptSetupActionAssertRuleInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRId;

class FHIRTestScriptSetupActionAssertRule extends FHIRBackboneElement implements FHIRTestScriptSetupActionAssertRuleInterface
{
    public const RESOURCE_NAME = 'TestScript.setup.action.assert.rule';

    protected ?FHIRId $ruleId = null;

    /** @var FHIRTestScriptSetupActionAssertRuleParam[] */
    protected array $param = [];

    public function getRuleId(): ?FHIRId
    {
        return $this->ruleId;
    }

    public function setRuleId(string|FHIRId|null $value): self
    {
        $this->ruleId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRTestScriptSetupActionAssertRuleParam[]
     */
    public function getParam(): array
    {
        return $this->param;
    }

    public function setParam(?FHIRTestScriptSetupActionAssertRuleParam ...$value): self
    {
        $this->param = array_filter($value);

        return $this;
    }

    public function addParam(?FHIRTestScriptSetupActionAssertRuleParam ...$value): self
    {
        $this->param = array_filter(array_merge($this->param, $value));

        return $this;
    }
}
