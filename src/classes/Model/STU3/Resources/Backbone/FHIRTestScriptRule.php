<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestScriptRule Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestScriptRuleInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;

class FHIRTestScriptRule extends FHIRBackboneElement implements FHIRTestScriptRuleInterface
{
    public const RESOURCE_NAME = 'TestScript.rule';

    protected ?FHIRReference $resource = null;

    /** @var FHIRTestScriptRuleParam[] */
    protected array $param = [];

    public function getResource(): ?FHIRReference
    {
        return $this->resource;
    }

    public function setResource(?FHIRReference $value): self
    {
        $this->resource = $value;

        return $this;
    }

    /**
     * @return FHIRTestScriptRuleParam[]
     */
    public function getParam(): array
    {
        return $this->param;
    }

    public function setParam(?FHIRTestScriptRuleParam ...$value): self
    {
        $this->param = array_filter($value);

        return $this;
    }

    public function addParam(?FHIRTestScriptRuleParam ...$value): self
    {
        $this->param = array_filter(array_merge($this->param, $value));

        return $this;
    }
}
