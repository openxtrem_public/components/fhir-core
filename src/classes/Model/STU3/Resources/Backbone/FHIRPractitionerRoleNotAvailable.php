<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PractitionerRoleNotAvailable Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRPractitionerRoleNotAvailableInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRPractitionerRoleNotAvailable extends FHIRBackboneElement implements FHIRPractitionerRoleNotAvailableInterface
{
    public const RESOURCE_NAME = 'PractitionerRole.notAvailable';

    protected ?FHIRString $description = null;
    protected ?FHIRPeriod $during = null;

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDuring(): ?FHIRPeriod
    {
        return $this->during;
    }

    public function setDuring(?FHIRPeriod $value): self
    {
        $this->during = $value;

        return $this;
    }
}
