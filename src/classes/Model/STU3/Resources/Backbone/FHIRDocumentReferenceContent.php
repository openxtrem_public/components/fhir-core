<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DocumentReferenceContent Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDocumentReferenceContentInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCoding;

class FHIRDocumentReferenceContent extends FHIRBackboneElement implements FHIRDocumentReferenceContentInterface
{
    public const RESOURCE_NAME = 'DocumentReference.content';

    protected ?FHIRAttachment $attachment = null;
    protected ?FHIRCoding $format = null;

    public function getAttachment(): ?FHIRAttachment
    {
        return $this->attachment;
    }

    public function setAttachment(?FHIRAttachment $value): self
    {
        $this->attachment = $value;

        return $this;
    }

    public function getFormat(): ?FHIRCoding
    {
        return $this->format;
    }

    public function setFormat(?FHIRCoding $value): self
    {
        $this->format = $value;

        return $this;
    }
}
