<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR OperationDefinitionParameterBinding Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIROperationDefinitionParameterBindingInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;

class FHIROperationDefinitionParameterBinding extends FHIRBackboneElement implements FHIROperationDefinitionParameterBindingInterface
{
    public const RESOURCE_NAME = 'OperationDefinition.parameter.binding';

    protected ?FHIRCode $strength = null;
    protected FHIRUri|FHIRReference|null $valueSet = null;

    public function getStrength(): ?FHIRCode
    {
        return $this->strength;
    }

    public function setStrength(string|FHIRCode|null $value): self
    {
        $this->strength = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getValueSet(): FHIRUri|FHIRReference|null
    {
        return $this->valueSet;
    }

    public function setValueSet(FHIRUri|FHIRReference|null $value): self
    {
        $this->valueSet = $value;

        return $this;
    }
}
