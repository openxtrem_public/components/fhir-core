<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ValueSetComposeIncludeConcept Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRValueSetComposeIncludeConceptInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRValueSetComposeIncludeConcept extends FHIRBackboneElement implements FHIRValueSetComposeIncludeConceptInterface
{
    public const RESOURCE_NAME = 'ValueSet.compose.include.concept';

    protected ?FHIRCode $code = null;
    protected ?FHIRString $display = null;

    /** @var FHIRValueSetComposeIncludeConceptDesignation[] */
    protected array $designation = [];

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDisplay(): ?FHIRString
    {
        return $this->display;
    }

    public function setDisplay(string|FHIRString|null $value): self
    {
        $this->display = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRValueSetComposeIncludeConceptDesignation[]
     */
    public function getDesignation(): array
    {
        return $this->designation;
    }

    public function setDesignation(?FHIRValueSetComposeIncludeConceptDesignation ...$value): self
    {
        $this->designation = array_filter($value);

        return $this;
    }

    public function addDesignation(?FHIRValueSetComposeIncludeConceptDesignation ...$value): self
    {
        $this->designation = array_filter(array_merge($this->designation, $value));

        return $this;
    }
}
