<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImplementationGuidePackage Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImplementationGuidePackageInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRImplementationGuidePackage extends FHIRBackboneElement implements FHIRImplementationGuidePackageInterface
{
    public const RESOURCE_NAME = 'ImplementationGuide.package';

    protected ?FHIRString $name = null;
    protected ?FHIRString $description = null;

    /** @var FHIRImplementationGuidePackageResource[] */
    protected array $resource = [];

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRImplementationGuidePackageResource[]
     */
    public function getResource(): array
    {
        return $this->resource;
    }

    public function setResource(?FHIRImplementationGuidePackageResource ...$value): self
    {
        $this->resource = array_filter($value);

        return $this;
    }

    public function addResource(?FHIRImplementationGuidePackageResource ...$value): self
    {
        $this->resource = array_filter(array_merge($this->resource, $value));

        return $this;
    }
}
