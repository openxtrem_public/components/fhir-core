<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImmunizationRecommendationRecommendation Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImmunizationRecommendationRecommendationInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRPositiveInt;

class FHIRImmunizationRecommendationRecommendation extends FHIRBackboneElement implements FHIRImmunizationRecommendationRecommendationInterface
{
    public const RESOURCE_NAME = 'ImmunizationRecommendation.recommendation';

    protected ?FHIRDateTime $date = null;
    protected ?FHIRCodeableConcept $vaccineCode = null;
    protected ?FHIRCodeableConcept $targetDisease = null;
    protected ?FHIRPositiveInt $doseNumber = null;
    protected ?FHIRCodeableConcept $forecastStatus = null;

    /** @var FHIRImmunizationRecommendationRecommendationDateCriterion[] */
    protected array $dateCriterion = [];
    protected ?FHIRImmunizationRecommendationRecommendationProtocol $protocol = null;

    /** @var FHIRReference[] */
    protected array $supportingImmunization = [];

    /** @var FHIRReference[] */
    protected array $supportingPatientInformation = [];

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getVaccineCode(): ?FHIRCodeableConcept
    {
        return $this->vaccineCode;
    }

    public function setVaccineCode(?FHIRCodeableConcept $value): self
    {
        $this->vaccineCode = $value;

        return $this;
    }

    public function getTargetDisease(): ?FHIRCodeableConcept
    {
        return $this->targetDisease;
    }

    public function setTargetDisease(?FHIRCodeableConcept $value): self
    {
        $this->targetDisease = $value;

        return $this;
    }

    public function getDoseNumber(): ?FHIRPositiveInt
    {
        return $this->doseNumber;
    }

    public function setDoseNumber(int|FHIRPositiveInt|null $value): self
    {
        $this->doseNumber = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getForecastStatus(): ?FHIRCodeableConcept
    {
        return $this->forecastStatus;
    }

    public function setForecastStatus(?FHIRCodeableConcept $value): self
    {
        $this->forecastStatus = $value;

        return $this;
    }

    /**
     * @return FHIRImmunizationRecommendationRecommendationDateCriterion[]
     */
    public function getDateCriterion(): array
    {
        return $this->dateCriterion;
    }

    public function setDateCriterion(?FHIRImmunizationRecommendationRecommendationDateCriterion ...$value): self
    {
        $this->dateCriterion = array_filter($value);

        return $this;
    }

    public function addDateCriterion(?FHIRImmunizationRecommendationRecommendationDateCriterion ...$value): self
    {
        $this->dateCriterion = array_filter(array_merge($this->dateCriterion, $value));

        return $this;
    }

    public function getProtocol(): ?FHIRImmunizationRecommendationRecommendationProtocol
    {
        return $this->protocol;
    }

    public function setProtocol(?FHIRImmunizationRecommendationRecommendationProtocol $value): self
    {
        $this->protocol = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupportingImmunization(): array
    {
        return $this->supportingImmunization;
    }

    public function setSupportingImmunization(?FHIRReference ...$value): self
    {
        $this->supportingImmunization = array_filter($value);

        return $this;
    }

    public function addSupportingImmunization(?FHIRReference ...$value): self
    {
        $this->supportingImmunization = array_filter(array_merge($this->supportingImmunization, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupportingPatientInformation(): array
    {
        return $this->supportingPatientInformation;
    }

    public function setSupportingPatientInformation(?FHIRReference ...$value): self
    {
        $this->supportingPatientInformation = array_filter($value);

        return $this;
    }

    public function addSupportingPatientInformation(?FHIRReference ...$value): self
    {
        $this->supportingPatientInformation = array_filter(array_merge($this->supportingPatientInformation, $value));

        return $this;
    }
}
