<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestScriptTeardownAction Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestScriptTeardownActionInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;

class FHIRTestScriptTeardownAction extends FHIRBackboneElement implements FHIRTestScriptTeardownActionInterface
{
    public const RESOURCE_NAME = 'TestScript.teardown.action';

    protected ?FHIRTestScriptSetupActionOperation $operation = null;

    public function getOperation(): ?FHIRTestScriptSetupActionOperation
    {
        return $this->operation;
    }

    public function setOperation(?FHIRTestScriptSetupActionOperation $value): self
    {
        $this->operation = $value;

        return $this;
    }
}
