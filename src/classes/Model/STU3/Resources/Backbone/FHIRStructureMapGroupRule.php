<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR StructureMapGroupRule Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRStructureMapGroupRuleInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRStructureMapGroupRule extends FHIRBackboneElement implements FHIRStructureMapGroupRuleInterface
{
    public const RESOURCE_NAME = 'StructureMap.group.rule';

    protected ?FHIRId $name = null;

    /** @var FHIRStructureMapGroupRuleSource[] */
    protected array $source = [];

    /** @var FHIRStructureMapGroupRuleTarget[] */
    protected array $target = [];

    /** @var FHIRStructureMapGroupRule[] */
    protected array $rule = [];

    /** @var FHIRStructureMapGroupRuleDependent[] */
    protected array $dependent = [];
    protected ?FHIRString $documentation = null;

    public function getName(): ?FHIRId
    {
        return $this->name;
    }

    public function setName(string|FHIRId|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRStructureMapGroupRuleSource[]
     */
    public function getSource(): array
    {
        return $this->source;
    }

    public function setSource(?FHIRStructureMapGroupRuleSource ...$value): self
    {
        $this->source = array_filter($value);

        return $this;
    }

    public function addSource(?FHIRStructureMapGroupRuleSource ...$value): self
    {
        $this->source = array_filter(array_merge($this->source, $value));

        return $this;
    }

    /**
     * @return FHIRStructureMapGroupRuleTarget[]
     */
    public function getTarget(): array
    {
        return $this->target;
    }

    public function setTarget(?FHIRStructureMapGroupRuleTarget ...$value): self
    {
        $this->target = array_filter($value);

        return $this;
    }

    public function addTarget(?FHIRStructureMapGroupRuleTarget ...$value): self
    {
        $this->target = array_filter(array_merge($this->target, $value));

        return $this;
    }

    /**
     * @return FHIRStructureMapGroupRule[]
     */
    public function getRule(): array
    {
        return $this->rule;
    }

    public function setRule(?FHIRStructureMapGroupRule ...$value): self
    {
        $this->rule = array_filter($value);

        return $this;
    }

    public function addRule(?FHIRStructureMapGroupRule ...$value): self
    {
        $this->rule = array_filter(array_merge($this->rule, $value));

        return $this;
    }

    /**
     * @return FHIRStructureMapGroupRuleDependent[]
     */
    public function getDependent(): array
    {
        return $this->dependent;
    }

    public function setDependent(?FHIRStructureMapGroupRuleDependent ...$value): self
    {
        $this->dependent = array_filter($value);

        return $this;
    }

    public function addDependent(?FHIRStructureMapGroupRuleDependent ...$value): self
    {
        $this->dependent = array_filter(array_merge($this->dependent, $value));

        return $this;
    }

    public function getDocumentation(): ?FHIRString
    {
        return $this->documentation;
    }

    public function setDocumentation(string|FHIRString|null $value): self
    {
        $this->documentation = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
