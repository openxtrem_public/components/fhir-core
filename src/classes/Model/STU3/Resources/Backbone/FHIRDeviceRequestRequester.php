<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DeviceRequestRequester Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDeviceRequestRequesterInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;

class FHIRDeviceRequestRequester extends FHIRBackboneElement implements FHIRDeviceRequestRequesterInterface
{
    public const RESOURCE_NAME = 'DeviceRequest.requester';

    protected ?FHIRReference $agent = null;
    protected ?FHIRReference $onBehalfOf = null;

    public function getAgent(): ?FHIRReference
    {
        return $this->agent;
    }

    public function setAgent(?FHIRReference $value): self
    {
        $this->agent = $value;

        return $this;
    }

    public function getOnBehalfOf(): ?FHIRReference
    {
        return $this->onBehalfOf;
    }

    public function setOnBehalfOf(?FHIRReference $value): self
    {
        $this->onBehalfOf = $value;

        return $this;
    }
}
