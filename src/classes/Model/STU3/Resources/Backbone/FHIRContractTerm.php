<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ContractTerm Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRContractTermInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRContractTerm extends FHIRBackboneElement implements FHIRContractTermInterface
{
    public const RESOURCE_NAME = 'Contract.term';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRDateTime $issued = null;
    protected ?FHIRPeriod $applies = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableConcept $subType = null;

    /** @var FHIRReference[] */
    protected array $topic = [];

    /** @var FHIRCodeableConcept[] */
    protected array $action = [];

    /** @var FHIRCodeableConcept[] */
    protected array $actionReason = [];

    /** @var FHIRCoding[] */
    protected array $securityLabel = [];

    /** @var FHIRContractTermAgent[] */
    protected array $agent = [];
    protected ?FHIRString $text = null;

    /** @var FHIRContractTermValuedItem[] */
    protected array $valuedItem = [];

    /** @var FHIRContractTerm[] */
    protected array $group = [];

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getIssued(): ?FHIRDateTime
    {
        return $this->issued;
    }

    public function setIssued(string|FHIRDateTime|null $value): self
    {
        $this->issued = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getApplies(): ?FHIRPeriod
    {
        return $this->applies;
    }

    public function setApplies(?FHIRPeriod $value): self
    {
        $this->applies = $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getSubType(): ?FHIRCodeableConcept
    {
        return $this->subType;
    }

    public function setSubType(?FHIRCodeableConcept $value): self
    {
        $this->subType = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getTopic(): array
    {
        return $this->topic;
    }

    public function setTopic(?FHIRReference ...$value): self
    {
        $this->topic = array_filter($value);

        return $this;
    }

    public function addTopic(?FHIRReference ...$value): self
    {
        $this->topic = array_filter(array_merge($this->topic, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getAction(): array
    {
        return $this->action;
    }

    public function setAction(?FHIRCodeableConcept ...$value): self
    {
        $this->action = array_filter($value);

        return $this;
    }

    public function addAction(?FHIRCodeableConcept ...$value): self
    {
        $this->action = array_filter(array_merge($this->action, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getActionReason(): array
    {
        return $this->actionReason;
    }

    public function setActionReason(?FHIRCodeableConcept ...$value): self
    {
        $this->actionReason = array_filter($value);

        return $this;
    }

    public function addActionReason(?FHIRCodeableConcept ...$value): self
    {
        $this->actionReason = array_filter(array_merge($this->actionReason, $value));

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getSecurityLabel(): array
    {
        return $this->securityLabel;
    }

    public function setSecurityLabel(?FHIRCoding ...$value): self
    {
        $this->securityLabel = array_filter($value);

        return $this;
    }

    public function addSecurityLabel(?FHIRCoding ...$value): self
    {
        $this->securityLabel = array_filter(array_merge($this->securityLabel, $value));

        return $this;
    }

    /**
     * @return FHIRContractTermAgent[]
     */
    public function getAgent(): array
    {
        return $this->agent;
    }

    public function setAgent(?FHIRContractTermAgent ...$value): self
    {
        $this->agent = array_filter($value);

        return $this;
    }

    public function addAgent(?FHIRContractTermAgent ...$value): self
    {
        $this->agent = array_filter(array_merge($this->agent, $value));

        return $this;
    }

    public function getText(): ?FHIRString
    {
        return $this->text;
    }

    public function setText(string|FHIRString|null $value): self
    {
        $this->text = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRContractTermValuedItem[]
     */
    public function getValuedItem(): array
    {
        return $this->valuedItem;
    }

    public function setValuedItem(?FHIRContractTermValuedItem ...$value): self
    {
        $this->valuedItem = array_filter($value);

        return $this;
    }

    public function addValuedItem(?FHIRContractTermValuedItem ...$value): self
    {
        $this->valuedItem = array_filter(array_merge($this->valuedItem, $value));

        return $this;
    }

    /**
     * @return FHIRContractTerm[]
     */
    public function getGroup(): array
    {
        return $this->group;
    }

    public function setGroup(?FHIRContractTerm ...$value): self
    {
        $this->group = array_filter($value);

        return $this;
    }

    public function addGroup(?FHIRContractTerm ...$value): self
    {
        $this->group = array_filter(array_merge($this->group, $value));

        return $this;
    }
}
