<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PatientAnimal Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRPatientAnimalInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;

class FHIRPatientAnimal extends FHIRBackboneElement implements FHIRPatientAnimalInterface
{
    public const RESOURCE_NAME = 'Patient.animal';

    protected ?FHIRCodeableConcept $species = null;
    protected ?FHIRCodeableConcept $breed = null;
    protected ?FHIRCodeableConcept $genderStatus = null;

    public function getSpecies(): ?FHIRCodeableConcept
    {
        return $this->species;
    }

    public function setSpecies(?FHIRCodeableConcept $value): self
    {
        $this->species = $value;

        return $this;
    }

    public function getBreed(): ?FHIRCodeableConcept
    {
        return $this->breed;
    }

    public function setBreed(?FHIRCodeableConcept $value): self
    {
        $this->breed = $value;

        return $this;
    }

    public function getGenderStatus(): ?FHIRCodeableConcept
    {
        return $this->genderStatus;
    }

    public function setGenderStatus(?FHIRCodeableConcept $value): self
    {
        $this->genderStatus = $value;

        return $this;
    }
}
