<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImplementationGuidePackageResource Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImplementationGuidePackageResourceInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;

class FHIRImplementationGuidePackageResource extends FHIRBackboneElement implements FHIRImplementationGuidePackageResourceInterface
{
    public const RESOURCE_NAME = 'ImplementationGuide.package.resource';

    protected ?FHIRBoolean $example = null;
    protected ?FHIRString $name = null;
    protected ?FHIRString $description = null;
    protected ?FHIRString $acronym = null;
    protected FHIRUri|FHIRReference|null $source = null;
    protected ?FHIRReference $exampleFor = null;

    public function getExample(): ?FHIRBoolean
    {
        return $this->example;
    }

    public function setExample(bool|FHIRBoolean|null $value): self
    {
        $this->example = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getAcronym(): ?FHIRString
    {
        return $this->acronym;
    }

    public function setAcronym(string|FHIRString|null $value): self
    {
        $this->acronym = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSource(): FHIRUri|FHIRReference|null
    {
        return $this->source;
    }

    public function setSource(FHIRUri|FHIRReference|null $value): self
    {
        $this->source = $value;

        return $this;
    }

    public function getExampleFor(): ?FHIRReference
    {
        return $this->exampleFor;
    }

    public function setExampleFor(?FHIRReference $value): self
    {
        $this->exampleFor = $value;

        return $this;
    }
}
