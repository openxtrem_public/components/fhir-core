<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationIngredient Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationIngredientInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRRatio;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;

class FHIRMedicationIngredient extends FHIRBackboneElement implements FHIRMedicationIngredientInterface
{
    public const RESOURCE_NAME = 'Medication.ingredient';

    protected FHIRCodeableConcept|FHIRReference|null $item = null;
    protected ?FHIRBoolean $isActive = null;
    protected ?FHIRRatio $amount = null;

    public function getItem(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->item;
    }

    public function setItem(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->item = $value;

        return $this;
    }

    public function getIsActive(): ?FHIRBoolean
    {
        return $this->isActive;
    }

    public function setIsActive(bool|FHIRBoolean|null $value): self
    {
        $this->isActive = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getAmount(): ?FHIRRatio
    {
        return $this->amount;
    }

    public function setAmount(?FHIRRatio $value): self
    {
        $this->amount = $value;

        return $this;
    }
}
