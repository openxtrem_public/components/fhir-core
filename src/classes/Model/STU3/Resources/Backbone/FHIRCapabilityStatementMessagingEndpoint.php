<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CapabilityStatementMessagingEndpoint Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCapabilityStatementMessagingEndpointInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;

class FHIRCapabilityStatementMessagingEndpoint extends FHIRBackboneElement implements FHIRCapabilityStatementMessagingEndpointInterface
{
    public const RESOURCE_NAME = 'CapabilityStatement.messaging.endpoint';

    protected ?FHIRCoding $protocol = null;
    protected ?FHIRUri $address = null;

    public function getProtocol(): ?FHIRCoding
    {
        return $this->protocol;
    }

    public function setProtocol(?FHIRCoding $value): self
    {
        $this->protocol = $value;

        return $this;
    }

    public function getAddress(): ?FHIRUri
    {
        return $this->address;
    }

    public function setAddress(string|FHIRUri|null $value): self
    {
        $this->address = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }
}
