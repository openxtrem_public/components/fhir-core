<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AuditEventSource Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRAuditEventSourceInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRAuditEventSource extends FHIRBackboneElement implements FHIRAuditEventSourceInterface
{
    public const RESOURCE_NAME = 'AuditEvent.source';

    protected ?FHIRString $site = null;
    protected ?FHIRIdentifier $identifier = null;

    /** @var FHIRCoding[] */
    protected array $type = [];

    public function getSite(): ?FHIRString
    {
        return $this->site;
    }

    public function setSite(string|FHIRString|null $value): self
    {
        $this->site = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRCoding ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRCoding ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }
}
