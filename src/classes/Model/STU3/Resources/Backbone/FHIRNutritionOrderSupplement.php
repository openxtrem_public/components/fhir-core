<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR NutritionOrderSupplement Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRNutritionOrderSupplementInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRNutritionOrderSupplement extends FHIRBackboneElement implements FHIRNutritionOrderSupplementInterface
{
    public const RESOURCE_NAME = 'NutritionOrder.supplement';

    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRString $productName = null;

    /** @var FHIRTiming[] */
    protected array $schedule = [];
    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRString $instruction = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getProductName(): ?FHIRString
    {
        return $this->productName;
    }

    public function setProductName(string|FHIRString|null $value): self
    {
        $this->productName = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRTiming[]
     */
    public function getSchedule(): array
    {
        return $this->schedule;
    }

    public function setSchedule(?FHIRTiming ...$value): self
    {
        $this->schedule = array_filter($value);

        return $this;
    }

    public function addSchedule(?FHIRTiming ...$value): self
    {
        $this->schedule = array_filter(array_merge($this->schedule, $value));

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getInstruction(): ?FHIRString
    {
        return $this->instruction;
    }

    public function setInstruction(string|FHIRString|null $value): self
    {
        $this->instruction = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
