<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationDispenseSubstitution Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationDispenseSubstitutionInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;

class FHIRMedicationDispenseSubstitution extends FHIRBackboneElement implements FHIRMedicationDispenseSubstitutionInterface
{
    public const RESOURCE_NAME = 'MedicationDispense.substitution';

    protected ?FHIRBoolean $wasSubstituted = null;
    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRCodeableConcept[] */
    protected array $reason = [];

    /** @var FHIRReference[] */
    protected array $responsibleParty = [];

    public function getWasSubstituted(): ?FHIRBoolean
    {
        return $this->wasSubstituted;
    }

    public function setWasSubstituted(bool|FHIRBoolean|null $value): self
    {
        $this->wasSubstituted = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReason(): array
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableConcept ...$value): self
    {
        $this->reason = array_filter($value);

        return $this;
    }

    public function addReason(?FHIRCodeableConcept ...$value): self
    {
        $this->reason = array_filter(array_merge($this->reason, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getResponsibleParty(): array
    {
        return $this->responsibleParty;
    }

    public function setResponsibleParty(?FHIRReference ...$value): self
    {
        $this->responsibleParty = array_filter($value);

        return $this;
    }

    public function addResponsibleParty(?FHIRReference ...$value): self
    {
        $this->responsibleParty = array_filter(array_merge($this->responsibleParty, $value));

        return $this;
    }
}
