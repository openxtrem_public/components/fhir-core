<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR EpisodeOfCareDiagnosis Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIREpisodeOfCareDiagnosisInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRPositiveInt;

class FHIREpisodeOfCareDiagnosis extends FHIRBackboneElement implements FHIREpisodeOfCareDiagnosisInterface
{
    public const RESOURCE_NAME = 'EpisodeOfCare.diagnosis';

    protected ?FHIRReference $condition = null;
    protected ?FHIRCodeableConcept $role = null;
    protected ?FHIRPositiveInt $rank = null;

    public function getCondition(): ?FHIRReference
    {
        return $this->condition;
    }

    public function setCondition(?FHIRReference $value): self
    {
        $this->condition = $value;

        return $this;
    }

    public function getRole(): ?FHIRCodeableConcept
    {
        return $this->role;
    }

    public function setRole(?FHIRCodeableConcept $value): self
    {
        $this->role = $value;

        return $this;
    }

    public function getRank(): ?FHIRPositiveInt
    {
        return $this->rank;
    }

    public function setRank(int|FHIRPositiveInt|null $value): self
    {
        $this->rank = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }
}
