<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CarePlanActivityDetail Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCarePlanActivityDetailInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRCarePlanActivityDetail extends FHIRBackboneElement implements FHIRCarePlanActivityDetailInterface
{
    public const RESOURCE_NAME = 'CarePlan.activity.detail';

    protected ?FHIRCodeableConcept $category = null;
    protected ?FHIRReference $definition = null;
    protected ?FHIRCodeableConcept $code = null;

    /** @var FHIRCodeableConcept[] */
    protected array $reasonCode = [];

    /** @var FHIRReference[] */
    protected array $reasonReference = [];

    /** @var FHIRReference[] */
    protected array $goal = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRString $statusReason = null;
    protected ?FHIRBoolean $prohibited = null;
    protected FHIRTiming|FHIRPeriod|FHIRString|null $scheduled = null;
    protected ?FHIRReference $location = null;

    /** @var FHIRReference[] */
    protected array $performer = [];
    protected FHIRCodeableConcept|FHIRReference|null $product = null;
    protected ?FHIRQuantity $dailyAmount = null;
    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRString $description = null;

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getDefinition(): ?FHIRReference
    {
        return $this->definition;
    }

    public function setDefinition(?FHIRReference $value): self
    {
        $this->definition = $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReasonCode(): array
    {
        return $this->reasonCode;
    }

    public function setReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter($value);

        return $this;
    }

    public function addReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter(array_merge($this->reasonCode, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReasonReference(): array
    {
        return $this->reasonReference;
    }

    public function setReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter($value);

        return $this;
    }

    public function addReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter(array_merge($this->reasonReference, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getGoal(): array
    {
        return $this->goal;
    }

    public function setGoal(?FHIRReference ...$value): self
    {
        $this->goal = array_filter($value);

        return $this;
    }

    public function addGoal(?FHIRReference ...$value): self
    {
        $this->goal = array_filter(array_merge($this->goal, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getStatusReason(): ?FHIRString
    {
        return $this->statusReason;
    }

    public function setStatusReason(string|FHIRString|null $value): self
    {
        $this->statusReason = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getProhibited(): ?FHIRBoolean
    {
        return $this->prohibited;
    }

    public function setProhibited(bool|FHIRBoolean|null $value): self
    {
        $this->prohibited = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getScheduled(): FHIRTiming|FHIRPeriod|FHIRString|null
    {
        return $this->scheduled;
    }

    public function setScheduled(FHIRTiming|FHIRPeriod|FHIRString|null $value): self
    {
        $this->scheduled = $value;

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPerformer(): array
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRReference ...$value): self
    {
        $this->performer = array_filter($value);

        return $this;
    }

    public function addPerformer(?FHIRReference ...$value): self
    {
        $this->performer = array_filter(array_merge($this->performer, $value));

        return $this;
    }

    public function getProduct(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->product;
    }

    public function setProduct(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->product = $value;

        return $this;
    }

    public function getDailyAmount(): ?FHIRQuantity
    {
        return $this->dailyAmount;
    }

    public function setDailyAmount(?FHIRQuantity $value): self
    {
        $this->dailyAmount = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
