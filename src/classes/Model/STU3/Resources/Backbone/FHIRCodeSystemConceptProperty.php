<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CodeSystemConceptProperty Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCodeSystemConceptPropertyInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRCodeSystemConceptProperty extends FHIRBackboneElement implements FHIRCodeSystemConceptPropertyInterface
{
    public const RESOURCE_NAME = 'CodeSystem.concept.property';

    protected ?FHIRCode $code = null;
    protected FHIRCode|FHIRCoding|FHIRString|FHIRInteger|FHIRBoolean|FHIRDateTime|null $value = null;

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getValue(): FHIRCode|FHIRCoding|FHIRString|FHIRInteger|FHIRBoolean|FHIRDateTime|null
    {
        return $this->value;
    }

    public function setValue(FHIRCode|FHIRCoding|FHIRString|FHIRInteger|FHIRBoolean|FHIRDateTime|null $value): self
    {
        $this->value = $value;

        return $this;
    }
}
