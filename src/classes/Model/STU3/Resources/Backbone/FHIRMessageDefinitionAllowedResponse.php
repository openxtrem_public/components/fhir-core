<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MessageDefinitionAllowedResponse Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMessageDefinitionAllowedResponseInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRMarkdown;

class FHIRMessageDefinitionAllowedResponse extends FHIRBackboneElement implements FHIRMessageDefinitionAllowedResponseInterface
{
    public const RESOURCE_NAME = 'MessageDefinition.allowedResponse';

    protected ?FHIRReference $message = null;
    protected ?FHIRMarkdown $situation = null;

    public function getMessage(): ?FHIRReference
    {
        return $this->message;
    }

    public function setMessage(?FHIRReference $value): self
    {
        $this->message = $value;

        return $this;
    }

    public function getSituation(): ?FHIRMarkdown
    {
        return $this->situation;
    }

    public function setSituation(string|FHIRMarkdown|null $value): self
    {
        $this->situation = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }
}
