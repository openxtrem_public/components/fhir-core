<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationDispensePerformer Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationDispensePerformerInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;

class FHIRMedicationDispensePerformer extends FHIRBackboneElement implements FHIRMedicationDispensePerformerInterface
{
    public const RESOURCE_NAME = 'MedicationDispense.performer';

    protected ?FHIRReference $actor = null;
    protected ?FHIRReference $onBehalfOf = null;

    public function getActor(): ?FHIRReference
    {
        return $this->actor;
    }

    public function setActor(?FHIRReference $value): self
    {
        $this->actor = $value;

        return $this;
    }

    public function getOnBehalfOf(): ?FHIRReference
    {
        return $this->onBehalfOf;
    }

    public function setOnBehalfOf(?FHIRReference $value): self
    {
        $this->onBehalfOf = $value;

        return $this;
    }
}
