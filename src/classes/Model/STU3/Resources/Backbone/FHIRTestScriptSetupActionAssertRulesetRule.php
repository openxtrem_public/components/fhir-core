<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestScriptSetupActionAssertRulesetRule Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestScriptSetupActionAssertRulesetRuleInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRId;

class FHIRTestScriptSetupActionAssertRulesetRule extends FHIRBackboneElement implements FHIRTestScriptSetupActionAssertRulesetRuleInterface
{
    public const RESOURCE_NAME = 'TestScript.setup.action.assert.ruleset.rule';

    protected ?FHIRId $ruleId = null;

    /** @var FHIRTestScriptSetupActionAssertRulesetRuleParam[] */
    protected array $param = [];

    public function getRuleId(): ?FHIRId
    {
        return $this->ruleId;
    }

    public function setRuleId(string|FHIRId|null $value): self
    {
        $this->ruleId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRTestScriptSetupActionAssertRulesetRuleParam[]
     */
    public function getParam(): array
    {
        return $this->param;
    }

    public function setParam(?FHIRTestScriptSetupActionAssertRulesetRuleParam ...$value): self
    {
        $this->param = array_filter($value);

        return $this;
    }

    public function addParam(?FHIRTestScriptSetupActionAssertRulesetRuleParam ...$value): self
    {
        $this->param = array_filter(array_merge($this->param, $value));

        return $this;
    }
}
