<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClaimResponseAddItem Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRClaimResponseAddItemInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRPositiveInt;

class FHIRClaimResponseAddItem extends FHIRBackboneElement implements FHIRClaimResponseAddItemInterface
{
    public const RESOURCE_NAME = 'ClaimResponse.addItem';

    /** @var FHIRPositiveInt[] */
    protected array $sequenceLinkId = [];
    protected ?FHIRCodeableConcept $revenue = null;
    protected ?FHIRCodeableConcept $category = null;
    protected ?FHIRCodeableConcept $service = null;

    /** @var FHIRCodeableConcept[] */
    protected array $modifier = [];
    protected ?FHIRMoney $fee = null;

    /** @var FHIRPositiveInt[] */
    protected array $noteNumber = [];

    /** @var FHIRClaimResponseItemAdjudication[] */
    protected array $adjudication = [];

    /** @var FHIRClaimResponseAddItemDetail[] */
    protected array $detail = [];

    /**
     * @return FHIRPositiveInt[]
     */
    public function getSequenceLinkId(): array
    {
        return $this->sequenceLinkId;
    }

    public function setSequenceLinkId(int|FHIRPositiveInt|null ...$value): self
    {
        $this->sequenceLinkId = [];
        $this->addSequenceLinkId(...$value);

        return $this;
    }

    public function addSequenceLinkId(int|FHIRPositiveInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRPositiveInt())->setValue($v) : $v, $value);

        $this->sequenceLinkId = array_filter(array_merge($this->sequenceLinkId, $values));

        return $this;
    }

    public function getRevenue(): ?FHIRCodeableConcept
    {
        return $this->revenue;
    }

    public function setRevenue(?FHIRCodeableConcept $value): self
    {
        $this->revenue = $value;

        return $this;
    }

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getService(): ?FHIRCodeableConcept
    {
        return $this->service;
    }

    public function setService(?FHIRCodeableConcept $value): self
    {
        $this->service = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getModifier(): array
    {
        return $this->modifier;
    }

    public function setModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->modifier = array_filter($value);

        return $this;
    }

    public function addModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->modifier = array_filter(array_merge($this->modifier, $value));

        return $this;
    }

    public function getFee(): ?FHIRMoney
    {
        return $this->fee;
    }

    public function setFee(?FHIRMoney $value): self
    {
        $this->fee = $value;

        return $this;
    }

    /**
     * @return FHIRPositiveInt[]
     */
    public function getNoteNumber(): array
    {
        return $this->noteNumber;
    }

    public function setNoteNumber(int|FHIRPositiveInt|null ...$value): self
    {
        $this->noteNumber = [];
        $this->addNoteNumber(...$value);

        return $this;
    }

    public function addNoteNumber(int|FHIRPositiveInt|null ...$value): self
    {
        $values = array_map(fn($v) => is_int($v) ? (new FHIRPositiveInt())->setValue($v) : $v, $value);

        $this->noteNumber = array_filter(array_merge($this->noteNumber, $values));

        return $this;
    }

    /**
     * @return FHIRClaimResponseItemAdjudication[]
     */
    public function getAdjudication(): array
    {
        return $this->adjudication;
    }

    public function setAdjudication(?FHIRClaimResponseItemAdjudication ...$value): self
    {
        $this->adjudication = array_filter($value);

        return $this;
    }

    public function addAdjudication(?FHIRClaimResponseItemAdjudication ...$value): self
    {
        $this->adjudication = array_filter(array_merge($this->adjudication, $value));

        return $this;
    }

    /**
     * @return FHIRClaimResponseAddItemDetail[]
     */
    public function getDetail(): array
    {
        return $this->detail;
    }

    public function setDetail(?FHIRClaimResponseAddItemDetail ...$value): self
    {
        $this->detail = array_filter($value);

        return $this;
    }

    public function addDetail(?FHIRClaimResponseAddItemDetail ...$value): self
    {
        $this->detail = array_filter(array_merge($this->detail, $value));

        return $this;
    }
}
