<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DataElementMapping Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRDataElementMappingInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;

class FHIRDataElementMapping extends FHIRBackboneElement implements FHIRDataElementMappingInterface
{
    public const RESOURCE_NAME = 'DataElement.mapping';

    protected ?FHIRId $identity = null;
    protected ?FHIRUri $uri = null;
    protected ?FHIRString $name = null;
    protected ?FHIRString $comment = null;

    public function getIdentity(): ?FHIRId
    {
        return $this->identity;
    }

    public function setIdentity(string|FHIRId|null $value): self
    {
        $this->identity = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getUri(): ?FHIRUri
    {
        return $this->uri;
    }

    public function setUri(string|FHIRUri|null $value): self
    {
        $this->uri = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getComment(): ?FHIRString
    {
        return $this->comment;
    }

    public function setComment(string|FHIRString|null $value): self
    {
        $this->comment = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
