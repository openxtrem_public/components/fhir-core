<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR FamilyMemberHistoryCondition Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRFamilyMemberHistoryConditionInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAge;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRFamilyMemberHistoryCondition extends FHIRBackboneElement implements FHIRFamilyMemberHistoryConditionInterface
{
    public const RESOURCE_NAME = 'FamilyMemberHistory.condition';

    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRCodeableConcept $outcome = null;
    protected FHIRAge|FHIRRange|FHIRPeriod|FHIRString|null $onset = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getOutcome(): ?FHIRCodeableConcept
    {
        return $this->outcome;
    }

    public function setOutcome(?FHIRCodeableConcept $value): self
    {
        $this->outcome = $value;

        return $this;
    }

    public function getOnset(): FHIRAge|FHIRRange|FHIRPeriod|FHIRString|null
    {
        return $this->onset;
    }

    public function setOnset(FHIRAge|FHIRRange|FHIRPeriod|FHIRString|null $value): self
    {
        $this->onset = $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }
}
