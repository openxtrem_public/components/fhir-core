<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CapabilityStatementRestResource Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRCapabilityStatementRestResourceInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRCapabilityStatementRestResource extends FHIRBackboneElement implements FHIRCapabilityStatementRestResourceInterface
{
    public const RESOURCE_NAME = 'CapabilityStatement.rest.resource';

    protected ?FHIRCode $type = null;
    protected ?FHIRReference $profile = null;
    protected ?FHIRMarkdown $documentation = null;

    /** @var FHIRCapabilityStatementRestResourceInteraction[] */
    protected array $interaction = [];
    protected ?FHIRCode $versioning = null;
    protected ?FHIRBoolean $readHistory = null;
    protected ?FHIRBoolean $updateCreate = null;
    protected ?FHIRBoolean $conditionalCreate = null;
    protected ?FHIRCode $conditionalRead = null;
    protected ?FHIRBoolean $conditionalUpdate = null;
    protected ?FHIRCode $conditionalDelete = null;

    /** @var FHIRCode[] */
    protected array $referencePolicy = [];

    /** @var FHIRString[] */
    protected array $searchInclude = [];

    /** @var FHIRString[] */
    protected array $searchRevInclude = [];

    /** @var FHIRCapabilityStatementRestResourceSearchParam[] */
    protected array $searchParam = [];

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getProfile(): ?FHIRReference
    {
        return $this->profile;
    }

    public function setProfile(?FHIRReference $value): self
    {
        $this->profile = $value;

        return $this;
    }

    public function getDocumentation(): ?FHIRMarkdown
    {
        return $this->documentation;
    }

    public function setDocumentation(string|FHIRMarkdown|null $value): self
    {
        $this->documentation = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCapabilityStatementRestResourceInteraction[]
     */
    public function getInteraction(): array
    {
        return $this->interaction;
    }

    public function setInteraction(?FHIRCapabilityStatementRestResourceInteraction ...$value): self
    {
        $this->interaction = array_filter($value);

        return $this;
    }

    public function addInteraction(?FHIRCapabilityStatementRestResourceInteraction ...$value): self
    {
        $this->interaction = array_filter(array_merge($this->interaction, $value));

        return $this;
    }

    public function getVersioning(): ?FHIRCode
    {
        return $this->versioning;
    }

    public function setVersioning(string|FHIRCode|null $value): self
    {
        $this->versioning = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getReadHistory(): ?FHIRBoolean
    {
        return $this->readHistory;
    }

    public function setReadHistory(bool|FHIRBoolean|null $value): self
    {
        $this->readHistory = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getUpdateCreate(): ?FHIRBoolean
    {
        return $this->updateCreate;
    }

    public function setUpdateCreate(bool|FHIRBoolean|null $value): self
    {
        $this->updateCreate = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getConditionalCreate(): ?FHIRBoolean
    {
        return $this->conditionalCreate;
    }

    public function setConditionalCreate(bool|FHIRBoolean|null $value): self
    {
        $this->conditionalCreate = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getConditionalRead(): ?FHIRCode
    {
        return $this->conditionalRead;
    }

    public function setConditionalRead(string|FHIRCode|null $value): self
    {
        $this->conditionalRead = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getConditionalUpdate(): ?FHIRBoolean
    {
        return $this->conditionalUpdate;
    }

    public function setConditionalUpdate(bool|FHIRBoolean|null $value): self
    {
        $this->conditionalUpdate = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getConditionalDelete(): ?FHIRCode
    {
        return $this->conditionalDelete;
    }

    public function setConditionalDelete(string|FHIRCode|null $value): self
    {
        $this->conditionalDelete = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getReferencePolicy(): array
    {
        return $this->referencePolicy;
    }

    public function setReferencePolicy(string|FHIRCode|null ...$value): self
    {
        $this->referencePolicy = [];
        $this->addReferencePolicy(...$value);

        return $this;
    }

    public function addReferencePolicy(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->referencePolicy = array_filter(array_merge($this->referencePolicy, $values));

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getSearchInclude(): array
    {
        return $this->searchInclude;
    }

    public function setSearchInclude(string|FHIRString|null ...$value): self
    {
        $this->searchInclude = [];
        $this->addSearchInclude(...$value);

        return $this;
    }

    public function addSearchInclude(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->searchInclude = array_filter(array_merge($this->searchInclude, $values));

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getSearchRevInclude(): array
    {
        return $this->searchRevInclude;
    }

    public function setSearchRevInclude(string|FHIRString|null ...$value): self
    {
        $this->searchRevInclude = [];
        $this->addSearchRevInclude(...$value);

        return $this;
    }

    public function addSearchRevInclude(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->searchRevInclude = array_filter(array_merge($this->searchRevInclude, $values));

        return $this;
    }

    /**
     * @return FHIRCapabilityStatementRestResourceSearchParam[]
     */
    public function getSearchParam(): array
    {
        return $this->searchParam;
    }

    public function setSearchParam(?FHIRCapabilityStatementRestResourceSearchParam ...$value): self
    {
        $this->searchParam = array_filter($value);

        return $this;
    }

    public function addSearchParam(?FHIRCapabilityStatementRestResourceSearchParam ...$value): self
    {
        $this->searchParam = array_filter(array_merge($this->searchParam, $value));

        return $this;
    }
}
