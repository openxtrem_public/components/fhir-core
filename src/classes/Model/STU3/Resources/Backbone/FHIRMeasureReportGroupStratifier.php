<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MeasureReportGroupStratifier Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMeasureReportGroupStratifierInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;

class FHIRMeasureReportGroupStratifier extends FHIRBackboneElement implements FHIRMeasureReportGroupStratifierInterface
{
    public const RESOURCE_NAME = 'MeasureReport.group.stratifier';

    protected ?FHIRIdentifier $identifier = null;

    /** @var FHIRMeasureReportGroupStratifierStratum[] */
    protected array $stratum = [];

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    /**
     * @return FHIRMeasureReportGroupStratifierStratum[]
     */
    public function getStratum(): array
    {
        return $this->stratum;
    }

    public function setStratum(?FHIRMeasureReportGroupStratifierStratum ...$value): self
    {
        $this->stratum = array_filter($value);

        return $this;
    }

    public function addStratum(?FHIRMeasureReportGroupStratifierStratum ...$value): self
    {
        $this->stratum = array_filter(array_merge($this->stratum, $value));

        return $this;
    }
}
