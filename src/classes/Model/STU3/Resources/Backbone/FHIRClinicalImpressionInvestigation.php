<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClinicalImpressionInvestigation Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRClinicalImpressionInvestigationInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;

class FHIRClinicalImpressionInvestigation extends FHIRBackboneElement implements FHIRClinicalImpressionInvestigationInterface
{
    public const RESOURCE_NAME = 'ClinicalImpression.investigation';

    protected ?FHIRCodeableConcept $code = null;

    /** @var FHIRReference[] */
    protected array $item = [];

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getItem(): array
    {
        return $this->item;
    }

    public function setItem(?FHIRReference ...$value): self
    {
        $this->item = array_filter($value);

        return $this;
    }

    public function addItem(?FHIRReference ...$value): self
    {
        $this->item = array_filter(array_merge($this->item, $value));

        return $this;
    }
}
