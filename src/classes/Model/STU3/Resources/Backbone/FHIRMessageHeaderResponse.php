<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MessageHeaderResponse Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMessageHeaderResponseInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRId;

class FHIRMessageHeaderResponse extends FHIRBackboneElement implements FHIRMessageHeaderResponseInterface
{
    public const RESOURCE_NAME = 'MessageHeader.response';

    protected ?FHIRId $identifier = null;
    protected ?FHIRCode $code = null;
    protected ?FHIRReference $details = null;

    public function getIdentifier(): ?FHIRId
    {
        return $this->identifier;
    }

    public function setIdentifier(string|FHIRId|null $value): self
    {
        $this->identifier = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCode
    {
        return $this->code;
    }

    public function setCode(string|FHIRCode|null $value): self
    {
        $this->code = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDetails(): ?FHIRReference
    {
        return $this->details;
    }

    public function setDetails(?FHIRReference $value): self
    {
        $this->details = $value;

        return $this;
    }
}
