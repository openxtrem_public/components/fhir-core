<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClaimResponsePayment Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRClaimResponsePaymentInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDate;

class FHIRClaimResponsePayment extends FHIRBackboneElement implements FHIRClaimResponsePaymentInterface
{
    public const RESOURCE_NAME = 'ClaimResponse.payment';

    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRMoney $adjustment = null;
    protected ?FHIRCodeableConcept $adjustmentReason = null;
    protected ?FHIRDate $date = null;
    protected ?FHIRMoney $amount = null;
    protected ?FHIRIdentifier $identifier = null;

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getAdjustment(): ?FHIRMoney
    {
        return $this->adjustment;
    }

    public function setAdjustment(?FHIRMoney $value): self
    {
        $this->adjustment = $value;

        return $this;
    }

    public function getAdjustmentReason(): ?FHIRCodeableConcept
    {
        return $this->adjustmentReason;
    }

    public function setAdjustmentReason(?FHIRCodeableConcept $value): self
    {
        $this->adjustmentReason = $value;

        return $this;
    }

    public function getDate(): ?FHIRDate
    {
        return $this->date;
    }

    public function setDate(string|FHIRDate|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getAmount(): ?FHIRMoney
    {
        return $this->amount;
    }

    public function setAmount(?FHIRMoney $value): self
    {
        $this->amount = $value;

        return $this;
    }

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }
}
