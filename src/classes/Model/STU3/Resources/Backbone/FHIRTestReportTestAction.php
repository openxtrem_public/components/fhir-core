<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestReportTestAction Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestReportTestActionInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;

class FHIRTestReportTestAction extends FHIRBackboneElement implements FHIRTestReportTestActionInterface
{
    public const RESOURCE_NAME = 'TestReport.test.action';

    protected ?FHIRTestReportSetupActionOperation $operation = null;
    protected ?FHIRTestReportSetupActionAssert $assert = null;

    public function getOperation(): ?FHIRTestReportSetupActionOperation
    {
        return $this->operation;
    }

    public function setOperation(?FHIRTestReportSetupActionOperation $value): self
    {
        $this->operation = $value;

        return $this;
    }

    public function getAssert(): ?FHIRTestReportSetupActionAssert
    {
        return $this->assert;
    }

    public function setAssert(?FHIRTestReportSetupActionAssert $value): self
    {
        $this->assert = $value;

        return $this;
    }
}
