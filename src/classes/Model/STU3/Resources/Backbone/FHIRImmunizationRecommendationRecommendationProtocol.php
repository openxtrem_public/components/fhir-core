<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImmunizationRecommendationRecommendationProtocol Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRImmunizationRecommendationRecommendationProtocolInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRImmunizationRecommendationRecommendationProtocol extends FHIRBackboneElement implements FHIRImmunizationRecommendationRecommendationProtocolInterface
{
    public const RESOURCE_NAME = 'ImmunizationRecommendation.recommendation.protocol';

    protected ?FHIRPositiveInt $doseSequence = null;
    protected ?FHIRString $description = null;
    protected ?FHIRReference $authority = null;
    protected ?FHIRString $series = null;

    public function getDoseSequence(): ?FHIRPositiveInt
    {
        return $this->doseSequence;
    }

    public function setDoseSequence(int|FHIRPositiveInt|null $value): self
    {
        $this->doseSequence = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getAuthority(): ?FHIRReference
    {
        return $this->authority;
    }

    public function setAuthority(?FHIRReference $value): self
    {
        $this->authority = $value;

        return $this;
    }

    public function getSeries(): ?FHIRString
    {
        return $this->series;
    }

    public function setSeries(string|FHIRString|null $value): self
    {
        $this->series = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
