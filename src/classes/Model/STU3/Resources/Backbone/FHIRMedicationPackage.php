<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationPackage Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRMedicationPackageInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;

class FHIRMedicationPackage extends FHIRBackboneElement implements FHIRMedicationPackageInterface
{
    public const RESOURCE_NAME = 'Medication.package';

    protected ?FHIRCodeableConcept $container = null;

    /** @var FHIRMedicationPackageContent[] */
    protected array $content = [];

    /** @var FHIRMedicationPackageBatch[] */
    protected array $batch = [];

    public function getContainer(): ?FHIRCodeableConcept
    {
        return $this->container;
    }

    public function setContainer(?FHIRCodeableConcept $value): self
    {
        $this->container = $value;

        return $this;
    }

    /**
     * @return FHIRMedicationPackageContent[]
     */
    public function getContent(): array
    {
        return $this->content;
    }

    public function setContent(?FHIRMedicationPackageContent ...$value): self
    {
        $this->content = array_filter($value);

        return $this;
    }

    public function addContent(?FHIRMedicationPackageContent ...$value): self
    {
        $this->content = array_filter(array_merge($this->content, $value));

        return $this;
    }

    /**
     * @return FHIRMedicationPackageBatch[]
     */
    public function getBatch(): array
    {
        return $this->batch;
    }

    public function setBatch(?FHIRMedicationPackageBatch ...$value): self
    {
        $this->batch = array_filter($value);

        return $this;
    }

    public function addBatch(?FHIRMedicationPackageBatch ...$value): self
    {
        $this->batch = array_filter(array_merge($this->batch, $value));

        return $this;
    }
}
