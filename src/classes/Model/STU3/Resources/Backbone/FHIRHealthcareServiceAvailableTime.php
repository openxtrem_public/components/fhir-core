<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR HealthcareServiceAvailableTime Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRHealthcareServiceAvailableTimeInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRTime;

class FHIRHealthcareServiceAvailableTime extends FHIRBackboneElement implements FHIRHealthcareServiceAvailableTimeInterface
{
    public const RESOURCE_NAME = 'HealthcareService.availableTime';

    /** @var FHIRCode[] */
    protected array $daysOfWeek = [];
    protected ?FHIRBoolean $allDay = null;
    protected ?FHIRTime $availableStartTime = null;
    protected ?FHIRTime $availableEndTime = null;

    /**
     * @return FHIRCode[]
     */
    public function getDaysOfWeek(): array
    {
        return $this->daysOfWeek;
    }

    public function setDaysOfWeek(string|FHIRCode|null ...$value): self
    {
        $this->daysOfWeek = [];
        $this->addDaysOfWeek(...$value);

        return $this;
    }

    public function addDaysOfWeek(string|FHIRCode|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRCode())->setValue($v) : $v, $value);

        $this->daysOfWeek = array_filter(array_merge($this->daysOfWeek, $values));

        return $this;
    }

    public function getAllDay(): ?FHIRBoolean
    {
        return $this->allDay;
    }

    public function setAllDay(bool|FHIRBoolean|null $value): self
    {
        $this->allDay = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getAvailableStartTime(): ?FHIRTime
    {
        return $this->availableStartTime;
    }

    public function setAvailableStartTime(string|FHIRTime|null $value): self
    {
        $this->availableStartTime = is_string($value) ? (new FHIRTime())->setValue($value) : $value;

        return $this;
    }

    public function getAvailableEndTime(): ?FHIRTime
    {
        return $this->availableEndTime;
    }

    public function setAvailableEndTime(string|FHIRTime|null $value): self
    {
        $this->availableEndTime = is_string($value) ? (new FHIRTime())->setValue($value) : $value;

        return $this;
    }
}
