<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR TestReportSetupAction Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRTestReportSetupActionInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;

class FHIRTestReportSetupAction extends FHIRBackboneElement implements FHIRTestReportSetupActionInterface
{
    public const RESOURCE_NAME = 'TestReport.setup.action';

    protected ?FHIRTestReportSetupActionOperation $operation = null;
    protected ?FHIRTestReportSetupActionAssert $assert = null;

    public function getOperation(): ?FHIRTestReportSetupActionOperation
    {
        return $this->operation;
    }

    public function setOperation(?FHIRTestReportSetupActionOperation $value): self
    {
        $this->operation = $value;

        return $this;
    }

    public function getAssert(): ?FHIRTestReportSetupActionAssert
    {
        return $this->assert;
    }

    public function setAssert(?FHIRTestReportSetupActionAssert $value): self
    {
        $this->assert = $value;

        return $this;
    }
}
