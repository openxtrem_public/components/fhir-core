<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ConsentPolicy Backbone
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources\Backbone;

use Ox\Components\FHIRCore\Interfaces\Resources\Backbone\FHIRConsentPolicyInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;

class FHIRConsentPolicy extends FHIRBackboneElement implements FHIRConsentPolicyInterface
{
    public const RESOURCE_NAME = 'Consent.policy';

    protected ?FHIRUri $authority = null;
    protected ?FHIRUri $uri = null;

    public function getAuthority(): ?FHIRUri
    {
        return $this->authority;
    }

    public function setAuthority(string|FHIRUri|null $value): self
    {
        $this->authority = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getUri(): ?FHIRUri
    {
        return $this->uri;
    }

    public function setUri(string|FHIRUri|null $value): self
    {
        $this->uri = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }
}
