<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR NutritionOrder Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRNutritionOrderInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRNutritionOrderEnteralFormula;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRNutritionOrderOralDiet;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRNutritionOrderSupplement;

class FHIRNutritionOrder extends FHIRDomainResource implements FHIRNutritionOrderInterface
{
    public const RESOURCE_NAME = 'NutritionOrder';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRReference $patient = null;
    protected ?FHIRReference $encounter = null;
    protected ?FHIRDateTime $dateTime = null;
    protected ?FHIRReference $orderer = null;

    /** @var FHIRReference[] */
    protected array $allergyIntolerance = [];

    /** @var FHIRCodeableConcept[] */
    protected array $foodPreferenceModifier = [];

    /** @var FHIRCodeableConcept[] */
    protected array $excludeFoodModifier = [];
    protected ?FHIRNutritionOrderOralDiet $oralDiet = null;

    /** @var FHIRNutritionOrderSupplement[] */
    protected array $supplement = [];
    protected ?FHIRNutritionOrderEnteralFormula $enteralFormula = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getDateTime(): ?FHIRDateTime
    {
        return $this->dateTime;
    }

    public function setDateTime(string|FHIRDateTime|null $value): self
    {
        $this->dateTime = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getOrderer(): ?FHIRReference
    {
        return $this->orderer;
    }

    public function setOrderer(?FHIRReference $value): self
    {
        $this->orderer = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAllergyIntolerance(): array
    {
        return $this->allergyIntolerance;
    }

    public function setAllergyIntolerance(?FHIRReference ...$value): self
    {
        $this->allergyIntolerance = array_filter($value);

        return $this;
    }

    public function addAllergyIntolerance(?FHIRReference ...$value): self
    {
        $this->allergyIntolerance = array_filter(array_merge($this->allergyIntolerance, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getFoodPreferenceModifier(): array
    {
        return $this->foodPreferenceModifier;
    }

    public function setFoodPreferenceModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->foodPreferenceModifier = array_filter($value);

        return $this;
    }

    public function addFoodPreferenceModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->foodPreferenceModifier = array_filter(array_merge($this->foodPreferenceModifier, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getExcludeFoodModifier(): array
    {
        return $this->excludeFoodModifier;
    }

    public function setExcludeFoodModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->excludeFoodModifier = array_filter($value);

        return $this;
    }

    public function addExcludeFoodModifier(?FHIRCodeableConcept ...$value): self
    {
        $this->excludeFoodModifier = array_filter(array_merge($this->excludeFoodModifier, $value));

        return $this;
    }

    public function getOralDiet(): ?FHIRNutritionOrderOralDiet
    {
        return $this->oralDiet;
    }

    public function setOralDiet(?FHIRNutritionOrderOralDiet $value): self
    {
        $this->oralDiet = $value;

        return $this;
    }

    /**
     * @return FHIRNutritionOrderSupplement[]
     */
    public function getSupplement(): array
    {
        return $this->supplement;
    }

    public function setSupplement(?FHIRNutritionOrderSupplement ...$value): self
    {
        $this->supplement = array_filter($value);

        return $this;
    }

    public function addSupplement(?FHIRNutritionOrderSupplement ...$value): self
    {
        $this->supplement = array_filter(array_merge($this->supplement, $value));

        return $this;
    }

    public function getEnteralFormula(): ?FHIRNutritionOrderEnteralFormula
    {
        return $this->enteralFormula;
    }

    public function setEnteralFormula(?FHIRNutritionOrderEnteralFormula $value): self
    {
        $this->enteralFormula = $value;

        return $this;
    }
}
