<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR FamilyMemberHistory Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRFamilyMemberHistoryInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAge;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRFamilyMemberHistoryCondition;

class FHIRFamilyMemberHistory extends FHIRDomainResource implements FHIRFamilyMemberHistoryInterface
{
    public const RESOURCE_NAME = 'FamilyMemberHistory';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRReference[] */
    protected array $definition = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRBoolean $notDone = null;
    protected ?FHIRCodeableConcept $notDoneReason = null;
    protected ?FHIRReference $patient = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRString $name = null;
    protected ?FHIRCodeableConcept $relationship = null;
    protected ?FHIRCode $gender = null;
    protected FHIRPeriod|FHIRDate|FHIRString|null $born = null;
    protected FHIRAge|FHIRRange|FHIRString|null $age = null;
    protected ?FHIRBoolean $estimatedAge = null;
    protected FHIRBoolean|FHIRAge|FHIRRange|FHIRDate|FHIRString|null $deceased = null;

    /** @var FHIRCodeableConcept[] */
    protected array $reasonCode = [];

    /** @var FHIRReference[] */
    protected array $reasonReference = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRFamilyMemberHistoryCondition[] */
    protected array $condition = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getDefinition(): array
    {
        return $this->definition;
    }

    public function setDefinition(?FHIRReference ...$value): self
    {
        $this->definition = array_filter($value);

        return $this;
    }

    public function addDefinition(?FHIRReference ...$value): self
    {
        $this->definition = array_filter(array_merge($this->definition, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getNotDone(): ?FHIRBoolean
    {
        return $this->notDone;
    }

    public function setNotDone(bool|FHIRBoolean|null $value): self
    {
        $this->notDone = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getNotDoneReason(): ?FHIRCodeableConcept
    {
        return $this->notDoneReason;
    }

    public function setNotDoneReason(?FHIRCodeableConcept $value): self
    {
        $this->notDoneReason = $value;

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getRelationship(): ?FHIRCodeableConcept
    {
        return $this->relationship;
    }

    public function setRelationship(?FHIRCodeableConcept $value): self
    {
        $this->relationship = $value;

        return $this;
    }

    public function getGender(): ?FHIRCode
    {
        return $this->gender;
    }

    public function setGender(string|FHIRCode|null $value): self
    {
        $this->gender = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getBorn(): FHIRPeriod|FHIRDate|FHIRString|null
    {
        return $this->born;
    }

    public function setBorn(FHIRPeriod|FHIRDate|FHIRString|null $value): self
    {
        $this->born = $value;

        return $this;
    }

    public function getAge(): FHIRAge|FHIRRange|FHIRString|null
    {
        return $this->age;
    }

    public function setAge(FHIRAge|FHIRRange|FHIRString|null $value): self
    {
        $this->age = $value;

        return $this;
    }

    public function getEstimatedAge(): ?FHIRBoolean
    {
        return $this->estimatedAge;
    }

    public function setEstimatedAge(bool|FHIRBoolean|null $value): self
    {
        $this->estimatedAge = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getDeceased(): FHIRBoolean|FHIRAge|FHIRRange|FHIRDate|FHIRString|null
    {
        return $this->deceased;
    }

    public function setDeceased(FHIRBoolean|FHIRAge|FHIRRange|FHIRDate|FHIRString|null $value): self
    {
        $this->deceased = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReasonCode(): array
    {
        return $this->reasonCode;
    }

    public function setReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter($value);

        return $this;
    }

    public function addReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter(array_merge($this->reasonCode, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReasonReference(): array
    {
        return $this->reasonReference;
    }

    public function setReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter($value);

        return $this;
    }

    public function addReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter(array_merge($this->reasonReference, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRFamilyMemberHistoryCondition[]
     */
    public function getCondition(): array
    {
        return $this->condition;
    }

    public function setCondition(?FHIRFamilyMemberHistoryCondition ...$value): self
    {
        $this->condition = array_filter($value);

        return $this;
    }

    public function addCondition(?FHIRFamilyMemberHistoryCondition ...$value): self
    {
        $this->condition = array_filter(array_merge($this->condition, $value));

        return $this;
    }
}
