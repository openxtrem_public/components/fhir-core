<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR StructureDefinition Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRStructureDefinitionInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRStructureDefinitionDifferential;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRStructureDefinitionMapping;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRStructureDefinitionSnapshot;

class FHIRStructureDefinition extends FHIRDomainResource implements FHIRStructureDefinitionInterface
{
    public const RESOURCE_NAME = 'StructureDefinition';

    protected ?FHIRUri $url = null;

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRString $version = null;
    protected ?FHIRString $name = null;
    protected ?FHIRString $title = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRBoolean $experimental = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRString $publisher = null;

    /** @var FHIRContactDetail[] */
    protected array $contact = [];
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRUsageContext[] */
    protected array $useContext = [];

    /** @var FHIRCodeableConcept[] */
    protected array $jurisdiction = [];
    protected ?FHIRMarkdown $purpose = null;
    protected ?FHIRMarkdown $copyright = null;

    /** @var FHIRCoding[] */
    protected array $keyword = [];
    protected ?FHIRId $fhirVersion = null;

    /** @var FHIRStructureDefinitionMapping[] */
    protected array $mapping = [];
    protected ?FHIRCode $kind = null;
    protected ?FHIRBoolean $abstract = null;
    protected ?FHIRCode $contextType = null;

    /** @var FHIRString[] */
    protected array $context = [];

    /** @var FHIRString[] */
    protected array $contextInvariant = [];
    protected ?FHIRCode $type = null;
    protected ?FHIRUri $baseDefinition = null;
    protected ?FHIRCode $derivation = null;
    protected ?FHIRStructureDefinitionSnapshot $snapshot = null;
    protected ?FHIRStructureDefinitionDifferential $differential = null;

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getExperimental(): ?FHIRBoolean
    {
        return $this->experimental;
    }

    public function setExperimental(bool|FHIRBoolean|null $value): self
    {
        $this->experimental = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPublisher(): ?FHIRString
    {
        return $this->publisher;
    }

    public function setPublisher(string|FHIRString|null $value): self
    {
        $this->publisher = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUseContext(): array
    {
        return $this->useContext;
    }

    public function setUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter($value);

        return $this;
    }

    public function addUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter(array_merge($this->useContext, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getJurisdiction(): array
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter($value);

        return $this;
    }

    public function addJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter(array_merge($this->jurisdiction, $value));

        return $this;
    }

    public function getPurpose(): ?FHIRMarkdown
    {
        return $this->purpose;
    }

    public function setPurpose(string|FHIRMarkdown|null $value): self
    {
        $this->purpose = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getCopyright(): ?FHIRMarkdown
    {
        return $this->copyright;
    }

    public function setCopyright(string|FHIRMarkdown|null $value): self
    {
        $this->copyright = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getKeyword(): array
    {
        return $this->keyword;
    }

    public function setKeyword(?FHIRCoding ...$value): self
    {
        $this->keyword = array_filter($value);

        return $this;
    }

    public function addKeyword(?FHIRCoding ...$value): self
    {
        $this->keyword = array_filter(array_merge($this->keyword, $value));

        return $this;
    }

    public function getFhirVersion(): ?FHIRId
    {
        return $this->fhirVersion;
    }

    public function setFhirVersion(string|FHIRId|null $value): self
    {
        $this->fhirVersion = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRStructureDefinitionMapping[]
     */
    public function getMapping(): array
    {
        return $this->mapping;
    }

    public function setMapping(?FHIRStructureDefinitionMapping ...$value): self
    {
        $this->mapping = array_filter($value);

        return $this;
    }

    public function addMapping(?FHIRStructureDefinitionMapping ...$value): self
    {
        $this->mapping = array_filter(array_merge($this->mapping, $value));

        return $this;
    }

    public function getKind(): ?FHIRCode
    {
        return $this->kind;
    }

    public function setKind(string|FHIRCode|null $value): self
    {
        $this->kind = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getAbstract(): ?FHIRBoolean
    {
        return $this->abstract;
    }

    public function setAbstract(bool|FHIRBoolean|null $value): self
    {
        $this->abstract = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getContextType(): ?FHIRCode
    {
        return $this->contextType;
    }

    public function setContextType(string|FHIRCode|null $value): self
    {
        $this->contextType = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getContext(): array
    {
        return $this->context;
    }

    public function setContext(string|FHIRString|null ...$value): self
    {
        $this->context = [];
        $this->addContext(...$value);

        return $this;
    }

    public function addContext(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->context = array_filter(array_merge($this->context, $values));

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getContextInvariant(): array
    {
        return $this->contextInvariant;
    }

    public function setContextInvariant(string|FHIRString|null ...$value): self
    {
        $this->contextInvariant = [];
        $this->addContextInvariant(...$value);

        return $this;
    }

    public function addContextInvariant(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->contextInvariant = array_filter(array_merge($this->contextInvariant, $values));

        return $this;
    }

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getBaseDefinition(): ?FHIRUri
    {
        return $this->baseDefinition;
    }

    public function setBaseDefinition(string|FHIRUri|null $value): self
    {
        $this->baseDefinition = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getDerivation(): ?FHIRCode
    {
        return $this->derivation;
    }

    public function setDerivation(string|FHIRCode|null $value): self
    {
        $this->derivation = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getSnapshot(): ?FHIRStructureDefinitionSnapshot
    {
        return $this->snapshot;
    }

    public function setSnapshot(?FHIRStructureDefinitionSnapshot $value): self
    {
        $this->snapshot = $value;

        return $this;
    }

    public function getDifferential(): ?FHIRStructureDefinitionDifferential
    {
        return $this->differential;
    }

    public function setDifferential(?FHIRStructureDefinitionDifferential $value): self
    {
        $this->differential = $value;

        return $this;
    }
}
