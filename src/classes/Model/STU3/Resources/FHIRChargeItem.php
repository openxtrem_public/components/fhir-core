<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ChargeItem Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRChargeItemInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRChargeItemParticipant;

class FHIRChargeItem extends FHIRDomainResource implements FHIRChargeItemInterface
{
    public const RESOURCE_NAME = 'ChargeItem';

    protected ?FHIRIdentifier $identifier = null;

    /** @var FHIRUri[] */
    protected array $definition = [];
    protected ?FHIRCode $status = null;

    /** @var FHIRReference[] */
    protected array $partOf = [];
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $context = null;
    protected FHIRDateTime|FHIRPeriod|FHIRTiming|null $occurrence = null;

    /** @var FHIRChargeItemParticipant[] */
    protected array $participant = [];
    protected ?FHIRReference $performingOrganization = null;
    protected ?FHIRReference $requestingOrganization = null;
    protected ?FHIRQuantity $quantity = null;

    /** @var FHIRCodeableConcept[] */
    protected array $bodysite = [];
    protected ?FHIRDecimal $factorOverride = null;
    protected ?FHIRMoney $priceOverride = null;
    protected ?FHIRString $overrideReason = null;
    protected ?FHIRReference $enterer = null;
    protected ?FHIRDateTime $enteredDate = null;

    /** @var FHIRCodeableConcept[] */
    protected array $reason = [];

    /** @var FHIRElement[] */
    protected array $service = [];

    /** @var FHIRReference[] */
    protected array $account = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRReference[] */
    protected array $supportingInformation = [];

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    /**
     * @return FHIRUri[]
     */
    public function getDefinition(): array
    {
        return $this->definition;
    }

    public function setDefinition(string|FHIRUri|null ...$value): self
    {
        $this->definition = [];
        $this->addDefinition(...$value);

        return $this;
    }

    public function addDefinition(string|FHIRUri|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUri())->setValue($v) : $v, $value);

        $this->definition = array_filter(array_merge($this->definition, $values));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPartOf(): array
    {
        return $this->partOf;
    }

    public function setPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter($value);

        return $this;
    }

    public function addPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter(array_merge($this->partOf, $value));

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getContext(): ?FHIRReference
    {
        return $this->context;
    }

    public function setContext(?FHIRReference $value): self
    {
        $this->context = $value;

        return $this;
    }

    public function getOccurrence(): FHIRDateTime|FHIRPeriod|FHIRTiming|null
    {
        return $this->occurrence;
    }

    public function setOccurrence(FHIRDateTime|FHIRPeriod|FHIRTiming|null $value): self
    {
        $this->occurrence = $value;

        return $this;
    }

    /**
     * @return FHIRChargeItemParticipant[]
     */
    public function getParticipant(): array
    {
        return $this->participant;
    }

    public function setParticipant(?FHIRChargeItemParticipant ...$value): self
    {
        $this->participant = array_filter($value);

        return $this;
    }

    public function addParticipant(?FHIRChargeItemParticipant ...$value): self
    {
        $this->participant = array_filter(array_merge($this->participant, $value));

        return $this;
    }

    public function getPerformingOrganization(): ?FHIRReference
    {
        return $this->performingOrganization;
    }

    public function setPerformingOrganization(?FHIRReference $value): self
    {
        $this->performingOrganization = $value;

        return $this;
    }

    public function getRequestingOrganization(): ?FHIRReference
    {
        return $this->requestingOrganization;
    }

    public function setRequestingOrganization(?FHIRReference $value): self
    {
        $this->requestingOrganization = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getBodysite(): array
    {
        return $this->bodysite;
    }

    public function setBodysite(?FHIRCodeableConcept ...$value): self
    {
        $this->bodysite = array_filter($value);

        return $this;
    }

    public function addBodysite(?FHIRCodeableConcept ...$value): self
    {
        $this->bodysite = array_filter(array_merge($this->bodysite, $value));

        return $this;
    }

    public function getFactorOverride(): ?FHIRDecimal
    {
        return $this->factorOverride;
    }

    public function setFactorOverride(float|FHIRDecimal|null $value): self
    {
        $this->factorOverride = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getPriceOverride(): ?FHIRMoney
    {
        return $this->priceOverride;
    }

    public function setPriceOverride(?FHIRMoney $value): self
    {
        $this->priceOverride = $value;

        return $this;
    }

    public function getOverrideReason(): ?FHIRString
    {
        return $this->overrideReason;
    }

    public function setOverrideReason(string|FHIRString|null $value): self
    {
        $this->overrideReason = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getEnterer(): ?FHIRReference
    {
        return $this->enterer;
    }

    public function setEnterer(?FHIRReference $value): self
    {
        $this->enterer = $value;

        return $this;
    }

    public function getEnteredDate(): ?FHIRDateTime
    {
        return $this->enteredDate;
    }

    public function setEnteredDate(string|FHIRDateTime|null $value): self
    {
        $this->enteredDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReason(): array
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableConcept ...$value): self
    {
        $this->reason = array_filter($value);

        return $this;
    }

    public function addReason(?FHIRCodeableConcept ...$value): self
    {
        $this->reason = array_filter(array_merge($this->reason, $value));

        return $this;
    }

    /**
     * @return FHIRElement[]
     */
    public function getService(): array
    {
        return $this->service;
    }

    public function setService(?FHIRElement ...$value): self
    {
        $this->service = array_filter($value);

        return $this;
    }

    public function addService(?FHIRElement ...$value): self
    {
        $this->service = array_filter(array_merge($this->service, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAccount(): array
    {
        return $this->account;
    }

    public function setAccount(?FHIRReference ...$value): self
    {
        $this->account = array_filter($value);

        return $this;
    }

    public function addAccount(?FHIRReference ...$value): self
    {
        $this->account = array_filter(array_merge($this->account, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupportingInformation(): array
    {
        return $this->supportingInformation;
    }

    public function setSupportingInformation(?FHIRReference ...$value): self
    {
        $this->supportingInformation = array_filter($value);

        return $this;
    }

    public function addSupportingInformation(?FHIRReference ...$value): self
    {
        $this->supportingInformation = array_filter(array_merge($this->supportingInformation, $value));

        return $this;
    }
}
