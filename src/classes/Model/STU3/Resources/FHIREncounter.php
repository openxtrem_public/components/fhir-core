<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Encounter Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIREncounterInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRDuration;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIREncounterClassHistory;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIREncounterDiagnosis;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIREncounterHospitalization;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIREncounterLocation;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIREncounterParticipant;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIREncounterStatusHistory;

class FHIREncounter extends FHIRDomainResource implements FHIREncounterInterface
{
    public const RESOURCE_NAME = 'Encounter';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;

    /** @var FHIREncounterStatusHistory[] */
    protected array $statusHistory = [];
    protected ?FHIRCoding $class = null;

    /** @var FHIREncounterClassHistory[] */
    protected array $classHistory = [];

    /** @var FHIRCodeableConcept[] */
    protected array $type = [];
    protected ?FHIRCodeableConcept $priority = null;
    protected ?FHIRReference $subject = null;

    /** @var FHIRReference[] */
    protected array $episodeOfCare = [];

    /** @var FHIRReference[] */
    protected array $incomingReferral = [];

    /** @var FHIREncounterParticipant[] */
    protected array $participant = [];
    protected ?FHIRReference $appointment = null;
    protected ?FHIRPeriod $period = null;
    protected ?FHIRDuration $length = null;

    /** @var FHIRCodeableConcept[] */
    protected array $reason = [];

    /** @var FHIREncounterDiagnosis[] */
    protected array $diagnosis = [];

    /** @var FHIRReference[] */
    protected array $account = [];
    protected ?FHIREncounterHospitalization $hospitalization = null;

    /** @var FHIREncounterLocation[] */
    protected array $location = [];
    protected ?FHIRReference $serviceProvider = null;
    protected ?FHIRReference $partOf = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIREncounterStatusHistory[]
     */
    public function getStatusHistory(): array
    {
        return $this->statusHistory;
    }

    public function setStatusHistory(?FHIREncounterStatusHistory ...$value): self
    {
        $this->statusHistory = array_filter($value);

        return $this;
    }

    public function addStatusHistory(?FHIREncounterStatusHistory ...$value): self
    {
        $this->statusHistory = array_filter(array_merge($this->statusHistory, $value));

        return $this;
    }

    public function getClass(): ?FHIRCoding
    {
        return $this->class;
    }

    public function setClass(?FHIRCoding $value): self
    {
        $this->class = $value;

        return $this;
    }

    /**
     * @return FHIREncounterClassHistory[]
     */
    public function getClassHistory(): array
    {
        return $this->classHistory;
    }

    public function setClassHistory(?FHIREncounterClassHistory ...$value): self
    {
        $this->classHistory = array_filter($value);

        return $this;
    }

    public function addClassHistory(?FHIREncounterClassHistory ...$value): self
    {
        $this->classHistory = array_filter(array_merge($this->classHistory, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }

    public function getPriority(): ?FHIRCodeableConcept
    {
        return $this->priority;
    }

    public function setPriority(?FHIRCodeableConcept $value): self
    {
        $this->priority = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEpisodeOfCare(): array
    {
        return $this->episodeOfCare;
    }

    public function setEpisodeOfCare(?FHIRReference ...$value): self
    {
        $this->episodeOfCare = array_filter($value);

        return $this;
    }

    public function addEpisodeOfCare(?FHIRReference ...$value): self
    {
        $this->episodeOfCare = array_filter(array_merge($this->episodeOfCare, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getIncomingReferral(): array
    {
        return $this->incomingReferral;
    }

    public function setIncomingReferral(?FHIRReference ...$value): self
    {
        $this->incomingReferral = array_filter($value);

        return $this;
    }

    public function addIncomingReferral(?FHIRReference ...$value): self
    {
        $this->incomingReferral = array_filter(array_merge($this->incomingReferral, $value));

        return $this;
    }

    /**
     * @return FHIREncounterParticipant[]
     */
    public function getParticipant(): array
    {
        return $this->participant;
    }

    public function setParticipant(?FHIREncounterParticipant ...$value): self
    {
        $this->participant = array_filter($value);

        return $this;
    }

    public function addParticipant(?FHIREncounterParticipant ...$value): self
    {
        $this->participant = array_filter(array_merge($this->participant, $value));

        return $this;
    }

    public function getAppointment(): ?FHIRReference
    {
        return $this->appointment;
    }

    public function setAppointment(?FHIRReference $value): self
    {
        $this->appointment = $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    public function getLength(): ?FHIRDuration
    {
        return $this->length;
    }

    public function setLength(?FHIRDuration $value): self
    {
        $this->length = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReason(): array
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableConcept ...$value): self
    {
        $this->reason = array_filter($value);

        return $this;
    }

    public function addReason(?FHIRCodeableConcept ...$value): self
    {
        $this->reason = array_filter(array_merge($this->reason, $value));

        return $this;
    }

    /**
     * @return FHIREncounterDiagnosis[]
     */
    public function getDiagnosis(): array
    {
        return $this->diagnosis;
    }

    public function setDiagnosis(?FHIREncounterDiagnosis ...$value): self
    {
        $this->diagnosis = array_filter($value);

        return $this;
    }

    public function addDiagnosis(?FHIREncounterDiagnosis ...$value): self
    {
        $this->diagnosis = array_filter(array_merge($this->diagnosis, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAccount(): array
    {
        return $this->account;
    }

    public function setAccount(?FHIRReference ...$value): self
    {
        $this->account = array_filter($value);

        return $this;
    }

    public function addAccount(?FHIRReference ...$value): self
    {
        $this->account = array_filter(array_merge($this->account, $value));

        return $this;
    }

    public function getHospitalization(): ?FHIREncounterHospitalization
    {
        return $this->hospitalization;
    }

    public function setHospitalization(?FHIREncounterHospitalization $value): self
    {
        $this->hospitalization = $value;

        return $this;
    }

    /**
     * @return FHIREncounterLocation[]
     */
    public function getLocation(): array
    {
        return $this->location;
    }

    public function setLocation(?FHIREncounterLocation ...$value): self
    {
        $this->location = array_filter($value);

        return $this;
    }

    public function addLocation(?FHIREncounterLocation ...$value): self
    {
        $this->location = array_filter(array_merge($this->location, $value));

        return $this;
    }

    public function getServiceProvider(): ?FHIRReference
    {
        return $this->serviceProvider;
    }

    public function setServiceProvider(?FHIRReference $value): self
    {
        $this->serviceProvider = $value;

        return $this;
    }

    public function getPartOf(): ?FHIRReference
    {
        return $this->partOf;
    }

    public function setPartOf(?FHIRReference $value): self
    {
        $this->partOf = $value;

        return $this;
    }
}
