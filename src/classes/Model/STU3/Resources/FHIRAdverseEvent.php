<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AdverseEvent Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRAdverseEventInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRAdverseEventSuspectEntity;

class FHIRAdverseEvent extends FHIRDomainResource implements FHIRAdverseEventInterface
{
    public const RESOURCE_NAME = 'AdverseEvent';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRCode $category = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRDateTime $date = null;

    /** @var FHIRReference[] */
    protected array $reaction = [];
    protected ?FHIRReference $location = null;
    protected ?FHIRCodeableConcept $seriousness = null;
    protected ?FHIRCodeableConcept $outcome = null;
    protected ?FHIRReference $recorder = null;
    protected ?FHIRReference $eventParticipant = null;
    protected ?FHIRString $description = null;

    /** @var FHIRAdverseEventSuspectEntity[] */
    protected array $suspectEntity = [];

    /** @var FHIRReference[] */
    protected array $subjectMedicalHistory = [];

    /** @var FHIRReference[] */
    protected array $referenceDocument = [];

    /** @var FHIRReference[] */
    protected array $study = [];

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getCategory(): ?FHIRCode
    {
        return $this->category;
    }

    public function setCategory(string|FHIRCode|null $value): self
    {
        $this->category = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReaction(): array
    {
        return $this->reaction;
    }

    public function setReaction(?FHIRReference ...$value): self
    {
        $this->reaction = array_filter($value);

        return $this;
    }

    public function addReaction(?FHIRReference ...$value): self
    {
        $this->reaction = array_filter(array_merge($this->reaction, $value));

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    public function getSeriousness(): ?FHIRCodeableConcept
    {
        return $this->seriousness;
    }

    public function setSeriousness(?FHIRCodeableConcept $value): self
    {
        $this->seriousness = $value;

        return $this;
    }

    public function getOutcome(): ?FHIRCodeableConcept
    {
        return $this->outcome;
    }

    public function setOutcome(?FHIRCodeableConcept $value): self
    {
        $this->outcome = $value;

        return $this;
    }

    public function getRecorder(): ?FHIRReference
    {
        return $this->recorder;
    }

    public function setRecorder(?FHIRReference $value): self
    {
        $this->recorder = $value;

        return $this;
    }

    public function getEventParticipant(): ?FHIRReference
    {
        return $this->eventParticipant;
    }

    public function setEventParticipant(?FHIRReference $value): self
    {
        $this->eventParticipant = $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAdverseEventSuspectEntity[]
     */
    public function getSuspectEntity(): array
    {
        return $this->suspectEntity;
    }

    public function setSuspectEntity(?FHIRAdverseEventSuspectEntity ...$value): self
    {
        $this->suspectEntity = array_filter($value);

        return $this;
    }

    public function addSuspectEntity(?FHIRAdverseEventSuspectEntity ...$value): self
    {
        $this->suspectEntity = array_filter(array_merge($this->suspectEntity, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSubjectMedicalHistory(): array
    {
        return $this->subjectMedicalHistory;
    }

    public function setSubjectMedicalHistory(?FHIRReference ...$value): self
    {
        $this->subjectMedicalHistory = array_filter($value);

        return $this;
    }

    public function addSubjectMedicalHistory(?FHIRReference ...$value): self
    {
        $this->subjectMedicalHistory = array_filter(array_merge($this->subjectMedicalHistory, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReferenceDocument(): array
    {
        return $this->referenceDocument;
    }

    public function setReferenceDocument(?FHIRReference ...$value): self
    {
        $this->referenceDocument = array_filter($value);

        return $this;
    }

    public function addReferenceDocument(?FHIRReference ...$value): self
    {
        $this->referenceDocument = array_filter(array_merge($this->referenceDocument, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getStudy(): array
    {
        return $this->study;
    }

    public function setStudy(?FHIRReference ...$value): self
    {
        $this->study = array_filter($value);

        return $this;
    }

    public function addStudy(?FHIRReference ...$value): self
    {
        $this->study = array_filter(array_merge($this->study, $value));

        return $this;
    }
}
