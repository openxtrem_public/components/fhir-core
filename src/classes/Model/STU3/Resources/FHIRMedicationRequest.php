<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationRequest Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMedicationRequestInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRDosage;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRMedicationRequestDispenseRequest;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRMedicationRequestRequester;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRMedicationRequestSubstitution;

class FHIRMedicationRequest extends FHIRDomainResource implements FHIRMedicationRequestInterface
{
    public const RESOURCE_NAME = 'MedicationRequest';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRReference[] */
    protected array $definition = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];
    protected ?FHIRIdentifier $groupIdentifier = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRCode $intent = null;
    protected ?FHIRCodeableConcept $category = null;
    protected ?FHIRCode $priority = null;
    protected FHIRCodeableConcept|FHIRReference|null $medication = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $context = null;

    /** @var FHIRReference[] */
    protected array $supportingInformation = [];
    protected ?FHIRDateTime $authoredOn = null;
    protected ?FHIRMedicationRequestRequester $requester = null;
    protected ?FHIRReference $recorder = null;

    /** @var FHIRCodeableConcept[] */
    protected array $reasonCode = [];

    /** @var FHIRReference[] */
    protected array $reasonReference = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRDosage[] */
    protected array $dosageInstruction = [];
    protected ?FHIRMedicationRequestDispenseRequest $dispenseRequest = null;
    protected ?FHIRMedicationRequestSubstitution $substitution = null;
    protected ?FHIRReference $priorPrescription = null;

    /** @var FHIRReference[] */
    protected array $detectedIssue = [];

    /** @var FHIRReference[] */
    protected array $eventHistory = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getDefinition(): array
    {
        return $this->definition;
    }

    public function setDefinition(?FHIRReference ...$value): self
    {
        $this->definition = array_filter($value);

        return $this;
    }

    public function addDefinition(?FHIRReference ...$value): self
    {
        $this->definition = array_filter(array_merge($this->definition, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    public function getGroupIdentifier(): ?FHIRIdentifier
    {
        return $this->groupIdentifier;
    }

    public function setGroupIdentifier(?FHIRIdentifier $value): self
    {
        $this->groupIdentifier = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getIntent(): ?FHIRCode
    {
        return $this->intent;
    }

    public function setIntent(string|FHIRCode|null $value): self
    {
        $this->intent = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getPriority(): ?FHIRCode
    {
        return $this->priority;
    }

    public function setPriority(string|FHIRCode|null $value): self
    {
        $this->priority = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getMedication(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->medication;
    }

    public function setMedication(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->medication = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getContext(): ?FHIRReference
    {
        return $this->context;
    }

    public function setContext(?FHIRReference $value): self
    {
        $this->context = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupportingInformation(): array
    {
        return $this->supportingInformation;
    }

    public function setSupportingInformation(?FHIRReference ...$value): self
    {
        $this->supportingInformation = array_filter($value);

        return $this;
    }

    public function addSupportingInformation(?FHIRReference ...$value): self
    {
        $this->supportingInformation = array_filter(array_merge($this->supportingInformation, $value));

        return $this;
    }

    public function getAuthoredOn(): ?FHIRDateTime
    {
        return $this->authoredOn;
    }

    public function setAuthoredOn(string|FHIRDateTime|null $value): self
    {
        $this->authoredOn = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getRequester(): ?FHIRMedicationRequestRequester
    {
        return $this->requester;
    }

    public function setRequester(?FHIRMedicationRequestRequester $value): self
    {
        $this->requester = $value;

        return $this;
    }

    public function getRecorder(): ?FHIRReference
    {
        return $this->recorder;
    }

    public function setRecorder(?FHIRReference $value): self
    {
        $this->recorder = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReasonCode(): array
    {
        return $this->reasonCode;
    }

    public function setReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter($value);

        return $this;
    }

    public function addReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter(array_merge($this->reasonCode, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReasonReference(): array
    {
        return $this->reasonReference;
    }

    public function setReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter($value);

        return $this;
    }

    public function addReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter(array_merge($this->reasonReference, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRDosage[]
     */
    public function getDosageInstruction(): array
    {
        return $this->dosageInstruction;
    }

    public function setDosageInstruction(?FHIRDosage ...$value): self
    {
        $this->dosageInstruction = array_filter($value);

        return $this;
    }

    public function addDosageInstruction(?FHIRDosage ...$value): self
    {
        $this->dosageInstruction = array_filter(array_merge($this->dosageInstruction, $value));

        return $this;
    }

    public function getDispenseRequest(): ?FHIRMedicationRequestDispenseRequest
    {
        return $this->dispenseRequest;
    }

    public function setDispenseRequest(?FHIRMedicationRequestDispenseRequest $value): self
    {
        $this->dispenseRequest = $value;

        return $this;
    }

    public function getSubstitution(): ?FHIRMedicationRequestSubstitution
    {
        return $this->substitution;
    }

    public function setSubstitution(?FHIRMedicationRequestSubstitution $value): self
    {
        $this->substitution = $value;

        return $this;
    }

    public function getPriorPrescription(): ?FHIRReference
    {
        return $this->priorPrescription;
    }

    public function setPriorPrescription(?FHIRReference $value): self
    {
        $this->priorPrescription = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getDetectedIssue(): array
    {
        return $this->detectedIssue;
    }

    public function setDetectedIssue(?FHIRReference ...$value): self
    {
        $this->detectedIssue = array_filter($value);

        return $this;
    }

    public function addDetectedIssue(?FHIRReference ...$value): self
    {
        $this->detectedIssue = array_filter(array_merge($this->detectedIssue, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEventHistory(): array
    {
        return $this->eventHistory;
    }

    public function setEventHistory(?FHIRReference ...$value): self
    {
        $this->eventHistory = array_filter($value);

        return $this;
    }

    public function addEventHistory(?FHIRReference ...$value): self
    {
        $this->eventHistory = array_filter(array_merge($this->eventHistory, $value));

        return $this;
    }
}
