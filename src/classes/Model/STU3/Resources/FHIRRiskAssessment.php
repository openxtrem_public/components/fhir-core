<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR RiskAssessment Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRRiskAssessmentInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRRiskAssessmentPrediction;

class FHIRRiskAssessment extends FHIRDomainResource implements FHIRRiskAssessmentInterface
{
    public const RESOURCE_NAME = 'RiskAssessment';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRReference $basedOn = null;
    protected ?FHIRReference $parent = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $method = null;
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $context = null;
    protected FHIRDateTime|FHIRPeriod|null $occurrence = null;
    protected ?FHIRReference $condition = null;
    protected ?FHIRReference $performer = null;
    protected FHIRCodeableConcept|FHIRReference|null $reason = null;

    /** @var FHIRReference[] */
    protected array $basis = [];

    /** @var FHIRRiskAssessmentPrediction[] */
    protected array $prediction = [];
    protected ?FHIRString $mitigation = null;
    protected ?FHIRString $comment = null;

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getBasedOn(): ?FHIRReference
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference $value): self
    {
        $this->basedOn = $value;

        return $this;
    }

    public function getParent(): ?FHIRReference
    {
        return $this->parent;
    }

    public function setParent(?FHIRReference $value): self
    {
        $this->parent = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getMethod(): ?FHIRCodeableConcept
    {
        return $this->method;
    }

    public function setMethod(?FHIRCodeableConcept $value): self
    {
        $this->method = $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getContext(): ?FHIRReference
    {
        return $this->context;
    }

    public function setContext(?FHIRReference $value): self
    {
        $this->context = $value;

        return $this;
    }

    public function getOccurrence(): FHIRDateTime|FHIRPeriod|null
    {
        return $this->occurrence;
    }

    public function setOccurrence(FHIRDateTime|FHIRPeriod|null $value): self
    {
        $this->occurrence = $value;

        return $this;
    }

    public function getCondition(): ?FHIRReference
    {
        return $this->condition;
    }

    public function setCondition(?FHIRReference $value): self
    {
        $this->condition = $value;

        return $this;
    }

    public function getPerformer(): ?FHIRReference
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRReference $value): self
    {
        $this->performer = $value;

        return $this;
    }

    public function getReason(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->reason;
    }

    public function setReason(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->reason = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasis(): array
    {
        return $this->basis;
    }

    public function setBasis(?FHIRReference ...$value): self
    {
        $this->basis = array_filter($value);

        return $this;
    }

    public function addBasis(?FHIRReference ...$value): self
    {
        $this->basis = array_filter(array_merge($this->basis, $value));

        return $this;
    }

    /**
     * @return FHIRRiskAssessmentPrediction[]
     */
    public function getPrediction(): array
    {
        return $this->prediction;
    }

    public function setPrediction(?FHIRRiskAssessmentPrediction ...$value): self
    {
        $this->prediction = array_filter($value);

        return $this;
    }

    public function addPrediction(?FHIRRiskAssessmentPrediction ...$value): self
    {
        $this->prediction = array_filter(array_merge($this->prediction, $value));

        return $this;
    }

    public function getMitigation(): ?FHIRString
    {
        return $this->mitigation;
    }

    public function setMitigation(string|FHIRString|null $value): self
    {
        $this->mitigation = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getComment(): ?FHIRString
    {
        return $this->comment;
    }

    public function setComment(string|FHIRString|null $value): self
    {
        $this->comment = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
