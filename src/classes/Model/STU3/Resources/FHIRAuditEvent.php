<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR AuditEvent Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRAuditEventInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRAuditEventAgent;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRAuditEventEntity;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRAuditEventSource;

class FHIRAuditEvent extends FHIRDomainResource implements FHIRAuditEventInterface
{
    public const RESOURCE_NAME = 'AuditEvent';

    protected ?FHIRCoding $type = null;

    /** @var FHIRCoding[] */
    protected array $subtype = [];
    protected ?FHIRCode $action = null;
    protected ?FHIRInstant $recorded = null;
    protected ?FHIRCode $outcome = null;
    protected ?FHIRString $outcomeDesc = null;

    /** @var FHIRCodeableConcept[] */
    protected array $purposeOfEvent = [];

    /** @var FHIRAuditEventAgent[] */
    protected array $agent = [];
    protected ?FHIRAuditEventSource $source = null;

    /** @var FHIRAuditEventEntity[] */
    protected array $entity = [];

    public function getType(): ?FHIRCoding
    {
        return $this->type;
    }

    public function setType(?FHIRCoding $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getSubtype(): array
    {
        return $this->subtype;
    }

    public function setSubtype(?FHIRCoding ...$value): self
    {
        $this->subtype = array_filter($value);

        return $this;
    }

    public function addSubtype(?FHIRCoding ...$value): self
    {
        $this->subtype = array_filter(array_merge($this->subtype, $value));

        return $this;
    }

    public function getAction(): ?FHIRCode
    {
        return $this->action;
    }

    public function setAction(string|FHIRCode|null $value): self
    {
        $this->action = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getRecorded(): ?FHIRInstant
    {
        return $this->recorded;
    }

    public function setRecorded(string|FHIRInstant|null $value): self
    {
        $this->recorded = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    public function getOutcome(): ?FHIRCode
    {
        return $this->outcome;
    }

    public function setOutcome(string|FHIRCode|null $value): self
    {
        $this->outcome = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getOutcomeDesc(): ?FHIRString
    {
        return $this->outcomeDesc;
    }

    public function setOutcomeDesc(string|FHIRString|null $value): self
    {
        $this->outcomeDesc = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getPurposeOfEvent(): array
    {
        return $this->purposeOfEvent;
    }

    public function setPurposeOfEvent(?FHIRCodeableConcept ...$value): self
    {
        $this->purposeOfEvent = array_filter($value);

        return $this;
    }

    public function addPurposeOfEvent(?FHIRCodeableConcept ...$value): self
    {
        $this->purposeOfEvent = array_filter(array_merge($this->purposeOfEvent, $value));

        return $this;
    }

    /**
     * @return FHIRAuditEventAgent[]
     */
    public function getAgent(): array
    {
        return $this->agent;
    }

    public function setAgent(?FHIRAuditEventAgent ...$value): self
    {
        $this->agent = array_filter($value);

        return $this;
    }

    public function addAgent(?FHIRAuditEventAgent ...$value): self
    {
        $this->agent = array_filter(array_merge($this->agent, $value));

        return $this;
    }

    public function getSource(): ?FHIRAuditEventSource
    {
        return $this->source;
    }

    public function setSource(?FHIRAuditEventSource $value): self
    {
        $this->source = $value;

        return $this;
    }

    /**
     * @return FHIRAuditEventEntity[]
     */
    public function getEntity(): array
    {
        return $this->entity;
    }

    public function setEntity(?FHIRAuditEventEntity ...$value): self
    {
        $this->entity = array_filter($value);

        return $this;
    }

    public function addEntity(?FHIRAuditEventEntity ...$value): self
    {
        $this->entity = array_filter(array_merge($this->entity, $value));

        return $this;
    }
}
