<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImagingStudy Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRImagingStudyInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIROid;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUnsignedInt;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRImagingStudySeries;

class FHIRImagingStudy extends FHIRDomainResource implements FHIRImagingStudyInterface
{
    public const RESOURCE_NAME = 'ImagingStudy';

    protected ?FHIROid $uid = null;
    protected ?FHIRIdentifier $accession = null;

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $availability = null;

    /** @var FHIRCoding[] */
    protected array $modalityList = [];
    protected ?FHIRReference $patient = null;
    protected ?FHIRReference $context = null;
    protected ?FHIRDateTime $started = null;

    /** @var FHIRReference[] */
    protected array $basedOn = [];
    protected ?FHIRReference $referrer = null;

    /** @var FHIRReference[] */
    protected array $interpreter = [];

    /** @var FHIRReference[] */
    protected array $endpoint = [];
    protected ?FHIRUnsignedInt $numberOfSeries = null;
    protected ?FHIRUnsignedInt $numberOfInstances = null;

    /** @var FHIRReference[] */
    protected array $procedureReference = [];

    /** @var FHIRCodeableConcept[] */
    protected array $procedureCode = [];
    protected ?FHIRCodeableConcept $reason = null;
    protected ?FHIRString $description = null;

    /** @var FHIRImagingStudySeries[] */
    protected array $series = [];

    public function getUid(): ?FHIROid
    {
        return $this->uid;
    }

    public function setUid(string|FHIROid|null $value): self
    {
        $this->uid = is_string($value) ? (new FHIROid())->setValue($value) : $value;

        return $this;
    }

    public function getAccession(): ?FHIRIdentifier
    {
        return $this->accession;
    }

    public function setAccession(?FHIRIdentifier $value): self
    {
        $this->accession = $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getAvailability(): ?FHIRCode
    {
        return $this->availability;
    }

    public function setAvailability(string|FHIRCode|null $value): self
    {
        $this->availability = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getModalityList(): array
    {
        return $this->modalityList;
    }

    public function setModalityList(?FHIRCoding ...$value): self
    {
        $this->modalityList = array_filter($value);

        return $this;
    }

    public function addModalityList(?FHIRCoding ...$value): self
    {
        $this->modalityList = array_filter(array_merge($this->modalityList, $value));

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getContext(): ?FHIRReference
    {
        return $this->context;
    }

    public function setContext(?FHIRReference $value): self
    {
        $this->context = $value;

        return $this;
    }

    public function getStarted(): ?FHIRDateTime
    {
        return $this->started;
    }

    public function setStarted(string|FHIRDateTime|null $value): self
    {
        $this->started = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    public function getReferrer(): ?FHIRReference
    {
        return $this->referrer;
    }

    public function setReferrer(?FHIRReference $value): self
    {
        $this->referrer = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getInterpreter(): array
    {
        return $this->interpreter;
    }

    public function setInterpreter(?FHIRReference ...$value): self
    {
        $this->interpreter = array_filter($value);

        return $this;
    }

    public function addInterpreter(?FHIRReference ...$value): self
    {
        $this->interpreter = array_filter(array_merge($this->interpreter, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEndpoint(): array
    {
        return $this->endpoint;
    }

    public function setEndpoint(?FHIRReference ...$value): self
    {
        $this->endpoint = array_filter($value);

        return $this;
    }

    public function addEndpoint(?FHIRReference ...$value): self
    {
        $this->endpoint = array_filter(array_merge($this->endpoint, $value));

        return $this;
    }

    public function getNumberOfSeries(): ?FHIRUnsignedInt
    {
        return $this->numberOfSeries;
    }

    public function setNumberOfSeries(int|FHIRUnsignedInt|null $value): self
    {
        $this->numberOfSeries = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    public function getNumberOfInstances(): ?FHIRUnsignedInt
    {
        return $this->numberOfInstances;
    }

    public function setNumberOfInstances(int|FHIRUnsignedInt|null $value): self
    {
        $this->numberOfInstances = is_int($value) ? (new FHIRUnsignedInt())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getProcedureReference(): array
    {
        return $this->procedureReference;
    }

    public function setProcedureReference(?FHIRReference ...$value): self
    {
        $this->procedureReference = array_filter($value);

        return $this;
    }

    public function addProcedureReference(?FHIRReference ...$value): self
    {
        $this->procedureReference = array_filter(array_merge($this->procedureReference, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getProcedureCode(): array
    {
        return $this->procedureCode;
    }

    public function setProcedureCode(?FHIRCodeableConcept ...$value): self
    {
        $this->procedureCode = array_filter($value);

        return $this;
    }

    public function addProcedureCode(?FHIRCodeableConcept ...$value): self
    {
        $this->procedureCode = array_filter(array_merge($this->procedureCode, $value));

        return $this;
    }

    public function getReason(): ?FHIRCodeableConcept
    {
        return $this->reason;
    }

    public function setReason(?FHIRCodeableConcept $value): self
    {
        $this->reason = $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRImagingStudySeries[]
     */
    public function getSeries(): array
    {
        return $this->series;
    }

    public function setSeries(?FHIRImagingStudySeries ...$value): self
    {
        $this->series = array_filter($value);

        return $this;
    }

    public function addSeries(?FHIRImagingStudySeries ...$value): self
    {
        $this->series = array_filter(array_merge($this->series, $value));

        return $this;
    }
}
