<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Sequence Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRSequenceInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRInteger;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRSequenceQuality;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRSequenceReferenceSeq;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRSequenceRepository;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRSequenceVariant;

class FHIRSequence extends FHIRDomainResource implements FHIRSequenceInterface
{
    public const RESOURCE_NAME = 'Sequence';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $type = null;
    protected ?FHIRInteger $coordinateSystem = null;
    protected ?FHIRReference $patient = null;
    protected ?FHIRReference $specimen = null;
    protected ?FHIRReference $device = null;
    protected ?FHIRReference $performer = null;
    protected ?FHIRQuantity $quantity = null;
    protected ?FHIRSequenceReferenceSeq $referenceSeq = null;

    /** @var FHIRSequenceVariant[] */
    protected array $variant = [];
    protected ?FHIRString $observedSeq = null;

    /** @var FHIRSequenceQuality[] */
    protected array $quality = [];
    protected ?FHIRInteger $readCoverage = null;

    /** @var FHIRSequenceRepository[] */
    protected array $repository = [];

    /** @var FHIRReference[] */
    protected array $pointer = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getType(): ?FHIRCode
    {
        return $this->type;
    }

    public function setType(string|FHIRCode|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCoordinateSystem(): ?FHIRInteger
    {
        return $this->coordinateSystem;
    }

    public function setCoordinateSystem(int|FHIRInteger|null $value): self
    {
        $this->coordinateSystem = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getSpecimen(): ?FHIRReference
    {
        return $this->specimen;
    }

    public function setSpecimen(?FHIRReference $value): self
    {
        $this->specimen = $value;

        return $this;
    }

    public function getDevice(): ?FHIRReference
    {
        return $this->device;
    }

    public function setDevice(?FHIRReference $value): self
    {
        $this->device = $value;

        return $this;
    }

    public function getPerformer(): ?FHIRReference
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRReference $value): self
    {
        $this->performer = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    public function getReferenceSeq(): ?FHIRSequenceReferenceSeq
    {
        return $this->referenceSeq;
    }

    public function setReferenceSeq(?FHIRSequenceReferenceSeq $value): self
    {
        $this->referenceSeq = $value;

        return $this;
    }

    /**
     * @return FHIRSequenceVariant[]
     */
    public function getVariant(): array
    {
        return $this->variant;
    }

    public function setVariant(?FHIRSequenceVariant ...$value): self
    {
        $this->variant = array_filter($value);

        return $this;
    }

    public function addVariant(?FHIRSequenceVariant ...$value): self
    {
        $this->variant = array_filter(array_merge($this->variant, $value));

        return $this;
    }

    public function getObservedSeq(): ?FHIRString
    {
        return $this->observedSeq;
    }

    public function setObservedSeq(string|FHIRString|null $value): self
    {
        $this->observedSeq = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRSequenceQuality[]
     */
    public function getQuality(): array
    {
        return $this->quality;
    }

    public function setQuality(?FHIRSequenceQuality ...$value): self
    {
        $this->quality = array_filter($value);

        return $this;
    }

    public function addQuality(?FHIRSequenceQuality ...$value): self
    {
        $this->quality = array_filter(array_merge($this->quality, $value));

        return $this;
    }

    public function getReadCoverage(): ?FHIRInteger
    {
        return $this->readCoverage;
    }

    public function setReadCoverage(int|FHIRInteger|null $value): self
    {
        $this->readCoverage = is_int($value) ? (new FHIRInteger())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRSequenceRepository[]
     */
    public function getRepository(): array
    {
        return $this->repository;
    }

    public function setRepository(?FHIRSequenceRepository ...$value): self
    {
        $this->repository = array_filter($value);

        return $this;
    }

    public function addRepository(?FHIRSequenceRepository ...$value): self
    {
        $this->repository = array_filter(array_merge($this->repository, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPointer(): array
    {
        return $this->pointer;
    }

    public function setPointer(?FHIRReference ...$value): self
    {
        $this->pointer = array_filter($value);

        return $this;
    }

    public function addPointer(?FHIRReference ...$value): self
    {
        $this->pointer = array_filter(array_merge($this->pointer, $value));

        return $this;
    }
}
