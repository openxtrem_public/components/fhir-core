<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ReferralRequest Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRReferralRequestInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRReferralRequestRequester;

class FHIRReferralRequest extends FHIRDomainResource implements FHIRReferralRequestInterface
{
    public const RESOURCE_NAME = 'ReferralRequest';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRReference[] */
    protected array $definition = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];

    /** @var FHIRReference[] */
    protected array $replaces = [];
    protected ?FHIRIdentifier $groupIdentifier = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRCode $intent = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCode $priority = null;

    /** @var FHIRCodeableConcept[] */
    protected array $serviceRequested = [];
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $context = null;
    protected FHIRDateTime|FHIRPeriod|null $occurrence = null;
    protected ?FHIRDateTime $authoredOn = null;
    protected ?FHIRReferralRequestRequester $requester = null;
    protected ?FHIRCodeableConcept $specialty = null;

    /** @var FHIRReference[] */
    protected array $recipient = [];

    /** @var FHIRCodeableConcept[] */
    protected array $reasonCode = [];

    /** @var FHIRReference[] */
    protected array $reasonReference = [];
    protected ?FHIRString $description = null;

    /** @var FHIRReference[] */
    protected array $supportingInfo = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRReference[] */
    protected array $relevantHistory = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getDefinition(): array
    {
        return $this->definition;
    }

    public function setDefinition(?FHIRReference ...$value): self
    {
        $this->definition = array_filter($value);

        return $this;
    }

    public function addDefinition(?FHIRReference ...$value): self
    {
        $this->definition = array_filter(array_merge($this->definition, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReplaces(): array
    {
        return $this->replaces;
    }

    public function setReplaces(?FHIRReference ...$value): self
    {
        $this->replaces = array_filter($value);

        return $this;
    }

    public function addReplaces(?FHIRReference ...$value): self
    {
        $this->replaces = array_filter(array_merge($this->replaces, $value));

        return $this;
    }

    public function getGroupIdentifier(): ?FHIRIdentifier
    {
        return $this->groupIdentifier;
    }

    public function setGroupIdentifier(?FHIRIdentifier $value): self
    {
        $this->groupIdentifier = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getIntent(): ?FHIRCode
    {
        return $this->intent;
    }

    public function setIntent(string|FHIRCode|null $value): self
    {
        $this->intent = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getPriority(): ?FHIRCode
    {
        return $this->priority;
    }

    public function setPriority(string|FHIRCode|null $value): self
    {
        $this->priority = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getServiceRequested(): array
    {
        return $this->serviceRequested;
    }

    public function setServiceRequested(?FHIRCodeableConcept ...$value): self
    {
        $this->serviceRequested = array_filter($value);

        return $this;
    }

    public function addServiceRequested(?FHIRCodeableConcept ...$value): self
    {
        $this->serviceRequested = array_filter(array_merge($this->serviceRequested, $value));

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getContext(): ?FHIRReference
    {
        return $this->context;
    }

    public function setContext(?FHIRReference $value): self
    {
        $this->context = $value;

        return $this;
    }

    public function getOccurrence(): FHIRDateTime|FHIRPeriod|null
    {
        return $this->occurrence;
    }

    public function setOccurrence(FHIRDateTime|FHIRPeriod|null $value): self
    {
        $this->occurrence = $value;

        return $this;
    }

    public function getAuthoredOn(): ?FHIRDateTime
    {
        return $this->authoredOn;
    }

    public function setAuthoredOn(string|FHIRDateTime|null $value): self
    {
        $this->authoredOn = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getRequester(): ?FHIRReferralRequestRequester
    {
        return $this->requester;
    }

    public function setRequester(?FHIRReferralRequestRequester $value): self
    {
        $this->requester = $value;

        return $this;
    }

    public function getSpecialty(): ?FHIRCodeableConcept
    {
        return $this->specialty;
    }

    public function setSpecialty(?FHIRCodeableConcept $value): self
    {
        $this->specialty = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getRecipient(): array
    {
        return $this->recipient;
    }

    public function setRecipient(?FHIRReference ...$value): self
    {
        $this->recipient = array_filter($value);

        return $this;
    }

    public function addRecipient(?FHIRReference ...$value): self
    {
        $this->recipient = array_filter(array_merge($this->recipient, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReasonCode(): array
    {
        return $this->reasonCode;
    }

    public function setReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter($value);

        return $this;
    }

    public function addReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter(array_merge($this->reasonCode, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReasonReference(): array
    {
        return $this->reasonReference;
    }

    public function setReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter($value);

        return $this;
    }

    public function addReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter(array_merge($this->reasonReference, $value));

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupportingInfo(): array
    {
        return $this->supportingInfo;
    }

    public function setSupportingInfo(?FHIRReference ...$value): self
    {
        $this->supportingInfo = array_filter($value);

        return $this;
    }

    public function addSupportingInfo(?FHIRReference ...$value): self
    {
        $this->supportingInfo = array_filter(array_merge($this->supportingInfo, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getRelevantHistory(): array
    {
        return $this->relevantHistory;
    }

    public function setRelevantHistory(?FHIRReference ...$value): self
    {
        $this->relevantHistory = array_filter($value);

        return $this;
    }

    public function addRelevantHistory(?FHIRReference ...$value): self
    {
        $this->relevantHistory = array_filter(array_merge($this->relevantHistory, $value));

        return $this;
    }
}
