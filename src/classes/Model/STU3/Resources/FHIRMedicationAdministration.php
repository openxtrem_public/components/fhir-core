<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MedicationAdministration Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRMedicationAdministrationInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRMedicationAdministrationDosage;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRMedicationAdministrationPerformer;

class FHIRMedicationAdministration extends FHIRDomainResource implements FHIRMedicationAdministrationInterface
{
    public const RESOURCE_NAME = 'MedicationAdministration';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRReference[] */
    protected array $definition = [];

    /** @var FHIRReference[] */
    protected array $partOf = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $category = null;
    protected FHIRCodeableConcept|FHIRReference|null $medication = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $context = null;

    /** @var FHIRReference[] */
    protected array $supportingInformation = [];
    protected FHIRDateTime|FHIRPeriod|null $effective = null;

    /** @var FHIRMedicationAdministrationPerformer[] */
    protected array $performer = [];
    protected ?FHIRBoolean $notGiven = null;

    /** @var FHIRCodeableConcept[] */
    protected array $reasonNotGiven = [];

    /** @var FHIRCodeableConcept[] */
    protected array $reasonCode = [];

    /** @var FHIRReference[] */
    protected array $reasonReference = [];
    protected ?FHIRReference $prescription = null;

    /** @var FHIRReference[] */
    protected array $device = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];
    protected ?FHIRMedicationAdministrationDosage $dosage = null;

    /** @var FHIRReference[] */
    protected array $eventHistory = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getDefinition(): array
    {
        return $this->definition;
    }

    public function setDefinition(?FHIRReference ...$value): self
    {
        $this->definition = array_filter($value);

        return $this;
    }

    public function addDefinition(?FHIRReference ...$value): self
    {
        $this->definition = array_filter(array_merge($this->definition, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPartOf(): array
    {
        return $this->partOf;
    }

    public function setPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter($value);

        return $this;
    }

    public function addPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter(array_merge($this->partOf, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    public function getMedication(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->medication;
    }

    public function setMedication(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->medication = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getContext(): ?FHIRReference
    {
        return $this->context;
    }

    public function setContext(?FHIRReference $value): self
    {
        $this->context = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupportingInformation(): array
    {
        return $this->supportingInformation;
    }

    public function setSupportingInformation(?FHIRReference ...$value): self
    {
        $this->supportingInformation = array_filter($value);

        return $this;
    }

    public function addSupportingInformation(?FHIRReference ...$value): self
    {
        $this->supportingInformation = array_filter(array_merge($this->supportingInformation, $value));

        return $this;
    }

    public function getEffective(): FHIRDateTime|FHIRPeriod|null
    {
        return $this->effective;
    }

    public function setEffective(FHIRDateTime|FHIRPeriod|null $value): self
    {
        $this->effective = $value;

        return $this;
    }

    /**
     * @return FHIRMedicationAdministrationPerformer[]
     */
    public function getPerformer(): array
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRMedicationAdministrationPerformer ...$value): self
    {
        $this->performer = array_filter($value);

        return $this;
    }

    public function addPerformer(?FHIRMedicationAdministrationPerformer ...$value): self
    {
        $this->performer = array_filter(array_merge($this->performer, $value));

        return $this;
    }

    public function getNotGiven(): ?FHIRBoolean
    {
        return $this->notGiven;
    }

    public function setNotGiven(bool|FHIRBoolean|null $value): self
    {
        $this->notGiven = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReasonNotGiven(): array
    {
        return $this->reasonNotGiven;
    }

    public function setReasonNotGiven(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonNotGiven = array_filter($value);

        return $this;
    }

    public function addReasonNotGiven(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonNotGiven = array_filter(array_merge($this->reasonNotGiven, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReasonCode(): array
    {
        return $this->reasonCode;
    }

    public function setReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter($value);

        return $this;
    }

    public function addReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter(array_merge($this->reasonCode, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReasonReference(): array
    {
        return $this->reasonReference;
    }

    public function setReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter($value);

        return $this;
    }

    public function addReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter(array_merge($this->reasonReference, $value));

        return $this;
    }

    public function getPrescription(): ?FHIRReference
    {
        return $this->prescription;
    }

    public function setPrescription(?FHIRReference $value): self
    {
        $this->prescription = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getDevice(): array
    {
        return $this->device;
    }

    public function setDevice(?FHIRReference ...$value): self
    {
        $this->device = array_filter($value);

        return $this;
    }

    public function addDevice(?FHIRReference ...$value): self
    {
        $this->device = array_filter(array_merge($this->device, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    public function getDosage(): ?FHIRMedicationAdministrationDosage
    {
        return $this->dosage;
    }

    public function setDosage(?FHIRMedicationAdministrationDosage $value): self
    {
        $this->dosage = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEventHistory(): array
    {
        return $this->eventHistory;
    }

    public function setEventHistory(?FHIRReference ...$value): self
    {
        $this->eventHistory = array_filter($value);

        return $this;
    }

    public function addEventHistory(?FHIRReference ...$value): self
    {
        $this->eventHistory = array_filter(array_merge($this->eventHistory, $value));

        return $this;
    }
}
