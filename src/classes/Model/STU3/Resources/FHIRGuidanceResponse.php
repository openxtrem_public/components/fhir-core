<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR GuidanceResponse Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRGuidanceResponseInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRDataRequirement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRId;

class FHIRGuidanceResponse extends FHIRDomainResource implements FHIRGuidanceResponseInterface
{
    public const RESOURCE_NAME = 'GuidanceResponse';

    protected ?FHIRId $requestId = null;
    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRReference $module = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $context = null;
    protected ?FHIRDateTime $occurrenceDateTime = null;
    protected ?FHIRReference $performer = null;
    protected FHIRCodeableConcept|FHIRReference|null $reason = null;

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /** @var FHIRReference[] */
    protected array $evaluationMessage = [];
    protected ?FHIRReference $outputParameters = null;
    protected ?FHIRReference $result = null;

    /** @var FHIRDataRequirement[] */
    protected array $dataRequirement = [];

    public function getRequestId(): ?FHIRId
    {
        return $this->requestId;
    }

    public function setRequestId(string|FHIRId|null $value): self
    {
        $this->requestId = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getModule(): ?FHIRReference
    {
        return $this->module;
    }

    public function setModule(?FHIRReference $value): self
    {
        $this->module = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getContext(): ?FHIRReference
    {
        return $this->context;
    }

    public function setContext(?FHIRReference $value): self
    {
        $this->context = $value;

        return $this;
    }

    public function getOccurrenceDateTime(): ?FHIRDateTime
    {
        return $this->occurrenceDateTime;
    }

    public function setOccurrenceDateTime(string|FHIRDateTime|null $value): self
    {
        $this->occurrenceDateTime = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPerformer(): ?FHIRReference
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRReference $value): self
    {
        $this->performer = $value;

        return $this;
    }

    public function getReason(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->reason;
    }

    public function setReason(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->reason = $value;

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEvaluationMessage(): array
    {
        return $this->evaluationMessage;
    }

    public function setEvaluationMessage(?FHIRReference ...$value): self
    {
        $this->evaluationMessage = array_filter($value);

        return $this;
    }

    public function addEvaluationMessage(?FHIRReference ...$value): self
    {
        $this->evaluationMessage = array_filter(array_merge($this->evaluationMessage, $value));

        return $this;
    }

    public function getOutputParameters(): ?FHIRReference
    {
        return $this->outputParameters;
    }

    public function setOutputParameters(?FHIRReference $value): self
    {
        $this->outputParameters = $value;

        return $this;
    }

    public function getResult(): ?FHIRReference
    {
        return $this->result;
    }

    public function setResult(?FHIRReference $value): self
    {
        $this->result = $value;

        return $this;
    }

    /**
     * @return FHIRDataRequirement[]
     */
    public function getDataRequirement(): array
    {
        return $this->dataRequirement;
    }

    public function setDataRequirement(?FHIRDataRequirement ...$value): self
    {
        $this->dataRequirement = array_filter($value);

        return $this;
    }

    public function addDataRequirement(?FHIRDataRequirement ...$value): self
    {
        $this->dataRequirement = array_filter(array_merge($this->dataRequirement, $value));

        return $this;
    }
}
