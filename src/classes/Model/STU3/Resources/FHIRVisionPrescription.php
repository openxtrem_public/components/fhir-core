<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR VisionPrescription Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRVisionPrescriptionInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRVisionPrescriptionDispense;

class FHIRVisionPrescription extends FHIRDomainResource implements FHIRVisionPrescriptionInterface
{
    public const RESOURCE_NAME = 'VisionPrescription';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRReference $patient = null;
    protected ?FHIRReference $encounter = null;
    protected ?FHIRDateTime $dateWritten = null;
    protected ?FHIRReference $prescriber = null;
    protected FHIRCodeableConcept|FHIRReference|null $reason = null;

    /** @var FHIRVisionPrescriptionDispense[] */
    protected array $dispense = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getDateWritten(): ?FHIRDateTime
    {
        return $this->dateWritten;
    }

    public function setDateWritten(string|FHIRDateTime|null $value): self
    {
        $this->dateWritten = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPrescriber(): ?FHIRReference
    {
        return $this->prescriber;
    }

    public function setPrescriber(?FHIRReference $value): self
    {
        $this->prescriber = $value;

        return $this;
    }

    public function getReason(): FHIRCodeableConcept|FHIRReference|null
    {
        return $this->reason;
    }

    public function setReason(FHIRCodeableConcept|FHIRReference|null $value): self
    {
        $this->reason = $value;

        return $this;
    }

    /**
     * @return FHIRVisionPrescriptionDispense[]
     */
    public function getDispense(): array
    {
        return $this->dispense;
    }

    public function setDispense(?FHIRVisionPrescriptionDispense ...$value): self
    {
        $this->dispense = array_filter($value);

        return $this;
    }

    public function addDispense(?FHIRVisionPrescriptionDispense ...$value): self
    {
        $this->dispense = array_filter(array_merge($this->dispense, $value));

        return $this;
    }
}
