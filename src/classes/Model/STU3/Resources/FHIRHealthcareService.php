<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR HealthcareService Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRHealthcareServiceInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRHealthcareServiceAvailableTime;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRHealthcareServiceNotAvailable;

class FHIRHealthcareService extends FHIRDomainResource implements FHIRHealthcareServiceInterface
{
    public const RESOURCE_NAME = 'HealthcareService';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRBoolean $active = null;
    protected ?FHIRReference $providedBy = null;
    protected ?FHIRCodeableConcept $category = null;

    /** @var FHIRCodeableConcept[] */
    protected array $type = [];

    /** @var FHIRCodeableConcept[] */
    protected array $specialty = [];

    /** @var FHIRReference[] */
    protected array $location = [];
    protected ?FHIRString $name = null;
    protected ?FHIRString $comment = null;
    protected ?FHIRString $extraDetails = null;
    protected ?FHIRAttachment $photo = null;

    /** @var FHIRContactPoint[] */
    protected array $telecom = [];

    /** @var FHIRReference[] */
    protected array $coverageArea = [];

    /** @var FHIRCodeableConcept[] */
    protected array $serviceProvisionCode = [];
    protected ?FHIRCodeableConcept $eligibility = null;
    protected ?FHIRString $eligibilityNote = null;

    /** @var FHIRString[] */
    protected array $programName = [];

    /** @var FHIRCodeableConcept[] */
    protected array $characteristic = [];

    /** @var FHIRCodeableConcept[] */
    protected array $referralMethod = [];
    protected ?FHIRBoolean $appointmentRequired = null;

    /** @var FHIRHealthcareServiceAvailableTime[] */
    protected array $availableTime = [];

    /** @var FHIRHealthcareServiceNotAvailable[] */
    protected array $notAvailable = [];
    protected ?FHIRString $availabilityExceptions = null;

    /** @var FHIRReference[] */
    protected array $endpoint = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getActive(): ?FHIRBoolean
    {
        return $this->active;
    }

    public function setActive(bool|FHIRBoolean|null $value): self
    {
        $this->active = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getProvidedBy(): ?FHIRReference
    {
        return $this->providedBy;
    }

    public function setProvidedBy(?FHIRReference $value): self
    {
        $this->providedBy = $value;

        return $this;
    }

    public function getCategory(): ?FHIRCodeableConcept
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept $value): self
    {
        $this->category = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter($value);

        return $this;
    }

    public function addType(?FHIRCodeableConcept ...$value): self
    {
        $this->type = array_filter(array_merge($this->type, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSpecialty(): array
    {
        return $this->specialty;
    }

    public function setSpecialty(?FHIRCodeableConcept ...$value): self
    {
        $this->specialty = array_filter($value);

        return $this;
    }

    public function addSpecialty(?FHIRCodeableConcept ...$value): self
    {
        $this->specialty = array_filter(array_merge($this->specialty, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getLocation(): array
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference ...$value): self
    {
        $this->location = array_filter($value);

        return $this;
    }

    public function addLocation(?FHIRReference ...$value): self
    {
        $this->location = array_filter(array_merge($this->location, $value));

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getComment(): ?FHIRString
    {
        return $this->comment;
    }

    public function setComment(string|FHIRString|null $value): self
    {
        $this->comment = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getExtraDetails(): ?FHIRString
    {
        return $this->extraDetails;
    }

    public function setExtraDetails(string|FHIRString|null $value): self
    {
        $this->extraDetails = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getPhoto(): ?FHIRAttachment
    {
        return $this->photo;
    }

    public function setPhoto(?FHIRAttachment $value): self
    {
        $this->photo = $value;

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getTelecom(): array
    {
        return $this->telecom;
    }

    public function setTelecom(?FHIRContactPoint ...$value): self
    {
        $this->telecom = array_filter($value);

        return $this;
    }

    public function addTelecom(?FHIRContactPoint ...$value): self
    {
        $this->telecom = array_filter(array_merge($this->telecom, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getCoverageArea(): array
    {
        return $this->coverageArea;
    }

    public function setCoverageArea(?FHIRReference ...$value): self
    {
        $this->coverageArea = array_filter($value);

        return $this;
    }

    public function addCoverageArea(?FHIRReference ...$value): self
    {
        $this->coverageArea = array_filter(array_merge($this->coverageArea, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getServiceProvisionCode(): array
    {
        return $this->serviceProvisionCode;
    }

    public function setServiceProvisionCode(?FHIRCodeableConcept ...$value): self
    {
        $this->serviceProvisionCode = array_filter($value);

        return $this;
    }

    public function addServiceProvisionCode(?FHIRCodeableConcept ...$value): self
    {
        $this->serviceProvisionCode = array_filter(array_merge($this->serviceProvisionCode, $value));

        return $this;
    }

    public function getEligibility(): ?FHIRCodeableConcept
    {
        return $this->eligibility;
    }

    public function setEligibility(?FHIRCodeableConcept $value): self
    {
        $this->eligibility = $value;

        return $this;
    }

    public function getEligibilityNote(): ?FHIRString
    {
        return $this->eligibilityNote;
    }

    public function setEligibilityNote(string|FHIRString|null $value): self
    {
        $this->eligibilityNote = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getProgramName(): array
    {
        return $this->programName;
    }

    public function setProgramName(string|FHIRString|null ...$value): self
    {
        $this->programName = [];
        $this->addProgramName(...$value);

        return $this;
    }

    public function addProgramName(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->programName = array_filter(array_merge($this->programName, $values));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCharacteristic(): array
    {
        return $this->characteristic;
    }

    public function setCharacteristic(?FHIRCodeableConcept ...$value): self
    {
        $this->characteristic = array_filter($value);

        return $this;
    }

    public function addCharacteristic(?FHIRCodeableConcept ...$value): self
    {
        $this->characteristic = array_filter(array_merge($this->characteristic, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReferralMethod(): array
    {
        return $this->referralMethod;
    }

    public function setReferralMethod(?FHIRCodeableConcept ...$value): self
    {
        $this->referralMethod = array_filter($value);

        return $this;
    }

    public function addReferralMethod(?FHIRCodeableConcept ...$value): self
    {
        $this->referralMethod = array_filter(array_merge($this->referralMethod, $value));

        return $this;
    }

    public function getAppointmentRequired(): ?FHIRBoolean
    {
        return $this->appointmentRequired;
    }

    public function setAppointmentRequired(bool|FHIRBoolean|null $value): self
    {
        $this->appointmentRequired = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRHealthcareServiceAvailableTime[]
     */
    public function getAvailableTime(): array
    {
        return $this->availableTime;
    }

    public function setAvailableTime(?FHIRHealthcareServiceAvailableTime ...$value): self
    {
        $this->availableTime = array_filter($value);

        return $this;
    }

    public function addAvailableTime(?FHIRHealthcareServiceAvailableTime ...$value): self
    {
        $this->availableTime = array_filter(array_merge($this->availableTime, $value));

        return $this;
    }

    /**
     * @return FHIRHealthcareServiceNotAvailable[]
     */
    public function getNotAvailable(): array
    {
        return $this->notAvailable;
    }

    public function setNotAvailable(?FHIRHealthcareServiceNotAvailable ...$value): self
    {
        $this->notAvailable = array_filter($value);

        return $this;
    }

    public function addNotAvailable(?FHIRHealthcareServiceNotAvailable ...$value): self
    {
        $this->notAvailable = array_filter(array_merge($this->notAvailable, $value));

        return $this;
    }

    public function getAvailabilityExceptions(): ?FHIRString
    {
        return $this->availabilityExceptions;
    }

    public function setAvailabilityExceptions(string|FHIRString|null $value): self
    {
        $this->availabilityExceptions = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEndpoint(): array
    {
        return $this->endpoint;
    }

    public function setEndpoint(?FHIRReference ...$value): self
    {
        $this->endpoint = array_filter($value);

        return $this;
    }

    public function addEndpoint(?FHIRReference ...$value): self
    {
        $this->endpoint = array_filter(array_merge($this->endpoint, $value));

        return $this;
    }
}
