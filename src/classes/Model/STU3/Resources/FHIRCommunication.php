<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Communication Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRCommunicationInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRCommunicationPayload;

class FHIRCommunication extends FHIRDomainResource implements FHIRCommunicationInterface
{
    public const RESOURCE_NAME = 'Communication';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRReference[] */
    protected array $definition = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];

    /** @var FHIRReference[] */
    protected array $partOf = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRBoolean $notDone = null;
    protected ?FHIRCodeableConcept $notDoneReason = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];

    /** @var FHIRCodeableConcept[] */
    protected array $medium = [];
    protected ?FHIRReference $subject = null;

    /** @var FHIRReference[] */
    protected array $recipient = [];

    /** @var FHIRReference[] */
    protected array $topic = [];
    protected ?FHIRReference $context = null;
    protected ?FHIRDateTime $sent = null;
    protected ?FHIRDateTime $received = null;
    protected ?FHIRReference $sender = null;

    /** @var FHIRCodeableConcept[] */
    protected array $reasonCode = [];

    /** @var FHIRReference[] */
    protected array $reasonReference = [];

    /** @var FHIRCommunicationPayload[] */
    protected array $payload = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getDefinition(): array
    {
        return $this->definition;
    }

    public function setDefinition(?FHIRReference ...$value): self
    {
        $this->definition = array_filter($value);

        return $this;
    }

    public function addDefinition(?FHIRReference ...$value): self
    {
        $this->definition = array_filter(array_merge($this->definition, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPartOf(): array
    {
        return $this->partOf;
    }

    public function setPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter($value);

        return $this;
    }

    public function addPartOf(?FHIRReference ...$value): self
    {
        $this->partOf = array_filter(array_merge($this->partOf, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getNotDone(): ?FHIRBoolean
    {
        return $this->notDone;
    }

    public function setNotDone(bool|FHIRBoolean|null $value): self
    {
        $this->notDone = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getNotDoneReason(): ?FHIRCodeableConcept
    {
        return $this->notDoneReason;
    }

    public function setNotDoneReason(?FHIRCodeableConcept $value): self
    {
        $this->notDoneReason = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getMedium(): array
    {
        return $this->medium;
    }

    public function setMedium(?FHIRCodeableConcept ...$value): self
    {
        $this->medium = array_filter($value);

        return $this;
    }

    public function addMedium(?FHIRCodeableConcept ...$value): self
    {
        $this->medium = array_filter(array_merge($this->medium, $value));

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getRecipient(): array
    {
        return $this->recipient;
    }

    public function setRecipient(?FHIRReference ...$value): self
    {
        $this->recipient = array_filter($value);

        return $this;
    }

    public function addRecipient(?FHIRReference ...$value): self
    {
        $this->recipient = array_filter(array_merge($this->recipient, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getTopic(): array
    {
        return $this->topic;
    }

    public function setTopic(?FHIRReference ...$value): self
    {
        $this->topic = array_filter($value);

        return $this;
    }

    public function addTopic(?FHIRReference ...$value): self
    {
        $this->topic = array_filter(array_merge($this->topic, $value));

        return $this;
    }

    public function getContext(): ?FHIRReference
    {
        return $this->context;
    }

    public function setContext(?FHIRReference $value): self
    {
        $this->context = $value;

        return $this;
    }

    public function getSent(): ?FHIRDateTime
    {
        return $this->sent;
    }

    public function setSent(string|FHIRDateTime|null $value): self
    {
        $this->sent = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getReceived(): ?FHIRDateTime
    {
        return $this->received;
    }

    public function setReceived(string|FHIRDateTime|null $value): self
    {
        $this->received = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getSender(): ?FHIRReference
    {
        return $this->sender;
    }

    public function setSender(?FHIRReference $value): self
    {
        $this->sender = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReasonCode(): array
    {
        return $this->reasonCode;
    }

    public function setReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter($value);

        return $this;
    }

    public function addReasonCode(?FHIRCodeableConcept ...$value): self
    {
        $this->reasonCode = array_filter(array_merge($this->reasonCode, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReasonReference(): array
    {
        return $this->reasonReference;
    }

    public function setReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter($value);

        return $this;
    }

    public function addReasonReference(?FHIRReference ...$value): self
    {
        $this->reasonReference = array_filter(array_merge($this->reasonReference, $value));

        return $this;
    }

    /**
     * @return FHIRCommunicationPayload[]
     */
    public function getPayload(): array
    {
        return $this->payload;
    }

    public function setPayload(?FHIRCommunicationPayload ...$value): self
    {
        $this->payload = array_filter($value);

        return $this;
    }

    public function addPayload(?FHIRCommunicationPayload ...$value): self
    {
        $this->payload = array_filter(array_merge($this->payload, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }
}
