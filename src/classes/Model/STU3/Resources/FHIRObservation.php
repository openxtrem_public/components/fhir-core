<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Observation Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRObservationInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRObservationComponent;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRObservationReferenceRange;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRObservationRelated;

class FHIRObservation extends FHIRDomainResource implements FHIRObservationInterface
{
    public const RESOURCE_NAME = 'Observation';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];

    /** @var FHIRReference[] */
    protected array $basedOn = [];
    protected ?FHIRCode $status = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $context = null;
    protected FHIRDateTime|FHIRPeriod|null $effective = null;
    protected ?FHIRInstant $issued = null;

    /** @var FHIRReference[] */
    protected array $performer = [];
    protected ?FHIRElement $value = null;
    protected ?FHIRCodeableConcept $dataAbsentReason = null;
    protected ?FHIRCodeableConcept $interpretation = null;
    protected ?FHIRString $comment = null;
    protected ?FHIRCodeableConcept $bodySite = null;
    protected ?FHIRCodeableConcept $method = null;
    protected ?FHIRReference $specimen = null;
    protected ?FHIRReference $device = null;

    /** @var FHIRObservationReferenceRange[] */
    protected array $referenceRange = [];

    /** @var FHIRObservationRelated[] */
    protected array $related = [];

    /** @var FHIRObservationComponent[] */
    protected array $component = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->basedOn;
    }

    public function setBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter($value);

        return $this;
    }

    public function addBasedOn(?FHIRReference ...$value): self
    {
        $this->basedOn = array_filter(array_merge($this->basedOn, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getContext(): ?FHIRReference
    {
        return $this->context;
    }

    public function setContext(?FHIRReference $value): self
    {
        $this->context = $value;

        return $this;
    }

    public function getEffective(): FHIRDateTime|FHIRPeriod|null
    {
        return $this->effective;
    }

    public function setEffective(FHIRDateTime|FHIRPeriod|null $value): self
    {
        $this->effective = $value;

        return $this;
    }

    public function getIssued(): ?FHIRInstant
    {
        return $this->issued;
    }

    public function setIssued(string|FHIRInstant|null $value): self
    {
        $this->issued = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPerformer(): array
    {
        return $this->performer;
    }

    public function setPerformer(?FHIRReference ...$value): self
    {
        $this->performer = array_filter($value);

        return $this;
    }

    public function addPerformer(?FHIRReference ...$value): self
    {
        $this->performer = array_filter(array_merge($this->performer, $value));

        return $this;
    }

    public function getValue(): ?FHIRElement
    {
        return $this->value;
    }

    public function setValue(?FHIRElement $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getDataAbsentReason(): ?FHIRCodeableConcept
    {
        return $this->dataAbsentReason;
    }

    public function setDataAbsentReason(?FHIRCodeableConcept $value): self
    {
        $this->dataAbsentReason = $value;

        return $this;
    }

    public function getInterpretation(): ?FHIRCodeableConcept
    {
        return $this->interpretation;
    }

    public function setInterpretation(?FHIRCodeableConcept $value): self
    {
        $this->interpretation = $value;

        return $this;
    }

    public function getComment(): ?FHIRString
    {
        return $this->comment;
    }

    public function setComment(string|FHIRString|null $value): self
    {
        $this->comment = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getBodySite(): ?FHIRCodeableConcept
    {
        return $this->bodySite;
    }

    public function setBodySite(?FHIRCodeableConcept $value): self
    {
        $this->bodySite = $value;

        return $this;
    }

    public function getMethod(): ?FHIRCodeableConcept
    {
        return $this->method;
    }

    public function setMethod(?FHIRCodeableConcept $value): self
    {
        $this->method = $value;

        return $this;
    }

    public function getSpecimen(): ?FHIRReference
    {
        return $this->specimen;
    }

    public function setSpecimen(?FHIRReference $value): self
    {
        $this->specimen = $value;

        return $this;
    }

    public function getDevice(): ?FHIRReference
    {
        return $this->device;
    }

    public function setDevice(?FHIRReference $value): self
    {
        $this->device = $value;

        return $this;
    }

    /**
     * @return FHIRObservationReferenceRange[]
     */
    public function getReferenceRange(): array
    {
        return $this->referenceRange;
    }

    public function setReferenceRange(?FHIRObservationReferenceRange ...$value): self
    {
        $this->referenceRange = array_filter($value);

        return $this;
    }

    public function addReferenceRange(?FHIRObservationReferenceRange ...$value): self
    {
        $this->referenceRange = array_filter(array_merge($this->referenceRange, $value));

        return $this;
    }

    /**
     * @return FHIRObservationRelated[]
     */
    public function getRelated(): array
    {
        return $this->related;
    }

    public function setRelated(?FHIRObservationRelated ...$value): self
    {
        $this->related = array_filter($value);

        return $this;
    }

    public function addRelated(?FHIRObservationRelated ...$value): self
    {
        $this->related = array_filter(array_merge($this->related, $value));

        return $this;
    }

    /**
     * @return FHIRObservationComponent[]
     */
    public function getComponent(): array
    {
        return $this->component;
    }

    public function setComponent(?FHIRObservationComponent ...$value): self
    {
        $this->component = array_filter($value);

        return $this;
    }

    public function addComponent(?FHIRObservationComponent ...$value): self
    {
        $this->component = array_filter(array_merge($this->component, $value));

        return $this;
    }
}
