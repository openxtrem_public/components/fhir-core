<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Consent Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRConsentInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRConsentActor;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRConsentData;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRConsentExcept;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRConsentPolicy;

class FHIRConsent extends FHIRDomainResource implements FHIRConsentInterface
{
    public const RESOURCE_NAME = 'Consent';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRCode $status = null;

    /** @var FHIRCodeableConcept[] */
    protected array $category = [];
    protected ?FHIRReference $patient = null;
    protected ?FHIRPeriod $period = null;
    protected ?FHIRDateTime $dateTime = null;

    /** @var FHIRReference[] */
    protected array $consentingParty = [];

    /** @var FHIRConsentActor[] */
    protected array $actor = [];

    /** @var FHIRCodeableConcept[] */
    protected array $action = [];

    /** @var FHIRReference[] */
    protected array $organization = [];
    protected FHIRAttachment|FHIRIdentifier|FHIRReference|null $source = null;

    /** @var FHIRConsentPolicy[] */
    protected array $policy = [];
    protected ?FHIRUri $policyRule = null;

    /** @var FHIRCoding[] */
    protected array $securityLabel = [];

    /** @var FHIRCoding[] */
    protected array $purpose = [];
    protected ?FHIRPeriod $dataPeriod = null;

    /** @var FHIRConsentData[] */
    protected array $data = [];

    /** @var FHIRConsentExcept[] */
    protected array $except = [];

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    public function setCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter($value);

        return $this;
    }

    public function addCategory(?FHIRCodeableConcept ...$value): self
    {
        $this->category = array_filter(array_merge($this->category, $value));

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    public function getDateTime(): ?FHIRDateTime
    {
        return $this->dateTime;
    }

    public function setDateTime(string|FHIRDateTime|null $value): self
    {
        $this->dateTime = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getConsentingParty(): array
    {
        return $this->consentingParty;
    }

    public function setConsentingParty(?FHIRReference ...$value): self
    {
        $this->consentingParty = array_filter($value);

        return $this;
    }

    public function addConsentingParty(?FHIRReference ...$value): self
    {
        $this->consentingParty = array_filter(array_merge($this->consentingParty, $value));

        return $this;
    }

    /**
     * @return FHIRConsentActor[]
     */
    public function getActor(): array
    {
        return $this->actor;
    }

    public function setActor(?FHIRConsentActor ...$value): self
    {
        $this->actor = array_filter($value);

        return $this;
    }

    public function addActor(?FHIRConsentActor ...$value): self
    {
        $this->actor = array_filter(array_merge($this->actor, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getAction(): array
    {
        return $this->action;
    }

    public function setAction(?FHIRCodeableConcept ...$value): self
    {
        $this->action = array_filter($value);

        return $this;
    }

    public function addAction(?FHIRCodeableConcept ...$value): self
    {
        $this->action = array_filter(array_merge($this->action, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getOrganization(): array
    {
        return $this->organization;
    }

    public function setOrganization(?FHIRReference ...$value): self
    {
        $this->organization = array_filter($value);

        return $this;
    }

    public function addOrganization(?FHIRReference ...$value): self
    {
        $this->organization = array_filter(array_merge($this->organization, $value));

        return $this;
    }

    public function getSource(): FHIRAttachment|FHIRIdentifier|FHIRReference|null
    {
        return $this->source;
    }

    public function setSource(FHIRAttachment|FHIRIdentifier|FHIRReference|null $value): self
    {
        $this->source = $value;

        return $this;
    }

    /**
     * @return FHIRConsentPolicy[]
     */
    public function getPolicy(): array
    {
        return $this->policy;
    }

    public function setPolicy(?FHIRConsentPolicy ...$value): self
    {
        $this->policy = array_filter($value);

        return $this;
    }

    public function addPolicy(?FHIRConsentPolicy ...$value): self
    {
        $this->policy = array_filter(array_merge($this->policy, $value));

        return $this;
    }

    public function getPolicyRule(): ?FHIRUri
    {
        return $this->policyRule;
    }

    public function setPolicyRule(string|FHIRUri|null $value): self
    {
        $this->policyRule = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getSecurityLabel(): array
    {
        return $this->securityLabel;
    }

    public function setSecurityLabel(?FHIRCoding ...$value): self
    {
        $this->securityLabel = array_filter($value);

        return $this;
    }

    public function addSecurityLabel(?FHIRCoding ...$value): self
    {
        $this->securityLabel = array_filter(array_merge($this->securityLabel, $value));

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getPurpose(): array
    {
        return $this->purpose;
    }

    public function setPurpose(?FHIRCoding ...$value): self
    {
        $this->purpose = array_filter($value);

        return $this;
    }

    public function addPurpose(?FHIRCoding ...$value): self
    {
        $this->purpose = array_filter(array_merge($this->purpose, $value));

        return $this;
    }

    public function getDataPeriod(): ?FHIRPeriod
    {
        return $this->dataPeriod;
    }

    public function setDataPeriod(?FHIRPeriod $value): self
    {
        $this->dataPeriod = $value;

        return $this;
    }

    /**
     * @return FHIRConsentData[]
     */
    public function getData(): array
    {
        return $this->data;
    }

    public function setData(?FHIRConsentData ...$value): self
    {
        $this->data = array_filter($value);

        return $this;
    }

    public function addData(?FHIRConsentData ...$value): self
    {
        $this->data = array_filter(array_merge($this->data, $value));

        return $this;
    }

    /**
     * @return FHIRConsentExcept[]
     */
    public function getExcept(): array
    {
        return $this->except;
    }

    public function setExcept(?FHIRConsentExcept ...$value): self
    {
        $this->except = array_filter($value);

        return $this;
    }

    public function addExcept(?FHIRConsentExcept ...$value): self
    {
        $this->except = array_filter(array_merge($this->except, $value));

        return $this;
    }
}
