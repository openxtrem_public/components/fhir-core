<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR PaymentReconciliation Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRPaymentReconciliationInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRPaymentReconciliationDetail;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRPaymentReconciliationProcessNote;

class FHIRPaymentReconciliation extends FHIRDomainResource implements FHIRPaymentReconciliationInterface
{
    public const RESOURCE_NAME = 'PaymentReconciliation';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRPeriod $period = null;
    protected ?FHIRDateTime $created = null;
    protected ?FHIRReference $organization = null;
    protected ?FHIRReference $request = null;
    protected ?FHIRCodeableConcept $outcome = null;
    protected ?FHIRString $disposition = null;
    protected ?FHIRReference $requestProvider = null;
    protected ?FHIRReference $requestOrganization = null;

    /** @var FHIRPaymentReconciliationDetail[] */
    protected array $detail = [];
    protected ?FHIRCodeableConcept $form = null;
    protected ?FHIRMoney $total = null;

    /** @var FHIRPaymentReconciliationProcessNote[] */
    protected array $processNote = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }

    public function getCreated(): ?FHIRDateTime
    {
        return $this->created;
    }

    public function setCreated(string|FHIRDateTime|null $value): self
    {
        $this->created = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getOrganization(): ?FHIRReference
    {
        return $this->organization;
    }

    public function setOrganization(?FHIRReference $value): self
    {
        $this->organization = $value;

        return $this;
    }

    public function getRequest(): ?FHIRReference
    {
        return $this->request;
    }

    public function setRequest(?FHIRReference $value): self
    {
        $this->request = $value;

        return $this;
    }

    public function getOutcome(): ?FHIRCodeableConcept
    {
        return $this->outcome;
    }

    public function setOutcome(?FHIRCodeableConcept $value): self
    {
        $this->outcome = $value;

        return $this;
    }

    public function getDisposition(): ?FHIRString
    {
        return $this->disposition;
    }

    public function setDisposition(string|FHIRString|null $value): self
    {
        $this->disposition = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getRequestProvider(): ?FHIRReference
    {
        return $this->requestProvider;
    }

    public function setRequestProvider(?FHIRReference $value): self
    {
        $this->requestProvider = $value;

        return $this;
    }

    public function getRequestOrganization(): ?FHIRReference
    {
        return $this->requestOrganization;
    }

    public function setRequestOrganization(?FHIRReference $value): self
    {
        $this->requestOrganization = $value;

        return $this;
    }

    /**
     * @return FHIRPaymentReconciliationDetail[]
     */
    public function getDetail(): array
    {
        return $this->detail;
    }

    public function setDetail(?FHIRPaymentReconciliationDetail ...$value): self
    {
        $this->detail = array_filter($value);

        return $this;
    }

    public function addDetail(?FHIRPaymentReconciliationDetail ...$value): self
    {
        $this->detail = array_filter(array_merge($this->detail, $value));

        return $this;
    }

    public function getForm(): ?FHIRCodeableConcept
    {
        return $this->form;
    }

    public function setForm(?FHIRCodeableConcept $value): self
    {
        $this->form = $value;

        return $this;
    }

    public function getTotal(): ?FHIRMoney
    {
        return $this->total;
    }

    public function setTotal(?FHIRMoney $value): self
    {
        $this->total = $value;

        return $this;
    }

    /**
     * @return FHIRPaymentReconciliationProcessNote[]
     */
    public function getProcessNote(): array
    {
        return $this->processNote;
    }

    public function setProcessNote(?FHIRPaymentReconciliationProcessNote ...$value): self
    {
        $this->processNote = array_filter($value);

        return $this;
    }

    public function addProcessNote(?FHIRPaymentReconciliationProcessNote ...$value): self
    {
        $this->processNote = array_filter(array_merge($this->processNote, $value));

        return $this;
    }
}
