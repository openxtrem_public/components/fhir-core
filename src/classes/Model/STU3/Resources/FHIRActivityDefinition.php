<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ActivityDefinition Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRActivityDefinitionInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRContributor;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRDosage;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRRange;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRRelatedArtifact;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRTiming;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRActivityDefinitionDynamicValue;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRActivityDefinitionParticipant;

class FHIRActivityDefinition extends FHIRDomainResource implements FHIRActivityDefinitionInterface
{
    public const RESOURCE_NAME = 'ActivityDefinition';

    protected ?FHIRUri $url = null;

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRString $version = null;
    protected ?FHIRString $name = null;
    protected ?FHIRString $title = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRBoolean $experimental = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRString $publisher = null;
    protected ?FHIRMarkdown $description = null;
    protected ?FHIRMarkdown $purpose = null;
    protected ?FHIRString $usage = null;
    protected ?FHIRDate $approvalDate = null;
    protected ?FHIRDate $lastReviewDate = null;
    protected ?FHIRPeriod $effectivePeriod = null;

    /** @var FHIRUsageContext[] */
    protected array $useContext = [];

    /** @var FHIRCodeableConcept[] */
    protected array $jurisdiction = [];

    /** @var FHIRCodeableConcept[] */
    protected array $topic = [];

    /** @var FHIRContributor[] */
    protected array $contributor = [];

    /** @var FHIRContactDetail[] */
    protected array $contact = [];
    protected ?FHIRMarkdown $copyright = null;

    /** @var FHIRRelatedArtifact[] */
    protected array $relatedArtifact = [];

    /** @var FHIRReference[] */
    protected array $library = [];
    protected ?FHIRCode $kind = null;
    protected ?FHIRCodeableConcept $code = null;
    protected FHIRTiming|FHIRDateTime|FHIRPeriod|FHIRRange|null $timing = null;
    protected ?FHIRReference $location = null;

    /** @var FHIRActivityDefinitionParticipant[] */
    protected array $participant = [];
    protected FHIRReference|FHIRCodeableConcept|null $product = null;
    protected ?FHIRQuantity $quantity = null;

    /** @var FHIRDosage[] */
    protected array $dosage = [];

    /** @var FHIRCodeableConcept[] */
    protected array $bodySite = [];
    protected ?FHIRReference $transform = null;

    /** @var FHIRActivityDefinitionDynamicValue[] */
    protected array $dynamicValue = [];

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getTitle(): ?FHIRString
    {
        return $this->title;
    }

    public function setTitle(string|FHIRString|null $value): self
    {
        $this->title = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getExperimental(): ?FHIRBoolean
    {
        return $this->experimental;
    }

    public function setExperimental(bool|FHIRBoolean|null $value): self
    {
        $this->experimental = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPublisher(): ?FHIRString
    {
        return $this->publisher;
    }

    public function setPublisher(string|FHIRString|null $value): self
    {
        $this->publisher = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getPurpose(): ?FHIRMarkdown
    {
        return $this->purpose;
    }

    public function setPurpose(string|FHIRMarkdown|null $value): self
    {
        $this->purpose = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getUsage(): ?FHIRString
    {
        return $this->usage;
    }

    public function setUsage(string|FHIRString|null $value): self
    {
        $this->usage = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getApprovalDate(): ?FHIRDate
    {
        return $this->approvalDate;
    }

    public function setApprovalDate(string|FHIRDate|null $value): self
    {
        $this->approvalDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getLastReviewDate(): ?FHIRDate
    {
        return $this->lastReviewDate;
    }

    public function setLastReviewDate(string|FHIRDate|null $value): self
    {
        $this->lastReviewDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getEffectivePeriod(): ?FHIRPeriod
    {
        return $this->effectivePeriod;
    }

    public function setEffectivePeriod(?FHIRPeriod $value): self
    {
        $this->effectivePeriod = $value;

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUseContext(): array
    {
        return $this->useContext;
    }

    public function setUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter($value);

        return $this;
    }

    public function addUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter(array_merge($this->useContext, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getJurisdiction(): array
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter($value);

        return $this;
    }

    public function addJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter(array_merge($this->jurisdiction, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getTopic(): array
    {
        return $this->topic;
    }

    public function setTopic(?FHIRCodeableConcept ...$value): self
    {
        $this->topic = array_filter($value);

        return $this;
    }

    public function addTopic(?FHIRCodeableConcept ...$value): self
    {
        $this->topic = array_filter(array_merge($this->topic, $value));

        return $this;
    }

    /**
     * @return FHIRContributor[]
     */
    public function getContributor(): array
    {
        return $this->contributor;
    }

    public function setContributor(?FHIRContributor ...$value): self
    {
        $this->contributor = array_filter($value);

        return $this;
    }

    public function addContributor(?FHIRContributor ...$value): self
    {
        $this->contributor = array_filter(array_merge($this->contributor, $value));

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    public function getCopyright(): ?FHIRMarkdown
    {
        return $this->copyright;
    }

    public function setCopyright(string|FHIRMarkdown|null $value): self
    {
        $this->copyright = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRRelatedArtifact[]
     */
    public function getRelatedArtifact(): array
    {
        return $this->relatedArtifact;
    }

    public function setRelatedArtifact(?FHIRRelatedArtifact ...$value): self
    {
        $this->relatedArtifact = array_filter($value);

        return $this;
    }

    public function addRelatedArtifact(?FHIRRelatedArtifact ...$value): self
    {
        $this->relatedArtifact = array_filter(array_merge($this->relatedArtifact, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getLibrary(): array
    {
        return $this->library;
    }

    public function setLibrary(?FHIRReference ...$value): self
    {
        $this->library = array_filter($value);

        return $this;
    }

    public function addLibrary(?FHIRReference ...$value): self
    {
        $this->library = array_filter(array_merge($this->library, $value));

        return $this;
    }

    public function getKind(): ?FHIRCode
    {
        return $this->kind;
    }

    public function setKind(string|FHIRCode|null $value): self
    {
        $this->kind = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getTiming(): FHIRTiming|FHIRDateTime|FHIRPeriod|FHIRRange|null
    {
        return $this->timing;
    }

    public function setTiming(FHIRTiming|FHIRDateTime|FHIRPeriod|FHIRRange|null $value): self
    {
        $this->timing = $value;

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    /**
     * @return FHIRActivityDefinitionParticipant[]
     */
    public function getParticipant(): array
    {
        return $this->participant;
    }

    public function setParticipant(?FHIRActivityDefinitionParticipant ...$value): self
    {
        $this->participant = array_filter($value);

        return $this;
    }

    public function addParticipant(?FHIRActivityDefinitionParticipant ...$value): self
    {
        $this->participant = array_filter(array_merge($this->participant, $value));

        return $this;
    }

    public function getProduct(): FHIRReference|FHIRCodeableConcept|null
    {
        return $this->product;
    }

    public function setProduct(FHIRReference|FHIRCodeableConcept|null $value): self
    {
        $this->product = $value;

        return $this;
    }

    public function getQuantity(): ?FHIRQuantity
    {
        return $this->quantity;
    }

    public function setQuantity(?FHIRQuantity $value): self
    {
        $this->quantity = $value;

        return $this;
    }

    /**
     * @return FHIRDosage[]
     */
    public function getDosage(): array
    {
        return $this->dosage;
    }

    public function setDosage(?FHIRDosage ...$value): self
    {
        $this->dosage = array_filter($value);

        return $this;
    }

    public function addDosage(?FHIRDosage ...$value): self
    {
        $this->dosage = array_filter(array_merge($this->dosage, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getBodySite(): array
    {
        return $this->bodySite;
    }

    public function setBodySite(?FHIRCodeableConcept ...$value): self
    {
        $this->bodySite = array_filter($value);

        return $this;
    }

    public function addBodySite(?FHIRCodeableConcept ...$value): self
    {
        $this->bodySite = array_filter(array_merge($this->bodySite, $value));

        return $this;
    }

    public function getTransform(): ?FHIRReference
    {
        return $this->transform;
    }

    public function setTransform(?FHIRReference $value): self
    {
        $this->transform = $value;

        return $this;
    }

    /**
     * @return FHIRActivityDefinitionDynamicValue[]
     */
    public function getDynamicValue(): array
    {
        return $this->dynamicValue;
    }

    public function setDynamicValue(?FHIRActivityDefinitionDynamicValue ...$value): self
    {
        $this->dynamicValue = array_filter($value);

        return $this;
    }

    public function addDynamicValue(?FHIRActivityDefinitionDynamicValue ...$value): self
    {
        $this->dynamicValue = array_filter(array_merge($this->dynamicValue, $value));

        return $this;
    }
}
