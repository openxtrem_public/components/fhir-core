<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ProcessRequest Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRProcessRequestInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRProcessRequestItem;

class FHIRProcessRequest extends FHIRDomainResource implements FHIRProcessRequestInterface
{
    public const RESOURCE_NAME = 'ProcessRequest';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCode $action = null;
    protected ?FHIRReference $target = null;
    protected ?FHIRDateTime $created = null;
    protected ?FHIRReference $provider = null;
    protected ?FHIRReference $organization = null;
    protected ?FHIRReference $request = null;
    protected ?FHIRReference $response = null;
    protected ?FHIRBoolean $nullify = null;
    protected ?FHIRString $reference = null;

    /** @var FHIRProcessRequestItem[] */
    protected array $item = [];

    /** @var FHIRString[] */
    protected array $include = [];

    /** @var FHIRString[] */
    protected array $exclude = [];
    protected ?FHIRPeriod $period = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getAction(): ?FHIRCode
    {
        return $this->action;
    }

    public function setAction(string|FHIRCode|null $value): self
    {
        $this->action = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getTarget(): ?FHIRReference
    {
        return $this->target;
    }

    public function setTarget(?FHIRReference $value): self
    {
        $this->target = $value;

        return $this;
    }

    public function getCreated(): ?FHIRDateTime
    {
        return $this->created;
    }

    public function setCreated(string|FHIRDateTime|null $value): self
    {
        $this->created = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getProvider(): ?FHIRReference
    {
        return $this->provider;
    }

    public function setProvider(?FHIRReference $value): self
    {
        $this->provider = $value;

        return $this;
    }

    public function getOrganization(): ?FHIRReference
    {
        return $this->organization;
    }

    public function setOrganization(?FHIRReference $value): self
    {
        $this->organization = $value;

        return $this;
    }

    public function getRequest(): ?FHIRReference
    {
        return $this->request;
    }

    public function setRequest(?FHIRReference $value): self
    {
        $this->request = $value;

        return $this;
    }

    public function getResponse(): ?FHIRReference
    {
        return $this->response;
    }

    public function setResponse(?FHIRReference $value): self
    {
        $this->response = $value;

        return $this;
    }

    public function getNullify(): ?FHIRBoolean
    {
        return $this->nullify;
    }

    public function setNullify(bool|FHIRBoolean|null $value): self
    {
        $this->nullify = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getReference(): ?FHIRString
    {
        return $this->reference;
    }

    public function setReference(string|FHIRString|null $value): self
    {
        $this->reference = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRProcessRequestItem[]
     */
    public function getItem(): array
    {
        return $this->item;
    }

    public function setItem(?FHIRProcessRequestItem ...$value): self
    {
        $this->item = array_filter($value);

        return $this;
    }

    public function addItem(?FHIRProcessRequestItem ...$value): self
    {
        $this->item = array_filter(array_merge($this->item, $value));

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getInclude(): array
    {
        return $this->include;
    }

    public function setInclude(string|FHIRString|null ...$value): self
    {
        $this->include = [];
        $this->addInclude(...$value);

        return $this;
    }

    public function addInclude(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->include = array_filter(array_merge($this->include, $values));

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getExclude(): array
    {
        return $this->exclude;
    }

    public function setExclude(string|FHIRString|null ...$value): self
    {
        $this->exclude = [];
        $this->addExclude(...$value);

        return $this;
    }

    public function addExclude(string|FHIRString|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRString())->setValue($v) : $v, $value);

        $this->exclude = array_filter(array_merge($this->exclude, $values));

        return $this;
    }

    public function getPeriod(): ?FHIRPeriod
    {
        return $this->period;
    }

    public function setPeriod(?FHIRPeriod $value): self
    {
        $this->period = $value;

        return $this;
    }
}
