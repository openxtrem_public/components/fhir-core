<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DocumentReference Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRDocumentReferenceInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRDocumentReferenceContent;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRDocumentReferenceContext;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRDocumentReferenceRelatesTo;

class FHIRDocumentReference extends FHIRDomainResource implements FHIRDocumentReferenceInterface
{
    public const RESOURCE_NAME = 'DocumentReference';

    protected ?FHIRIdentifier $masterIdentifier = null;

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCode $docStatus = null;
    protected ?FHIRCodeableConcept $type = null;
    protected ?FHIRCodeableConcept $class = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRDateTime $created = null;
    protected ?FHIRInstant $indexed = null;

    /** @var FHIRReference[] */
    protected array $author = [];
    protected ?FHIRReference $authenticator = null;
    protected ?FHIRReference $custodian = null;

    /** @var FHIRDocumentReferenceRelatesTo[] */
    protected array $relatesTo = [];
    protected ?FHIRString $description = null;

    /** @var FHIRCodeableConcept[] */
    protected array $securityLabel = [];

    /** @var FHIRDocumentReferenceContent[] */
    protected array $content = [];
    protected ?FHIRDocumentReferenceContext $context = null;

    public function getMasterIdentifier(): ?FHIRIdentifier
    {
        return $this->masterIdentifier;
    }

    public function setMasterIdentifier(?FHIRIdentifier $value): self
    {
        $this->masterIdentifier = $value;

        return $this;
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getDocStatus(): ?FHIRCode
    {
        return $this->docStatus;
    }

    public function setDocStatus(string|FHIRCode|null $value): self
    {
        $this->docStatus = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getClass(): ?FHIRCodeableConcept
    {
        return $this->class;
    }

    public function setClass(?FHIRCodeableConcept $value): self
    {
        $this->class = $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getCreated(): ?FHIRDateTime
    {
        return $this->created;
    }

    public function setCreated(string|FHIRDateTime|null $value): self
    {
        $this->created = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getIndexed(): ?FHIRInstant
    {
        return $this->indexed;
    }

    public function setIndexed(string|FHIRInstant|null $value): self
    {
        $this->indexed = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAuthor(): array
    {
        return $this->author;
    }

    public function setAuthor(?FHIRReference ...$value): self
    {
        $this->author = array_filter($value);

        return $this;
    }

    public function addAuthor(?FHIRReference ...$value): self
    {
        $this->author = array_filter(array_merge($this->author, $value));

        return $this;
    }

    public function getAuthenticator(): ?FHIRReference
    {
        return $this->authenticator;
    }

    public function setAuthenticator(?FHIRReference $value): self
    {
        $this->authenticator = $value;

        return $this;
    }

    public function getCustodian(): ?FHIRReference
    {
        return $this->custodian;
    }

    public function setCustodian(?FHIRReference $value): self
    {
        $this->custodian = $value;

        return $this;
    }

    /**
     * @return FHIRDocumentReferenceRelatesTo[]
     */
    public function getRelatesTo(): array
    {
        return $this->relatesTo;
    }

    public function setRelatesTo(?FHIRDocumentReferenceRelatesTo ...$value): self
    {
        $this->relatesTo = array_filter($value);

        return $this;
    }

    public function addRelatesTo(?FHIRDocumentReferenceRelatesTo ...$value): self
    {
        $this->relatesTo = array_filter(array_merge($this->relatesTo, $value));

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSecurityLabel(): array
    {
        return $this->securityLabel;
    }

    public function setSecurityLabel(?FHIRCodeableConcept ...$value): self
    {
        $this->securityLabel = array_filter($value);

        return $this;
    }

    public function addSecurityLabel(?FHIRCodeableConcept ...$value): self
    {
        $this->securityLabel = array_filter(array_merge($this->securityLabel, $value));

        return $this;
    }

    /**
     * @return FHIRDocumentReferenceContent[]
     */
    public function getContent(): array
    {
        return $this->content;
    }

    public function setContent(?FHIRDocumentReferenceContent ...$value): self
    {
        $this->content = array_filter($value);

        return $this;
    }

    public function addContent(?FHIRDocumentReferenceContent ...$value): self
    {
        $this->content = array_filter(array_merge($this->content, $value));

        return $this;
    }

    public function getContext(): ?FHIRDocumentReferenceContext
    {
        return $this->context;
    }

    public function setContext(?FHIRDocumentReferenceContext $value): self
    {
        $this->context = $value;

        return $this;
    }
}
