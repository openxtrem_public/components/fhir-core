<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ExplanationOfBenefit Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRExplanationOfBenefitInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRExplanationOfBenefitAccident;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRExplanationOfBenefitAddItem;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRExplanationOfBenefitBenefitBalance;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRExplanationOfBenefitCareTeam;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRExplanationOfBenefitDiagnosis;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRExplanationOfBenefitInformation;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRExplanationOfBenefitInsurance;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRExplanationOfBenefitItem;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRExplanationOfBenefitPayee;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRExplanationOfBenefitPayment;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRExplanationOfBenefitProcedure;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRExplanationOfBenefitProcessNote;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRExplanationOfBenefitRelated;

class FHIRExplanationOfBenefit extends FHIRDomainResource implements FHIRExplanationOfBenefitInterface
{
    public const RESOURCE_NAME = 'ExplanationOfBenefit';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRCodeableConcept[] */
    protected array $subType = [];
    protected ?FHIRReference $patient = null;
    protected ?FHIRPeriod $billablePeriod = null;
    protected ?FHIRDateTime $created = null;
    protected ?FHIRReference $enterer = null;
    protected ?FHIRReference $insurer = null;
    protected ?FHIRReference $provider = null;
    protected ?FHIRReference $organization = null;
    protected ?FHIRReference $referral = null;
    protected ?FHIRReference $facility = null;
    protected ?FHIRReference $claim = null;
    protected ?FHIRReference $claimResponse = null;
    protected ?FHIRCodeableConcept $outcome = null;
    protected ?FHIRString $disposition = null;

    /** @var FHIRExplanationOfBenefitRelated[] */
    protected array $related = [];
    protected ?FHIRReference $prescription = null;
    protected ?FHIRReference $originalPrescription = null;
    protected ?FHIRExplanationOfBenefitPayee $payee = null;

    /** @var FHIRExplanationOfBenefitInformation[] */
    protected array $information = [];

    /** @var FHIRExplanationOfBenefitCareTeam[] */
    protected array $careTeam = [];

    /** @var FHIRExplanationOfBenefitDiagnosis[] */
    protected array $diagnosis = [];

    /** @var FHIRExplanationOfBenefitProcedure[] */
    protected array $procedure = [];
    protected ?FHIRPositiveInt $precedence = null;
    protected ?FHIRExplanationOfBenefitInsurance $insurance = null;
    protected ?FHIRExplanationOfBenefitAccident $accident = null;
    protected ?FHIRPeriod $employmentImpacted = null;
    protected ?FHIRPeriod $hospitalization = null;

    /** @var FHIRExplanationOfBenefitItem[] */
    protected array $item = [];

    /** @var FHIRExplanationOfBenefitAddItem[] */
    protected array $addItem = [];
    protected ?FHIRMoney $totalCost = null;
    protected ?FHIRMoney $unallocDeductable = null;
    protected ?FHIRMoney $totalBenefit = null;
    protected ?FHIRExplanationOfBenefitPayment $payment = null;
    protected ?FHIRCodeableConcept $form = null;

    /** @var FHIRExplanationOfBenefitProcessNote[] */
    protected array $processNote = [];

    /** @var FHIRExplanationOfBenefitBenefitBalance[] */
    protected array $benefitBalance = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSubType(): array
    {
        return $this->subType;
    }

    public function setSubType(?FHIRCodeableConcept ...$value): self
    {
        $this->subType = array_filter($value);

        return $this;
    }

    public function addSubType(?FHIRCodeableConcept ...$value): self
    {
        $this->subType = array_filter(array_merge($this->subType, $value));

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getBillablePeriod(): ?FHIRPeriod
    {
        return $this->billablePeriod;
    }

    public function setBillablePeriod(?FHIRPeriod $value): self
    {
        $this->billablePeriod = $value;

        return $this;
    }

    public function getCreated(): ?FHIRDateTime
    {
        return $this->created;
    }

    public function setCreated(string|FHIRDateTime|null $value): self
    {
        $this->created = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getEnterer(): ?FHIRReference
    {
        return $this->enterer;
    }

    public function setEnterer(?FHIRReference $value): self
    {
        $this->enterer = $value;

        return $this;
    }

    public function getInsurer(): ?FHIRReference
    {
        return $this->insurer;
    }

    public function setInsurer(?FHIRReference $value): self
    {
        $this->insurer = $value;

        return $this;
    }

    public function getProvider(): ?FHIRReference
    {
        return $this->provider;
    }

    public function setProvider(?FHIRReference $value): self
    {
        $this->provider = $value;

        return $this;
    }

    public function getOrganization(): ?FHIRReference
    {
        return $this->organization;
    }

    public function setOrganization(?FHIRReference $value): self
    {
        $this->organization = $value;

        return $this;
    }

    public function getReferral(): ?FHIRReference
    {
        return $this->referral;
    }

    public function setReferral(?FHIRReference $value): self
    {
        $this->referral = $value;

        return $this;
    }

    public function getFacility(): ?FHIRReference
    {
        return $this->facility;
    }

    public function setFacility(?FHIRReference $value): self
    {
        $this->facility = $value;

        return $this;
    }

    public function getClaim(): ?FHIRReference
    {
        return $this->claim;
    }

    public function setClaim(?FHIRReference $value): self
    {
        $this->claim = $value;

        return $this;
    }

    public function getClaimResponse(): ?FHIRReference
    {
        return $this->claimResponse;
    }

    public function setClaimResponse(?FHIRReference $value): self
    {
        $this->claimResponse = $value;

        return $this;
    }

    public function getOutcome(): ?FHIRCodeableConcept
    {
        return $this->outcome;
    }

    public function setOutcome(?FHIRCodeableConcept $value): self
    {
        $this->outcome = $value;

        return $this;
    }

    public function getDisposition(): ?FHIRString
    {
        return $this->disposition;
    }

    public function setDisposition(string|FHIRString|null $value): self
    {
        $this->disposition = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitRelated[]
     */
    public function getRelated(): array
    {
        return $this->related;
    }

    public function setRelated(?FHIRExplanationOfBenefitRelated ...$value): self
    {
        $this->related = array_filter($value);

        return $this;
    }

    public function addRelated(?FHIRExplanationOfBenefitRelated ...$value): self
    {
        $this->related = array_filter(array_merge($this->related, $value));

        return $this;
    }

    public function getPrescription(): ?FHIRReference
    {
        return $this->prescription;
    }

    public function setPrescription(?FHIRReference $value): self
    {
        $this->prescription = $value;

        return $this;
    }

    public function getOriginalPrescription(): ?FHIRReference
    {
        return $this->originalPrescription;
    }

    public function setOriginalPrescription(?FHIRReference $value): self
    {
        $this->originalPrescription = $value;

        return $this;
    }

    public function getPayee(): ?FHIRExplanationOfBenefitPayee
    {
        return $this->payee;
    }

    public function setPayee(?FHIRExplanationOfBenefitPayee $value): self
    {
        $this->payee = $value;

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitInformation[]
     */
    public function getInformation(): array
    {
        return $this->information;
    }

    public function setInformation(?FHIRExplanationOfBenefitInformation ...$value): self
    {
        $this->information = array_filter($value);

        return $this;
    }

    public function addInformation(?FHIRExplanationOfBenefitInformation ...$value): self
    {
        $this->information = array_filter(array_merge($this->information, $value));

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitCareTeam[]
     */
    public function getCareTeam(): array
    {
        return $this->careTeam;
    }

    public function setCareTeam(?FHIRExplanationOfBenefitCareTeam ...$value): self
    {
        $this->careTeam = array_filter($value);

        return $this;
    }

    public function addCareTeam(?FHIRExplanationOfBenefitCareTeam ...$value): self
    {
        $this->careTeam = array_filter(array_merge($this->careTeam, $value));

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitDiagnosis[]
     */
    public function getDiagnosis(): array
    {
        return $this->diagnosis;
    }

    public function setDiagnosis(?FHIRExplanationOfBenefitDiagnosis ...$value): self
    {
        $this->diagnosis = array_filter($value);

        return $this;
    }

    public function addDiagnosis(?FHIRExplanationOfBenefitDiagnosis ...$value): self
    {
        $this->diagnosis = array_filter(array_merge($this->diagnosis, $value));

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitProcedure[]
     */
    public function getProcedure(): array
    {
        return $this->procedure;
    }

    public function setProcedure(?FHIRExplanationOfBenefitProcedure ...$value): self
    {
        $this->procedure = array_filter($value);

        return $this;
    }

    public function addProcedure(?FHIRExplanationOfBenefitProcedure ...$value): self
    {
        $this->procedure = array_filter(array_merge($this->procedure, $value));

        return $this;
    }

    public function getPrecedence(): ?FHIRPositiveInt
    {
        return $this->precedence;
    }

    public function setPrecedence(int|FHIRPositiveInt|null $value): self
    {
        $this->precedence = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getInsurance(): ?FHIRExplanationOfBenefitInsurance
    {
        return $this->insurance;
    }

    public function setInsurance(?FHIRExplanationOfBenefitInsurance $value): self
    {
        $this->insurance = $value;

        return $this;
    }

    public function getAccident(): ?FHIRExplanationOfBenefitAccident
    {
        return $this->accident;
    }

    public function setAccident(?FHIRExplanationOfBenefitAccident $value): self
    {
        $this->accident = $value;

        return $this;
    }

    public function getEmploymentImpacted(): ?FHIRPeriod
    {
        return $this->employmentImpacted;
    }

    public function setEmploymentImpacted(?FHIRPeriod $value): self
    {
        $this->employmentImpacted = $value;

        return $this;
    }

    public function getHospitalization(): ?FHIRPeriod
    {
        return $this->hospitalization;
    }

    public function setHospitalization(?FHIRPeriod $value): self
    {
        $this->hospitalization = $value;

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitItem[]
     */
    public function getItem(): array
    {
        return $this->item;
    }

    public function setItem(?FHIRExplanationOfBenefitItem ...$value): self
    {
        $this->item = array_filter($value);

        return $this;
    }

    public function addItem(?FHIRExplanationOfBenefitItem ...$value): self
    {
        $this->item = array_filter(array_merge($this->item, $value));

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitAddItem[]
     */
    public function getAddItem(): array
    {
        return $this->addItem;
    }

    public function setAddItem(?FHIRExplanationOfBenefitAddItem ...$value): self
    {
        $this->addItem = array_filter($value);

        return $this;
    }

    public function addAddItem(?FHIRExplanationOfBenefitAddItem ...$value): self
    {
        $this->addItem = array_filter(array_merge($this->addItem, $value));

        return $this;
    }

    public function getTotalCost(): ?FHIRMoney
    {
        return $this->totalCost;
    }

    public function setTotalCost(?FHIRMoney $value): self
    {
        $this->totalCost = $value;

        return $this;
    }

    public function getUnallocDeductable(): ?FHIRMoney
    {
        return $this->unallocDeductable;
    }

    public function setUnallocDeductable(?FHIRMoney $value): self
    {
        $this->unallocDeductable = $value;

        return $this;
    }

    public function getTotalBenefit(): ?FHIRMoney
    {
        return $this->totalBenefit;
    }

    public function setTotalBenefit(?FHIRMoney $value): self
    {
        $this->totalBenefit = $value;

        return $this;
    }

    public function getPayment(): ?FHIRExplanationOfBenefitPayment
    {
        return $this->payment;
    }

    public function setPayment(?FHIRExplanationOfBenefitPayment $value): self
    {
        $this->payment = $value;

        return $this;
    }

    public function getForm(): ?FHIRCodeableConcept
    {
        return $this->form;
    }

    public function setForm(?FHIRCodeableConcept $value): self
    {
        $this->form = $value;

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitProcessNote[]
     */
    public function getProcessNote(): array
    {
        return $this->processNote;
    }

    public function setProcessNote(?FHIRExplanationOfBenefitProcessNote ...$value): self
    {
        $this->processNote = array_filter($value);

        return $this;
    }

    public function addProcessNote(?FHIRExplanationOfBenefitProcessNote ...$value): self
    {
        $this->processNote = array_filter(array_merge($this->processNote, $value));

        return $this;
    }

    /**
     * @return FHIRExplanationOfBenefitBenefitBalance[]
     */
    public function getBenefitBalance(): array
    {
        return $this->benefitBalance;
    }

    public function setBenefitBalance(?FHIRExplanationOfBenefitBenefitBalance ...$value): self
    {
        $this->benefitBalance = array_filter($value);

        return $this;
    }

    public function addBenefitBalance(?FHIRExplanationOfBenefitBenefitBalance ...$value): self
    {
        $this->benefitBalance = array_filter(array_merge($this->benefitBalance, $value));

        return $this;
    }
}
