<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Claim Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRClaimInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRMoney;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRClaimAccident;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRClaimCareTeam;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRClaimDiagnosis;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRClaimInformation;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRClaimInsurance;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRClaimItem;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRClaimPayee;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRClaimProcedure;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRClaimRelated;

class FHIRClaim extends FHIRDomainResource implements FHIRClaimInterface
{
    public const RESOURCE_NAME = 'Claim';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRCodeableConcept[] */
    protected array $subType = [];
    protected ?FHIRCode $use = null;
    protected ?FHIRReference $patient = null;
    protected ?FHIRPeriod $billablePeriod = null;
    protected ?FHIRDateTime $created = null;
    protected ?FHIRReference $enterer = null;
    protected ?FHIRReference $insurer = null;
    protected ?FHIRReference $provider = null;
    protected ?FHIRReference $organization = null;
    protected ?FHIRCodeableConcept $priority = null;
    protected ?FHIRCodeableConcept $fundsReserve = null;

    /** @var FHIRClaimRelated[] */
    protected array $related = [];
    protected ?FHIRReference $prescription = null;
    protected ?FHIRReference $originalPrescription = null;
    protected ?FHIRClaimPayee $payee = null;
    protected ?FHIRReference $referral = null;
    protected ?FHIRReference $facility = null;

    /** @var FHIRClaimCareTeam[] */
    protected array $careTeam = [];

    /** @var FHIRClaimInformation[] */
    protected array $information = [];

    /** @var FHIRClaimDiagnosis[] */
    protected array $diagnosis = [];

    /** @var FHIRClaimProcedure[] */
    protected array $procedure = [];

    /** @var FHIRClaimInsurance[] */
    protected array $insurance = [];
    protected ?FHIRClaimAccident $accident = null;
    protected ?FHIRPeriod $employmentImpacted = null;
    protected ?FHIRPeriod $hospitalization = null;

    /** @var FHIRClaimItem[] */
    protected array $item = [];
    protected ?FHIRMoney $total = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSubType(): array
    {
        return $this->subType;
    }

    public function setSubType(?FHIRCodeableConcept ...$value): self
    {
        $this->subType = array_filter($value);

        return $this;
    }

    public function addSubType(?FHIRCodeableConcept ...$value): self
    {
        $this->subType = array_filter(array_merge($this->subType, $value));

        return $this;
    }

    public function getUse(): ?FHIRCode
    {
        return $this->use;
    }

    public function setUse(string|FHIRCode|null $value): self
    {
        $this->use = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getBillablePeriod(): ?FHIRPeriod
    {
        return $this->billablePeriod;
    }

    public function setBillablePeriod(?FHIRPeriod $value): self
    {
        $this->billablePeriod = $value;

        return $this;
    }

    public function getCreated(): ?FHIRDateTime
    {
        return $this->created;
    }

    public function setCreated(string|FHIRDateTime|null $value): self
    {
        $this->created = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getEnterer(): ?FHIRReference
    {
        return $this->enterer;
    }

    public function setEnterer(?FHIRReference $value): self
    {
        $this->enterer = $value;

        return $this;
    }

    public function getInsurer(): ?FHIRReference
    {
        return $this->insurer;
    }

    public function setInsurer(?FHIRReference $value): self
    {
        $this->insurer = $value;

        return $this;
    }

    public function getProvider(): ?FHIRReference
    {
        return $this->provider;
    }

    public function setProvider(?FHIRReference $value): self
    {
        $this->provider = $value;

        return $this;
    }

    public function getOrganization(): ?FHIRReference
    {
        return $this->organization;
    }

    public function setOrganization(?FHIRReference $value): self
    {
        $this->organization = $value;

        return $this;
    }

    public function getPriority(): ?FHIRCodeableConcept
    {
        return $this->priority;
    }

    public function setPriority(?FHIRCodeableConcept $value): self
    {
        $this->priority = $value;

        return $this;
    }

    public function getFundsReserve(): ?FHIRCodeableConcept
    {
        return $this->fundsReserve;
    }

    public function setFundsReserve(?FHIRCodeableConcept $value): self
    {
        $this->fundsReserve = $value;

        return $this;
    }

    /**
     * @return FHIRClaimRelated[]
     */
    public function getRelated(): array
    {
        return $this->related;
    }

    public function setRelated(?FHIRClaimRelated ...$value): self
    {
        $this->related = array_filter($value);

        return $this;
    }

    public function addRelated(?FHIRClaimRelated ...$value): self
    {
        $this->related = array_filter(array_merge($this->related, $value));

        return $this;
    }

    public function getPrescription(): ?FHIRReference
    {
        return $this->prescription;
    }

    public function setPrescription(?FHIRReference $value): self
    {
        $this->prescription = $value;

        return $this;
    }

    public function getOriginalPrescription(): ?FHIRReference
    {
        return $this->originalPrescription;
    }

    public function setOriginalPrescription(?FHIRReference $value): self
    {
        $this->originalPrescription = $value;

        return $this;
    }

    public function getPayee(): ?FHIRClaimPayee
    {
        return $this->payee;
    }

    public function setPayee(?FHIRClaimPayee $value): self
    {
        $this->payee = $value;

        return $this;
    }

    public function getReferral(): ?FHIRReference
    {
        return $this->referral;
    }

    public function setReferral(?FHIRReference $value): self
    {
        $this->referral = $value;

        return $this;
    }

    public function getFacility(): ?FHIRReference
    {
        return $this->facility;
    }

    public function setFacility(?FHIRReference $value): self
    {
        $this->facility = $value;

        return $this;
    }

    /**
     * @return FHIRClaimCareTeam[]
     */
    public function getCareTeam(): array
    {
        return $this->careTeam;
    }

    public function setCareTeam(?FHIRClaimCareTeam ...$value): self
    {
        $this->careTeam = array_filter($value);

        return $this;
    }

    public function addCareTeam(?FHIRClaimCareTeam ...$value): self
    {
        $this->careTeam = array_filter(array_merge($this->careTeam, $value));

        return $this;
    }

    /**
     * @return FHIRClaimInformation[]
     */
    public function getInformation(): array
    {
        return $this->information;
    }

    public function setInformation(?FHIRClaimInformation ...$value): self
    {
        $this->information = array_filter($value);

        return $this;
    }

    public function addInformation(?FHIRClaimInformation ...$value): self
    {
        $this->information = array_filter(array_merge($this->information, $value));

        return $this;
    }

    /**
     * @return FHIRClaimDiagnosis[]
     */
    public function getDiagnosis(): array
    {
        return $this->diagnosis;
    }

    public function setDiagnosis(?FHIRClaimDiagnosis ...$value): self
    {
        $this->diagnosis = array_filter($value);

        return $this;
    }

    public function addDiagnosis(?FHIRClaimDiagnosis ...$value): self
    {
        $this->diagnosis = array_filter(array_merge($this->diagnosis, $value));

        return $this;
    }

    /**
     * @return FHIRClaimProcedure[]
     */
    public function getProcedure(): array
    {
        return $this->procedure;
    }

    public function setProcedure(?FHIRClaimProcedure ...$value): self
    {
        $this->procedure = array_filter($value);

        return $this;
    }

    public function addProcedure(?FHIRClaimProcedure ...$value): self
    {
        $this->procedure = array_filter(array_merge($this->procedure, $value));

        return $this;
    }

    /**
     * @return FHIRClaimInsurance[]
     */
    public function getInsurance(): array
    {
        return $this->insurance;
    }

    public function setInsurance(?FHIRClaimInsurance ...$value): self
    {
        $this->insurance = array_filter($value);

        return $this;
    }

    public function addInsurance(?FHIRClaimInsurance ...$value): self
    {
        $this->insurance = array_filter(array_merge($this->insurance, $value));

        return $this;
    }

    public function getAccident(): ?FHIRClaimAccident
    {
        return $this->accident;
    }

    public function setAccident(?FHIRClaimAccident $value): self
    {
        $this->accident = $value;

        return $this;
    }

    public function getEmploymentImpacted(): ?FHIRPeriod
    {
        return $this->employmentImpacted;
    }

    public function setEmploymentImpacted(?FHIRPeriod $value): self
    {
        $this->employmentImpacted = $value;

        return $this;
    }

    public function getHospitalization(): ?FHIRPeriod
    {
        return $this->hospitalization;
    }

    public function setHospitalization(?FHIRPeriod $value): self
    {
        $this->hospitalization = $value;

        return $this;
    }

    /**
     * @return FHIRClaimItem[]
     */
    public function getItem(): array
    {
        return $this->item;
    }

    public function setItem(?FHIRClaimItem ...$value): self
    {
        $this->item = array_filter($value);

        return $this;
    }

    public function addItem(?FHIRClaimItem ...$value): self
    {
        $this->item = array_filter(array_merge($this->item, $value));

        return $this;
    }

    public function getTotal(): ?FHIRMoney
    {
        return $this->total;
    }

    public function setTotal(?FHIRMoney $value): self
    {
        $this->total = $value;

        return $this;
    }
}
