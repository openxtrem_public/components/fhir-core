<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Binary Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRBinaryInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBase64Binary;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;

class FHIRBinary extends FHIRResource implements FHIRBinaryInterface
{
    public const RESOURCE_NAME = 'Binary';

    protected ?FHIRCode $contentType = null;
    protected ?FHIRReference $securityContext = null;
    protected ?FHIRBase64Binary $content = null;

    public function getContentType(): ?FHIRCode
    {
        return $this->contentType;
    }

    public function setContentType(string|FHIRCode|null $value): self
    {
        $this->contentType = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getSecurityContext(): ?FHIRReference
    {
        return $this->securityContext;
    }

    public function setSecurityContext(?FHIRReference $value): self
    {
        $this->securityContext = $value;

        return $this;
    }

    public function getContent(): ?FHIRBase64Binary
    {
        return $this->content;
    }

    public function setContent(string|FHIRBase64Binary|null $value): self
    {
        $this->content = is_string($value) ? (new FHIRBase64Binary())->setValue($value) : $value;

        return $this;
    }
}
