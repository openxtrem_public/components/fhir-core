<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Contract Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRContractInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRContractAgent;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRContractFriendly;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRContractLegal;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRContractRule;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRContractSigner;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRContractTerm;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRContractValuedItem;

class FHIRContract extends FHIRDomainResource implements FHIRContractInterface
{
    public const RESOURCE_NAME = 'Contract';

    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRDateTime $issued = null;
    protected ?FHIRPeriod $applies = null;

    /** @var FHIRReference[] */
    protected array $subject = [];

    /** @var FHIRReference[] */
    protected array $topic = [];

    /** @var FHIRReference[] */
    protected array $authority = [];

    /** @var FHIRReference[] */
    protected array $domain = [];
    protected ?FHIRCodeableConcept $type = null;

    /** @var FHIRCodeableConcept[] */
    protected array $subType = [];

    /** @var FHIRCodeableConcept[] */
    protected array $action = [];

    /** @var FHIRCodeableConcept[] */
    protected array $actionReason = [];
    protected ?FHIRCodeableConcept $decisionType = null;
    protected ?FHIRCodeableConcept $contentDerivative = null;

    /** @var FHIRCoding[] */
    protected array $securityLabel = [];

    /** @var FHIRContractAgent[] */
    protected array $agent = [];

    /** @var FHIRContractSigner[] */
    protected array $signer = [];

    /** @var FHIRContractValuedItem[] */
    protected array $valuedItem = [];

    /** @var FHIRContractTerm[] */
    protected array $term = [];
    protected FHIRAttachment|FHIRReference|null $binding = null;

    /** @var FHIRContractFriendly[] */
    protected array $friendly = [];

    /** @var FHIRContractLegal[] */
    protected array $legal = [];

    /** @var FHIRContractRule[] */
    protected array $rule = [];

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getIssued(): ?FHIRDateTime
    {
        return $this->issued;
    }

    public function setIssued(string|FHIRDateTime|null $value): self
    {
        $this->issued = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getApplies(): ?FHIRPeriod
    {
        return $this->applies;
    }

    public function setApplies(?FHIRPeriod $value): self
    {
        $this->applies = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSubject(): array
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference ...$value): self
    {
        $this->subject = array_filter($value);

        return $this;
    }

    public function addSubject(?FHIRReference ...$value): self
    {
        $this->subject = array_filter(array_merge($this->subject, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getTopic(): array
    {
        return $this->topic;
    }

    public function setTopic(?FHIRReference ...$value): self
    {
        $this->topic = array_filter($value);

        return $this;
    }

    public function addTopic(?FHIRReference ...$value): self
    {
        $this->topic = array_filter(array_merge($this->topic, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAuthority(): array
    {
        return $this->authority;
    }

    public function setAuthority(?FHIRReference ...$value): self
    {
        $this->authority = array_filter($value);

        return $this;
    }

    public function addAuthority(?FHIRReference ...$value): self
    {
        $this->authority = array_filter(array_merge($this->authority, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getDomain(): array
    {
        return $this->domain;
    }

    public function setDomain(?FHIRReference ...$value): self
    {
        $this->domain = array_filter($value);

        return $this;
    }

    public function addDomain(?FHIRReference ...$value): self
    {
        $this->domain = array_filter(array_merge($this->domain, $value));

        return $this;
    }

    public function getType(): ?FHIRCodeableConcept
    {
        return $this->type;
    }

    public function setType(?FHIRCodeableConcept $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSubType(): array
    {
        return $this->subType;
    }

    public function setSubType(?FHIRCodeableConcept ...$value): self
    {
        $this->subType = array_filter($value);

        return $this;
    }

    public function addSubType(?FHIRCodeableConcept ...$value): self
    {
        $this->subType = array_filter(array_merge($this->subType, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getAction(): array
    {
        return $this->action;
    }

    public function setAction(?FHIRCodeableConcept ...$value): self
    {
        $this->action = array_filter($value);

        return $this;
    }

    public function addAction(?FHIRCodeableConcept ...$value): self
    {
        $this->action = array_filter(array_merge($this->action, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getActionReason(): array
    {
        return $this->actionReason;
    }

    public function setActionReason(?FHIRCodeableConcept ...$value): self
    {
        $this->actionReason = array_filter($value);

        return $this;
    }

    public function addActionReason(?FHIRCodeableConcept ...$value): self
    {
        $this->actionReason = array_filter(array_merge($this->actionReason, $value));

        return $this;
    }

    public function getDecisionType(): ?FHIRCodeableConcept
    {
        return $this->decisionType;
    }

    public function setDecisionType(?FHIRCodeableConcept $value): self
    {
        $this->decisionType = $value;

        return $this;
    }

    public function getContentDerivative(): ?FHIRCodeableConcept
    {
        return $this->contentDerivative;
    }

    public function setContentDerivative(?FHIRCodeableConcept $value): self
    {
        $this->contentDerivative = $value;

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getSecurityLabel(): array
    {
        return $this->securityLabel;
    }

    public function setSecurityLabel(?FHIRCoding ...$value): self
    {
        $this->securityLabel = array_filter($value);

        return $this;
    }

    public function addSecurityLabel(?FHIRCoding ...$value): self
    {
        $this->securityLabel = array_filter(array_merge($this->securityLabel, $value));

        return $this;
    }

    /**
     * @return FHIRContractAgent[]
     */
    public function getAgent(): array
    {
        return $this->agent;
    }

    public function setAgent(?FHIRContractAgent ...$value): self
    {
        $this->agent = array_filter($value);

        return $this;
    }

    public function addAgent(?FHIRContractAgent ...$value): self
    {
        $this->agent = array_filter(array_merge($this->agent, $value));

        return $this;
    }

    /**
     * @return FHIRContractSigner[]
     */
    public function getSigner(): array
    {
        return $this->signer;
    }

    public function setSigner(?FHIRContractSigner ...$value): self
    {
        $this->signer = array_filter($value);

        return $this;
    }

    public function addSigner(?FHIRContractSigner ...$value): self
    {
        $this->signer = array_filter(array_merge($this->signer, $value));

        return $this;
    }

    /**
     * @return FHIRContractValuedItem[]
     */
    public function getValuedItem(): array
    {
        return $this->valuedItem;
    }

    public function setValuedItem(?FHIRContractValuedItem ...$value): self
    {
        $this->valuedItem = array_filter($value);

        return $this;
    }

    public function addValuedItem(?FHIRContractValuedItem ...$value): self
    {
        $this->valuedItem = array_filter(array_merge($this->valuedItem, $value));

        return $this;
    }

    /**
     * @return FHIRContractTerm[]
     */
    public function getTerm(): array
    {
        return $this->term;
    }

    public function setTerm(?FHIRContractTerm ...$value): self
    {
        $this->term = array_filter($value);

        return $this;
    }

    public function addTerm(?FHIRContractTerm ...$value): self
    {
        $this->term = array_filter(array_merge($this->term, $value));

        return $this;
    }

    public function getBinding(): FHIRAttachment|FHIRReference|null
    {
        return $this->binding;
    }

    public function setBinding(FHIRAttachment|FHIRReference|null $value): self
    {
        $this->binding = $value;

        return $this;
    }

    /**
     * @return FHIRContractFriendly[]
     */
    public function getFriendly(): array
    {
        return $this->friendly;
    }

    public function setFriendly(?FHIRContractFriendly ...$value): self
    {
        $this->friendly = array_filter($value);

        return $this;
    }

    public function addFriendly(?FHIRContractFriendly ...$value): self
    {
        $this->friendly = array_filter(array_merge($this->friendly, $value));

        return $this;
    }

    /**
     * @return FHIRContractLegal[]
     */
    public function getLegal(): array
    {
        return $this->legal;
    }

    public function setLegal(?FHIRContractLegal ...$value): self
    {
        $this->legal = array_filter($value);

        return $this;
    }

    public function addLegal(?FHIRContractLegal ...$value): self
    {
        $this->legal = array_filter(array_merge($this->legal, $value));

        return $this;
    }

    /**
     * @return FHIRContractRule[]
     */
    public function getRule(): array
    {
        return $this->rule;
    }

    public function setRule(?FHIRContractRule ...$value): self
    {
        $this->rule = array_filter($value);

        return $this;
    }

    public function addRule(?FHIRContractRule ...$value): self
    {
        $this->rule = array_filter(array_merge($this->rule, $value));

        return $this;
    }
}
