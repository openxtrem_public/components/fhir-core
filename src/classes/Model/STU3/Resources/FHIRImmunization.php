<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Immunization Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRImmunizationInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRImmunizationExplanation;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRImmunizationPractitioner;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRImmunizationReaction;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRImmunizationVaccinationProtocol;

class FHIRImmunization extends FHIRDomainResource implements FHIRImmunizationInterface
{
    public const RESOURCE_NAME = 'Immunization';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRBoolean $notGiven = null;
    protected ?FHIRCodeableConcept $vaccineCode = null;
    protected ?FHIRReference $patient = null;
    protected ?FHIRReference $encounter = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRBoolean $primarySource = null;
    protected ?FHIRCodeableConcept $reportOrigin = null;
    protected ?FHIRReference $location = null;
    protected ?FHIRReference $manufacturer = null;
    protected ?FHIRString $lotNumber = null;
    protected ?FHIRDate $expirationDate = null;
    protected ?FHIRCodeableConcept $site = null;
    protected ?FHIRCodeableConcept $route = null;
    protected ?FHIRQuantity $doseQuantity = null;

    /** @var FHIRImmunizationPractitioner[] */
    protected array $practitioner = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];
    protected ?FHIRImmunizationExplanation $explanation = null;

    /** @var FHIRImmunizationReaction[] */
    protected array $reaction = [];

    /** @var FHIRImmunizationVaccinationProtocol[] */
    protected array $vaccinationProtocol = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getNotGiven(): ?FHIRBoolean
    {
        return $this->notGiven;
    }

    public function setNotGiven(bool|FHIRBoolean|null $value): self
    {
        $this->notGiven = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getVaccineCode(): ?FHIRCodeableConcept
    {
        return $this->vaccineCode;
    }

    public function setVaccineCode(?FHIRCodeableConcept $value): self
    {
        $this->vaccineCode = $value;

        return $this;
    }

    public function getPatient(): ?FHIRReference
    {
        return $this->patient;
    }

    public function setPatient(?FHIRReference $value): self
    {
        $this->patient = $value;

        return $this;
    }

    public function getEncounter(): ?FHIRReference
    {
        return $this->encounter;
    }

    public function setEncounter(?FHIRReference $value): self
    {
        $this->encounter = $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPrimarySource(): ?FHIRBoolean
    {
        return $this->primarySource;
    }

    public function setPrimarySource(bool|FHIRBoolean|null $value): self
    {
        $this->primarySource = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getReportOrigin(): ?FHIRCodeableConcept
    {
        return $this->reportOrigin;
    }

    public function setReportOrigin(?FHIRCodeableConcept $value): self
    {
        $this->reportOrigin = $value;

        return $this;
    }

    public function getLocation(): ?FHIRReference
    {
        return $this->location;
    }

    public function setLocation(?FHIRReference $value): self
    {
        $this->location = $value;

        return $this;
    }

    public function getManufacturer(): ?FHIRReference
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?FHIRReference $value): self
    {
        $this->manufacturer = $value;

        return $this;
    }

    public function getLotNumber(): ?FHIRString
    {
        return $this->lotNumber;
    }

    public function setLotNumber(string|FHIRString|null $value): self
    {
        $this->lotNumber = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getExpirationDate(): ?FHIRDate
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(string|FHIRDate|null $value): self
    {
        $this->expirationDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    public function getSite(): ?FHIRCodeableConcept
    {
        return $this->site;
    }

    public function setSite(?FHIRCodeableConcept $value): self
    {
        $this->site = $value;

        return $this;
    }

    public function getRoute(): ?FHIRCodeableConcept
    {
        return $this->route;
    }

    public function setRoute(?FHIRCodeableConcept $value): self
    {
        $this->route = $value;

        return $this;
    }

    public function getDoseQuantity(): ?FHIRQuantity
    {
        return $this->doseQuantity;
    }

    public function setDoseQuantity(?FHIRQuantity $value): self
    {
        $this->doseQuantity = $value;

        return $this;
    }

    /**
     * @return FHIRImmunizationPractitioner[]
     */
    public function getPractitioner(): array
    {
        return $this->practitioner;
    }

    public function setPractitioner(?FHIRImmunizationPractitioner ...$value): self
    {
        $this->practitioner = array_filter($value);

        return $this;
    }

    public function addPractitioner(?FHIRImmunizationPractitioner ...$value): self
    {
        $this->practitioner = array_filter(array_merge($this->practitioner, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }

    public function getExplanation(): ?FHIRImmunizationExplanation
    {
        return $this->explanation;
    }

    public function setExplanation(?FHIRImmunizationExplanation $value): self
    {
        $this->explanation = $value;

        return $this;
    }

    /**
     * @return FHIRImmunizationReaction[]
     */
    public function getReaction(): array
    {
        return $this->reaction;
    }

    public function setReaction(?FHIRImmunizationReaction ...$value): self
    {
        $this->reaction = array_filter($value);

        return $this;
    }

    public function addReaction(?FHIRImmunizationReaction ...$value): self
    {
        $this->reaction = array_filter(array_merge($this->reaction, $value));

        return $this;
    }

    /**
     * @return FHIRImmunizationVaccinationProtocol[]
     */
    public function getVaccinationProtocol(): array
    {
        return $this->vaccinationProtocol;
    }

    public function setVaccinationProtocol(?FHIRImmunizationVaccinationProtocol ...$value): self
    {
        $this->vaccinationProtocol = array_filter($value);

        return $this;
    }

    public function addVaccinationProtocol(?FHIRImmunizationVaccinationProtocol ...$value): self
    {
        $this->vaccinationProtocol = array_filter(array_merge($this->vaccinationProtocol, $value));

        return $this;
    }
}
