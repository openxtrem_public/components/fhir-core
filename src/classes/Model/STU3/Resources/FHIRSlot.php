<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Slot Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRSlotInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRInstant;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRSlot extends FHIRDomainResource implements FHIRSlotInterface
{
    public const RESOURCE_NAME = 'Slot';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCodeableConcept $serviceCategory = null;

    /** @var FHIRCodeableConcept[] */
    protected array $serviceType = [];

    /** @var FHIRCodeableConcept[] */
    protected array $specialty = [];
    protected ?FHIRCodeableConcept $appointmentType = null;
    protected ?FHIRReference $schedule = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRInstant $start = null;
    protected ?FHIRInstant $end = null;
    protected ?FHIRBoolean $overbooked = null;
    protected ?FHIRString $comment = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getServiceCategory(): ?FHIRCodeableConcept
    {
        return $this->serviceCategory;
    }

    public function setServiceCategory(?FHIRCodeableConcept $value): self
    {
        $this->serviceCategory = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getServiceType(): array
    {
        return $this->serviceType;
    }

    public function setServiceType(?FHIRCodeableConcept ...$value): self
    {
        $this->serviceType = array_filter($value);

        return $this;
    }

    public function addServiceType(?FHIRCodeableConcept ...$value): self
    {
        $this->serviceType = array_filter(array_merge($this->serviceType, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSpecialty(): array
    {
        return $this->specialty;
    }

    public function setSpecialty(?FHIRCodeableConcept ...$value): self
    {
        $this->specialty = array_filter($value);

        return $this;
    }

    public function addSpecialty(?FHIRCodeableConcept ...$value): self
    {
        $this->specialty = array_filter(array_merge($this->specialty, $value));

        return $this;
    }

    public function getAppointmentType(): ?FHIRCodeableConcept
    {
        return $this->appointmentType;
    }

    public function setAppointmentType(?FHIRCodeableConcept $value): self
    {
        $this->appointmentType = $value;

        return $this;
    }

    public function getSchedule(): ?FHIRReference
    {
        return $this->schedule;
    }

    public function setSchedule(?FHIRReference $value): self
    {
        $this->schedule = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getStart(): ?FHIRInstant
    {
        return $this->start;
    }

    public function setStart(string|FHIRInstant|null $value): self
    {
        $this->start = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    public function getEnd(): ?FHIRInstant
    {
        return $this->end;
    }

    public function setEnd(string|FHIRInstant|null $value): self
    {
        $this->end = is_string($value) ? (new FHIRInstant())->setValue($value) : $value;

        return $this;
    }

    public function getOverbooked(): ?FHIRBoolean
    {
        return $this->overbooked;
    }

    public function setOverbooked(bool|FHIRBoolean|null $value): self
    {
        $this->overbooked = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getComment(): ?FHIRString
    {
        return $this->comment;
    }

    public function setComment(string|FHIRString|null $value): self
    {
        $this->comment = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
