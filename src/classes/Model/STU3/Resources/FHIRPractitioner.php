<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Practitioner Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRPractitionerInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAddress;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRHumanName;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDate;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRPractitionerQualification;

class FHIRPractitioner extends FHIRDomainResource implements FHIRPractitionerInterface
{
    public const RESOURCE_NAME = 'Practitioner';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRBoolean $active = null;

    /** @var FHIRHumanName[] */
    protected array $name = [];

    /** @var FHIRContactPoint[] */
    protected array $telecom = [];

    /** @var FHIRAddress[] */
    protected array $address = [];
    protected ?FHIRCode $gender = null;
    protected ?FHIRDate $birthDate = null;

    /** @var FHIRAttachment[] */
    protected array $photo = [];

    /** @var FHIRPractitionerQualification[] */
    protected array $qualification = [];

    /** @var FHIRCodeableConcept[] */
    protected array $communication = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getActive(): ?FHIRBoolean
    {
        return $this->active;
    }

    public function setActive(bool|FHIRBoolean|null $value): self
    {
        $this->active = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRHumanName[]
     */
    public function getName(): array
    {
        return $this->name;
    }

    public function setName(?FHIRHumanName ...$value): self
    {
        $this->name = array_filter($value);

        return $this;
    }

    public function addName(?FHIRHumanName ...$value): self
    {
        $this->name = array_filter(array_merge($this->name, $value));

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getTelecom(): array
    {
        return $this->telecom;
    }

    public function setTelecom(?FHIRContactPoint ...$value): self
    {
        $this->telecom = array_filter($value);

        return $this;
    }

    public function addTelecom(?FHIRContactPoint ...$value): self
    {
        $this->telecom = array_filter(array_merge($this->telecom, $value));

        return $this;
    }

    /**
     * @return FHIRAddress[]
     */
    public function getAddress(): array
    {
        return $this->address;
    }

    public function setAddress(?FHIRAddress ...$value): self
    {
        $this->address = array_filter($value);

        return $this;
    }

    public function addAddress(?FHIRAddress ...$value): self
    {
        $this->address = array_filter(array_merge($this->address, $value));

        return $this;
    }

    public function getGender(): ?FHIRCode
    {
        return $this->gender;
    }

    public function setGender(string|FHIRCode|null $value): self
    {
        $this->gender = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getBirthDate(): ?FHIRDate
    {
        return $this->birthDate;
    }

    public function setBirthDate(string|FHIRDate|null $value): self
    {
        $this->birthDate = is_string($value) ? (new FHIRDate())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRAttachment[]
     */
    public function getPhoto(): array
    {
        return $this->photo;
    }

    public function setPhoto(?FHIRAttachment ...$value): self
    {
        $this->photo = array_filter($value);

        return $this;
    }

    public function addPhoto(?FHIRAttachment ...$value): self
    {
        $this->photo = array_filter(array_merge($this->photo, $value));

        return $this;
    }

    /**
     * @return FHIRPractitionerQualification[]
     */
    public function getQualification(): array
    {
        return $this->qualification;
    }

    public function setQualification(?FHIRPractitionerQualification ...$value): self
    {
        $this->qualification = array_filter($value);

        return $this;
    }

    public function addQualification(?FHIRPractitionerQualification ...$value): self
    {
        $this->qualification = array_filter(array_merge($this->qualification, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCommunication(): array
    {
        return $this->communication;
    }

    public function setCommunication(?FHIRCodeableConcept ...$value): self
    {
        $this->communication = array_filter($value);

        return $this;
    }

    public function addCommunication(?FHIRCodeableConcept ...$value): self
    {
        $this->communication = array_filter(array_merge($this->communication, $value));

        return $this;
    }
}
