<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ClinicalImpression Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRClinicalImpressionInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRClinicalImpressionFinding;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRClinicalImpressionInvestigation;

class FHIRClinicalImpression extends FHIRDomainResource implements FHIRClinicalImpressionInterface
{
    public const RESOURCE_NAME = 'ClinicalImpression';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRCode $status = null;
    protected ?FHIRCodeableConcept $code = null;
    protected ?FHIRString $description = null;
    protected ?FHIRReference $subject = null;
    protected ?FHIRReference $context = null;
    protected FHIRDateTime|FHIRPeriod|null $effective = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRReference $assessor = null;
    protected ?FHIRReference $previous = null;

    /** @var FHIRReference[] */
    protected array $problem = [];

    /** @var FHIRClinicalImpressionInvestigation[] */
    protected array $investigation = [];

    /** @var FHIRUri[] */
    protected array $protocol = [];
    protected ?FHIRString $summary = null;

    /** @var FHIRClinicalImpressionFinding[] */
    protected array $finding = [];

    /** @var FHIRCodeableConcept[] */
    protected array $prognosisCodeableConcept = [];

    /** @var FHIRReference[] */
    protected array $prognosisReference = [];

    /** @var FHIRReference[] */
    protected array $action = [];

    /** @var FHIRAnnotation[] */
    protected array $note = [];

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->code;
    }

    public function setCode(?FHIRCodeableConcept $value): self
    {
        $this->code = $value;

        return $this;
    }

    public function getDescription(): ?FHIRString
    {
        return $this->description;
    }

    public function setDescription(string|FHIRString|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSubject(): ?FHIRReference
    {
        return $this->subject;
    }

    public function setSubject(?FHIRReference $value): self
    {
        $this->subject = $value;

        return $this;
    }

    public function getContext(): ?FHIRReference
    {
        return $this->context;
    }

    public function setContext(?FHIRReference $value): self
    {
        $this->context = $value;

        return $this;
    }

    public function getEffective(): FHIRDateTime|FHIRPeriod|null
    {
        return $this->effective;
    }

    public function setEffective(FHIRDateTime|FHIRPeriod|null $value): self
    {
        $this->effective = $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getAssessor(): ?FHIRReference
    {
        return $this->assessor;
    }

    public function setAssessor(?FHIRReference $value): self
    {
        $this->assessor = $value;

        return $this;
    }

    public function getPrevious(): ?FHIRReference
    {
        return $this->previous;
    }

    public function setPrevious(?FHIRReference $value): self
    {
        $this->previous = $value;

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getProblem(): array
    {
        return $this->problem;
    }

    public function setProblem(?FHIRReference ...$value): self
    {
        $this->problem = array_filter($value);

        return $this;
    }

    public function addProblem(?FHIRReference ...$value): self
    {
        $this->problem = array_filter(array_merge($this->problem, $value));

        return $this;
    }

    /**
     * @return FHIRClinicalImpressionInvestigation[]
     */
    public function getInvestigation(): array
    {
        return $this->investigation;
    }

    public function setInvestigation(?FHIRClinicalImpressionInvestigation ...$value): self
    {
        $this->investigation = array_filter($value);

        return $this;
    }

    public function addInvestigation(?FHIRClinicalImpressionInvestigation ...$value): self
    {
        $this->investigation = array_filter(array_merge($this->investigation, $value));

        return $this;
    }

    /**
     * @return FHIRUri[]
     */
    public function getProtocol(): array
    {
        return $this->protocol;
    }

    public function setProtocol(string|FHIRUri|null ...$value): self
    {
        $this->protocol = [];
        $this->addProtocol(...$value);

        return $this;
    }

    public function addProtocol(string|FHIRUri|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUri())->setValue($v) : $v, $value);

        $this->protocol = array_filter(array_merge($this->protocol, $values));

        return $this;
    }

    public function getSummary(): ?FHIRString
    {
        return $this->summary;
    }

    public function setSummary(string|FHIRString|null $value): self
    {
        $this->summary = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRClinicalImpressionFinding[]
     */
    public function getFinding(): array
    {
        return $this->finding;
    }

    public function setFinding(?FHIRClinicalImpressionFinding ...$value): self
    {
        $this->finding = array_filter($value);

        return $this;
    }

    public function addFinding(?FHIRClinicalImpressionFinding ...$value): self
    {
        $this->finding = array_filter(array_merge($this->finding, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getPrognosisCodeableConcept(): array
    {
        return $this->prognosisCodeableConcept;
    }

    public function setPrognosisCodeableConcept(?FHIRCodeableConcept ...$value): self
    {
        $this->prognosisCodeableConcept = array_filter($value);

        return $this;
    }

    public function addPrognosisCodeableConcept(?FHIRCodeableConcept ...$value): self
    {
        $this->prognosisCodeableConcept = array_filter(array_merge($this->prognosisCodeableConcept, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPrognosisReference(): array
    {
        return $this->prognosisReference;
    }

    public function setPrognosisReference(?FHIRReference ...$value): self
    {
        $this->prognosisReference = array_filter($value);

        return $this;
    }

    public function addPrognosisReference(?FHIRReference ...$value): self
    {
        $this->prognosisReference = array_filter(array_merge($this->prognosisReference, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getAction(): array
    {
        return $this->action;
    }

    public function setAction(?FHIRReference ...$value): self
    {
        $this->action = array_filter($value);

        return $this;
    }

    public function addAction(?FHIRReference ...$value): self
    {
        $this->action = array_filter(array_merge($this->action, $value));

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->note;
    }

    public function setNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter($value);

        return $this;
    }

    public function addNote(?FHIRAnnotation ...$value): self
    {
        $this->note = array_filter(array_merge($this->note, $value));

        return $this;
    }
}
