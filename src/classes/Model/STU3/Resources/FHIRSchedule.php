<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Schedule Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRScheduleInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRReference;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRSchedule extends FHIRDomainResource implements FHIRScheduleInterface
{
    public const RESOURCE_NAME = 'Schedule';

    /** @var FHIRIdentifier[] */
    protected array $identifier = [];
    protected ?FHIRBoolean $active = null;
    protected ?FHIRCodeableConcept $serviceCategory = null;

    /** @var FHIRCodeableConcept[] */
    protected array $serviceType = [];

    /** @var FHIRCodeableConcept[] */
    protected array $specialty = [];

    /** @var FHIRReference[] */
    protected array $actor = [];
    protected ?FHIRPeriod $planningHorizon = null;
    protected ?FHIRString $comment = null;

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter($value);

        return $this;
    }

    public function addIdentifier(?FHIRIdentifier ...$value): self
    {
        $this->identifier = array_filter(array_merge($this->identifier, $value));

        return $this;
    }

    public function getActive(): ?FHIRBoolean
    {
        return $this->active;
    }

    public function setActive(bool|FHIRBoolean|null $value): self
    {
        $this->active = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getServiceCategory(): ?FHIRCodeableConcept
    {
        return $this->serviceCategory;
    }

    public function setServiceCategory(?FHIRCodeableConcept $value): self
    {
        $this->serviceCategory = $value;

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getServiceType(): array
    {
        return $this->serviceType;
    }

    public function setServiceType(?FHIRCodeableConcept ...$value): self
    {
        $this->serviceType = array_filter($value);

        return $this;
    }

    public function addServiceType(?FHIRCodeableConcept ...$value): self
    {
        $this->serviceType = array_filter(array_merge($this->serviceType, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSpecialty(): array
    {
        return $this->specialty;
    }

    public function setSpecialty(?FHIRCodeableConcept ...$value): self
    {
        $this->specialty = array_filter($value);

        return $this;
    }

    public function addSpecialty(?FHIRCodeableConcept ...$value): self
    {
        $this->specialty = array_filter(array_merge($this->specialty, $value));

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getActor(): array
    {
        return $this->actor;
    }

    public function setActor(?FHIRReference ...$value): self
    {
        $this->actor = array_filter($value);

        return $this;
    }

    public function addActor(?FHIRReference ...$value): self
    {
        $this->actor = array_filter(array_merge($this->actor, $value));

        return $this;
    }

    public function getPlanningHorizon(): ?FHIRPeriod
    {
        return $this->planningHorizon;
    }

    public function setPlanningHorizon(?FHIRPeriod $value): self
    {
        $this->planningHorizon = $value;

        return $this;
    }

    public function getComment(): ?FHIRString
    {
        return $this->comment;
    }

    public function setComment(string|FHIRString|null $value): self
    {
        $this->comment = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
