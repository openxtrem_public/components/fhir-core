<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ImplementationGuide Resource
 */

namespace Ox\Components\FHIRCore\Model\STU3\Resources;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRImplementationGuideInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRBoolean;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRId;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRImplementationGuideDependency;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRImplementationGuideGlobal;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRImplementationGuidePackage;
use Ox\Components\FHIRCore\Model\STU3\Resources\Backbone\FHIRImplementationGuidePage;

class FHIRImplementationGuide extends FHIRDomainResource implements FHIRImplementationGuideInterface
{
    public const RESOURCE_NAME = 'ImplementationGuide';

    protected ?FHIRUri $url = null;
    protected ?FHIRString $version = null;
    protected ?FHIRString $name = null;
    protected ?FHIRCode $status = null;
    protected ?FHIRBoolean $experimental = null;
    protected ?FHIRDateTime $date = null;
    protected ?FHIRString $publisher = null;

    /** @var FHIRContactDetail[] */
    protected array $contact = [];
    protected ?FHIRMarkdown $description = null;

    /** @var FHIRUsageContext[] */
    protected array $useContext = [];

    /** @var FHIRCodeableConcept[] */
    protected array $jurisdiction = [];
    protected ?FHIRMarkdown $copyright = null;
    protected ?FHIRId $fhirVersion = null;

    /** @var FHIRImplementationGuideDependency[] */
    protected array $dependency = [];

    /** @var FHIRImplementationGuidePackage[] */
    protected array $package = [];

    /** @var FHIRImplementationGuideGlobal[] */
    protected array $global = [];

    /** @var FHIRUri[] */
    protected array $binary = [];
    protected ?FHIRImplementationGuidePage $page = null;

    public function getUrl(): ?FHIRUri
    {
        return $this->url;
    }

    public function setUrl(string|FHIRUri|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getVersion(): ?FHIRString
    {
        return $this->version;
    }

    public function setVersion(string|FHIRString|null $value): self
    {
        $this->version = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getName(): ?FHIRString
    {
        return $this->name;
    }

    public function setName(string|FHIRString|null $value): self
    {
        $this->name = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getStatus(): ?FHIRCode
    {
        return $this->status;
    }

    public function setStatus(string|FHIRCode|null $value): self
    {
        $this->status = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }

    public function getExperimental(): ?FHIRBoolean
    {
        return $this->experimental;
    }

    public function setExperimental(bool|FHIRBoolean|null $value): self
    {
        $this->experimental = is_bool($value) ? (new FHIRBoolean())->setValue($value) : $value;

        return $this;
    }

    public function getDate(): ?FHIRDateTime
    {
        return $this->date;
    }

    public function setDate(string|FHIRDateTime|null $value): self
    {
        $this->date = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getPublisher(): ?FHIRString
    {
        return $this->publisher;
    }

    public function setPublisher(string|FHIRString|null $value): self
    {
        $this->publisher = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter($value);

        return $this;
    }

    public function addContact(?FHIRContactDetail ...$value): self
    {
        $this->contact = array_filter(array_merge($this->contact, $value));

        return $this;
    }

    public function getDescription(): ?FHIRMarkdown
    {
        return $this->description;
    }

    public function setDescription(string|FHIRMarkdown|null $value): self
    {
        $this->description = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUseContext(): array
    {
        return $this->useContext;
    }

    public function setUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter($value);

        return $this;
    }

    public function addUseContext(?FHIRUsageContext ...$value): self
    {
        $this->useContext = array_filter(array_merge($this->useContext, $value));

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getJurisdiction(): array
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter($value);

        return $this;
    }

    public function addJurisdiction(?FHIRCodeableConcept ...$value): self
    {
        $this->jurisdiction = array_filter(array_merge($this->jurisdiction, $value));

        return $this;
    }

    public function getCopyright(): ?FHIRMarkdown
    {
        return $this->copyright;
    }

    public function setCopyright(string|FHIRMarkdown|null $value): self
    {
        $this->copyright = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }

    public function getFhirVersion(): ?FHIRId
    {
        return $this->fhirVersion;
    }

    public function setFhirVersion(string|FHIRId|null $value): self
    {
        $this->fhirVersion = is_string($value) ? (new FHIRId())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRImplementationGuideDependency[]
     */
    public function getDependency(): array
    {
        return $this->dependency;
    }

    public function setDependency(?FHIRImplementationGuideDependency ...$value): self
    {
        $this->dependency = array_filter($value);

        return $this;
    }

    public function addDependency(?FHIRImplementationGuideDependency ...$value): self
    {
        $this->dependency = array_filter(array_merge($this->dependency, $value));

        return $this;
    }

    /**
     * @return FHIRImplementationGuidePackage[]
     */
    public function getPackage(): array
    {
        return $this->package;
    }

    public function setPackage(?FHIRImplementationGuidePackage ...$value): self
    {
        $this->package = array_filter($value);

        return $this;
    }

    public function addPackage(?FHIRImplementationGuidePackage ...$value): self
    {
        $this->package = array_filter(array_merge($this->package, $value));

        return $this;
    }

    /**
     * @return FHIRImplementationGuideGlobal[]
     */
    public function getGlobal(): array
    {
        return $this->global;
    }

    public function setGlobal(?FHIRImplementationGuideGlobal ...$value): self
    {
        $this->global = array_filter($value);

        return $this;
    }

    public function addGlobal(?FHIRImplementationGuideGlobal ...$value): self
    {
        $this->global = array_filter(array_merge($this->global, $value));

        return $this;
    }

    /**
     * @return FHIRUri[]
     */
    public function getBinary(): array
    {
        return $this->binary;
    }

    public function setBinary(string|FHIRUri|null ...$value): self
    {
        $this->binary = [];
        $this->addBinary(...$value);

        return $this;
    }

    public function addBinary(string|FHIRUri|null ...$value): self
    {
        $values = array_map(fn($v) => is_string($v) ? (new FHIRUri())->setValue($v) : $v, $value);

        $this->binary = array_filter(array_merge($this->binary, $values));

        return $this;
    }

    public function getPage(): ?FHIRImplementationGuidePage
    {
        return $this->page;
    }

    public function setPage(?FHIRImplementationGuidePage $value): self
    {
        $this->page = $value;

        return $this;
    }
}
