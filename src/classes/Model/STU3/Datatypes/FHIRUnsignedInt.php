<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR unsignedInt Primitive
 */

namespace Ox\Components\FHIRCore\Model\STU3\Datatypes;

use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRUnsignedIntInterface;

class FHIRUnsignedInt extends FHIRInteger implements FHIRUnsignedIntInterface
{
    public const RESOURCE_NAME = 'unsignedInt';
}
