<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR decimal Primitive
 */

namespace Ox\Components\FHIRCore\Model\STU3\Datatypes;

use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRDecimalInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRElement;

class FHIRDecimal extends FHIRElement implements FHIRDecimalInterface
{
    public const RESOURCE_NAME = 'decimal';

    protected ?float $value = null;

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): self
    {
        $this->value = $value;

        return $this;
    }
}
