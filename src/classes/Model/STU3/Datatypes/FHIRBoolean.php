<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR boolean Primitive
 */

namespace Ox\Components\FHIRCore\Model\STU3\Datatypes;

use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRBooleanInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRElement;

class FHIRBoolean extends FHIRElement implements FHIRBooleanInterface
{
    public const RESOURCE_NAME = 'boolean';

    protected ?bool $value = null;

    public function getValue(): ?bool
    {
        return $this->value;
    }

    public function setValue(?bool $value): self
    {
        $this->value = $value;

        return $this;
    }
}
