<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CodeableConcept Complex
 */

namespace Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRCodeableConceptInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRCodeableConcept extends FHIRElement implements FHIRCodeableConceptInterface
{
    public const RESOURCE_NAME = 'CodeableConcept';

    /** @var FHIRCoding[] */
    protected array $coding = [];
    protected ?FHIRString $text = null;

    /**
     * @return FHIRCoding[]
     */
    public function getCoding(): array
    {
        return $this->coding;
    }

    public function setCoding(?FHIRCoding ...$value): self
    {
        $this->coding = array_filter($value);

        return $this;
    }

    public function addCoding(?FHIRCoding ...$value): self
    {
        $this->coding = array_filter(array_merge($this->coding, $value));

        return $this;
    }

    public function getText(): ?FHIRString
    {
        return $this->text;
    }

    public function setText(string|FHIRString|null $value): self
    {
        $this->text = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function searchCoding(string $system, ?string $code = null): ?FHIRCoding
    {
        foreach ($this->coding as $_coding) {
            if ($code && $_coding->isMatch($system, $code)) {
                return $_coding;
            }

            if (!$code && $_coding->getSystem()->isMatch($system)) {
                return $_coding;
            }
        }

        return null;
    }
}
