<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR SampledData Complex
 */

namespace Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRSampledDataInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDecimal;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRPositiveInt;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRSampledData extends FHIRElement implements FHIRSampledDataInterface
{
    public const RESOURCE_NAME = 'SampledData';

    protected ?FHIRQuantity $origin = null;
    protected ?FHIRDecimal $period = null;
    protected ?FHIRDecimal $factor = null;
    protected ?FHIRDecimal $lowerLimit = null;
    protected ?FHIRDecimal $upperLimit = null;
    protected ?FHIRPositiveInt $dimensions = null;
    protected ?FHIRString $data = null;

    public function getOrigin(): ?FHIRQuantity
    {
        return $this->origin;
    }

    public function setOrigin(?FHIRQuantity $value): self
    {
        $this->origin = $value;

        return $this;
    }

    public function getPeriod(): ?FHIRDecimal
    {
        return $this->period;
    }

    public function setPeriod(float|FHIRDecimal|null $value): self
    {
        $this->period = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getFactor(): ?FHIRDecimal
    {
        return $this->factor;
    }

    public function setFactor(float|FHIRDecimal|null $value): self
    {
        $this->factor = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getLowerLimit(): ?FHIRDecimal
    {
        return $this->lowerLimit;
    }

    public function setLowerLimit(float|FHIRDecimal|null $value): self
    {
        $this->lowerLimit = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getUpperLimit(): ?FHIRDecimal
    {
        return $this->upperLimit;
    }

    public function setUpperLimit(float|FHIRDecimal|null $value): self
    {
        $this->upperLimit = is_float($value) ? (new FHIRDecimal())->setValue($value) : $value;

        return $this;
    }

    public function getDimensions(): ?FHIRPositiveInt
    {
        return $this->dimensions;
    }

    public function setDimensions(int|FHIRPositiveInt|null $value): self
    {
        $this->dimensions = is_int($value) ? (new FHIRPositiveInt())->setValue($value) : $value;

        return $this;
    }

    public function getData(): ?FHIRString
    {
        return $this->data;
    }

    public function setData(string|FHIRString|null $value): self
    {
        $this->data = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
