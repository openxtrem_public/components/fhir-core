<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR MarketingStatus Complex
 */

namespace Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRMarketingStatusInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;

class FHIRMarketingStatus extends FHIRBackboneElement implements FHIRMarketingStatusInterface
{
    public const RESOURCE_NAME = 'MarketingStatus';

    protected ?FHIRCodeableConcept $country = null;
    protected ?FHIRCodeableConcept $jurisdiction = null;
    protected ?FHIRCodeableConcept $status = null;
    protected ?FHIRPeriod $dateRange = null;
    protected ?FHIRDateTime $restoreDate = null;

    public function getCountry(): ?FHIRCodeableConcept
    {
        return $this->country;
    }

    public function setCountry(?FHIRCodeableConcept $value): self
    {
        $this->country = $value;

        return $this;
    }

    public function getJurisdiction(): ?FHIRCodeableConcept
    {
        return $this->jurisdiction;
    }

    public function setJurisdiction(?FHIRCodeableConcept $value): self
    {
        $this->jurisdiction = $value;

        return $this;
    }

    public function getStatus(): ?FHIRCodeableConcept
    {
        return $this->status;
    }

    public function setStatus(?FHIRCodeableConcept $value): self
    {
        $this->status = $value;

        return $this;
    }

    public function getDateRange(): ?FHIRPeriod
    {
        return $this->dateRange;
    }

    public function setDateRange(?FHIRPeriod $value): self
    {
        $this->dateRange = $value;

        return $this;
    }

    public function getRestoreDate(): ?FHIRDateTime
    {
        return $this->restoreDate;
    }

    public function setRestoreDate(string|FHIRDateTime|null $value): self
    {
        $this->restoreDate = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }
}
