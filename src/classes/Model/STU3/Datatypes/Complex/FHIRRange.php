<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Range Complex
 */

namespace Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRRangeInterface;

class FHIRRange extends FHIRElement implements FHIRRangeInterface
{
    public const RESOURCE_NAME = 'Range';

    protected ?FHIRQuantity $low = null;
    protected ?FHIRQuantity $high = null;

    public function getLow(): ?FHIRQuantity
    {
        return $this->low;
    }

    public function setLow(?FHIRQuantity $value): self
    {
        $this->low = $value;

        return $this;
    }

    public function getHigh(): ?FHIRQuantity
    {
        return $this->high;
    }

    public function setHigh(?FHIRQuantity $value): self
    {
        $this->high = $value;

        return $this;
    }
}
