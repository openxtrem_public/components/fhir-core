<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Population Complex
 */

namespace Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRPopulationInterface;

class FHIRPopulation extends FHIRBackboneElement implements FHIRPopulationInterface
{
    public const RESOURCE_NAME = 'Population';

    protected FHIRRange|FHIRCodeableConcept|null $age = null;
    protected ?FHIRCodeableConcept $gender = null;
    protected ?FHIRCodeableConcept $race = null;
    protected ?FHIRCodeableConcept $physiologicalCondition = null;

    public function getAge(): FHIRRange|FHIRCodeableConcept|null
    {
        return $this->age;
    }

    public function setAge(FHIRRange|FHIRCodeableConcept|null $value): self
    {
        $this->age = $value;

        return $this;
    }

    public function getGender(): ?FHIRCodeableConcept
    {
        return $this->gender;
    }

    public function setGender(?FHIRCodeableConcept $value): self
    {
        $this->gender = $value;

        return $this;
    }

    public function getRace(): ?FHIRCodeableConcept
    {
        return $this->race;
    }

    public function setRace(?FHIRCodeableConcept $value): self
    {
        $this->race = $value;

        return $this;
    }

    public function getPhysiologicalCondition(): ?FHIRCodeableConcept
    {
        return $this->physiologicalCondition;
    }

    public function setPhysiologicalCondition(?FHIRCodeableConcept $value): self
    {
        $this->physiologicalCondition = $value;

        return $this;
    }
}
