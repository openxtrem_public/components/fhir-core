<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR CodeableReference Complex
 */

namespace Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRCodeableReferenceInterface;

class FHIRCodeableReference extends FHIRElement implements FHIRCodeableReferenceInterface
{
    public const RESOURCE_NAME = 'CodeableReference';

    protected ?FHIRCodeableConcept $concept = null;
    protected ?FHIRReference $reference = null;

    public function getConcept(): ?FHIRCodeableConcept
    {
        return $this->concept;
    }

    public function setConcept(?FHIRCodeableConcept $value): self
    {
        $this->concept = $value;

        return $this;
    }

    public function getReference(): ?FHIRReference
    {
        return $this->reference;
    }

    public function setReference(?FHIRReference $value): self
    {
        $this->reference = $value;

        return $this;
    }
}
