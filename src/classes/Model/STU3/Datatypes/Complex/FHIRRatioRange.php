<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR RatioRange Complex
 */

namespace Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRRatioRangeInterface;

class FHIRRatioRange extends FHIRElement implements FHIRRatioRangeInterface
{
    public const RESOURCE_NAME = 'RatioRange';

    protected ?FHIRQuantity $lowNumerator = null;
    protected ?FHIRQuantity $highNumerator = null;
    protected ?FHIRQuantity $denominator = null;

    public function getLowNumerator(): ?FHIRQuantity
    {
        return $this->lowNumerator;
    }

    public function setLowNumerator(?FHIRQuantity $value): self
    {
        $this->lowNumerator = $value;

        return $this;
    }

    public function getHighNumerator(): ?FHIRQuantity
    {
        return $this->highNumerator;
    }

    public function setHighNumerator(?FHIRQuantity $value): self
    {
        $this->highNumerator = $value;

        return $this;
    }

    public function getDenominator(): ?FHIRQuantity
    {
        return $this->denominator;
    }

    public function setDenominator(?FHIRQuantity $value): self
    {
        $this->denominator = $value;

        return $this;
    }
}
