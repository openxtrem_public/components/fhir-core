<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Reference Complex
 */

namespace Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRReferenceInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRUri;

class FHIRReference extends FHIRElement implements FHIRReferenceInterface
{
    public const RESOURCE_NAME = 'Reference';

    protected ?FHIRString $reference = null;
    protected ?FHIRUri $type = null;
    protected ?FHIRIdentifier $identifier = null;
    protected ?FHIRString $display = null;

    public function getReference(): ?FHIRString
    {
        return $this->reference;
    }

    public function setReference(string|FHIRString|null $value): self
    {
        $this->reference = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getType(): ?FHIRUri
    {
        return $this->type;
    }

    public function setType(string|FHIRUri|null $value): self
    {
        $this->type = is_string($value) ? (new FHIRUri())->setValue($value) : $value;

        return $this;
    }

    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->identifier;
    }

    public function setIdentifier(?FHIRIdentifier $value): self
    {
        $this->identifier = $value;

        return $this;
    }

    public function getDisplay(): ?FHIRString
    {
        return $this->display;
    }

    public function setDisplay(string|FHIRString|null $value): self
    {
        $this->display = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }
}
