<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Annotation Complex
 */

namespace Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRAnnotationInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRDateTime;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRMarkdown;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRAnnotation extends FHIRElement implements FHIRAnnotationInterface
{
    public const RESOURCE_NAME = 'Annotation';

    protected FHIRReference|FHIRString|null $author = null;
    protected ?FHIRDateTime $time = null;
    protected ?FHIRMarkdown $text = null;

    public function getAuthor(): FHIRReference|FHIRString|null
    {
        return $this->author;
    }

    public function setAuthor(FHIRReference|FHIRString|null $value): self
    {
        $this->author = $value;

        return $this;
    }

    public function getTime(): ?FHIRDateTime
    {
        return $this->time;
    }

    public function setTime(string|FHIRDateTime|null $value): self
    {
        $this->time = is_string($value) ? (new FHIRDateTime())->setValue($value) : $value;

        return $this;
    }

    public function getText(): ?FHIRMarkdown
    {
        return $this->text;
    }

    public function setText(string|FHIRMarkdown|null $value): self
    {
        $this->text = is_string($value) ? (new FHIRMarkdown())->setValue($value) : $value;

        return $this;
    }
}
