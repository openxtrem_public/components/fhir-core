<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DataRequirementCodeFilter Element
 */

namespace Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\Element;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\Element\FHIRDataRequirementCodeFilterInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRCoding;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCanonical;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRDataRequirementCodeFilter extends FHIRElement implements FHIRDataRequirementCodeFilterInterface
{
    public const RESOURCE_NAME = 'DataRequirement.codeFilter';

    protected ?FHIRString $path = null;
    protected ?FHIRString $searchParam = null;
    protected ?FHIRCanonical $valueSet = null;

    /** @var FHIRCoding[] */
    protected array $code = [];

    public function getPath(): ?FHIRString
    {
        return $this->path;
    }

    public function setPath(string|FHIRString|null $value): self
    {
        $this->path = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getSearchParam(): ?FHIRString
    {
        return $this->searchParam;
    }

    public function setSearchParam(string|FHIRString|null $value): self
    {
        $this->searchParam = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getValueSet(): ?FHIRCanonical
    {
        return $this->valueSet;
    }

    public function setValueSet(string|FHIRCanonical|null $value): self
    {
        $this->valueSet = is_string($value) ? (new FHIRCanonical())->setValue($value) : $value;

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getCode(): array
    {
        return $this->code;
    }

    public function setCode(?FHIRCoding ...$value): self
    {
        $this->code = array_filter($value);

        return $this;
    }

    public function addCode(?FHIRCoding ...$value): self
    {
        $this->code = array_filter(array_merge($this->code, $value));

        return $this;
    }
}
