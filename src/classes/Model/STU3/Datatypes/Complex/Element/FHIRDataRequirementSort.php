<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR DataRequirementSort Element
 */

namespace Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\Element;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\Element\FHIRDataRequirementSortInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRCode;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRDataRequirementSort extends FHIRElement implements FHIRDataRequirementSortInterface
{
    public const RESOURCE_NAME = 'DataRequirement.sort';

    protected ?FHIRString $path = null;
    protected ?FHIRCode $direction = null;

    public function getPath(): ?FHIRString
    {
        return $this->path;
    }

    public function setPath(string|FHIRString|null $value): self
    {
        $this->path = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getDirection(): ?FHIRCode
    {
        return $this->direction;
    }

    public function setDirection(string|FHIRCode|null $value): self
    {
        $this->direction = is_string($value) ? (new FHIRCode())->setValue($value) : $value;

        return $this;
    }
}
