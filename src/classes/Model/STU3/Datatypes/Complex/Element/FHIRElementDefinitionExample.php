<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR ElementDefinitionExample Element
 */

namespace Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\Element;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\Element\FHIRElementDefinitionExampleInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex\FHIRElement;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRElementDefinitionExample extends FHIRElement implements FHIRElementDefinitionExampleInterface
{
    public const RESOURCE_NAME = 'ElementDefinition.example';

    protected ?FHIRString $label = null;
    protected ?FHIRElement $value = null;

    public function getLabel(): ?FHIRString
    {
        return $this->label;
    }

    public function setLabel(string|FHIRString|null $value): self
    {
        $this->label = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getValue(): ?FHIRElement
    {
        return $this->value;
    }

    public function setValue(?FHIRElement $value): self
    {
        $this->value = $value;

        return $this;
    }
}
