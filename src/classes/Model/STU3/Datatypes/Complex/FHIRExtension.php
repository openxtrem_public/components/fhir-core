<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR Extension Complex
 */

namespace Ox\Components\FHIRCore\Model\STU3\Datatypes\Complex;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRExtensionInterface;
use Ox\Components\FHIRCore\Model\STU3\Datatypes\FHIRString;

class FHIRExtension extends FHIRElement implements FHIRExtensionInterface
{
    public const RESOURCE_NAME = 'Extension';

    protected ?FHIRString $url = null;
    protected ?FHIRElement $value = null;

    public function getUrl(): ?FHIRString
    {
        return $this->url;
    }

    public function setUrl(string|FHIRString|null $value): self
    {
        $this->url = is_string($value) ? (new FHIRString())->setValue($value) : $value;

        return $this;
    }

    public function getValue(): ?FHIRElement
    {
        return $this->value;
    }

    public function setValue(?FHIRElement $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getFromUrls(string ...$urls): ?self
    {
        $url = reset($urls);
        if (!$this->url || $this->url->getValue() !== $url) {
            return null;
        }

        if ($other_urls = array_slice($urls, 1)) {
            return $this->getExtensionFromUrls(...$other_urls);
        } else {
          return $this;
        }
    }
}
