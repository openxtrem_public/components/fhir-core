<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 * FHIR id Primitive
 */

namespace Ox\Components\FHIRCore\Model\STU3\Datatypes;

use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRIdInterface;

class FHIRId extends FHIRString implements FHIRIdInterface
{
    public const RESOURCE_NAME = 'id';
}
