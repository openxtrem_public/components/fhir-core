<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Parser;

use DOMElement;
use DOMNode;
use Ox\Components\FHIRCore\Definition\Field;
use Ox\Components\FHIRCore\Definition\ResourceDefinition;
use Ox\Components\FHIRCore\Enum\FHIRVersion;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRBaseInterface;
use Ox\Components\FHIRCore\Interfaces\Resources\FHIRResourceInterface;
use Ox\Components\FHIRCore\Model\R4\StructureDefinition;

/**
 * Description
 */
abstract class AbstractParser implements ParserInterface
{
    private const DEFINITIONS = [
        FHIRVersion::STU3->name => \Ox\Components\FHIRCore\Model\STU3\StructureDefinition::class,
        FHIRVersion::R4->name   => StructureDefinition::class,
        FHIRVersion::R4B->name  => \Ox\Components\FHIRCore\Model\R4B\StructureDefinition::class,
        FHIRVersion::R5->name   => \Ox\Components\FHIRCore\Model\R5\StructureDefinition::class,
    ];

    public string                    $resourceName;
    protected ?FHIRResourceInterface $resource = null;
    /** @var class-string $structure_definition */
    protected string $structure_definition;

    public function __construct(protected FHIRVersion $FHIR_version)
    {
        $this->structure_definition = self::DEFINITIONS[$FHIR_version->name];
    }

    protected function findResourceObject(?string $objectName): ?FHIRResourceInterface
    {
        $resourceDef = $this->structure_definition::DEFINITIONS[$objectName] ?? null;
        if (is_null($resourceDef)) {
            return null;
        }
        $className = $resourceDef['object'];

        return new $className();
    }

    /**
     * @param DOMElement|array  $element
     * @param FHIRBaseInterface $class
     *
     */
    abstract protected function parseElement(mixed $element, FHIRBaseInterface $class): void;

    /**
     * @param array  $fields
     * @param string $fieldName
     *
     * @return Field
     */
    protected function findField(array $fields, string $fieldName): ?Field
    {
        // In case of multiple type, register possible fields with typeNames. ex : value -> valueString
        $searching_fields = $fields;
        foreach ($fields as $name => $field) {
            $fieldTypes = $field->getTypes();
            if (sizeof($fieldTypes) > 1) {
                foreach (array_keys($fieldTypes) as $type) {
                    $searching_fields[$name . ucfirst($type)] = $field;
                }
            }
        }

        // TODO : Ajouter une option pour throw une erreur ou non
        if (!key_exists($fieldName, $searching_fields)) {
            return null;
        }

        return $searching_fields[$fieldName];
    }

    /**
     * @param Field  $elementField
     * @param string $fieldName
     *
     * @return string
     */
    protected function findType(Field $elementField, string $fieldName): string
    {
        $types = $elementField->getTypes();
        if (sizeof($types) === 1) {
            $type = array_keys($types)[0];
        } else {
            $type = '';
            foreach ($types as $typeName => $final) {
                if ($elementField->getLabel() . ucfirst($typeName) === $fieldName) {
                    $type = $typeName;
                }
            }
        }

        return $type;
    }

    private static array $primitivesTypes = [
        "http://hl7.org/fhirpath/System.Boolean"  => 'bool',
        "http://hl7.org/fhirpath/System.Date"     => 'string',
        "http://hl7.org/fhirpath/System.Time"     => 'string',
        'http://hl7.org/fhirpath/System.String'   => 'string',
        'http://hl7.org/fhirpath/System.Decimal'  => 'float',
        'http://hl7.org/fhirpath/System.Integer'  => 'int',
        'http://hl7.org/fhirpath/System.DateTime' => 'string',
    ];

    /**
     * @param FHIRBaseInterface $class
     * @param string            $type
     * @param string            $value
     * @param string            $valueName
     *
     * @return void
     */
    protected function addFinal(
        FHIRBaseInterface $class,
        string            $type,
        string            $value,
        string            $valueName = 'value'
    ): void {
        $type = self::$primitivesTypes[$type] ?? '';
        if ($type === 'bool') {
            $value = (($value === 'true') || ($value === '1'));
        }
        if ($type === 'int') {
            $value = intval($value);
        }
        if ($type === 'float') {
            $value = floatval($value);
        }

        $setter = 'set' . ucfirst($valueName);
        $class->$setter($value);
    }

    /**
     * @param DOMNode            $element
     * @param ResourceDefinition $resourceDefinitio
     *
     * @return FHIRBaseInterface
     */
    protected function registerClass(Field $elementField, string $type): FHIRBaseInterface
    {
        $resourceDef = $this->structure_definition::DEFINITIONS[$type] ??
            $this->structure_definition::DEFINITIONS[$elementField->getId()];

        $className = $resourceDef['object'];

        return new $className();
    }
}
