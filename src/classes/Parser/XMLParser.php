<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Parser;

use DOMDocument;
use DOMElement;
use DOMXPath;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRBaseInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRXhtmlInterface;
use Ox\Components\FHIRCore\Interfaces\Resources\FHIRResourceInterface;
use Ox\Components\FHIRCore\Parser\Exception\NoElementException;
use Ox\Components\FHIRCore\Parser\Exception\ParserException;

/**
 * Description
 */
class XMLParser extends AbstractParser implements ParserInterface
{
    public DOMXPath     $xpath;
    private DOMDocument $doc;

    /**
     *
     * @throws NoElementException
     * @throws ParserException
     */
    public function parseResource(string $raw): FHIRResourceInterface
    {
        $doc       = new DOMDocument();
        $this->doc = $doc;
        $doc->loadXML($raw);
        $this->xpath = new DOMXPath($doc);
        $element     = $doc->firstChild;
        while (($element != null) && (($element->nodeName === "#text") || ($element->nodeName === "#comment"))) {
            $element = $element->nextSibling;
        }

        if ($element === null) {
            throw new NoElementException();
        }

        $class = $this->findResourceObject($element->nodeName);
        if ($class === null) {
            throw new ParserException();
        }

        $this->resourceName = $element->nodeName;

        $this->parseElement($element, $class);

        return $this->resource = $class;
    }

    /**
     * @param DOMElement $element
     *
     * @throws NoElementException|ParserException
     */
    protected function parseElement(mixed $element, FHIRBaseInterface $class): void
    {
        if ($class instanceof FHIRXhtmlInterface) {
            /** @var FHIRXhtmlInterface $class */
            $class->setValue($this->doc->saveHTML($element));

            return;
        }
        $fields = $this->structure_definition::getResourceDefinition($class)->getFields();

        $attributes = $element->attributes;
        for ($i = 0; $i < $attributes->count(); $i++) {
            $attribute = $attributes->item($i);

            $elementField = $this->findField($fields, $attribute->nodeName);
            if (!($elementField)) {
                // Field not found
                continue;
            }

            $type = $this->findType($elementField, $attribute->nodeName);

            $this->addFinal($class, $type, $attribute->nodeValue, $attribute->nodeName);
        }

        $child = $element->firstChild;
        while ($child != null) {
            if (($child->nodeName === "#text") || ($child->nodeName === "#comment")) {
                // skip
                $child = $child->nextSibling;
                continue;
            }

            if (($child->nodeName === 'id') && ($element->nodeName === $this->resourceName)) {
                // IDs of xml resources are not in attributes
                $class->setID($child->attributes->getNamedItem("value")->nodeValue);
                $child = $child->nextSibling;
                continue;
            }

            $elementField = $this->findField($fields, $child->nodeName);
            if (!($elementField)) {
                // Field not found
                $child = $child->nextSibling;
                continue;
            }

            $type = $this->findType($elementField, $child->nodeName);

            if ($type === 'Resource') {
                $subChild = $child->firstChild;
                while ($subChild != null) {
                    if (($subChild->nodeName === "#text") || ($subChild->nodeName === "#comment")) {
                        // skip
                        $subChild = $subChild->nextSibling;
                        continue;
                    }

                    $subParser = new static($this->FHIR_version);
                    $newClass  = $subParser->parseResource($this->doc->saveXML($subChild));

                    if ($elementField->isUnique()) {
                        $class->{'set' . ucfirst($elementField->getLabel())}($newClass);
                    } else {
                        $class->{'add' . ucfirst($elementField->getLabel())}($newClass);
                    }

                    $subChild = $subChild->nextSibling;
                }
                $child = $child->nextSibling;
                continue;
            }


            $newClass = $this->registerClass($elementField, $type);
            $this->parseElement($child, $newClass);

            // create setter or adder if element is multiple
            if ($elementField->isUnique()) {
                $setter = 'set' . ucfirst($elementField->getLabel());
            } else {
                $setter = 'add' . ucfirst($elementField->getLabel());
            }
            $class->$setter($newClass);

            $child = $child->nextSibling;
        }
    }
}
