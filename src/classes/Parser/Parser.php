<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Parser;

use Ox\Components\FHIRCore\Enum\FHIRVersion;
use Ox\Components\FHIRCore\Interfaces\Resources\FHIRResourceInterface;
use Ox\Components\FHIRCore\Parser\Exception\NoElementException;
use Ox\Components\FHIRCore\Parser\Exception\ParserException;

/**
 * Static class that use XMLParser or JSONParser
 */
class Parser
{
    /**
     * Determine format type and parse FHIR resource.
     *  If the format is known, please use `JSONParser` or `XMLParser`.
     * @throws ParserException
     */
    public static function parse(string $raw, FHIRVersion $FHIR_version): FHIRResourceInterface
    {
        if (!$raw) {
            throw new ParserException('Empty content.');
        }

        if (str_starts_with($raw, '{')) {
            $parser = new JSONParser($FHIR_version);
        } elseif (str_starts_with($raw, '<?xml') || str_starts_with($raw, '<')) {
            $parser = new XMLParser($FHIR_version);
        } else {
            throw new ParserException('Unprocessable Data.');
        }

        try {
            return $parser->parseResource($raw);
        } catch (NoElementException) {
            throw new ParserException('Parse Failed.');
        }
    }
}
