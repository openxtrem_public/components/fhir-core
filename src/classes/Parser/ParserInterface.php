<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Parser;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRResourceInterface;
use Ox\Components\FHIRCore\Parser\Exception\ParserException;

interface ParserInterface
{
    /**
     * @throws ParserException
     */
    public function parseResource(string $raw): FHIRResourceInterface;
}
