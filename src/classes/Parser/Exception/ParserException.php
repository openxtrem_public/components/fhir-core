<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Parser\Exception;

use Exception;

/**
 * Description
 */
class ParserException extends Exception
{
    public static function dataUnprocessable(string $data): self
    {
        return new self("'$data' is not processable.");
    }
}
