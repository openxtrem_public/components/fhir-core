<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Parser;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRBaseInterface;
use Ox\Components\FHIRCore\Interfaces\Resources\FHIRResourceInterface;
use Ox\Components\FHIRCore\Parser\Exception\NoElementException;

/**
 * Description
 */
class JSONParser extends AbstractParser implements ParserInterface
{
    /**
     *
     * @throws NoElementException
     */
    public function parseResource(string $raw): FHIRResourceInterface
    {
        $element       = json_decode($raw, true);
        $resource_name = $element['resourceType'] ?? null;

        $class = $this->findResourceObject($resource_name);
        if ($class === null) {
            throw new NoElementException();
        }
        $this->resourceName = $resource_name;

        $this->parseElement($element, $class);

        return $this->resource = $class;
    }

    /**
     * @param array $element
     *
     * @throws NoElementException
     */
    protected function parseElement(mixed $element, FHIRBaseInterface $class): void
    {
        $fields = $this->structure_definition::getResourceDefinition($class)->getFields();
        foreach ($element as $fieldName => $value) {
            if ($fieldName === 'resourceType') {
                continue;
            }

            // Lonely prepended elements are not handled in other case
            $prepended = false;
            if (str_starts_with($fieldName, '_')) {
                if (array_key_exists(substr($fieldName, 1), $element)) {
                    // not prepended exists
                    continue;
                }
                $fieldName = substr($fieldName, 1);
                $prepended = true;
            }

            $elementField = $this->findField($fields, $fieldName);
            if (!($elementField)) {
                // Field not found
                continue;
            }

            $type = $this->findType($elementField, $fieldName);

            if ($elementField->isUnique()) {
                $setter = 'set' . ucfirst($elementField->getLabel());

                if ($type === 'Resource') {
                    $newClass = $this->parseResource(json_encode($value));
                    $class->$setter($newClass);
                    continue;
                }

                $newClass = $this->registerClass($elementField, $type);

                if (is_array($value) && !$prepended) {
                    $this->parseElement($value, $newClass);
                } else {
                    // if this is a last value (primitive)
                    if (!$prepended) {
                        $this->addFinal($newClass, $type, $value);
                    }
                    if (key_exists("_$fieldName", $element)) {
                        $this->parseElement($element["_$fieldName"], $newClass);
                    }
                }
                $class->$setter($newClass);
            } else {
                $setter = 'add' . ucfirst($elementField->getLabel());

                // Find prepend or create empty set
                if (key_exists("_$fieldName", $element)) {
                    $prepend = $element["_$fieldName"];
                    foreach ($prepend as &$p) {
                        if (is_null($p)) {
                            $p = [];
                        }
                    }
                } else {
                    $prepend = [];
                    foreach ($value as $ignored) {
                        $prepend[] = [];
                    }
                }

                for ($i = 0; $i < sizeof($value); $i++) {
                    if (!key_exists($i, $value)) {
                        // often case of non unique field created as unique field
                        continue;
                    }

                    if ($type === 'Resource') {
                        $newClass = $this->parseResource(json_encode($value[$i]));
                        $class->$setter($newClass);
                        continue;
                    }

                    $newClass = $this->registerClass($elementField, $type);
                    if (is_array($value[$i]) && !$prepended) {
                        $this->parseElement($value[$i], $newClass);
                    } else {
                        if (!is_null($value[$i]) && !$prepended) {
                            $this->addFinal($newClass, $type, $value[$i]);
                        }
                        // Parse prepend
                        $this->parseElement($prepend[$i], $newClass);
                    }
                    $class->$setter($newClass);
                }
            }
        }
    }
}
