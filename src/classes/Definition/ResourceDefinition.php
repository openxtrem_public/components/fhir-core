<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Definition;

/**
 * Description
 */
class ResourceDefinition
{
    /** @var Field[] $fields */
    private array               $fields = [];
    private string              $resourceName;
    private string              $objectName;
    private ?ResourceDefinition $parent;
    private string              $ID;

    public function __construct(string $name)
    {
        $this->resourceName = $name;
    }

    public function getParent(): ?ResourceDefinition
    {
        return $this->parent;
    }

    public function setParent(?ResourceDefinition $parent): ResourceDefinition
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return string
     */
    public function getObjectName(): string
    {
        return $this->objectName;
    }

    /**
     * @param string $objectName
     *
     * @return ResourceDefinition
     */
    public function setObjectName(string $objectName): ResourceDefinition
    {
        $this->objectName = $objectName;

        return $this;
    }

    /**
     * @return string
     */
    public function getID(): string
    {
        return $this->ID;
    }

    /**
     * @param string $ID
     *
     * @return ResourceDefinition
     */
    public function setID(string $ID): ResourceDefinition
    {
        $this->ID = $ID;

        return $this;
    }

    public function getResourceName(): string
    {
        return $this->resourceName;
    }

    public function addFields(Field ...$fields): self
    {
        foreach ($fields as $field) {
            $this->fields[$field->getLabel()] = $field;
        }

        return $this;
    }

    /**
     * @return Field[]
     */
    public function getFields(): array
    {
        return $this->fields;
    }
}
