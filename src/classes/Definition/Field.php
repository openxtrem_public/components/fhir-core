<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Definition;

/**
 * Description
 */
class Field
{
    private string $label;
    private string $id;
    /** @var array<string, bool> $types type => final */
    private array $types = [];
    private bool  $unique;
    private bool  $nullable;
    private bool  $summary;

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     *
     * @return Field
     */
    public function setLabel(string $label): Field
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return Field
     */
    public function setId(string $id): Field
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return array<string, bool>
     * type => final
     */
    public function getTypes(): array
    {
        return $this->types;
    }

    /**
     * @param array<string, bool> $types type => final
     *
     * @return Field
     */
    public function setTypes(array $types): Field
    {
        $this->types = $types;

        return $this;
    }

    /**
     * @return bool
     */
    public function isUnique(): bool
    {
        return $this->unique;
    }

    /**
     * @param bool $unique
     *
     * @return Field
     */
    public function setUnique(bool $unique): Field
    {
        $this->unique = $unique;

        return $this;
    }

    /**
     * @return bool
     */
    public function isNullable(): bool
    {
        return $this->nullable;
    }

    /**
     * @param bool $nullable
     *
     * @return Field
     */
    public function setNullable(bool $nullable): Field
    {
        $this->nullable = $nullable;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSummary(): bool
    {
        return $this->summary;
    }

    /**
     * @param bool $summary
     *
     * @return Field
     */
    public function setSummary(bool $summary): Field
    {
        $this->summary = $summary;

        return $this;
    }
}
