<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Enum;

/**
 * Description
 */
enum FHIRVersion: string
{
    case STU3 = 'STU3';
    case R4   = 'R4';
    case R4B  = 'R4B';
    case R5   = 'R5';
}
