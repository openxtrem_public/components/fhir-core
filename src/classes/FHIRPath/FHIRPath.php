<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath;

use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRPathException;
use Ox\Components\FHIRCore\FHIRPath\Expressions\ExpressionsInformation;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\BooleanLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\DecimalLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\IntegerLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\StringLiteral;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRBaseInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRBooleanInterface;

/**
 * Description
 */
class FHIRPath
{
    final public const DEFAULT_VARIABLES = [
        'sct'   => 'http://snomed.info/sct',
        'loinc' => 'http://loinc.org',
        'ucum'  => 'http://unitsofmeasure.org',
    ];
    private array $variables = [];

    public function __construct()
    {
        foreach (self::DEFAULT_VARIABLES as $key => $value) {
            $this->addEnvironmentVariable($key, $value);
        }
    }

    public function addEnvironmentVariable(string $name, mixed $value): self
    {
        if (is_string($value)) {
            $this->variables[$name] = (new StringLiteral())->setValue($value);
        } elseif (is_bool($value)) {
            $this->variables[$name] = (new BooleanLiteral())->setValue($value);
        } elseif (is_float($value) || is_double($value)) {
            $this->variables[$name] = (new DecimalLiteral())->setValue($value);
        } elseif (is_integer($value)) {
            $this->variables[$name] = (new IntegerLiteral())->setValue($value);
        } else {
            $this->variables[$name] = $value;
        }

        return $this;
    }

    public function removeEnvironmentVariable(string $name): self
    {
        unset($this->variables[$name]);

        return $this;
    }

    /**
     * @return FHIRBaseInterface[]
     * @throws FHIRPathException
     */
    public function apply(FHIRBaseInterface $resource, string $fhirPath): array
    {
        $info            = new ExpressionsInformation();
        $info->element   = $resource;
        $info->variables = [
            ...$this->variables,
            'resource'     => $resource,
            'rootResource' => $resource,
            'context'      => $resource,
        ];

        $fhirPathParser = new PathParser($fhirPath);
        $expression     = $fhirPathParser->getExpression();

        try {
            return $expression->getValue($info);
        } catch (Exception\FHIRLiteralFound|Exception\SystemLiteralFound) {
            throw new FHIRPathException('Unexpected error.');
        }
    }

    /**
     * @throws FHIRPathException
     */
    public function predicate(FHIRBaseInterface $resource, string $fhirPath): bool
    {
        $result = $this->apply($resource, $fhirPath);

        if (sizeof($result) != 1) {
            throw FHIRPathException::expectedSingleOutput(sizeof($result));
        }

        $unique = reset($result);
        if (!($unique instanceof FHIRBooleanInterface)) {
            throw FHIRPathException::expectedBooleanOutput($unique);
        }

        /** @var FHIRBooleanInterface $unique */
        return $unique->getValue();
    }

    /**
     * @throws FHIRPathException
     */
    public function placeValue(FHIRBaseInterface $resource, string $fhirPath, mixed $value): FHIRBaseInterface
    {
        $info            = new ExpressionsInformation();
        $info->element   = $resource;
        $info->variables = [
            ...$this->variables,
            'resource'     => $resource,
            'rootResource' => $resource,
            'context'      => $resource,
        ];

        $fhirPathParser = new PathParser($fhirPath);
        $expression     = $fhirPathParser->getExpression();

        if ($value instanceof FHIRBaseInterface) {
            $expression->getTargetToWrite($info, null, null, $value);
        } else {
            $elements = $expression->getTargetToWrite($info);

            foreach ($elements as $element) {
                $element->setValue($value);
            }
        }

        return $resource;
    }
}
