<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions\Literals;

use Ox\Components\FHIRCore\Definition\Field;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRDateInterface;

class DateLiteral extends AbstractLiteral implements LiteralInterface, FHIRDateInterface
{
    final public const LITERAL_NAME  = 'Date';
    final public const RESOURCE_NAME = 'date';

    protected function getFields(): array
    {
        return [
            (new Field())
                ->setLabel('value')
                ->setUnique(true)
                ->setNullable(true)
                ->setTypes(["http://hl7.org/fhirpath/System.Date" => true])
        ];
    }
}
