<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions\Literals;

use Ox\Components\FHIRCore\Definition\Field;
use Ox\Components\FHIRCore\Definition\ResourceDefinition;
use Ox\Components\FHIRCore\Interfaces\StructureDefinitionInterface;

abstract class AbstractLiteral implements LiteralInterface
{
    public bool $isFhirPath = true;

    private mixed $value = null;

    public function getValue(): mixed
    {
        return $this->value;
    }

    public function setValue(mixed $value): AbstractLiteral
    {
        $this->value = $value;

        return $this;
    }

    public static function getFhirStructureDefinition(): string
    {
        return StructureDefinitionInterface::class;
    }

    public function getFhirResourceDefinition(): ResourceDefinition
    {
        return (new ResourceDefinition('ClassInfo'))
            ->setObjectName(self::class)
            ->addFields(...$this->getFields());
    }

    abstract protected function getFields(): array;
}
