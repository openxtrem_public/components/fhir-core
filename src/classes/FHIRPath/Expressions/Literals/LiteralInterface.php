<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions\Literals;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRElementInterface;

interface LiteralInterface extends FHIRElementInterface
{
    public const LITERAL_NAME = '';
}
