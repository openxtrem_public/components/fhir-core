<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions\Literals;

class NotAValidLiteral extends AbstractLiteral implements LiteralInterface
{
    final public const LITERAL_NAME  = 'Invalid - ShouldNotBeFound';
    final public const RESOURCE_NAME = 'Invalid - ShouldNotBeFound';

    protected function getFields(): array
    {
        return [];
    }
}
