<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions\Literals;

use Ox\Components\FHIRCore\Definition\Field;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRBooleanInterface;

class BooleanLiteral extends AbstractLiteral implements LiteralInterface, FHIRBooleanInterface
{
    final public const LITERAL_NAME  = 'Boolean';
    final public const RESOURCE_NAME = 'boolean';

    protected function getFields(): array
    {
        return [
            (new Field())
                ->setLabel('value')
                ->setUnique(true)
                ->setNullable(true)
                ->setTypes(["http://hl7.org/fhirpath/System.Boolean" => true])
        ];
    }
}
