<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions\Literals;

use Ox\Components\FHIRCore\Definition\Field;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRQuantityInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRDecimalInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRStringInterface;

class QuantityLiteral extends AbstractLiteral implements LiteralInterface, FHIRQuantityInterface
{
    final public const LITERAL_NAME  = 'Quantity';
    final public const RESOURCE_NAME = 'quantity';

    public bool $isFhirPath = true;

    protected ?FHIRDecimalInterface $value = null;
    protected ?FHIRStringInterface  $unit  = null;

    public function getValue(): ?FHIRDecimalInterface
    {
        return $this->value;
    }

    /**
     * @param float|FHIRDecimalInterface|null $value
     *
     * @return $this
     */
    public function setValue(mixed $value): self
    {
        $this->value = ($value instanceof FHIRDecimalInterface) ? $value : (new DecimalLiteral())->setValue($value);

        return $this;
    }

    public function getUnit(): ?FHIRStringInterface
    {
        return $this->unit;
    }

    public function setUnit(string|FHIRStringInterface|null $value): self
    {
        $this->unit = is_string($value) ? (new StringLiteral())->setValue($value) : $value;

        return $this;
    }

    protected function getFields(): array
    {
        return [
            (new Field())
                ->setLabel('value')
                ->setUnique(true)
                ->setNullable(true)
                ->setTypes(["decimal" => false]),
            (new Field())
                ->setLabel('unit')
                ->setUnique(true)
                ->setNullable(true)
                ->setTypes(["string" => true])
        ];
    }
}
