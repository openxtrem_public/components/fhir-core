<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions\Literals;

use Ox\Components\FHIRCore\Definition\Field;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRDecimalInterface;

class DecimalLiteral extends AbstractLiteral implements LiteralInterface, FHIRDecimalInterface
{
    final public const LITERAL_NAME  = 'Decimal';
    final public const RESOURCE_NAME = 'decimal';

    private float $value;

    public function setValue(mixed $value): AbstractLiteral
    {
        $this->value = $value;

        return $this;
    }

    public function getValue(): float
    {
        return $this->value;
    }

    protected function getFields(): array
    {
        return [
            (new Field())
                ->setLabel('value')
                ->setUnique(true)
                ->setNullable(true)
                ->setTypes(["http://hl7.org/fhirpath/System.Decimal" => true])
        ];
    }
}
