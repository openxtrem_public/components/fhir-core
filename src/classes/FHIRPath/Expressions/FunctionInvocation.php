<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions;

use DateTime;
use DateTimeInterface;
use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRLiteralFound;
use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRPathException;
use Ox\Components\FHIRCore\FHIRPath\Exception\SystemLiteralFound;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Functions\Conversions;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Functions\FunctionHandler;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Functions\Math;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Functions\StringManipulation;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Functions\Types\Types;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\BooleanLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\DateLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\DateTimeLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\IntegerLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\TimeLiteral;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRBaseInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRBooleanInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRIntegerInterface;
use Ox\Components\FHIRCore\Utils\Comparisons;

class FunctionInvocation extends AbstractExpression
{
    private ?ExpressionInterface      $left;
    protected string                 $function;
    protected ExpressionInterface     $arguments;
    protected Comparisons            $helpers;
    protected DateTime               $now;
    protected ExpressionsInformation $info;

    public function __construct(
        ?ExpressionInterface $left,
        string               $function,
        ExpressionInterface  $arguments,
        Comparisons          $helpers = null
    ) {
        $this->left      = $left;
        $this->function  = $function;
        $this->arguments = $arguments;
        if (is_null($helpers)) {
            $this->helpers = new Comparisons();
        } else {
            $this->helpers = $helpers;
        }
    }

    final public const FUNCTIONS_HANDLERS = [
        StringManipulation::class,
        Conversions::class,
        Math::class,
    ];

    public function getClassHandler(string $function): object|false|string
    {
        if (method_exists($this, $this->function)) {
            return $this;
        }

        foreach (self::FUNCTIONS_HANDLERS as $class) {
            /** @var $class FunctionHandler */
            if (in_array($function, $class::METHODS)) {
                return new $class($this->left, $this->function, $this->arguments, $this->helpers);
            }
        }

        return false;
    }

    /**
     *
     * @param ExpressionsInformation $information *
     *
     * @return array
     * @throws FHIRPathException
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    public function getValue(
        ExpressionsInformation $information = new ExpressionsInformation()
    ): array {
        $this->info = $information;

        $classOfMethod = $this->getClassHandler($this->function);
        if (!$classOfMethod) {
            // may be cut (extension(str) -> extension.where(url=str))
            try {
                $collection = (new Path($this->left, $this->function))->getValue($information);
            } catch (FHIRPathException) {
                throw FHIRPathException::invalidFunction($this->function);
            }

            if ($this->function === "extension") {
                $this->arguments = new Operator(
                    Operator::EQUAL,
                    new Path(null, 'url'),
                    $this->arguments
                );
            }
            $this->function = 'where';
            $classOfMethod  = $this;
        } elseif (!isset($this->left)) {
            // $this might be omit
            $collection = $information->invokeThis;
        } else {
            $collection = $this->left->getValue($information);
        }

        if (is_a($collection, FHIRBaseInterface::class)) {
            $collection = [$collection];
        }

        if (is_null($collection)) {
            $collection = [];
        }
        $result = $classOfMethod->processFunction($collection, $information);

        return is_array($result) ? $result : [$result];
    }

    public function processFunction(array $input, ExpressionsInformation $info): mixed
    {
        return $this->{$this->function}($input);
    }

    private function isOneBoolean(array $input): bool
    {
        if (count($input) != 1) {
            return false;
        }

        return is_a(reset($input), FHIRBooleanInterface::class);
    }

    public function empty(array $toApply): BooleanLiteral
    {
        if (is_array($toApply) && (count($toApply) == 0)) {
            return (new BooleanLiteral())
                ->setValue(true);
        }

        return (new BooleanLiteral())
            ->setValue(false);
    }

    /**
     * @throws FHIRPathException
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    public function exists(array $toApply): BooleanLiteral
    {
        if (!is_a($this->arguments, VoidExpression::class)) {
            $toApply = $this->where($toApply);
        }


        // .exists() is a shortcut for .empty().not()
        return $this->not([$this->empty($toApply)]);
    }

    /**
     * @throws FHIRPathException
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    public function all(array $toApply): BooleanLiteral
    {
        if (empty($toApply)) {
            return (new BooleanLiteral())->setValue(true);
        }

        foreach ($toApply as $index => $element) {
            $info             = $this->info->copy();
            $info->invokeThis = $element;
            $info->index      = $index;
            $result           = $this->arguments->getValue($info);
            if (count($result) > 1) {
                throw FHIRPathException::expectedSingleElement('all');
            }
            if ((count($result) == 0) || !reset($result)->getValue()) {
                return (new BooleanLiteral())->setValue(false);
            }
        }

        return (new BooleanLiteral())->setValue(true);
    }

    /**
     * @throws FHIRPathException
     */
    public function allTrue(array $toApply): BooleanLiteral
    {
        foreach ($toApply as $bool) {
            if (!is_a($bool, FHIRBooleanInterface::class)) {
                throw FHIRPathException::invalidInput('allTrue');
            }
            if (!$bool->getValue()) {
                return (new BooleanLiteral())->setValue(false);
            }
        }

        return (new BooleanLiteral())->setValue(true);
    }

    public function anyTrue(array $toApply): BooleanLiteral
    {
        foreach ($toApply as $bool) {
            if ($bool->getValue()) {
                return (new BooleanLiteral())->setValue(true);
            }
        }

        return (new BooleanLiteral())->setValue(false);
    }

    public function allFalse(array $toApply): BooleanLiteral
    {
        foreach ($toApply as $bool) {
            if ($bool->getValue()) {
                return (new BooleanLiteral())->setValue(false);
            }
        }

        return (new BooleanLiteral())->setValue(true);
    }

    public function anyFalse(array $toApply): BooleanLiteral
    {
        foreach ($toApply as $bool) {
            if (!$bool->getValue()) {
                return (new BooleanLiteral())->setValue(true);
            }
        }

        return (new BooleanLiteral())->setValue(false);
    }

    /**
     * @throws FHIRPathException
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    public function subsetOf(array $toApply): BooleanLiteral
    {
        $superset = $this->arguments->getValue($this->info);

        if ($this->helpers->subsetOf($toApply, $superset)) {
            return (new BooleanLiteral())->setValue(true);
        }

        return (new BooleanLiteral())->setValue(false);
    }

    /**
     * @throws FHIRPathException
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    public function supersetOf(array $toApply): BooleanLiteral
    {
        $subset = $this->arguments->getValue($this->info);

        if ($this->helpers->subsetOf($subset, $toApply)) {
            return (new BooleanLiteral())->setValue(true);
        }

        return (new BooleanLiteral())->setValue(false);
    }

    public function count(array $toApply): IntegerLiteral
    {
        return (new IntegerLiteral())->setValue(count($toApply));
    }

    /**
     * @throws FHIRPathException
     */
    public function distinct(array $toApply): array
    {
        $unique = [];

        foreach ($toApply as $element) {
            if (!$this->helpers->includes($element, $unique)) {
                $unique[] = $element;
            }
        }

        return $unique;
    }

    /**
     * @throws FHIRPathException
     */
    public function isDistinct(array $toApply): BooleanLiteral
    {
        // X.count() = X.distinct().count()
        if ($this->count($toApply)->getValue() === $this->count($this->distinct($toApply))->getValue()) {
            return (new BooleanLiteral())->setValue(true);
        }

        return (new BooleanLiteral())->setValue(false);
    }

    /**
     * @throws FHIRPathException
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    public function where(array $toApply): array
    {
        $filtered = [];

        foreach ($toApply as $index => $element) {
            $info             = $this->info->copy();
            $info->invokeThis = $element;
            $info->index      = $index;
            $result           = $this->arguments->getValue($info);
            if (count($result) === 0) {
                continue;
            }
            if ((count($result) === 1) && is_a(reset($result), FHIRBooleanInterface::class)) {
                if (reset($result)->getValue()) {
                    $filtered[] = $element;
                }
            } else {
                throw FHIRPathException::expectedSingleElement('where', 'boolean');
            }
        }

        return $filtered;
    }

    /**
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    public function select(array $toApply): array
    {
        $selected = [];

        foreach ($toApply as $index => $element) {
            $info             = $this->info->copy();
            $info->invokeThis = $element;
            $info->index      = $index;
            $selected         = array_merge(
                $selected,
                $this->arguments->getValue($info)
            );
        }

        return $selected;
    }

    /**
     * @throws FHIRPathException
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    public function repeat(array $toApply, array $alreadySelected = []): array
    {
        $selected = [];

        $found = $this->select($toApply);
        foreach ($found as $subElement) {
            if (
                is_a($subElement, FHIRBaseInterface::class) &&
                !$this->helpers->includes($subElement, $alreadySelected)
            ) {
                $selected[] = $subElement;
                $selected   = array_merge(
                    $selected,
                    $this->repeat([$subElement], array_merge($alreadySelected, $selected))
                );
            }
        }

        return $selected;
    }

    /**
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    public function ofType(array $toApply): BooleanLiteral|array
    {
        $this->info->type = true;
        $type             = $this->arguments->getValue($this->info);
        $type             = reset($type);

        /** @var FHIRBaseInterface $type */

        return array_filter(
            $toApply,
            fn($element) => !is_array($element) && ($element::RESOURCE_NAME === $type::RESOURCE_NAME)
        );
    }

    /**
     * @throws FHIRPathException
     */
    public function single(array $toApply): mixed
    {
        if (count($toApply) == 0) {
            return [];
        }

        if (count($toApply) > 1) {
            throw FHIRPathException::expectedSingleInput('single');
        }

        return reset($toApply);
    }

    public function first(array $toApply): mixed
    {
        if (count($toApply) == 0) {
            return [];
        }

        return reset($toApply);
    }

    public function last(array $toApply): mixed
    {
        if (count($toApply) == 0) {
            return [];
        }

        return end($toApply);
    }

    public function tail(array $toApply): array
    {
        if (count($toApply) == 0) {
            return [];
        }

        array_shift($toApply);

        return $toApply;
    }

    /**
     * @throws FHIRPathException
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    public function skip(array $toApply): array
    {
        $args = $this->arguments->getValue($this->info);

        if ((count($args) !== 1) || !is_a(reset($args), FHIRIntegerInterface::class)) {
            throw FHIRPathException::expectedSingleElement('skip', 'integer');
        }

        $num = reset($args)->getValue();

        if (!is_integer($num)) {
            throw FHIRPathException::expectedSingleElement('skip', 'integer');
        }

        if ($num <= 0) {
            return $toApply;
        }

        if ($num > count($toApply)) {
            return [];
        }

        return array_slice($toApply, $num);
    }


    /**
     * @throws FHIRPathException
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    public function take(array $toApply): array
    {
        $args = $this->arguments->getValue($this->info);

        if ((count($args) !== 1) || !is_a(reset($args), FHIRIntegerInterface::class)) {
            throw FHIRPathException::expectedSingleElement('take', 'integer');
        }

        $num = reset($args)->getValue();

        if (!is_integer($num)) {
            throw FHIRPathException::expectedSingleElement('take', 'integer');
        }

        if ($num <= 0) {
            return [];
        }

        return array_slice($toApply, 0, $num);
    }

    /**
     * @throws FHIRPathException
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    public function intersect(array $toApply): array
    {
        $intersection = [];

        $second = $this->arguments->getValue($this->info);

        foreach ($toApply as $element) {
            if ($this->helpers->includes($element, $second) && !$this->helpers->includes($element, $intersection)) {
                $intersection[] = $element;
            }
        }

        return $intersection;
    }

    /**
     * @throws FHIRPathException
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    public function exclude(array $toApply): array
    {
        $exclusion = [];

        $second = $this->arguments->getValue($this->info);

        foreach ($toApply as $element) {
            if (!$this->helpers->includes($element, $second)) {
                $exclusion[] = $element;
            }
        }

        return $exclusion;
    }

    /**
     * @throws FHIRPathException
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    public function union(array $toApply): array
    {
        $second = $this->arguments->getValue($this->info);

        return $this->distinct(array_merge($toApply, $second));
    }

    /**
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    public function combine(array $toApply): array
    {
        $second = $this->arguments->getValue($this->info);

        return array_merge($toApply, $second);
    }

    /**
     * @throws FHIRPathException
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    public function iif(array $toApply): array
    {
        $args = $this->arguments->getValue($this->info);

        if (count($toApply) > 1) {
            throw  FHIRPathException::expectedSingleElement('iif', 'any');
        }

        if (count($args) < 2) {
            throw FHIRPathException::invalidArgument('iif', '', 'Expects 2 or 3 arguments.');
        }

        $info             = $this->info->copy();
        $info->invokeThis = reset($toApply) ?: null;
        $info->index      = 0;

        $criterion = reset($args)->getValue($info);

        if (count($criterion) == 0) {
            $criterion = [(new BooleanLiteral())->setValue(false)];
        }

        if (!$this->isOneBoolean($criterion)) {
            throw FHIRPathException::expectedSingleElement('iif', 'bool');
        }

        if (reset($criterion)->getValue()) {
            return $args[1]->getValue($this->info);
        }

        return (count($args) < 3) ? [] : $args[2]->getValue($this->info);
    }

    public function children(array $toApply): array
    {
        $children = [];

        /** @var FHIRBaseInterface $element */
        foreach ($toApply as $element) {
            $definition = $element->getFhirResourceDefinition();
            foreach ($definition->getFields() as $field) {
                $value = $element->{'get' . ucfirst($field->getLabel())}();
                if ($field->isUnique()) {
                    if (!is_null($value)) {
                        $children[] = $value;
                    }
                } else {
                    $children = array_merge($children, $value);
                }
            }
        }

        return $children;
    }

    public function descendants(array $toApply): array
    {
        return array_merge(...array_map(fn($e) => $this->helpers->descendants($e), $toApply));
    }

    public function trace(array $toApply): array
    {
        // TODO : log it
        return $toApply;
    }

    public function now(): DateTimeLiteral
    {
        return (new DateTimeLiteral())->setValue($this->info->now->format(DateTimeInterface::ATOM));
    }

    public function timeOfDay(): TimeLiteral
    {
        return (new TimeLiteral())->setValue($this->info->now->format('H:i:s'));
    }

    public function today(): DateLiteral
    {
        return (new DateLiteral())->setValue($this->info->now->format('Y-m-d'));
    }

    /**
     * @throws FHIRPathException
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    public function is(): BooleanLiteral|array
    {
        // May be deprecated
        return (new Operator(Operator::IS, $this->left, $this->arguments, $this->helpers))
            ->getValue($this->info);
    }

    /**
     * @throws FHIRPathException
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    public function as(): BooleanLiteral|array
    {
        // May be deprecated
        return (new Operator(Operator::AS, $this->left, $this->arguments, $this->helpers))
            ->getValue($this->info);
    }

    /**
     * @throws FHIRPathException
     */
    public function not(array $toApply): BooleanLiteral|array
    {
        if (count($toApply) > 1) {
            throw FHIRPathException::expectedSingleInput('not');
        }

        if (count($toApply) == 0) {
            return [];
        }

        $elem = reset($toApply);
        if (is_a($elem, FHIRBooleanInterface::class)) {
            $elem->setValue(!$elem->getValue());

            return $elem;
        }

        return (new BooleanLiteral())->setValue(false);
    }


    /**
     * @throws FHIRPathException
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    public function aggregate(array $toApply): array|FHIRBaseInterface
    {
        if (is_a($this->arguments, Arguments::class)) {
            $args = $this->arguments->getValue($this->info);

            if (count($args) < 1) {
                throw FHIRPathException::invalidArgument('aggregate', '', 'Expects 1 or 2 arguments.');
            }

            $expression = reset($args);
            $total      = (sizeof($args) > 1) ? $args[1]->getValue($this->info) : [];
        } else {
            $expression = $this->arguments;
            $total      = [];
        }

        $index = 0;
        foreach ($toApply as $element) {
            $info             = $this->info->copy();
            $info->index      = $index;
            $info->invokeThis = $element;
            $info->total      = $total;
            $result           = $expression->getValue($info);
            $total            = is_array($result) ? $result : [$result];
            $index++;
        }

        return $total;
    }

    public function type(array $toApply): array
    {
        $types = new Types();

        return $types->getTypes($toApply);
    }
}
