<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions\Operators;

use DateTime;
use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRPathException;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Operator;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRBaseInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRQuantityInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRDateInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRDateTimeInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRDecimalInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRIntegerInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRStringInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRTimeInterface;
use Ox\Components\FHIRCore\Utils\Comparisons;

class Comparison
{
    public function __construct(private Comparisons $helpers)
    {
    }

    /**
     * @throws FHIRPathException
     */
    public function comparison(array $left, array $right, string $operator): bool|array
    {
        if ((count($left) == 0) || (count($right) == 0)) {
            return [];
        }

        if ((count($left) > 1) || (count($right) > 1)) {
            throw FHIRPathException::operator($operator, '', '');
        }

        /** @var FHIRBaseInterface $l */
        $l = reset($left);
        /** @var FHIRBaseInterface $r */
        $r = reset($right);

        [$l, $r] = $this->helpers->implicitConversion($l, $r);

        if (
            is_a($l, FHIRDateInterface::class) ||
            is_a($l, FHIRDateTimeInterface::class) ||
            is_a($l, FHIRTimeInterface::class)
        ) {
            /** @var FHIRDateInterface|FHIRTimeInterface|FHIRDateTimeInterface $r */
            return $this->compareDateTime($l, $r, $operator);
        }

        /**
         * @var FHIRIntegerInterface|FHIRDecimalInterface|FHIRStringInterface|FHIRQuantityInterface $l
         * @var FHIRIntegerInterface|FHIRDecimalInterface|FHIRStringInterface|FHIRQuantityInterface $r
         */
        $l = $l->getValue();
        $r = $r->getValue();

        if (is_a($l, FHIRDecimalInterface::class)) {
            // Quantity case
            $l = $l->getValue();
            $r = $r->getValue();
        }


        if (($operator === Operator::GREATER) || ($operator === Operator::GREATER_EQUAL)) {
            return $this->greaterThan($l, $r);
        }

        if (($operator === Operator::LOWER) || ($operator === Operator::LOWER_EQUAL)) {
            return $this->lessThan($l, $r);
        }

        throw FHIRPathException::operator($operator, '', '');
    }

    /**
     * @throws FHIRPathException
     */
    public function compareDateTime(
        FHIRDateInterface|FHIRTimeInterface|FHIRDateTimeInterface $left,
        FHIRDateInterface|FHIRTimeInterface|FHIRDateTimeInterface $right,
        string                                                    $operator
    ): bool|array {
        if (!$this->helpers->isSameDateTimePrecision($left, $right)) {
            return [];
        }

        $dl = date_create($left->getValue());
        $dr = date_create($right->getValue());

        return match ($operator) {
            '>', '>=' => $this->greaterThan($dl, $dr),
            '<', '<=' => $this->lessThan($dl, $dr),
            default => throw FHIRPathException::operator($operator, '', ''),
        };
    }

    public function greaterThan(DateTime|int|float|string $left, DateTime|int|float|string $right): bool
    {
        return $left > $right;
    }

    public function lessThan(DateTime|int|float|string $left, DateTime|int|float|string $right): bool
    {
        return $left < $right;
    }
}
