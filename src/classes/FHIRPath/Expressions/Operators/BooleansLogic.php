<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions\Operators;

use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRPathException;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Operator;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRBooleanInterface;

class BooleansLogic
{
    /**
     * @throws FHIRPathException
     */
    public function logic(mixed $left, mixed $right, string $operator): bool|array
    {
        if (!is_array($left) || !is_array($right)) {
            return [];
        }

        if ((count($left) > 1) || (count($right) > 1)) {
            throw FHIRPathException::operator($operator, '', '');
        }

        if (count($left) == 0) {
            $l = $left;
        } elseif (is_a(reset($left), FHIRBooleanInterface::class)) {
            $l = reset($left)->getValue();
        } else {
            throw FHIRPathException::expectedSingleInput($operator, 'bool or {}');
        }

        if (count($right) == 0) {
            $r = $right;
        } elseif (is_a(reset($right), FHIRBooleanInterface::class)) {
            $r = reset($right)->getValue();
        } else {
            throw FHIRPathException::expectedSingleInput($operator, 'bool or {}');
        }

        return match ($operator) {
            Operator::AND => $this->and($l, $r),
            Operator::OR => $this->or($l, $r),
            Operator::XOR => $this->xor($l, $r),
            Operator::IMPLIES => $this->implies($l, $r),
            default => throw FHIRPathException::operator($operator, '', ''),
        };
    }

    public function and(bool|array $left, bool|array $right): bool|array
    {
        if ((is_bool($left) && !$left) || (is_bool($right) && !$right)) {
            return false;
        }

        if (is_array($left) || is_array($right)) {
            return [];
        }

        return true;
    }

    public function or(bool|array $left, bool|array $right): bool|array
    {
        if ((is_bool($left) && $left) || (is_bool($right) && $right)) {
            return true;
        }

        if (is_array($left) || is_array($right)) {
            return [];
        }

        return false;
    }

    public function xor(bool|array $left, bool|array $right): bool|array
    {
        if (is_array($left) || is_array($right)) {
            return [];
        }

        return !($left === $right);
    }

    public function implies(bool|array $left, bool|array $right): bool|array
    {
        if ((is_bool($left) && $left)) {
            return $right;
        }

        if ((is_bool($right) && $right) || (is_bool($left) && !$left)) {
            return true;
        }

        return [];
    }
}
