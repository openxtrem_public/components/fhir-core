<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions\Operators;

use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRPathException;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Operator;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRElementInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRQuantityInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRDecimalInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRIntegerInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRStringInterface;

class Math
{
    /**
     * @throws FHIRPathException
     */
    public function math(
        FHIRElementInterface $left,
        FHIRElementInterface $right,
        string               $operator
    ): string|int|array|float {
        return match ($operator) {
            Operator::MULTIPLICATION => $this->multiplication($left, $right),
            Operator::DIVISION => $this->division($left, $right),
            Operator::ADDITION => $this->addition($left, $right),
            Operator::SUBTRACTION => $this->subtraction($left, $right),
            Operator::DIV => $this->div($left, $right),
            Operator::MODULO => $this->mod($left, $right),
        };
    }

    private function isNumber(FHIRElementInterface $input): bool
    {
        if (
            is_a($input, FHIRIntegerInterface::class) or
            is_a($input, FHIRDecimalInterface::class) or
            is_a($input, FHIRQuantityInterface::class)
        ) {
            return true;
        }

        return false;
    }

    /**
     * @throws FHIRPathException
     */
    private function multiplication(FHIRElementInterface $left, FHIRElementInterface $right): float
    {
        if (!$this->isNumber($left) || !$this->isNumber($right)) {
            throw FHIRPathException::operator('*', $left, $right);
        }

        /** @var FHIRIntegerInterface|FHIRDecimalInterface|FHIRQuantityInterface $left */
        /** @var FHIRIntegerInterface|FHIRDecimalInterface|FHIRQuantityInterface $right */

        // TODO : unit conversions (1cm * 3cm2 = 3cm3)

        return $left->getValue() * $right->getValue();
    }

    /**
     * @throws FHIRPathException
     */
    private function division(FHIRElementInterface $left, FHIRElementInterface $right): float|array
    {
        if (!$this->isNumber($left) || !$this->isNumber($right)) {
            throw FHIRPathException::operator('/', $left, $right);
        }

        /** @var FHIRIntegerInterface|FHIRDecimalInterface|FHIRQuantityInterface $left */
        /** @var FHIRIntegerInterface|FHIRDecimalInterface|FHIRQuantityInterface $right */

        if ($right->getValue() == 0) {
            return [];
        }

        // TODO : unit conversions (12 'cm2' / 3 'cm' = 4.0 'cm')

        return fdiv($left->getValue(), $right->getValue());
    }

    /**
     * @throws FHIRPathException
     */
    private function addition(FHIRElementInterface $left, FHIRElementInterface $right): float|string
    {
        // left and right should be the same type
        if (is_a($left, FHIRStringInterface::class)) {
            /** @var FHIRStringInterface $left */
            /** @var FHIRStringInterface $right */
            return $left->getValue() . $right->getValue();
        }

        if (!$this->isNumber($left)) {
            throw FHIRPathException::operator('+', $left, $right);
        }

        /** @var FHIRIntegerInterface|FHIRDecimalInterface|FHIRQuantityInterface $left */
        /** @var FHIRIntegerInterface|FHIRDecimalInterface|FHIRQuantityInterface $right */


        // TODO : unit conversions (3 'm' + 3 'cm' = 303 'cm') /!\ not possible : 3 'cm' + 3 'cm2'

        return $left->getValue() + $right->getValue();
    }

    /**
     * @throws FHIRPathException
     */
    private function subtraction(FHIRElementInterface $left, FHIRElementInterface $right): float
    {
        // left and right should be the same type
        if (!$this->isNumber($left)) {
            throw FHIRPathException::operator('-', $left, $right);
        }

        /** @var FHIRIntegerInterface|FHIRDecimalInterface|FHIRQuantityInterface $left */
        /** @var FHIRIntegerInterface|FHIRDecimalInterface|FHIRQuantityInterface $right */

        // TODO : unit conversions

        return $left->getValue() - $right->getValue();
    }

    /**
     * @throws FHIRPathException
     */
    private function div(FHIRElementInterface $left, FHIRElementInterface $right): int|array
    {
        // left and right should be the same type
        if (!is_a($left, FHIRIntegerInterface::class) && !is_a($left, FHIRDecimalInterface::class)) {
            throw FHIRPathException::operator('div', $left, $right);
        }

        /** @var FHIRIntegerInterface|FHIRDecimalInterface $left */
        /** @var FHIRIntegerInterface|FHIRDecimalInterface $right */

        if ($right->getValue() == 0) {
            return [];
        }

        return floor($left->getValue() / $right->getValue());
    }

    /**
     * @throws FHIRPathException
     */
    private function mod(FHIRElementInterface $left, FHIRElementInterface $right): float|array
    {
        // left and right should be the same type
        if (!is_a($left, FHIRIntegerInterface::class) && !is_a($left, FHIRDecimalInterface::class)) {
            throw FHIRPathException::operator('mod', $left, $right);
        }

        /** @var FHIRIntegerInterface|FHIRDecimalInterface $left */
        /** @var FHIRIntegerInterface|FHIRDecimalInterface $right */

        if ($right->getValue() == 0) {
            return [];
        }

        $mod = fmod($left->getValue(), $right->getValue());

        return is_a($left, FHIRIntegerInterface::class) ? (int)$mod : $mod;
    }
}
