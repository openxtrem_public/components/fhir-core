<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions;

use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\BooleanLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\DateLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\DateTimeLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\DecimalLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\IntegerLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\QuantityLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\StringLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\TimeLiteral;

class Literal extends AbstractExpression
{
    final public const EMPTY    = 'empty';
    final public const BOOLEAN  = 'boolean';
    final public const INTEGER  = 'integer';
    final public const DECIMAL  = 'decimal';
    final public const STRING   = 'string';
    final public const DATE     = 'date';
    final public const DATETIME = 'datetime';
    final public const TIME     = 'time';
    final public const QUANTITY = 'quantity';

    final public const PATTERNS = [
        // order is important
        self::QUANTITY => "/\d+(\.\d+)?( (years?|months?|weeks?|days?|hours?|minutes?|seconds?|milliseconds?|'.*?'))/",
        self::STRING   => "/(?<![^\\\]\\\)'(.|\n)*?(?<![^\\\]\\\)'/",
        self::DATETIME =>
            "/@\d{4}(-\d{2}(-\d{2})?)?T(([01]\d|2[0-4])(:[0-5]\d(:[0-5]\d(\.\d+)?)?)?(Z|(\+|-)[0-5]\d:[0-5]\d)?)?/",
        self::TIME     => "/@T([01]\d|2[0-4])(:[0-5]\d(:[0-5]\d(\.\d+)?)?)?/",
        self::DATE     => "/@\d{4}(-\d{2}(-\d{2})?)?/",
        self::DECIMAL  => "/\d+\.\d+/",
        self::INTEGER  => "/\d+/",
        self::EMPTY    => "/\{\ }/",
        self::BOOLEAN  => "/true|false/",
    ];

    public string $data;
    public string $type;

    public function __construct(string $data, string $type)
    {
        $this->data = $data;
        $this->type = $type;
    }

    /**
     * @param string $data
     *
     * @return false|string type found or false
     */
    public static function isLiteral(string $data): false|string
    {
        foreach (self::PATTERNS as $type => $pattern) {
            $p = '/^' . trim($pattern, '/') . '$/';
            if (preg_match($p, trim($data))) {
                return $type;
            }
        }

        return false;
    }

    /**
     *
     * @param ExpressionsInformation $information *
     *
     * @return array
     */
    public function getValue(ExpressionsInformation $information = new ExpressionsInformation()): array
    {
        if ($this->type === self::EMPTY) {
            return [];
        }

        if ($this->type === self::BOOLEAN) {
            return [
                (new BooleanLiteral())
                    ->setValue($this->data === 'true'),
            ];
        }

        if ($this->type === self::INTEGER) {
            return [
                (new IntegerLiteral())
                    ->setValue(intval($this->data)),
            ];
        }

        if ($this->type === self::DECIMAL) {
            return [
                (new DecimalLiteral())
                    ->setValue(floatval($this->data)),
            ];
        }

        if ($this->type === self::DATE) {
            return [
                (new DateLiteral())
                    ->setValue(ltrim($this->data, '@')),
            ];
        }

        if ($this->type === self::TIME) {
            return [
                (new TimeLiteral())
                    ->setValue(ltrim($this->data, '@')),
            ];
        }

        if ($this->type === self::DATETIME) {
            return [
                (new DateTimeLiteral())
                    ->setValue(ltrim($this->data, '@')),
            ];
        }

        if ($this->type === self::QUANTITY) {
            $explode  = explode(' ', $this->data, 2);
            $quantity = (new QuantityLiteral())
                ->setValue(floatval($explode[0]));
            if (sizeof($explode) == 2) {
                $quantity->setUnit($explode[1]);
            }

            return [$quantity];
        }

        if ($this->type === self::STRING) {
            $unicode_decoded = html_entity_decode(
                preg_replace("/\\\u([0-9A-F]{4})/", "&#x\\1;", $this->data),
                ENT_NOQUOTES,
                'UTF-8'
            );

            return [
                (new StringLiteral())
                    ->setValue(substr($unicode_decoded, 1, -1)),
            ];
        }

        return [];
    }
}
