<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions;

use Exception;
use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRPathException;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRQuantityInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRDecimalInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRIntegerInterface;

class Modifier extends AbstractExpression
{
    final public const PLUS  = '+';
    final public const MINUS = '-';

    public function __construct(private string $modifier, public ExpressionInterface $data)
    {
    }

    /**
     * @throws FHIRPathException
     * @throws Exception
     */
    public function getValue(ExpressionsInformation $information = new ExpressionsInformation()): array
    {
        $subCollection = $this->data->getValue($information);

        if (count($subCollection) != 1) {
            throw FHIRPathException::operationOnMultiple($this->modifier);
        }

        $value = reset($subCollection);

        if (
            !(
                is_a($value, FHIRIntegerInterface::class) or
                is_a($value, FHIRDecimalInterface::class) or
                is_a($value, FHIRQuantityInterface::class)
            )
        ) {
            throw FHIRPathException::unaryOperators($this->modifier);
        }

        if ($this->modifier === self::PLUS) {
            return [$value];
        }
        if ($this->modifier === self::MINUS) {
            if (is_a($value->getValue(), FHIRDecimalInterface::class)) {
                $value->getValue()->setValue(-$value->getValue()->getValue());

                return [$value];
            }

            return [$value->setValue(-$value->getValue())];
        }

        throw new Exception("Invalid modifier '$this->modifier'.");
    }
}
