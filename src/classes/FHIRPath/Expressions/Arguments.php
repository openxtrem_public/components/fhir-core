<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions;

class Arguments extends AbstractExpression
{
    /** @var AbstractExpression[] */
    public array $data;

    public function __construct(ExpressionInterface ...$arguments)
    {
        $this->data = $arguments;
    }

    public function getValue(ExpressionsInformation $information = new ExpressionsInformation()): array
    {
        return $this->data;
        $arguments = [];

        foreach ($this->data as $arg) {
            $arguments[] = $arg->getValue($information);
        }

        return $arguments;
    }
}
