<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions\Functions;

use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRPathException;
use Ox\Components\FHIRCore\FHIRPath\Expressions\ExpressionsInformation;
use Ox\Components\FHIRCore\FHIRPath\Expressions\FunctionInvocation;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literal;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\BooleanLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\DecimalLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\IntegerLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\QuantityLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\StringLiteral;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRElementInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRQuantityInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRBooleanInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRDateInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRDateTimeInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRDecimalInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRIntegerInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRStringInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRTimeInterface;

class Conversions extends FunctionInvocation implements FunctionHandler
{
    final public const METHODS = [
        'toBoolean',
        'convertsToBoolean',
        'toInteger',
        'convertsToInteger',
        'toDecimal',
        'convertsToDecimal',
        'toString',
        'convertsToString',
        'toQuantity',
        'convertsToQuantity',
        'toDate',
        'convertsToDate',
        'toTime',
        'convertsToTime',
        'toDateTime',
        'convertsToDateTime',
    ];

    /**
     * @throws FHIRPathException
     */
    public function processFunction(array $input, ExpressionsInformation $info): mixed
    {
        if (sizeof($input) == 0) {
            return [];
        } elseif (sizeof($input) > 1) {
            throw  FHIRPathException::expectedSingleInput($this->function);
        }

        return $this->{$this->function}(reset($input));
    }

    public function toBoolean(mixed $primitive): BooleanLiteral|array
    {
        if (is_a($primitive, FHIRBooleanInterface::class)) {
            return (new BooleanLiteral())
                ->setValue($primitive->getValue());
        }

        $value = $primitive->getValue();

        if (is_string($value)) {
            $value = strtolower($value);
        }

        return match ($value) {
            1, 1.0, 'true', 't', 'yes', 'y', '1', '1.0' => (new BooleanLiteral())->setValue(true),
            0, 0.0, 'false', 'f', 'no', 'n', '0', '0.0' => (new BooleanLiteral())->setValue(false),
            default => [],
        };
    }

    public function convertsToBoolean(mixed $primitive): BooleanLiteral|array
    {
        if (is_a($primitive, FHIRBooleanInterface::class)) {
            return (new BooleanLiteral())->setValue(true);
        }

        $value = $primitive->getValue();

        if (is_string($value)) {
            $value = strtolower($value);
        }

        return match ($value) {
            1, 1.0, 'true', 't', 'yes', 'y', '1', '1.0', 0, 0.0, 'false', 'f', 'no', 'n', '0', '0.0'
            => (new BooleanLiteral())->setValue(true),
            default => (new BooleanLiteral())->setValue(false),
        };
    }

    public function toInteger(mixed $primitive): IntegerLiteral|array
    {
        if (is_a($primitive, FHIRIntegerInterface::class)) {
            return (new IntegerLiteral())
                ->setValue($primitive->getValue());
        }

        if (is_a($primitive, FHIRBooleanInterface::class)) {
            return (new IntegerLiteral())->setValue($primitive->getValue() ? 1 : 0);
        }

        if (is_a($primitive, FHIRStringInterface::class)) {
            $v = $primitive->getValue();
            if (preg_match_all('/^[+-]?\d+$/', $v) == 1) {
                return (new IntegerLiteral())->setValue(intval($v));
            }
        }

        return [];
    }

    public function convertsToInteger(mixed $primitive): BooleanLiteral|array
    {
        if (is_a($primitive, FHIRIntegerInterface::class) || is_a($primitive, FHIRBooleanInterface::class)) {
            return (new BooleanLiteral())->setValue(true);
        }

        if (is_a($primitive, FHIRStringInterface::class)) {
            $v = $primitive->getValue();
            if (preg_match_all('/^[+-]?\d+$/', $v) == 1) {
                return (new BooleanLiteral())->setValue(true);
            }
        }

        return (new BooleanLiteral())->setValue(false);
    }

    public function toDecimal(mixed $primitive): DecimalLiteral|array
    {
        if (!$this->convertsToDecimal($primitive)->getValue()) {
            return [];
        }

        return (new DecimalLiteral())->setValue(floatval($primitive->getValue()));
    }

    public function convertsToDecimal(mixed $primitive): BooleanLiteral|array
    {
        $toInt = $this->convertsToInteger($primitive);
        if (!is_array($toInt) && $toInt->getValue()) {
            return (new BooleanLiteral())->setValue(true);
        }

        if (is_a($primitive, FHIRDecimalInterface::class) || is_a($primitive, FHIRBooleanInterface::class)) {
            return (new BooleanLiteral())->setValue(true);
        }

        if (is_a($primitive, FHIRStringInterface::class)) {
            $v = $primitive->getValue();
            if (preg_match_all('/^([+-])?\d+(\.\d+)?$/', $v) == 1) {
                return (new BooleanLiteral())->setValue(true);
            }
        }

        return (new BooleanLiteral())->setValue(false);
    }

    public function toString(mixed $primitive): StringLiteral|array
    {
        if (is_a($primitive, FHIRStringInterface::class)) {
            return (new StringLiteral())
                ->setValue($primitive->getValue());
        }

        if (is_a($primitive, FHIRBooleanInterface::class)) {
            return $primitive->getValue() ?
                (new StringLiteral())->setValue('true') :
                (new StringLiteral())->setValue('false');
        }

        if (is_a($primitive, FHIRDecimalInterface::class)) {
            return ($primitive->getValue() == (int)$primitive->getValue()) ?
                (new StringLiteral())->setValue($primitive->getValue() . '.0') :
                (new StringLiteral())->setValue(strval($primitive->getValue()));
        }

        if (
            is_a($primitive, FHIRIntegerInterface::class) or
            is_a($primitive, FHIRDateInterface::class) or
            is_a($primitive, FHIRDateTimeInterface::class) or
            is_a($primitive, FHIRTimeInterface::class)
        ) {
            return (new StringLiteral())->setValue(strval($primitive->getValue()));
        }

        if (is_a($primitive, FHIRQuantityInterface::class)) {
            return (new StringLiteral())->setValue(
                $primitive->getValue()->getValue() .
                " '" .
                $primitive->getUnit()->getValue() .
                "'"
            );
        }

        return [];
    }

    public function convertsToString(mixed $primitive): BooleanLiteral|array
    {
        if (
            is_a($primitive, FHIRIntegerInterface::class) or
            is_a($primitive, FHIRQuantityInterface::class) or
            is_a($primitive, FHIRDecimalInterface::class) or
            is_a($primitive, FHIRDateInterface::class) or
            is_a($primitive, FHIRDateTimeInterface::class) or
            is_a($primitive, FHIRTimeInterface::class) or
            is_a($primitive, FHIRStringInterface::class) or
            is_a($primitive, FHIRBooleanInterface::class)
        ) {
            return (new BooleanLiteral())->setValue(true);
        }

        return (new BooleanLiteral())->setValue(false);
    }

    public function convertsToDate(mixed $primitive): BooleanLiteral|array
    {
        if (
            is_a($primitive, FHIRDateInterface::class) or
            is_a($primitive, FHIRDateTimeInterface::class)
        ) {
            return (new BooleanLiteral())->setValue(true);
        }

        $pattern = str_replace('@', '', Literal::PATTERNS[Literal::DATE]);

        if (
            is_a($primitive, FHIRStringInterface::class) and
            preg_match($pattern, $primitive->getValue())
        ) {
            return (new BooleanLiteral())->setValue(true);
        }

        return (new BooleanLiteral())->setValue(false);
    }

    public function convertsToDateTime(mixed $primitive): BooleanLiteral|array
    {
        if (
            is_a($primitive, FHIRDateInterface::class) or
            is_a($primitive, FHIRDateTimeInterface::class)
        ) {
            return (new BooleanLiteral())->setValue(true);
        }

        $pattern = '/\d{4}(-\d{2}(-\d{2}(T([01]\d|2[0-4])(:[0-5]\d(:[0-5]\d(\.\d+)?)?)?(Z|[+-][0-5]\d:[0-5]\d)?)?)?)?/';

        if (
            is_a($primitive, FHIRStringInterface::class) and
            preg_match($pattern, $primitive->getValue())
        ) {
            return (new BooleanLiteral())->setValue(true);
        }

        return (new BooleanLiteral())->setValue(false);
    }

    public function convertsToTime(mixed $primitive): BooleanLiteral|array
    {
        if (is_a($primitive, FHIRTimeInterface::class)) {
            return (new BooleanLiteral())->setValue(true);
        }

        $pattern = str_replace('@T', '', Literal::PATTERNS[Literal::TIME]);

        if (
            is_a($primitive, FHIRStringInterface::class) and
            preg_match($pattern, $primitive->getValue())
        ) {
            return (new BooleanLiteral())->setValue(true);
        }

        return (new BooleanLiteral())->setValue(false);
    }

    public function convertsToQuantity(FHIRElementInterface $primitive): BooleanLiteral|array
    {
        if (
            is_a($primitive, FHIRIntegerInterface::class) or
            is_a($primitive, FHIRQuantityInterface::class) or
            is_a($primitive, FHIRDecimalInterface::class) or
            is_a($primitive, FHIRBooleanInterface::class)
        ) {
            return (new BooleanLiteral())->setValue(true);
        }

        $pattern = "/^[+-]?\d+(\.\d+)?\s*((year|month|week|day|hour|minute|second|millisecond)s?|\\\?'([^']+)\\\?')?$/";
        if (
            is_a($primitive, FHIRStringInterface::class) and
            (preg_match($pattern, $primitive->getValue()) == 1)
        ) {
            return (new BooleanLiteral())->setValue(true);
        }

        return (new BooleanLiteral())->setValue(false);
    }

    public function toQuantity(FHIRElementInterface $primitive): QuantityLiteral|array
    {
        // TODO : unit argument

        $converts = $this->convertsToQuantity($primitive);
        if (!$converts->getValue()) {
            return [];
        }

        if (is_a($primitive, FHIRIntegerInterface::class) || is_a($primitive, FHIRDecimalInterface::class)) {
            return (new QuantityLiteral())
                ->setValue($primitive->getValue())
                ->setUnit('1');
        }

        if (is_a($primitive, FHIRQuantityInterface::class)) {
            return (new QuantityLiteral())
                ->setValue($primitive->getValue()->getValue())
                ->setUnit($primitive->getUnit()->getValue());
        }

        if (is_a($primitive, FHIRBooleanInterface::class)) {
            return (new QuantityLiteral())
                ->setValue($primitive->getValue() ? 1.0 : 0.0)
                ->setUnit('1');
        }

        if (is_a($primitive, FHIRStringInterface::class)) {
            $explode  = preg_split('/\s+/', $primitive->getValue(), 2);
            $quantity = (new QuantityLiteral())
                ->setValue(floatval(reset($explode)));

            if (sizeof($explode) > 1) {
                $quantity->setUnit(trim(end($explode), "\\'"));
            } else {
                $quantity->setUnit('1');
            }


            return $quantity;
        }

        return [];
    }
}
