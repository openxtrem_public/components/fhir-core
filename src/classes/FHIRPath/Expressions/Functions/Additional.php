<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions\Functions;

use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRLiteralFound;
use Ox\Components\FHIRCore\FHIRPath\Exception\SystemLiteralFound;
use Ox\Components\FHIRCore\FHIRPath\Expressions\ExpressionsInformation;
use Ox\Components\FHIRCore\FHIRPath\Expressions\FunctionInvocation;

class Additional extends FunctionInvocation implements FunctionHandler
{
    final public const METHODS = [
        'extension',
        'hasValue',
        'getValue',
        'resolve',
        'ofType',
        'elementDefinition',
        'checkModifiers',
        'conformsTo',
        'memberOf',
        'subsumes',
        'subsumedBy',
        'htmlChecks',
        'lowBoundary',
        'highBoundary',
        'comparable',
    ];

    /**
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    public function processFunction(array $input, ExpressionsInformation $info): mixed
    {
        $arguments = $this->arguments->getValue($info);

        return $this->{$this->function}($input, $arguments);
    }

    // TODO : https://build.fhir.org/fhirpath.html#functions
}
