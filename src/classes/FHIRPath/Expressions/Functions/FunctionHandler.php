<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions\Functions;

use Ox\Components\FHIRCore\FHIRPath\Expressions\ExpressionsInformation;

interface FunctionHandler
{
    public const METHODS = [];

    public function processFunction(array $input, ExpressionsInformation $info): mixed;
}
