<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions\Functions;

use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRLiteralFound;
use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRPathException;
use Ox\Components\FHIRCore\FHIRPath\Exception\SystemLiteralFound;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Arguments;
use Ox\Components\FHIRCore\FHIRPath\Expressions\ExpressionsInformation;
use Ox\Components\FHIRCore\FHIRPath\Expressions\FunctionInvocation;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\BooleanLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\IntegerLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\StringLiteral;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRIntegerInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRStringInterface;

class StringManipulation extends FunctionInvocation implements FunctionHandler
{
    final public const METHODS = [
        'indexOf',
        'substring',
        'startsWith',
        'endsWith',
        'contains',
        'upper',
        'lower',
        'replace',
        'matches',
        'replaceMatches',
        'length',
        'toChars',
    ];

    /**
     * @throws FHIRPathException
     */
    public function indexOf(string $string, array $arguments): IntegerLiteral|array
    {
        if (count($arguments) == 0) {
            return [];
        }

        if ((count($arguments) > 1) || !is_a(reset($arguments), FHIRStringInterface::class)) {
            throw FHIRPathException::expectedSingleElement('indexOf', 'string');
        }

        $substring = reset($arguments)->getValue();

        if ($substring === '') {
            return (new IntegerLiteral())->setValue(0);
        }

        $result = strpos($string, $substring);

        if (is_bool($result)) {
            return (new IntegerLiteral())->setValue(-1);
        }

        return (new IntegerLiteral())->setValue($result);
    }

    /**
     * @throws FHIRPathException
     */
    public function substring(string $string, array|Arguments $arguments): StringLiteral|array
    {
        if (count($arguments) == 0) {
            return [];
        }

        $start = (sizeof($arguments) > 1) ?
            reset($arguments)->getValue($this->info) :
            $arguments;

        if ((count($start) > 1) || !is_a(reset($start), FHIRIntegerInterface::class)) {
            throw FHIRPathException::expectedSingleElement('substring', 'int');
        }
        $start = reset($start)->getValue();

        if (($start >= strlen($string)) || ($start < 0)) {
            return [];
        }

        if (count($arguments) == 1) {
            return (new StringLiteral())->setValue(substr($string, $start));
        }

        $length = $arguments[1]->getValue($this->info);
        if (count($length) == 0) {
            return (new StringLiteral())->setValue(substr($string, $start));
        }
        if ((count($length) > 1) || !is_a(reset($length), FHIRIntegerInterface::class)) {
            throw FHIRPathException::expectedSingleInput('substring', 'int');
        }
        $length = reset($length)->getValue();

        return (new StringLiteral())->setValue(substr($string, $start, $length));
    }

    /**
     * @throws FHIRPathException
     */
    public function startsWith(string $string, array $arguments): BooleanLiteral|array
    {
        if ((count($arguments) > 1) || !is_a(reset($arguments), FHIRStringInterface::class)) {
            throw FHIRPathException::expectedSingleElement('startsWith', 'string');
        }

        $prefix = reset($arguments)->getValue();

        if ($prefix === '') {
            return (new BooleanLiteral())->setValue(true);
        }

        return (new BooleanLiteral())->setValue(str_starts_with($string, $prefix));
    }

    /**
     * @throws FHIRPathException
     */
    public function endsWith(string $string, array $arguments): BooleanLiteral|array
    {
        if ((count($arguments) > 1) || !is_a(reset($arguments), FHIRStringInterface::class)) {
            throw FHIRPathException::expectedSingleElement('endsWith', 'string');
        }

        $suffix = reset($arguments)->getValue();

        if ($suffix === '') {
            return (new BooleanLiteral())->setValue(true);
        }

        return (new BooleanLiteral())->setValue(str_ends_with($string, $suffix));
    }

    /**
     * @throws FHIRPathException
     */
    public function contains(string $string, array $arguments): BooleanLiteral|array
    {
        if ((count($arguments) > 1) || !is_a(reset($arguments), FHIRStringInterface::class)) {
            throw FHIRPathException::expectedSingleElement('contains', 'string');
        }

        $substring = reset($arguments)->getValue();

        if ($substring === '') {
            return (new BooleanLiteral())->setValue(true);
        }

        return (new BooleanLiteral())->setValue(str_contains($string, $substring));
    }

    public function upper(string $string): StringLiteral|array
    {
        return (new StringLiteral())->setValue(strtoupper($string));
    }

    public function lower(string $string): StringLiteral|array
    {
        return (new StringLiteral())->setValue(strtolower($string));
    }

    /**
     * @throws FHIRPathException
     */
    public function replace(string $string, array $arguments): StringLiteral|array
    {
        if (count($arguments) != 2) {
            throw FHIRPathException::invalidArgument('replace', '', 'Expects 2 arguments.');
        }

        $pattern      = reset($arguments)->getValue($this->info);
        $substitution = $arguments[1]->getValue($this->info);

        if ((count($pattern) == 0) || (count($substitution) == 0)) {
            return [];
        }

        if ((count($pattern) > 1) || !is_a(reset($pattern), FHIRStringInterface::class)) {
            throw FHIRPathException::expectedSingleElement('replace', 'string');
        }

        if ((count($substitution) > 1) || !is_a(reset($substitution), FHIRStringInterface::class)) {
            throw FHIRPathException::expectedSingleElement('replace', 'string');
        }

        $pattern      = reset($pattern)->getValue();
        $substitution = reset($substitution)->getValue();

        if ($pattern === '') {
            $result = $substitution . implode($substitution, str_split($string)) . $substitution;
        } else {
            $result = str_replace($pattern, $substitution, $string);
        }

        return (new StringLiteral())->setValue($result);
    }

    /**
     * @throws FHIRPathException
     */
    public function matches(string $string, array $arguments): BooleanLiteral|array
    {
        if (count($arguments) == 0) {
            return [];
        }

        if ((count($arguments) > 1) || !is_a(reset($arguments), FHIRStringInterface::class)) {
            throw FHIRPathException::expectedSingleElement('matches', 'string');
        }

        $regex = reset($arguments)->getValue();

        $result = preg_match("#$regex#", $string);

        if (!$result) {
            return (new BooleanLiteral())->setValue(false);
        }

        return (new BooleanLiteral())->setValue(true);
    }

    public function length(string $string): IntegerLiteral
    {
        return (new IntegerLiteral())->setValue(strlen($string));
    }

    public function toChars(string $string): array
    {
        $chars = [];

        foreach (str_split($string) as $char) {
            $chars[] = (new StringLiteral())->setValue($char);
        }

        return $chars;
    }

    /**
     * @throws FHIRPathException
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    public function processFunction(array $input, ExpressionsInformation $info): mixed
    {
        if ((count($input) == 0)) {
            return [];
        }

        if ((count($input) > 1) || !is_a(reset($input), FHIRStringInterface::class)) {
            throw FHIRPathException::expectedSingleInput($this->function, 'string');
        }
        /** @var string $string */
        $string = reset($input)->getValue();

        $this->info = $info;

        $arguments = $this->arguments->getValue($this->info);

        return $this->{$this->function}($string, $arguments);
    }
}
