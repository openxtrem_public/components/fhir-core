<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions\Functions\Types;

use Ox\Components\FHIRCore\Definition\Field;
use Ox\Components\FHIRCore\Definition\ResourceDefinition;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\StringLiteral;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRElementInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRStringInterface;
use Ox\Components\FHIRCore\Interfaces\StructureDefinitionInterface;

class SimpleTypeInfo implements FHIRElementInterface
{
    final public const RESOURCE_NAME = 'INFO';

    public string $namespace;
    public string $name;
    public string $baseType;

    /**
     * @return string
     */
    public function getNamespace(): FHIRStringInterface
    {
        return (new StringLiteral())->setValue($this->namespace);
    }

    /**
     * @return string
     */
    public function getName(): FHIRStringInterface
    {
        return (new StringLiteral())->setValue($this->name);
    }

    /**
     * @return string
     */
    public function getBaseType(): FHIRStringInterface
    {
        return (new StringLiteral())->setValue($this->baseType);
    }

    public function __construct()
    {
    }

    public function getFhirResourceDefinition(): ResourceDefinition
    {
        return (new ResourceDefinition('SimpleTypeInfo'))
            ->setObjectName(self::class)
            ->addFields(...$this->getFields());
    }

    protected function getFields(): array
    {
        return [
            (new Field())
                ->setLabel('namespace')
                ->setUnique(true)
                ->setNullable(false)
                ->setTypes(["string" => true]),
            (new Field())
                ->setLabel('name')
                ->setUnique(true)
                ->setNullable(false)
                ->setTypes(["string" => true]),
            (new Field())
                ->setLabel('baseType')
                ->setUnique(true)
                ->setNullable(false)
                ->setTypes(["string" => true]),
        ];
    }

    public static function getFhirStructureDefinition(): string
    {
        return StructureDefinitionInterface::class;
    }
}
