<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions\Functions\Types;

use Ox\Components\FHIRCore\Definition\Field;
use Ox\Components\FHIRCore\Definition\ResourceDefinition;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\BooleanLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\StringLiteral;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRElementInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRBooleanInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRStringInterface;
use Ox\Components\FHIRCore\Interfaces\StructureDefinitionInterface;

class ClassInfoElement implements FHIRElementInterface
{
    final public const RESOURCE_NAME = 'INFO';

    public string $name;
    public string $type;
    public bool   $isOneBased;

    /**
     * @return string
     */
    public function getName(): FHIRStringInterface
    {
        return (new StringLiteral())->setValue($this->name);
    }

    /**
     * @return string
     */
    public function getType(): FHIRStringInterface
    {
        return (new StringLiteral())->setValue($this->type);
    }

    /**
     * @return bool
     */
    public function isOneBased(): FHIRBooleanInterface
    {
        return (new BooleanLiteral())->setValue($this->isOneBased);
    }
    public function __construct()
    {
    }

    public function getFhirResourceDefinition(): ResourceDefinition
    {
        return (new ResourceDefinition('ClassInfoElement'))
            ->setObjectName(self::class)
            ->addFields(...$this->getFields());
    }

    protected function getFields(): array
    {
        return [
            (new Field())
                ->setLabel('name')
                ->setUnique(true)
                ->setNullable(false)
                ->setTypes(["string" => true]),
            (new Field())
                ->setLabel('type')
                ->setUnique(true)
                ->setNullable(false)
                ->setTypes(["string" => true]),
            (new Field())
                ->setLabel('isOneBased')
                ->setUnique(true)
                ->setNullable(false)
                ->setTypes(["bool" => true]),
        ];
    }

    public static function getFhirStructureDefinition(): string
    {
        return StructureDefinitionInterface::class;
    }
}
