<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions\Functions\Types;

use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\LiteralInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRBackboneElementInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRBaseInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRElementInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRQuantityInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRBooleanInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRDateInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRDateTimeInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRDecimalInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRIntegerInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRStringInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRTimeInterface;

class Types
{
    public function getTypes(array $elements): array
    {
        return array_map(fn($e) => $this->getType($e), $elements);
    }

    private function getType(FHIRBaseInterface $resource): SimpleTypeInfo
    {
        if (
            is_a($resource, FHIRBooleanInterface::class) or
            is_a($resource, FHIRIntegerInterface::class) or
            is_a($resource, FHIRDecimalInterface::class) or
            is_a($resource, FHIRStringInterface::class) or
            is_a($resource, FHIRDateInterface::class) or
            is_a($resource, FHIRDateTimeInterface::class) or
            is_a($resource, FHIRTimeInterface::class) or
            is_a($resource, FHIRQuantityInterface::class)
        ) {
            $typeInfo = new SimpleTypeInfo();
            if ($resource instanceof LiteralInterface) {
                $typeInfo->namespace = 'System';
                $typeInfo->name      = $resource::LITERAL_NAME;
            } else {
                /** @var FHIRElementInterface $resource */
                $typeInfo->namespace = 'FHIR';
                $typeInfo->name      = $resource::RESOURCE_NAME;
            }

            $typeInfo->baseType = $typeInfo->namespace . '.Any';

            return $typeInfo;
        }

        $parent = get_parent_class($resource) ? get_parent_class($resource)::RESOURCE_NAME : 'Any';

        if (is_subclass_of($resource, FHIRBackboneElementInterface::class)) {
            $info = new TupleTypeInfo();
        } else {
            $info            = new ClassInfo();
            $info->namespace = 'FHIR';
            $info->name      = $resource::RESOURCE_NAME;
            $info->baseType  = 'FHIR.' . $parent;
        }

        $info->element = [];

        return $info;
    }
}
