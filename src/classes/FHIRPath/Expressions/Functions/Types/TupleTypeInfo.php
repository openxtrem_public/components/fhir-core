<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions\Functions\Types;

use Ox\Components\FHIRCore\Definition\Field;
use Ox\Components\FHIRCore\Definition\ResourceDefinition;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRElementInterface;
use Ox\Components\FHIRCore\Interfaces\StructureDefinitionInterface;

class TupleTypeInfo implements FHIRElementInterface
{
    final public const RESOURCE_NAME = 'INFO';

    public array $element;

    /**
     * @return array
     */
    public function getElement(): array
    {
        return $this->element;
    }

    public function getFhirResourceDefinition(): ResourceDefinition
    {
        return (new ResourceDefinition('ClassInfo'))
            ->setObjectName(self::class)
            ->addFields(...$this->getFields());
    }

    protected function getFields(): array
    {
        $fields   = [];
        $fields[] = (new Field())
            ->setLabel('element')
            ->setUnique(false)
            ->setNullable(false)
            ->setTypes(["???" => false]);

        return $fields;
    }

    public static function getFhirStructureDefinition(): string
    {
        return StructureDefinitionInterface::class;
    }
}
