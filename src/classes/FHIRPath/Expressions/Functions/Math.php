<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions\Functions;

use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRLiteralFound;
use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRPathException;
use Ox\Components\FHIRCore\FHIRPath\Exception\SystemLiteralFound;
use Ox\Components\FHIRCore\FHIRPath\Expressions\ExpressionsInformation;
use Ox\Components\FHIRCore\FHIRPath\Expressions\FunctionInvocation;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\DecimalLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\IntegerLiteral;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRQuantityInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRDecimalInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRIntegerInterface;

class Math extends FunctionInvocation implements FunctionHandler
{
    final public const METHODS = [
        'isOneNumber',
        'abs',
        'ceiling',
        'exp',
        'floor',
        'ln',
        'log',
        'power',
        'round',
        'sqrt',
        'truncate',
    ];

    /**
     * @throws FHIRPathException
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    public function processFunction(array $input, ExpressionsInformation $info): mixed
    {
        if ((count($input) == 0)) {
            return [];
        }

        if (!$this->isOneNumber($input)) {
            throw FHIRPathException::expectedSingleInput($this->function, 'number');
        }

        $number = reset($input);

        $arguments = $this->arguments->getValue($info);

        return $this->{$this->function}($number, $arguments);
    }

    private function isOneNumber(array $input): bool
    {
        if (count($input) != 1) {
            return false;
        }

        $first = reset($input);

        return is_a($first, FHIRIntegerInterface::class) or
            is_a($first, FHIRDecimalInterface::class) or
            is_a($first, FHIRQuantityInterface::class);
    }

    public function abs(mixed $number): IntegerLiteral|FHIRDecimalInterface|FHIRQuantityInterface|array
    {
        if (is_a($number, FHIRQuantityInterface::class)) {
            $abs = abs($number->getValue()->getValue());
            $number->getValue()->setValue($abs);

            return $number;
        }

        $abs = abs($number->getValue());

        return $number->setValue($abs);
    }

    public function ceiling(mixed $number): IntegerLiteral|array
    {
        $value = ceil($number->getValue());

        return (new IntegerLiteral())->setValue($value);
    }

    public function exp(mixed $number): DecimalLiteral|array
    {
        $value = exp($number->getValue());

        return (new DecimalLiteral())->setValue($value);
    }

    public function floor(mixed $number): IntegerLiteral|array
    {
        $value = floor($number->getValue());

        return (new IntegerLiteral())->setValue($value);
    }

    public function ln(mixed $number): DecimalLiteral|array
    {
        $value = log($number->getValue());

        return (new DecimalLiteral())->setValue($value);
    }

    /**
     * @throws FHIRPathException
     */
    public function log(mixed $number, array $arguments): DecimalLiteral|array
    {
        if (count($arguments) == 0) {
            return [];
        }

        if (!$this->isOneNumber($arguments)) {
            throw FHIRPathException::expectedSingleElement('log', 'number');
        }

        $value = log($number->getValue(), reset($arguments)->getValue());

        return (new DecimalLiteral())->setValue($value);
    }

    /**
     * @throws FHIRPathException
     */
    public function power(mixed $number, array $arguments): DecimalLiteral|FHIRIntegerInterface|array
    {
        if (count($arguments) == 0) {
            return [];
        }

        if (!$this->isOneNumber($arguments)) {
            throw FHIRPathException::expectedSingleElement('power', 'number');
        }

        $value = $number->getValue() ** reset($arguments)->getValue();

        if (is_nan($value)) {
            return [];
        } elseif (is_integer($value)) {
            return (new IntegerLiteral())->setValue($value);
        } elseif (is_double($value)) {
            return (new DecimalLiteral())->setValue($value);
        } else {
            return [];
        }
    }

    /**
     * @throws FHIRPathException
     */
    public function round(mixed $number, array $arguments): DecimalLiteral|array
    {
        if (count($arguments) == 1) {
            if (!is_a(reset($arguments), FHIRIntegerInterface::class)) {
                throw FHIRPathException::expectedSingleElement('round', 'integer');
            }
            $precision = reset($arguments)->getValue();
            if ($precision < 0) {
                throw FHIRPathException::invalidArgument('round', $precision, 'Must be >= 0.');
            }
        } else {
            $precision = 0;
        }


        $value = round($number->getValue(), $precision);

        return (new DecimalLiteral())->setValue($value);
    }

    /**
     * @throws FHIRPathException
     */
    public function sqrt(mixed $number): DecimalLiteral|array
    {
        $argument = (new DecimalLiteral())->setValue(0.5);

        return $this->power($number, [$argument]);
    }

    public function truncate(mixed $number): IntegerLiteral|array
    {
        return (new IntegerLiteral())->setValue((int)($number->getValue()));
    }
}
