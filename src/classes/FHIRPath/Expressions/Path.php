<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions;

use Exception;
use Ox\Components\FHIRCore\Definition\Field;
use Ox\Components\FHIRCore\Definition\ResourceDefinition;
use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRLiteralFound;
use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRPathException;
use Ox\Components\FHIRCore\FHIRPath\Exception\SystemLiteralFound;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\IntegerLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\NotAValidLiteral;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRBaseInterface;
use Ox\Components\FHIRCore\Interfaces\StructureDefinitionInterface;
use Ox\Components\FHIRCore\Utils\Comparisons;
use Throwable;

class Path extends AbstractExpression
{
    public function __construct(private ?ExpressionInterface $left, private string $path)
    {
    }

    /**
     *
     * @param ExpressionsInformation $information *
     *
     * @return array
     * @throws FHIRPathException
     * @throws SystemLiteralFound
     * @throws FHIRLiteralFound
     */
    public function getValue(ExpressionsInformation $information = new ExpressionsInformation()): array
    {
        if ($this->path === '{}') {
            return [];
        }

        if ($this->path === 'System') {
            throw SystemLiteralFound::systemLiteral();
        }
        if ($this->path === 'FHIR') {
            throw FHIRLiteralFound::fhirLiteral();
        }

        if (str_starts_with($this->path, '%')) {
            $key = substr($this->path, 1);

            if (!array_key_exists($key, $information->variables)) {
                throw FHIRPathException::undefinedVariable($key);
            }

            return [$information->variables[substr($this->path, 1)]];
        }

        if (is_null($this->left)) {
            // might be in a function
            if ($this->path === '$this') {
                return [$information->invokeThis];
            } elseif ($this->path === '$index') {
                return [(new IntegerLiteral())->setValue($information->index)];
            } elseif ($this->path === '$total') {
                return $information->total;
            }

            /** @var class-string $structure_definition */
            $structure_definition = $information->element::getFhirStructureDefinition();

            // In case of type
            if ($information->type) {
                if ($information->total === 'FHIR') {
                    return [new ($structure_definition::getResourceDefinition($this->path)->getObjectName())()];
                } elseif ($information->total === 'System') {
                    try {
                        $className = 'Ox\Components\FHIRCore\FhirPath\Expressions\Literals\\' .
                            $this->path .
                            'Literal';

                        return [new $className()];
                    } catch (Throwable) {
                        return [new NotAValidLiteral()];
                    }
                } else {
                    try {
                        return [new ($structure_definition::getResourceDefinition($this->path)->getObjectName())()];
                    } catch (Throwable) {
                        // FhirPath Literal
                        $className = 'Ox\Components\FHIRCore\FhirPath\Expressions\Literals\\' .
                            $this->path .
                            'Literal';
                        try {
                            return [new $className()];
                        } catch (Throwable) {
                            throw FHIRPathException::invalidType($this->path);
                        }
                    }
                }
            }

            // We can omit $this OR BASE keyword to get attributes
            // .where(use = 'official') -> .where($this.use = 'official')
            // name.given -> Patient.name.given
            // TODO : check with definition (not merged atm)
            // TODO : type context (know which fields are available for current context)
            $getter = 'get' . ucfirst($this->path);
            if (
                $information->invokeThis &&
                method_exists($information->invokeThis, $getter)
            ) { // Inside function parameters
                $collection = $information->invokeThis;
            } elseif (method_exists($information->element, $getter)) { // First elem of fhir path
                $collection = $information->element;
            } else {
                // Not in a function special case
                // First element of the Path -> resource Type

                // TODO : parents resources (Patient is a match for Resource)
                if ($this->path !== $information->element::RESOURCE_NAME) {
                    return [];
                }

                return [$information->element];
            }
        } else {
            try {
                $collection = $this->left->getValue($information);
            } catch (SystemLiteralFound) { // use of $total because he is not used in this case
                // 'System' is for FhirPathTypes
                $this->left         = null;
                $information->total = 'System';

                return $this->getValue($information);
            } catch (FHIRLiteralFound) {
                // 'FHIR' is for Fhir types
                $this->left         = null;
                $information->total = 'FHIR';

                return $this->getValue($information);
            }
        }

        if (!is_array($collection)) {
            $collection = [$collection];
        }

        if (count($collection) == 0) {
            return [];
        }

        $getter = 'get' . ucfirst($this->path);
        $values = [];

        foreach ($collection as $subElement) {
            if (!method_exists($subElement, $getter)) {
                continue;
            }
            if (!is_null($value = $subElement->$getter())) {
                if (!is_array($value)) {
                    $values[] = $value;
                } else {
                    $values = array_merge($values, $value);
                }
            }
        }

        return $values;
    }

    /**
     * @throws FHIRPathException
     */
    private function findField(FHIRBaseInterface $element, string $label): Field
    {
        $filtered = array_filter(
            $element->getFhirResourceDefinition()->getFields(),
            fn ($field) => $field->getLabel() === $label
        );

        $field = reset($filtered);

        if ($field === false) {
            $element_name = $element::RESOURCE_NAME;
            throw new FHIRPathException("Field '$label' not found in '$element_name'.");
        }

        return $field;
    }

    /**
     *
     * @param FHIRBaseInterface|null $value *
     *
     * @inheritDoc
     * @throws FHIRPathException
     * @throws Exception
     */
    public function getTargetToWrite(
        ExpressionsInformation $information = new ExpressionsInformation(),
        ?int                    $target_index = null,
        ?string                $next_label = null,
        ?FHIRBaseInterface     $value = null
    ): array {
        if ($this->left === null) {
            $c = new Comparisons();
            if ($c->isTypeOrSubclass($information->element, $this->path)) {
                return [$information->element];
            }
            // else might be a child
            $collection = [$information->element];
        } else {
            $collection = $this->left->getTargetToWrite($information, $target_index, $this->path);
        }


        $getter = 'get' . ucfirst($this->path);
        $setter = 'set' . ucfirst($this->path);
        $adder  = 'add' . ucfirst($this->path);

        if ($collection === []) {
            return [];
        }

        if ($value) {
            foreach ($collection as $element) {
                $field  = $this->findField($element, $this->path);

                if ($field->isUnique()) {
                    $element->{$setter}($value);
                } elseif ($target_index === -1) {
                    $element->{$adder}($value);
                } elseif ($target_index === null) {
                    $element->{$setter}($value);
                } else {
                    $list = $element->{$getter}();
                    $list[$target_index] = $value;
                    $element->{$setter}(...$list);
                }
            }

            return $collection;
        }

        $new_collection = [];
        foreach ($collection as $element) {
            $field  = $this->findField($element, $this->path);
            $target = $element->{$getter}();
            $definition = $element::getFhirStructureDefinition();

            $type = array_keys($field->getTypes())[0];
            $class = $definition::getResourceDefinition($type)->getObjectName();
            $object = new $class();

            // element that can be multiple : cannot be a choice
            if ($field->isUnique() && (count($field->getTypes()) === 1) && ($target === null)) {
                // unique field unique type null
                $new_collection[] = $object;
                $element->{$setter}($object);
            } elseif ($field->isUnique() && ($target === null)) {
                // multiple types (only unique field)
                if ($next_label === null) {
                    throw new FHIRPathException('You must set has value a FHIR datatype for choice elements.');
                }
                $object           = $this->findClassFromNextLabel($definition, $field, $next_label);
                $new_collection[] = $object;
                $element->{$setter}($object);
            } elseif (!$field->isUnique() && ($target_index === null)) {
                $new_collection = [$object];
                $element->{$setter}($object);
            } elseif (!$field->isUnique() && ($target_index === -1)) {
                $new_collection[] = $object;
                $target[] = $object;
                $element->{$setter}(...$target);
            } elseif (!$field->isUnique() && !array_key_exists($target_index, $target)) {
                // multiple field (only unique type)
                $new_collection[$target_index] = $object;
                $target[$target_index] = $object;
                $element->{$setter}(...$target);
            } elseif (is_array($target)) {
                $new_collection = array_merge($new_collection, $target);
            }

        }

        return $new_collection;
    }

    /**
     * @param class-string $sd
     * @throws FHIRPathException
     */
    private function findClassFromNextLabel(string $sd, Field $field, string $label): object
    {
        foreach ($field->getTypes() as $type => $final) {
            /** @var ResourceDefinition $def */
            $def = $sd::getResourceDefinition($type);
            foreach ($def->getFields() as $field) {
                if ($field->getLabel() === $label) {
                    return new ($def->getObjectName())();
                }
            }
        }

        throw new FHIRPathException('Choice type not found.');
    }
}
