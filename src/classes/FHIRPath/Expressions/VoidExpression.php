<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions;

class VoidExpression extends AbstractExpression
{
    public function getValue(ExpressionsInformation $information = new ExpressionsInformation()): array
    {
        return [];
    }
}
