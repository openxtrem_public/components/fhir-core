<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions;

use Exception;

class Unprocessed extends AbstractExpression
{
    public function __construct(public string $data)
    {
    }

    /**
     * @throws Exception
     */
    public function getValue(ExpressionsInformation $information = new ExpressionsInformation()): array
    {
        throw new Exception("You are not supposed to get value of unprocessed");
    }
}
