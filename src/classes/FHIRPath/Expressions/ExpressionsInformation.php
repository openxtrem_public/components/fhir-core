<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions;

use DateTime;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRBaseInterface;

class ExpressionsInformation
{
    /** @var FHIRBaseInterface $element Context of the FHIRPath */
    public FHIRBaseInterface $element;

    /** @var DateTime $now Datetime for the function now(). Should be always exactly the same in the FhirPath */
    public DateTime $now;

    /** @var FHIRBaseInterface|null $invokeThis $this keyword */
    public FHIRBaseInterface|null $invokeThis = null;

    /** @var int $index $index keyword */
    public int $index = -1;

    /** @var mixed|int $total $total keyword */
    public mixed $total = 0;

    /** @var bool $type For as and is operations & functions */
    public bool $type = false;

    /** @var array $variables Environments Variables */
    public array $variables = [];

    public function __construct()
    {
        $this->now = new DateTime();
    }

    public function copy(): static
    {
        $new             = new ExpressionsInformation();
        $new->element    = $this->element;
        $new->now        = $this->now;
        $new->invokeThis = $this->invokeThis;
        $new->index      = $this->index;
        $new->total      = $this->total;
        $new->type       = $this->type;
        $new->variables  = $this->variables;

        return $new;
    }

    public function getArray(): array
    {
        return [
            $this->element,
            $this->now,
            $this->invokeThis,
            $this->index,
            $this->total,
            $this->type,
            $this->variables,
        ];
    }
}
