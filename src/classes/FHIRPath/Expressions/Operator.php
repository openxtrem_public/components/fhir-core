<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions;

use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRLiteralFound;
use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRPathException;
use Ox\Components\FHIRCore\FHIRPath\Exception\SystemLiteralFound;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\BooleanLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\DecimalLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\IntegerLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\LiteralInterface;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\QuantityLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\StringLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Operators\BooleansLogic;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Operators\Comparison;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Operators\Math;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRBaseInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRDateInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRDateTimeInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRIntegerInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRStringInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRTimeInterface;
use Ox\Components\FHIRCore\Utils\Comparisons;

class Operator extends AbstractExpression
{
    private string             $operator;
    private AbstractExpression $left;
    private AbstractExpression $right;
    private Comparisons        $helpers;

    final public const IMPLIES        = ' implies ';
    final public const XOR            = ' xor ';
    final public const OR             = ' or ';
    final public const AND            = ' and ';
    final public const IN             = ' in ';
    final public const CONTAINS       = ' contains ';
    final public const EQUAL          = '=';
    final public const EQUIVALENT     = '~';
    final public const NOT_EQUAL      = '!=';
    final public const NOT_EQUIVALENT = '!~';
    final public const GREATER        = '>';
    final public const LOWER          = '<';
    final public const GREATER_EQUAL  = '>=';
    final public const LOWER_EQUAL    = '<=';
    final public const UNION          = '|';
    final public const IS             = ' is ';
    final public const AS             = ' as ';
    final public const ADDITION       = '+';
    final public const SUBTRACTION    = '-';
    final public const CONCATENATION  = '&';
    final public const MULTIPLICATION = '*';
    final public const DIVISION       = '/';
    final public const DIV            = 'div';
    final public const MODULO         = 'mod';

    /**
     * @param string             $operator
     * @param AbstractExpression $left
     * @param AbstractExpression $right
     * @param Comparisons|null   $helpers
     */
    public function __construct(
        string              $operator,
        ExpressionInterface $left,
        ExpressionInterface $right,
        Comparisons         $helpers = null
    ) {
        $this->operator = $operator;
        $this->left     = $left;
        $this->right    = $right;
        if (is_null($helpers)) {
            $this->helpers = new Comparisons();
        } else {
            $this->helpers = $helpers;
        }
    }

    /**
     *
     * @param ExpressionsInformation $information *
     *
     * @return array
     * @throws FHIRPathException
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    public function getValue(ExpressionsInformation $information = new ExpressionsInformation()): array
    {
        $left = $this->left->getValue($information);

        if (($this->operator === Operator::IS) || ($this->operator === Operator::AS)) {
            $info       = $information->copy();
            $info->type = true;
            $right      = $this->right->getValue($info);
            $value      = match ($this->operator) {
                self::IS => $this->is($left, $right),
                self::AS => $this->as($left, $right),
            };

            return is_array($value) ? $value : [$value];
        }

        $right = $this->right->getValue($information);

        $value = match ($this->operator) {
            self::EQUAL => $this->equal($left, $right),
            self::NOT_EQUAL => $this->notEqual($left, $right),
            self::EQUIVALENT => $this->equivalent($left, $right),
            self::NOT_EQUIVALENT => $this->notEquivalent($left, $right),
            self::GREATER, self::GREATER_EQUAL, self::LOWER, self::LOWER_EQUAL => $this->comparison(
                $left,
                $right,
                $this->operator
            ),
            self::UNION => (new FunctionInvocation(null, 'distinct', new VoidExpression()))
                ->distinct(array_merge($left, $right)),
            self::IN => $this->in($left, $right),
            self::CONTAINS => $this->in($right, $left),
            self::AND, self::OR, self::XOR, self::IMPLIES => $this->booleans($left, $right, $this->operator),
            self::MULTIPLICATION, self::DIVISION, self::ADDITION, self::SUBTRACTION, self::DIV, self::MODULO =>
            $this->math(
                $left,
                $right,
                $this->operator
            ),
            self::CONCATENATION => $this->concat($left, $right),
            default => [],
        };

        return is_array($value) ? $value : [$value];
    }

    /**
     * @param $left
     * @param $right
     *
     * @return BooleanLiteral[]
     * @throws FHIRPathException
     */
    public function equal(array $left, array $right): array
    {
        if ((sizeof($left) == 0) || (sizeof($right) == 0)) {
            return [];
        }

        if (sizeof($left) != sizeof($right)) {
            return [(new BooleanLiteral())->setValue(false)];
        }

        for ($i = 0; $i < sizeof($left); $i++) {
            if (
                (
                    is_a($left[$i], FHIRTimeInterface::class) ||
                    is_a($left[$i], FHIRDateTimeInterface::class) ||
                    is_a($left[$i], FHIRDateInterface::class)
                ) && (
                    ($left[$i]::RESOURCE_NAME !== $right[$i]::RESOURCE_NAME) ||
                    (!$this->helpers->isSameDateTimePrecision($left[$i], $right[$i]))
                )
            ) {
                return [];
            }

            if (!$this->helpers->equal($left[$i], $right[$i])) {
                return [(new BooleanLiteral())->setValue(false)];
            }
        }

        return [(new BooleanLiteral())->setValue(true)];
    }

    /**
     * @throws FHIRPathException
     */
    public function notEqual(array $left, array $right): BooleanLiteral|array
    {
        $equal = $this->equal($left, $right);

        // [] if [], true if false, false if true
        return (count($equal) == 0) ? [] : reset($equal)->setValue(!reset($equal)->getValue());
    }

    public function equivalent(array $left, array $right): BooleanLiteral|array
    {
        if ((sizeof($left) == 0) && (sizeof($right) == 0)) {
            return (new BooleanLiteral())->setValue(true);
        }

        if (sizeof($left) != sizeof($right)) {
            return (new BooleanLiteral())->setValue(false);
        }

        if (sizeof($left) > 1) {
            foreach ($left as $element) {
                if (!$this->helpers->equivalentInArray($element, $right)) {
                    return (new BooleanLiteral())->setValue(false);
                }
            }

            return (new BooleanLiteral())->setValue(true);
        }

        return (new BooleanLiteral())->setValue($this->helpers->equivalent(reset($left), reset($right)));
    }

    public function notEquivalent(array $left, array $right): BooleanLiteral|array
    {
        $equal = $this->equivalent($left, $right);

        if (is_array($equal)) {
            return $equal;
        }

        return $equal->setValue(!$equal->getValue());
    }

    /**
     * @throws FHIRPathException
     */
    public function comparison(array $left, array $right, string $operator): BooleanLiteral|array
    {
        $comparison = (new Comparison($this->helpers))->comparison($left, $right, $operator);

        if (is_array($comparison)) {
            return [];
        }

        if ($comparison) {
            return (new BooleanLiteral())->setValue(true);
        }

        if (($operator === self::GREATER_EQUAL) || ($operator === self::LOWER_EQUAL)) {
            $equal = $this->equal($left, $right);

            if ((sizeof($equal) == 0) || reset($equal)->getValue()) {
                return $equal;
            }
        }

        return (new BooleanLiteral())->setValue(false);
    }

    /**
     * @throws FHIRPathException
     */
    public function is(array $left, array $right): BooleanLiteral|array
    {
        $right = reset($right);
        if (count($left) > 1) {
            // TODO : exception about multiple args
            throw FHIRPathException::operator('is', '', '');
        }

        if (count($left) === 0) {
            return [];
        }

        if ($right instanceof LiteralInterface) {
            $type = $right::LITERAL_NAME;
        } else {
            $type = $right::RESOURCE_NAME;
        }

        /** @var FHIRBaseInterface $l */
        $l = reset($left);

        return (new BooleanLiteral())->setValue($this->helpers->isTypeOrSubclass($l, $type));
    }

    /**
     * @throws FHIRPathException
     */
    public function as(array $left, array $right): mixed
    {
        $is = $this->is($left, $right);

        if (is_array($is)) {
            return [];
        }

        if ($is->getValue()) {
            return reset($left);
        }

        if ($this->helpers->isTypeOrSubclass(reset($left), reset($right)::RESOURCE_NAME)) {
            return reset($left);
        }

        return [];
    }

    /**
     * @throws FHIRPathException
     */
    public function in(array $left, array $right): BooleanLiteral|array
    {
        if (count($left) === 0) {
            return [];
        }

        if (count($right) === 0) {
            return (new BooleanLiteral())->setValue(false);
        }

        if (count($left) > 1) {
            throw FHIRPathException::expectedSingleInput('in');
        }

        $this->helpers->includes(reset($left), $right);

        return (new BooleanLiteral())
            ->setValue($this->helpers->includes(reset($left), $right));
    }

    /**
     * @throws FHIRPathException
     */
    public function booleans(array $left, array $right, string $operator): BooleanLiteral|array
    {
        $v = (new BooleansLogic())->logic($left, $right, $operator);

        if (is_array($v)) {
            return [];
        }

        return (new BooleanLiteral())->setValue($v);
    }

    /**
     * @throws FHIRPathException
     */
    public function math(
        array  $left,
        array  $right,
        string $operator
    ): StringLiteral|IntegerLiteral|DecimalLiteral|QuantityLiteral|array {
        if ((count($left) == 0) || (count($right) == 0)) {
            return [];
        }

        if ((count($left) > 1) || (count($right) > 1)) {
            throw FHIRPathException::operator($operator, 'multiple', '');
        }

        $left  = reset($left);
        $right = reset($right);

        if (($operator === self::DIVISION) && is_a($left, FHIRIntegerInterface::class)) {
            $left = (new DecimalLiteral())->setValue($left->getValue());
        }

        $operands = $this->helpers->implicitConversion($left, $right);
        $value    = (new Math())->math($operands[0], $operands[1], $operator);

        if (is_array($value)) {
            return [];
        }

        return $left->setValue($value);
    }

    /**
     * @throws FHIRPathException
     */
    public function concat(array $left, array $right): StringLiteral
    {
        if ((count($left) == 0)) {
            $left = '';
        } else {
            if ((count($left) > 1) || !is_a(reset($left), FHIRStringInterface::class)) {
                throw FHIRPathException::operator('&', '', '');
            }
            $left = reset($left)->getValue();
        }

        if ((count($right) == 0)) {
            $right = '';
        } else {
            if ((count($right) > 1) || !is_a(reset($right), FHIRStringInterface::class)) {
                throw FHIRPathException::operator('&', '', '');
            }
            $right = reset($right)->getValue();
        }

        return (new StringLiteral())->setValue($left . $right);
    }
}
