<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions;

use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRLiteralFound;
use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRPathException;
use Ox\Components\FHIRCore\FHIRPath\Exception\SystemLiteralFound;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRBaseInterface;

abstract class AbstractExpression implements ExpressionInterface
{
    /**
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    abstract public function getValue(ExpressionsInformation $information = new ExpressionsInformation()): array;

    /**
     * @throws FHIRPathException
     */
    public function getTargetToWrite(
        ExpressionsInformation $information = new ExpressionsInformation(),
        ?int                    $target_index = null,
        ?string                $next_label = null,
        ?FHIRBaseInterface     $value = null
    ): array {
        throw new FHIRPathException('Only path are allowed in a write expression.');
    }
}
