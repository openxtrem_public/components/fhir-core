<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions;

use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRBaseInterface;

class Subset extends AbstractExpression
{
    public function __construct(public ExpressionInterface $data, private ?int $index)
    {
    }

    public function getValue(ExpressionsInformation $information = new ExpressionsInformation()): array
    {
        $collection = $this->data->getValue($information);

        if (array_key_exists($this->index, $collection)) {
            return [$collection[$this->index]];
        }

        return [];
    }

    public function getTargetToWrite(
        ExpressionsInformation $information = new ExpressionsInformation(),
        ?int                    $target_index = null,
        ?string                $next_label = null,
        ?FHIRBaseInterface     $value = null
    ): array {
        $target_index = $this->index;
        if ($this->index === null) {
            $target_index = -1;
        }
        $source = $this->data->getTargetToWrite($information, $target_index, $next_label, $value);

        if ($value !== null) {
            return $source;
        }

        if ($this->index === null) {
            return [end($source)];
        }

        return [$source[$this->index]];
    }
}
