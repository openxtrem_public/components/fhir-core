<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Expressions;

use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRLiteralFound;
use Ox\Components\FHIRCore\FHIRPath\Exception\SystemLiteralFound;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRBaseInterface;

interface ExpressionInterface
{
    /**
     * @throws FHIRLiteralFound
     * @throws SystemLiteralFound
     */
    public function getValue(ExpressionsInformation $information = new ExpressionsInformation()): array;

    /**
     * @param ExpressionsInformation $information
     * @param int                    $target_index in case of use of [] operator
     * @param string|null            $next_label   in case of choice element
     * @param FHIRBaseInterface|null $value
     *
     * @return array
     */
    public function getTargetToWrite(
        ExpressionsInformation $information = new ExpressionsInformation(),
        int                    $target_index = 0,
        ?string                $next_label = null,
        ?FHIRBaseInterface     $value = null
    ): array;
}
