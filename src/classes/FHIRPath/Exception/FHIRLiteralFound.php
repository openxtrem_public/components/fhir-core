<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Exception;

use Exception;

/**
 * Description
 */
class FHIRLiteralFound extends Exception
{
    public static function fhirLiteral(): self
    {
        // thrown when keyword 'FHIR' is found before element, used for types
        // Eg : FHIR.Patient, FHIR.date, FHIR.Resource
        return new self("Should not be thrown.");
    }
}
