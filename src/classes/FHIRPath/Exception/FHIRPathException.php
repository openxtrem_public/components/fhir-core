<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Exception;

use Exception;

/**
 * Description
 */
class FHIRPathException extends Exception
{
    public static function unprocessable(string $a, string $b): self
    {
        return new self("FhirPath unprocessable around '$a' and '$b'.");
    }

    public static function pathNotFound(string $path): self
    {
        return new self("'$path' has not been found.");
    }

    public static function operationOnMultiple(string $operation): self
    {
        return new self("Operation '$operation' is used on a collection");
    }

    public static function invalidFunction(string $function): self
    {
        return new self("'$function' is not a valid function.");
    }

    public static function expectedSingleElement(string $function, string $type = null): self
    {
        $type = $type ? "of type '$type'" : "";

        return new self("Function '$function' can accept as argument only a collection with a single element $type.");
    }

    public static function operator(string $operator, mixed $left, mixed $right): self
    {
        if (($left === '') && ($right === '')) {
            return new self("Could not perform operation around operator '$operator'.");
        }

        $lType = getType($left);
        $rType = getType($right);

        return new self("Could not perform operation around operator '$operator' between '$lType' and '$rType'.");
    }

    public static function expectedSingleInput(string $function, string $type = null): self
    {
        $type = $type ? " of type '$type'." : ".";

        return new self("Function '$function' can accept in input only a collection with a single element$type");
    }

    public static function invalidArgument(string $function, string $argument, string $msg = ''): self
    {
        return new self("Invalid argument '$argument' for function '$function'. $msg");
    }

    public static function implicitConversion(string $first, string $second): self
    {
        return new self("Could not perform implicit conversion between '$first' and '$second'.");
    }

    public static function undefinedVariable(string $variable): self
    {
        return new self("The variable '$variable' is not set.");
    }

    public static function expectedSingleOutput(int $length): self
    {
        return new self("Expected a single output from the expression, got a collection of $length.");
    }

    public static function expectedBooleanOutput(mixed $got): self
    {
        $type = is_object($got) ? $got::class : gettype($got);

        return new self("Expected an output of type FHIRBoolean, got element of type '$type'.");
    }

    public static function unaryOperators(string $op): self
    {
        return new self("Unary operator '$op' cannot process those kind of value.");
    }

    public static function invalidInput(string $fun): self
    {
        return new self("Function '$fun' cannot process this input.");
    }

    public static function invalidType(string $type): self
    {
        return new self("Type '$type' is invalid.");
    }
}
