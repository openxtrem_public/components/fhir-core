<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath\Exception;

use Exception;

class SystemLiteralFound extends Exception
{
    public static function systemLiteral(): self
    {
        // thrown when keyword 'System' is found before element, used for system types
        // Eg : System.Patient (does not exists), System.String, System.Boolean
        return new self("Should not be thrown.");
    }
}
