<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\FHIRPath;

use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRPathException;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Arguments;
use Ox\Components\FHIRCore\FHIRPath\Expressions\AbstractExpression;
use Ox\Components\FHIRCore\FHIRPath\Expressions\ExpressionInterface;
use Ox\Components\FHIRCore\FHIRPath\Expressions\FunctionInvocation;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Identifier;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literal;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Modifier;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Operator;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Path;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Subset;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Unprocessed;
use Ox\Components\FHIRCore\FHIRPath\Expressions\VoidExpression;

/**
 * Description
 */
class PathParser
{
    private array $fhirPath = [];

    /**
     * @throws FHIRPathException
     */
    public function __construct(string $fhirPath)
    {
        $this->fhirPath = $this->separateIdentifiers($fhirPath);

        // Important order
        $pre_processable = [
            Literal::STRING,
            Literal::QUANTITY,
            Literal::DATETIME, // for '-' and '.'
            Literal::TIME, // for '.' in time
            Literal::DATE, // for '-' in dates
            Literal::DECIMAL,
        ];

        foreach ($pre_processable as $type) {
            $this->fhirPath = $this->separateLiteralType($this->fhirPath, $type);
        }

        $this->fhirPath = $this->groupNumberStringAsQuantity($this->fhirPath);
    }

    /**
     * @throws FHIRPathException
     */
    public function getExpression(): ExpressionInterface
    {
        return $this->parseUnprocessed($this->getGroups());
    }

    public function getGroups(): array
    {
        return $this->groupUnprocessed($this->fhirPath);
    }

    /**
     * @param AbstractExpression[] $data
     *
     * @return array
     * @throws FHIRPathException
     */
    private function separateIdentifiers(string $data): array
    {
        $expressionArray = [];

        $explode = preg_split("/(?<![^\\\]\\\)`/", $data);
        if (sizeof($explode) % 2 == 0) {
            throw FHIRPathException::unprocessable($data, 'incorrect number of "`"');
        }

        for ($i = 0; $i < sizeof($explode); $i++) {
            if ($i % 2) {
                $expressionArray[] = new Identifier($explode[$i]);
            } else {
                $expressionArray[] = new Unprocessed($explode[$i]);
            }
        }

        return $expressionArray;
    }

    /**
     * Groups parenthesis in nested array. Eg : ['aaa(bb', 'b).cc'] => ['aaa', ['bb', 'b'], '.cc']
     *
     * @param array $expression
     *
     * @return array
     */
    public function groupUnprocessed(array $expression): array
    {
        $final = [];

        $current_group     = [];
        $parenthesis_count = 0;
        foreach ($expression as $part) {
            $idx = 0;
            if (is_a($part, Unprocessed::class)) {
                $first_in_current = false;
                for ($i = 0; $i < strlen($part->data); $i++) {
                    if (($part->data[$i] === '(') && (($i == 0) || ($part->data[$i - 1] !== '\\'))) {
                        if ($parenthesis_count == 0) {
                            $first_in_current = true;
                            if ('' !== $str = substr($part->data, $idx, $i - $idx)) {
                                $final[] = new Unprocessed($str);
                            }
                            $idx = $i;
                        }
                        ++$parenthesis_count;
                    }

                    if (($part->data[$i] === ')') && (($i == 0) || ($part->data[$i - 1] !== '\\'))) {
                        --$parenthesis_count;
                        if ($parenthesis_count == 0) {
                            if ($i != 0) {
                                $index           = $first_in_current ? ($idx + 1) : $idx;
                                $current_group[] = new Unprocessed(
                                    substr(
                                        $part->data,
                                        $index,
                                        $i - $index
                                    )
                                );
                            }

                            $idx           = $i + 1;
                            $final[]       = $this->groupUnprocessed($current_group);
                            $current_group = [];
                        }
                    }
                }

                if ($parenthesis_count == 0) {
                    if ('' !== $v = substr($part->data, $idx)) {
                        $final[] = new Unprocessed($v);
                    }
                } elseif ('' !== $v = substr($part->data, $first_in_current ? ($idx + 1) : 0)) {
                    // +1 in case of parenthesis first char
                    $current_group[] = new Unprocessed($v);
                }
            } elseif ($parenthesis_count == 0) {
                $final[] = $part;
            } else {
                $current_group[] = $part;
            }
        }

        return $final;
    }

    final public const PRECEDENCE = [
        [' implies '],
        [' xor ', ' or '],
        [' and '],
        [' in ', ' contains '],
        ['!=', '!~', '=', '~'], // Order is important
        ['>=', '<=', '>', '<'], // Order is important
        ['|'],
        [' is ', ' as '],
        ['+', '-', '&'],
        ['*', '/', 'div', 'mod'],
    ];

    /**
     * @param array $data
     *
     * @return array
     *
     * trim spaces and delete empty
     */
    private function normalizeArray(array $data): array
    {
        $kept = [];

        foreach ($data as $part) {
            if (is_a($part, Unprocessed::class)) {
                $part->data = trim($part->data);
                if ($part->data !== '') {
                    $kept[] = $part;
                }
            } elseif (is_array($part)) {
                $kept[] = $this->normalizeArray($part);
            } else {
                $kept[] = $part;
            }
        }

        return $kept;
    }

    /**
     * @throws FHIRPathException
     */
    public function parseUnprocessed(array $data): ?ExpressionInterface
    {
        $data = $this->normalizeArray($data);

        if (count($data) == 0) {
            return new VoidExpression();
        }

        if (sizeof($data) == 1) {
            if (is_array(reset($data))) {
                return $this->parseUnprocessed(reset($data));
            }
            if (is_string(reset($data))) {
                $data = [new Unprocessed(reset($data))];
            }
            if (is_a(reset($data), Identifier::class)) {
                return new Path(null, reset($data)->data);
            }
            if (!is_a(reset($data), Unprocessed::class)) {
                return reset($data);
            }
        }

        if ($arguments = $this->createFunctionArguments($data)) {
            return $arguments;
        }

        if ($operator = $this->createOperatorExpression($data)) {
            return $operator;
        }

        if ($modifier = $this->createModifier($data)) {
            return $modifier;
        }

        if ($subset = $this->createSubset($data)) {
            return $subset;
        }

        if ($path = $this->createPathOrFunctionOrLiteral($data)) {
            return $path;
        }

        return null;
    }

    /**
     * @throws FHIRPathException
     */
    public function createFunctionArguments(array $data): ?Arguments
    {
        $arguments = [];
        $current   = [];

        foreach ($data as $part) {
            if (is_a($part, Unprocessed::class)) {
                $explode = explode(',', $part->data);
                if (sizeof($explode) < 2) {
                    $current[] = $part;
                    continue;
                }
                $arguments[] = $this->parseUnprocessed([...$current, new Unprocessed($explode[0])]);
                foreach (array_slice($explode, 1, -1) as $subPart) {
                    $arguments[] = $this->parseUnprocessed([new Unprocessed($subPart)]);
                }
                $current = [new Unprocessed(end($explode))];
            } else {
                $current[] = $part;
            }
        }

        // no comma found, not an argument type (at least not multiple)
        if (count($arguments) == 0) {
            return null;
        }

        // last argument
        $arguments[] = $this->parseUnprocessed($current);

        return new Arguments(...$arguments);
    }


    /**
     * @throws FHIRPathException
     */
    public function createOperatorExpression(array $data): ?Operator
    {
        foreach (self::PRECEDENCE as $operators) {
            foreach ($operators as $operator) {
                $idx = -1;
                foreach ($data as $part) {
                    $idx++;
                    if (is_a($part, Unprocessed::class)) {
                        // ' ' in case where first of last space for specifics operators are trim TODO : check no pb
                        $explode = explode($operator, ' ' . $part->data . ' ', 2);
                        if (sizeof($explode) != 2) {
                            continue;
                        }
                        $explode[0] = substr($explode[0], 1); // deleted added front space
                        $explode[1] = substr($explode[1], 0, -1); // deleted added last space
                        if (($idx == 0) && (strlen($explode[0]) == 0) && in_array($operator, ['+', '-'])) {
                            // not unary + or -
                            continue;
                        }
                        $before = substr($explode[0], -1);
                        if (
                            $before and
                            ($operator === Operator::EQUAL) and
                            in_array($before, [Operator::LOWER, Operator::GREATER])
                        ) {
                            // operator <= or >=
                            continue;
                        }
                        $left   = array_slice($data, 0, $idx);
                        $right  = [new Unprocessed($explode[1])];
                        $left[] = new Unprocessed($explode[0]);
                        if ($idx + 1 < count($data)) {
                            $right = array_merge($right, array_slice($data, $idx + 1));
                        }

                        $lProcessed = $this->parseUnprocessed($left);
                        $rProcessed = $this->parseUnprocessed($right);
                        if (is_a($lProcessed, VoidExpression::class) || is_a($rProcessed, VoidExpression::class)) {
                            continue;
                        }

                        return new Operator($operator, $lProcessed, $rProcessed);
                    }
                }
            }
        }

        return null;
    }

    /**
     * Create modifier id data starts with + or -
     *
     * @param array $data
     *
     * @return Modifier|null
     * @throws FHIRPathException
     */
    public function createModifier(array $data): ?Modifier
    {
        $part = reset($data);

        if (!is_a($part, Unprocessed::class)) {
            return null;
        }

        $content = $part->data;
        if (($content[0] === Modifier::PLUS) || ($content[0] === Modifier::MINUS)) {
            if ('' !== $remains = substr($content, 1)) {
                $expression = array_merge([new Unprocessed($remains)], array_slice($data, 1));
            } else {
                $expression = array_slice($data, 1);
            }

            return new Modifier($content[0], $this->parseUnprocessed($expression));
        }

        return null;
    }

    /**
     * Search for index [?]
     *
     * @param array $data
     *
     * @return Subset|null
     * @throws FHIRPathException
     */
    public function createSubset(array $data): ?ExpressionInterface
    {
        $idx = 0;
        foreach ($data as $part) {
            if (is_a($part, Unprocessed::class)) {
                $explode = preg_split('/\[\d*]/', $part->data, 2);
                if (sizeof($explode) == 2) {
                    $left   = array_slice($data, 0, $idx);
                    $right  = array_slice($data, $idx + 1);
                    $left[] = new Unprocessed($explode[0]);
                    $right  = [new Unprocessed($explode[1]), ...$right];
                    $right  = $this->normalizeArray($right);
                    preg_match('/\[(\d*)]/', $part->data, $matches);
                    $index_value = ($matches[1] !== "") ? intval($matches[1]) : null;
                    $left = new Subset($this->parseUnprocessed($left), $index_value);
                    if (empty($right)) {
                        return $left;
                    } else {
                        return $this->parseUnprocessed([$left, ...$right]);
                    }
                }
            }
            $idx++;
        }

        return null;
    }

    /**
     * @param array $data
     *
     * @return Path|Literal|null
     * @throws FHIRPathException
     */
    public function createPathOrFunctionOrLiteral(array $data): ?ExpressionInterface
    {
        $remains = $data;
        $next    = null; // may be function arguments
        foreach (array_reverse($data) as $part) {
            if (is_a($part, Unprocessed::class)) {
                $idx = strrpos($part->data, '.');
                if ($idx || ($idx === 0)) {
                    array_pop($remains);
                    $left     = $remains;
                    $leftPart = substr($part->data, 0, $idx);
                    if (($leftPart !== false) && ($leftPart !== '')) { // dont avoid "0"
                        $left = array_merge($remains, [new Unprocessed($leftPart)]);
                    }
                    $left = $this->parseUnprocessed($left);
                    if ($idx === strlen($part->data) - 1) {
                        // . is the last char
                        if (is_a($next, Identifier::class)) {
                            return new Path($left, $next->data);
                        } else {
                            throw FHIRPathException::unprocessable($part->data, $next->data);
                        }
                    } else {
                        $right = substr($part->data, $idx + 1);
                    }
                } else {
                    // resource, single element or literal
                    $right = $part->data;
                    $left  = null;
                }
                if (is_array($next)) {
                    // function
                    return new FunctionInvocation($left, $right, $this->parseUnprocessed($next));
                } elseif ($type = Literal::isLiteral($right)) {
                    return new Literal($right, $type);
                } else {
                    if (($right === trim('%')) && is_a($next, Identifier::class)) {
                        // might be a var with name in identifier
                        return new Path($left, '%' . $next->data);
                    }

                    // simple path
                    return new Path($left, $right);
                }
            }
            $next = $part;
            array_pop($remains);
        }

        return null;
    }

    private function separateLiteralType(array $fhirPath, string $type): array
    {
        $pattern = Literal::PATTERNS[$type];
        $kept    = [];

        foreach ($fhirPath as $part) {
            if (!is_a($part, Unprocessed::class)) {
                $kept[] = $part;
                continue;
            }
            if (preg_match_all($pattern, $part->data, $matches)) {
                $idx = 0;
                foreach (preg_split($pattern, $part->data) as $preg_part) {
                    if ($preg_part) {
                        $kept[] = new Unprocessed($preg_part);
                    }
                    if (sizeof($matches[0]) > $idx) {
                        $kept[] = new Literal($matches[0][$idx], $type);
                    }
                    $idx++;
                }
            } else {
                $kept[] = $part;
            }
        }

        return $kept;
    }

    /**
     * Quantities are sometimes 4 'g' -> Number + " " + String
     *
     * @param array $fhirPath
     *
     * @return array
     */
    private function groupNumberStringAsQuantity(array $fhirPath): array
    {
        if (sizeof($fhirPath) < 2) {
            return $fhirPath;
        }

        $kept = [$fhirPath[0]];

        // Quantity integer in Unprocessed string : [Unprocessed('.. = 1 '), String('mg')
        for ($i = 1; $i < sizeof($fhirPath); $i++) {
            $first  = $fhirPath[$i - 1];
            $second = $fhirPath[$i];

            if (
                is_a($second, Literal::class) and
                ($second->type === Literal::STRING) and
                is_a($first, Unprocessed::class) and
                (preg_match('/\d+\s+$/', $first->data, $m) == 1) // Integer not yet processed
            ) {
                $begin = substr($first->data, 0, -strlen(reset($m)));
                $value = substr($first->data, -strlen(reset($m)));
                array_pop($kept);
                $kept[] = new Unprocessed($begin);
                $kept[] = new Literal($value . trim($second->data, "'"), Literal::QUANTITY);
            } else {
                $kept[] = $second;
            }
        }

        // Quantity number : [Number(1.0), Unprocessed(' '), String('mg')
        $fhirPath = $kept;
        if (sizeof($fhirPath) < 3) {
            return $fhirPath;
        }

        $kept = [$fhirPath[0], $fhirPath[1]];

        for ($i = 2; $i < sizeof($fhirPath); $i++) {
            $first  = $fhirPath[$i - 2];
            $second = $fhirPath[$i - 1];
            $third  = $fhirPath[$i];

            if (
                is_a($third, Literal::class) and
                ($third->type === Literal::STRING) and
                is_a($second, Unprocessed::class) and
                (preg_match('/^\s+$/', $second->data) == 1) and
                is_a($first, Literal::class) and
                in_array($first->type, [Literal::DECIMAL, Literal::INTEGER])
            ) {
                array_pop($kept);
                array_pop($kept);
                $kept[] = new Literal($first->data . " " . trim($third->data, "'"), Literal::QUANTITY);
            } else {
                $kept[] = $third;
            }
        }

        return $kept;
    }
}
