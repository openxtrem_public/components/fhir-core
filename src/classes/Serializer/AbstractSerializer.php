<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Serializer;

use Ox\Components\FHIRCore\Definition\Field;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRBaseInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRElementInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRXhtmlInterface;
use Ox\Components\FHIRCore\Interfaces\Resources\FHIRResourceInterface;
use Ox\Components\FHIRCore\Serializer\Exception\SerializerException;

/**
 * Description
 */
abstract class AbstractSerializer implements SerializerInterface
{
    protected bool   $formatOutput      = true;
    protected bool   $summarize         = false;
    protected string $contentSerialized = '';


    public function serializeResource(FHIRResourceInterface $resource, array $options = []): string
    {
        $this->summarize    = $options['summarize'] ?? false;
        $this->formatOutput = $options['formatOutput'] ?? true;

        return '';
    }

    abstract protected function createEmptyElement(string $fieldName): mixed;

    /**
     * @throws SerializerException
     */
    protected function createElement(FHIRBaseInterface $object, string $fieldName): mixed
    {
        $element = $this->createEmptyElement($fieldName);

        $def = $object->getFhirResourceDefinition();

        foreach ($def->getFields() as $field) {
            $this->registerField($element, $object, $field, $fieldName);
        }

        return $element;
    }

    protected function addXHtml(mixed &$element, mixed $value, string $name): void
    {
        $this->addFinal($element, $value, $name);
    }

    /**
     * @param mixed $element Element we're creating
     *
     * @throws SerializerException
     */
    protected function registerField(mixed &$element, FHIRBaseInterface $object, Field $field, string $parentName): void
    {
        $getter = 'get' . $field->getLabel();

        $objValues = $object->$getter();
        if (is_null($objValues) || (is_array($objValues) && (count($objValues) === 0))) {
            return;
        }

        if ($this->isAttributeField($object, $field->getLabel(), $parentName, $object::RESOURCE_NAME)) {
            if ($object instanceof FHIRXhtmlInterface) {
                // xhtml fields are not stored in value attributes in XML
                $this->addXHtml($element, $objValues, $field->getLabel());
            } else {
                $this->addFinal($element, $objValues, $field->getLabel());
            }

            return;
        }

        if (!$field->isUnique() && ctype_lower($objValues[0]::RESOURCE_NAME[0])) {
            // sort if type is primitive
            usort($objValues, [$this, 'comparePrimitive']);
        }

        if (!$this->summarize || $field->isSummary()) {
            if (array_keys($field->getTypes())[0] === 'Resource') { // contains sub resource
                if ($field->isUnique()) {
                    $child = $this->createElement($objValues, $objValues::RESOURCE_NAME);
                    $this->addChildToElement($child, $element, $field->getLabel(), $objValues::RESOURCE_NAME);
                } else {
                    $contained = $this->createEmptyElement($field->getLabel());
                    foreach ($objValues as $obj_value) {
                        $child = $this->createElement($obj_value, $obj_value::RESOURCE_NAME);
                        $this->addChildToElement($child, $contained, null, $obj_value::RESOURCE_NAME);
                        $this->addChildToElement($contained, $element, $field->getLabel());
                    }
                }


                return;
            }

            $this->appendChild($element, $field, $objValues);
        }
    }

    abstract protected function addChildToElement(
        mixed   $child,
        mixed   &$element,
        ?string $fieldName = null,
        ?string $resourceName = null
    ): void;

    private function comparePrimitive(FHIRElementInterface $object1, FHIRElementInterface $object2): int
    {
        $v1 = $object1->getValue();
        $v2 = $object2->getValue();

        if (is_null($v1) || is_null($v2)) {
            return $v2 ? -1 : 1;
        }

        if (is_numeric($v1) && is_numeric($v2)) {
            return $v1 <=> $v2;
        }

        // string
        return strcmp($v1, $v2);
    }

    protected function isAttributeField(
        FHIRBaseInterface $currentObject,
        string            $fieldName,
        string            $parentName,
        string            $objectName
    ): bool {
        if (($currentObject::RESOURCE_NAME === 'Extension') && ($fieldName === 'url')) {
            return true;
        }
        if ($fieldName === 'id') {
            return true;
        }
        if (ctype_lower($currentObject::RESOURCE_NAME[0]) && ($fieldName === 'value')) {
            return true;
        }

        return false;
    }

    /**
     * @param mixed $element
     *
     * @return bool
     */
    abstract protected function checkElementEmpty(mixed $element): bool;

    abstract protected function addFinal(mixed &$element, mixed $value, string $name): void;
}
