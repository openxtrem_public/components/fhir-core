<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Serializer;

use Ox\Components\FHIRCore\Interfaces\Resources\FHIRResourceInterface;
use Ox\Components\FHIRCore\Serializer\Exception\SerializerException;

/**
 * FHIR Serializer
 */
interface SerializerInterface
{
    /**
     * @throws SerializerException
     */
    public function serializeResource(FHIRResourceInterface $resource, array $options = []): string;
}
