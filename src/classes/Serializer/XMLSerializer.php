<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Serializer;

use DOMDocument;
use DOMElement;
use DOMException;
use DOMNode;
use Ox\Components\FHIRCore\Definition\Field;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRBaseInterface;
use Ox\Components\FHIRCore\Interfaces\Resources\FHIRResourceInterface;
use Ox\Components\FHIRCore\Serializer\Exception\SerializerException;
use Throwable;

/**
 * Description
 */
class XMLSerializer extends AbstractSerializer
{
    private DOMDocument $doc;

    /**
     * @throws SerializerException
     */
    public function serializeResource(FHIRResourceInterface $resource, array $options = []): string
    {
        parent::serializeResource($resource, $options);
        $this->doc = new DOMDocument('1.0', 'UTF-8');
        /** @var DOMElement $first */
        $first = $this->createElement($resource, $resource::RESOURCE_NAME);
        $this->doc->appendChild($first);
        $first->setAttribute('xmlns', 'http://hl7.org/fhir');
        $this->doc->formatOutput = $this->formatOutput;

        return $this->contentSerialized = $this->doc->saveXML();
    }

    protected function isAttributeField(
        FHIRBaseInterface $currentObject,
        string            $fieldName,
        string            $parentName,
        string            $objectName
    ): bool {
        if (ctype_upper($parentName[0])) {
            // No attribute in the main resource created
            return false;
        }

        return parent::isAttributeField($currentObject, $fieldName, $parentName, $objectName);
    }

    protected function addXHtml(mixed &$element, mixed $value, string $name): void
    {
        $doc = new DOMDocument();
        $doc->loadXML($value);

        $v     = '';
        $child = $doc->firstChild->firstChild;
        while ($child !== null) {
            $v     .= $doc->saveXML($child);
            $child = $child->nextSibling;
        }

        try {
            $doc->loadXML("<div>$v</div>");
            $xhtml   = $this->doc->importNode($doc->firstChild, true);
            $element = $xhtml;
            $this->addAttribute($element, 'xmlns', 'http://www.w3.org/1999/xhtml');
        } catch (Throwable) {
        }
    }

    /**
     * @param DOMElement $element
     * @param mixed      $value
     * @param string     $name
     *
     * @return void
     * @throws SerializerException
     */
    protected function addFinal(mixed &$element, mixed $value, string $name): void
    {
        $this->addAttribute($element, $name, $value);
    }

    /**
     * @param DOMElement $element
     * @param string     $name
     * @param mixed      $value
     *
     * @return void
     * @throws SerializerException
     */
    private function addAttribute(DOMElement $element, string $name, mixed $value): void
    {
        if (is_object($value)) {
            $value = $value->getValue();
        }
        if (is_null($value) || ($value === '')) {
            return;
        }
        if (is_bool($value)) {
            $value = $value ? 'true' : 'false';
        }

        if (!preg_match('//u', $value) && ($value !== '')) {
            $value = mb_convert_encoding($value, 'UTF-8');
        }

        $element->setAttribute($name, $value);
    }

    /**
     * @throws SerializerException
     * @throws DOMException
     */
    protected function appendChild(mixed &$element, Field $field, mixed $objValues): void
    {
        $suffix = '';

        if ($field->isUnique()) {
            if (sizeof($field->getTypes()) > 1) {
                // specify the type used
                $suffix = ucfirst($objValues::RESOURCE_NAME);
            }
            $objValues = [$objValues];
        }
        foreach ($objValues as $objValue) {
            $newElem = $this->createElement($objValue, $field->getLabel() . $suffix);
            if (!$this->checkElementEmpty($newElem)) {
                $element->appendChild($newElem);
            }
        }
    }

    /**
     * @throws DOMException
     */
    protected function createEmptyElement(string $fieldName): mixed
    {
        return $this->doc->createElement($fieldName);
    }

    /**
     * @param DOMElement $element
     */
    protected function checkElementEmpty(mixed $element): bool
    {
        return !$element->hasChildNodes() && !$element->hasAttribute('value');
    }

    /**
     * @param DOMNode     $child
     * @param DOMNode     $element
     * @param string|null $fieldName
     * @param string|null $resourceName
     *
     * @throws DOMException
     */
    protected function addChildToElement(
        mixed   $child,
        mixed   &$element,
        ?string $fieldName = null,
        ?string $resourceName = null
    ): void {
        if (($fieldName === null) || ($fieldName === $child->nodeName)) {
            $element->appendChild($child);
        } else {
            $betweenNode = $this->createEmptyElement($fieldName);
            $betweenNode->appendChild($child);
            $element->appendChild($betweenNode);
        }
    }

    public function getDocument(): DOMDocument
    {
        return $this->doc;
    }
}
