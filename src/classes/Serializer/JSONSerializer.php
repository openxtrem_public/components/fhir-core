<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Serializer;

use Ox\Components\FHIRCore\Definition\Field;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRBaseInterface;
use Ox\Components\FHIRCore\Interfaces\Resources\FHIRResourceInterface;
use Ox\Components\FHIRCore\Serializer\Exception\SerializerException;

/**
 * Description
 */
class JSONSerializer extends AbstractSerializer
{
    /**
     * @throws SerializerException
     */
    public function serializeResource(FHIRResourceInterface $resource, array $options = []): string
    {
        parent::serializeResource($resource, $options);
        $final = $this->createElement($resource, $resource::RESOURCE_NAME);
        $final = array_merge(['resourceType' => $resource::RESOURCE_NAME], $final);

        $flags = $this->formatOutput ? JSON_PRETTY_PRINT : 0;

        return $this->contentSerialized = json_encode($final, $flags | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }

    /**
     * @param string $fieldName
     *
     * @return array
     */
    protected function createEmptyElement(string $fieldName): mixed
    {
        return [];
    }

    /**
     * @param array $element
     *
     * @return bool
     */
    protected function checkElementEmpty(mixed $element): bool
    {
        if (
            is_null($element)
            || (is_array($element) && (sizeof($element) === 0))
            || ($element === '')
        ) {
            return true;
        }

        // If one of the array isn't empty, array isn't empty
        if (is_array($element)) {
            $isEmpty = true;
            foreach ($element as $e) {
                if (!$this->checkElementEmpty($e)) {
                    $isEmpty = false;
                }
            }

            return $isEmpty;
        }

        return false;
    }

    protected function isAttributeField(
        FHIRBaseInterface $currentObject,
        string            $fieldName,
        string            $parentName,
        string            $objectName
    ): bool {
        if (ctype_upper($objectName[0]) || ($fieldName === 'id')) {
            // only in case of datatype
            return false;
        }

        return parent::isAttributeField($currentObject, $fieldName, $parentName, $objectName);
    }

    /**
     * @param mixed  $element
     * @param mixed  $value
     * @param string $name
     *
     * @return void
     */
    protected function addFinal(mixed &$element, mixed $value, string $name): void
    {
        if (!preg_match('//u', $value) && ($value !== '')) {
            $value = mb_convert_encoding($value, 'UTF-8');
        }

        // replace current array to a simple value
        $element = $value;
    }

    /**
     * @param mixed $objValues
     * @param Field $field
     *
     * @return array|null
     * @throws SerializerException
     */
    private function getPrepended(mixed $objValues, Field $field): ?array
    {
        if (ctype_upper($objValues::RESOURCE_NAME[0])) {
            return null;
        }
        $prepend    = [];
        $id         = $objValues->getId();
        $extensions = $objValues->getExtension();
        if ($id) {
            $prepend['id'] = $id->getValue();
        }
        if (sizeof($extensions) != 0) {
            $prepend['extension'] = [];
            foreach ($extensions as $ext) {
                $prepend['extension'][] = $this->createElement($ext, $field->getLabel());
            }
        }
        if (sizeof($prepend) === 0) {
            return null;
        }

        return $prepend;
    }

    /**
     * @param mixed $element
     * @param Field $field
     * @param mixed $objValues
     *
     * @return void
     * @throws SerializerException
     */
    protected function appendChild(mixed &$element, Field $field, mixed $objValues): void
    {
        $suffix = '';
        if ($field->isUnique()) {
            if ($field->getLabel() === 'resource') {
                $element['resourceType'] = $objValues::RESOURCE_NAME;
            }

            $res     = $this->createElement($objValues, $field->getLabel());
            $prepend = $this->getPrepended($objValues, $field); // extensions et id des primitifs
            if ($res === $prepend) {
                // id DatatypeString is returned (prepended)
                $res = null;
            }
            if (sizeof($field->getTypes()) > 1) {
                // specify the type used
                $suffix = ucfirst($objValues::RESOURCE_NAME);
            }
        } else {
            $res     = [];
            $prepend = [];
            foreach ($objValues as $objValue) {
                $created   = $this->createElement($objValue, $field->getLabel());
                $myPrepend = $this->getPrepended($objValue, $field);
                if ($created === $myPrepend) {
                    // id DatatypeString is returned (prepended)
                    $created = null;
                }
                if ($field->getLabel() === 'resource') {
                    $res['resourceType'] = $objValue::RESOURCE_NAME;
                }
                $res[]     = $created;
                $prepend[] = $myPrepend;
            }
            if (self::arrayIsNull($prepend)) {
                $prepend = null;
            }
            if (self::arrayIsNull($res)) {
                $res = null;
            }
        }

        if ($this->checkElementEmpty($res) && $this->checkElementEmpty($prepend)) {
            return;
        }

        if (!is_null($res)) {
            $element[$field->getLabel() . $suffix] = $res;
        }
        if (!is_null($prepend)) {
            $element['_' . $field->getLabel() . $suffix] = $prepend;
        }
    }

    public static function arrayIsNull(array $array): bool
    {
        $null = true;
        foreach ($array as $elem) {
            if (!is_null($elem)) {
                $null = false;
            }
        }

        return $null;
    }

    /**
     * @param array $child
     * @param array $element
     */
    protected function addChildToElement(
        mixed   $child,
        mixed   &$element,
        ?string $fieldName = null,
        ?string $resourceName = null
    ): void {
        if ($resourceName !== null) {
            $child = array_merge(['resourceType' => $resourceName], $child);
        }

        if ($fieldName !== null) {
            $element[$fieldName] = $child;
        } else {
            $element[] = $child;
        }
    }
}
