<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Serializer\Exception;

use Exception;

class SerializerException extends Exception
{
    public static function invalidFormat(string $format): self
    {
        return new self("'$format' is not a valid format.");
    }
}
