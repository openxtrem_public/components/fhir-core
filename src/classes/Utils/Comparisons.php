<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\FHIRCore\Utils;

use Exception;
use Ox\Components\FHIRCore\Definition\ResourceDefinition;
use Ox\Components\FHIRCore\FHIRPath\Exception\FHIRPathException;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\BooleanLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\DateTimeLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\DecimalLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\IntegerLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\LiteralInterface;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\QuantityLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\StringLiteral;
use Ox\Components\FHIRCore\FHIRPath\Expressions\Literals\TimeLiteral;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRBaseInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRElementInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\Complex\FHIRQuantityInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRBase64BinaryInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRBooleanInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRDateInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRDateTimeInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRDecimalInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRIdInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRInstantInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRIntegerInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRMarkdownInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRPositiveIntInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRStringInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRTimeInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRUnsignedIntInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRUriInterface;
use Ox\Components\FHIRCore\Interfaces\Datatypes\FHIRUuidInterface;
use Throwable;

/**
 * Description
 */
class Comparisons
{
    final public const TYPES_CORRESPONDENCES = [
        BooleanLiteral::class  => [FHIRBooleanInterface::class],
        StringLiteral::class   => [
            FHIRStringInterface::class,
            FHIRUriInterface::class,
            FHIRIdInterface::class,
            FHIRUuidInterface::class,
            FHIRMarkdownInterface::class,
            FHIRBase64BinaryInterface::class,
        ],
        IntegerLiteral::class  => [
            FHIRIntegerInterface::class,
            FHIRUnsignedIntInterface::class,
            FHIRPositiveIntInterface::class,
        ],
        DecimalLiteral::class  => [FHIRDecimalInterface::class],
        DateTimeLiteral::class => [FHIRDateInterface::class, FHIRDateTimeInterface::class, FHIRInstantInterface::class],
        TimeLiteral::class     => [FHIRTimeInterface::class],
    ];

    /**
     * @throws FHIRPathException
     */
    public function equal(FHIRBaseInterface $left, FHIRBaseInterface $right): bool
    {
        if ($left::RESOURCE_NAME !== $right::RESOURCE_NAME) {
            try {
                [$left, $right] = $this->implicitConversion($left, $right);
            } catch (Throwable) {
                return false;
            }
        }

        if (
            is_a($left, FHIRTimeInterface::class) or
            is_a($left, FHIRDateTimeInterface::class) or
            is_a($left, FHIRDateInterface::class)
        ) {
            /** @var FHIRDateInterface|FHIRTimeInterface|FHIRDateTimeInterface $right */
            $a = date_create($left->getValue());
            $b = date_create($right->getValue());

            return $b == $a;
        }

        if (is_a($left, QuantityLiteral::class) || is_a($right, QuantityLiteral::class)) {
            return ($left->getValue()->getValue() === $right->getValue()->getValue()) and
                ($left->getUnit()->getValue() === $right->getUnit()->getValue());
        }

        $definition = $left->getFhirResourceDefinition();

        foreach ($definition->getFields() as $field) {
            // TODO : for objects : which fields ? extensions ? id ?
            if (($field->getLabel() === 'id') || ($field->getLabel() === 'extension')) {
                continue;
            }

            $l = $left->{'get' . ucfirst($field->getLabel())}();
            $r = $right->{'get' . ucfirst($field->getLabel())}();

            if ($field->isUnique()) {
                $l = [$l];
                $r = [$r];
            }

            if (count($l) != count($r)) {
                return false;
            }

            for ($i = 0; $i < count($l); $i++) {
                if (is_object($l[$i]) && is_object($r[$i])) {
                    if (!$this->equal($l[$i], $r[$i])) {
                        return false;
                    }
                } elseif (is_object($l[$i]) || is_object($r[$i])) {
                    // one of them is object, the other not
                    return false;
                } elseif ($l[$i] !== $r[$i]) {
                    if (is_double($l[$i]) && (abs($l[$i] - $r[$i]) < 1e-8)) {
                        continue;
                    }

                    return false;
                }
            }
        }

        return true;
    }

    public function equivalent(FHIRBaseInterface $left, FHIRBaseInterface $right): bool
    {
        if ($left::RESOURCE_NAME !== $right::RESOURCE_NAME) {
            try {
                [$left, $right] = $this->implicitConversion($left, $right);
            } catch (FHIRPathException|Throwable) {
                return false;
            }
        }

        if (is_a($left, FHIRIntegerInterface::class) || is_a($left, FHIRBooleanInterface::class)) {
            /** @var FHIRIntegerInterface|FHIRBooleanInterface $right */
            return $left->getValue() === $right->getValue();
        }

        if (is_a($left, FHIRDecimalInterface::class)) {
            /** @var FHIRIntegerInterface|FHIRBooleanInterface $right */
            $value_a = $left->getValue();
            $value_b = $right->getValue();

            $precision_a = $this->getDecimalPrecision($value_a);
            $precision_b = $this->getDecimalPrecision($value_b);
            $round       = min($precision_a, $precision_b);

            return round($value_a, $round) == round($value_b, $round);
        }

        if (is_a($left, FHIRQuantityInterface::class)) {
            /** @var FHIRQuantityInterface $right */
            return $this->equivalent($left->getValue(), $right->getValue()) and
                ($left->getUnit()?->getValue() === $right->getUnit()?->getValue());
        }

        if (
            is_a($left, FHIRTimeInterface::class) or
            is_a($left, FHIRDateTimeInterface::class) or
            is_a($left, FHIRDateInterface::class)
        ) {
            /** @var FHIRDateInterface|FHIRTimeInterface|FHIRDateTimeInterface $right */
            if (!$this->isSameDateTimePrecision($left, $right)) {
                return false;
            }

            $a = date_create($left->getValue());
            $b = date_create($right->getValue());

            return $b == $a;
        }

        if (is_a($left, FHIRStringInterface::class)) {
            /** @var FHIRStringInterface $right */
            $a = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $left->getValue());
            $b = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $right->getValue());

            return strcasecmp($a, $b) === 0;
        }

        $definition = $left->getFhirResourceDefinition();

        foreach ($definition->getFields() as $field) {
            $label   = $field->getLabel();
            $value_a = $left->{'get' . ucfirst($label)}();
            $value_b = $right->{'get' . ucfirst($label)}();

            if (is_null($value_a) || is_null($value_b)) {
                if (!is_null($value_a) || !is_null($value_b)) {
                    return false;
                }

                return true;
            }

            if (is_array($value_a) || is_array($value_b)) {
                if (!is_array($value_a) || !is_array($value_b)) {
                    return false;
                }
                if (sizeof($value_a) != sizeof($value_b)) {
                    return false;
                }
                foreach ($value_a as $element) {
                    if (!$this->equivalentInArray($element, $value_b)) {
                        return false;
                    }
                }

                return true;
            }
        }

        return false;
    }

    /**
     * @param FHIRBaseInterface   $element
     * @param FHIRBaseInterface[] $array
     *
     * @return bool
     */
    public function equivalentInArray(FHIRBaseInterface $element, array $array): bool
    {
        foreach ($array as $item) {
            if ($this->equivalent($element, $item)) {
                return true;
            }
        }

        return false;
    }

    public function getDecimalPrecision(float $number): int
    {
        $explode = explode('.', strval($number), 2);
        if (sizeof($explode) == 1) {
            return 0;
        }

        return strlen(end($explode));
    }

    /**
     * @throws FHIRPathException
     */
    public function includes(mixed $needle, array $haystack): bool
    {
        foreach ($haystack as $element) {
            if ($this->equal($needle, $element)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @throws FHIRPathException
     */
    public function subsetOf(array $sub, array $super): bool
    {
        foreach ($sub as $element) {
            if (!$this->includes($element, $super)) {
                return false;
            }
        }

        return true;
    }

    public function isTypeOrSubclass(FHIRBaseInterface $resource, string $type): bool
    {
        if ($resource::RESOURCE_NAME === $type) {
            return true;
        }

        if ($resource instanceof LiteralInterface) {
            if ($resource::LITERAL_NAME === $type) {
                return true;
            }

            return false;
        }

        return $this->isTypeOrSubclassDefinition($resource->getFhirResourceDefinition(), $type);
    }

    private function isTypeOrSubclassDefinition(?ResourceDefinition $definition, string $type): bool
    {
        if ($definition === null) {
            return false;
        }

        if ($definition->getResourceName() === $type) {
            return true;
        }

        return $this->isTypeOrSubclassDefinition($definition->getParent(), $type);
    }

    public function fhirTypeToFhirPathType(FHIRElementInterface $datatype): ?LiteralInterface
    {
        if (is_a($datatype, LiteralInterface::class)) {
            return $datatype;
        }

        if (is_a($datatype, FHIRQuantityInterface::class)) {
            return (new QuantityLiteral())
                ->setValue((new DecimalLiteral())->setValue($datatype->getValue()->getValue()))
                ->setUnit((new StringLiteral())->setValue($datatype->getUnit()->getValue()));
        }

        foreach (self::TYPES_CORRESPONDENCES as $to => $from) {
            if (in_array($datatype::class, $from)) {
                return (new $to())->setValue($datatype->getValue());
            }
        }

        return null;
    }

    /**
     * @param FHIRElementInterface $first
     * @param FHIRElementInterface $second
     * @param bool                 $firstConversion
     *
     * @return array<FHIRElementInterface, FHIRElementInterface>
     * @throws FHIRPathException
     * @throws Exception
     */
    public function implicitConversion(
        FHIRElementInterface $first,
        FHIRElementInterface $second,
        bool                 $firstConversion = true
    ): array {
        if ($system_type = $this->fhirTypeToFhirPathType($first)) {
            $first = $system_type;
        }
        if ($system_type = $this->fhirTypeToFhirPathType($second)) {
            $second = $system_type;
        }

        if ($first::RESOURCE_NAME === $second::RESOURCE_NAME) {
            return [$first, $second];
        }

        if (
            (($first instanceof FHIRStringInterface) || ($first instanceof FHIRUriInterface)) &&
            (($second instanceof FHIRStringInterface) || ($second instanceof FHIRUriInterface))
        ) {
            /** @var FHIRStringInterface $first */
            /** @var FHIRStringInterface $second */
            return [
                (new StringLiteral())->setValue($first->getValue()),
                (new StringLiteral())->setValue($second->getValue()),
            ];
        }

        if (is_a($first, FHIRIntegerInterface::class)) {
            if (is_a($second, FHIRDecimalInterface::class)) {
                $new = clone $first;
                $new->setValue($first->getValue());

                return [$new, $second];
            }
            if (is_a($second, FHIRQuantityInterface::class)) {
                $new = clone $first;
                $new->setValue($first->getValue());

                return [$new, $second];
            }
        }

        if (is_a($first, FHIRDecimalInterface::class) && is_a($second, FHIRQuantityInterface::class)) {
            $new = clone $first;
            $new->setValue($first->getValue());

            return [$new, $second];
        }

        if (is_a($first, FHIRDateInterface::class) && is_a($second, FHIRDateTimeInterface::class)) {
            $new = clone $first;
            $new->setValue($first->getValue());

            return [$new, $second];
        }

        if ($firstConversion) {
            [$a, $b] = $this->implicitConversion($second, $first, false);

            return [$b, $a];
        }

        throw FHIRPathException::implicitConversion($first::RESOURCE_NAME, $second::RESOURCE_NAME);
    }

    public function isSameDateTimePrecision(
        FHIRDateTimeInterface|FHIRDateInterface|FHIRTimeInterface $a,
        FHIRDateTimeInterface|FHIRDateInterface|FHIRTimeInterface $b
    ): bool {
        [$pa, $pb] = $this->dateTimeParts($a, $b);

        $malus_a = in_array(substr(end($pa), 0, 1), ['Z', '+', '-']) ? 1 : 0;
        $malus_b = in_array(substr(end($pb), 0, 1), ['Z', '+', '-']) ? 1 : 0;

        return ($malus_a == $malus_b) && (sizeof($pa) - $malus_a == sizeof($pb) - $malus_b);
    }

    public function dateTimeParts(
        FHIRDateTimeInterface|FHIRDateInterface|FHIRTimeInterface $a,
        FHIRDateTimeInterface|FHIRDateInterface|FHIRTimeInterface $b
    ): array {
        $va = $a->getValue();
        $vb = $b->getValue();

        if (is_a($a, FHIRDateTimeInterface::class) || is_a($a, FHIRDateInterface::class)) {
            $pattern = '/^(\d{4})(?:-(\d{2})(?:-(\d{2})(?:T([01]\d|2[0-4])(?::([0-5]\d)(?::([0-5]\d(?:\.\d+)?))?)?(Z|[+-][0-5]\d:[0-5]\d)?)?)?)?$/';
        } else {
            $pattern = '/^T([01]\d|2[0-4])(?::([0-5]\d)(?::([0-5]\d(?:\.\d+)?))?)?(Z|[+-][0-5]\d:[0-5]\d)?$/';
        }

        $matches_a = [];
        $matches_b = [];
        preg_match($pattern, $va, $matches_a);
        preg_match($pattern, $vb, $matches_b);

        return [$matches_a, $matches_b];
    }

    public function descendants(FHIRBaseInterface $resource): array
    {
        $fields      = $resource->getFhirResourceDefinition()->getFields();
        $descendants = [];

        foreach ($fields as $f) {
            $value = $resource->{'get' . (ucfirst($f->getLabel()))}();
            if (!is_null($value)) {
                if ($f->isUnique()) {
                    if (is_object($value)) {
                        $descendants[] = $value;
                        $descendants   = array_merge($descendants, $this->descendants($value));
                    }
                } elseif (is_object(reset($value))) {
                    $descendants = array_merge($descendants, $value);
                    $descendants = array_merge($descendants, ...array_map(fn($d) => $this->descendants($d), $value));
                }
            }
        }

        return $descendants;
    }
}
